package rs.ac.uns.ftn.ktsnvt.repository.unit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.model.entity.Building;
import rs.ac.uns.ftn.ktsnvt.model.entity.Council;
import rs.ac.uns.ftn.ktsnvt.model.entity.Meeting;
import rs.ac.uns.ftn.ktsnvt.model.entity.Record;
import rs.ac.uns.ftn.ktsnvt.repository.BuildingRepository;
import rs.ac.uns.ftn.ktsnvt.repository.NotificationRepository;
import rs.ac.uns.ftn.ktsnvt.repository.RecordRepository;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Ivana Zeljkovic on 07/12/2017.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class RecordRepositoryUnitTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private RecordRepository recordRepository;

    private Long building_id, record_id, meeting_id;


    @Before
    public void setUp() {


        Building building = new Building();
        Council council = new Council();
        Meeting meeting = new Meeting();
        Record record = new Record("Record");

        building.setCouncil(council);
        council.setMeetings(new ArrayList<Meeting>(){{add(meeting);}});
        council.getMeetings().get(0).setRecord(record);

        meeting.setCouncil(council);
        record.setMeeting(meeting);

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, -1);

        meeting.setDate(cal.getTime());

        building_id = entityManager.persistAndGetId(building, Long.class);
        entityManager.persist(council);
        meeting_id = entityManager.persistAndGetId(meeting, Long.class);
        record_id = entityManager.persistAndGetId(record, Long.class);

    }

    @Test
    @Transactional
    public void testFindAllByBuildingId(){
        /*
         * Test proverava ispravnost rada metode findAllByBuildingId repozitorijuma RecordRepository
         * (pronalazenje svih izvestaja na osnovu ID-a zgrade)
         * */

        // zgradi sa ID-em pripada samo jedan izvestaj sa ID-em record_id
        Page<Record> records = this.recordRepository.findAllByBuildingId(building_id, Calendar.getInstance().getTime(),
                new PageRequest(0, 2, Sort.Direction.ASC, "id"));


        assertEquals(1, records.getTotalElements());
        assertEquals(record_id, records.getContent().get(0).getId());
    }
}
