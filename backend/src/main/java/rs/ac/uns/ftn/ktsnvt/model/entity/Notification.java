package rs.ac.uns.ftn.ktsnvt.model.entity;

import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "notification")
@Where(clause="deleted=0")
public class Notification {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Column(nullable = false)
    private String title;

    @Column(nullable = false)
    private String content;

    @Column(nullable = false)
    private Date date;

    @Column(nullable = false, columnDefinition = "INTEGER DEFAULT 0")
    @Version
    private int version;

    @Column(nullable = false, columnDefinition = "BOOL DEFAULT FALSE")
    private boolean deleted;

    @OneToOne
    private Tenant creator;

    @ManyToOne
    private Billboard billboard;

    public Notification() { }

    public Notification(String title, String content, Date date) {
        this.title = title;
        this.content = content;
        this.date = date;
    }

    public Notification(String title, String content, Date date, Tenant creator, Billboard billboard) {
        this.title = title;
        this.content = content;
        this.date = date;
        this.creator = creator;
        this.billboard = billboard;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() { return title; }

    public void setTitle(String title) { this.title = title; }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Tenant getCreator() {
        return creator;
    }

    public void setCreator(Tenant creator) {
        this.creator = creator;
    }

    public Billboard getBillboard() { return billboard; }

    public void setBillboard(Billboard billboard) { this.billboard = billboard; }
}
