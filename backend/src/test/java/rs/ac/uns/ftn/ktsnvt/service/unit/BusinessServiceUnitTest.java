package rs.ac.uns.ftn.ktsnvt.service.unit;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.exception.ForbiddenException;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.*;
import rs.ac.uns.ftn.ktsnvt.model.enumeration.BusinessType;
import rs.ac.uns.ftn.ktsnvt.repository.BuildingRepository;
import rs.ac.uns.ftn.ktsnvt.repository.BusinessRepository;
import rs.ac.uns.ftn.ktsnvt.service.BuildingService;
import rs.ac.uns.ftn.ktsnvt.service.BusinessService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;

/**
 * Created by Ivana Zeljkovic on 29/11/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BusinessServiceUnitTest {

    @Autowired
    private BusinessService businessService;

    @Autowired
    private BuildingService buildingService;

    @MockBean
    private BusinessRepository businessRepository;

    @MockBean
    private BuildingRepository buildingRepository;


    @Before
    public void setUp() {
        // FIRM, INSTITUTTIONS
        Business firm = new Business("First firm", "Description for first firm", "123456789",
                "ivana.zeljkovic995@gmail.com", BusinessType.valueOf("FIRM_FOR_PLUMBING"), true);
        Business institution_1 = new Business("Second institution", "Description for second institution", "123456789",
                "ivana.zeljkovic995@gmail.com", BusinessType.valueOf("FIRE_SERVICE"), false);
        Business institution_2 = new Business("Third institution", "Description for third institution", "987654321",
                "ivana.zeljkovic995@gmail.com", BusinessType.valueOf("FIRE_SERVICE"), false);

        // ADDRESS FOR FIRM
        City city = new City("Novi Sad", 21000);
        Address address = new Address("Janka VEselinovica", 8, city);
        address.setId(new Long(102));
        firm.setAddress(address);

        // BUILDINGS
        Building building_1 = new Building(5, true);
        building_1.setId(new Long(100));
        Council council_1 = new Council();
        council_1.setResponsibleCompany(firm);
        building_1.setCouncil(council_1);

        Building building_2 = new Building(5, false);
        building_2.setId(new Long(101));
        Council council_2 = new Council();
        building_2.setCouncil(council_2);

        // RESULTS
        List<Business> firms_and_institutions = new ArrayList<>();
        firms_and_institutions.add(firm);
        List<Business> firms_and_institutions_empty = new ArrayList<>();
        List<Business> firms = new ArrayList<>();
        firms.add(firm);
        List<Business> institutions = new ArrayList<>();
        institutions.add(institution_1);
        institutions.add(institution_2);

        given(
                this.businessRepository.findOne(new Long(100))
        ).willReturn(
                firm
        );

        given(
                this.businessRepository.findOne(new Long(200))
        ).willReturn(
                null
        );

        given(
                this.businessRepository.findByAddressId(new Long(102), true)
        ).willReturn(
                firm
        );

        given(
                this.businessRepository.findByAddressId(new Long(105), true)
        ).willReturn(
                null
        );

        given(
                this.buildingRepository.findOne(new Long(100))
        ).willReturn(
                building_1
        );

        given(
                this.buildingRepository.findOne(new Long(101))
        ).willReturn(
                building_2
        );

    }

    @Test
    public void testFindOne() {
        /*
         * Test proverava ispravnost rada metode findOne servisa BusinessService
         * (dobavljanje objekta firme/institucije na osnovu ID-a)
         * u slucaju kada postoji firma/institucija sa zadatim ID-em
         * */

        /*
        firma sa ID-em = 100 ima podatke:
            naziv = First firm,
            opis = Description for first firm,
            broj telefona = 123456789,
            email = ivana.zeljkovic995@gmail.com
        */
        Business firm = this.businessService.findOne(new Long(100));

        assertNotNull(firm);
        assertEquals("First firm", firm.getName());
        assertEquals("Description for first firm", firm.getDescription());
        assertEquals("123456789", firm.getPhoneNumber());
        assertEquals("ivana.zeljkovic995@gmail.com", firm.getEmail());
        assertTrue(firm.isIsCompany());
    }

    @Test(expected = NotFoundException.class)
    public void testFindOneNonexistent() {
        /*
         * Test proverava ispravnost rada metode findOne servisa BusinessService
         * (dobavljanje objekta firme/institucije na osnovu ID-a)
         * u slucaju kada ne postoji firma/institucija sa zadatim ID-em - ocekivano NotFoundException
         * */

        // firma sa ID-em = 200 ne postoji
        Business firm = this.businessService.findOne(new Long(200));
    }

    @Test
    public void testFindByAddressId() {
        /*
         * Test proverava ispravnost rada metode findByAddressId servisa BusinessService
         * (dobavljanje objekta firme/institucije koja se nalazi na adresi sa zadatim ID-em)
         * u slucaju kada postoji firma na adresi ciji ID odgovara prosledjenom ID-u
         * */

        /*
        firma sa ID-em = 100 je na adresi sa ID-em = 102
        podaci firme:
            naziv = First firm,
            opis = Description for first firm,
            tip = FIRM_FOR_PLUMBING
        */
        Business firm = this.businessService.findByAddressId(new Long(102), true);

        assertNotNull(firm);
        assertEquals("First firm", firm.getName());
        assertEquals("Description for first firm", firm.getDescription());
        assertEquals(BusinessType.valueOf("FIRM_FOR_PLUMBING"), firm.getBusinessType());
        assertTrue(firm.isIsCompany());
    }

    @Test(expected = NotFoundException.class)
    public void testFindByAddressIdNonexistent() {
        /*
         * Test proverava ispravnost rada metode findByAddressId servisa BusinessService
         * (dobavljanje objekta firme/institucije koja se nalazi na adresi sa zadatim ID-em)
         * u slucaju kada ne postoji firma na adresi ciji ID odgovara prosledjenom ID-u - ocekivano NotFoundException
         * */

        // ne postoji firma na adresi sa ID-em = 105
        Business firm = this.businessService.findByAddressId(new Long(105), true);

    }

    @Test(expected = NullPointerException.class)
    public void testCheckPermissionForCurrentBusiness() {
        /*
         * Test proverava ispravnost rada metode checkPermissionForCurrentBusiness servisa BusinessService
         * (provera poklapanja ID-a trenutno logovane firme/institucije i prosledjenog ID-a)
         * u slucaju kada nema trenutno ulogovane firme/institucije na nivou aplikacije - ocekivano NullPointerException
         * */

        // trenutno nema firme/institucije na sesiji
        this.businessService.checkPermissionForCurrentBusiness(new Long(100));
    }

    @Test(expected = NullPointerException.class)
    public void testCheckPermissionForCurrentBusinessInBuilding() {
        /*
         * Test proverava ispravnost rada metode checkPermissionForCurrentBusinessInBuilding servisa BusinessService
         * (provera da li je trenutno logovana firma/institucija odgovorna za upravljanje zgradom koja je prosledjena)
         * u slucaju kada nema trenutno logovane firme/institucije u aplikaciji - ocekivano NullPointerException
         * */

        /*
        trenutno nema firme/institucije na sesiji
        u zgradi sa ID-em = 100 postoji odgovorna firma za upravljanje zgradom (ID = 100)
        */
        Building building = this.buildingService.findOne(new Long(100));

        this.businessService.checkPermissionForCurrentBusinessInBuilding(building);
    }

    @Test(expected = ForbiddenException.class)
    public void testCheckPermissionForCurrentBusinessInBuildingNonexistentFirm() {
        /*
         * Test proverava ispravnost rada metode checkPermissionForCurrentBusinessInBuilding servisa BusinessService
         * (provera da li je trenutno logovana firma/institucija odgovorna za upravljanje zgradom koja je prosledjena)
         * u slucaju kada nema trenutno logovane firme/institucije u aplikaciji - ocekivano ForbiddenException
         * */

        /*
        trenutno nema firme/institucije na sesiji
        u zgradi sa ID-em = 101 ne postoji odgovorna firma za upravljanje zgradom
        */
        Building building = this.buildingService.findOne(new Long(101));

        this.businessService.checkPermissionForCurrentBusinessInBuilding(building);
    }
}
