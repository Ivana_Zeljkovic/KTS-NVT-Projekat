import { TestBed, inject } from '@angular/core/testing';

import { QuestionnaireService } from './questionnaire.service';
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {QuestionnaireResults} from "../../shared/models/questionnaire-results";

describe('QuestionnaireService', () => {
  let questionnaireService: QuestionnaireService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [ QuestionnaireService ]
    });

    questionnaireService = TestBed.get(QuestionnaireService);
    httpMock = TestBed.get(HttpTestingController);
  });

  // make sure that there are no outstanding requests
  afterEach(() => {
    httpMock.verify();
  });



  it('should be created', inject([QuestionnaireService], (service: QuestionnaireService) => {
    expect(service).toBeTruthy();
  }));


  it('should return results for questionnaire with given id', () => {
    const response = {
      'questionnaireId': 1,
      'numberOfVotes': 5,
      'results': [
        {
          'questionId': 1,
          'content': 'First question',
          'answers': [
            {
              'content': 'First answer on first question',
              'numberOfVotes': 2
            },
            {
              'content': 'Second answer on first question',
              'numberOfVotes': 1
            },
            {
              'content': 'Third answer on first question',
              'numberOfVotes': 2
            }
          ]
        },
        {
          'questionId': 2,
          'content': 'Second question',
          'answers': [
            {
              'content': 'First answer on second question',
              'numberOfVotes': 1
            },
            {
              'content': 'Second answer on second question',
              'numberOfVotes': 1
            },
            {
              'content': 'Third answer on second question',
              'numberOfVotes': 3
            }
          ]
        }
      ]
    };

    questionnaireService.getResults(100, 1)
      .subscribe((results: QuestionnaireResults) => {
        expect(results).toEqual(response);
      });
    const httpRequest = httpMock.expectOne('/api/buildings/100/questionnaires/1/results');
    expect(httpRequest.request.method).toEqual('GET');
    httpRequest.flush(response);
  });
});
