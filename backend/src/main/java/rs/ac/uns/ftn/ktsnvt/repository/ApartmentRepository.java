package rs.ac.uns.ftn.ktsnvt.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import rs.ac.uns.ftn.ktsnvt.model.entity.Apartment;

/**
 * Created by Nikola Garabandic on 06/11/2017.
 */
public interface ApartmentRepository extends JpaRepository<Apartment, Long> {

    @Query("SELECT a FROM Apartment a WHERE a.building.id = :id")
    Page<Apartment> getAllFromBuilding(@Param("id") Long buildingId, Pageable pageable);

    @Query("SELECT a FROM Apartment a WHERE a.id NOT IN " +
            "(SELECT DISTINCT(t.apartmentLiveIn.id) FROM Tenant t WHERE t.apartmentLiveIn.id IS NOT NULL)")
    Page<Apartment> findFreeApartments(Pageable pageable);

    @Query("SELECT a FROM Apartment a WHERE a.building.id = :building_id AND a.id NOT IN " +
            "(SELECT t.apartmentLiveIn.id FROM Tenant t WHERE t.apartmentLiveIn.id IS NOT NULL)")
    Page<Apartment> findFreeApartmentsInBuilding(@Param("building_id") Long buildingId, Pageable pageable);

    @Query(value = "SELECT a FROM Apartment a WHERE a.surface >= :minVal AND a.surface <= :maxVal " +
            "AND a.building.id = :buildingId AND a.id NOT IN " +
            "(SELECT t.apartmentLiveIn.id FROM Tenant t WHERE t.apartmentLiveIn.id IS NOT NULL)")
    Page<Apartment> findFreeApartmentsBySize(@Param("minVal") int min, @Param("maxVal") int max,
                                             @Param("buildingId") Long buildingId, Pageable pageable);

    @Query(value = "SELECT COUNT(*) FROM Apartment a WHERE a.owner_id = :owner_id AND a.building_id = :building_id",
            nativeQuery = true)
    int countApartmentsInBuilding(@Param("owner_id") Long ownerId, @Param("building_id") Long buildingId);
}
