import {by, element, ElementFinder} from "protractor";

export class BuildingRegistrationPage {
  // get methods
  getPageTitle(): ElementFinder {
    return element(by.css('.panel-title'));
  }

  getPanelBody(): ElementFinder {
    return element(by.css('.panel-body'));
  }

  getNumberOfFloors(): ElementFinder {
    return element(by.id('numberOfFloors'));
  }

  getDropdownFloor(): ElementFinder {
    return element(by.id('initialFloor'));
  }

  getRadioParking(): ElementFinder {
    return element(by.id('radioParking'));
  }

  getApartmentHeader(): ElementFinder {
    return element.all(by.css('.account-header')).get(0);
  }

  getApartmentTable(): ElementFinder {
    return element(by.id('apartTable'));
  }

  getDisplayAddApartment(): ElementFinder {
    return element.all(by.css('.btn.btn-primary.account')).get(0);
  }

  getStreet(): ElementFinder {
    return element(by.id('street'));
  }

  getNumber(): ElementFinder {
    return element(by.id('number'));
  }

  getCity(): ElementFinder {
    return element(by.id('city'));
  }

  getPostalCode(): ElementFinder {
    return element(by.id('postalCode'));
  }

  getManagerHeader(): ElementFinder {
    return element.all(by.css('.account-header')).get(1);
  }

  getManagerTable(): ElementFinder {
    return element(by.id('managerTable'));
  }

  getDisplayAddManager(): ElementFinder {
    return element.all(by.css('.btn.btn-primary.account')).get(1);
  }

  getSaveButton(): ElementFinder {
    return element(by.buttonText('Register'));
  }

  getToaster(): ElementFinder {
    return element(by.tagName("toaster-container"));
  }

    // set methods
  setStreet(text: string) {
    this.getStreet().sendKeys(text);
  }

  setNumber(text: string) {
    this.getNumber().sendKeys(text);
  }

  setCity(text: string) {
    this.getCity().sendKeys(text);
  }

  setPostalCode(text: string) {
    this.getPostalCode().sendKeys(text);
  }

  // get error methods
  getStreetRequiredError(): ElementFinder {
    return element(by.id('streetRequiredError'));
  }

  getStreetMinLengthError(): ElementFinder {
    return element(by.id('streetMinLengthError'));
  }

  getNumberRequiredError(): ElementFinder {
    return element(by.id('numberRequiredError'));
  }

  getNumberSpaceError(): ElementFinder {
    return element(by.id('numberSpaceError'));
  }

  getCityRequiredError(): ElementFinder {
    return element(by.id('cityRequiredError'));
  }

  getPostalCodeRequiredError(): ElementFinder {
    return element(by.id('postalCodeRequiredError'));
  }

  getPostalCodeLengthError(): ElementFinder {
    return element(by.id('postalCodeLengthError'));
  }
}
