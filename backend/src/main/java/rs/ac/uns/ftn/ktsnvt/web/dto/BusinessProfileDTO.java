package rs.ac.uns.ftn.ktsnvt.web.dto;

import rs.ac.uns.ftn.ktsnvt.model.entity.Business;

import java.util.ArrayList;
import java.util.List;

public class BusinessProfileDTO {

    private Long id;
    private String name;
    private String description;
    private String phoneNumber;
    private String email;
    private String businessType;
    private AddressDTO address;
    private List<LoginDTO> usernamesList;
    private boolean deactivated;

    public BusinessProfileDTO(){
    }

    public BusinessProfileDTO(Business b)
    {
        this.id = b.getId();
        this.name = b.getName();
        this.description = b.getDescription();
        this.phoneNumber = b.getPhoneNumber();
        this.email = b.getEmail();
        this.businessType = b.getBusinessType().toString();
        this.address = new AddressDTO(b.getAddress());
        this.usernamesList = new ArrayList<>();

        b.getAccounts().forEach(account -> usernamesList.add(new LoginDTO(account.getUsername(), account.getPassword())));
        this.deactivated = b.isDeleted();

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public AddressDTO getAddress() {
        return address;
    }

    public void setAddress(AddressDTO address) {
        this.address = address;
    }

    public List<LoginDTO> getUsernamesList() {
        return usernamesList;
    }

    public void setUsernamesList(List<LoginDTO> usernamesList) {
        this.usernamesList = usernamesList;
    }

    public boolean isDeactivated() {
        return deactivated;
    }

    public void setDeactivated(boolean deactivated) {
        this.deactivated = deactivated;
    }
}
