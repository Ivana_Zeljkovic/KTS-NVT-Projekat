import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";
// component
import { TenantHomeComponent } from "./tenant-home/tenant-home.component";
import { NotificationListComponent } from "./notification-list/notification-list.component";
import { MeetingItemListComponent } from "./meeting-item-list/meeting-item-list.component";
import { NotificationComponent } from "./notification/notification.component";
import { MeetingItemComponent } from "./meeting-item/meeting-item.component";


const routes: Routes = [
  {
    path: '',
    component: TenantHomeComponent,
    children:[
      {
        path: 'notifications',
        component: NotificationListComponent
      },
      {
        path: 'meeting-details-items',
        component: MeetingItemListComponent
      },
      {
        path: '**',
        redirectTo: ''
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TenantHomeRouterModule { }
