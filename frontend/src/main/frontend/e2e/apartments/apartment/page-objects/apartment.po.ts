import {by, element, ElementFinder} from "protractor";

export class ApartmentPage {
  // get methods
  getHeading(): ElementFinder {
    return element(by.css('.panel-heading.panel-heading-personal'));
  }

  getTenantTable(): ElementFinder {
    return element(by.id('tenantTable'));
  }

  getNoElements(): ElementFinder {
    return element(by.tagName("h3"));
  }

  getAddTenantButton(): ElementFinder {
    return element(by.buttonText("Add new tenant"));
  }

  getApartmentTable(): ElementFinder {
    return element(by.id('apartmentTable'));
  }

  getChangeOwnerButton(): ElementFinder {
    return element(by.buttonText("Change owner"));
  }

  getToaster(): ElementFinder {
    return element(by.tagName("toaster-container"));
  }
}
