package rs.ac.uns.ftn.ktsnvt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.core.env.Environment;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import rs.ac.uns.ftn.ktsnvt.model.entity.Business;
import rs.ac.uns.ftn.ktsnvt.model.entity.Damage;
import rs.ac.uns.ftn.ktsnvt.model.entity.Offer;
import rs.ac.uns.ftn.ktsnvt.model.entity.Tenant;

/**
 * Created by Katarina Cukurov on 23/11/2017.
 */
@Service
public class EmailService {

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private Environment env;

    private static final String URL = "http://localhost:4200/#/";
    private static final String EMAIL_START = "Dear";
    private static final String EMAIL_USERNAME = "spring.mail.username";

    @Async
    public void sendEmailDamageToFirms(Business business, Damage damage, Tenant tenant, Business institution) throws MailException, InterruptedException {

        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(business.getEmail());
        mail.setFrom(env.getProperty(EMAIL_USERNAME));
        mail.setSubject(EMAIL_START + business.getName());
        if(institution == null)
            mail.setText(EMAIL_START + business.getName() + ",\n\nWe need your offer for the newest damage we've got in our building:\n\n" +
                    "To get more details and to send the offer, please visit the following link: " + URL + "damage-requests\n\nBest regards,\n"+
            tenant.getFirstName() + "  " + tenant.getLastName());
        else if(tenant == null)
            mail.setText(EMAIL_START + business.getName() + ",\n\nWe need your offer for the newest damage we've got in our building:\n\n" +
                    "To get more details and to send the offer, please visit the following link: " + URL +"damage-requests\n\nBest regards,\n"+
                    institution.getName());
        javaMailSender.send(mail);
    }

    @Async
    public void sendEmailOfferForDamage(Offer offer, Damage damage, Tenant tenant) throws MailException, InterruptedException {

        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(tenant.getEmail());
        mail.setFrom(env.getProperty(EMAIL_USERNAME));
        mail.setSubject(EMAIL_START + tenant.getFirstName() + " " + tenant.getLastName());
        mail.setText(EMAIL_START + tenant.getFirstName() + " " + tenant.getLastName() +
                ",\n\nWe have sen't you an offer. You can find the price we've sent you on the following link: " + URL + "damages"
        + "\n\nBest Regards\n" + offer.getBusiness().getName());
        javaMailSender.send(mail);
    }

    @Async
    public void sendEmailAcceptingOffer(Offer offer, Damage damage) throws MailException, InterruptedException {

        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(offer.getBusiness().getEmail());
        mail.setFrom(env.getProperty(EMAIL_USERNAME));
        mail.setSubject(EMAIL_START + offer.getBusiness().getName());
        mail.setText(EMAIL_START + offer.getBusiness().getName() +
                ", your offer for fixing our damage has been accepted!\nWe hope you will come to fix it as soon as you can.\n\n" +
                "Best regards\n" + (damage.getResponsiblePerson() == null ? damage.getResponsibleInstitution().getName() :
                                    damage.getResponsiblePerson().getFirstName() + "  " + damage.getResponsiblePerson().getLastName()));
        javaMailSender.send(mail);
    }

    @Async
    public void sendEmailDeclareDamageFixed(Offer offer, Damage damage) throws MailException, InterruptedException {

        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(damage.getResponsiblePerson().getEmail());
        mail.setFrom(env.getProperty(EMAIL_USERNAME));
        mail.setSubject(EMAIL_START + damage.getResponsiblePerson().getFirstName() + " " + damage.getResponsiblePerson().getLastName());
        mail.setText(EMAIL_START + damage.getResponsiblePerson().getFirstName() + " " + damage.getResponsiblePerson().getLastName() +
                ",\n\nDamage you had has been fixed!\n\nDamage Description: " + damage.getDescription()
                + "\nDate created: " + damage.getDate().toString() + "\nAddress: " +
                damage.getBuilding().getAddress().getStreet() + " " + damage.getBuilding().getAddress().getNumber() +
                (offer != null ? "\nPrice for this damage fix: "  + offer.getPrice() + "\nPlease pay this as soon as you can" : "" ) +".\n\nBest regards,\n\n"
                + damage.getSelectedBusiness().getName());
        javaMailSender.send(mail);
    }
}
