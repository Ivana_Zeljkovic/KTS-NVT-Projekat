package rs.ac.uns.ftn.ktsnvt.repository.unit;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.model.entity.Building;
import rs.ac.uns.ftn.ktsnvt.model.entity.Council;
import rs.ac.uns.ftn.ktsnvt.model.entity.Meeting;
import rs.ac.uns.ftn.ktsnvt.repository.MeetingRepository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Ivana Zeljkovic on 07/12/2017.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class MeetingRepositoryUnitTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private MeetingRepository meetingRepository;

    private Long meeting_1_id;
    private Long meeting_2_id;
    private Long council_id;
    private Long building_id;


    @Before
    public void setUp() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Meeting meeting_1 = new Meeting();
        Meeting meeting_2 = new Meeting();
        meeting_1.setDuration(60);
        meeting_2.setDuration(60);
        try {
            meeting_1.setDate(sdf.parse("2018-12-30 12:00:00"));
            meeting_2.setDate(sdf.parse("2018-12-30 10:00:00"));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Building building = new Building();
        Council council = new Council();

        this.meeting_1_id = (Long)entityManager.persistAndGetId(meeting_1);
        this.meeting_2_id = (Long)entityManager.persistAndGetId(meeting_2);

        council.getMeetings().add(meeting_1);
        council.getMeetings().add(meeting_2);
        this.council_id = (Long)entityManager.persistAndGetId(council);

        building.setCouncil(council);
        this.building_id = (Long)entityManager.persistAndGetId(building);
        entityManager.merge(council);

        meeting_1.setCouncil(council);
        meeting_2.setCouncil(council);
        entityManager.merge(meeting_1);
        entityManager.merge(meeting_2);
    }

    @Test
    @Transactional
    public void testFindFirstNextMeetingInBuilding() {
        /*
        * Test proverava ispravnost rada metode findFirstNextMeetingInBuilding repozitorijuma MeetingRepository
        * (dobavljanje prvog sledeceg sastanka na nivou zgrade od cije je skupstine id zadat)
        * */

        /*
        prvi sledeci sastanak za zgradu sa ID-em = building_id (skupstinu stanara sa ID-em = council_id) je sastanak:
            id = meeting_id_2,
            duration = 60
            council_id = council_id
        */
        Meeting meeting = this.meetingRepository.findFirstNextMeetingInBuilding(this.council_id, new Date());

        assertNotNull(meeting);
        assertEquals(this.meeting_2_id, meeting.getId());
        assertEquals(60, meeting.getDuration());
        assertEquals(this.council_id, meeting.getCouncil().getId());
    }

    @Test
    @Transactional
    public void testFindAllByBuildingId() {
        /*
        * Test proverava ispravnost rada metode findAllByBuildingId repozitorijuma MeetingRepository
        * (dobavljanje liste svih sastanaka koji ce se odrzati u buducnosti na nivou zgrade sa zadatim ID-em)
        * */

        /*
        zgrada sa ID-em = building_id ima 2 sastanka koji ce se odrzati u buducnosti:
            1 - id = meeting_1_id,
                duration = 60,
                date = 2018-12-30 12:00:00
            2 - id = meeting_2_id,
                duration = 60,
                date = 2018-12-30 10:00:00
        */
        Page<Meeting> meetingList = this.meetingRepository.findAllByBuildingId(this.building_id, new Date(),
                new PageRequest(0, 2, Sort.Direction.ASC, "id"));

        assertEquals(2, meetingList.getContent().size());
        assertEquals(this.meeting_1_id, meetingList.getContent().get(0).getId());
        assertEquals(60, meetingList.getContent().get(0).getDuration());
        assertEquals(this.meeting_2_id, meetingList.getContent().get(1).getId());
        assertEquals(60, meetingList.getContent().get(1).getDuration());
    }
}
