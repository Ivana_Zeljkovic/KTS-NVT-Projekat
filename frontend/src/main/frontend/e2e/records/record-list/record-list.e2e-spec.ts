import { browser } from "protractor";
// page
import { LoginPage } from "../../page-objects/login.po";
import { MeetingListPage } from "../../page-objects/meeting-list.po";
import { RecordListPage } from "../page-objects/record-list.po";
import { RecordPage } from "../page-objects/record.po";
import { RecordCreatePage } from "../../page-objects/record-create.po";


describe('Record list', () => {
  let loginPage: LoginPage;
  let meetingListPage: MeetingListPage;
  let recordListPage: RecordListPage;
  let recordPage: RecordPage;
  let recordCreatePage: RecordCreatePage;
  let existingMeeting: any;
  let numberOfFinishedMeetings: any;
  let indexOfRecord: any;


  beforeAll(() => {
    loginPage = new LoginPage();
    meetingListPage = new MeetingListPage();
    recordListPage = new RecordListPage();
    recordPage = new RecordPage();
    recordCreatePage = new RecordCreatePage();
    existingMeeting = {'date': '', 'recordContent': 'Some content', 'numberOfMeetingItems':''};
    numberOfFinishedMeetings = {'value':0};
    indexOfRecord = {'value': -1};


    browser.get('/');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');

    loginPage.setUsername('kaca');
    loginPage.setPassword('kaca');
    loginPage.getSignInButton().click();

    browser.wait(function () {
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/home/notifications';
      });
    }).then(function () {
      browser.get('http://localhost:49152/#/meetings');

      browser.wait(function () {
        return meetingListPage.getMeetingListElement().isDisplayed();
      });
    });
  });

  afterAll(() => {
    browser.executeScript('window.localStorage.clear();');
  });


  it('should create record for existing meeting that is finished and show that record in record list page', () => {
    meetingListPage.getFinishedMeetingsList().isPresent().then(isPresent => {
      if(isPresent) {
        meetingListPage.getFinishedMeetingsTableRows().count().then(value => {
          numberOfFinishedMeetings.value = value;

          if (numberOfFinishedMeetings.value > 0) {
            meetingListPage.getFinishedMeetingDate(0).getText().then(date => {
              existingMeeting.date = date;
            });
            meetingListPage.getFinishedMeetingNumberOfMeetingItems(0).getText().then(numberOfMeetingItems => {
              existingMeeting.numberOfMeetingItems = numberOfMeetingItems;
            });
            meetingListPage.getFinishedMeetingMenu(0).click();

            browser.wait(function () {
              return meetingListPage.getContentOfFinishedMeetingMenu(0).isDisplayed();
            }).then(function () {
              expect(meetingListPage.getCreateRecordOptionFromFinishedMeetingMenu(0).isDisplayed()).toBe(true);
              meetingListPage.getCreateRecordOptionFromFinishedMeetingMenu(0).click();

              browser.wait(function () {
                return recordCreatePage.getRecordCreateElement().isDisplayed();
              }).then(function () {
                recordCreatePage.setContent(existingMeeting.recordContent);
                recordCreatePage.getTitle().click();
                recordCreatePage.getSaveButton().click();

                browser.get('http://localhost:49152/#/records');

                browser.wait(function() {
                  return recordListPage.getRecordListElement().isDisplayed();
                }).then(function() {
                  expect(recordListPage.getTitle().isDisplayed()).toBe(true);
                  recordListPage.getRecords().count().then(numberOfRecords => {
                    // there must be at least one record
                    expect(numberOfRecords).toBeGreaterThanOrEqual(1);

                    // iterate to find date of meeting with date from existingMeeting
                    // every record should have meeting date, content and menu with view Details option
                    for(let i:number = 0; i < numberOfRecords; i++) {
                      expect(recordListPage.getContentFromRecord(i).isDisplayed()).toBe(true);
                      expect(recordListPage.getMeetingDateFromRecord(i).isDisplayed()).toBe(true);
                      if(indexOfRecord.value === -1) {
                        recordListPage.getMeetingDateFromRecord(i).getText().then(date => {
                          if (date === existingMeeting.date)
                            indexOfRecord.value = i;
                        });
                      }
                      expect(recordListPage.getMenuFromRecord(i).isDisplayed()).toBe(true);
                      recordListPage.getMenuFromRecord(i).click();

                      browser.wait(function() {
                        return recordListPage.getContentOfMenuFromRecord(i).isDisplayed();
                      }).then(function() {
                        expect(recordListPage.getDetailsOptionFromMenu(i).isDisplayed()).toBe(true);
                        recordListPage.getMenuFromRecord(i).click();
                      });
                    }
                  });
                });
              });
            });
          }
        });
      }
    });
  });



  it('should open details page for existing record, if this record is found on first page in ' +
    'previous test', () => {
    if(numberOfFinishedMeetings.value >= 1 && indexOfRecord.value !== -1) {
      expect(recordListPage.getMenuFromRecord(indexOfRecord.value).isDisplayed()).toBe(true);
      recordListPage.getMenuFromRecord(indexOfRecord.value).click();

      browser.wait(function() {
        return recordListPage.getContentOfMenuFromRecord(indexOfRecord.value).isDisplayed();
      }).then(function() {
        expect(recordListPage.getDetailsOptionFromMenu(indexOfRecord.value).isDisplayed()).toBe(true);
        recordListPage.getDetailsOptionFromMenu(indexOfRecord.value).click();

        browser.wait(function() {
          return recordPage.getRecordElement().isDisplayed();
        }).then(function() {
          expect(recordPage.getTitle().isDisplayed()).toBe(true);
          expect(recordPage.getDateOfMeeting().isDisplayed()).toBe(true);
          expect(recordPage.getDateOfMeeting().getText()).toEqual(`Content of record for meeting in: ${existingMeeting.date}`);
          expect(recordPage.getRecordContent().isDisplayed()).toBe(true);
          expect(recordPage.getRecordContent().getText()).toEqual(existingMeeting.recordContent);
          if(existingMeeting.numberOfMeetingItems > 0) {
            recordPage.getMeetingItems().count().then(numberOfMeetingItems => {
              expect(numberOfMeetingItems).toEqual(existingMeeting.numberOfMeetingItems);
              for (let i: number = 0; i < numberOfMeetingItems; i++) {
                expect(recordPage.getMeetingItemTitle(i).isDisplayed()).toBe(true);
                expect(recordPage.getMeetingItemDate(i).isDisplayed()).toBe(true);
                expect(recordPage.getMeetingItemCreator(i).isDisplayed()).toBe(true);
                expect(recordPage.getMeetingItemContent(i).isDisplayed()).toBe(true);
              }
            });
          }
          else {
            expect(recordPage.getNoMeetingItemsMessage().isDisplayed()).toBe(true);
          }
        });
      });
    }
  });
});
