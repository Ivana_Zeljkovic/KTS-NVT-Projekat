import { Component, OnInit } from '@angular/core';
import { ToasterConfig, ToasterService } from "angular5-toaster/dist";
import { Router } from "@angular/router";
import { BsModalService } from "ngx-bootstrap";
// model
import { Meeting } from "../../shared/models/meeting";
import { PageAndSort } from "../../shared/models/page-and-sort";
import { RecordCreate } from "../../shared/models/record-create";
// service
import { MeetingService } from "../../core/services/meeting.service";
import { JwtService } from "../../core/services/jwt.service";
// error
import { AppError } from "../../shared/errors/app-error";
import { ForbiddenError } from "../../shared/errors/forbidden-error";
import { NotFoundError } from "../../shared/errors/not-found-error";
import { BadRequestError } from "../../shared/errors/bad-request-error";
// component
import { ConfirmDeleteModalComponent } from "../../shared/components/confirm-delete-modal/confirm-delete-modal.component";
import { MeetingAddComponent } from "../meeting-add/meeting-add.component";
import { RecordCreateComponent } from "../record-create/record-create.component";


@Component({
  selector: 'app-meeting-list',
  templateUrl: './meeting-list.component.html',
  styleUrls: ['./meeting-list.component.css']
})
export class MeetingListComponent implements OnInit {
  meetingList: Array<Meeting>;
  meetingWithoutRecordList: Array<Meeting>;
  toasterConfig: ToasterConfig;

  // Elements for bootstrap paging
  currentPage: number;
  currentPage2: number;
  totalItems: number;
  totalItems2: number;
  maxSize: number;
  itemsPerPage: number;

  // Elements for getting data from server
  pageSize: string;
  sortDirection: string;
  sortProperties: string[];


  constructor(private meetingService: MeetingService, private jwtService: JwtService,
              private toasterService: ToasterService, private router: Router,
              private modalService: BsModalService) {
    // toaster config
    this.toasterConfig = new ToasterConfig({
      timeout: {success: 2000, error: 4000, warning: 4000}
    });

    // Sett up for bootstrap paging
    this.currentPage = 1;
    this.currentPage2 = 1;
    this.maxSize = 3;
    this.itemsPerPage = 15;

    // Sett up for getting damages request
    this.pageSize = '15';
    this.sortDirection = 'ASC';
    this.sortProperties = ['date'];
  }

  ngOnInit() {
    this.loadMeetingList();
    if(this.jwtService.hasRole('COUNCIL_PRESIDENT')) {
      this.itemsPerPage = 7;
      this.pageSize = "7";
      this.loadFinishedMeetingList();
    }
  }

  add() {
    const modalRef = this.modalService.show(MeetingAddComponent);

    modalRef.content.meetingCreated.subscribe(meetingCreated => {
      if(meetingCreated) {
        this.toasterService.pop('success', 'Good job', 'Meeting sucessfully created!');
        this.currentPage = 1;
        this.loadMeetingList();
      }
    })
  }

  details(meeting: Meeting) {
    this.router.navigate(['meetings', meeting.id]);
  }

  remove(meeting: Meeting) {
    const modalRef = this.modalService.show(
      ConfirmDeleteModalComponent,
      Object.assign({},{ class: 'modal-sm' })
    );

    let numberOfPages = Math.ceil(meeting.numberOfElements / +(this.pageSize));
    let pageShouldDecrement = false;
    if(numberOfPages === this.currentPage && (meeting.numberOfElements - 1) % +(this.pageSize) === 0) {
      pageShouldDecrement = true;
    }

    modalRef.content.confirmed.subscribe((value) => {
      if(value) {
        this.meetingService.remove(meeting.id, 'buildings', this.jwtService.getBuildingIdFromToken(), 'council/meetings')
          .subscribe(() => {
            if(pageShouldDecrement && this.currentPage !== 1)
              this.currentPage--;
            this.loadMeetingList();
          }, (error: AppError) => {
            if(error instanceof BadRequestError)
              this.toasterService.pop('error', 'Error',
                'Selected meeting-details doesn\'t belong to selected building!');
            else if(error instanceof ForbiddenError)
              this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
            else if(error instanceof NotFoundError)
              this.toasterService.pop('error', 'Error', 'Meeting not found!');
            else {
              this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
              throw error;
            }
          });
      }
    });
  }

  createRecord(meeting: Meeting) {
    const modalRef = this.modalService.show(RecordCreateComponent);
    modalRef.content.model = new RecordCreate('');
    modalRef.content.meetingID = meeting.id;

    modalRef.content.submitted.subscribe(recordCreated => {
      if(recordCreated)
        this.toasterService.pop('success', 'God job', 'Record successfully created!');
      this.loadFinishedMeetingList();
    });
  }

  pageChanged(event: any) {
    this.currentPage = event.page;
    this.loadMeetingList();
  }

  page2Changed(event: any) {
    this.currentPage2 = event.page;
    this.loadFinishedMeetingList();
  }

  private loadMeetingList() {
    let page = new PageAndSort((this.currentPage - 1).toString(), this.pageSize, this.sortDirection, this.sortProperties);

    this.meetingService.getPage(page, 'buildings', this.jwtService.getBuildingIdFromToken(), 'council/meetings')
      .subscribe(meetings => {
        if (meetings.length > 0)
          this.totalItems = meetings[0].numberOfElements;
        else this.totalItems = 0;
        this.meetingList = meetings;
      }, (error: AppError) => {
        this.handleErrors(error);
      });
  }

  private loadFinishedMeetingList() {
    let page = new PageAndSort((this.currentPage2 - 1).toString(), this.pageSize, this.sortDirection, this.sortProperties);

    this.meetingService.getPage(page, 'buildings', this.jwtService.getBuildingIdFromToken(), 'council/finished_meetings')
      .subscribe(meetingsWithoutRecords => {
        if (meetingsWithoutRecords.length > 0)
          this.totalItems2 = meetingsWithoutRecords[0].numberOfElements;
        else this.totalItems2 = 0;
        this.meetingWithoutRecordList = meetingsWithoutRecords;
      }, (error: AppError) => {
        this.handleErrors(error);
      });
  }

  private handleErrors(error: AppError) {
    if (error instanceof ForbiddenError)
      this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
    else if (error instanceof NotFoundError)
      this.toasterService.pop('error', 'Error', 'Building not found!');
    else {
      this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
      throw error;
    }
  }
}
