export interface Record {
  id: number;
  content: string;
  meetingDate: Date;
  meetingId: number;
  numberOfElements?: number;
}
