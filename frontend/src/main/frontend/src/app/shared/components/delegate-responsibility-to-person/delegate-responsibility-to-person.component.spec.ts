import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DelegateResponsibilityToPersonComponent } from './delegate-responsibility-to-person.component';

describe('DelegateResponsibilityToPersonComponent', () => {
  let component: DelegateResponsibilityToPersonComponent;
  let fixture: ComponentFixture<DelegateResponsibilityToPersonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DelegateResponsibilityToPersonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DelegateResponsibilityToPersonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
