import {by, element, ElementArrayFinder, ElementFinder} from "protractor";

export class QuestionnairePage {
  // get methods
  getQuestionnaireElement(): ElementFinder {
    return element(by.tagName('app-questionnaire'));
  }

  getTitleFromHeader(): ElementFinder {
    return element(by.css('h4.modal-title'));
  }

  getQuestionsPanel(): ElementFinder {
    return element(by.id('questionsPanel'));
  }

  getLinkForCreateQuestion(): ElementFinder {
    return element(by.css('div.question-links > div > a'));
  }

  getAllQuestions(): ElementArrayFinder {
    return element.all(by.css('#questionsPanel > ul > li.list-group-item'));
  }

  getQuestionContent(index: number): ElementFinder {
    return element(by.css(`#questionsPanel > ul > li.list-group-item:nth-child(${index+1}) > p:first-of-type`))
  }

  getAnswersFromQuestion(index: number): ElementArrayFinder {
    return element.all(by.css('#questionsPanel > ul > li.list-group-item > div > ul > li'));
  }

  getAnswerFromQuestion(index: number, index2: number): ElementFinder {
    return element(by.css(`#questionsPanel > ul > li.list-group-item:nth-child(${index+1}) > div > ul > li:nth-child(${index2+1})`));
  }

  getQuestionRemoveLink(index: number): ElementFinder {
    return element(by.css(`#questionsPanel > ul > li.list-group-item:nth-child(${index+1}) > span:first-of-type`))
  }

  getSaveButton(): ElementFinder {
    return element(by.id('saveButton'));
  }

  getResetButton(): ElementFinder {
    return element(by.id('resetButton'));
  }

  getCancelButton(): ElementFinder {
    return element(by.id('cancelButton'));
  }
}
