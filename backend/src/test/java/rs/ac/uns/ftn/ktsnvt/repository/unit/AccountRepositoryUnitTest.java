package rs.ac.uns.ftn.ktsnvt.repository.unit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.model.entity.Account;
import rs.ac.uns.ftn.ktsnvt.repository.AccountRepository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by Nikola Garabandic on 30/11/2017.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class AccountRepositoryUnitTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private AccountRepository accountRepository;


    @Before
    public void setUp(){
        //ACCOUNT
        Account account = new Account("kaca", "kaca");
        entityManager.persist(account);
    }

    @Test
    public void testFindByUsername(){
        /*
         * Test proverava ispravnost rada metode findByUsername repozitorijuma AccountRepository
         * (pronalazenje naloga na osnovu korisnickog imena)
         * */

        /*
        postoji samo jedan stanar koji zadovoljava pretragu sa parametrom:
            korisnicko ime = kaca
         */
        Account account = this.accountRepository.findByUsername("kaca");

        assertNotNull(account);
        assertEquals("kaca", account.getUsername());
    }

    @Test
    public void testNotFoundByUsername(){
        /*
         * Test proverava ispravnost rada metode findByUsername repozitorijuma AccountRepository
         * (pronalazenje naloga na osnovu korisnickog imena)
         * */

        // ne postoji nijedan stanar koji ima korisnicko ime = kca
        Account account = this.accountRepository.findByUsername("kca");

        assertNull(account);
    }

}
