import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
  name: 'damage_type_format'
})
export class DamageTypeFormatPipe implements PipeTransform {
  transform(value: any, args?: any[]) {
    if(!value)
      return null;

    let str = value.toString();
    str = str.toLowerCase().split('_')[0];
    str = str[0].toUpperCase() + str.slice(1);
    return str;
  }
}
