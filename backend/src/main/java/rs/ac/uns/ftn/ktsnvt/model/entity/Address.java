package rs.ac.uns.ftn.ktsnvt.model.entity;

import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@Table(name = "address")
@Where(clause="deleted=0")
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Column(nullable = false)
    private String street;

    @Column(nullable = false)
    private int number;

    @Column(nullable = false, columnDefinition = "INTEGER DEFAULT 0")
    @Version
    private int version;

    @Column(nullable = false, columnDefinition = "BOOL DEFAULT FALSE")
    private boolean deleted;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private City city;

    public Address() { }

    public Address(String street, int number) {
        this.street = street;
        this.number = number;
    }

    public Address(String street, int number, City city) {
        this.street = street;
        this.number = number;
        this.city = city;
    }

    public Long getId() { return id; }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) { this.city = city; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Address address = (Address) o;

        if (number != address.number) return false;
        if (version != address.version) return false;
        if (deleted != address.deleted) return false;
        if (id != null ? !id.equals(address.id) : address.id != null) return false;
        if (street != null ? !street.equals(address.street) : address.street != null) return false;
        return city != null ? city.equals(address.city) : address.city == null;
    }
}
