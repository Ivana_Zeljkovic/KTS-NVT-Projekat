import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
import { Injectable } from "@angular/core";
import { JwtService } from "../services/jwt.service";

@Injectable()
export class BusinessGuard implements CanActivate {

  constructor(private router: Router, private jwtService: JwtService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let roles: Array<string> = this.jwtService.getRolesFromToken();
    if(roles.indexOf('COMPANY') != -1 || roles.indexOf('INSTITUTION') != -1) return true;

    // logged user hasn't role COMPANY or INSTITUTION
    this.router.navigate(['home']);
    return false;
  }
}
