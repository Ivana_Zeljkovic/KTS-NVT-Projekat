import { browser } from "protractor";
// page
import { DamageForQuestionnairePage } from "../page-objects/damage-for-questionnaire.po";
import { MeetingItemListPage } from "../page-objects/meeting-item-list.po";
import { MeetingItemPage } from "../../../page-objects/meeting-item.po";
import { LoginPage } from "../../../page-objects/login.po";


describe('Choosing damage that is connected with meeting item which is in creating process', () => {
  let damageForQuestionnairePage: DamageForQuestionnairePage;
  let meetingItemListPage: MeetingItemListPage;
  let meetingItemPage: MeetingItemPage;
  let loginPage: LoginPage;

  beforeAll(() => {
    loginPage = new LoginPage();
    meetingItemPage = new MeetingItemPage();
    meetingItemListPage = new MeetingItemListPage();
    damageForQuestionnairePage = new DamageForQuestionnairePage();

    browser.get('/');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');
    loginPage.setUsername('kaca');
    loginPage.setPassword('kaca');
    loginPage.getSignInButton().click();

    browser.wait(function() {
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/home/notifications';
      });
    }).then(function() {
      browser.get('http://localhost:49152/#/home/meeting-details-items');
      expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/home/meeting-details-items');
      expect(meetingItemListPage.getMeetingItemListElement().isDisplayed()).toBe(true);
    });
  });

  afterAll(() => {
    browser.executeScript('window.localStorage.clear();');
  });


  it('should check are all elements of damage for questionnaire page displayed when ' +
    'user click on Choose damage option in meeting item page', () => {
    expect(meetingItemListPage.getAddButton().isDisplayed()).toBe(true);
    meetingItemListPage.getAddButton().click();

    browser.wait(function() {
      // wait for meeting item page is loaded
      return meetingItemPage.getMeetingItemElement().isDisplayed();
    }).then(function() {
      expect(meetingItemPage.getChooseDamageLink().isDisplayed()).toBe(true);
      meetingItemPage.getChooseDamageLink().click();

      browser.wait(function() {
        // wait for damage for questionnaire page is loaded
        return damageForQuestionnairePage.getDamageForQuestionnaireElement().isDisplayed();
      }).then(function() {
        expect(damageForQuestionnairePage.getTitleFromHeader().isDisplayed()).toBe(true);
        expect(damageForQuestionnairePage.getTitleFromHeader().getText()).toEqual('Damages you are responsible for');
        expect(damageForQuestionnairePage.getCloseButtonFromHeader().isDisplayed()).toBe(true);

        if(damageForQuestionnairePage.getDamageList().isPresent()) {
          // expect to see damage collection with pagination, where every damage is present as panel
          // with few elements
          expect(damageForQuestionnairePage.getPaginatorInDamageList().isDisplayed()).toBe(true);
          damageForQuestionnairePage.getPanelsFromDamageList().count().then(value => {
            for(let i: number = 0; i<value; i++) {
              expect(damageForQuestionnairePage.getCreatorNameFromDamagePanel(i).isDisplayed()).toBe(true);
              expect(damageForQuestionnairePage.getDateFromDamagePanel(i).isDisplayed()).toBe(true);
              expect(damageForQuestionnairePage.getDamageTypeFromDamagePanel(i).isDisplayed()).toBe(true);
              expect(damageForQuestionnairePage.getDamageDescriptionFromDamagePanel(i).isDisplayed()).toBe(true);
              expect(damageForQuestionnairePage.getMenuFromDamagePanel(i).isDisplayed()).toBe(true);
              damageForQuestionnairePage.getMenuFromDamagePanel(i).click();
              expect(damageForQuestionnairePage.getChooseOptionFromDamagePanel(i).isDisplayed()).toBe(true);
            }
          });
        }
        else {
          // expect to there is only message that indicates to user there is no element in list
          expect(damageForQuestionnairePage.getMessageNoDamageInList().isDisplayed()).toBe(true);
          expect(damageForQuestionnairePage.getMessageNoDamageInList().getText()).toEqual('There is no damages ' +
            'you are responsible person for...');
        }
      });
    });
  });



  it('should close modal for choosing damage when user click on Close button', () => {
    expect(damageForQuestionnairePage.getCloseButtonFromHeader().isDisplayed()).toBe(true);
    damageForQuestionnairePage.getCloseButtonFromHeader().click();

    //to be sure that modal was closed after click, we click again on option for opening modal
    expect(meetingItemPage.getChooseDamageLink().isDisplayed()).toBe(true);
    meetingItemPage.getChooseDamageLink().click();

    browser.wait(function() {
      // wait for damage for questionnaire page is loaded
      return damageForQuestionnairePage.getDamageForQuestionnaireElement().isDisplayed();
    });
  });
});
