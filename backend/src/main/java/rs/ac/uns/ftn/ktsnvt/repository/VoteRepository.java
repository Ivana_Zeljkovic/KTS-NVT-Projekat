package rs.ac.uns.ftn.ktsnvt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import rs.ac.uns.ftn.ktsnvt.model.entity.Vote;

/**
 * Created by Ivana Zeljkovic on 19/11/2017.
 */
public interface VoteRepository extends JpaRepository<Vote, Long> {

    @Query(value = "SELECT COUNT(v) FROM Vote v WHERE v.questionnaire.id = :questionnaireId AND v.tenant.id = :tenantId")
    int countPersonalVotes(@Param("tenantId") Long tenantId, @Param("questionnaireId") Long questionnaireId);

}
