package rs.ac.uns.ftn.ktsnvt.repository.unit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.model.entity.*;
import rs.ac.uns.ftn.ktsnvt.model.enumeration.BusinessType;
import rs.ac.uns.ftn.ktsnvt.repository.OfferRepository;

import java.awt.print.Pageable;
import java.util.ArrayList;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@DataJpaTest
public class OfferRepositoryUnitTest {

    Pageable pageable;

    @Autowired
    private TestEntityManager entityManager;


    @Autowired
    private OfferRepository offerRepository;


    private Long damage_id;

    @Before
    public void setUp(){

        Offer offer = new Offer(false, 200);
        Business business = new Business("Test", "Test", "12512", "skfaf@gmail.com",
                BusinessType.FIRE_SERVICE, true);
        DamageType damageType = new DamageType("TEST");
        Account account = new Account("kaca", "kaca");
        Tenant tenant = new Tenant("Testic", "Testic", new Date(), "sfiasf@gmail.com", "531531513", true, account);
        Damage damage = new Damage("Opis", new Date(), damageType, false, tenant);

        offer.setBusiness(business);
        damage.getOffers().add(offer);
        damageType.getDamages().add(damage);

        Offer offer2 = new Offer(false, 200);
        Business business2 = new Business("Test", "Test", "12512", "skfaf@gmail.com",
                BusinessType.FIRE_SERVICE, true);

        offer2.setBusiness(business2);
        damage.getOffers().add(offer2);

        offer2.setDamage(damage);
        offer.setDamage(damage);

        entityManager.persist(damageType);
        entityManager.persist(account);
        entityManager.persist(tenant);
        entityManager.persist(offer);
        entityManager.persist(business2);
        entityManager.persist(offer2);
        entityManager.persist(business);
        damage_id = entityManager.persistAndGetId(damage, Long.class);

    }

    @Test
    public void testFindAllForCurrentDamage(){
        /*
        Test proverava rad ispravnosti metode findaAllForCurrentDamage repozitorijuma OfferRepository.
        (dobavljanje svih ponuda za kvar ciji id se prosledjuje)
        kao sto se vidi u metodi setUp za kvar sa ID-em damage_id ima tacno 2 elementa.
         */

        Page<Offer> offers = this.offerRepository.findAllForCurrentDamage(damage_id, new PageRequest(0, 3, Sort.Direction.ASC, "id"));

        assertEquals(2L, offers.getTotalElements());
    }


    @Test
    public void testFindNoneForCurrentDamage(){
        /*
        Test proverava rad ispravnosti metode findaAllForCurrentDamage repozitorijuma OfferRepository.
        (dobavljanje svih ponuda za trenutni kvar)
        kvar sa id-em 489L ne postoji
         */

        Page<Offer> offers = this.offerRepository.findAllForCurrentDamage(489L, new PageRequest(0, 3, Sort.Direction.ASC, "id"));

        assertEquals(0L, offers.getTotalElements());
    }


}
