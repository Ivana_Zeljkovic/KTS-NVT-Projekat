import { Address } from "../../../shared/models/address";

export interface TenantProfile {
  id: number;
  firstName: string;
  lastName: string;
  birthDate: Date;
  email: string;
  phoneNumber: string;
  address?: Address;
  apartmentNumber?: number;
  apartmentID?: number;
  buildingID?: number;
}
