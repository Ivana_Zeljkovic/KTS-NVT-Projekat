package rs.ac.uns.ftn.ktsnvt.model.entity;

import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "vote")
@Where(clause="deleted=0")
public class Vote {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @OneToOne
    private Tenant tenant;

    @Column(nullable = false, columnDefinition = "INTEGER DEFAULT 0")
    @Version
    private int version;

    @Column(nullable = false, columnDefinition = "BOOL DEFAULT FALSE")
    private boolean deleted;

    @OneToMany(mappedBy = "vote", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<QuestionVote> questionsVotes;

    @ManyToOne
    private Questionnaire questionnaire;

    public Vote() {
        this.questionsVotes = new ArrayList<>();
    }

    public Vote(Tenant tenant) {
        this.tenant = tenant;
        this.questionsVotes = new ArrayList<>();
    }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public Tenant getTenant() { return tenant; }

    public void setTenant(Tenant tenant) { this.tenant = tenant; }

    public int getVersion() { return version; }

    public void setVersion(int version) { this.version = version; }

    public boolean isDeleted() { return deleted; }

    public void setDeleted(boolean deleted) { this.deleted = deleted; }

    public List<QuestionVote> getQuestionsVotes() { return questionsVotes; }

    public void setQuestionsVotes(List<QuestionVote> questionsVotes) { this.questionsVotes = questionsVotes; }

    public Questionnaire getQuestionnaire() { return questionnaire; }

    public void setQuestionnaire(Questionnaire questionnaire) { this.questionnaire = questionnaire; }
}
