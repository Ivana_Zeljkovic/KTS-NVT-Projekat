package rs.ac.uns.ftn.ktsnvt.service.unit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Account;
import rs.ac.uns.ftn.ktsnvt.model.entity.Apartment;
import rs.ac.uns.ftn.ktsnvt.model.entity.Building;
import rs.ac.uns.ftn.ktsnvt.model.entity.Tenant;
import rs.ac.uns.ftn.ktsnvt.repository.BuildingRepository;
import rs.ac.uns.ftn.ktsnvt.repository.TenantRepository;
import rs.ac.uns.ftn.ktsnvt.service.BuildingService;
import rs.ac.uns.ftn.ktsnvt.service.TenantService;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;

/**
 * Created by Ivana Zeljkovic on 01/12/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TenantServiceUnitTest {

    @Autowired
    private TenantService tenantService;

    @Autowired
    private BuildingService buildingService;

    @MockBean
    private TenantRepository tenantRepository;

    @MockBean
    private BuildingRepository buildingRepository;


    @Before
    public void setUp() {
        // stanar
        Account account = new Account("ivana", "ivana");

        Tenant tenant = new Tenant();
        tenant.setId(new Long(100));
        tenant.setFirstName("Ivana");
        tenant.setLastName("Zeljkovic");
        tenant.setAccount(account);
        account.setTenant(tenant);

        // stanovi u prvoj zgradi
        Apartment apartment_1 = new Apartment(1, 1, 60);
        apartment_1.setId(new Long(101));
        apartment_1.setOwner(tenant);
        tenant.setApartmentLiveIn(apartment_1);
        tenant.getPersonalApartments().add(apartment_1);
        apartment_1.getTenants().add(tenant);
        Apartment apartment_2 = new Apartment(2,1,100);
        apartment_2.setId(new Long(102));
        apartment_2.setOwner(tenant);
        tenant.getPersonalApartments().add(apartment_2);

        // prva zgrada
        Building building_1 = new Building(5, true);
        building_1.setId(new Long(101));
        apartment_1.setBuilding(building_1);
        building_1.getApartments().add(apartment_1);
        apartment_2.setBuilding(building_1);
        building_1.getApartments().add(apartment_2);

        // stan u drugoj zgradi
        Apartment apartment_3 = new Apartment(1,1,70);
        apartment_3.setId(new Long(103));

        // druga zgrada
        Building building_2 = new Building(4, false);
        building_2.setId(new Long(102));
        apartment_3.setBuilding(building_2);
        building_2.getApartments().add(apartment_3);


        given(
                this.tenantRepository.findOne(new Long(100))
        ).willReturn(
                tenant
        );

        given(
                this.tenantRepository.findOne(new Long(101))
        ).willReturn(
                null
        );

        given(
                this.tenantRepository.findByNameAndUsername("Ivana", "Zeljkovic", "ivana")
        ).willReturn(
                tenant
        );

        given(
                this.tenantRepository.findByNameAndUsername("Ivana", "Zeljkovic", "kaca")
        ).willReturn(
                null
        );

        given(
                this.buildingRepository.findOne(new Long(101))
        ).willReturn(
                building_1
        );

        given(
                this.buildingRepository.findOne(new Long(102))
        ).willReturn(
                building_2
        );
    }

    @Test
    public void testFindOne() {
        /*
         * Test proverava ispravnost metode findOne servisa TenantService
         * (dobavljanje objekta stanara sa zadatim ID-em)
         * u slucaju kada postoji stanar sa zadatim ID-em
         * */

        /*
        postoji stanar sa ID-em = 100 i podacima:
            ime = Ivana,
            prezime = Zeljkovic,
            stan u kom zivi ima ID = 101
         */
        Tenant tenant = this.tenantService.findOne(new Long(100));

        assertNotNull(tenant);
        assertEquals(Long.valueOf(100), tenant.getId());
        assertEquals("Ivana", tenant.getFirstName());
        assertEquals("Zeljkovic", tenant.getLastName());
        assertEquals(Long.valueOf(101), tenant.getApartmentLiveIn().getId());
    }

    @Test(expected = NotFoundException.class)
    public void testFindOneNonexistent() {
        /*
         * Test proverava ispravnost metode findOne servisa TenantService
         * (dobavljanje objekta stanara sa zadatim ID-em)
         * u slucaju kada postoji ne stanar sa zadatim ID-em - ocekivano NotFoundException
         * */

        // ne postoji stanar sa ID-em = 200
        Tenant tenant = this.tenantService.findOne(new Long(200));
    }

    @Test
    public void testCheckPasswordUpdate() {
        /*
         * Test proverava ispravnost rada metode checkPasswordUpdate servisa TenantService
         * (provera prava izmene trenutne vrednosti lozinke)
         * u slucaju kada su vrednosti trenutne lozinke i nove lozinke ispravne (nisu null, trenutna vrednost lozinke
         * odgovara vrednosti lozinke koja je prezistenta)
         * */

        /*
        stanar sa ID-em = 100 ima sledece podatke:
            ime = Ivana,
            prezime = Zeljkovic,
            korisnicko ime = ivana,
            lozinka = ivana
         */
        Tenant tenant = this.tenantService.findOne(new Long(100));
        String current_password = "ivana";
        String new_password = "novi";

        boolean password_changed = this.tenantService.checkPasswordUpdate(tenant, current_password, new_password);

        assertTrue(password_changed);

        BCryptPasswordEncoder crypt = new BCryptPasswordEncoder();
        boolean password_matched = crypt.matches(new_password, tenant.getAccount().getPassword());

        assertTrue(password_matched);
    }

    @Test(expected = BadRequestException.class)
    public void testCheckPasswordUpdateMissingNewPass() {
        /*
         * Test proverava ispravnost rada metode checkPasswordUpdate servisa TenantService
         * (provera prava izmene trenutne vrednosti lozinke)
         * u slucaju kada je vrednost trenutne lozinke ispravna (nije null i odgovara vrednosti lozinke koja je prezistenta)
         * i nedostaje vrednost nove lozinke - ocekivano BadRequestException
         * */

        /*
        stanar sa ID-em = 100 ima sledece podatke:
            ime = Ivana,
            prezime = Zeljkovic,
            korisnicko ime = ivana,
            lozinka = ivana
         */
        Tenant tenant = this.tenantService.findOne(new Long(100));
        String current_password = "ivana";

        boolean password_changed = this.tenantService.checkPasswordUpdate(tenant, current_password, null);
    }

    @Test(expected = BadRequestException.class)
    public void testCheckPasswordUpdateMissingCurrentPass() {
        /*
         * Test proverava ispravnost rada metode checkPasswordUpdate servisa TenantService
         * (provera prava izmene trenutne vrednosti lozinke)
         * u slucaju kada je vrednost nove lozinke ispravna (nije null)
         * i nedostaje vrednost trenutne lozinke - ocekivano BadRequestException
         * */

        /*
        stanar sa ID-em = 100 ima sledece podatke:
            ime = Ivana,
            prezime = Zeljkovic,
            korisnicko ime = ivana,
            lozinka = ivana
         */
        Tenant tenant = this.tenantService.findOne(new Long(100));
        String new_password = "novi";

        boolean password_changed = this.tenantService.checkPasswordUpdate(tenant, null, new_password);
    }

    @Test(expected = BadRequestException.class)
    public void testCheckPasswordUpdateInvalidCurrentPass() {
        /*
         * Test proverava ispravnost rada metode checkPasswordUpdate servisa TenantService
         * (provera prava izmene trenutne vrednosti lozinke)
         * u slucaju kada je vrednost trenutne lozinke neispravna (nije null ali ne odgovara vrednosti lozinke koja je prezistenta)
         * i vrednost nove lozinke ispravna (nije null) - ocekivano BadRequestException
         * */

        /*
        stanar sa ID-em = 100 ima sledece podatke:
            ime = Ivana,
            prezime = Zeljkovic,
            username = ivana,
            password = ivana
         */
        Tenant tenant = this.tenantService.findOne(new Long(100));
        String current_password = "nevalidno";
        String new_password = "novi";

        boolean password_changed = this.tenantService.checkPasswordUpdate(tenant, current_password, new_password);
    }

    @Test
    public void testCheckTenantsPersonalApartments() {
        /*
         * Test proverava ispravnost rada metode checkTenantsPersonalApartments servisa TenantService
         * (dobavljanje indikatora da li zadati stanar poseduje bar jedan stan u zgradi sa zadatim ID-em)
         * u slucaju kada zadati stanar poseduje bar jedan stan u zgradi sa zadatim ID-em
         * */

        Tenant tenant = this.tenantService.findOne(new Long(100));

        // stanar sa ID-em = 100 poseduje 2 stana u zgradi sa ID-em = 101
        boolean has_apartments = this.tenantService.checkTenantsPersonalApartments(tenant, new Long(101));

        assertTrue(has_apartments);
    }

    @Test
    public void testCheckTenantsPersonalApartmentsNonexistent() {
        /*
         * Test proverava ispravnost rada metode checkTenantsPersonalApartments servisa TenantService
         * (dobavljanje indikatora da li zadati stanar poseduje bar jedan stan u zgradi sa zadatim ID-em)
         * u slucaju kada zadati stanar ne poseduje nijedan stan u zgradi sa zadatim ID-em
         * */

        Tenant tenant = this.tenantService.findOne(new Long(100));

        // stanar sa ID-em = 100 ne poseduje stanove u zgradi sa ID-em = 102
        boolean has_apartments = this.tenantService.checkTenantsPersonalApartments(tenant, new Long(102));

        assertFalse(has_apartments);
    }

    @Test
    public void testFindOneByFirstNameAndLastNameAndUsername() {
        /*
         * Test proverava ispravnost metode findOneByFirstNameAndLastNameAndUsername servisa TenantService
         * (dobavljanje objekta stanara na osnovu zadatog imena, prezimena i korisnickog imena)
         * u slucaju kada postoji stanar kom odgovara zadato ime, prezime i korisnicko ime
         * */

        /*
        postoji stanar sa ID-em = 100 ima sledece podatke:
            ime = Ivana,
            prezime = Zeljkovic,
            korisnicko ime = ivana
         */
        Tenant tenant = this.tenantService.findOneByFirstNameAndLastNameAndUsername("Ivana", "Zeljkovic", "ivana");

        assertNotNull(tenant);
        assertEquals(Long.valueOf(100), tenant.getId());
        assertEquals("Ivana", tenant.getFirstName());
        assertEquals("Zeljkovic", tenant.getLastName());
        assertEquals("ivana", tenant.getAccount().getUsername());
    }

    @Test(expected = NotFoundException.class)
    public void testFindOneByFirstNameAndLastNameAndUsernameNonexistent() {
        /*
         * Test proverava ispravnost metode findOneByFirstNameAndLastNameAndUsername servisa TenantService
         * (dobavljanje objekta stanara na osnovu zadatog imena, prezimena i korisnickog imena)
         * u slucaju kada ne postoji stanar kom odgovara zadato ime, prezime i korisnicko ime - ocekivano NotFoundException
         * */

        /*
        ne postoji stanar koji ima sledece podatke:
            ime = Ivana,
            prezime = Zeljkovic,
            korisnicko ime = kaca
         */
        Tenant tenant = this.tenantService.findOneByFirstNameAndLastNameAndUsername("Ivana", "Zeljkovic", "kaca");
    }

    @Test(expected = NullPointerException.class)
    public void testCheckPermissionForCurrentTenant() {
        /*
         * Test proverava ispravnost rada metode checkPermissionForCurrentTenant servisa TenantService
         * (provera poklapanja ID-a trenutno logovanog stanara i prosledjenog ID-a)
         * u slucaju kada nema trenutno ulogovanog stanara na nivou aplikacije - ocekivano NullPointerException
         * */

        // trenutno nema stanara na sesiji
        this.tenantService.checkPermissionForCurrentTenant(new Long(100));
    }

    @Test(expected = NullPointerException.class)
    public void testCheckPermissionForCurrentTenantInBuilding() {
        /*
         * Test proverava ispravnost rada metode checkPermissionForCurrentTenantInBuilding servisa TenantService
         * (provera da li trenutno logovani stanar zivi u zgradi koja je prosledjena)
         * u slucaju kada nema trenutno ulogovanog stanara na nivou aplikacije - ocekivano NullPointerException
         * */

        Building building = this.buildingService.findOne(new Long(101));

        // trenutno nema stanara na sesiji
        this.tenantService.checkPermissionForCurrentPresidentInBuilding(building);
    }

    @Test(expected = NullPointerException.class)
    public void testCheckPermissionForCurrentPresidentInBuilding() {
        /*
         * Test proverava ispravnost rada metode checkPermissionForCurrentPresidentInBuilding servisa TenantService
         * (provera da li je trenutno logovani stanar predsednik skupstine stanara u zgradi koja je prosledjena)
         * u slucaju kada nema trenutno ulogovanog stanara na nivou aplikacije - ocekivano NullPointerException
         * */

        Building building = this.buildingService.findOne(new Long(101));

        // trenutno nema stanara na sesiji
        this.tenantService.checkPermissionForCurrentPresidentInBuilding(building);
    }
}
