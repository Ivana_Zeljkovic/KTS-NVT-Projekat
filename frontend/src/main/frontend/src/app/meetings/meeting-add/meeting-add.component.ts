import { Component, EventEmitter, Output } from '@angular/core';
import { ToasterService } from "angular5-toaster/dist";
import { BsDatepickerConfig, BsLocaleService, BsModalRef, BsModalService, defineLocale, enGb } from "ngx-bootstrap";
import * as _ from "lodash";
// model
import { MeetingCreate } from "../../shared/models/meeting-create";
import { MeetingItem } from "../../shared/models/meeting-item";
import { QuestionnaireUpdate } from "../../shared/models/questionnaire-update";
// service
import { MeetingService } from "../../core/services/meeting.service";
import { JwtService } from "../../core/services/jwt.service";
// error
import { AppError } from "../../shared/errors/app-error";
import { BadRequestError } from "../../shared/errors/bad-request-error";
import { ForbiddenError } from "../../shared/errors/forbidden-error";
import { NotFoundError } from "../../shared/errors/not-found-error";
// component
import { MeetingItemListModalComponent } from "../meeting-item-list-modal/meeting-item-list-modal.component";
import { QuestionnaireComponent } from "../../shared/components/questionnaire/questionnaire.component";


@Component({
  selector: 'app-meeting-add',
  templateUrl: './meeting-add.component.html',
  styleUrls: ['./meeting-add.component.css']
})
export class MeetingAddComponent {
  @Output() meetingCreated = new EventEmitter<boolean>();
  model: MeetingCreate;
  meetingTime: Date;
  initialValues: MeetingCreate;
  selectedMeetingItems: Array<MeetingItem>;
  minValue: number;
  maxValue: number;

  bsConfig: Partial<BsDatepickerConfig>;
  minDate: Date;
  minTime: Date;
  maxTime: Date;

  disableSaveAndReset: boolean;
  dateChanged: boolean;
  timeChanged: boolean;
  meetingItemListChanged: boolean;

  // Elements for bootstrap paging
  currentPage: number;
  totalItems: number;
  maxSize: number;
  itemsPerPage: number;

  // Elements for getting data from server
  pageSize: string;
  sortDirection: string;
  sortProperties: string[];


  constructor(private meetingService: MeetingService, private modalService: BsModalService,
              private modalRef: BsModalRef, private jwtService: JwtService,
              private toasterService: ToasterService, private localeService: BsLocaleService) {
    this.model = new MeetingCreate(null, 0, new Array<number>());
    this.initialValues = _.cloneDeep(this.model);
    this.selectedMeetingItems = new Array<MeetingItem>();
    this.meetingTime = new Date();
    this.meetingTime.setHours(8);
    this.meetingTime.setMinutes(0);

    this.disableSaveAndReset = true;
    this.dateChanged = false;
    this.dateChanged = false;
    this.meetingItemListChanged = false;

    this.minValue = 45;
    this.maxValue = 180;

    this.bsConfig = Object.assign({}, {containerClass: 'theme-dark-blue'});
    this.minDate = new Date();
    defineLocale('enGb', enGb);
    this.localeService.use('enGb');
    this.minTime = new Date();
    this.maxTime = new Date();
    this.minTime.setHours(7);
    this.minTime.setMinutes(59);
    this.maxTime.setHours(17);
    this.maxTime.setMinutes(1);

    // Sett up for bootstrap paging
    this.currentPage = 1;
    this.totalItems = 0;
    this.maxSize = 3;
    this.itemsPerPage = 3;

    // Sett up for getting damages request
    this.pageSize = '3';
    this.sortDirection = 'ASC';
    this.sortProperties = ['date'];

    this.model.meetingDate = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
  }

  save() {
    this.model.meetingDate.setHours(this.meetingTime.getHours());
    this.model.meetingDate.setMinutes(this.meetingTime.getMinutes());
    this.model.meetingDate.setSeconds(0);
    this.selectedMeetingItems.forEach((meetingItem: MeetingItem) => {
      this.model.meetingItemsId.push(meetingItem.id);
    });

    this.meetingService.save(this.model, 'buildings', this.jwtService.getBuildingIdFromToken(), 'council/meetings')
      .subscribe(newMeeting => {
        this.meetingCreated.emit(true);
        this.modalRef.hide();
      }, (error: AppError) => {
        if(error instanceof BadRequestError)
          this.toasterService.pop('error', 'Error',
            'Given data have bad format/Some of selected meeting items doesn\'t belong to selected building or is already connected with some other meeting!');
        else if(error instanceof ForbiddenError)
          this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
        else if(error instanceof NotFoundError)
          this.toasterService.pop('error', 'Error', 'Building/Some of selected meeting items not found!');
        else {
          this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
          throw error;
        }
      });
  }

  showExistingMeetingItems() {
    const modalRef = this.modalService.show(MeetingItemListModalComponent);
    modalRef.content.initialValues = _.clone(this.selectedMeetingItems);

    modalRef.content.submitted.subscribe((newList) => {
      this.selectedMeetingItems = newList;
      if(this.selectedMeetingItems.length > 0)
        this.meetingItemListChanged = true;
    });
  }

  viewQuestionnaire(meetingItem: MeetingItem) {
    const modalRef = this.modalService.show(QuestionnaireComponent);

    modalRef.content.isReviewMode = true;
    modalRef.content.model = new QuestionnaireUpdate(meetingItem.questionnaire.id, meetingItem.questionnaire.questions);
    modalRef.content.initialValues = _.cloneDeep(modalRef.content.model);
  }

  pageChanged(event: any) {
    this.currentPage = event.page;
  }

  dateChange() {
    if(this.model.meetingDate)
      this.dateChanged = true;
  }

  timeChange() {
    if((this.meetingTime.getHours() && this.meetingTime.getHours() !== 8)
      || (this.meetingTime.getMinutes() && this.meetingTime.getMinutes() !== 0))
      this.timeChanged = true;
  }

  changeExist(newValues) {
    this.disableSaveAndReset = (newValues.meetingDate === this.initialValues.meetingDate &&
      newValues.duration === this.initialValues.duration);
  }

  reset() {
    this.model = _.cloneDeep(this.initialValues);
    this.model.meetingDate = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
    this.meetingTime = new Date();
    this.meetingTime.setHours(8);
    this.meetingTime.setMinutes(0);
    this.selectedMeetingItems.length = 0;

    this.disableSaveAndReset = true;
    this.dateChanged = false;
    this.timeChanged = false;
    this.meetingItemListChanged = false;
  }
}
