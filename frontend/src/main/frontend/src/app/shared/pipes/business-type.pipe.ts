import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
  name: 'business_type'
})
export class BusinessTypePipe implements PipeTransform {
  transform(value: any, args?: any[]) {
      if(!value)
        return null;

      let str = value.toString();
      str = str.toLowerCase();
      str = str[0].toUpperCase() + str.slice(1);
      str = str.replace(/_/g, ' ');
      return str;
  }
}
