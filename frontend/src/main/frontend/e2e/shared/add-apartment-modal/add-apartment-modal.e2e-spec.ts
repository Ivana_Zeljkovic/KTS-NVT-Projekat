import {browser} from "protractor";
// page
import {AddApartmentModalPage} from "./page-objects/add-apartment-modal.po";
import {LoginPage} from "../../page-objects/login.po";
import {BuildingListPage} from "../../buildings/building-list/page-objects/building-list.po";
import {BuildingRegistrationPage} from "../../buildings/building-registration/page-objects/building-registration.po";

describe('Add apartment modal', () => {
  let loginPage: LoginPage;
  let addApartmentModalPage: AddApartmentModalPage;
  let buildingsListPage: BuildingListPage;
  let buildingRegistrationPage: BuildingRegistrationPage;

  beforeEach(() => {
    loginPage = new LoginPage();
    addApartmentModalPage = new AddApartmentModalPage();
    buildingsListPage = new BuildingListPage();
    buildingRegistrationPage = new BuildingRegistrationPage();
    browser.get("/");

    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');
    loginPage.setUsername('ivana');
    loginPage.setPassword('ivana');
    loginPage.getSignInButton().click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/home');
    browser.get('http://localhost:49152/#/buildings');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/buildings');

    buildingsListPage.getAddButton().click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/buildings/register');
    buildingRegistrationPage.getDisplayAddApartment().click();
  });

  afterEach(() => {
    browser.executeScript('window.localStorage.clear();');
  });

  it('should load all elements of page', () => {
    expect(addApartmentModalPage.getPageTitle().isDisplayed()).toBe(true);
    expect(addApartmentModalPage.getNumber().isDisplayed()).toBe(true);
    expect(addApartmentModalPage.getSurface().isDisplayed()).toBe(true);
    expect(addApartmentModalPage.getFloor().isDisplayed()).toBe(true);
    expect(addApartmentModalPage.getConfirmButton().isDisplayed()).toBe(true);
    expect(addApartmentModalPage.getCloseButton().isDisplayed()).toBe(true);

    expect(addApartmentModalPage.getConfirmButton().isEnabled()).toBe(false);
  });

  it('should enable confirm button when all inputs are correct', () =>{

    addApartmentModalPage.setNumber(1);
    addApartmentModalPage.setFloor(1);
    addApartmentModalPage.setSurface(1);
    expect(addApartmentModalPage.getConfirmButton().isEnabled()).toBe(true);

  });

  it('should show error for required apartment number when user put focus and leaves it blank', () =>{

    addApartmentModalPage.getNumber().click();
    addApartmentModalPage.getFloor().click();
    expect(addApartmentModalPage.getNumberRequiredError().isDisplayed()).toBe(true);
    expect(addApartmentModalPage.getConfirmButton().isEnabled()).toBe(false);

  });

  it('should show error for required apartment surface when user put focus and leaves it blank', () =>{

    addApartmentModalPage.getSurface().click();
    addApartmentModalPage.getFloor().click();
    expect(addApartmentModalPage.getSurfaceRequiredError().isDisplayed()).toBe(true);
    expect(addApartmentModalPage.getConfirmButton().isEnabled()).toBe(false);

  });

  it('should show error for required apartment floor when user put focus and leaves it blank', () =>{

    addApartmentModalPage.getFloor().click();
    addApartmentModalPage.getNumber().click();
    expect(addApartmentModalPage.getFloorRequiredError().isDisplayed()).toBe(true);
    expect(addApartmentModalPage.getConfirmButton().isEnabled()).toBe(false);

  });

});
