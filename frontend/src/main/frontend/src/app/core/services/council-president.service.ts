import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
// service
import { DataService } from "./data.service";


@Injectable()
export class CouncilPresidentService extends DataService<void>{

  constructor(http: HttpClient) {
    super('', http);
  }
}
