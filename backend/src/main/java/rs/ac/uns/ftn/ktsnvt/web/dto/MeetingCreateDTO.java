package rs.ac.uns.ftn.ktsnvt.web.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Nikola Garabandic on 22/11/2017.
 */
public class MeetingCreateDTO implements Serializable {

    @NotNull
    private Date meetingDate;
    @NotNull
    private Integer duration;
    @NotNull
    @Size(min = 1)
    private List<Long> meetingItemsId;

    public MeetingCreateDTO() { meetingItemsId = new ArrayList<>();}

    public MeetingCreateDTO(Date meetingDate, Integer duration, List<Long> meetingItemsId) {
        this.meetingDate = meetingDate;
        this.duration = duration;
        this.meetingItemsId = meetingItemsId;
    }

    public MeetingCreateDTO(Date meetingDate) {
        this.meetingDate = meetingDate;
    }

    public Date getMeetingDate() {
        return this.meetingDate;
    }

    public void setMeetingDate(Date meetingDate) {
        this.meetingDate = meetingDate;
    }

    public Integer getDuration() {
        return this.duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public List<Long> getMeetingItemsId() {
        return meetingItemsId;
    }

    public void setMeetingItemsId(List<Long> meetingItemsId) {
        this.meetingItemsId = meetingItemsId;
    }
}
