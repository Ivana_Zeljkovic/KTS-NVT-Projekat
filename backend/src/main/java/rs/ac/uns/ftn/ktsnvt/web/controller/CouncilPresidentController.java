package rs.ac.uns.ftn.ktsnvt.web.controller;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.ForbiddenException;
import rs.ac.uns.ftn.ktsnvt.model.entity.*;
import rs.ac.uns.ftn.ktsnvt.service.*;
import rs.ac.uns.ftn.ktsnvt.web.controller.util.MessageConstants;
import rs.ac.uns.ftn.ktsnvt.web.dto.UpdatePresidentDTO;

import javax.validation.Valid;


/**
 * Created by Ivana Zeljkovic on 18/11/2017.
 */
@RestController
@RequestMapping(value = "/api")
@Api(value="council presidents", description="Operations pertaining to council presidents in application.")
public class CouncilPresidentController {

    @Autowired
    private BuildingService buildingService;

    @Autowired
    private CouncilService councilService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private QuestionnaireService questionnaireService;

    @Autowired
    private TenantService tenantService;

    @Autowired
    private AuthorityService authorityService;

    @Autowired
    private AccountAuthorityService accountAuthorityService;



    @RequestMapping(
            value = "/buildings/{buildingId}/council_president",
            method = RequestMethod.PATCH,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Update current president of the council.",
            notes = "You have to provide a valid building ID in the URL.",
            httpMethod = "PATCH",
            produces = "application/json",
            consumes = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully set up new president of the building's council!"),
            @ApiResponse(code = 400, message = "Inappropriate object sent in request body"),
            @ApiResponse(code = 401, message = "You are not authorized to update the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('COUNCIL_PRESIDENT') or hasAuthority('COMPANY') or hasAuthority('ADMIN')")
    public ResponseEntity setUpPresident(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The object with name and username of new president.", required = true) @Valid @RequestBody UpdatePresidentDTO updatePresidentDTO,
            @ApiParam(value = "The object that contains all errors from validation of DTO object") BindingResult errors) {

        Account account = this.accountService.findByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName());
        Building building = this.buildingService.findOne(buildingId);

        Tenant currentPresident = null;
        Administrator currentAdministrator = null;
        Business currentFirm = null;

        for(GrantedAuthority authority : SecurityContextHolder.getContext().getAuthentication().getAuthorities()) {
            if(authority.getAuthority().equals(MessageConstants.ADMIN_ROLE))
                currentAdministrator = account.getAdministrator();
            else if(authority.getAuthority().equals(MessageConstants.FIRM_ROLE))
                currentFirm = account.getBusinessOwner();
            else if(authority.getAuthority().equals(MessageConstants.PRESIDENT_ROLE))
                currentPresident = account.getTenant();
        }

        // provera ko je trenutno ulogovan i provera prava da li ulogovani korisnik ima pravo da izvrsi akciju
        checkPermissionForSetUpPresident(building, currentAdministrator, currentPresident, currentFirm);

        if(updatePresidentDTO.getQuestionnaireId() != null && currentPresident != null) {
            // ukoliko je novi predsednik biran na osnovu ankete - nije izbor incijalnog predsednika, tada treba
            // proveriti da li anketa ciji je ID prosledjen zaista pripada toj zgradi i
            // da li je isteklo vreme za glasanje na anketu
            Questionnaire questionnaire = this.questionnaireService.findOne(updatePresidentDTO.getQuestionnaireId());

            this.questionnaireService.checkBuildingForQuestionnaire(questionnaire.getId(), building.getBillboard().getId());
            this.questionnaireService.checkQuestionnaireExpired(questionnaire);
        }
        else if((currentAdministrator != null || currentFirm != null) && updatePresidentDTO.getQuestionnaireId() != null)
            throw new BadRequestException("The request object is not valid!");

        Tenant newPresident = this.tenantService.findOneByFirstNameAndLastNameAndUsername(updatePresidentDTO.getFirstName(),
                updatePresidentDTO.getLastName(), updatePresidentDTO.getUsername());

        if(newPresident.getApartmentLiveIn() == null || !newPresident.getApartmentLiveIn().getBuilding().getId().equals(buildingId))
            throw new BadRequestException("Selected user doesn't live in this building");

        if(newPresident.getCouncil() == null)
            throw new BadRequestException("Selected tenant is not member of building's council!");

        //dodajemo rolu COUNCIL_PRESIDENT za novoizbranog predsednika skupstine stanara
        AccountAuthority accountAuthority = new AccountAuthority();
        accountAuthority.setAuthority(this.authorityService.findByName(MessageConstants.PRESIDENT_ROLE));
        accountAuthority.setAccount(newPresident.getAccount());
        newPresident.getAccount().getAccountAuthorities().add(accountAuthority);
        this.accountAuthorityService.save(accountAuthority);

        if(currentPresident != null) {
            //starom predsedniku skupstine stanara ukidamo rolu COUNCIL_PRESIDENT
            this.tenantService.removePresidentRole(currentPresident);
            if(currentPresident.getCouncil() == null) {
                boolean alreadyExistsMemberOfCouncil = false;
                //ukoliko za stan u kom zivi trenutni predsednik nema nijedan clan u skupstini stanara,
                // tada tog predsednika setujemo kao predstavnika u skupstini za svoj stan
                for (Tenant member : building.getCouncil().getMembers()) {
                    if (member.getApartmentLiveIn().getId().equals(currentPresident.getApartmentLiveIn().getId())) {
                        alreadyExistsMemberOfCouncil = true;
                        break;
                    }
                }

                if (!alreadyExistsMemberOfCouncil) {
                    currentPresident.setCouncil(building.getCouncil());
                    this.tenantService.save(currentPresident);
                }
            }
        }

        building.getCouncil().setPresident(newPresident);

        this.tenantService.save(newPresident);
        this.councilService.save(building.getCouncil());

        return new ResponseEntity<>(HttpStatus.OK);
    }



    private void checkPermissionForSetUpPresident(Building building, Administrator currentAdministrator,
                                                  Tenant currentPresident, Business currentFirm) {
        // zgrada nema predsednika skupstine stanara
        // (novog postavlja zaduzena firma - ako postoji, ili administrator - ako ne postoji zaduzena firma)
        if(building.getCouncil().getPresident() == null) {
            // zaduzena firma postoji
            if(building.getCouncil().getResponsibleCompany() != null) {
                // ako je trenutno logovan administrator ili neki predsednik SS
                if(currentAdministrator != null || currentPresident != null)
                    throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);
                    // ako trenutno logovana firma nije ujedno i firma koja je odgovorna za upravljanje zgradom
                else if(currentFirm != null &&
                        !building.getCouncil().getResponsibleCompany().getId().equals(currentFirm.getId()))
                    throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);
            }
            // zaduzena firma ne postoji
            else {
                // ako je trenutno logovana neka firma ili neki predsednik SS
                if(currentFirm != null || currentPresident != null)
                    throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);
            }
        }
        // zgrada ima predsednika skupstine stanara
        // (on postavlja novog)
        else {
            // ako je trenutno logovan administrator ili firma
            if(currentFirm != null || currentAdministrator != null)
                throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);
                // ako trenutno logovani predsednik SS nije ujedno i predsednik SS u ovoj zgradi
            else if(currentPresident != null &&
                    !building.getCouncil().getPresident().getId().equals(currentPresident.getId()))
                throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);
        }

    }
}
