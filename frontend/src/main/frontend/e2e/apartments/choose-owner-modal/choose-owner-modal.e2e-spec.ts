import {browser, by, element} from "protractor";
// page
import {LoginPage} from "../../page-objects/login.po";
import {ChooseOwnerModalPage} from "./page-objects/choose-owner-modal.po";
import {ApartmentPage} from "../apartment/page-objects/apartment.po";

describe('Choose owner modal', () => {
  let loginPage: LoginPage;
  let chooseOwnerModalPage: ChooseOwnerModalPage;
  let apartmentPage: ApartmentPage;

  beforeEach(() => {
    loginPage = new LoginPage();
    chooseOwnerModalPage = new ChooseOwnerModalPage();
    apartmentPage = new ApartmentPage();
    browser.get("/");

    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');
    loginPage.setUsername('ivana');
    loginPage.setPassword('ivana');
    loginPage.getSignInButton().click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/home');
    browser.get('http://localhost:49152/#/apartments/104');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/apartments/104');

  });

  afterEach(() => {
    browser.executeScript('window.localStorage.clear();');
  });

  it('should load all elements of modal', () => {

    apartmentPage.getAddTenantButton().click();
    expect(chooseOwnerModalPage.getPageTitle().isDisplayed()).toBe(true);
    expect(chooseOwnerModalPage.getTenantsTable().isDisplayed()).toBe(true);
    expect(chooseOwnerModalPage.getPagination().isDisplayed()).toBe(true);
    expect(chooseOwnerModalPage.getCloseButton().isDisplayed()).toBe(true);
  });


});
