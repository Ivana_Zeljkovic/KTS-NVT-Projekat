package rs.ac.uns.ftn.ktsnvt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import rs.ac.uns.ftn.ktsnvt.model.entity.AccountAuthority;

/**
 * Created by Ivana Zeljkovic on 05/11/2017.
 */
public interface AccountAuthorityRepository extends JpaRepository<AccountAuthority, Long>{

    @Modifying
    @Query("DELETE FROM AccountAuthority WHERE account.id = :accountId AND authority.id IN " +
            "(SELECT a.id FROM Authority a WHERE a.name = 'COUNCIL_PRESIDENT')")
    int removePresidentRole(@Param("accountId") Long accountId);
}
