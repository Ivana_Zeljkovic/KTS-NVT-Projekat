package rs.ac.uns.ftn.ktsnvt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.ktsnvt.model.entity.Authority;

/**
 * Created by Nikola Garabandic on 06/11/2017.
 */
public interface AuthorityRepository extends JpaRepository<Authority, Long> {

    Authority findByName(String name);

}
