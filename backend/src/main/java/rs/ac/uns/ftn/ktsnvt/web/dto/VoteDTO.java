package rs.ac.uns.ftn.ktsnvt.web.dto;

import rs.ac.uns.ftn.ktsnvt.model.entity.Vote;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ivana Zeljkovic on 20/11/2017.
 */
public class VoteDTO implements Serializable {

    private Long id;
    private TenantDTO tenant;
    private List<VoteQuestionDTO> questionsVotes;

    public VoteDTO() {
        this.questionsVotes = new ArrayList<>();
    }

    public VoteDTO(Vote vote) {
        this.id = vote.getId();
        this.tenant = new TenantDTO(vote.getTenant());
        List<VoteQuestionDTO> voteQuestionDTOS = new ArrayList<>();
        vote.getQuestionsVotes().forEach(questionVote -> voteQuestionDTOS.add(new VoteQuestionDTO(questionVote)));
        this.questionsVotes = voteQuestionDTOS;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TenantDTO getTenant() {
        return tenant;
    }

    public void setTenant(TenantDTO tenant) {
        this.tenant = tenant;
    }

    public List<VoteQuestionDTO> getQuestionsVotes() {
        return questionsVotes;
    }

    public void setQuestionsVotes(List<VoteQuestionDTO> questionsVotes) {
        this.questionsVotes = questionsVotes;
    }
}
