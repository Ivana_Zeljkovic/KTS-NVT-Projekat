package rs.ac.uns.ftn.ktsnvt.service.unit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.info.ProjectInfoProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.*;
import rs.ac.uns.ftn.ktsnvt.model.enumeration.BusinessType;
import rs.ac.uns.ftn.ktsnvt.repository.ApartmentRepository;
import rs.ac.uns.ftn.ktsnvt.repository.BuildingRepository;
import rs.ac.uns.ftn.ktsnvt.repository.BusinessRepository;
import rs.ac.uns.ftn.ktsnvt.repository.DamageRepository;
import rs.ac.uns.ftn.ktsnvt.service.DamageService;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.BDDMockito.given;

/**
 * Created by Katarina Cukurov on 30/11/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DamageServiceUnitTest {

    @Autowired
    private DamageService damageService;

    @MockBean
    private DamageRepository damageRepository;

    @MockBean
    private BusinessRepository businessRepository;

    @Before
    public void setUp(){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = dateFormat.parse("2017-11-27 00:00:00");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Damage damage = new Damage("Damage for unit test.",  date,
                new DamageType("ELECTRICAL_PROBLEM"), false, null);
        Apartment apartment = new Apartment(1, 1, 60);
        Building building = new Building(4, true);
        Meeting meeting = new Meeting(date);
        Account account = new Account("kacac", "cuki");
        Tenant tenant = new Tenant("Katarina", "Cukurov", date, "kaca.cukurov@gmail.com",
                "321354", true, account);
        Business business = new Business("Inst1", "Opis", "123231", "mail@gmail.com", BusinessType.FIRE_SERVICE, false);


        business.setId(102L);
        building.setId(new Long(100));
        apartment.setId(new Long(100));
        meeting.setId(new Long(100));
        tenant.setId(new Long(100));
        apartment.setBuilding(building);
        damage.setApartment(apartment);
        damage.setBuilding(building);
        damage.setResponsiblePerson(tenant);
        damage.setResponsibleInstitution(business);


        given(
                this.damageRepository.findOne(new Long(100))
        ).willReturn(
                damage
        );

        given(
                this.damageRepository.findOne(new Long(500))
        ).willReturn(
                null
        );

        given(
                this.businessRepository.findOne(102L)
        ).willReturn(
                business
        );
    }

    @Test
    public void testFindOne() {
        /*
         * Test proverava ispravnost rada metode findOne servisa DamageService
         * (dobavljanje testa na osnovu ID-a)
         * u slucaju kada postoji test sa zadatim ID-em
         * */

        /*
        test sa ID-em = 100 ima podatke:
            id = 100,
            date = 2017-11-27,
            description = 'Damage for unit test.',
            fixed = false,
            apartment_id = 100,
            building_id = 100,
            responsible_institution_id = null,
            responsible_person_id = 100
         */
        Damage damage = this.damageService.findOne(new Long(100));

        assertNotNull(damage);
        assertEquals("Mon Nov 27 00:00:00 CET 2017", damage.getDate().toString());
        assertEquals("Damage for unit test.", damage.getDescription());
        assertEquals(new Long(100), damage.getBuilding().getId());
        assertEquals(new Long(100), damage.getApartment().getId());
        assertEquals(new Long(100), damage.getResponsiblePerson().getId());
    }

    @Test(expected = NotFoundException.class)
    public void testFindOneNonexistent() {
         /*
         * Test proverava ispravnost rada metode findOne servisa DamageService
         * (dobavljanje testa na osnovu ID-a)
         * u slucaju kada ne postoji test sa zadatim ID-em - ocekivano NotFoundException
         * */

        // damage sa ID-em = 500 ne postoji
        Damage damage = this.damageService.findOne(new Long(500));
    }

    @Test
    public void testCheckDamageInBuilding() {
        /*
         * Test proverava ispravnost rada metode checkDamageInBuilding servisa DamageService
         * (provera pripadnosti konkretnog kvara zgradi ciji je ID prosledjen)
         * u slucaju kada prosledjeni kvar zaista pripada zgradi sa prosledjenim ID-em
         * */

        // damage sa ID-em = 100 pripada zgradi sa ID-em = 100
        Damage damage = this.damageService.findOne(new Long(100));

        this.damageService.checkDamageInBuilding(damage, new Long(100));
    }

    @Test(expected = BadRequestException.class)
    public void testCheckDamageInBuildingInvalid() {
        /*
         * Test proverava ispravnost rada metode checkDamageInBuilding servisa DamageService
         * (provera pripadnosti konkretnog kvara zgradi ciji je ID prosledjen)
         * u slucaju kada prosledjeni kvar ne pripada zgradi sa prosledjenim ID-em - ocekivano BadRequestException
         * */

        // damage sa ID-em = 100 ne pripada zgradi sa ID-em = 101
        Damage damage = this.damageService.findOne(new Long(100));

        this.damageService.checkDamageInBuilding(damage, new Long(101));
    }
}
