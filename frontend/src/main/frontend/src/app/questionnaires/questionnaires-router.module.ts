import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";
// component
import { QuestionnaireListComponent } from "./questionnaire-list/questionnaire-list.component";
import { TenantOwnerGuard } from "../core/guards/tenant-owner.guard";
import { CouncilMemberGuard } from "../core/guards/council-member.guard";


const routes: Routes = [
  { path: '', component: QuestionnaireListComponent, canActivate: [TenantOwnerGuard] },
  { path: 'results', component: QuestionnaireListComponent, canActivate: [CouncilMemberGuard] }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class QuestionnairesRouterModule { }
