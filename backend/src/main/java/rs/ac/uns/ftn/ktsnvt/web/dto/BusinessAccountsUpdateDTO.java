package rs.ac.uns.ftn.ktsnvt.web.dto;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Created by Katarina Cukurov on 05/12/2017.
 */
public class BusinessAccountsUpdateDTO {

    @Valid
    @Size(min = 1)
    private List<LoginDTO> loginAccounts;

    public BusinessAccountsUpdateDTO() { }

    public BusinessAccountsUpdateDTO(List<LoginDTO> loginAccounts) {
    this.loginAccounts = loginAccounts;
    }

    public List<LoginDTO> getLoginAccounts() {return loginAccounts;}

    public void setLoginAccounts(List<LoginDTO> loginAccounts) {this.loginAccounts = loginAccounts;}
}
