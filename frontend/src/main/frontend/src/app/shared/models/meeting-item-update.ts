import { QuestionnaireUpdate } from "./questionnaire-update";

export class MeetingItemUpdate {
  constructor(public content: string,
              public title: string,
              public questionnaireUpdate?: QuestionnaireUpdate,
              public damageId?: number) { }
}
