package rs.ac.uns.ftn.ktsnvt.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;
import rs.ac.uns.ftn.ktsnvt.controller.util.TestUtil;
import rs.ac.uns.ftn.ktsnvt.web.dto.LoginDTO;
import rs.ac.uns.ftn.ktsnvt.web.dto.ResponsibleForDamageTypeDTO;
import rs.ac.uns.ftn.ktsnvt.web.dto.TokenDTO;

import javax.annotation.PostConstruct;
import java.nio.charset.Charset;

import static org.hamcrest.Matchers.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Created by Ivana Zeljkovic on 06/12/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class DamageTypeControllerIntegrationTest {

    private static final String URL_PREFIX = "/api";

    private MediaType contentType = new MediaType(
            MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    private MockMvc mockMvc;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja stanara koji je ujedno i predsednik skupstine stanara
    private String accessTokenPresident;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja stanara koji je ujedno i predsednik skupstine stanara
    // ali u nekoj drugoj zgradi
    private String accessTokenInvalidPresident;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja stanara
    private String accessTokenTenant;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja institucije
    private String accessTokenInstitution;
    private MultiValueMap<String, String> params;


    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private TestRestTemplate testRestTemplate;


    @PostConstruct
    public void setup() {
        this.mockMvc = MockMvcBuilders.
                webAppContextSetup(webApplicationContext).apply(springSecurity()).build();
    }

    // pre izvrsavanja testa, neophodno je izvrsiti logovanje u aplikaciju da bismo dobili token
    @Before
    public void login() throws Exception {
        // logovanje stanara
        LoginDTO loginDTOTenant = new LoginDTO("jasmina", "jasmina");

        ResponseEntity<TokenDTO> responseEntityTenant =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOTenant,
                        TokenDTO.class);
        // preuzmemo token za logovanog stanara iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenTenant = responseEntityTenant.getBody().getValue();


        // logovanje stanara koji je ujedno i predsednik skupstine stanara
        LoginDTO loginDTOPresident = new LoginDTO("kaca", "kaca");

        ResponseEntity<TokenDTO> responseEntityPresident =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOPresident,
                        TokenDTO.class);
        // preuzmemo token za logovanog stanara koji je ujedno i predsednik skupstine stanara iz zaglavlja odgovora
        // i setujemo ga na globalni nivo, jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenPresident = responseEntityPresident.getBody().getValue();


        // logovanje stanara koji je ujedno i predsednik skupstine stanara, ali u drugoj zgradi
        LoginDTO loginDTOPresident_2 = new LoginDTO("leontina", "leontina");

        ResponseEntity<TokenDTO> responseEntityPresident_2 =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOPresident_2,
                        TokenDTO.class);
        // preuzmemo token za logovanog stanara koji je ujedno i predsednik skupstine stanara iz zaglavlja odgovora
        // i setujemo ga na globalni nivo, jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenInvalidPresident = responseEntityPresident_2.getBody().getValue();


        // logovanje institucije
        LoginDTO loginDTOInstitution = new LoginDTO("inst1", "inst1");

        ResponseEntity<TokenDTO> responseEntityInstitution =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOInstitution,
                        TokenDTO.class);
        // preuzmemo token za logovanu instituciju iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenInstitution = responseEntityInstitution.getBody().getValue();

        // za pageable atribut
        params = TestUtil.getParams(
                "0", "10", "ASC", "id", "");
    }

    // TESTOVI za setResponsibleInstitutionForDamageType
    @Test
    @Transactional
    @Rollback(true)
    public void testSetResponsibleInstitutionForDamageType() throws Exception {
       /*
        * Test proverava ispravnost rada metode setResponsibleInstitutionForDamageType kontrolera DamageTypeController
        * (postavljanje odgovorne institucije za odredjenu vrstu kvara na nivou zadate zgrade od strane predsednika skupstine stanara)
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(102L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        /*
        u zgradi sa ID-em = 100 ne postoji odgovorna institucija za tip kvara sa ID-em = 5 (OTHER),
        pa se sada ta odgovornost pripisuje instituciji sa sledecim podacima:
            id = 102,
            name = First institution,
            description = Description for first institution,
            business_type = POLICE
        */
        this.mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damage_types/5/responsible_institution")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.institutionId").value(102))
                .andExpect(jsonPath("$.damageTypeId").value(5))
                .andExpect(jsonPath("$.buildingId").value(100));
    }

    @Test
    public void testSetResponsibleInstitutionForDamageTypeByNotAuthenticatedUser() throws Exception {
       /*
        * Test proverava ispravnost rada metode setResponsibleInstitutionForDamageType kontrolera DamageTypeController
        * (postavljanje odgovorne institucije za odredjenu vrstu kvara na nivou zadate zgrade od strane predsednika skupstine stanara)
        * u slucaju kada akciju obavlja nelogovani korisnik
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(102L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        this.mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damage_types/5/responsible_institution")
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSetResponsibleInstitutionForDamageTypeByNotAuthenticatedPresident() throws Exception {
       /*
        * Test proverava ispravnost rada metode setResponsibleInstitutionForDamageType kontrolera DamageTypeController
        * (postavljanje odgovorne institucije za odredjenu vrstu kvara na nivou zadate zgrade od strane predsednika skupstine stanara)
        * u slucaju kada akciju obavlja ulogovani korisnik koji nema ulogu predsednika skupstine stanara
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(102L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        this.mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damage_types/5/responsible_institution")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSetResponsibleInstitutionForDamageTypeNonexistentBuilding() throws Exception {
       /*
        * Test proverava ispravnost rada metode setResponsibleInstitutionForDamageType kontrolera DamageTypeController
        * (postavljanje odgovorne institucije za odredjenu vrstu kvara na nivou zadate zgrade od strane predsednika skupstine stanara)
        * u slucaju kada ne postoji zgrada sa zadatim ID-em
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(102L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // ne postoji zgrada sa ID-em = 200
        this.mockMvc.perform(patch(URL_PREFIX + "/buildings/200/damage_types/5/responsible_institution")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testSetResponsibleInstitutionForDamageTypeNonexistentDamageType() throws Exception {
       /*
        * Test proverava ispravnost rada metode setResponsibleInstitutionForDamageType kontrolera DamageTypeController
        * (postavljanje odgovorne institucije za odredjenu vrstu kvara na nivou zadate zgrade od strane predsednika skupstine stanara)
        * u slucaju kada ne postoji tip kvara sa zadatim ID-em
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(102L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // ne postoji tip kvara sa ID-em = 100
        this.mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damage_types/100/responsible_institution")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testSetResponsibleInstitutionForDamageTypePresidentFromInvalidBuilding() throws Exception {
       /*
        * Test proverava ispravnost rada metode setResponsibleInstitutionForDamageType kontrolera DamageTypeController
        * (postavljanje odgovorne institucije za odredjenu vrstu kvara na nivou zadate zgrade od strane predsednika skupstine stanara)
        * u slucaju kada trenutno ulogovani predsednik skupstine stanara nije ujedno i predsednik skupstine stanara u zgradi sa zadatim ID-em
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(102L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // stanar koji je trenutno logovan je predsednik skupstine stanara u zgradi sa ID-em = 101 (a ne ID-em = 100)
        this.mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damage_types/5/responsible_institution")
                .header("Authentication-Token", this.accessTokenInvalidPresident)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSetResponsibleInstitutionForDamageTypeNonexistentInstitution() throws Exception {
       /*
        * Test proverava ispravnost rada metode setResponsibleInstitutionForDamageType kontrolera DamageTypeController
        * (postavljanje odgovorne institucije za odredjenu vrstu kvara na nivou zadate zgrade od strane predsednika skupstine stanara)
        * u slucaju kada ne postoji institucija sa zadatim ID-em u DTO-u
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(200L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // ne postoji institucija sa ID-em = 200
        this.mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damage_types/5/responsible_institution")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testSetResponsibleInstitutionForDamageTypeNotInstitution() throws Exception {
       /*
        * Test proverava ispravnost rada metode setResponsibleInstitutionForDamageType kontrolera DamageTypeController
        * (postavljanje odgovorne institucije za odredjenu vrstu kvara na nivou zadate zgrade od strane predsednika skupstine stanara)
        * u slucaju kada torka kojoj pripada ID zadat u DTO-u nije institucija, nego firma
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(100L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // business torka sa ID-em = 100 je firma, nije institucija
        this.mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damage_types/5/responsible_institution")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testSetResponsibleInstitutionForDamageTypeMissingId() throws Exception {
       /*
        * Test proverava ispravnost rada metode setResponsibleInstitutionForDamageType kontrolera DamageTypeController
        * (postavljanje odgovorne institucije za odredjenu vrstu kvara na nivou zadate zgrade od strane predsednika skupstine stanara)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne poseduje sva neophodna polja - nedostaje polje id (nove odgovorne institucije)
        */

       ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO();
       // nedostaje vrednost polja id
       responsibilityDTO.setId(null);
       String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        this.mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damage_types/5/responsible_institution")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testSetResponsibleInstitutionForDamageTypeInappropriateTypeOfId() throws Exception {
       /*
        * Test proverava ispravnost rada metode setResponsibleInstitutionForDamageType kontrolera DamageTypeController
        * (postavljanje odgovorne institucije za odredjenu vrstu kvara na nivou zadate zgrade od strane predsednika skupstine stanara)
        * u slucaju kada je dolazni DTO objekat nevalidan: poseduje sva neophodna polja - ali je polje id nevalidnog tipa
        */

        // neodgovarajuca vrednost polja id
        String responsibilityDTOJson = "{id:'greska'}";

        this.mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damage_types/5/responsible_institution")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za setResponsiblePersonForDamageType
    @Test
    @Transactional
    @Rollback(true)
    public void testSetResponsiblePersonForDamageType() throws Exception {
       /*
        * Test proverava ispravnost rada metode setResponsiblePersonForDamageType kontrolera DamageTypeController
        * (postavljanje odgovornog lica za odredjenu vrstu kvara na nivou zadate zgrade od strane predsednika skupstine stanara)
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(101L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        /*
        u zgradi sa ID-em = 100 ne postoji odgovorna osoba za tip kvara sa ID-em = 5 (OTHER),
        pa se sada ta odgovornost pripisuje stanaru sa sledecim podacima:
            id = 101,
            firstName = Katarina,
            lastName = Cukurov,
            apartment_id = 101
        */
        this.mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damage_types/5/responsible_person")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.tenantId").value(101))
                .andExpect(jsonPath("$.damageTypeId").value(5))
                .andExpect(jsonPath("$.buildingId").value(100));
    }

    @Test
    public void testSetResponsiblePersonForDamageTypeByNotAuthenticatedUser() throws Exception {
       /*
        * Test proverava ispravnost rada metode setResponsiblePersonForDamageType kontrolera DamageTypeController
        * (postavljanje odgovornog lica za odredjenu vrstu kvara na nivou zadate zgrade od strane predsednika skupstine stanara)
        * u slucaju kada akciju obavlja nelogovani korisnik
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(101L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        this.mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damage_types/5/responsible_person")
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSetResponsiblePersonForDamageTypeByNotAuthenticatedPresident() throws Exception {
       /*
        * Test proverava ispravnost rada metode setResponsiblePersonForDamageType kontrolera DamageTypeController
        * (postavljanje odgovornog lica za odredjenu vrstu kvara na nivou zadate zgrade od strane predsednika skupstine stanara)
        * u slucaju kada akciju obavlja ulogovani korisnik koji nema ulogu predsednika skupstine stanara
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(101L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        this.mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damage_types/5/responsible_person")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSetResponsiblePersonForDamageTypeNonexistentBuilding() throws Exception {
       /*
        * Test proverava ispravnost rada metode setResponsiblePersonForDamageType kontrolera DamageTypeController
        * (postavljanje odgovornog lica za odredjenu vrstu kvara na nivou zadate zgrade od strane predsednika skupstine stanara)
        * u slucaju kada ne postoji zgrada sa zadatim ID-em
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(101L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // ne postoji zgrada sa ID-em = 200
        this.mockMvc.perform(patch(URL_PREFIX + "/buildings/200/damage_types/5/responsible_person")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testSetResponsiblePersonForDamageTypeNonexistentDamageType() throws Exception {
       /*
        * Test proverava ispravnost rada metode setResponsiblePersonForDamageType kontrolera DamageTypeController
        * (postavljanje odgovornog lica za odredjenu vrstu kvara na nivou zadate zgrade od strane predsednika skupstine stanara)
        * u slucaju kada ne postoji tip kvara sa zadatim ID-em
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(101L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // ne postoji tip kvara sa ID-em = 100
        this.mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damage_types/100/responsible_person")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testSetResponsiblePersonForDamageTypePresidentFromInvalidBuilding() throws Exception {
       /*
        * Test proverava ispravnost rada metode setResponsiblePersonForDamageType kontrolera DamageTypeController
        * (postavljanje odgovornog lica za odredjenu vrstu kvara na nivou zadate zgrade od strane predsednika skupstine stanara)
        * u slucaju kada trenutno ulogovani predsednik skupstine stanara nije ujedno i predsednik skupstine stanara u zgradi sa zadatim ID-em
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(101L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // stanar koji je trenutno logovan je predsednik skupstine stanara u zgradi sa ID-em = 101 (a ne ID-em = 100)
        this.mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damage_types/5/responsible_person")
                .header("Authentication-Token", this.accessTokenInvalidPresident)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSetResponsiblePersonForDamageTypeNonexistentTenant() throws Exception {
       /*
        * Test proverava ispravnost rada metode setResponsiblePersonForDamageType kontrolera DamageTypeController
        * (postavljanje odgovornog lica za odredjenu vrstu kvara na nivou zadate zgrade od strane predsednika skupstine stanara)
        * u slucaju kada ne postoji stanar sa zadatim ID-em u DTO-u
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(200L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // ne postoji stanar sa ID-em = 200
        this.mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damage_types/5/responsible_institution")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testSetResponsiblePersonForDamageTypeTenantFromInvalidBuilding() throws Exception {
       /*
        * Test proverava ispravnost rada metode setResponsiblePersonForDamageType kontrolera DamageTypeController
        * (postavljanje odgovornog lica za odredjenu vrstu kvara na nivou zadate zgrade od strane predsednika skupstine stanara)
        * u slucaju kada stanar sa ID-em zadatim u DTO-u ne zivi u zgradi sa zadatim ID-em
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(107L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // stanar sa ID-em = 107 zivi u zgradi sa ID-em = 101 (a ne sa ID-em = 100)
        this.mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damage_types/5/responsible_person")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testSetResponsiblePersonForDamageTypeMissingId() throws Exception {
       /*
        * Test proverava ispravnost rada metode setResponsiblePersonForDamageType kontrolera DamageTypeController
        * (postavljanje odgovornog lica za odredjenu vrstu kvara na nivou zadate zgrade od strane predsednika skupstine stanara)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne poseduje sva neophodna polja - nedostaje polje id (nove odgovorne institucije)
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO();
        // nedostaje vrednost polja id
        responsibilityDTO.setId(null);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        this.mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damage_types/5/responsible_person")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testSetResponsiblePersonForDamageTypeInappropriateTypeOfId() throws Exception {
       /*
        * Test proverava ispravnost rada metode setResponsiblePersonForDamageType kontrolera DamageTypeController
        * (postavljanje odgovornog lica za odredjenu vrstu kvara na nivou zadate zgrade od strane predsednika skupstine stanara)
        * u slucaju kada je dolazni DTO objekat nevalidan: poseduje sva neophodna polja - ali je polje id nevalidnog tipa
        */

        // neodgovarajuca vrednost polja id
        String responsibilityDTOJson = "{id:'greska'}";

        this.mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damage_types/5/responsible_person")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za delegateResponsibilityToInstitutionForDamageType
    @Test
    @Transactional
    @Rollback(true)
    public void testDelegateResponsibilityToInstitutionForDamageType() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToInstitutionForDamageType kontrolera DamageTypeController
        * (delegiranje odgovornosti nekoj drugoj instituciji za zadati tip kvara u zadatoj zgradi od trenutno odgovorne institucije)
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(103L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        /*
        u zgradi sa ID-em = 100 postoji odgovorna institucija za tip kvara sa ID-em = 1 (PLUMBING_DAMAGE),
        pri cemu je odgovorna institucija opisana sledecim podacima:
            id = 102,
            name = First institution,
            description = Description for first institution,
            business_type = POLICE
        */
        this.mockMvc.perform(patch(URL_PREFIX + "/institutions/102/damage_types/1/buildings/100/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenInstitution)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.institutionId").value(103))
                .andExpect(jsonPath("$.damageTypeId").value(1))
                .andExpect(jsonPath("$.buildingId").value(100));
    }

    @Test
    public void testDelegateResponsibilityToInstitutionForDamageTypeByNotAuthenticatedUser() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToInstitutionForDamageType kontrolera DamageTypeController
        * (delegiranje odgovornosti nekoj drugoj instituciji za zadati tip kvara u zadatoj zgradi od trenutno odgovorne institucije)
        * u slucaju kada akciju obavlja nelogovani korisnik
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(103L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        this.mockMvc.perform(patch(URL_PREFIX + "/institutions/102/damage_types/1/buildings/100/delegate_responsibility")
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDelegateResponsibilityToInstitutionForDamageTypeByNotAuthenticatedInstitution() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToInstitutionForDamageType kontrolera DamageTypeController
        * (delegiranje odgovornosti nekoj drugoj instituciji za zadati tip kvara u zadatoj zgradi od trenutno odgovorne institucije)
        * u slucaju kada akciju obavlja logovani korisnik koji nema ulogu institucije
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(103L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        this.mockMvc.perform(patch(URL_PREFIX + "/institutions/102/damage_types/1/buildings/100/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDelegateResponsibilityToInstitutionForDamageTypeNonexistentInstitution() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToInstitutionForDamageType kontrolera DamageTypeController
        * (delegiranje odgovornosti nekoj drugoj instituciji za zadati tip kvara u zadatoj zgradi od trenutno odgovorne institucije)
        * u slucaju kada ne postoji institucija sa zadatim ID-em
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(103L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // ne postoji institucija sa ID-em = 200
        this.mockMvc.perform(patch(URL_PREFIX + "/institutions/200/damage_types/1/buildings/100/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenInstitution)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDelegateResponsibilityToInstitutionForDamageTypeInvalidInstitution() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToInstitutionForDamageType kontrolera DamageTypeController
        * (delegiranje odgovornosti nekoj drugoj instituciji za zadati tip kvara u zadatoj zgradi od trenutno odgovorne institucije)
        * u slucaju kada trenutno ulogovana institucija nema ID koji je zadat u URL-u
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(103L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // trenutno ulogovana institucija ima ID = 102 (ne ID = 103)
        this.mockMvc.perform(patch(URL_PREFIX + "/institutions/103/damage_types/1/buildings/100/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenInstitution)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDelegateResponsibilityToInstitutionForDamageTypeNonexistentDamageType() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToInstitutionForDamageType kontrolera DamageTypeController
        * (delegiranje odgovornosti nekoj drugoj instituciji za zadati tip kvara u zadatoj zgradi od trenutno odgovorne institucije)
        * u slucaju kada ne postoji tip kvara sa zadatim ID-em
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(103L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // ne postoji tip kvara sa ID-em = 100
        this.mockMvc.perform(patch(URL_PREFIX + "/institutions/102/damage_types/100/buildings/100/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenInstitution)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDelegateResponsibilityToInstitutionForDamageTypeNonexistentBuilding() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToInstitutionForDamageType kontrolera DamageTypeController
        * (delegiranje odgovornosti nekoj drugoj instituciji za zadati tip kvara u zadatoj zgradi od trenutno odgovorne institucije)
        * u slucaju kada ne postoji zgrada sa zadatim ID-em
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(103L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // ne postoji zgrada sa ID-em = 200
        this.mockMvc.perform(patch(URL_PREFIX + "/institutions/102/damage_types/1/buildings/200/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenInstitution)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDelegateResponsibilityToInstitutionForDamageTypeNonexistentResponsibility() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToInstitutionForDamageType kontrolera DamageTypeController
        * (delegiranje odgovornosti nekoj drugoj instituciji za zadati tip kvara u zadatoj zgradi od trenutno odgovorne institucije)
        * u slucaju kada trenutno ulogovana institucija nije odgovorna za zadati tip kvara u zadatoj zgradi
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(103L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // ne postoji odgovornost institucije sa ID-em = 102 za tip kvara sa ID-em = 5 u zgradi sa ID-em = 100
        this.mockMvc.perform(patch(URL_PREFIX + "/institutions/102/damage_types/5/buildings/100/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenInstitution)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDelegateResponsibilityToInstitutionForDamageTypeNonexistentInstitutionFromDTO() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToInstitutionForDamageType kontrolera DamageTypeController
        * (delegiranje odgovornosti nekoj drugoj instituciji za zadati tip kvara u zadatoj zgradi od trenutno odgovorne institucije)
        * u slucaju kada ne postoji institucija sa ID-em zadatim u DTO-u (nova odgovorna institucija)
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(200L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // ne postoji institucija sa ID-em = 200
        this.mockMvc.perform(patch(URL_PREFIX + "/institutions/102/damage_types/1/buildings/100/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenInstitution)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDelegateResponsibilityToInstitutionForDamageTypeNotInstitution() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToInstitutionForDamageType kontrolera DamageTypeController
        * (delegiranje odgovornosti nekoj drugoj instituciji za zadati tip kvara u zadatoj zgradi od trenutno odgovorne institucije)
        * u slucaju kada torka kojoj pripada ID zadat u DTO-u nije institucija nego firma
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(100L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // business torka sa ID-em = 100 nije institucija, nego firma
        this.mockMvc.perform(patch(URL_PREFIX + "/institutions/102/damage_types/1/buildings/100/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenInstitution)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testDelegateResponsibilityToInstitutionForDamageTypeMissingId() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToInstitutionForDamageType kontrolera DamageTypeController
        * (delegiranje odgovornosti nekoj drugoj instituciji za zadati tip kvara u zadatoj zgradi od trenutno odgovorne institucije)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne poseduje sva neophodna polja - nedostaje polje id (nove odgovorne institucije)
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO();
        // nedostaje polje id
        responsibilityDTO.setId(null);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        this.mockMvc.perform(patch(URL_PREFIX + "/institutions/102/damage_types/1/buildings/100/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenInstitution)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testDelegateResponsibilityToInstitutionForDamageTypeInappropriateTypeOfId() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToInstitutionForDamageType kontrolera DamageTypeController
        * (delegiranje odgovornosti nekoj drugoj instituciji za zadati tip kvara u zadatoj zgradi od trenutno odgovorne institucije)
        * u slucaju kada je dolazni DTO objekat nevalidan: poseduje sva neophodna polja - ali je polje id nevalidnog tipa
        */

        // polje id nevalidnog tipa
        String responsibilityDTOJson = "{id:'greska'}";

        this.mockMvc.perform(patch(URL_PREFIX + "/institutions/102/damage_types/1/buildings/100/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenInstitution)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za delegateResponsibilityToPersonForDamageType
    @Test
    @Transactional
    @Rollback(true)
    public void testDelegateResponsibilityToPersonForDamageType() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToPersonForDamageType kontrolera DamageTypeController
        * (delegiranje odgovornosti nekom drugom stanaru za zadati tip kvara u zadatoj zgradi od trenutno odgovornog lica - stanara)
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(103L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        /*
        u zgradi sa ID-em = 100 postoji odgovorna osoba (stanar) za tip kvara sa ID-em = 1 (PLUMBING_DAMAGE),
        pri cemu je odgovorni stanar opisan sledecim podacima:
            id = 101,
            firstName = Katarina,
            lastName = Cukurov,
            apartment_id = 101 (building_id = 100)
        */
        this.mockMvc.perform(patch(URL_PREFIX + "/tenants/101/damage_types/1/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.tenantId").value(103))
                .andExpect(jsonPath("$.damageTypeId").value(1))
                .andExpect(jsonPath("$.buildingId").value(100));
    }

    @Test
    public void testDelegateResponsibilityToPersonForDamageTypeByNotAuthenticatedUser() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToPersonForDamageType kontrolera DamageTypeController
        * (delegiranje odgovornosti nekom drugom stanaru za zadati tip kvara u zadatoj zgradi od trenutno odgovornog lica - stanara)
        * u slucaju kada akciju obavlja nelogovani korisnik
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(103L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        this.mockMvc.perform(patch(URL_PREFIX + "/tenants/101/damage_types/1/delegate_responsibility")
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDelegateResponsibilityToPersonForDamageTypeByNotAuthenticatedTenant() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToPersonForDamageType kontrolera DamageTypeController
        * (delegiranje odgovornosti nekom drugom stanaru za zadati tip kvara u zadatoj zgradi od trenutno odgovornog lica - stanara)
        * u slucaju kada akciju obavlja logovani korisnik koji nema ulogu stanara
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(103L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        this.mockMvc.perform(patch(URL_PREFIX + "/tenants/101/damage_types/1/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenInstitution)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDelegateResponsibilityToPersonForDamageTypeNonexistentTenant() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToPersonForDamageType kontrolera DamageTypeController
        * (delegiranje odgovornosti nekom drugom stanaru za zadati tip kvara u zadatoj zgradi od trenutno odgovornog lica - stanara)
        * u slucaju kada ne postoji stanar sa zadatim ID-em
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(103L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // ne postoji stanar sa ID-em = 200
        this.mockMvc.perform(patch(URL_PREFIX + "/tenants/200/damage_types/1/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDelegateResponsibilityToPersonForDamageTypeInvalidTenant() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToPersonForDamageType kontrolera DamageTypeController
        * (delegiranje odgovornosti nekom drugom stanaru za zadati tip kvara u zadatoj zgradi od trenutno odgovornog lica - stanara)
        * u slucaju kada trenutno ulogovan stanar nema ID koji je zadat u URL-u
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(103L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // trenutno logovani stanar ima ID = 103
        this.mockMvc.perform(patch(URL_PREFIX + "/tenants/101/damage_types/1/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDelegateResponsibilityToPersonForDamageTypeNonexistentDamageType() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToPersonForDamageType kontrolera DamageTypeController
        * (delegiranje odgovornosti nekom drugom stanaru za zadati tip kvara u zadatoj zgradi od trenutno odgovornog lica - stanara)
        * u slucaju kada ne postoji tip kvara sa zadatim ID-em
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(103L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // ne postoji tip kvara sa ID-em = 100
        this.mockMvc.perform(patch(URL_PREFIX + "/tenants/101/damage_types/100/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDelegateResponsibilityToPersonForDamageTypeNonexistentResponsibility() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToPersonForDamageType kontrolera DamageTypeController
        * (delegiranje odgovornosti nekom drugom stanaru za zadati tip kvara u zadatoj zgradi od trenutno odgovornog lica - stanara)
        * u slucaju kada trenutno ulogovan stanar nije odgovoran za zadati tip kvara u sopstvenoj zgradi
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(103L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // ne postoji odgovornost stanara sa ID-em = 101 za tip kvara sa ID-em = 5 u zgradi sa ID-em = 100
        this.mockMvc.perform(patch(URL_PREFIX + "/tenants/101/damage_types/5/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDelegateResponsibilityToPersonForDamageTypeNonexistentTenantFromDTO() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToPersonForDamageType kontrolera DamageTypeController
        * (delegiranje odgovornosti nekom drugom stanaru za zadati tip kvara u zadatoj zgradi od trenutno odgovornog lica - stanara)
        * u slucaju kada ne postoji stanar sa ID-em zadatim u DTO-u (novo odgovorno lice - stanar)
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(200L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // ne postoji stanar sa ID-em = 200
        this.mockMvc.perform(patch(URL_PREFIX + "/tenants/101/damage_types/1/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDelegateResponsibilityToPersonForDamageTypeTenantFromInvalidBuilding() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToPersonForDamageType kontrolera DamageTypeController
        * (delegiranje odgovornosti nekom drugom stanaru za zadati tip kvara u zadatoj zgradi od trenutno odgovornog lica - stanara)
        * u slucaju kada stanar ciji je ID zadat u DTO-u ne zivi u istoj zgradi kao i trenutno logovan stanar
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(107L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // stanar sa ID-em = 107 ne zivi u istoj zgradi kao i trenutno ulogovani stanar (vec zivi u zgradi sa ID-em = 101)
        this.mockMvc.perform(patch(URL_PREFIX + "/tenants/101/damage_types/1/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testDelegateResponsibilityPersonForDamageTypeMissingId() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToPersonForDamageType kontrolera DamageTypeController
        * (delegiranje odgovornosti nekom drugom stanaru za zadati tip kvara u zadatoj zgradi od trenutno odgovornog lica - stanara)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne poseduje sva neophodna polja - nedostaje polje id (novog odgovornog lica - stanara)
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO();
        // nedostaje polje id
        responsibilityDTO.setId(null);

        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        this.mockMvc.perform(patch(URL_PREFIX + "/tenants/101/damage_types/1/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testDelegateResponsibilityToPersonForDamageTypeInappropriateTypeOfId() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToPersonForDamageType kontrolera DamageTypeController
        * (delegiranje odgovornosti nekom drugom stanaru za zadati tip kvara u zadatoj zgradi od trenutno odgovornog lica - stanara)
        * u slucaju kada je dolazni DTO objekat nevalidan: poseduje sva neophodna polja - ali je polje id nevalidnog tipa
        */

        // nevalidan tip polja id
        String responsibilityDTOJson = "{id:'greska'}";

        this.mockMvc.perform(patch(URL_PREFIX + "/tenants/101/damage_types/1/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za getAllDamageTypeResponsibilityForTenant
    @Test
    public void testGetAllDamageTypeResponsibilityForTenant() throws Exception{
        /*
        * Test proverava ispravnost metode getAllDamageTypeResponsibilityForTenant kontrolera DamageTypeController
        * (vraca listu svih tipova kvara za koje je stanar sa zadatim ID-em odgovoran u sklopu zgrade u kojoj zivi)
        * */

        /*
        stanar sa ID-em = 103 je zaduzen za tip kvara sa ID-em = 4 i nazivom EXTERIOR_DAMAGE
        */
        mockMvc.perform(get("/api/buildings/100/tenants/103/responsible_for_damage_types")
                .params(params)
                .header("Authentication-Token", this.accessTokenTenant))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.[*]", hasSize(1)))
                .andExpect(jsonPath("$.[0].id").value(103))
                .andExpect(jsonPath("$.[0].tenantId").value(103))
                .andExpect(jsonPath("$.[0].tenantName").value("Jasmina Markovic"))
                .andExpect(jsonPath("$.[0].damageTypeId").value(4))
                .andExpect(jsonPath("$.[0].damageTypeName").value("EXTERIOR_DAMAGE"))
                .andExpect(jsonPath("$.[0].buildingId").value(100));
    }

    // TESTOVI za getAllDamageTypeResponsibilityForInstitution
    @Test
    public void testGetAllDamageTypeResponsibilityForInstitution() throws Exception{
        /*
        * Test proverava ispravnost metode getAllDamageTypeResponsibilityForInstitution kontrolera DamageTypeController
        * (vraca listu svih tipova kvara za koje je institucija sa zadatim ID-em odgovorna u bilo kojoj od zgrada
        * koje su registrovane u sistemu)
        * */

        /*
        institucija sa ID-em = 102 je zaduzena za tip kvara sa ID-em = 1 i nazivom PLUMBING_DAMAGE u zgradi sa ID-em = 100
        */
        mockMvc.perform(get("/api/institutions/102/responsible_for_damage_types")
                .params(params)
                .header("Authentication-Token", this.accessTokenInstitution))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.[*]", hasSize(1)))
                .andExpect(jsonPath("$.[0].id").value(100))
                .andExpect(jsonPath("$.[0].institutionId").value(102))
                .andExpect(jsonPath("$.[0].tenantId").value(101))
                .andExpect(jsonPath("$.[0].tenantName").value("Katarina Cukurov"))
                .andExpect(jsonPath("$.[0].damageTypeId").value(1))
                .andExpect(jsonPath("$.[0].damageTypeName").value("PLUMBING_DAMAGE"))
                .andExpect(jsonPath("$.[0].buildingId").value(100));
    }

    // TESTOVI za getAllDamageTypeResponsibilityinBuilding
    @Test
    public void testGetAllDamageTypeResponsibilityinBuilding() throws Exception {
        /*
        * Test proverava ispravnost metode getAllDamageTypeResponsibilityinBuilding kontrolera DamageTypeController
        * (vraca listu svih tipova kvara koji postoje u zgradi sa zadatim ID-em)
        * */

        /*
        u zgradi sa ID-em = 100 postoje sledeci tipovi kvara i odgovornosti za iste:
            1 - id = 100,
                damageTypeName = PLUMBING_DAMAGE,
                tenantId = 101,
                tenantName = Katarina Cukurov,
                institutionID = 102
                damageTypeId = 1
            2 - id = 103,
                damageTypeName = EXTERIOR_DAMAGE,
                tenantId = 103,
                tenantName = Jasmina Markovic,
                damageTypeId = 4
            3 - id = 101,
                damageTypeName = GAS_DAMAGE,
                tenantId = 102,
                tenantName = Nikola Garabandic,
                damageTypeId = 3
            4 - id = 102,
                damageTypeName = ELECTRICAL_DAMAGE,
                institutionID = 103
                damageTypeId = 2
        */
        mockMvc.perform(get("/api/buildings/100/damage_types_responsibilities")
                .params(params)
                .header("Authentication-Token", this.accessTokenPresident))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.[*]", hasSize(4)))
                .andExpect(jsonPath("$.[*].id", containsInAnyOrder(100, 101, 102, 103)))
                .andExpect(jsonPath("$.[*].damageTypeId", containsInAnyOrder(1, 2, 3, 4)))
                .andExpect(jsonPath("$.[*].damageTypeName",
                        containsInAnyOrder("PLUMBING_DAMAGE", "GAS_DAMAGE", "EXTERIOR_DAMAGE", "ELECTRICAL_DAMAGE")))
                .andExpect(jsonPath("$.[*].buildingId", contains(100,100,100,100)));
    }
}
