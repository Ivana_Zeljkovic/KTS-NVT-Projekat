package rs.ac.uns.ftn.ktsnvt.web.dto;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Ivana Zeljkovic on 13/11/2017.
 */
public class QuestionCreateDTO implements Serializable {

    @NotNull
    @NotEmpty
    private String content;
    @NotNull
    @Size(min = 1)
    private List<String> answers;

    public QuestionCreateDTO() { }

    public QuestionCreateDTO(String content, List<String> answers) {
        this.content = content;
        this.answers = answers;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<String> getAnswers() {
        return answers;
    }

    public void setAnswers(List<String> answers) {
        this.answers = answers;
    }
}
