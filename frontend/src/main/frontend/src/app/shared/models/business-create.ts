import { AddressCreate } from "./address-create";
import { Login } from "./login";

export class BusinessCreate {
  constructor(private name: string, private description: string,
              private phoneNumber: string, private email: string,
              private businessType: string, private address: AddressCreate,
              private loginAccounts: Array<Login>) {
  }
}
