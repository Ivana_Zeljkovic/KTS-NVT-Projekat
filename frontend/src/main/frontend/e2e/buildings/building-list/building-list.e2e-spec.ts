import {browser, by, element} from "protractor";
// page
import {LoginPage} from "../../page-objects/login.po";
import {ConfirmDeletePage} from "../../page-objects/confirm-delete.po";
import {BuildingListPage} from "./page-objects/building-list.po";
import {BuildingRegistrationPage} from "../building-registration/page-objects/building-registration.po";
import {AddApartmentModalPage} from "../../shared/add-apartment-modal/page-objects/add-apartment-modal.po";
import {ChooseManagerModalPage} from "../../shared/choose-manager-modal/page-objects/choose-manager-modal.po";
import {ChoosePresidentModalPage} from "../../shared/choose-president-modal/page-objects/choose-president-modal.po";

describe('Buildings list view', () => {
  let loginPage: LoginPage;
  let buildingListPage: BuildingListPage;
  let confirmDeletePage: ConfirmDeletePage;
  let buildingRegistrationPage: BuildingRegistrationPage;
  let addApartmentModalPage: AddApartmentModalPage;
  let chooseManagerModalPage: ChooseManagerModalPage;
  let choosePresidentModalPage: ChoosePresidentModalPage;
  let empty: any;

  beforeEach(() => {
    loginPage = new LoginPage();
    buildingListPage = new BuildingListPage();
    confirmDeletePage = new ConfirmDeletePage();
    browser.get("/");

    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');
    loginPage.setUsername('ivana');
    loginPage.setPassword('ivana');
    loginPage.getSignInButton().click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/home');
    browser.get('http://localhost:49152/#/buildings');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/buildings');

  });

  afterEach(() => {
    browser.executeScript('window.localStorage.clear();');
  });

  it("should successfully load page and all of its components", () => {
    expect(buildingListPage.getPageTitle().isDisplayed()).toBe(true);
    expect(buildingListPage.getCityLabel().isDisplayed()).toBe(true);
    expect(buildingListPage.getSerachInput().isDisplayed()).toBe(true);
    expect(buildingListPage.getSearchButton().isDisplayed()).toBe(true);
    expect(buildingListPage.getAddButton().isDisplayed()).toBe(true);
    expect(buildingListPage.getBuildingsTable().isDisplayed()).toBe(true);
    expect(buildingListPage.getPagination().isDisplayed()).toBe(true);
    expect(buildingListPage.getToaster().isDisplayed()).toBe(true);

    expect(buildingListPage.getSearchButton().isEnabled()).toBe(true);
    expect(buildingListPage.getAddButton().isEnabled()).toBe(true);

  });

  it('should open page with more details about building when click on button', () => {
    let moreButton = element(by.xpath("//tr[1]/td[6]")).element(by.css('.btn.btn-default'));
    moreButton.click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/buildings/104');
  });

  it('should filter buildings by city name when there are buildings in city', () => {
    buildingListPage.setSeachInput('Novi Sad');
    buildingListPage.getSearchButton().click();

    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/buildings');
    expect(buildingListPage.getNoElements().isPresent()).toBe(false);
    element.all(by.tagName('tr')).then(function (elems) {
      expect(elems.length == 5);
    });

  });

  it('should filter buildings by city name when there are no buildings in city', () => {
    buildingListPage.setSeachInput('Beograd');
    buildingListPage.getSearchButton().click();

    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/buildings');
    expect(buildingListPage.getNoElements().isDisplayed()).toBe(true);
  });

  it('should register new building and add it to table when click on add button', () => {
    let oldId = element(by.xpath("//tr[last()]/td[1]")).getText();
    buildingListPage.getAddButton().click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/buildings/register');

    buildingRegistrationPage = new BuildingRegistrationPage();
    addApartmentModalPage = new AddApartmentModalPage();
    chooseManagerModalPage = new ChooseManagerModalPage();

    buildingRegistrationPage.getDisplayAddApartment().click().then(function () {
      addApartmentModalPage = new AddApartmentModalPage();
      addApartmentModalPage.setNumber(1);
      addApartmentModalPage.setSurface(50);
      addApartmentModalPage.setFloor(1);
      addApartmentModalPage.getConfirmButton().click();
      addApartmentModalPage.getCloseButton().click();
    });

    buildingRegistrationPage.setNumber("3");
    buildingRegistrationPage.setCity("Novi Sad");
    buildingRegistrationPage.setPostalCode("21000");
    buildingRegistrationPage.setStreet("Anotna Cehova");

    expect(buildingRegistrationPage.getSaveButton().isEnabled()).toBe(true);

    buildingRegistrationPage.getSaveButton().click().then(function () {
      expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/buildings/register');
      expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/home');
    });

    browser.get('http://localhost:49152/#/buildings');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/buildings');
    let newId = element(by.xpath("//tr[last()]/td[1]")).getText();
    expect(oldId != newId);
  });

  it("should delete building and remove it from table", () => {
    let oldId = element(by.xpath("//tr[last()]/td[1]")).getText();
    let deleteButton = element(by.xpath("//tr[last()]/td[5]")).element(by.css('.btn.btn-danger'));
    deleteButton.click();

    browser.wait(function() {

      return confirmDeletePage.getConfirmDeleteElement().isDisplayed();
    }).then(function() {

      expect(confirmDeletePage.getConfirmButton().isDisplayed()).toBe(true);
      expect(confirmDeletePage.getConfirmButton().isEnabled()).toBe(true);
      expect(confirmDeletePage.getDeclineButton().isDisplayed()).toBe(true);
      expect(confirmDeletePage.getDeclineButton().isEnabled()).toBe(true);

      confirmDeletePage.getConfirmButton().click();
      expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/buildings');
      let newId = element(by.xpath("//tr[last()]/td[1]")).getText();
      expect(oldId != newId);
    });
  });

  it('should open modal for choosing president and choose one', () => {
    let choose = element(by.xpath("//tr[3]/td[7]")).element(by.css('.btn.btn-success'));
    choose.click().then(function () {
      choosePresidentModalPage = new ChoosePresidentModalPage();
      let president = element.all(by.xpath("//tr[2]/td[4]")).get(1).element(by.css('.btn.btn-info'));
      president.click();
    });
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/buildings');
    empty = {'empty':''};
    let ch = element(by.xpath("//tr[3]/td[7]"));
    expect(ch == empty.empty);
  });
});
