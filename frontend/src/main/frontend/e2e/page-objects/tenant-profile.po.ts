import {by, element, ElementFinder, protractor} from "protractor";

export class TenantProfilePage {
  // get methods
  getTenantProfileElement(): ElementFinder {
    return element(by.tagName('app-tenant-profile'));
  }

  getTenantStreetAndNumber(): ElementFinder {
    return element(by.css('.container > div:nth-child(1) .panel .panel-heading > div:nth-child(2) > div:nth-child(1)'));
  }

  getTenantCity(): ElementFinder {
    return element(by.css('.container > div:nth-child(1) .panel .panel-heading > div:nth-child(2) > div:nth-child(2)'));
  }

  getTenantApartment(): ElementFinder {
    return element(by.css('.container > div:nth-child(1) .panel .panel-heading > div:nth-child(2) > div:nth-child(3)'));
  }

  getMap(): ElementFinder {
    return element(by.css('.container > div:nth-child(1) .panel .panel-body'));
  }

  getFirstName(): ElementFinder {
    return element(by.id('firstName'));
  }

  getLastName(): ElementFinder {
    return element(by.id('lastName'));
  }

  getEmail(): ElementFinder {
    return element(by.id('email'));
  }

  getPhoneNumber(): ElementFinder {
    return element(by.id('phoneNumber'));
  }

  getBirthDate(): ElementFinder {
    return element(by.id('birthDate'));
  }

  getUsername(): ElementFinder {
    return element(by.id('username'));
  }

  getCurrentPassword(): ElementFinder {
    return element(by.id('currentPassword'));
  }

  getPassword(): ElementFinder {
    return element(by.id('password'));
  }

  getConfirmPassword(): ElementFinder {
    return element(by.id('confirmPassword'));
  }

  getSaveButton(): ElementFinder {
    return element(by.buttonText('Save'));
  }

  getResetButton(): ElementFinder {
    return element(by.buttonText('Reset'));
  }

  // get error methods
  getFirstNameRequiredError(): ElementFinder {
    return element(by.id('firstNameRequiredError'));
  }

  getLastNameRequiredError(): ElementFinder {
    return element(by.id('lastNameRequiredError'));
  }

  getEmailRequiredError(): ElementFinder {
    return element(by.id('emailRequiredError'));
  }

  getPhoneNumberRequiredError(): ElementFinder {
    return element(by.id('phoneNumberRequiredError'));
  }

  getBirthDateRequiredError(): ElementFinder {
    return element(by.id('birthDateRequiredError'));
  }

  getCurrentPasswordRequiredError(): ElementFinder {
    return element(by.id('currentPasswordRequiredError'));
  }

  getPasswordRequiredError(): ElementFinder {
    return element(by.id('passwordRequiredError'));
  }

  getConfirmPasswordRequiredError(): ElementFinder {
    return element(by.id('confirmPasswordRequiredError'));
  }

  getFirstNameMinLengthError(): ElementFinder {
    return element(by.id('firstNameMinLengthError'));
  }

  getLastNameMinLengthError(): ElementFinder {
    return element(by.id('lastNameMinLengthError'));
  }

  getPasswordMinLengthError(): ElementFinder {
    return element(by.id('passwordMinLengthError'));
  }

  getEmailPatternError(): ElementFinder {
    return element(by.id('emailPatternError'));
  }

  getPhoneNumberSpaceError(): ElementFinder {
    return element(by.id('phoneNumberSpaceError'));
  }

  getPhoneNumberPatternError(): ElementFinder {
    return element(by.id('phoneNumberPatternError'));
  }

  getPasswordDontMatchError(): ElementFinder {
    return element(by.id('passwordDontMatchError'));
  }

  // set methods
  setFirstName(text: string) {
    this.getFirstName().sendKeys(text);
  }

  setLastName(text: string) {
    this.clearField(this.getLastName());
    this.getLastName().sendKeys(text);
  }

  setEmail(text: string) {
    this.getEmail().sendKeys(text);
  }

  setPhoneNumber(text: string) {
    this.getPhoneNumber().sendKeys(text);
  }

  setCurrentPassword(text: string) {
    this.getCurrentPassword().sendKeys(text);
  }

  setPassword(text: string) {
    this.getPassword().sendKeys(text);
  }

  setConfirmPassword(text: string) {
    this.getConfirmPassword().sendKeys(text);
  }

  clearField(field) {
    field.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, "a"));
    field.sendKeys(protractor.Key.BACK_SPACE);
    field.clear();
  }
}
