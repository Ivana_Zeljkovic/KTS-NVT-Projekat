import {browser} from "protractor";
// page
import {LoginPage} from "../../page-objects/login.po";
import {AdminProfilePage} from "./page-objects/admin-profile.po";


describe('Admin profile', () => {

  let adminProfilePage: AdminProfilePage;
  let loginPage: LoginPage;


  beforeAll(() => {
    loginPage = new LoginPage();
    browser.get("http://localhost:49152/#/login?returnUrl=%2Fprofile");
    loginPage.setUsername('ivana');
    loginPage.setPassword('ivana');
    loginPage.getSignInButton().submit();

    adminProfilePage = new AdminProfilePage();
    browser.wait(function () {
      return browser.getCurrentUrl().then(value => {
        return value == 'http://localhost:49152/#/profile'
      });
    });

  });

  beforeEach(() => {
    adminProfilePage = new AdminProfilePage();
  });

  afterEach(() => {

    if (adminProfilePage.getResetButton().isEnabled())//only in first test this button will be disabled.
      adminProfilePage.getResetButton().click();

  });

  afterAll(() => {
    browser.executeScript('window.localStorage.clear();');
  });

  it('Should display all inputs of business profile correctly', () => {
    expect(adminProfilePage.getPanelHeading().isDisplayed()).toBe(true);
    expect(adminProfilePage.getSaveButton().isDisplayed()).toBe(true);
    expect(adminProfilePage.getResetButton().isDisplayed()).toBe(true);

    expect(adminProfilePage.getUsernameInput().isDisplayed()).toBe(true);
    expect(adminProfilePage.getFirstNameInput().isDisplayed()).toBe(true);
    expect(adminProfilePage.getLastNameInput().isDisplayed()).toBe(true);

    expect(adminProfilePage.getCurrentPasswordInput().isDisplayed()).toBe(true);
    expect(adminProfilePage.getNewPasswordInput().isDisplayed()).toBe(true);
    expect(adminProfilePage.getConfirmPasswordInput().isDisplayed()).toBe(true);
  });


  it('Should revert values back to original when reset button is clicked', () => {
    let name = adminProfilePage.getFirstNameInput().getAttribute('value');
    adminProfilePage.setFirstName('fasafs');
    let newName = adminProfilePage.getFirstNameInput().getAttribute('value');

    expect(adminProfilePage.getFirstNameInput().getAttribute('value')).toEqual(newName);

    adminProfilePage.getResetButton().click().then(function () {
      expect(adminProfilePage.getFirstNameInput().getAttribute('value')).toEqual(name);
    })

  });

  it('Should display error about first name required when field is empty and lost focus', () => {
    adminProfilePage.setFirstName('');
    adminProfilePage.getLastNameInput().click();
    expect(adminProfilePage.getFirstNameRequired().isDisplayed()).toBe(true);
  });

  it('Should display error about first name minimum length', () => {
    adminProfilePage.setFirstName('a');
    adminProfilePage.getLastNameInput().click();
    expect(adminProfilePage.getfirstNameMinLengthError().isDisplayed()).toBe(true);

  });

  it('Should display error about last name required when field is empty and lost focus', () => {
    adminProfilePage.setLastName('');
    adminProfilePage.getLastNameInput().click();
    expect(adminProfilePage.getLastNameRequired().isDisplayed()).toBe(true);
  });

  it('Should display error about last name minimum length', () => {
    adminProfilePage.setLastName('a');
    adminProfilePage.getLastNameInput().click();
    expect(adminProfilePage.getLastNameMinLengthError().isDisplayed()).toBe(true);

  });

  it('Should display new password\'s minimum length is 5 characters', () => {
    adminProfilePage.setNewPassword('a');
    adminProfilePage.getFirstNameInput().click();
    expect(adminProfilePage.getNewPasswordLength().isDisplayed()).toBe(true);
  });

  it('Should display current password is required when new password with valid length is entered', () => {
    adminProfilePage.setNewPassword('123341a');
    adminProfilePage.getFirstNameInput().click();
    expect(adminProfilePage.getCurrentPasswordRequired().isDisplayed()).toBe(true);
  });

  it('Should display confirm password is required when new password with valid length is entered', () => {
    adminProfilePage.setNewPassword('123341a');
    adminProfilePage.getFirstNameInput().click();
    expect(adminProfilePage.getConfirmPasswordRequired().isDisplayed()).toBe(true);
  });

  it('Should display passwords don\'t match when new password and confirm password input fields have different content', () => {
    adminProfilePage.setNewPassword('123341a');
    adminProfilePage.setConfirmPassword('123341');
    adminProfilePage.getFirstNameInput().click();
    expect(adminProfilePage.getConfirmPasswordMatch().isDisplayed()).toBe(true);
  });

  it('Should enable reset and save button when new data is entered in any of the fields', () => {
    adminProfilePage.setCurrentPassword('new');
    expect(adminProfilePage.getResetButton().isEnabled()).toBe(true);
    expect(adminProfilePage.getSaveButton().isEnabled()).toBe(true);
  });

});


