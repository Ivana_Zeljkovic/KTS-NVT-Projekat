package rs.ac.uns.ftn.ktsnvt.service;

import rs.ac.uns.ftn.ktsnvt.model.entity.DamageType;

/**
 * Created by Ivana Zeljkovic on 23/11/2017.
 */
public interface DamageTypeService {

    DamageType findOne(Long id);

    DamageType findByName(String name);
}
