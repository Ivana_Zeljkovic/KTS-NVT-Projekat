package rs.ac.uns.ftn.ktsnvt.service;

import rs.ac.uns.ftn.ktsnvt.model.entity.Meeting;
import rs.ac.uns.ftn.ktsnvt.model.entity.Questionnaire;

/**
 * Created by Ivana Zeljkovic on 13/11/2017.
 */
public interface QuestionnaireService {

    Questionnaire save(Questionnaire questionnaire);

    Questionnaire findOne(Long id);

    void checkQuestionnaireExpired(Questionnaire questionnaire);

    void remove(Long questionnaireId);

    void isVoteEnabled(Questionnaire questionnaire);

    void checkBuildingForQuestionnaire(Long questionnaireId, Long billboardId);

    Questionnaire setDateExpired(Questionnaire questionnaire, Meeting meeting);
}
