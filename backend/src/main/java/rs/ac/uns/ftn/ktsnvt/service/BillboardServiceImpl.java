package rs.ac.uns.ftn.ktsnvt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.model.entity.Billboard;
import rs.ac.uns.ftn.ktsnvt.repository.BillboardRepository;

/**
 * Created by Ivana Zeljkovic on 13/11/2017.
 */
@Service
public class BillboardServiceImpl implements BillboardService {

    @Autowired
    private BillboardRepository billboardRepository;


    @Override
    @Transactional(readOnly = false)
    public Billboard save(Billboard billboard) {
        return this.billboardRepository.save(billboard);
    }

}
