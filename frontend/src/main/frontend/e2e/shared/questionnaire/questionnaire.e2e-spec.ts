import { browser} from "protractor";
// page
import { MeetingItemPage } from "../../page-objects/meeting-item.po";
import { QuestionnairePage } from "../../page-objects/questionnaire.po";
import { QuestionPage } from "../../page-objects/question.po";
import { LoginPage } from "../../page-objects/login.po";
import { MeetingItemListPage } from "../../home/tenant-home/page-objects/meeting-item-list.po";


describe('Questionnaire page', () => {
  let meetingItemPage: MeetingItemPage;
  let meetingItemListPage: MeetingItemListPage;
  let questionnairePage: QuestionnairePage;
  let questionPage: QuestionPage;
  let loginPage: LoginPage;
  let newQuestion: any;
  let numberOfQuestions: any;

  beforeAll(() => {
    loginPage = new LoginPage();
    meetingItemListPage = new MeetingItemListPage();
    meetingItemPage = new MeetingItemPage();
    questionnairePage = new QuestionnairePage();
    questionPage = new QuestionPage();
    newQuestion = {'content':'First question?', 'answers':['First answer','Second answer']};
    numberOfQuestions = {'value':0};

    browser.get('/');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');

    loginPage.setUsername('kaca');
    loginPage.setPassword('kaca');
    loginPage.getSignInButton().click();

    browser.wait(function() {
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/home/notifications';
      });
    }).then(function() {
      browser.get('http://localhost:49152/#/home/meeting-details-items');

      // expect to redirect on page for review list of meeting items
      expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/home/meeting-details-items');
      expect(meetingItemListPage.getMeetingItemListElement().isDisplayed()).toBe(true);
    });
  });

  afterAll(() => {
    browser.executeScript('window.localStorage.clear();');
  });



  it('should open modal dialog and render all its elements - questionnaire page, when user click ' +
    'on Questionnaire link in meeting item modal dialog', () => {
    expect(meetingItemListPage.getAddButton().isDisplayed()).toBe(true);
    meetingItemListPage.getAddButton().click();

    browser.wait(function() {
      // expect to show meeting item modal page when user click on button
      return meetingItemPage.getMeetingItemElement().isDisplayed();
    }).then(function() {
      expect(meetingItemPage.getQuestionnaireLink().isDisplayed()).toBe(true);
      meetingItemPage.getQuestionnaireLink().click();

      browser.wait(function() {
        return questionnairePage.getQuestionnaireElement().isDisplayed();
      }).then(function() {
        // expect that questionnaire page has all elements
        expect(questionnairePage.getTitleFromHeader().getText()).toEqual('Questionnaire in meeting item');
        expect(questionnairePage.getSaveButton().isDisplayed()).toBe(true);
        expect(questionnairePage.getSaveButton().isEnabled()).toBe(false);
        expect(questionnairePage.getResetButton().isDisplayed()).toBe(true);
        expect(questionnairePage.getResetButton().isEnabled()).toBe(false);
        expect(questionnairePage.getCancelButton().isDisplayed()).toBe(true);
        expect(questionnairePage.getCancelButton().isEnabled()).toBe(true);
        expect(questionnairePage.getLinkForCreateQuestion().isDisplayed()).toBe(true);
      });
    });
  });



  it('should open modal for creating new question when user click on Create question link', () => {
    expect(questionnairePage.getLinkForCreateQuestion().isDisplayed()).toBe(true);
    questionnairePage.getLinkForCreateQuestion().click();

    browser.wait(function() {
      return questionPage.getQuestionElement().isDisplayed();
    }).then(function() {
      expect(questionPage.getContentInput().isDisplayed()).toBe(true);
      expect(questionPage.getAnswerInput().isDisplayed()).toBe(true);
      expect(questionPage.getAddAnswerButton().isDisplayed()).toBe(true);
      expect(questionPage.getAddAnswerButton().isEnabled()).toBe(false);
      expect(questionPage.getSaveButton().isDisplayed()).toBe(true);
      expect(questionPage.getSaveButton().isEnabled()).toBe(false);
      expect(questionPage.getResetButton().isDisplayed()).toBe(true);
      expect(questionPage.getResetButton().isEnabled()).toBe(false);
      expect(questionPage.getCancelButton().isDisplayed()).toBe(true);
      expect(questionPage.getCancelButton().isEnabled()).toBe(true);
    });
  });



  it('should show error message for required content and more then one answer for question, enable reset ' +
    'but still disable save button when user add only one answer in question page', () => {
    questionPage.setContent('');
    questionPage.setNewAnswer('first answer');

    // expect to enable add answer button
    expect(questionPage.getAddAnswerButton().isEnabled()).toBe(true);
    questionPage.getAddAnswerButton().click();

    expect(questionPage.getAnswersElement().isDisplayed()).toBe(true);
    questionPage.getAllAnswers().count().then(value => {
      // expect to have one answer in list
      expect(value).toEqual(1);
    });
    expect(questionPage.getContentRequiredError().isDisplayed()).toBe(true);
    expect(questionPage.getContentRequiredError().getText()).toEqual('The content is required!');
    expect(questionPage.getOnlyOneAnswerError().isDisplayed()).toBe(true);
    expect(questionPage.getOnlyOneAnswerError().getText()).toEqual('Question must have at least 2 answers!');
    expect(questionPage.getSaveButton().isEnabled()).toBe(false);
    expect(questionPage.getResetButton().isEnabled()).toBe(true);
  });



  it('should reset content value and remove all answers when user click on reset button', () => {
    expect(questionPage.getResetButton().isEnabled()).toBe(true);
    questionPage.getResetButton().click();

    expect(questionPage.getContentInput().getAttribute('value')).toEqual('');
    expect(questionPage.getAddAnswerButton().isEnabled()).toBe(false);
    expect(questionPage.getSaveButton().isEnabled()).toBe(false);
    expect(questionPage.getResetButton().isEnabled()).toBe(false);
  });



  it('should add new question in question list in questionnaire page when user add new question and ' +
    'click on Save button on question page', () => {
    questionPage.setContent(newQuestion.content);
    questionPage.setNewAnswer(newQuestion.answers[0]);
    questionPage.getAddAnswerButton().click();
    questionPage.setNewAnswer(newQuestion.answers[1]);
    questionPage.getAddAnswerButton().click();

    expect(questionPage.getSaveButton().isEnabled()).toBe(true);
    questionPage.getSaveButton().click();

    expect(questionnairePage.getQuestionsPanel().isDisplayed()).toBe(true);
    expect(questionnairePage.getSaveButton().isEnabled()).toBe(true);
    expect(questionnairePage.getResetButton().isEnabled()).toBe(true);

    questionnairePage.getAllQuestions().count().then(value => {
      expect(value).toEqual(1);

      expect(questionnairePage.getQuestionRemoveLink(0).isDisplayed()).toBe(true);
      expect(questionnairePage.getQuestionContent(0).getText()).toBe(newQuestion.content);
      questionnairePage.getAnswersFromQuestion(0).count().then(valueAnswers => {
        expect(valueAnswers).toEqual(2);
        expect(questionnairePage.getAnswerFromQuestion(0,0).getText()).toEqual(newQuestion.answers[0]);
        expect(questionnairePage.getAnswerFromQuestion(0,1).getText()).toEqual(newQuestion.answers[1]);
      })
    });
  });



  it('should be same number of questions in questionnaire when user click on Cancel button on ' +
    'question page, as it was before opening question modal dialog', () => {
    questionnairePage.getAllQuestions().count().then(value => {
      numberOfQuestions.value = value;
    });
    questionnairePage.getLinkForCreateQuestion().click();

    browser.wait(function() {
      return questionPage.getQuestionElement().isDisplayed();
    }).then(function() {
      expect(questionPage.getCancelButton().isDisplayed()).toBe(true);
      expect(questionPage.getCancelButton().isEnabled()).toBe(true);
      questionPage.getCancelButton().click();

      questionnairePage.getAllQuestions().count().then(value => {
        expect(value).toEqual(numberOfQuestions.value);
      });
    });
  });



  it('should show state of questionnaire as before click on Save button, when user save questionnaire ' +
    'and after that click on Questionnaire link in meeting item modal', () => {
    expect(questionnairePage.getSaveButton().isEnabled()).toBe(true);
    questionnairePage.getSaveButton().click();

    expect(meetingItemPage.getQuestionnaireLink().isDisplayed()).toBe(true);
    meetingItemPage.getQuestionnaireLink().click();

    browser.wait(function() {
      return questionnairePage.getQuestionnaireElement().isDisplayed();
    }).then(function() {
      expect(questionnairePage.getQuestionsPanel().isDisplayed()).toBe(true);
      expect(questionnairePage.getSaveButton().isDisplayed()).toBe(true);
      expect(questionnairePage.getSaveButton().isEnabled()).toBe(false);
      expect(questionnairePage.getResetButton().isDisplayed()).toBe(true);
      expect(questionnairePage.getResetButton().isEnabled()).toBe(false);
      questionnairePage.getAllQuestions().count().then(value => {
        expect(value).toEqual(numberOfQuestions.value);

        for(let i:number =0; i<value; i++) {
          expect(questionnairePage.getQuestionContent(i).getText()).toEqual(newQuestion.content);
          questionnairePage.getAnswersFromQuestion(i).count().then(valueQuestions => {
            expect(valueQuestions).toEqual(newQuestion.answers.length);

            for(let j:number=0; j<valueQuestions; j++) {
              expect(questionnairePage.getAnswerFromQuestion(i,j).getText()).toEqual(newQuestion.answers[j]);
            }
          });
        }
      });
    });
  });


  it('should remove question from question list in questionnaire modal when user click on Remove ' +
    'link on some question, and show previous state of questions when click on Reset button', () => {
    // before remove question, we need to add one more, so we can see how number of questions changes
    questionnairePage.getLinkForCreateQuestion().click();

    let secondQuestion = {'content':'Second question', 'answers':['Third answer', 'Fourth question']};

    browser.wait(function() {
      return questionPage.getQuestionElement().isDisplayed();
    }).then(function() {
      questionPage.setContent(secondQuestion.content);
      questionPage.setNewAnswer(secondQuestion.answers[0]);
      questionPage.getAddAnswerButton().click();
      questionPage.setNewAnswer(secondQuestion.answers[1]);
      questionPage.getAddAnswerButton().click();

      expect(questionPage.getSaveButton().isEnabled()).toBe(true);
      questionPage.getSaveButton().click();

      // expect that number of questions in questionnaire is for 1 greater then numberOfQuestions.value
      questionnairePage.getAllQuestions().count().then(value => {
        expect(value).toEqual(1+numberOfQuestions.value);

        expect(questionnairePage.getQuestionRemoveLink(0).isDisplayed()).toBe(true);
        questionnairePage.getQuestionRemoveLink(0).click();

        // after removing first question from list, expect that number of questions is again equal to numberOfQuestions.value
        questionnairePage.getAllQuestions().count().then(newValue => {
          expect(newValue).toEqual(numberOfQuestions.value);

          expect(questionnairePage.getResetButton().isEnabled()).toBe(true);
          questionnairePage.getResetButton().click();

          // expect that after reset there is again only one question, first question with all elements of newQuestion
          questionnairePage.getAllQuestions().count().then(valueAfterReset => {
            expect(valueAfterReset).toEqual(numberOfQuestions.value);

            expect(questionnairePage.getQuestionContent(0).getText()).toEqual(newQuestion.content);
            expect(questionnairePage.getAnswerFromQuestion(0,0).getText()).toEqual(newQuestion.answers[0]);
            expect(questionnairePage.getAnswerFromQuestion(0,1).getText()).toEqual(newQuestion.answers[1]);
            expect(questionnairePage.getResetButton().isEnabled()).toBe(false);
          })
        });
      })
    });
  });
});
