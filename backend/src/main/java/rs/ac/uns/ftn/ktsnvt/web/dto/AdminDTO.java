package rs.ac.uns.ftn.ktsnvt.web.dto;

import rs.ac.uns.ftn.ktsnvt.model.entity.Administrator;

import java.io.Serializable;

/**
 * Created by Ivana Zeljkovic on 08/11/2017.
 */
public class AdminDTO implements Serializable {

    private Long id;
    private String firstName;
    private String lastName;
    private String username;
    private long numberOfElements;

    public AdminDTO() { }

    public AdminDTO(Administrator administrator) {
        this.id = administrator.getId();
        this.firstName = administrator.getFirstName();
        this.lastName = administrator.getLastName();
        this.username = administrator.getAccount().getUsername();
    }

    public AdminDTO(Administrator administrator, long numberOfElements) {
        this.id = administrator.getId();
        this.firstName = administrator.getFirstName();
        this.lastName = administrator.getLastName();
        this.username = administrator.getAccount().getUsername();
        this.numberOfElements = numberOfElements;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public long getNumberOfElements() { return numberOfElements; }

    public void setNumberOfElements(long numberOfElements) { this.numberOfElements = numberOfElements; }
}
