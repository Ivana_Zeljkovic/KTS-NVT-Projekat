import {by, element, ElementFinder} from "protractor";

export class AdminProfilePage {
  // get methods
  getAdminProfileElement(): ElementFinder {
    return element(by.tagName('app-admin-profile'));
  }
}
