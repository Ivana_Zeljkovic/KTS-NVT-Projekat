import { NgModule } from '@angular/core';
// module
import { SharedModule } from "../shared/shared.module";
import { QuestionnairesRouterModule } from "./questionnaires-router.module";
// component
import { QuestionnaireListComponent } from './questionnaire-list/questionnaire-list.component';
import { QuestionnaireComponent } from "../shared/components/questionnaire/questionnaire.component";
import { VoteComponent } from './vote/vote.component';
import { ResultsComponent } from './results/results.component';


@NgModule({
  imports: [
    SharedModule,
    QuestionnairesRouterModule
  ],
  declarations: [
    QuestionnaireListComponent,
    VoteComponent,
    ResultsComponent
  ],
  entryComponents: [
    QuestionnaireComponent,
    VoteComponent,
    ResultsComponent
  ]
})
export class QuestionnairesModule { }
