import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
// service
import { FirmInstitutionService } from "./firm-institution.service";


@Injectable()
export class InstitutionService extends FirmInstitutionService{

  constructor(http: HttpClient) {
    super('institutions', http);
  }
}
