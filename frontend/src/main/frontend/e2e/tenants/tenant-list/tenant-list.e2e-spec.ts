import {browser, by, element, ElementFinder} from "protractor";
// page
import {LoginPage} from "../../page-objects/login.po";
import {ConfirmDeletePage} from "../../page-objects/confirm-delete.po";
import {TenantListPage} from "./page-objects/tenant-list.po";

describe('Tenant list view', () => {
  let loginPage: LoginPage;
  let tenantListPage: TenantListPage;
  let confirmDeletePage: ConfirmDeletePage;
  let empty: any;

  beforeEach(() => {
    loginPage = new LoginPage();
    tenantListPage = new TenantListPage();
    confirmDeletePage = new ConfirmDeletePage();
    browser.get("/");

    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');
    loginPage.setUsername('ivana');
    loginPage.setPassword('ivana');
    loginPage.getSignInButton().click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/home');
    browser.get('http://localhost:49152/#/tenants');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/tenants');

  });

  afterEach(() => {
    browser.executeScript('window.localStorage.clear();');
  });

  it("should successfully load page and all of its components", () => {

    expect(tenantListPage.getPageTitle().isDisplayed()).toBe(true);
    expect(tenantListPage.getDropdownToggle().isDisplayed()).toBe(true);
    expect(tenantListPage.getSortButton().isDisplayed()).toBe(true);
    expect(tenantListPage.getAdminsTable().isDisplayed()).toBe(true);
    expect(tenantListPage.getPagination().isDisplayed()).toBe(true);

  });

  it("should be sorted by first name when loaded", () => {
    let firstNameFirstRow = element(by.xpath("//tr[first()]/td[1]"));
    let firstNameLastRow = element(by.xpath("//tr[last()]/td[1]"));
    expect(firstNameFirstRow < firstNameLastRow);
  });

  it("should sort tenants by last name", () => {
    tenantListPage.getDropdownToggle().click();
    element(by.css('.dropdown-menu li:nth-child(2)')).click();
    expect(tenantListPage.getDropdownToggle().getText()).toBe("Last name");
    tenantListPage.getSortButton().click();
    let lastNameFirstRow = element(by.xpath("//tr[first()]/td[2]"));
    let lastNameLastRow = element(by.xpath("//tr[last()]/td[2]"));
    expect(lastNameFirstRow < lastNameLastRow);

  });

  it("should sort tenants by first name", () => {
    tenantListPage.getDropdownToggle().click();
    element(by.css('.dropdown-menu li:nth-child(1)')).click();
    expect(tenantListPage.getDropdownToggle().getText()).toBe("First name");
    tenantListPage.getSortButton().click();
    let firstNameFirstRow = element(by.xpath("//tr[first()]/td[1]"));
    let firstNameLastRow = element(by.xpath("//tr[last()]/td[1]"));
    expect(firstNameFirstRow < firstNameLastRow);
  });

  it("should sort tenants by email", () => {
    tenantListPage.getDropdownToggle().click();
    element(by.css('.dropdown-menu li:nth-child(3)')).click();
    expect(tenantListPage.getDropdownToggle().getText()).toBe("Email");
    tenantListPage.getSortButton().click();
    let emailFirstRow = element(by.xpath("//tr[first()]/td[5]"));
    let emailLastRow = element(by.xpath("//tr[last()]/td[5]"));
    expect(emailFirstRow < emailLastRow);
  });

  it("should sort tenants by phone number", () => {
    tenantListPage.getDropdownToggle().click();
    element(by.css('.dropdown-menu li:nth-child(4)')).click();
    expect(tenantListPage.getDropdownToggle().getText()).toBe("Phone number");
    tenantListPage.getSortButton().click();
    let phoneNumberFirstRow = element(by.xpath("//tr[first()]/td[6]"));
    let phoneNumberLastRow = element(by.xpath("//tr[last()]/td[6]"));
    expect(phoneNumberFirstRow < phoneNumberLastRow);
  });


  it('should open page with more details about tenant when click on button', () => {
    let moreButton = element(by.xpath("//tr[7]/td[8]")).element(by.css('.btn.btn-default'));
    moreButton.click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/tenants/109');
  });

  it('should confirm tenants registration when click on confirm button', () => {
    let confirmButton = element(by.xpath("//tr[7]/td[9]")).element(by.css('.btn.btn-success'));
    confirmButton.click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/tenants');
    let confirmSpace = element(by.xpath("//tr[7]/td[9]"));
    empty = {
      'empty': ''
    };
    expect(confirmSpace == empty.empty);

  });


  it('should delete tenant when clicked on delete button', () => {
    let deleteButton = element(by.xpath("//tr[7]/td[7]")).element(by.css('.btn.btn-danger'));
    let firstNameDeleted = element(by.xpath("//tr[7]/td[1]"));
    deleteButton.click();

    browser.wait(function() {
      return confirmDeletePage.getConfirmDeleteElement().isDisplayed();
    }).then(function() {

      expect(confirmDeletePage.getConfirmButton().isDisplayed()).toBe(true);
      expect(confirmDeletePage.getConfirmButton().isEnabled()).toBe(true);
      expect(confirmDeletePage.getDeclineButton().isDisplayed()).toBe(true);
      expect(confirmDeletePage.getDeclineButton().isEnabled()).toBe(true);

      confirmDeletePage.getConfirmButton().click();
      expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/tenants');
      let firstNameSeventhRow = element(by.xpath("//tr[7]/td[1]"));
      expect(firstNameSeventhRow != firstNameDeleted);
    });
  });

});
