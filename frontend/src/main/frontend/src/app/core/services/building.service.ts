import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
// service
import { DataService } from "./data.service";
// models
import { Building } from "../../shared/models/building";


@Injectable()
export class BuildingService extends DataService<Building>{
  private readonly buildingUrl = 'buildings';

  constructor(http: HttpClient) {
    super('buildings', http);
  }

  getFromAddress(cityName: string): Observable<Array<Building>> {
    let params: HttpParams = new HttpParams()
      .append('city_name', cityName);

    return this.http.get<Array<Building>>(`${this.urlBase}/${this.buildingUrl}/building_address`, {params})
      .catch(this.handleErrors);
  }
}
