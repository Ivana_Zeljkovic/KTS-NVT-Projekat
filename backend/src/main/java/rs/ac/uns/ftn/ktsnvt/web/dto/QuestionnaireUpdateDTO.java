package rs.ac.uns.ftn.ktsnvt.web.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Ivana Zeljkovic on 06/12/2017.
 */
public class QuestionnaireUpdateDTO implements Serializable {
    @NotNull
    private Long id;
    @NotNull
    @Size(min = 1)
    @Valid
    private List<QuestionUpdateDTO> questions;

    public QuestionnaireUpdateDTO() { }

    public QuestionnaireUpdateDTO(Long id, List<QuestionUpdateDTO> questions) {
        this.id = id;
        this.questions = questions;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<QuestionUpdateDTO> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionUpdateDTO> questions) {
        this.questions = questions;
    }
}
