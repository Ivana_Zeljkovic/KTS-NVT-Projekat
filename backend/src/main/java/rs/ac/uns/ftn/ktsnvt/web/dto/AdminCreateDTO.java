package rs.ac.uns.ftn.ktsnvt.web.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by Ivana Zeljkovic on 08/11/2017.
 */
public class AdminCreateDTO implements Serializable {

    @NotNull
    @Valid
    private LoginDTO loginAccount;
    @NotNull
    private String firstName;
    @NotNull
    private String lastName;

    public AdminCreateDTO() {}

    public LoginDTO getLoginAccount() {
        return loginAccount;
    }

    public void setLoginAccount(LoginDTO loginAccount) {
        this.loginAccount = loginAccount;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
