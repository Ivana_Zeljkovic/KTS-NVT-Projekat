package rs.ac.uns.ftn.ktsnvt.service.unit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.City;
import rs.ac.uns.ftn.ktsnvt.repository.CityRepository;
import rs.ac.uns.ftn.ktsnvt.service.CityService;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;

/**
 * Created by Nikola Garabandic on 30/11/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CityServiceUnitTest {

    @Autowired
    private CityService cityService;

    @MockBean
    private CityRepository cityRepository;


    @Before
    public void setUp(){
        City city = new City("Novi Sad", 21000);

        given(
                this.cityRepository.findByNameAndPostalNumber("Novi Sad", 21000)
        ).willReturn(
                city
        );

        given(
                this.cityRepository.findByNameAndPostalNumber("Novi sd", 21000)
        ).willReturn(
                null
        );
    }

    @Test
    public void testFindByNameAndPostalNumber(){
        /*
         * Test proverava ispravnost rada metode FindByNameAndPostalNumber servisa CityService
         * (dobavljanje city objekta na osnovu Imena i postanskog broja)
         * u slucaju kada postoji grad sa zadatim imenom i postanskim brojem
         * */

        City city = this.cityService.findByNameAndPostalNumber("Novi Sad", 21000);

        assertNotNull(city);
        assertEquals("Novi Sad", city.getName());
        assertEquals(21000, city.getPostalNumber());
    }

    @Test(expected = NotFoundException.class)
    public void testFindNoneByNameAndPostalNumber(){
        /*
         * Test proverava ispravnost rada metode FindByNameAndPostalNumber servisa CityService
         * (dobavljanje city objekta na osnovu Imena i postanskog broja)
         * u slucaju kada ne postoji grad sa zadatim imenom i postanskim brojem - ocekivano NotFoundException
         * */

        City city = this.cityService.findByNameAndPostalNumber("Novi sd", 21000);
    }
}
