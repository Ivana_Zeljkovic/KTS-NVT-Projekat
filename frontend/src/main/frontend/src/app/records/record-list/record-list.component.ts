import { Component, OnInit } from '@angular/core';
import { BsModalService } from "ngx-bootstrap";
import { ToasterConfig, ToasterService } from "angular5-toaster/dist";
import { Router } from "@angular/router";
// service
import { RecordService } from "../../core/services/record.service";
import { JwtService } from "../../core/services/jwt.service";
// model
import { Record } from "../../shared/models/record";
import { PageAndSort } from "../../shared/models/page-and-sort";
// error
import { AppError } from "../../shared/errors/app-error";
import { ForbiddenError } from "../../shared/errors/forbidden-error";
import { NotFoundError } from "../../shared/errors/not-found-error";


@Component({
  selector: 'app-record-list',
  templateUrl: './record-list.component.html',
  styleUrls: ['./record-list.component.css']
})
export class RecordListComponent implements OnInit {
  recordList: Array<Record>;
  toasterConfig: ToasterConfig;

  // Elements for bootstrap paging
  currentPage: number;
  totalItems: number;
  maxSize: number;
  itemsPerPage: number;

  // Elements for getting data from server
  pageSize: string;
  sortDirection: string;
  sortProperties: string[];

  constructor(private recordService: RecordService, private modalService: BsModalService,
              private toasterService: ToasterService, private jwtService: JwtService,
              private router: Router) {
    // toaster config
    this.toasterConfig = new ToasterConfig({
      timeout: {success: 2000, error: 4000, warning: 4000}
    });

    // Sett up for bootstrap paging
    this.currentPage = 1;
    this.maxSize = 3;
    this.itemsPerPage = 15;

    // Sett up for getting damages request
    this.pageSize = '15';
    this.sortDirection = 'ASC';
    this.sortProperties = ['meeting.date'];
  }

  ngOnInit() {
    this.loadRecords();
  }

  details(record: Record) {
    this.router.navigate(['records', record.id]);
  }

  pageChanged(event: any) {
    this.currentPage = event.page;
    this.loadRecords();
  }

  private loadRecords() {
    let page = new PageAndSort((this.currentPage - 1).toString(), this.pageSize, this.sortDirection, this.sortProperties);

    this.recordService.getPage(page, 'buildings', this.jwtService.getBuildingIdFromToken(), 'council/records')
      .subscribe(records => {
        if (records.length > 0)
          this.totalItems = records[0].numberOfElements;
        else this.totalItems = 0;
        this.recordList = records;
      }, (error: AppError) => {
        if(error instanceof ForbiddenError)
          this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
        else if(error instanceof NotFoundError)
          this.toasterService.pop('error', 'Error', 'Building not found!');
        else {
          this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
          throw error;
        }
      });
  }
}
