import {Question} from "./question";

export class QuestionUpdate {
  private id: number;
  private content: string;
  private answers: Array<string>;

  constructor(id: number, content: string, answers: Array<string>) {
    this.id = id;
    this.answers = new Array<string>();
    this.content = content;

    answers.forEach(answer => {
      this.answers.push(answer);
    })
  }
}
