import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { BsModalRef } from "ngx-bootstrap";
import { ToasterService } from "angular5-toaster/dist";
// service
import { DamageService } from "../../../core/services/damage.service";
import { JwtService } from "../../../core/services/jwt.service";
// model
import { Damage } from "../../../shared/models/damage";
import { DamageForMeetingItem } from "../shared/models/damage-for-meeting-item";
import { PageAndSort } from "../../../shared/models/page-and-sort";
// error
import { AppError } from "../../../shared/errors/app-error";
import { ForbiddenError } from "../../../shared/errors/forbidden-error";
import { NotFoundError } from "../../../shared/errors/not-found-error";


@Component({
  selector: 'app-damage-for-questionnaire',
  templateUrl: './damage-for-questionnaire.component.html',
  styleUrls: ['./damage-for-questionnaire.component.css']
})
export class DamageForQuestionnaireComponent implements OnInit {
  damageList: Array<Damage>;
  @Output() damageSelected = new EventEmitter<DamageForMeetingItem>();

  // Elements for bootstrap paging
  currentPage: number;
  totalItems: number;
  maxSize: number;
  itemsPerPage: number;

  // Elements for getting data from server
  pageSize: string;
  sortDirection: string;
  sortProperties: string[];


  constructor(private modalRef: BsModalRef, private damageService: DamageService,
              private jwtService: JwtService, private toasterService: ToasterService) {
    // Sett up for bootstrap paging
    this.currentPage = 1;
    this.maxSize = 3;
    this.itemsPerPage = 4;

    // Sett up for getting damages request
    this.pageSize = '4';
    this.sortDirection = 'ASC';
    this.sortProperties = ['urgent', 'date'];
  }

  ngOnInit() {
    this.loadDamageList();
  }

  chooseDamage(damage) {
    this.damageSelected.emit(new DamageForMeetingItem(damage.id, damage.description));
    this.modalRef.hide();
  }

  pageChanged(event: any) {
    this.currentPage = event.page;
    this.loadDamageList();
  }

  private loadDamageList() {
    let page = new PageAndSort((this.currentPage - 1).toString(), this.pageSize, this.sortDirection, this.sortProperties);

    this.damageService.getPage(page, 'tenants', this.jwtService.getIdFromToken(), 'responsible_for_damages')
      .subscribe(damages => {
        if (damages.length > 0)
          this.totalItems = damages[0].numberOfElements;
        else this.totalItems = 0;
        this.damageList = damages;
      }, (error: AppError) => {
        if (error instanceof ForbiddenError)
          this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
        else if (error instanceof NotFoundError)
          this.toasterService.pop('error', 'Error', 'Tenant/Institution/Firm not found!');
        else {
          this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
          throw error;
        }
        });
  }
}
