import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
import { Injectable } from "@angular/core";
import { JwtService } from "../services/jwt.service";

@Injectable()
export class InstitutionGuard implements CanActivate {

  constructor(private router: Router, private jwtService: JwtService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if(this.jwtService.hasRole('INSTITUTION')) return true;

    // logged user hasn't role INSTITUTION
    this.router.navigate(['home']);
    return false;
  }
}
