package rs.ac.uns.ftn.ktsnvt.model.entity;

import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "comment")
@Where(clause="deleted=0")
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Column(nullable = false)
    private String content;

    @Column(nullable = false)
    private Date date;

    @Column(nullable = false, columnDefinition = "INTEGER DEFAULT 0")
    @Version
    private int version;

    @Column(nullable = false, columnDefinition = "BOOL DEFAULT FALSE")
    private boolean deleted;

    @OneToOne
    private Account creator;

    @ManyToOne
    private Damage damage;

    public Comment() { }

    public Comment(String content, Date date) {
        this.content = content;
        this.date = date;
    }

    public Comment(String content, Date date, Account creator) {

        this.content = content;
        this.date = date;
        this.creator = creator;
    }

    public Long getId() { return id; }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Account getCreator() { return creator; }

    public void setCreator(Account creator) { this.creator = creator; }

    public Damage getDamage() { return damage; }

    public void setDamage(Damage damage) { this.damage = damage; }
}
