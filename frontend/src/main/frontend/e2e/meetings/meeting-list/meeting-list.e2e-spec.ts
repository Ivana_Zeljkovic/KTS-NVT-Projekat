import { browser } from "protractor";
// page
import { LoginPage } from "../../page-objects/login.po";
import { MeetingListPage } from "../../page-objects/meeting-list.po";
import { MeetingAddPage } from "../page-objects/meeting-add.po";
import { MeetingDetailsPage } from "../page-objects/meeting-details.po";
import { MeetingItemListModalPage } from "../page-objects/meeting-item-list-modal.po";
import { MeetingItemListPage } from "../../home/tenant-home/page-objects/meeting-item-list.po";
import { MeetingItemPage } from "../../page-objects/meeting-item.po";
import { ConfirmDeletePage } from "../../page-objects/confirm-delete.po";
import { RecordCreatePage } from "../../page-objects/record-create.po";
// helper
import { DateConverter } from "../../util/date-converter";


describe('Meeting list', () => {
  let loginPage: LoginPage;
  let meetingListPage: MeetingListPage;
  let meetingAddPage: MeetingAddPage;
  let meetingDetailsPage: MeetingDetailsPage;
  let meetingItemListModalPage: MeetingItemListModalPage;
  let meetingItemListPage: MeetingItemListPage;
  let meetingItemPage: MeetingItemPage;
  let recordCreatePage: RecordCreatePage;
  let confirmDeletePage: ConfirmDeletePage;
  let newMeetingItem: any;
  let newMeeting: any;


  beforeAll(() => {
    loginPage = new LoginPage();
    meetingListPage = new MeetingListPage();
    meetingAddPage = new MeetingAddPage();
    meetingDetailsPage = new MeetingDetailsPage();
    meetingItemListModalPage = new MeetingItemListModalPage();
    meetingItemListPage = new MeetingItemListPage();
    meetingItemPage = new MeetingItemPage();
    recordCreatePage = new RecordCreatePage();
    confirmDeletePage = new ConfirmDeletePage();
    newMeetingItem = {'title':'Some title', 'content':'Some content', 'date':'', 'creator':''};
    newMeeting = {'date':'', 'duration':''};


    browser.get('/');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');

    loginPage.setUsername('kaca');
    loginPage.setPassword('kaca');
    loginPage.getSignInButton().click();

    browser.wait(function() {
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/home/notifications';
      });
    }).then(function() {
      browser.get('http://localhost:49152/#/home/meeting-details-items');

      browser.wait(function() {
        return browser.getCurrentUrl().then(value => {
          return value === 'http://localhost:49152/#/home/meeting-details-items';
        });
      }).then(function() {
        expect(meetingItemListPage.getMeetingItemListElement().isDisplayed()).toBe(true);
        // before test we should add new meeting item
        expect(meetingItemListPage.getAddButton().isDisplayed()).toBe(true);
        expect(meetingItemListPage.getAddButton().isEnabled()).toBe(true);

        meetingItemListPage.getAddButton().click();

        browser.wait(function () {
          // expect to show modal - meeting item page for create new meeting item
          return meetingItemPage.getMeetingItemElement().isDisplayed();
        }).then(function () {
          meetingItemPage.setTitle(newMeetingItem.title);
          meetingItemPage.setContent(newMeetingItem.content);
          meetingItemPage.getSaveButton().click();

          meetingItemListPage = new MeetingItemListPage();

          browser.wait(function() {
            return meetingItemListPage.getMeetingItemListElement().isDisplayed();
          }).then(function() {
            // expect that new meeting item is first in list include menu with edit and delete links
            expect(meetingItemListPage.getTitleFromMeetingItemPanel(0).getText()).toEqual(newMeetingItem.title);
            expect(meetingItemListPage.getContentFromMeetingItemPanel(0).getText()).toEqual(newMeetingItem.content);
            meetingItemListPage.getDateFromMeetingItemPanel(0).getText().then(value => {
              // difference between current time and time of new meeting item is less then 5sec
              let currentDate = new Date().getTime();
              newMeetingItem.date = value;
              let meetingItemDate = DateConverter.convertStringToDate(value).getTime();
              expect(currentDate - meetingItemDate).toBeLessThan(5000);
            });
            meetingItemListPage.getCreatorNameFromMeetingItemPanel(0).getText().then(value => {
              newMeetingItem.creator = value;
            });

            browser.get('http://localhost:49152/#/meetings');

            browser.wait(function() {
              return meetingListPage.getMeetingListElement().isDisplayed();
            });
          });
        });
      });
    });
  });

  afterAll(() => {
    browser.executeScript('window.localStorage.clear();');
  });



  it('should show all elements on page when president is logged in', () => {
    expect(meetingListPage.getTitle().isDisplayed()).toBe(true);
    expect(meetingListPage.getMeetingsTable().isDisplayed()).toBe(true);

    meetingListPage.getMeetingsTableRows().count().then(value => {
      // every row in table should have date, duration, number of meeting items and menu
       for(let i:number =0; i < value; i++) {
         expect(meetingListPage.getMeetingDate(i).isDisplayed()).toBe(true);
         expect(meetingListPage.getMeetingDuration(i).isDisplayed()).toBe(true);
         expect(meetingListPage.getMeetingNumberOfMeetingItems(i).isDisplayed()).toBe(true);
         expect(meetingListPage.getMeetingMenu(i).isDisplayed()).toBe(true);

         meetingListPage.getMeetingMenu(i).click();

        browser.wait(function() {
          return meetingListPage.getContentOfMeetingMenu(i).isDisplayed();
        }).then(function() {
          // in menu are two options: Details and Remove meeting
          expect(meetingListPage.getDetailsOptionFromMeetingMenu(i).isDisplayed()).toBe(true);
          expect(meetingListPage.getRemoveOptionFromMeetingMenu(i).isDisplayed()).toBe(true);
        });
      }
    });

    expect(meetingListPage.getTitleForFinishedMeetingList().isDisplayed()).toBe(true);
    // if there are finished meeting, every row in table should have same elements as table above,
    // but in row menu second option is Create record
    meetingListPage.getFinishedMeetingsList().isPresent().then(isPresent => {
      if(isPresent) {
        expect(meetingListPage.getFinishedMeetingsList().isDisplayed()).toBe(true);

        meetingListPage.getFinishedMeetingsTableRows().count().then(value => {
          // every row in table should have date, duration, number of meeting items and menu
          for(let i:number =0; i < value; i++) {
            expect(meetingListPage.getFinishedMeetingDate(i).isDisplayed()).toBe(true);
            expect(meetingListPage.getFinishedMeetingDuration(i).isDisplayed()).toBe(true);
            expect(meetingListPage.getFinishedMeetingNumberOfMeetingItems(i).isDisplayed()).toBe(true);
            expect(meetingListPage.getFinishedMeetingMenu(i).isDisplayed()).toBe(true);

            meetingListPage.getFinishedMeetingMenu(i).click();

            browser.wait(function() {
              return meetingListPage.getContentOfFinishedMeetingMenu(i).isDisplayed();
            }).then(function() {
              // in menu are two options: Details and Create record
              expect(meetingListPage.getDetailsOptionFromFinishedMeetingMenu(i).isDisplayed()).toBe(true);
              expect(meetingListPage.getCreateRecordOptionFromFinishedMeetingMenu(i).isDisplayed()).toBe(true);
            });
          }
        });
      }
    });
  });



  it('should add new meeting in list as first in list, when user click on Create meeting link', () => {
    expect(meetingListPage.getAddLink().isDisplayed()).toBe(true);

    meetingListPage.getAddLink().click();

    browser.wait(function() {
      return meetingAddPage.getMeetingAddElement().isDisplayed();
    }).then(function() {
      expect(meetingAddPage.getMeetingDuration().isDisplayed()).toBe(true);
      expect(meetingAddPage.getChooseMeetingItemsLink().isDisplayed()).toBe(true);

      meetingAddPage.getMeetingDuration().clear();
      meetingAddPage.setMeetingDuration('60');
      newMeeting.duration = '60';
      let date = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
      date.setHours(8,0,0);
      newMeeting.date = DateConverter.convertDateToString(date);
      meetingAddPage.getChooseMeetingItemsLink().click();

      browser.wait(function() {
        return meetingItemListModalPage.getMeetingItemListModalElement().isDisplayed();
      }).then(function() {
        meetingItemListModalPage.getMeetingItems().count().then(value => {
          // we expect that in list is for sure one meeting item (that we added in beforeAll section)
          expect(value).toBeGreaterThan(0);

          expect(meetingItemListModalPage.getMeetingItemCreator(0).getText()).toEqual(newMeetingItem.creator);
          meetingItemListModalPage.getMeetingItemDate(0).getText().then(date => {
            let valueDate = date.split(' ')[0];
            let valueTime = `${date.split(' ')[1].split(':')[0]}:${date.split(' ')[1].split(':')[1]}`;
            let existingDate = newMeetingItem.date.split(' ')[0];
            let existingTime = `${newMeetingItem.date.split(' ')[1].split(':')[0]}:${newMeetingItem.date.split(' ')[1].split(':')[1]}`;
            expect(valueDate).toEqual(existingDate);
            expect(valueTime).toEqual(existingTime);
          });
          expect(meetingItemListModalPage.getLabelSelect(0).isDisplayed()).toBe(true);

          meetingItemListModalPage.getLabelSelect(0).click();

          browser.wait(function() {
            return meetingItemListModalPage.getLabelRemove(0).isDisplayed();
          }).then(function() {
            expect(meetingItemListModalPage.getSaveButton().isEnabled()).toBe(true);
            meetingItemListModalPage.getSaveButton().click();
            meetingAddPage = new MeetingAddPage();

            browser.wait(function() {
              return meetingAddPage.getMeetingAddElement().isDisplayed();
            }).then(function() {
              meetingAddPage.getMeetingItems().count().then(numberOfMeetingItems => {
                expect(numberOfMeetingItems).toEqual(1);
                expect(meetingAddPage.getSaveButton().isEnabled()).toBe(true);
                meetingAddPage.getSaveButton().click();

                meetingListPage = new MeetingListPage();

                browser.wait(function () {
                  return meetingListPage.getMeetingListElement().isDisplayed();
                }).then(function () {
                  // expect to see new meeting as first in list
                  meetingListPage.getMeetingDate(0).getText().then(date => {
                    let valueDate = date.split(' ')[0];
                    let valueTime = `${date.split(' ')[1].split(':')[0]}:${date.split(' ')[1].split(':')[1]}`;
                    let existingDate = newMeeting.date.split(' ')[0];
                    let existingTime = `${newMeeting.date.split(' ')[1].split(':')[0]}:${newMeeting.date.split(' ')[1].split(':')[1]}`;
                    expect(valueDate).toEqual(existingDate);
                    expect(valueTime).toEqual(existingTime);
                  });
                  expect(meetingListPage.getMeetingDuration(0).getText()).toEqual(`${newMeeting.duration} minutes`);
                  expect(meetingListPage.getMeetingNumberOfMeetingItems(0).getText()).toEqual('1');
                });
              });
            });
          });
        });
      });
    });
  });



  it('should open details page when user click on Details option in meeting menu', () => {
    expect(meetingListPage.getMeetingMenu(0).isDisplayed()).toBe(true);
    meetingListPage.getMeetingMenu(0).click();

    browser.wait(function() {
      return meetingListPage.getContentOfMeetingMenu(0).isDisplayed();
    }).then(function() {
      expect(meetingListPage.getDetailsOptionFromMeetingMenu(0).isDisplayed()).toBe(true);
      meetingListPage.getDetailsOptionFromMeetingMenu(0).click();

      browser.wait(function() {
        return meetingDetailsPage.getMeetingDetailsElement().isDisplayed();
      }).then(function() {
        expect(meetingDetailsPage.getTitle().isDisplayed()).toBe(true);
        expect(meetingDetailsPage.getMeetingDate().isDisplayed()).toBe(true);
        meetingDetailsPage.getMeetingDate().getText().then(date => {
          date = date.substr(15);
          let valueDate = date.split(' ')[0];
          let valueTime = `${date.split(' ')[1].split(':')[0]}:${date.split(' ')[1].split(':')[1]}`;
          let existingDate = newMeeting.date.split(' ')[0];
          let existingTime = `${newMeeting.date.split(' ')[1].split(':')[0]}:${newMeeting.date.split(' ')[1].split(':')[1]}`;
          expect(valueDate).toEqual(existingDate);
          expect(valueTime).toEqual(existingTime);
        });
        expect(meetingDetailsPage.getMeetingDuration().isDisplayed()).toBe(true);
        expect(meetingDetailsPage.getMeetingDuration().getText()).toEqual(`Duration (in minutes): ${newMeeting.duration}`);

        meetingDetailsPage.getMeetingItems().count().then(value => {
          expect(value).toEqual(1);

          expect(meetingDetailsPage.getMeetingItemTitle(0).getText()).toEqual(newMeetingItem.title);
          expect(meetingDetailsPage.getContentFromMeetingItem(0).getText()).toEqual(newMeetingItem.content);
          expect(meetingDetailsPage.getDateFromMeetingItem(0).getText()).toEqual(newMeetingItem.date);
          expect(meetingDetailsPage.getCreatorFromMeetingItem(0).getText()).toEqual(newMeetingItem.creator);
        });
      });
    });
  });



  it('should remove added meeting, when user click on Remove option in meeting menu', () => {
    meetingDetailsPage = new MeetingDetailsPage();
    meetingListPage = new MeetingListPage();
    browser.get('http://localhost:49152/#/meetings');

    browser.wait(function() {
      return meetingListPage.getMeetingListElement().isDisplayed();
    }).then(function() {
      expect(meetingListPage.getMeetingMenu(0).isDisplayed()).toBe(true);
      meetingListPage.getMeetingMenu(0).click();

      browser.wait(function() {
        return meetingListPage.getContentOfMeetingMenu(0).isDisplayed();
      }).then(function() {
        expect(meetingListPage.getRemoveOptionFromMeetingMenu(0).isDisplayed()).toBe(true);
        meetingListPage.getRemoveOptionFromMeetingMenu(0).click();

        browser.wait(function() {
          return confirmDeletePage.getConfirmDeleteElement().isDisplayed();
        }).then(function() {
          expect(confirmDeletePage.getConfirmButton().isDisplayed()).toBe(true);
          expect(confirmDeletePage.getConfirmButton().isEnabled()).toBe(true);
          confirmDeletePage.getConfirmButton().click();

          meetingListPage = new MeetingListPage();

          browser.wait(function() {
            return meetingListPage.getMeetingListElement().isDisplayed();
          }).then(function() {
            meetingListPage.getMeetingMenu(0).click();

            browser.wait(function() {
              return meetingListPage.getContentOfMeetingMenu(0).isDisplayed();
            }).then(function() {
              expect(meetingListPage.getDetailsOptionFromMeetingMenu(0).isDisplayed()).toBe(true);

              // open first meeting in details view to be sure that we removed selected one
              meetingListPage.getDetailsOptionFromMeetingMenu(0).click();

              browser.wait(function() {
                return meetingDetailsPage.getMeetingDetailsElement().isDisplayed();
              }).then(function() {
                meetingDetailsPage.getMeetingItems().count().then(value => {
                  // if selected meeting has only one meeting item, we need to check is that meeting item one
                  // that we added in beforeAll section
                  if(value === 1) {
                    // we expect to see date different then newMeetingItem.date (then we are sure that we remove selected meeting)
                    expect(meetingDetailsPage.getDateFromMeetingItem(0).getText()).not.toEqual(newMeetingItem.date);
                  }
                });
              });
            });
          });
        });
      });
    });
  });



  it('should open create record modal dialog when user click on Create record option in menu of ' +
    'finished meeting', () => {
    meetingListPage = new MeetingListPage();
    browser.get('http://localhost:49152/#/meetings');

    browser.wait(function() {
      return meetingListPage.getMeetingListElement().isDisplayed();
    }).then(function() {
      meetingListPage.getFinishedMeetingsList().isPresent().then(isPresent => {
        if(isPresent) {
          expect(meetingListPage.getFinishedMeetingMenu(0).isDisplayed()).toBe(true);
          meetingListPage.getFinishedMeetingMenu(0).click();

          browser.wait(function () {
            return meetingListPage.getContentOfFinishedMeetingMenu(0).isDisplayed();
          }).then(function () {
            expect(meetingListPage.getCreateRecordOptionFromFinishedMeetingMenu(0).isDisplayed()).toBe(true);
            meetingListPage.getCreateRecordOptionFromFinishedMeetingMenu(0).click();

            browser.wait(function() {
              return recordCreatePage.getRecordCreateElement().isDisplayed();
            }).then(function () {
              expect(recordCreatePage.getContentInput().isDisplayed()).toBe(true);
            });
          });
        }
      });
    });
  });
});
