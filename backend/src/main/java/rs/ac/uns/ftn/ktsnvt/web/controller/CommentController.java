package rs.ac.uns.ftn.ktsnvt.web.controller;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.ktsnvt.exception.ForbiddenException;
import rs.ac.uns.ftn.ktsnvt.model.entity.*;
import rs.ac.uns.ftn.ktsnvt.service.*;
import rs.ac.uns.ftn.ktsnvt.web.controller.util.MessageConstants;
import rs.ac.uns.ftn.ktsnvt.web.dto.CommentCreateOrUpdateDTO;
import rs.ac.uns.ftn.ktsnvt.web.dto.CommentDTO;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static rs.ac.uns.ftn.ktsnvt.web.util.ConverterDTOToModel.convertCommentCreateDTOToComment;


/**
 * Created by Ivana Zeljkovic on 13/01/2018.
 */
@RestController
@RequestMapping(value = "/api")
@Api(value = "comments", description = "Operations pertaining to comments on damage.")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @Autowired
    private BuildingService buildingService;

    @Autowired
    private DamageService damageService;

    @Autowired
    private TenantService tenantService;

    @Autowired
    private AccountService accountService;


    @RequestMapping(
            value = "/damages/{damageId}/comments",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get list of all comments for selected damage in page form.",
            notes = "You have to provide a valid damage ID in the URL.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of comments.", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT') or hasAuthority('INSTITUTION') or hasAuthority('COMPANY')")
    public ResponseEntity getAllCommentsForDamage(
            @ApiParam(value = "The ID of the existing damage resource.", required = true) @PathVariable("damageId") Long damageId,
            @ApiParam(value = "The page number for getting damages.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting damages.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting damages.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "First sort property for getting damages.", required = true) @RequestParam String sortProperty) {

        Damage damage = this.damageService.findOne(damageId);

        List<CommentDTO> commentDTOS = new ArrayList<>();

        Page<Comment> comments = this.commentService.getAllForDamage(damage.getId(), Integer.valueOf(pageNumber),
                Integer.valueOf(pageSize), sortDirection, sortProperty);

        comments.getContent().forEach(comment -> commentDTOS.add(new CommentDTO(comment, comments.getTotalElements())));

        return new ResponseEntity<>(commentDTOS, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/damages/{damageId}/comments/{id}",
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Update comment resource.",
            notes = "You have to provide valid IDs for damage and comment in the URL. " +
                    "Method returns the comment being updated.",
            httpMethod = "PUT",
            consumes = "application/json",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated comment.", response = CommentDTO.class),
            @ApiResponse(code = 400, message = "Inappropriate comment object sent in request body."),
            @ApiResponse(code = 401, message = "You are not authorized to update the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT') or hasAuthority('INSTITUTION') or hasAuthority('COMPANY')")
    public ResponseEntity updateComment(
            @ApiParam(value = "The ID of the existing damage resource.", required = true) @PathVariable("damageId") Long damageId,
            @ApiParam(value = "The ID of the existing comment resource.", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "The comment object.", required = true) @Valid @RequestBody CommentCreateOrUpdateDTO commentUpdateDTO,
            @ApiParam(value = "The object that contains all errors from validation of DTO object") BindingResult errors) {

        Damage damage = this.damageService.findOne(damageId);
        Comment comment = this.commentService.findOne(id);

        // provera da li je komentar pripada izabranom kvaru
        this.commentService.belongToDamage(comment.getId(), damage);

        String currentUsername = SecurityContextHolder.getContext().getAuthentication().getName();

        if(!comment.getCreator().getUsername().equals(currentUsername))
            //komentar nije kreirao trenutno logovani korisnik (stanar, firma ili institucija)
            throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);

        comment.setContent(commentUpdateDTO.getContent());

        comment = this.commentService.save(comment);
        return new ResponseEntity<>(new CommentDTO(comment), HttpStatus.OK);
    }



    @RequestMapping(
            value = "/damages/{damageId}/comments/{id}",
            method = RequestMethod.DELETE
    )
    @ApiOperation(
            value = "Delete comment resource.",
            notes = "You have to provide valid IDs for damage and comment in the URL. " +
                    "Once deleted the resource can not be recovered.",
            httpMethod = "DELETE"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully deleted comment.", response = CommentDTO.class),
            @ApiResponse(code = 401, message = "You are not authorized to delete the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT') or hasAuthority('INSTITUTION') or hasAuthority('COMPANY')")
    public ResponseEntity deleteComment(
            @ApiParam(value = "The ID of the existing damage resource.", required = true) @PathVariable("damageId") Long damageId,
            @ApiParam(value = "The ID of the existing comment resource.", required = true) @PathVariable("id") Long id) {

        Damage damage = this.damageService.findOne(damageId);
        Comment comment = this.commentService.findOne(id);

        // provera da li je komentar pripada izabranom kvaru
        this.commentService.belongToDamage(comment.getId(), damage);

        String currentUsername = SecurityContextHolder.getContext().getAuthentication().getName();

        if(!comment.getCreator().getUsername().equals(currentUsername))
            //komentar nije kreirao trenutno logovani korisnik (stanar, firma ili institucija)
            throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);

        this.commentService.delete(comment.getId());

        return new ResponseEntity<>(HttpStatus.OK);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/damages/{damageId}/comments",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Create a comment resource.",
            notes = "You have to provide a valid IDs for building and damage in the URL.",
            httpMethod = "POST",
            produces = "application/json",
            consumes = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully created comment", response = CommentDTO.class),
            @ApiResponse(code = 400, message = "Inappropriate object sent in request body or building's/damage's ID in URL"),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT') or hasAuthority('INSTITUTION') or hasAuthority('COMPANY')")
    public ResponseEntity postComment(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The ID of the existing damage resource.", required = true) @PathVariable("damageId") Long damageId,
            @ApiParam(value = "The comment object.", required = true) @Valid @RequestBody CommentCreateOrUpdateDTO commentCreateOrUpdateDTO,
            @ApiParam(value = "The object that contains all errors from validation of DTO object") BindingResult errors) {

        Building building = this.buildingService.findOne(buildingId);
        Damage damage = this.damageService.findOne(damageId);

        // provera da li zadati kvar pripada zadatoj zgradi
        this.damageService.checkDamageInBuilding(damage, building.getId());

        Account account = this.accountService.findByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName());

        for(GrantedAuthority grantedAuthority : SecurityContextHolder.getContext().getAuthentication().getAuthorities())
        {
            if(grantedAuthority.getAuthority().equals(MessageConstants.TENANT_ROLE))
            {
                this.tenantService.checkPermissionForCurrentTenantInBuilding(building.getId());
                Tenant currentTenant = account.getTenant();
                // da bi mogao da komentarise kvar, stanar mora biti odgovorno lice za njega
                if(damage.getResponsiblePerson() == null ||
                        (damage.getResponsiblePerson() != null &&
                                !damage.getResponsiblePerson().getId().equals(currentTenant.getId())))
                    throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);

            }
            else if(grantedAuthority.getAuthority().equals(MessageConstants.INSTITUTION_ROLE))
            {
                Business business = account.getBusinessOwner();
                // da bi mogla da komentarise kvar, institucija mora biti odgovorna institucija za taj kvar
                // (koja pronalazi konkretno ko ce resiti taj kvar)
                // ili mora biti izabrana institucija za taj kvar (kao institucija koja ce kvar otkloniti)
                if((damage.getResponsibleInstitution() == null && damage.getSelectedBusiness() == null) ||
                        (damage.getResponsibleInstitution() != null &&
                                !business.getId().equals(damage.getResponsibleInstitution().getId()) &&
                                damage.getSelectedBusiness() != null &&
                                !business.getId().equals(damage.getSelectedBusiness().getId())))
                    throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);
            }
            else if(grantedAuthority.getAuthority().equals(MessageConstants.FIRM_ROLE))
            {
                Business business = account.getBusinessOwner();
                // da bi mogla da komentarise, firma mora biti izabrana za resavanje tog kvara
                if(damage.getSelectedBusiness() == null ||
                        (damage.getSelectedBusiness() != null &&
                                !business.getId().equals(damage.getSelectedBusiness().getId())))
                    throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);
            }
        }
        Comment comment = convertCommentCreateDTOToComment(commentCreateOrUpdateDTO);
        comment.setDate(new Date());
        comment.setCreator(account);
        comment.setDamage(damage);
        comment = commentService.save(comment);

        damage.getComments().add(comment);
        damageService.save(damage);
        return new ResponseEntity<>(new CommentDTO(comment), HttpStatus.CREATED);
    }
}
