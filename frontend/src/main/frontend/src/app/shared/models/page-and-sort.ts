export class PageAndSort {
  constructor(public pageNumber: string,
              public pageSize: string,
              public sortDirection: string,
              public sortProperties: string[]) { }
}
