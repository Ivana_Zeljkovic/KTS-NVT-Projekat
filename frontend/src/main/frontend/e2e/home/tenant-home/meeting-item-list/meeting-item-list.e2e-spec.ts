import { browser } from "protractor";
// page
import { ConfirmDeletePage } from "../../../page-objects/confirm-delete.po";
import { LoginPage } from "../../../page-objects/login.po";
import { MeetingItemListPage } from "../page-objects/meeting-item-list.po";
import { MeetingItemPage } from "../../../page-objects/meeting-item.po";
// helper
import { DateConverter } from "../../../util/date-converter";


describe('Meeting item list page', () => {
  let loginPage: LoginPage;
  let meetingItemListPage: MeetingItemListPage;
  let meetingItemPage: MeetingItemPage;
  let confirmDeletePage: ConfirmDeletePage;
  let newMeetingItem: any;

  beforeAll(() => {
    loginPage = new LoginPage();
    meetingItemListPage = new MeetingItemListPage();
    meetingItemPage = new MeetingItemPage();
    confirmDeletePage = new ConfirmDeletePage();
    newMeetingItem = {
      'title': 'Title of new meeting item',
      'content': 'Content of new meeting item', 'date': '', creator: ''
    };

    browser.get('/');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');

    loginPage.setUsername('kaca');
    loginPage.setPassword('kaca');
    loginPage.getSignInButton().click();

    browser.wait(function() {
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/home/notifications';
      });
    }).then(function() {
      browser.get('http://localhost:49152/#/home/meeting-details-items');
      expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/home/meeting-details-items');
      expect(meetingItemListPage.getMeetingItemListElement().isDisplayed()).toBe(true);
    });
  });

  afterAll(() => {
    browser.executeScript('window.localStorage.clear();');
  });



  it('should show list of meeting items when user is logged in and redirect to page' +
    ' with meeting items', () => {
    // expected view of page
    expect(meetingItemListPage.getSearchDateRangeInput().isDisplayed()).toBe(true);
    expect(meetingItemListPage.getSearchDateRangeSearchButton().isDisplayed()).toBe(true);
    expect(meetingItemListPage.getSearchDateRangeSearchButton().isEnabled()).toBe(true);
    expect(meetingItemListPage.getSearchDateRangeClearButton().isDisplayed()).toBe(true);
    expect(meetingItemListPage.getSearchDateRangeClearButton().isEnabled()).toBe(true);
    expect(meetingItemListPage.getAddButton().isDisplayed()).toBe(true);
    expect(meetingItemListPage.getAddButton().isEnabled()).toBe(true);

    // if list of meeting items is not empty, list is displayed
    if(meetingItemListPage.getMeetingItemList().isPresent() && meetingItemListPage.getMeetingItemList().isDisplayed()) {
      expect(meetingItemListPage.getMessageNoMeetingItemInList().isPresent()).toBe(false);
      expect(meetingItemListPage.getPanelsFromMeetingItemList().isPresent()).toBe(true);
      expect(meetingItemListPage.getPaginatorInMeetingItemList().isDisplayed()).toBe(true);

      let numberOfMeetingItems = {'value':0};
      meetingItemListPage.getPanelsFromMeetingItemList().count().then(value => {
          numberOfMeetingItems.value = value;
        });

      for(let i: number = 0; i<numberOfMeetingItems.value; i++) {
        expect(meetingItemListPage.getTitleFromMeetingItemPanel(i).isDisplayed()).toBe(true);
        expect(meetingItemListPage.getCreatorNameFromMeetingItemPanel(i).isDisplayed()).toBe(true);
        expect(meetingItemListPage.getDateFromMeetingItemPanel(i).isDisplayed()).toBe(true);
        expect(meetingItemListPage.getContentFromMeetingItemPanel(i).isDisplayed()).toBe(true);

        // if exist meeting item menu on panel (if logged user is creator of that meeting item
        // or meeting item contains questionnaire)
        // there must be one or three links (review questionnaire, edit and delete meeting item)
        if(meetingItemListPage.getMenuFromMeetingItemPanel(i).isPresent() &&
          meetingItemListPage.getMenuFromMeetingItemPanel(i).isDisplayed()) {
          meetingItemListPage.getMenuFromMeetingItemPanel(i).click();

          browser.wait(function() {
            return meetingItemListPage.getContentOfMenuFromMeetingItemPanel(i).isDisplayed();
          }).then(function() {
            meetingItemListPage.getMenuOptionsFromMeetingItemPanel().count().then(value => {
              if(value === 1) {
                // meeting item conatins questionnaire and current logged user isn't creator
                expect(meetingItemListPage.getQuestionnaireReviewOptionFromMeetingItemPanel(i).isDisplayed()).toBe(true);
              }
              else if(value === 2) {
                // meeting item doesn't contain questionnaire and current user is creator
                expect(meetingItemListPage.getEditMenuOptionFromMeetingItemPanel(i, 1).isDisplayed()).toBe(true);
                expect(meetingItemListPage.getDeleteMenuOptionFromMeetingItemPanel(i).isDisplayed()).toBe(true);
              }
              else {
                // meeting item contains questionnaire and current user is creator
                expect(meetingItemListPage.getQuestionnaireReviewOptionFromMeetingItemPanel(i).isDisplayed()).toBe(true);
                expect(meetingItemListPage.getEditMenuOptionFromMeetingItemPanel(i, 2).isDisplayed()).toBe(true);
                expect(meetingItemListPage.getDeleteMenuOptionFromMeetingItemPanel(i).isDisplayed()).toBe(true);
              }
            });
          });
        }
      }
    }
    // opposite, list is not displayed
    else {
      expect(meetingItemListPage.getMessageNoMeetingItemInList().isDisplayed()).toBe(true);
      expect(meetingItemListPage.getMessageNoMeetingItemInList().getText()).toEqual('There is still' +
        ' no suggestions of meeting item on this billboard...');
    }
  });



  it('should add new meeting item at first position in list', () => {
    // expected view of page
    expect(meetingItemListPage.getAddButton().isDisplayed()).toBe(true);
    expect(meetingItemListPage.getAddButton().isEnabled()).toBe(true);

    meetingItemListPage.getAddButton().click();

    browser.wait(function () {
      // expect to show modal - meeting item page for create new meeting item
      return meetingItemPage.getMeetingItemElement().isDisplayed();
    }).then(function () {
      // expect to modal has all elements
      expect(meetingItemPage.getTitleInput().isDisplayed()).toBe(true);
      expect(meetingItemPage.getContentInput().isDisplayed()).toBe(true);
      expect(meetingItemPage.getChooseDamageLink().isDisplayed()).toBe(true);
      expect(meetingItemPage.getQuestionnaireLink().isDisplayed()).toBe(true);
      expect(meetingItemPage.getSaveButton().isDisplayed()).toBe(true);
      expect(meetingItemPage.getSaveButton().isEnabled()).toBe(false);

      meetingItemPage.setTitle(newMeetingItem.title);
      meetingItemPage.setContent(newMeetingItem.content);
      meetingItemPage.getSaveButton().click();

      // expect that new meeting item is first in list include menu with edit and delete links
      expect(meetingItemListPage.getTitleFromMeetingItemPanel(0).getText()).toEqual(newMeetingItem.title);
      expect(meetingItemListPage.getContentFromMeetingItemPanel(0).getText()).toEqual(newMeetingItem.content);
      meetingItemListPage.getDateFromMeetingItemPanel(0).getText().then(value => {
        // difference between current time and time of new meeting item is less then 5sec
        let currentDate = new Date().getTime();
        newMeetingItem.date = value;
        let meetingItemDate = DateConverter.convertStringToDate(value).getTime();
        expect(currentDate - meetingItemDate).toBeLessThan(5000);
      });
      meetingItemListPage.getCreatorNameFromMeetingItemPanel(0).getText().then(value => {
        newMeetingItem.creator = value;
      });
    });
  });



  it('should open modal view for editing previous added meeting item when user click on Edit option ' +
    'from menu in meeting item panel', () => {
    expect(meetingItemListPage.getMenuFromMeetingItemPanel(0).isDisplayed()).toBe(true);
    meetingItemListPage.getMenuFromMeetingItemPanel(0).click();

    browser.wait(function() {
      return meetingItemListPage.getContentOfMenuFromMeetingItemPanel(0).isDisplayed();
    }).then(function() {
      expect(meetingItemListPage.getEditMenuOptionFromMeetingItemPanel(0, 1).isDisplayed()).toBe(true);
      expect(meetingItemListPage.getDeleteMenuOptionFromMeetingItemPanel(0).isDisplayed()).toBe(true);

      meetingItemListPage.getEditMenuOptionFromMeetingItemPanel(0, 1).click();

      browser.wait(function() {
        // expect to show modal - meeting item page for edit selected meeting item
        return meetingItemPage.getMeetingItemElement().isDisplayed();
      }).then(function() {
        // expect to modal has all elements
        expect(meetingItemPage.getTitleInput().isDisplayed()).toBe(true);
        expect(meetingItemPage.getContentInput().isDisplayed()).toBe(true);
        expect(meetingItemPage.getSaveButton().isDisplayed()).toBe(true);
        expect(meetingItemPage.getSaveButton().isEnabled()).toBe(false);

        let updatedMeetingItem = {'title': 'Updated title', 'content': 'Updated content'};

        meetingItemPage.getTitleInput().clear();
        meetingItemPage.setTitle(updatedMeetingItem.title);
        meetingItemPage.getContentInput().clear();
        meetingItemPage.setContent(updatedMeetingItem.content);
        meetingItemPage.getSaveButton().click().then(function() {
          // expect that modal is closed and updated meeting item has updated values for title and content fields,
          // but date and creator fields must be same as in newMeetingItem object
          expect(meetingItemListPage.getTitleFromMeetingItemPanel(0).getText()).toEqual(updatedMeetingItem.title);
          expect(meetingItemListPage.getContentFromMeetingItemPanel(0).getText()).toEqual(updatedMeetingItem.content);
          meetingItemListPage.getDateFromMeetingItemPanel(0).getText().then(value => {
            expect(value).toEqual(newMeetingItem.date);
          });
          meetingItemListPage.getCreatorNameFromMeetingItemPanel(0).getText().then(value => {
            expect(value).toEqual(newMeetingItem.creator);
          });
        });
      });
    });
  });



  it('should remove previous added and updated meeting item from list when user click on Delete option ' +
    'from menu in meeting item panel', () => {
    expect(meetingItemListPage.getMenuFromMeetingItemPanel(0).isDisplayed()).toBe(true);
    meetingItemListPage.getMenuFromMeetingItemPanel(0).click();

    browser.wait(function() {
      return meetingItemListPage.getContentOfMenuFromMeetingItemPanel(0).isDisplayed();
    }).then(function() {
      meetingItemListPage.getDeleteMenuOptionFromMeetingItemPanel(0).click();

      browser.wait(function() {
        // expect to show modal dialog for delete confirmation
        return confirmDeletePage.getConfirmDeleteElement().isDisplayed();
      }).then(function() {
        // expect to modal has all elements
        expect(confirmDeletePage.getConfirmButton().isDisplayed()).toBe(true);
        expect(confirmDeletePage.getConfirmButton().isEnabled()).toBe(true);
        expect(confirmDeletePage.getDeclineButton().isDisplayed()).toBe(true);
        expect(confirmDeletePage.getDeclineButton().isEnabled()).toBe(true);

        confirmDeletePage.getConfirmButton().click();

        // expect to first meeting item in list has date less then date of new meeting item (that we removed in previous line)
        // title, content, creator possibly can be equals
        meetingItemListPage.getDateFromMeetingItemPanel(0).getText().then(value => {
          let date = DateConverter.convertStringToDate(value).getTime();
          let removedMeetingItemDate = DateConverter.convertStringToDate(newMeetingItem.date).getTime();
          expect(removedMeetingItemDate - date).toBeGreaterThan(0);
        });
      });
    });
  });
});
