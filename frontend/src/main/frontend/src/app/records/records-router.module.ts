import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";
// component
import { RecordListComponent } from "./record-list/record-list.component";
import { RecordComponent } from "./record/record.component";


const routes: Routes = [
  { path: '', component: RecordListComponent },
  { path: ':id', component: RecordComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class RecordsRouterModule { }
