import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from "ngx-bootstrap";
// component
import { BusinessAddAccountModalComponent } from "../business-add-account/bussiness-add-account-modal.component";
// model
import { Login} from "../../models/login";
//service
import { JwtService } from "../../../core/services/jwt.service";


@Component({
  selector: 'app-business-profile-users-modal',
  templateUrl: './business-profile-accounts-modal.component.html',
  styleUrls: ['./business-profile-accounts-modal.component.css']
})
export class BusinessProfileAccountsModalComponent implements OnInit{
  @Input() accountsList: Login[];
  title: string = "Accounts";
  username: string;
  addAccountModalRef: BsModalRef;
  thisAccountModalRef: BsModalRef;

  constructor(private changeDetectorRef: ChangeDetectorRef, private jwtService: JwtService,
              private modalService: BsModalService) {
      this.username = this.jwtService.getUsernameFromToken();
  }

  ngOnInit() {
    this.changeDetectorRef.detectChanges();
  }

  displayAddAccount() {
    this.addAccountModalRef = this.modalService.show(BusinessAddAccountModalComponent);
    this.addAccountModalRef.content.usersList = this.accountsList;
    this.addAccountModalRef.content.bsModalRef = this.addAccountModalRef;
  }

  removeUsername(username) {
    let index = this.accountsList.indexOf(username);
    this.accountsList.splice(index, 1);
  }
}
