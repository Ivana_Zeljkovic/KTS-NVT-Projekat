package rs.ac.uns.ftn.ktsnvt.repository.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.model.entity.Authority;
import rs.ac.uns.ftn.ktsnvt.repository.AuthorityRepository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by Nikola Garabandic on 30/11/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class AuthorityRepositoryIntegrationTest {

    @Autowired
    private AuthorityRepository authorityRepository;


    @Test
    public void testFindByName(){
        /*
        * Test proverava ispravnost rada metode findByName repozitorijuma AuthorityRepository
        * (pronalazenje role na osnovu njenog imena)
        */

        // u bazi postoji rola sa name = TENANT
        Authority authority = authorityRepository.findByName("TENANT");

        assertNotNull(authority);
        assertEquals(new Long(2L), authority.getId());
        assertEquals("TENANT", authority.getName());
    }

    @Test
    public void testFindNoneByName(){
        /*
        * Test proverava ispravnost rada metode findByName repozitorijuma AuthorityRepository
        * (pronalazenje role na osnovu njenog imena)
        *
        */

        // u bazi ne postoji rola sa name = TENAANT
        Authority authority = authorityRepository.findByName("TENAANT");

        assertNull(authority);
    }
}
