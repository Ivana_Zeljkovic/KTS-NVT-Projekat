package rs.ac.uns.ftn.ktsnvt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Record;
import rs.ac.uns.ftn.ktsnvt.repository.RecordRepository;

import java.util.Date;

/**
 * Created by Nikola Garabandic on 22/11/2017.
 */
@Service
public class RecordServiceImpl implements RecordService {

    @Autowired
    private RecordRepository recordRepository;


    @Override
    @Transactional(readOnly = false)
    public Record save(Record record) {
        return this.recordRepository.save(record);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Record> findAllByBuildingId(Long buildingId, Integer pageNumber, Integer pageSize,
                                            String sortDirection, String sortProperty) {
        return this.recordRepository.findAllByBuildingId(buildingId, new Date(),
                new PageRequest(pageNumber, pageSize, (sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC),
                        sortProperty));
    }

    @Override
    @Transactional(readOnly = true)
    public void checkCouncilForRecord(Long recordId, Long councilId) {
        Record record = this.recordRepository.findOneByIdAndCouncilId(recordId, councilId);
        if(record == null) throw new BadRequestException("Selected record doesn't belong to selected building!");
    }

    @Override
    @Transactional(readOnly = true)
    public Record findOne(Long recordId) {
        Record record = this.recordRepository.findOne(recordId);
        if(record == null) throw new NotFoundException("Record not found!");
        return record;
    }
}
