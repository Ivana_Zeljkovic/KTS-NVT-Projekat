import { NgModule } from '@angular/core';
// module
import { SharedModule } from "../shared/shared.module";
import { HomeRouterModule } from "./home-router.module";
import { TenantHomeModule } from "./tenant-home/tenant-home.module";
import { DamagesModule } from "../damages/damages.module";
// component
import { HomeComponent } from "./home/home.component";
import { AdminHomeComponent } from './admin-home/admin-home.component';


@NgModule({
  imports: [
    SharedModule,
    HomeRouterModule,
    TenantHomeModule,
    DamagesModule
  ],
  declarations: [
    HomeComponent,
    AdminHomeComponent
  ]
})
export class HomeModule { }
