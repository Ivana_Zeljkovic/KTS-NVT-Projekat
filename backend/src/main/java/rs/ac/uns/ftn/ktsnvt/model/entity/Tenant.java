package rs.ac.uns.ftn.ktsnvt.model.entity;

import org.hibernate.annotations.Where;
import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "tenant")
@Where(clause="deleted=0")
public class Tenant {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @Column(nullable = false)
    private Date birthDate;

    @Column(nullable = false)
    @Email
    private String email;

    @Column(nullable = false)
    private String phoneNumber;

    @Column(nullable = false)
    private boolean confirmed;

    @OneToOne(mappedBy = "tenant")
    private Account account;

    @Column(nullable = false, columnDefinition = "INTEGER DEFAULT 0")
    @Version
    private int version;

    @Column(nullable = false, columnDefinition = "BOOL DEFAULT FALSE")
    private boolean deleted;

    @OneToMany(mappedBy = "owner", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private List<Apartment> personalApartments;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private Apartment apartmentLiveIn;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private Council council;

    @OneToMany(mappedBy = "responsiblePerson", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private List<Damage> responsibleForDamages;

    @OneToMany(mappedBy = "responsibleTenant", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<DamageResponsibility> responsibleForDamageTypes;

    @OneToMany(mappedBy = "creator", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Damage> myDamages;

    public Tenant() {
        this.personalApartments = new ArrayList<>();
        this.responsibleForDamages = new ArrayList<>();
        this.responsibleForDamageTypes = new ArrayList<>();
        this.myDamages = new ArrayList<>();
        this.confirmed = false;
    }

    public Tenant(String firstName, String lastName, Date birthDate, String email, String phoneNumber,
                  boolean confirmed, Account account) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.confirmed = confirmed;
        this.account = account;
        this.personalApartments = new ArrayList<>();
        this.responsibleForDamages = new ArrayList<>();
        this.responsibleForDamageTypes = new ArrayList<>();
        this.myDamages = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean getConfirmed() { return confirmed; }

    public void setConfirmed(Boolean confirmed) { this.confirmed = confirmed; }

    public Account getAccount() { return account; }

    public void setAccount(Account account) { this.account = account; }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public List<Apartment> getPersonalApartments() {
        return personalApartments;
    }

    public void setPersonalApartments(List<Apartment> personalApartments) { this.personalApartments = personalApartments; }

    public Apartment getApartmentLiveIn() {
        return apartmentLiveIn;
    }

    public void setApartmentLiveIn(Apartment apartmentLiveIn) {
        this.apartmentLiveIn = apartmentLiveIn;
    }

    public Council getCouncil() {
        return council;
    }

    public void setCouncil(Council council) {
        this.council = council;
    }

    public List<Damage> getResponsibleForDamages() { return responsibleForDamages; }

    public void setResponsibleForDamages(List<Damage> responsibleForDamages) { this.responsibleForDamages = responsibleForDamages; }

    public List<DamageResponsibility> getResponsibleForDamageTypes() { return responsibleForDamageTypes; }

    public void setResponsibleForDamageTypes(List<DamageResponsibility> responsibleForDamageTypes) { this.responsibleForDamageTypes = responsibleForDamageTypes; }

    public List<Damage> getMyDamages() { return myDamages; }

    public void setMyDamages(List<Damage> myDamages) { this.myDamages = myDamages; }

    public boolean findDamageWithIdInResponsibleForList(Long damageId) {
        for(Damage damage : this.responsibleForDamages) {
            if(damage.getId().equals(damageId))
                return true;
        }
        return false;
    }

    public int sizeOfResponsibleForNotFixedDamages() {
        int count = 0;
        for(Damage damage : this.responsibleForDamages) {
            if(!damage.isFixed()) count++;
        }
        return count;
    }
}
