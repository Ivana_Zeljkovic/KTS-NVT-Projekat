import { QuestionUpdate } from "./question-update";
import { Questionnaire } from "./questionnaire";
import {Question} from "./question";

export class QuestionnaireUpdate {
  private id: number;
  private questions: Array<QuestionUpdate>;

  constructor(questionnaireId: number, questions: Array<Question>) {
    this.questions = new Array<QuestionUpdate>();
    this.id = questionnaireId;

    questions.forEach(question => {
      this.questions.push(new QuestionUpdate(question.id, question.content, question.answers));
    })
  }
}
