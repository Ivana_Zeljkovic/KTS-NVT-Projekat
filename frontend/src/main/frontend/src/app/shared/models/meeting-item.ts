import { Tenant } from "./tenant";
import { Questionnaire } from "./questionnaire";

export interface MeetingItem {
  id: number;
  title: string;
  content: string;
  date: Date;
  creator: Tenant;
  questionnaire?: Questionnaire;
  numberOfElements?: number;
  damageId?: number;
  damageDescription?: string;
}
