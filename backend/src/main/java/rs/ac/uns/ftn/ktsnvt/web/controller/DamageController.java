package rs.ac.uns.ftn.ktsnvt.web.controller;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.ConflictException;
import rs.ac.uns.ftn.ktsnvt.exception.ForbiddenException;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.*;
import rs.ac.uns.ftn.ktsnvt.service.*;
import rs.ac.uns.ftn.ktsnvt.web.controller.util.MessageConstants;
import rs.ac.uns.ftn.ktsnvt.web.dto.*;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Created by Ivana Zeljkovic on 23/11/2017.
 */
@RestController
@RequestMapping(value = "/api")
@Api(value="damages", description="Operations pertaining to damages in application.")
public class DamageController {

    @Autowired
    private TenantService tenantService;

    @Autowired
    private DamageService damageService;

    @Autowired
    private DamageTypeService damageTypeService;

    @Autowired
    private DamageResponsibilityService damageResponsibilityService;

    @Autowired
    private BuildingService buildingService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private BusinessService businessService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private ApartmentService apartmentService;

    @Autowired
    private OfferService offerService;

    @Autowired
    private MeetingItemService meetingItemService;



    @RequestMapping(
            value = "/institutions/{institutionId}/responsible_for_damages",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get list of all damages for which the selected institution is responsible, in page form.",
            notes = "You have to provide a valid institution ID in the URL.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of damages."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('INSTITUTION')")
    public ResponseEntity getAllDamagesForInstitution(
            @ApiParam(value = "The ID of the existing institution resource.", required = true) @PathVariable("institutionId") Long institutionId,
            @ApiParam(value = "The page number for getting damages.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting damages.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting damages.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "First sort property for getting damages.", required = true) @RequestParam String sortProperty,
            @ApiParam(value = "Second sort property for getting damages.", required = true) @RequestParam String sortPropertySecond) {

        Business currentInstitution = this.businessService.findOne(institutionId);
        this.businessService.checkPermissionForCurrentBusiness(currentInstitution.getId());

        List<DamageDTO> damageDTOS = new ArrayList<>();

        Page<Damage> damages = this.damageService.getDamagesForInstitution(
                currentInstitution.getId(), Integer.valueOf(pageNumber),
                Integer.valueOf(pageSize), sortDirection, sortProperty, sortPropertySecond);

        damages.getContent().forEach(damage -> damageDTOS.add(new DamageDTO(damage, damages.getTotalElements())));

        return new ResponseEntity<>(damageDTOS, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/institutions/{institutionId}/responsible_for_damages_from_to",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a list of all damages for which are created in given period and the selected institution " +
                    "is responsible for them, in page form.",
            notes = "You have to provide a valid institution ID in the URL.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of damages", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to view these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")})
    @PreAuthorize("hasAuthority('INSTITUTION')")
    public ResponseEntity searchAllDamagesForInstitution(
            @ApiParam(value = "The ID of the existing institution resource.", required = true) @PathVariable("institutionId") Long institutionId,
            @ApiParam(value = "From date") @RequestParam("from") String from,
            @ApiParam(value = "To date") @RequestParam("to") String to,
            @ApiParam(value = "The page number for getting damages.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting damages.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting damages.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "The sort property for getting damages.", required = true) @RequestParam String sortProperty,
            @ApiParam(value = "Second sort property for getting damages.", required = true) @RequestParam String sortPropertySecond) {

        Business currentInstitution = this.businessService.findOne(institutionId);
        this.businessService.checkPermissionForCurrentBusiness(currentInstitution.getId());

        SimpleDateFormat sdf = new SimpleDateFormat(MessageConstants.DATE_FORMAT);
        Date fromDate = null;
        Date toDate = null;
        try {
            fromDate = sdf.parse(from);
            toDate = sdf.parse(to);
        } catch (Exception e) {
            throw new BadRequestException(MessageConstants.INVALID_FORMAT_OF_DATE_PARAMETER);
        }

        List<DamageDTO> damageDTOS = new ArrayList<>();

        Page<Damage> damages = this.damageService.getDamagesForInstitutionFromTo(currentInstitution.getId(), fromDate,
                toDate, Integer.valueOf(pageNumber),
                Integer.valueOf(pageSize), sortDirection, sortProperty, sortPropertySecond);
        damages.forEach(damage -> damageDTOS.add(new DamageDTO(damage, damages.getTotalElements())));

        return new ResponseEntity<>(damageDTOS, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/businesses/{businessId}/damages_for_repair",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a list of all damages which selected institution/firm should to repair, in page form.",
            notes = "You have to provide a valid institution/firm ID in the URL.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of damages."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('INSTITUTION') or hasAuthority('COMPANY')")
    public ResponseEntity getAllDamagesForRepair(
            @ApiParam(value = "The ID of the existing institution/firm resource.", required = true) @PathVariable("businessId") Long businessId,
            @ApiParam(value = "The page number for getting damages.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting damages.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting damages.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "First sort property for getting damages.", required = true) @RequestParam String sortProperty,
            @ApiParam(value = "Second sort property for getting damages.", required = true) @RequestParam String sortPropertySecond) {

        Business currentBusiness = this.businessService.findOne(businessId);
        this.businessService.checkPermissionForCurrentBusiness(currentBusiness.getId());

        List<DamageDTO> damageDTOS = new ArrayList<>();

        Page<Damage> damages = this.damageService.getDamagesForRepair(currentBusiness.getId(),
                Integer.valueOf(pageNumber), Integer.valueOf(pageSize), sortDirection, sortProperty, sortPropertySecond);

        damages.getContent().forEach(damage -> damageDTOS.add(new DamageDTO(damage, damages.getTotalElements())));

        return new ResponseEntity<>(damageDTOS, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/businesses/{businessId}/damages_for_repair_from_to",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a list of all damages for which are created in given period and the selected " +
                    "institution/firm should to repair them, in page form.",
            notes = "You have to provide a valid institution/firm ID in the URL.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of damages", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to view these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('INSTITUTION') or hasAuthority('COMPANY')")
    public ResponseEntity searchAllDamagesForRepair(
            @ApiParam(value = "The ID of the existing institution/firm resource.", required = true) @PathVariable("businessId") Long businessId,
            @ApiParam(value = "From date") @RequestParam("from") String from,
            @ApiParam(value = "To date") @RequestParam("to") String to,
            @ApiParam(value = "The page number for getting damages.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting damages.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting damages.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "The sort property for getting damages.", required = true) @RequestParam String sortProperty,
            @ApiParam(value = "Second sort property for getting damages.", required = true) @RequestParam String sortPropertySecond) {

        Business currentBusiness = this.businessService.findOne(businessId);
        this.businessService.checkPermissionForCurrentBusiness(currentBusiness.getId());

        SimpleDateFormat sdf = new SimpleDateFormat(MessageConstants.DATE_FORMAT);
        Date fromDate = null;
        Date toDate = null;
        try {
            fromDate = sdf.parse(from);
            toDate = sdf.parse(to);
        } catch (Exception e) {
            throw new BadRequestException(MessageConstants.INVALID_FORMAT_OF_DATE_PARAMETER);
        }

        List<DamageDTO> damageDTOS = new ArrayList<>();

        Page<Damage> damages = this.damageService.getDamagesForRepairFromTo(currentBusiness.getId(), fromDate,
                toDate, Integer.valueOf(pageNumber), Integer.valueOf(pageSize), sortDirection,
                sortProperty, sortPropertySecond);
        damages.forEach(damage -> damageDTOS.add(new DamageDTO(damage, damages.getTotalElements())));

        return new ResponseEntity<>(damageDTOS, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/businesses/{businessId}/damages_create_offer",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a list of all damages which selected institution/firm got request to send an offer, in page form.",
            notes = "You have to provide a valid institution/firm ID in the URL.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of damages I need to send an offer for."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('INSTITUTION') or hasAuthority('COMPANY')")
    public ResponseEntity getAllOfferDamages(
            @ApiParam(value = "The ID of the existing institution/firm resource.", required = true) @PathVariable("businessId") Long businessId,
            @ApiParam(value = "The page number for getting damages.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting damages.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting damages.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "First sort property for getting damages.", required = true) @RequestParam String sortProperty) {

        Business currentBusiness = this.businessService.findOne(businessId);

        this.businessService.checkPermissionForCurrentBusiness(currentBusiness.getId());
        //Pronalazenje svih damage-a za koje sam dobio ponudu a nisam poslao ponudu za njih.
        Page<Damage> damages = this.damageService.findAllDamageFixRequests(businessId, Integer.valueOf(pageNumber),
                Integer.valueOf(pageSize), sortDirection, sortProperty);

        List<DamageDTO> damageDtos = new ArrayList<>();

        damages.getContent().forEach(damage -> damageDtos.add(new DamageDTO(damage, damages.getTotalElements())));

        return new ResponseEntity<>(damageDtos, HttpStatus.OK);
    }




    @RequestMapping(
            value = "/businesses/{businessId}/damages/{damageId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a single damage for current institution/firm.",
            notes = "You have to provide valid IDs for business and damage in the URL.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved damage."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('INSTITUTION') or hasAuthority('COMPANY')")
    public ResponseEntity getDamageForBusiness(
            @ApiParam(value = "The ID of the existing business resource.", required = true) @PathVariable("businessId") Long businessId,
            @ApiParam(value = "The ID of the existing damage resource.", required = true) @PathVariable("damageId") Long damageId) {

        Business currentBusiness = this.businessService.findOne(businessId);
        this.businessService.checkPermissionForCurrentBusiness(currentBusiness.getId());

        Damage damage = this.damageService.findOne(damageId);

        // provera da li je kvar vec resen
        this.damageService.isDamageFixed(damage);

        boolean isFirm = currentBusiness.isIsCompany();
        boolean damageFound = true;

        // i u slucaju logovane firme i u slucaju logovane institucije, prvo se proverava
        // da li kvar sa zadatim ID-em postoji u listi kvarova koje ta logovana firma/institucija treba da resi
        //ili se nalazi u listi kvarova za koju treba da se posalje ponuda
        if (!currentBusiness.findDamageWithIdInRepairForList(damage.getId()) &&
                !(this.damageService.findDamageIdInOfferRequest(damageId,  businessId))) {
            damageFound = false;
            if(isFirm)
                throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);
        }

        if(damageFound)
            return new ResponseEntity<>(new DamageDTO(damage), HttpStatus.OK);

        for(GrantedAuthority authority : SecurityContextHolder.getContext().getAuthentication().getAuthorities()) {
            // dodatna provera se vrsi ako je logovana institucija, proverava se da li kvar pripada
            // listi kvarova za koje je ta institucija odgovorna
            if(authority.getAuthority().equals(MessageConstants.INSTITUTION_ROLE) &&
                !currentBusiness.findDamageWithIdInResponsibleForList(damage.getId()))
                    throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);
        }

        return new ResponseEntity<>(new DamageDTO(damage), HttpStatus.OK);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/damages",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get list of all damages in building where the current tenant is council president, in page form.",
            notes = "You have to provide a valid tenant ID.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of damages."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('COUNCIL_PRESIDENT')")
    public ResponseEntity getAllDamagesForPresident(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The page number for getting damages.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting damages.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting damages.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "First sort property for getting damages.", required = true) @RequestParam String sortProperty,
            @ApiParam(value = "Second sort property for getting damages.", required = true) @RequestParam String sortPropertySecond) {

        Building building = this.buildingService.findOne(buildingId);
        this.tenantService.checkPermissionForCurrentPresidentInBuilding(building);

        List<DamageDTO> damageDTOS = new ArrayList<>();

        Page<Damage> damages = this.damageService.getDamagesInBuilding(building.getId(), Integer.valueOf(pageNumber),
                Integer.valueOf(pageSize), sortDirection, sortProperty, sortPropertySecond);

        damages.getContent().forEach(damage -> damageDTOS.add(new DamageDTO(damage, damages.getTotalElements())));

        return new ResponseEntity<>(damageDTOS, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/damages_from_to",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a list of all damages for which are created in given period and they are in building " +
                    "where the current tenant is council president, in page form.",
            notes = "You have to provide a valid tenant ID.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of damages", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to view these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('COUNCIL_PRESIDENT')")
    public ResponseEntity searchAllDamagesForPresident(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "From date") @RequestParam("from") String from,
            @ApiParam(value = "To date") @RequestParam("to") String to,
            @ApiParam(value = "The page number for getting damages.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting damages.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting damages.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "The sort property for getting damages.", required = true) @RequestParam String sortProperty,
            @ApiParam(value = "Second sort property for getting damages.", required = true) @RequestParam String sortPropertySecond) {

        Building building = this.buildingService.findOne(buildingId);
        this.tenantService.checkPermissionForCurrentPresidentInBuilding(building);

        SimpleDateFormat sdf = new SimpleDateFormat(MessageConstants.DATE_FORMAT);
        Date fromDate = null;
        Date toDate = null;
        try {
            fromDate = sdf.parse(from);
            toDate = sdf.parse(to);
        } catch (Exception e) {
            throw new BadRequestException(MessageConstants.INVALID_FORMAT_OF_DATE_PARAMETER);
        }

        List<DamageDTO> damageDTOS = new ArrayList<>();

        Page<Damage> damages = this.damageService.getDamagesInBuildingFromTo(building.getId(), fromDate, toDate,
                Integer.valueOf(pageNumber), Integer.valueOf(pageSize), sortDirection, sortProperty, sortPropertySecond);
        damages.forEach(damage -> damageDTOS.add(new DamageDTO(damage, damages.getTotalElements())));

        return new ResponseEntity<>(damageDTOS, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/presidents/{presidentId}/damages/{damageId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get single damage for council president.",
            notes = "You have to provide a valid IDs for tenant and damage.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved damage."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "Request caused some error on server side")
    })
    @PreAuthorize("hasAuthority('COUNCIL_PRESIDENT')")
    public ResponseEntity getDamageForPresident(
            @ApiParam(value = "The ID of the existing tenant resource.", required = true) @PathVariable("presidentId") Long presidentId,
            @ApiParam(value = "The ID of the existing damage resource.", required = true) @PathVariable("damageId") Long damageId) {

        Tenant currentPresident = this.tenantService.findOne(presidentId);

        Damage damage = this.damageService.findOne(damageId);

        // provera da li kvar sa zadatim ID-em pripada zgradi u kojoj je trenutno ulogovani stanar predsednik skupstine
        boolean inBuilding = damage.getBuilding().getId().equals(
                currentPresident.getApartmentLiveIn().getBuilding().getId());

        if(!inBuilding) {
            this.damageService.isDamageFixed(damage);
        }

        // provera da li je trenutno ulogovani stanar odgovorna osoba za zadati kvar
        // ili je kvar u listi kvarova koji su prijavljeni u stanu ciji je vlasnik trenutno ulogovani stanar
        // ili je kvar kreiran od strane trenutno ulogovanog stanara
        if(!inBuilding && !currentPresident.findDamageWithIdInResponsibleForList(damage.getId()) &&
                !damage.getApartment().getOwner().getId().equals(currentPresident.getId()) &&
                !damage.getCreator().getId().equals(currentPresident.getId()))
            throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);

        return new ResponseEntity<>(new DamageDTO(damage), HttpStatus.OK);
    }



    @RequestMapping(
            value = "/tenants/{tenantId}/responsible_for_damages",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get list of all damages for which the current tenant is responsible, in page form.",
            notes = "You have to provide a valid tenant ID in the URL.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of damages."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity getAllDamagesForTenant(
            @ApiParam(value = "The ID of the existing tenant resource.", required = true) @PathVariable("tenantId") Long tenantId,
            @ApiParam(value = "The page number for getting damages.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting damages.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting damages.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "First sort property for getting damages.", required = true) @RequestParam String sortProperty,
            @ApiParam(value = "Second sort property for getting damages.", required = true) @RequestParam String sortPropertySecond) {

        Tenant currentTenant = this.tenantService.findOne(tenantId);
        this.tenantService.checkPermissionForCurrentTenant(tenantId);

        List<DamageDTO> damageDTOS = new ArrayList<>();

        Page<Damage> damages = this.damageService.getDamagesForTenant(currentTenant.getId(),
                Integer.valueOf(pageNumber), Integer.valueOf(pageSize), sortDirection, sortProperty, sortPropertySecond);

        damages.getContent().forEach(damage -> damageDTOS.add(new DamageDTO(damage, damages.getTotalElements())));

        return new ResponseEntity<>(damageDTOS, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/tenants/{tenantId}/responsible_for_damages_from_to",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a list of all damages for which are created in given period and current tenant " +
                    "is responsible for them, in page form.",
            notes = "You have to provide a valid tenant ID in the URL.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of damages", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to view these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity searchAllDamagesForTenant(
            @ApiParam(value = "The ID of the existing tenant resource.", required = true) @PathVariable("tenantId") Long tenantId,
            @ApiParam(value = "From date") @RequestParam("from") String from,
            @ApiParam(value = "To date") @RequestParam("to") String to,
            @ApiParam(value = "The page number for getting damages.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting damages.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting damages.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "The sort property for getting damages.", required = true) @RequestParam String sortProperty,
            @ApiParam(value = "Second sort property for getting damages.", required = true) @RequestParam String sortPropertySecond) {

        Tenant currentTenant = this.tenantService.findOne(tenantId);
        this.tenantService.checkPermissionForCurrentTenant(tenantId);

        SimpleDateFormat sdf = new SimpleDateFormat(MessageConstants.DATE_FORMAT);
        Date fromDate = null;
        Date toDate = null;
        try {
            fromDate = sdf.parse(from);
            toDate = sdf.parse(to);
        } catch (Exception e) {
            throw new BadRequestException(MessageConstants.INVALID_FORMAT_OF_DATE_PARAMETER);
        }

        List<DamageDTO> damageDTOS = new ArrayList<>();

        Page<Damage> damages = this.damageService.getDamagesForTenantFromTo(currentTenant.getId(), fromDate, toDate,
                Integer.valueOf(pageNumber), Integer.valueOf(pageSize), sortDirection, sortProperty, sortPropertySecond);
        damages.forEach(damage -> damageDTOS.add(new DamageDTO(damage, damages.getTotalElements())));

        return new ResponseEntity<>(damageDTOS, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/tenants/{tenantId}/personal_apartments/damages",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get list of all damages in personal apartments of current tenant, in page form.",
            notes = "You have to provide a valid tenant ID in the URL.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of damages."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity getAllDamagesInPersonalApartmentsForTenant(
            @ApiParam(value = "The ID of the existing tenant resource.", required = true) @PathVariable("tenantId") Long tenantId,
            @ApiParam(value = "The page number for getting damages.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting damages.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting damages.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "First sort property for getting damages.", required = true) @RequestParam String sortProperty,
            @ApiParam(value = "Second sort property for getting damages.", required = true) @RequestParam String sortPropertySecond) {

        Tenant currentTenant = this.tenantService.findOne(tenantId);
        this.tenantService.checkPermissionForCurrentTenant(tenantId);

        List<DamageDTO> damageDTOS = new ArrayList<>();
        // salju se svi kvarovi koji su vezani za bilo koji stan ciji je vlasnik trenutno ulogovani stanar,
        // bez obzira u kojoj je zgradi taj njegov stan
        Page<Damage> damages = this.damageService.getDamagesInPersonalApartments(currentTenant.getId(),
                Integer.valueOf(pageNumber), Integer.valueOf(pageSize), sortDirection, sortProperty, sortPropertySecond);

        damages.getContent().forEach(damage -> damageDTOS.add(new DamageDTO(damage, damages.getTotalElements())));

        return new ResponseEntity<>(damageDTOS, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/tenants/{tenantId}/personal_apartments/damages_from_to",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a list of all damages for which are created in given period and they are in " +
                    "personal apartments of current tenant, in page form.",
            notes = "You have to provide a valid tenant ID in the URL.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of damages", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to view these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity searchAllDamagesInPersonalApartmentsForTenant(
            @ApiParam(value = "The ID of the existing tenant resource.", required = true) @PathVariable("tenantId") Long tenantId,
            @ApiParam(value = "From date") @RequestParam("from") String from,
            @ApiParam(value = "To date") @RequestParam("to") String to,
            @ApiParam(value = "The page number for getting damages.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting damages.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting damages.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "The sort property for getting damages.", required = true) @RequestParam String sortProperty,
            @ApiParam(value = "Second sort property for getting damages.", required = true) @RequestParam String sortPropertySecond) {

        Tenant currentTenant = this.tenantService.findOne(tenantId);
        this.tenantService.checkPermissionForCurrentTenant(tenantId);

        SimpleDateFormat sdf = new SimpleDateFormat(MessageConstants.DATE_FORMAT);
        Date fromDate = null;
        Date toDate = null;
        try {
            fromDate = sdf.parse(from);
            toDate = sdf.parse(to);
        } catch (Exception e) {
            throw new BadRequestException(MessageConstants.INVALID_FORMAT_OF_DATE_PARAMETER);
        }

        List<DamageDTO> damageDTOS = new ArrayList<>();

        Page<Damage> damages = this.damageService.getDamagesInPersonalApartmentsFromTo(currentTenant.getId(),
                fromDate, toDate, Integer.valueOf(pageNumber), Integer.valueOf(pageSize),
                sortDirection, sortProperty, sortPropertySecond);
        damages.forEach(damage -> damageDTOS.add(new DamageDTO(damage, damages.getTotalElements())));

        return new ResponseEntity<>(damageDTOS, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/tenants/{tenantId}/apartment_live_in/damages",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get list of all damages in apartment where current tenant lives, in page form.",
            notes = "You have to provide a valid tenant ID in the URL.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of damages."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity getAllDamagesInApartmentForTenant(
            @ApiParam(value = "The ID of the existing tenant resource.", required = true) @PathVariable("tenantId") Long tenantId,
            @ApiParam(value = "The page number for getting damages.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting damages.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting damages.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "First sort property for getting damages.", required = true) @RequestParam String sortProperty,
            @ApiParam(value = "Second sort property for getting damages.", required = true) @RequestParam String sortPropertySecond) {

        Tenant currentTenant = this.tenantService.findOne(tenantId);
        this.tenantService.checkPermissionForCurrentTenant(tenantId);

        List<DamageDTO> damageDTOS = new ArrayList<>();
        // salju se svi kvarovi koji su vezani za stan u kom zivi trenutno logovani stanar
        if(currentTenant.getApartmentLiveIn() != null) {
            Page<Damage> damages = this.damageService.getDamagesInApartment(currentTenant.getApartmentLiveIn().getId(),
                    Integer.valueOf(pageNumber), Integer.valueOf(pageSize), sortDirection,
                    sortProperty, sortPropertySecond);

            damages.getContent().forEach(damage -> damageDTOS.add(new DamageDTO(damage, damages.getTotalElements())));
        }

        return new ResponseEntity<>(damageDTOS, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/tenants/{tenantId}/apartment_live_in/damages_from_to",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a list of all damages for which are created in given period and they are in " +
                    "apartment where current tenant lives, in page form.",
            notes = "You have to provide a valid tenant ID in the URL.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of damages", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to view these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity searchAllDamagesInApartmentForTenant(
            @ApiParam(value = "The ID of the existing tenant resource.", required = true) @PathVariable("tenantId") Long tenantId,
            @ApiParam(value = "From date") @RequestParam("from") String from,
            @ApiParam(value = "To date") @RequestParam("to") String to,
            @ApiParam(value = "The page number for getting damages.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting damages.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting damages.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "The sort property for getting damages.", required = true) @RequestParam String sortProperty,
            @ApiParam(value = "Second sort property for getting damages.", required = true) @RequestParam String sortPropertySecond) {

        Tenant currentTenant = this.tenantService.findOne(tenantId);
        this.tenantService.checkPermissionForCurrentTenant(tenantId);

        SimpleDateFormat sdf = new SimpleDateFormat(MessageConstants.DATE_FORMAT);
        Date fromDate = null;
        Date toDate = null;
        try {
            fromDate = sdf.parse(from);
            toDate = sdf.parse(to);
        } catch (Exception e) {
            throw new BadRequestException(MessageConstants.INVALID_FORMAT_OF_DATE_PARAMETER);
        }

        List<DamageDTO> damageDTOS = new ArrayList<>();

        // salju se svi kvarovi koji su vezani za stan u kom zivi trenutno logovani stanar
        if(currentTenant.getApartmentLiveIn() != null) {
            Page<Damage> damages = this.damageService.getDamagesInApartmentFromTo(
                    currentTenant.getApartmentLiveIn().getId(), fromDate, toDate, Integer.valueOf(pageNumber),
                    Integer.valueOf(pageSize), sortDirection, sortProperty, sortPropertySecond);
            damages.forEach(damage -> damageDTOS.add(new DamageDTO(damage, damages.getTotalElements())));
        }

        return new ResponseEntity<>(damageDTOS, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/tenants/{tenantId}/my_damages",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get list of all damages that current tenant created, in page form.",
            notes = "You have to provide a valid tenant ID in the URL.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of damages."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity getMyDamages(
            @ApiParam(value = "The ID of the existing tenant resource.", required = true) @PathVariable("tenantId") Long tenantId,
            @ApiParam(value = "The page number for getting damages.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting damages.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting damages.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "First sort property for getting damages.", required = true) @RequestParam String sortProperty,
            @ApiParam(value = "Second sort property for getting damages.", required = true) @RequestParam String sortPropertySecond) {

        Tenant currentTenant = this.tenantService.findOne(tenantId);
        this.tenantService.checkPermissionForCurrentTenant(tenantId);

        List<DamageDTO> damageDTOS = new ArrayList<>();
        // salju se svi kvarovi koje je kreirao trenutno logovani stanar
        Page<Damage> damages = this.damageService.getMyDamages(currentTenant.getId(), Integer.valueOf(pageNumber),
                Integer.valueOf(pageSize), sortDirection, sortProperty, sortPropertySecond);

        damages.getContent().forEach(damage -> damageDTOS.add(new DamageDTO(damage, damages.getTotalElements())));

        return new ResponseEntity<>(damageDTOS, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/tenants/{tenantId}/my_damages_from_to",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a list of all damages for which are created in given period and they are created by " +
                    "current tenant, in page form.",
            notes = "You have to provide a valid tenant ID in the URL.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of damages", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to view these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity searchMyDamages(
            @ApiParam(value = "The ID of the existing tenant resource.", required = true) @PathVariable("tenantId") Long tenantId,
            @ApiParam(value = "From date") @RequestParam("from") String from,
            @ApiParam(value = "To date") @RequestParam("to") String to,
            @ApiParam(value = "The page number for getting damages.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting damages.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting damages.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "The sort property for getting damages.", required = true) @RequestParam String sortProperty,
            @ApiParam(value = "Second sort property for getting damages.", required = true) @RequestParam String sortPropertySecond) {

        Tenant currentTenant = this.tenantService.findOne(tenantId);
        this.tenantService.checkPermissionForCurrentTenant(tenantId);

        SimpleDateFormat sdf = new SimpleDateFormat(MessageConstants.DATE_FORMAT);
        Date fromDate = null;
        Date toDate = null;
        try {
            fromDate = sdf.parse(from);
            toDate = sdf.parse(to);
        } catch (Exception e) {
            throw new BadRequestException(MessageConstants.INVALID_FORMAT_OF_DATE_PARAMETER);
        }

        List<DamageDTO> damageDTOS = new ArrayList<>();

        Page<Damage> damages = this.damageService.getMyDamagesFromTo(currentTenant.getId(), fromDate, toDate,
                Integer.valueOf(pageNumber), Integer.valueOf(pageSize), sortDirection, sortProperty, sortPropertySecond);
        damages.forEach(damage -> damageDTOS.add(new DamageDTO(damage, damages.getTotalElements())));

        return new ResponseEntity<>(damageDTOS, HttpStatus.OK);
    }




    @RequestMapping(
            value = "/tenants/{tenantId}/damages/{damageId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a single damage for current tenant.",
            notes = "You have to provide valid IDs for tenant and damage in the URL.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved damage."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity getDamageForTenant(
            @ApiParam(value = "The ID of the existing tenant resource.", required = true) @PathVariable("tenantId") Long tenantId,
            @ApiParam(value = "The ID of the existing damage resource.", required = true) @PathVariable("damageId") Long damageId) {

        Tenant currentTenant = this.tenantService.findOne(tenantId);
        this.tenantService.checkPermissionForCurrentTenant(currentTenant.getId());

        Damage damage = this.damageService.findOne(damageId);

        // provera da li je kvar vec resen
        this.damageService.isDamageFixed(damage);

        // provera da li je kvar u listi kvarova za koje je odgovoran trenutno logovani stanar
        // ili je u listi kvarova u nekom od stanova ciji je vlasnik trenutno logovani stanar
        // ili je u listi kvarova u stanu u kom zivi trenutno logovani stanar
        // ili je u listi kvarova koje je kreirao trenutno logovani stanar
        if(!currentTenant.findDamageWithIdInResponsibleForList(damage.getId()) &&
                !damage.getApartment().getOwner().getId().equals(currentTenant.getId()) &&
                !damage.getApartment().getId().equals(currentTenant.getApartmentLiveIn().getId()) &&
                !damage.getCreator().getId().equals(currentTenant.getId()))
            throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);

        return new ResponseEntity<>(new DamageDTO(damage), HttpStatus.OK);
    }



    @RequestMapping(
            value = "/tenants/{tenantId}/my_damages/{damageId}",
            method = RequestMethod.DELETE
    )
    @ApiOperation(
            value = "Delete damage resource.",
            notes = "You have to provide valid IDs for tenant and damage in the URL. " +
                    "Once deleted the resource can not be recovered.",
            httpMethod = "DELETE"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully deleted damage."),
            @ApiResponse(code = 400, message = "Damage already in the repair process."),
            @ApiResponse(code = 401, message = "You are not authorized to delete the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity deleteMyDamage(
            @ApiParam(value = "The ID of the existing tenant resource.", required = true) @PathVariable("tenantId") Long tenantId,
            @ApiParam(value = "The ID of the existing damage resource.", required = true) @PathVariable("damageId") Long damageId) {

        Tenant currentTenant = this.tenantService.findOne(tenantId);
        this.tenantService.checkPermissionForCurrentTenant(currentTenant.getId());

        Damage damage = this.damageService.findOne(damageId);

        // provera da li je kvar vec resen
        this.damageService.isDamageFixed(damage);

        if(!damage.getCreator().getId().equals(currentTenant.getId()))
            // kvar ciji je ID zadat nije kreirao trenutno logovani stanar
            throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);

        if(damage.getSelectedBusiness() != null)
            // kvar je vec prosledjen nekoj od firmi/institucija za resavanje i ne moze se obrisati
            throw new BadRequestException("Damage is already in proccess of fixing!");

        // provera da li je kvar referenciran iz neke tacke dnevnog reda
        this.meetingItemService.containsDamage(damage.getId());

        this.damageService.remove(damage.getId());
        return new ResponseEntity<>(HttpStatus.OK);
    }



    @RequestMapping(
            value = "/tenants/{tenantId}/my_damages/{id}",
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Update damage resource.",
            notes = "You have to provide valid IDs for tenant and damage in the URL. " +
                    "Method returns the damage being updated.",
            httpMethod = "PUT",
            consumes = "application/json",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated damage.", response = DamageDTO.class),
            @ApiResponse(code = 400, message = "Inappropriate damage object sent in request body or damage already in the repair process."),
            @ApiResponse(code = 401, message = "You are not authorized to update the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity updateMyDamage(
            @ApiParam(value = "The ID of the existing tenant resource.", required = true) @PathVariable("tenantId") Long tenantId,
            @ApiParam(value = "The ID of the existing damage resource.", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "The damage object.", required = true) @Valid @RequestBody DamageUpdateDTO damageUpdateDTO,
            @ApiParam(value = "The object that contains all errors from validation of DTO object") BindingResult errors) {

        Tenant currentTenant = this.tenantService.findOne(tenantId);
        this.tenantService.checkPermissionForCurrentTenant(currentTenant.getId());

        Damage damage = this.damageService.findOne(id);

        // provera da li je kvar vec resen
        this.damageService.isDamageFixed(damage);

        if(!damage.getCreator().getId().equals(currentTenant.getId()))
            // kvar ciji je ID zadat nije kreirao trenutno logovani stanar
            throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);

        if(damage.getSelectedBusiness() != null)
            // kvar je vec prosledjen nekoj od firmi/institucija za resavanje i ne moze se azurirati
            throw new BadRequestException("Damage is already in process of fixing!");

        damage.setUrgent(damageUpdateDTO.isUrgent());
        damage.setDescription(damageUpdateDTO.getDescription());

        damage = this.damageService.save(damage);
        return new ResponseEntity<>(new DamageDTO(damage), HttpStatus.OK);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/damages",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Create a damage resource.",
            notes = "You have to provide a valid building ID in the URL. " +
                    "Method returns the damage being saved.",
            httpMethod = "POST",
            produces = "application/json",
            consumes = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully created damage resource", response = DamageDTO.class),
            @ApiResponse(code = 400, message = "Inappropriate object sent in request body"),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity reportDamage(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The object that represents new damage with necessary values.", required = true) @Valid @RequestBody DamageCreateDTO damageCreateDTO,
            @ApiParam(value = "The object that contains all errors from validation of DTO object") BindingResult errors) {

        Building building = this.buildingService.findOne(buildingId);
        this.tenantService.checkPermissionForCurrentTenantInBuilding(building.getId());

        Apartment apartment = null;
        if(damageCreateDTO.getApartmentId() != null) {
            apartment = this.apartmentService.findOne(damageCreateDTO.getApartmentId());

            if(!apartment.getBuilding().getId().equals(building.getId()))
                throw new BadRequestException("Selected apartment doesn't belong to selected building!");
        }

        DamageType damageType = this.damageTypeService.findByName(damageCreateDTO.getTypeName());

        Tenant currentTenant = this.accountService.findByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()).getTenant();

        Damage damage = new Damage(damageCreateDTO.getDescription(), new Date(),
                damageType, damageCreateDTO.isUrgent(), currentTenant);
        if(damageCreateDTO.getImage() != null && !damageCreateDTO.getImage().equals(""))
            damage.setImage(damageCreateDTO.getImage());
        DamageResponsibility damageResponsibility = this.damageResponsibilityService.findOneByDamageTypeAndBuilding(
                damageType.getId(), buildingId);

        if(damageResponsibility == null) {
            Tenant councilPresident = building.getCouncil().getPresident();
            // ukoliko ne postoji odgovorna osoba ili institucija koja je zaduzena za vrstu kvara
            // (koju poseduje i kvar koji se trenutno kreira) u zgradi u kojoj se kvar prijavljuje,
            // predsednik skupstine stanara te zgrade se proglasava odgovornim licem za taj kvar dok on ne odluci drugacije
            if(councilPresident != null) {
                councilPresident.getResponsibleForDamages().add(damage);
                damage.setResponsiblePerson(councilPresident);
            }
            // ukoliko trenutno ne postoji predsednik skupstine stanara na nivou zgrade u kojoj se prijavljuje kvar,
            // kao odgovorna osoba za prijavljeni kvar se postavlja sam stanar koji je kvar i prijavio
            else {
                currentTenant.getResponsibleForDamages().add(damage);
                damage.setResponsiblePerson(currentTenant);
            }
        }
        else {
            // ukoliko na nivou zgrade za zadati tip kvara postoji odgovorna osoba i/ili odgovorna institucija,
            // te vrednosti se referenciraju i na kvar koji se kreira
            if(damageResponsibility.getResponsibleTenant() != null)
                damage.setResponsiblePerson(damageResponsibility.getResponsibleTenant());
            if(damageResponsibility.getResponsibleInstitution() != null)
                damage.setResponsibleInstitution(damageResponsibility.getResponsibleInstitution());
        }

        if(apartment != null) {
            damage.setApartment(apartment);
            apartment.getDamages().add(damage);
        }
        damage.setBuilding(building);
        building.getDamages().add(damage);

        damage = this.damageService.save(damage);
        return new ResponseEntity<>(new DamageDTO(damage), HttpStatus.CREATED);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/damages/{damageId}/responsible_institution",
            method = RequestMethod.PATCH,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Set responsible institution for selected damage in selected building.",
            notes = "You have to provide valid IDs for building and damage in the URL.",
            httpMethod = "PATCH",
            produces = "application/json",
            consumes = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated damage responsibility", response = DamageDTO.class),
            @ApiResponse(code = 400, message = "Inappropriate object sent in request body"),
            @ApiResponse(code = 401, message = "You are not authorized to update the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('COUNCIL_PRESIDENT')")
    public ResponseEntity setResponsibleInstitutionForDamage(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The ID of the existing damage resource.", required = true) @PathVariable("damageId") Long damageId,
            @ApiParam(value = "The object with institution ID.", required = true) @Valid @RequestBody ResponsibleForDamageTypeDTO responsibleForDamageDTO,
            @ApiParam(value = "The object that contains all errors from validation of DTO object") BindingResult errors) {

        Building building = this.buildingService.findOne(buildingId);
        Damage damage = this.damageService.findOne(damageId);

        this.damageService.isDamageFixed(damage);

        this.tenantService.checkPermissionForCurrentPresidentInBuilding(building);

        // provera da li zadati kvar pripada zadatoj zgradi
        this.damageService.checkDamageInBuilding(damage, building.getId());

        Business institution = this.businessService.findOne(responsibleForDamageDTO.getId());

        // ukoliko kvar vec poseduje odgovornu instituciju, predsednik je ne moze izmeniti ponovo
        if(damage.getResponsibleInstitution() != null)
            throw new BadRequestException("Selected damage already has responsible institution!");

        if(institution.isIsCompany())
            throw new BadRequestException("You have to mark institution (not firm) as responsible!");

        institution.getResponsibleForDamages().add(damage);
        damage.setResponsibleInstitution(institution);
        damage = this.damageService.save(damage);

        return new ResponseEntity<>(new DamageDTO(damage), HttpStatus.OK);
    }



    @RequestMapping(
            value = "/institutions/{institutionId}/responsible_for_damages/{damageId}/delegate_responsibility",
            method = RequestMethod.PATCH,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Delegate responsibility to another institution for selected damage.",
            notes = "You have to provide valid IDs for institution and damage in the URL.",
            httpMethod = "PATCH",
            produces = "application/json",
            consumes = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated damage responsibility", response = DamageDTO.class),
            @ApiResponse(code = 400, message = "Inappropriate object sent in request body or institution's/damage's ID sent in URL"),
            @ApiResponse(code = 401, message = "You are not authorized to update the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('INSTITUTION')")
    public ResponseEntity delegateResponsibilityToInstitutionForDamage(
            @ApiParam(value = "The ID of the existing institution resource.", required = true) @PathVariable("institutionId") Long institutionId,
            @ApiParam(value = "The ID of the existing damage resource.", required = true) @PathVariable("damageId") Long damageId,
            @ApiParam(value = "The object with institution ID.", required = true) @Valid @RequestBody ResponsibleForDamageTypeDTO responsibleInstitutionForDamageDTO,
            @ApiParam(value = "The object that contains all errors from validation of DTO object") BindingResult errors) {

        Business currentInstitution = this.businessService.findOne(institutionId);
        if(currentInstitution.isIsCompany()) throw new NotFoundException("Institution not found!");

        this.businessService.checkPermissionForCurrentBusiness(institutionId);

        Damage damage = this.damageService.findOne(damageId);
        this.damageService.isDamageFixed(damage);

        Business institution = this.businessService.findOne(responsibleInstitutionForDamageDTO.getId());

        // ukoliko kvar sa zadatim ID-em nije kvar za koji je odgovorna trenutno ulogovana institucija
        if(!currentInstitution.findDamageWithIdInResponsibleForList(damage.getId()))
            throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);

        // ukoliko novoizabrana institucija za delegiranje odgovornosti nije institucija nego firma
        if (institution.isIsCompany())
            throw new BadRequestException("You can't delegate responsibility to firm, just on another institution!");

        // uklanjanje odgovornosti za zadati kvar kod trenutno logovane institucije
        currentInstitution.getResponsibleForDamages().remove(damage);
        // dodavanje te odgovornosti novoizbranoj instituciji
        damage.setResponsibleInstitution(institution);
        institution.getResponsibleForDamages().add(damage);
        damage = this.damageService.save(damage);

        return new ResponseEntity<>(new DamageDTO(damage), HttpStatus.OK);
    }



    @RequestMapping(
            value = "/tenants/{tenantId}/responsible_for_damages/{damageId}/delegate_responsibility",
            method = RequestMethod.PATCH,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Delegate responsibility to another tenant for selected damage.",
            notes = "You have to provide valid IDs for tenant and damage in the URL.",
            httpMethod = "PATCH",
            produces = "application/json",
            consumes = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated damage responsibility", response = DamageDTO.class),
            @ApiResponse(code = 400, message = "Inappropriate object sent in request body or tenant's ID sent in URL"),
            @ApiResponse(code = 401, message = "You are not authorized to update the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity delegateResponsibilityToPersonForDamage(
            @ApiParam(value = "The ID of the existing tenant resource.", required = true) @PathVariable("tenantId") Long tenantId,
            @ApiParam(value = "The ID of the existing damage resource.", required = true) @PathVariable("damageId") Long damageId,
            @ApiParam(value = "The object with tenant ID.", required = true) @Valid @RequestBody ResponsibleForDamageTypeDTO responsiblePersonForDamageDTO,
            @ApiParam(value = "The object that contains all errors from validation of DTO object") BindingResult errors) {

        Tenant currentTenant = this.tenantService.findOne(tenantId);
        this.tenantService.checkPermissionForCurrentTenant(tenantId);

        Damage damage = this.damageService.findOne(damageId);
        this.damageService.isDamageFixed(damage);

        Tenant tenant = this.tenantService.findOne(responsiblePersonForDamageDTO.getId());

        // ukoliko kvar sa zadatim ID-em nije kvar za koji je odgovoran trenutno ulogovani stanar
        if(!currentTenant.findDamageWithIdInResponsibleForList(damage.getId()))
            throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);

        // ukoliko stanar kom trenutno logovani stanar zeli da delegira odgovornost ne stanuje u
        // istoj zgradi (kao i trenutno logovani) delegiranj odgovornosti nije moguce
        if (!tenant.getApartmentLiveIn().getBuilding().getId().equals(
                currentTenant.getApartmentLiveIn().getBuilding().getId()))
            throw new BadRequestException("Tenant who you want to delegate your " +
                    "responsibility doesn't live in your building!");

        // uklanjanje odgovornosti za zadati kvar kod trenutno logovanog korisnika
        currentTenant.getResponsibleForDamages().remove(damage);
        // dodavanje te odgovornosti novoizabranom stanaru
        damage.setResponsiblePerson(tenant);
        tenant.getResponsibleForDamages().add(damage);
        damage = this.damageService.save(damage);

        return new ResponseEntity<>(new DamageDTO(damage), HttpStatus.OK);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/damages/{damageId}/firm_or_institution_for_fixing",
            method = RequestMethod.PATCH,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Select some firm/institution for repair of selected damage.",
            notes = "You have to provide a valid IDs for building and damage in the URL.",
            httpMethod = "PATCH",
            consumes = "application/json",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully selected firm/institution for repair of damage"),
            @ApiResponse(code = 400, message = "Inappropriate object sent in request body or building's/damage's ID sent in URL"),
            @ApiResponse(code = 401, message = "You are not authorized for this action."),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('INSTITUTION') or hasAuthority('TENANT')")
    public ResponseEntity selectFirmForFixingDamage(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The ID of the existing damage resource.", required = true) @PathVariable("damageId") Long damageId,
            @ApiParam(value = "The object with id of selected firm/institution and id of offer (if necessary).", required = true)
            @Valid @RequestBody SelectedBusinessForDamageRepairDTO selectedBusinessForDamageRepairDTO,
            @ApiParam(value = "The object that contains all errors from validation of DTO object") BindingResult errors) {

        Building building = this.buildingService.findOne(buildingId);
        Damage damage = this.damageService.findOne(damageId);

        // provera da li zadati kvar pripada zadatoj zgradi
        this.damageService.checkDamageInBuilding(damage, building.getId());

        if(damage.getSelectedBusiness() != null)
            throw new BadRequestException("Selected firm/institution for repairing selected damage already exist!");

        Business business = this.businessService.findOne(selectedBusinessForDamageRepairDTO.getId());

        Account account = this.accountService.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());

        for(GrantedAuthority authority : SecurityContextHolder.getContext().getAuthentication().getAuthorities()) {
            if(authority.getAuthority().equals(MessageConstants.INSTITUTION_ROLE)) {
                Business currentInstitution = account.getBusinessOwner();
                // ukoliko ne postoji odgovorna institucija ili
                // trenutno logovana institucija nije ujedno i odgovorna institucija za ovaj kvar
                if(damage.getResponsibleInstitution() == null ||
                        (damage.getResponsibleInstitution() != null &&
                                !damage.getResponsibleInstitution().getId().equals(currentInstitution.getId())))
                    throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);
            }
            if(authority.getAuthority().equals(MessageConstants.TENANT_ROLE)) {
                this.tenantService.checkPermissionForCurrentTenantInBuilding(building.getId());

                Tenant currentTenant = account.getTenant();
                // ukoliko ne postoji odgovorna osoba ili
                // trenutno logovani stanar nije ujedno i odgovorna osoba za ovaj kvar
                if(damage.getResponsiblePerson() == null ||
                        (damage.getResponsiblePerson() != null &&
                                !damage.getResponsiblePerson().getId().equals(currentTenant.getId())))
                    throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);
            }
        }
        damage = selectFirmForFixingDamageSave(selectedBusinessForDamageRepairDTO, damage, business);
        return new ResponseEntity<>(new DamageDTO(damage), HttpStatus.OK);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/damages/{damageId}/send_email_to_firm",
            method = RequestMethod.PATCH,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Send request to firms and ask for offers.",
            notes = "You have to provide a valid DTO object with list of firm IDs, " +
                    "valid IDs for building and damage in the URL.",
            httpMethod = "PATCH",
            consumes = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully sent e-mails to all selected firms"),
            @ApiResponse(code = 400, message = "Inappropriate object sent in request body or building's/damage's ID in URL"),
            @ApiResponse(code = 401, message = "You are not authorized for this action."),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT') or hasAuthority('INSTITUTION')")
    public ResponseEntity sendDamageMailToBusinesses(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The ID of the existing damage resource.", required = true) @PathVariable("damageId") Long damageId,
            @ApiParam(value = "The object with list of firm ID.", required = true) @Valid @RequestBody DamageBusinessDTO damageBusinessDTO,
            @ApiParam(value = "The object that contains all errors from validation of DTO object") BindingResult errors) {

        Building building = this.buildingService.findOne(buildingId);
        Damage damage = this.damageService.findOne(damageId);

        // provera da li zadati kvar pripada zadatoj zgradi
        this.damageService.checkDamageInBuilding(damage, building.getId());

        Account account = this.accountService.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        Tenant currentTenant = null;
        Business currentInstitution = null;

        for(GrantedAuthority authority : SecurityContextHolder.getContext().getAuthentication().getAuthorities()) {
            if(authority.getAuthority().equals(MessageConstants.TENANT_ROLE)) {
                this.tenantService.checkPermissionForCurrentTenantInBuilding(building.getId());

                currentTenant = account.getTenant();
                // ukoliko ne postoji odgovorna osoba ili
                // trenutno logovani stanar nije ujedno i odgovorna osoba za ovaj kvar
                if(damage.getResponsiblePerson() == null ||
                        (damage.getResponsiblePerson() != null &&
                                !damage.getResponsiblePerson().getId().equals(currentTenant.getId())))
                    throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);
            }
            else if(authority.getAuthority().equals("INSTITUTION")) {
                currentInstitution = account.getBusinessOwner();
                // ukoliko ne postoji odgovorna institucija ili
                // trenutno logovana institucija nije ujedno i odgovorna institucija za ovaj kvar
                if(damage.getResponsibleInstitution() == null ||
                        (damage.getResponsibleInstitution() != null &&
                                !damage.getResponsibleInstitution().getId().equals(currentInstitution.getId())))
                    throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);
            }
        }

        for(Long business_id : damageBusinessDTO.getBusinessId()){
            Business business = this.businessService.findOne(business_id);
            damage.getBusinessesDamageSentToIds().add(business_id);
            try {
                if(currentTenant != null)
                    this.emailService.sendEmailDamageToFirms(business, damage, currentTenant, null);
                else if(currentInstitution != null)
                    this.emailService.sendEmailDamageToFirms(business, damage, null, currentInstitution);
            } catch (InterruptedException e) {
                throw new ConflictException(MessageConstants.CONFLICT_IN_MAIL_SENDING);
            }
        }

        this.damageService.save(damage);
        return new ResponseEntity<>(HttpStatus.OK);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/damages/{damageId}/declare_damage_fixed",
            method = RequestMethod.PATCH,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Declare damage is fixed.",
            notes = "You have to provide a valid IDs for building and damage in the URL.",
            httpMethod = "PATCH",
            consumes = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully declared damage as fixed"),
            @ApiResponse(code = 400, message = "Inappropriate object sent in request body or building's/damage's ID in the URL"),
            @ApiResponse(code = 401, message = "You are not authorized for this action."),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('INSTITUTION') or hasAuthority('COMPANY')")
    public ResponseEntity declareDamageFixed(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The ID of the damage resource.", required = true) @PathVariable("damageId") Long damageId,
            @ApiParam(value = "The damage object with offer id.", required = true) @RequestBody @Valid DamageFixedDTO damageFixedDTO,
            @ApiParam(value = "The object that contains all errors from validation of DTO object") BindingResult errors) {

        Building building = this.buildingService.findOne(buildingId);
        Damage damage = this.damageService.findOne(damageId);

        this.damageService.checkDamageInBuilding(damage, building.getId());

        Business business = this.accountService.findByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()).getBusinessOwner();
        if(damage.getSelectedBusiness() == null ||
                (damage.getSelectedBusiness() != null && !damage.getSelectedBusiness().getId().equals(business.getId())))
            throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);

        Offer offer = null;
        if(damageFixedDTO.getOfferId() != null)
            offer = this.offerService.findOne(damageFixedDTO.getOfferId());

        if(offer != null){
            if(!damage.getOffers().contains(offer))
                throw new BadRequestException("The selected offer doesn't belong to the selected damage!");
            else{
                try {
                    this.emailService.sendEmailDeclareDamageFixed(offer, damage);
                } catch (InterruptedException e) {
                    throw new ConflictException(MessageConstants.CONFLICT_IN_MAIL_SENDING);
                }
            }
        }

        damage.setFixed(true);
        this.damageService.save(damage);

        return new ResponseEntity<>(HttpStatus.OK);
    }



    private Damage selectFirmForFixingDamageSave(SelectedBusinessForDamageRepairDTO selectedBusinessForDamageRepairDTO,
                                                 Damage damage, Business business){
        if(selectedBusinessForDamageRepairDTO.getOfferId() != null) {
            // ukoliko firmu/instituciju koja ce da resava kvar biramo na osnovu ponuda (predracuna) koje su
            // razlicite firme slale kao odgovor
            // na raspisani konkurs
            Offer offer = this.offerService.findOne(selectedBusinessForDamageRepairDTO.getOfferId());

            if(!damage.getOffers().contains(offer))
                throw new BadRequestException("The selected offer doesn't belong to the selected damage!");

            if(selectedBusinessForDamageRepairDTO.getId() != offer.getBusiness().getId())
                throw new BadRequestException("ID of firm/institution in selected offer and ID of selected " +
                        "firm/institution for damage repairing are different!");

            offer.setAccepted(true);
            damage.setSelectedBusiness(offer.getBusiness());
            offer.getBusiness().getDamagesForRepair().add(damage);

            try {
                this.emailService.sendEmailAcceptingOffer(offer, damage);
            } catch (InterruptedException e) {
                throw new ConflictException(MessageConstants.CONFLICT_IN_MAIL_SENDING);
            }

            this.offerService.save(offer);
        }
        else {
            // ukoliko je odgovorna institucija/osoba samostalno izabrala neku od institucija/firmi za
            // resavanje posmatranog kvara ili na osnovu sprovedene ankete na nekom od sastanaka
            damage.setSelectedBusiness(business);
            business.getDamagesForRepair().add(damage);
        }
        return this.damageService.save(damage);
    }
}
