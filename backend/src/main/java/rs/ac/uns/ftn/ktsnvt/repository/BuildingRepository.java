package rs.ac.uns.ftn.ktsnvt.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import rs.ac.uns.ftn.ktsnvt.model.entity.Building;

import java.util.List;

/**
 * Created by Ivana Zeljkovic on 05/11/2017.
 */
public interface BuildingRepository extends JpaRepository<Building, Long> {

    Building findByAddressId(Long addressId);

    @Query(value = "SELECT * FROM Building b WHERE b.address_id in (Select a.id from address a where a.city_id = :cityId)",
            nativeQuery = true)
    List<Building> findByCity(@Param("cityId") Long cityId);

    @Query("SELECT b FROM Building b WHERE b.council.id IN (SELECT id FROM Council c WHERE c.responsibleCompany.id = :id)")
    Page<Building> findAllByFirm(@Param("id") Long id, Pageable pageable);

    Building findByBillboardId(Long billboardId);
}
