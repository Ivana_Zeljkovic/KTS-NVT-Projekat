package rs.ac.uns.ftn.ktsnvt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Apartment;
import rs.ac.uns.ftn.ktsnvt.repository.ApartmentRepository;

import java.util.List;

/**
 * Created by Ivana Zeljkovic on 05/11/2017.
 */
@Service
public class ApartmentServiceImpl implements ApartmentService {

    @Autowired
    ApartmentRepository apartmentRepository;


    @Override
    @Transactional(readOnly = true)
    public List<Apartment> findAll() {
        return this.apartmentRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Apartment> findAll(Pageable page) {
        return this.apartmentRepository.findAll(page);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Apartment> getAllFromBuilding(Long buildingId, Integer pageNumber, Integer pageSize,
                                     String sortDirection, String sortProperty, String sortPropertySecond) {
        return this.apartmentRepository.getAllFromBuilding(buildingId,
                new PageRequest(pageNumber, pageSize, (sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC),
                        sortProperty, sortPropertySecond));
    }

    @Override
    @Transactional(readOnly = true)
    public Apartment findOne(Long id) {
        Apartment apartment = this.apartmentRepository.findOne(id);
        if(apartment == null) throw new NotFoundException("Apartment not found!");
        return apartment;
    }

    @Override
    @Transactional(readOnly = true)
    public Apartment findOneIfExist(Long id) {
        return this.apartmentRepository.findOne(id);
    }

    @Override
    @Transactional(readOnly = false)
    public Apartment save(Apartment apartment) {
        return this.apartmentRepository.save(apartment);
    }

    @Override
    @Transactional(readOnly = false)
    public void remove(Long id) {
        this.apartmentRepository.delete(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Apartment> findFreeApartments(Integer pageNumber, Integer pageSize,
                                              String sortDirection, String sortProperty) {
        return this.apartmentRepository.findFreeApartments(new PageRequest(pageNumber, pageSize,
                (sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortProperty));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Apartment> findFreeApartmentsInBuilding(Long buildingId, Integer pageNumber, Integer pageSize,
                                                        String sortDirection, String sortProperty) {
        return this.apartmentRepository.findFreeApartmentsInBuilding(buildingId, new PageRequest(pageNumber, pageSize,
                (sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortProperty));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Apartment> findFreeApartmentsBySize(int min, int max, Long buildingId, Integer pageNumber, Integer pageSize,
                                                    String sortDirection, String sortProperty) {
        return this.apartmentRepository.findFreeApartmentsBySize(min, max, buildingId, new PageRequest(pageNumber, pageSize,
                (sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortProperty));
    }

    @Override
    @Transactional(readOnly = true)
    public int countApartmentsInBuilding(Long ownerId, Long buildingId) {
        return this.apartmentRepository.countApartmentsInBuilding(ownerId, buildingId);
    }

    @Override
    public void checkBuilding(Apartment apartment, Long buildingId) {
        if(!apartment.getBuilding().getId().equals(buildingId))
            throw new BadRequestException("Selected apartments doesn't belong to selected building!");
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Apartment> findAllPageable(Integer pageNumber, Integer pageSize,
                                          String sortDirection, String sortProperty) {
        return this.apartmentRepository.findAll(new PageRequest(pageNumber, pageSize,
                (sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortProperty));
    }
}
