import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: 'floor'
})
export class FloorInBuildingPipe implements PipeTransform {
  transform(value: any, args?: any[]) {
    if(!value)
      return null;

    let str = '';
    switch (value) {
      case 1: {
        str = 'First';
        break;
      }
      case 2: {
        str = 'Second';
        break;
      }
      case 3: {
        str = 'Third';
        break;
      }
      case 4: {
        str = 'Fourth';
        break;
      }
      case 5: {
        str = 'Fifth';
        break;
      }
      case 6: {
        str = 'Sixth';
        break;
      }
      case 7: {
        str = 'Seventh';
        break;
      }
      case 8: {
        str = 'Eight';
        break;
      }
      case 9: {
        str = 'Ninth';
        break;
      }
      case 10: {
        str = 'Tenth';
        break;
      }
    }
    return str;
  }
}
