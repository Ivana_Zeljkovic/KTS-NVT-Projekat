package rs.ac.uns.ftn.ktsnvt.web.dto;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by Ivana Zeljkovic on 13/11/2017.
 */
public class NotificationCreateUpdateDTO implements Serializable {

    @NotNull
    @NotEmpty
    private String content;

    @NotNull
    @NotEmpty
    private String title;

    public NotificationCreateUpdateDTO() { }

    public NotificationCreateUpdateDTO(String content, String title) {
        this.content = content;
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() { return title; }

    public void setTitle(String title) { this.title = title; }
}
