package rs.ac.uns.ftn.ktsnvt.service.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Address;
import rs.ac.uns.ftn.ktsnvt.model.entity.Building;
import rs.ac.uns.ftn.ktsnvt.service.BuildingService;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Nikola Garabandic on 01/12/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class BuildingServiceIntegrationTest {

    @Autowired
    private BuildingService buildingService;


    @Test
    @Transactional
    public void testFindOne(){
        //Zgrada sa id-em 100 ima sledece podatke: id = 100 number_of_floors = 5 has_parking = 1 council_id = 100 address_id = 100 billboard_id = 100
        Building building = this.buildingService.findOne(100L);

        assertNotNull(building);
        assertEquals("Janka Veselinovica", building.getAddress().getStreet());
        assertEquals(8, building.getAddress().getNumber());
        assertEquals("Novi Sad", building.getAddress().getCity().getName());
        assertEquals(21000, building.getAddress().getCity().getPostalNumber());
        assertEquals(5, building.getNumberOfFloors());
    }

    @Test(expected = NotFoundException.class)
    public void testFindOneNonexistent() {
        //zgrada sa id-em 140 ne postoji
        Building building = this.buildingService.findOne(140L);
    }

    @Test
    @Transactional
    public void testFindByAddressId(){

        //Zgrada sa address_id-em 100 ima sledece podatke: id = 100 number_of_floors = 5 has_parking = 1 council_id = 100 address_id = 100 billboard_id = 100
        Building building = this.buildingService.findByAddressId(100L);

        assertNotNull(building);
        assertEquals(Long.valueOf(100), building.getAddress().getId());
        assertEquals("Janka Veselinovica", building.getAddress().getStreet());
        assertEquals(8, building.getAddress().getNumber());
        assertEquals("Novi Sad", building.getAddress().getCity().getName());
        assertEquals(21000, building.getAddress().getCity().getPostalNumber());
        assertEquals(5, building.getNumberOfFloors());
    }

    @Test(expected = NotFoundException.class)
    public void testNotFoundByAddressId(){
        //zgrada sa address_id-em 140 ne postoji
        Building building = this.buildingService.findByAddressId(140L);

    }


}
