import { Login } from "./login";

export class AdministratorCreate {
  constructor(private loginAccount: Login,
              private firstName: string,
              private lastName: string){ }
}
