import {browser, by, element, ElementFinder} from "protractor";
// page
import {LoginPage} from "../../page-objects/login.po";
import {BusinessPage} from "./page-objects/business.po";


describe('Business more details view', () => {
  let loginPage: LoginPage;
  let businessPage: BusinessPage;
  let businessWithId100: any;

  beforeEach(() => {
    loginPage = new LoginPage();
    businessPage = new BusinessPage();
    browser.get("/");

    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');
    loginPage.setUsername('ivana');
    loginPage.setPassword('ivana');
    loginPage.getSignInButton().click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/home');
    browser.get('http://localhost:49152/#/businesses/100');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/businesses/100');

    businessWithId100 = {
      'name': 'First firm',
      'description': 'Description for first firm',
      'email': 'ivana.zeljkovic995@gmail.com',
      'phoneNumber': '123456789',
      'businessType' : 'Firm for plumbing',
      'city': 'Novi Sad',
      'postalNumber': '21000',
      'street': 'Janka Veselinovica',
      'number': '12'
    };

  });

  afterEach(() => {
    browser.executeScript('window.localStorage.clear();');
  });

  it('should load all details for business with id:100 ', () => {
    // all elements should be loaded
    expect(businessPage.getPanelHeading().isDisplayed()).toBe(true);
    expect(businessPage.getPanelBody().isDisplayed()).toBe(true);
    expect(businessPage.getMap().isDisplayed()).toBe(true);
    expect(businessPage.getBusinessTable().isDisplayed()).toBe(true);
    expect(businessPage.getName().isDisplayed()).toBe(true);
    expect(businessPage.getDescription().isDisplayed()).toBe(true);
    expect(businessPage.getBusinessType().isDisplayed()).toBe(true);
    expect(businessPage.getEmail().isDisplayed()).toBe(true);
    expect(businessPage.getPhoneNumber().isDisplayed()).toBe(true);
    expect(businessPage.getCity().isDisplayed()).toBe(true);
    expect(businessPage.getPostalNumber().isDisplayed()).toBe(true);
    expect(businessPage.getStreet().isDisplayed()).toBe(true);
    expect(businessPage.getNumber().isDisplayed()).toBe(true);

    // table should be filled with data
    expect(businessPage.getName() == businessWithId100.name);
    expect(businessPage.getDescription() == businessWithId100.description);
    expect(businessPage.getBusinessType() == businessWithId100.businessType);
    expect(businessPage.getEmail() == businessWithId100.email);
    expect(businessPage.getPhoneNumber() == businessWithId100.phoneNumber);
    expect(businessPage.getCity() == businessWithId100.city);
    expect(businessPage.getPostalNumber() == businessWithId100.postalNumber);
    expect(businessPage.getStreet() == businessWithId100.street);
    expect(businessPage.getNumber() == businessWithId100.number);
  });

});
