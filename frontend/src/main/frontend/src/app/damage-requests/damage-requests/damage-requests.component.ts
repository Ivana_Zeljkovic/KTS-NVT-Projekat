import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//services
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Damage } from "../../shared/models/damage";
import { DamageService } from "../../core/services/damage.service";
import { JwtService } from "../../core/services/jwt.service";
import { PageAndSort } from "../../shared/models/page-and-sort";
// component
import { OfferModalComponent } from "../offer-modal/offer-modal.component";
import {ForbiddenError} from "../../shared/errors/forbidden-error";
import {ToasterService} from "angular5-toaster/dist";


@Component({
  selector: 'app-damage-requests',
  templateUrl: './damage-requests.component.html',
  styleUrls: ['./damage-requests.component.css']
})
export class DamageRequestsComponent implements OnInit {
  pageSize: string;
  currentPage: number;
  maxSize: number;
  itemsPerPage: number;
  sortDirection: string;
  sortProperties: [string];
  totalItems: number;
  bsModalRef: BsModalRef;

  damages: Damage[] = [];

  constructor(private router: Router, private damageService : DamageService, private jwtService : JwtService,
              private bsModalService : BsModalService, private toasterService : ToasterService) {
    // Sett up for bootstrap paging
    this.currentPage = 1;
    this.maxSize = 3;
    this.itemsPerPage = 4;

    // Sett up for getting damages request
    this.pageSize = '4';
    this.sortDirection = 'ASC';
    this.sortProperties = ['id'];
  }

  ngOnInit() {
    this.loadDamages();
  }

  loadDamages(){
    let page = new PageAndSort((this.currentPage - 1).toString(), this.pageSize, this.sortDirection, this.sortProperties);

    this.damageService.getPage(page, 'businesses', this.jwtService.getIdFromToken(), 'damages_create_offer')
      .subscribe(data =>{
        this.totalItems = data.length == 0 ? 0 : data[0].numberOfElements;
        this.damages = data;
      }, error => {
          if(error instanceof ForbiddenError)
            this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
          else {
            this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
            throw error;
          }
      });
  }

  sendOffer(damage: Damage) {
      this.bsModalRef = this.bsModalService.show(OfferModalComponent);
      this.bsModalRef.content.damage = damage;

      this.bsModalRef.content.offerSent.subscribe(data =>{
          this.loadDamages();
      });
  }

  details(damage: Damage) {
    this.router.navigate(['damages', damage.id]);
  }
}
