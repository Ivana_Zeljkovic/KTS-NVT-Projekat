package rs.ac.uns.ftn.ktsnvt.web.controller;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Account;
import rs.ac.uns.ftn.ktsnvt.security.JWTUtils;
import rs.ac.uns.ftn.ktsnvt.service.AccountService;
import rs.ac.uns.ftn.ktsnvt.web.controller.util.MessageConstants;
import rs.ac.uns.ftn.ktsnvt.web.dto.BusinessProfileDTO;
import rs.ac.uns.ftn.ktsnvt.web.dto.AdminDTO;
import rs.ac.uns.ftn.ktsnvt.web.dto.LoginDTO;
import rs.ac.uns.ftn.ktsnvt.web.dto.TenantProfileDTO;
import rs.ac.uns.ftn.ktsnvt.web.dto.TokenDTO;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Ivana Zeljkovic on 06/11/2017.
 */
@RestController
@Api(value="user's essentials functionalities", description="Operations pertaining to all kind of users in application.")
public class AppUserController {

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private JWTUtils jwtUtils;

    @Autowired
    private AuthenticationManager authenticationManager;



    @RequestMapping(
            value = "/api/login",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Login user to application.",
            notes = "You have to provide a valid user's credentials.",
            httpMethod = "POST",
            consumes = "application/json",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully login and retrieved generated jwt token", response = TokenDTO.class),
            @ApiResponse(code = 400, message = "Inappropriate login object sent in request body"),
            @ApiResponse(code = 401, message = "You are not authorized to complete activity"),
            @ApiResponse(code = 404, message = "Invalid login")
    })
    public ResponseEntity login(
            @ApiParam(value = "The user's credentials.", required = true) @Valid @RequestBody LoginDTO loginDTO,
            @ApiParam(value = "The object that contains all errors from validation of DTO object") BindingResult errors) {

        try {
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
                    loginDTO.getUsername(), loginDTO.getPassword());
            authenticationManager.authenticate(token);
            Account account = this.accountService.findByUsername(loginDTO.getUsername());
            if((account.getTenant() != null && !account.getTenant().getConfirmed()) ||
                    (account.getBusinessOwner() != null && !account.getBusinessOwner().getConfirmed()))
                throw new BadCredentialsException("Account isn't still confirmed!");

            UserDetails details = userDetailsService.loadUserByUsername(loginDTO.getUsername());

            Long buildingID = -1L;
            Long apartmentID = -1L;
            boolean councilMember = false;
            boolean isOwner = false;
            if(account.getTenant() != null) {
                if(account.getTenant().getApartmentLiveIn() != null) {
                    apartmentID = account.getTenant().getApartmentLiveIn().getId();
                    buildingID = account.getTenant().getApartmentLiveIn().getBuilding().getId();
                }
                if(account.getTenant().getCouncil() != null)
                    councilMember = true;
                if(!account.getTenant().getPersonalApartments().isEmpty())
                    isOwner = true;
            }

            Long id = null;
            if(account.getTenant() != null)
                id = account.getTenant().getId();
            else if(account.getBusinessOwner() != null)
                id = account.getBusinessOwner().getId();
            else if(account.getAdministrator() != null)
                id = account.getAdministrator().getId();

            TokenDTO userToken = new TokenDTO(jwtUtils.generateToken(details, apartmentID, buildingID,
                    id, councilMember, isOwner));
            return new ResponseEntity(userToken, HttpStatus.OK);
        } catch (Exception ex) {
            throw new NotFoundException("Invalid login!");
        }
    }



    @RequestMapping(
            value = "/api/check_username",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Check does given username already exist.",
            notes = "You must provide valid username in the URL.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully checked availability of given username"),
            @ApiResponse(code = 400, message = "Username parameter is missing")
    })
    public ResponseEntity checkUsername(
            @ApiParam(value = "User's username") @RequestParam("username") String username) {

        if(username == null || username.equals(""))
            throw new BadRequestException("Username can't be empty!");

        return new ResponseEntity(this.accountService.isUsernameTaken(username), HttpStatus.OK);
    }



    @RequestMapping(
            value = "/api/current_user",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get current logged user.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved current user")
    })
    public ResponseEntity getCurrentUser() {
        Account account = this.accountService.findByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName());
        List<String> roles = new ArrayList<>();
        SecurityContextHolder.getContext().getAuthentication().getAuthorities().forEach(
                authority -> roles.add(authority.getAuthority()));

        ResponseEntity responseEntity = null;
        if(roles.contains(MessageConstants.TENANT_ROLE))
            responseEntity = new ResponseEntity(new TenantProfileDTO(account.getTenant()), HttpStatus.OK);
        else if(roles.contains(MessageConstants.ADMIN_ROLE))
            responseEntity = new ResponseEntity(new AdminDTO(account.getAdministrator()), HttpStatus.OK);
        else if(roles.contains(MessageConstants.FIRM_ROLE) || roles.contains(MessageConstants.INSTITUTION_ROLE))
            responseEntity = new ResponseEntity(new BusinessProfileDTO(account.getBusinessOwner()), HttpStatus.OK);
        return responseEntity;
    }
}
