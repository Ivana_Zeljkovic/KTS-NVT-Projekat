import {by, element, ElementArrayFinder, ElementFinder} from "protractor";

export class MeetingAddPage {
  // get methods
  getMeetingAddElement(): ElementFinder {
    return element(by.tagName('app-meeting-add'));
  }

  getTitle(): ElementFinder {
    return element(by.css('.modal-header > h4'));
  }

  getMeetingDate(): ElementFinder {
    return element(by.id('meetingDate'));
  }

  getMeetingTime(): ElementFinder {
    return element(by.id('timeForMeeting'));
  }

  getMeetingDuration(): ElementFinder {
    return element(by.id('duration'));
  }

  getChooseMeetingItemsLink(): ElementFinder {
    return element(by.css('a.choose-meeting-items-link'));
  }

  getMeetingItems(): ElementArrayFinder {
    return element.all(by.css('div.selected-meeting-item-list .panel'));
  }

  getMeetingItem(index: number): ElementFinder {
    return element(by.css(`div.selected-meeting-item-list .panel:nth-child(${index+1})`));
  }

  getMeetingItemTitle(index: number): ElementFinder {
    return element(by.css(`div.selected-meeting-item-list .panel:nth-child(${index+1}) .meeting-item-in-meeting-title`));
  }

  getMeetingItemMenu(index: number): ElementFinder {
    return element(by.css(`div.selected-meeting-item-list .panel:nth-child(${index+1}) .meeting-item-in-meeting-menu`));
  }

  getViewQuestionnaireOptionFromMeetingItemMenu(index: number): ElementFinder {
    return element(by.css(`div.selected-meeting-item-list .panel:nth-child(${index+1}) .meeting-item-in-meeting-menu a`));
  }

  getCreatorFromMeetingItem(index: number): ElementFinder {
    return element(by.css(`div.selected-meeting-item-list .panel:nth-child(${index+1}) div.meeting-item-in-meeting-metadata > span:nth-child(2)`));
  }

  getDateFromMeetingItem(index: number): ElementFinder {
    return element(by.css(`div.selected-meeting-item-list .panel:nth-child(${index+1}) .panel-heading > div:nth-child(2) > span:nth-child(4)`));
  }

  getMeetingItemContent(index: number): ElementFinder {
    return element(by.css(`div.selected-meeting-item-list .panel:nth-child(${index+1}) .panel-body`));
  }

  getSaveButton(): ElementFinder {
    return element(by.buttonText('Save'));
  }

  getResetButton(): ElementFinder {
    return element(by.buttonText('Reset'));
  }

  getCancelButton(): ElementFinder {
    return element(by.buttonText('Cancel'));
  }

  // set methods
  setMeetingDuration(text: string) {
    this.getMeetingDuration().sendKeys(text);
  }

  // get error methods
  getMeetingTimeRequiredError(): ElementFinder {
    return element(by.id('meetingTimeRequiredError'));
  }

  getMeetingTimeMinValueError(): ElementFinder {
    return element(by.id('meetingTimeMinValueError'));
  }

  getMeetingTimeMaxValueError(): ElementFinder {
    return element(by.id('meetingTimeMaxValueError'));
  }

  getMeetingDurationRequiredError(): ElementFinder {
    return element(by.id('meetingDurationRequiredError'));
  }

  getMeetingDurationMinValueError(): ElementFinder {
    return element(by.id('meetingDurationMinValueError'));
  }

  getMeetingDurationMaxValueError(): ElementFinder {
    return element(by.id('meetingDurationMaxValueError'));
  }
}
