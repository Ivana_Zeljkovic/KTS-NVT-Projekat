package rs.ac.uns.ftn.ktsnvt.repository.unit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.model.entity.Account;
import rs.ac.uns.ftn.ktsnvt.model.entity.AccountAuthority;
import rs.ac.uns.ftn.ktsnvt.model.entity.Authority;
import rs.ac.uns.ftn.ktsnvt.repository.AccountAuthorityRepository;
import rs.ac.uns.ftn.ktsnvt.repository.AccountRepository;

import static org.junit.Assert.assertEquals;

/**
 * Created by Ivana Zeljkovic on 27/11/2017.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class AccountAuthorityRepositoryUnitTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private AccountAuthorityRepository accountAuthorityRepository;

    @Autowired
    private AccountRepository accountRepository;

    private Long account_1_id;
    private Long account_2_id;


    @Before
    public void setUp() {
        // AUTHORITIES
        Authority authority_tenant = new Authority("TENANT");
        Authority authority_president = new Authority("COUNCIL_PRESIDENT");
        entityManager.persist(authority_tenant);
        entityManager.persist(authority_president);

        // ACCOUNTS, TENANTS
        Account account_1 = new Account("kaca", "kaca");
        Account account_2 = new Account("nikola", "nikola");
        this.account_1_id = (Long)entityManager.persistAndGetId(account_1);
        this.account_2_id = (Long)entityManager.persistAndGetId(account_2);

        // ACCOUNT AUTHORITIES
        AccountAuthority account_1_tenant = new AccountAuthority(account_1, authority_tenant);
        AccountAuthority account_1_president = new AccountAuthority(account_1, authority_president);
        AccountAuthority account_2_tenant = new AccountAuthority(account_2, authority_tenant);
        entityManager.persist(account_1_tenant);
        entityManager.persist(account_1_president);
        entityManager.persist(account_2_tenant);

        // azuriranje perzistentnih objekata klase Account, tako da su ovi objekti povezani sa odgovarajucim
        // objektima klase AccountAuthority
        account_1.getAccountAuthorities().add(account_1_tenant);
        account_1.getAccountAuthorities().add(account_1_president);
        entityManager.merge(account_1);

        account_2.getAccountAuthorities().add(account_2_tenant);
        entityManager.merge(account_2);
    }

    @Test
    public void testRemovePresidentRole() {
        /*
         * Test proverava ispravnost rada metode removePresidentRole repozitorijuma AccountAuthorityRepository
         * (uklanjanje uloge predsednika skupstine stanara nad konkretnim stanarom)
         * u slucaju kada izabrani stanar poseduje zadatu rolu (PRESIDENT)
         * */

        Account account_1 = this.accountRepository.findOne(this.account_1_id);
        int role_size_before_remove = account_1.getAccountAuthorities().size();

        // stanar ciji account ima username = kaca je predsednik skupstine stanara
        int deleted_rows_num = this.accountAuthorityRepository.removePresidentRole(this.account_1_id);

        account_1 = this.accountRepository.findOne(this.account_1_id);
        assertEquals(1, deleted_rows_num);
        assertEquals(1, (role_size_before_remove-1));
        assertEquals("TENANT", account_1.getAccountAuthorities().get(0).getAuthority().getName());
    }

    @Test
    public void testRemoveNonexistentPresidentRole() {
        /*
         * Test proverava ispravnost rada metode removePresidentRole repozitorijuma AccountAuthorityRepository
         * (uklanjanje uloge predsednika skupstine stanara nad konkretnim stanarom)
         * u slucaju kada izabrani stanar ne poseduje zadatu rolu (PRESIDENT)
         * */

        Account account_2 = this.accountRepository.findOne(this.account_2_id);
        int role_size_before_remove = account_2.getAccountAuthorities().size();

        // stanar ciji account ima username = nikola nije predsednik skupstine stanara
        int deleted_rows_num = this.accountAuthorityRepository.removePresidentRole(this.account_2_id);

        account_2 = this.accountRepository.findOne(this.account_2_id);
        assertEquals(0, deleted_rows_num);
        assertEquals(1, role_size_before_remove);
        assertEquals("TENANT", account_2.getAccountAuthorities().get(0).getAuthority().getName());
    }
}
