package rs.ac.uns.ftn.ktsnvt.repository.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.model.entity.Address;
import rs.ac.uns.ftn.ktsnvt.repository.AddressRepository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Katarina Cukurov on 28/11/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class AddressRepositoryIntegrationTest {

    @Autowired
    private AddressRepository addressRepository;

    
    @Test
    @Transactional
    public void testFindByCityStreetNumber() {
        /*
         * Test proverava ispravnost rada metode findByCityStreetNumber repozitorijuma AddressRepository
         * (pronalazenje adrese na osnovu zadatog naziva grada, naziva ulice i broja)
         * */

        /*
        adresa sa ID-em = 101 ima sledece podatke:
            ulica = Janka Veselinovica,
            broj = 8,
            id grada kom adresa pripada = 10
        */
        Address address = this.addressRepository.findByCityStreetNumber(new Long (100), "Janka Veselinovica", 10);

        assertNotNull(address);
        assertEquals("Janka Veselinovica", address.getStreet());
        assertEquals(Long.valueOf(100), address.getCity().getId());
        assertEquals("Novi Sad", address.getCity().getName());
        assertEquals(10, address.getNumber());
    }
}
