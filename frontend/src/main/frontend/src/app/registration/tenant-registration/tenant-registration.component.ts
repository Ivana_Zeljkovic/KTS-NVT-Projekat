import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { BsDatepickerConfig, BsLocaleService, enGb } from "ngx-bootstrap";
import { defineLocale } from 'ngx-bootstrap/bs-moment';
import { ToasterConfig, ToasterService } from "angular5-toaster/dist";
// validator
import { UsernameUniqueValidator } from "../../shared/validators/username-unique.validator";
import { PasswordValidator } from "../../shared/validators/password.validator";
import { SpaceValidator } from "../../shared/validators/space.validator";
// model
import { TenantCreate } from "../../shared/models/tenant-create";
import { Login } from "../../shared/models/login";
// service
import { TenantService } from "../../core/services/tenant.service";
// error
import { AppError } from "../../shared/errors/app-error";
import { BadRequestError } from "../../shared/errors/bad-request-error";
import { NotFoundError } from "../../shared/errors/not-found-error";


@Component({
  selector: 'app-tenant-registration',
  templateUrl: './tenant-registration.component.html',
  styleUrls: ['./tenant-registration.component.css']
})
export class TenantRegistrationComponent implements OnInit {
  form: FormGroup;
  returnURL: string;
  bsConfig: Partial<BsDatepickerConfig>;
  maxDate: Date;
  toasterConfig: ToasterConfig;

  constructor(private fb: FormBuilder, private router: Router, private route: ActivatedRoute,
              private usernameUniqueValidator: UsernameUniqueValidator, private passwordValidator: PasswordValidator,
              private tenantService: TenantService, private localeService: BsLocaleService, private toasterService: ToasterService) {
    this.form = this.fb.group({
      firstName: ['', [
        Validators.required,
        Validators.minLength(2)
      ]],
      lastName: ['', [
        Validators.required,
        Validators.minLength(2)
      ]],
      username: ['', [
        Validators.required,
        SpaceValidator.cannotContainSpace
      ],
        this.usernameUniqueValidator.isUsernameTaken.bind(this.usernameUniqueValidator)
      ],
      passwords: fb.group({
        password: ['', [
          Validators.required,
          Validators.minLength(5)
        ]],
        confirmPassword: ['',
          Validators.required
        ]
      }, {validator: this.passwordValidator.matchPasswords.bind(this.passwordValidator)}),
      email: ['', [
        Validators.required, Validators.email
      ]],
      phoneNumber: ['', [
        Validators.required,
        SpaceValidator.cannotContainSpace,
        Validators.pattern('[0-9]{6,10}')
      ]],
      birthDate: [new Date(), [
        Validators.required
      ]]
    });

    this.bsConfig = Object.assign({}, {containerClass: 'theme-dark-blue'});
    this.maxDate = new Date();
    defineLocale('enGb', enGb);
    this.localeService.use('enGb');

    this.toasterConfig = new ToasterConfig({
      timeout: {success: 2000, error: 4000}
    })
  }

  ngOnInit() {
    this.returnURL = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  get firstName() {
    return this.form.get('firstName');
  }

  get lastName() {
    return this.form.get('lastName');
  }

  get username() {
    return this.form.get('username');
  }

  get passwords() {
    return this.form.get('passwords');
  }

  get password() {
    return this.form.get('passwords').get('password');
  }

  get confirmPassword() {
    return this.form.get('passwords').get('confirmPassword');
  }

  get email() {
    return this.form.get('email');
  }

  get birthDate() {
    return this.form.get('birthDate');
  }

  get phoneNumber() {
    return this.form.get('phoneNumber');
  }

  register() {
    let login = new Login(this.username.value, this.password.value);
    let tenant = new TenantCreate(login, this.firstName.value, this.lastName.value,
      new Date(this.birthDate.value), this.email.value, this.phoneNumber.value);
    this.tenantService.save(tenant)
      .subscribe(() => {
        this.router.navigateByUrl(this.returnURL);
      }, (error: AppError) => {
        if (error instanceof BadRequestError)
          this.toasterService.pop('error', 'Error', 'Given data have bad format!');
        else if (error instanceof NotFoundError)
          this.toasterService.pop('error', 'Error', 'Registration can\'t be done, because tenant role is missing!');
        else {
          this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
          throw error;
        }
      });
  }
}
