package rs.ac.uns.ftn.ktsnvt.web.controller;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.*;
import rs.ac.uns.ftn.ktsnvt.service.*;
import rs.ac.uns.ftn.ktsnvt.web.dto.DamageResponsibilityDTO;
import rs.ac.uns.ftn.ktsnvt.web.dto.ResponsibleForDamageTypeDTO;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Ivana Zeljkovic on 25/11/2017.
 */
@RestController
@RequestMapping(value = "/api")
@Api(value="damage types", description="Operations pertaining to damage types in application.")
public class DamageTypeController {

    @Autowired
    private BuildingService buildingService;

    @Autowired
    private DamageTypeService damageTypeService;

    @Autowired
    private BusinessService businessService;

    @Autowired
    private DamageResponsibilityService damageResponsibilityService;

    @Autowired
    private TenantService tenantService;



    @RequestMapping(
            value = "/buildings/{buildingId}/damage_types/{damageTypeId}/responsible_institution",
            method = RequestMethod.PATCH,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Update responsible institution for selected damage type in selected building.",
            notes = "You have to provide valid IDs for building and damage type in the URL.",
            httpMethod = "PATCH",
            produces = "application/json",
            consumes = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated/created damage type responsibility",
                    response = DamageResponsibilityDTO.class),
            @ApiResponse(code = 400, message = "Inappropriate object sent in request body"),
            @ApiResponse(code = 401, message = "You are not authorized to update the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('COUNCIL_PRESIDENT')")
    public ResponseEntity setResponsibleInstitutionForDamageType(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The ID of the existing damage type resource.", required = true) @PathVariable("damageTypeId") Long damageTypeId,
            @ApiParam(value = "The object with institution ID.", required = true) @Valid @RequestBody ResponsibleForDamageTypeDTO responsibleForDamageTypeDTO,
            @ApiParam(value = "The object that contains all errors from validation of DTO object") BindingResult errors) {

        Building building = this.buildingService.findOne(buildingId);
        DamageType damageType = this.damageTypeService.findOne(damageTypeId);

        this.tenantService.checkPermissionForCurrentPresidentInBuilding(building);

        Business institution = this.businessService.findOne(responsibleForDamageTypeDTO.getId());

        if(institution.isIsCompany())
            throw new BadRequestException("You have to mark institution (not firm) as responsible!");

        DamageResponsibility damageResponsibility =
                this.damageResponsibilityService.findOneByDamageTypeAndBuilding(damageTypeId, buildingId);

        if(damageResponsibility != null) {
            if(damageResponsibility.getResponsibleInstitution() != null){
                // vec postoji institucija na nivou zgrade sa ID-em buildingId koja je odgovorna za
                // vrstu kvara sa ID-em damageTypeId pa je neophodno osloboditi trenutno zaduzenu
                // instituciju te odgovornosti i tek onda tu duznost dodeliti instituciji ciji je ID prosledjen u DTO-u
                damageType.getDamageResponsibilities().remove(damageResponsibility);
                this.damageResponsibilityService.remove(damageResponsibility.getId());
            }
            else {
                // vec postoji odgovorna osoba na nivou zgrade sa ID-em buildingId koja je zaduzena za
                // vrstu kvara sa ID-em damageTypeId pa je neophodno azurirati tu torku u bazi time sto ce se dodati
                // i polje odgovorne institucije pored odgovorne osobe
                damageResponsibility.setResponsibleInstitution(institution);
                damageResponsibility = this.damageResponsibilityService.save(damageResponsibility);
                return new ResponseEntity<>(new DamageResponsibilityDTO(damageResponsibility), HttpStatus.OK);
            }
        }

        // ukoliko postoji odgovorna osoba za tip kvara za koji vrsimo azuriranje odgovorne institucije,
        // ta osoba ce se naci u novokreiranoj odgovornosti za dati tip kvara
        DamageResponsibility newDamageResponsibility = new DamageResponsibility(
                (damageResponsibility == null || damageResponsibility.getResponsibleTenant() == null) ?
                        null : damageResponsibility.getResponsibleTenant(), institution, damageType, building);
        institution.getResponsibleForDamageTypes().add(newDamageResponsibility);
        damageType.getDamageResponsibilities().add(newDamageResponsibility);
        this.damageResponsibilityService.save(newDamageResponsibility);

        return new ResponseEntity<>(new DamageResponsibilityDTO(newDamageResponsibility), HttpStatus.OK);
    }



    @RequestMapping(
            value = "/institutions/{institutionId}/damage_types/{damageTypeId}/buildings/{buildingId}/delegate_responsibility",
            method = RequestMethod.PATCH,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Delegate responsibility to another institution for selected damage type in selected building.",
            notes = "You have to provide valid IDs for institution, damage type and building in the URL.",
            httpMethod = "PATCH",
            produces = "application/json",
            consumes = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated damage type responsibility",
                    response = DamageResponsibilityDTO.class),
            @ApiResponse(code = 400, message = "Inappropriate object sent in request body or institution's/damage " +
                    "type's/building's ID sent in URL"),
            @ApiResponse(code = 401, message = "You are not authorized to update the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('INSTITUTION')")
    public ResponseEntity delegateResponsibilityToInstitutionForDamageType(
            @ApiParam(value = "The ID of the existing institution resource.", required = true) @PathVariable("institutionId") Long institutionId,
            @ApiParam(value = "The ID of the existing damage type resource.", required = true) @PathVariable("damageTypeId") Long damageTypeId,
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The object with institution ID.", required = true) @Valid @RequestBody ResponsibleForDamageTypeDTO responsibleInstitutionForDamageTypeDTO,
            @ApiParam(value = "The object that contains all errors from validation of DTO object") BindingResult errors) {

        Business currentInstitution = this.businessService.findOne(institutionId);
        if(currentInstitution.isIsCompany()) throw new NotFoundException("Institution not found!");

        this.businessService.checkPermissionForCurrentBusiness(institutionId);

        DamageType damageType = this.damageTypeService.findOne(damageTypeId);
        Building building = this.buildingService.findOne(buildingId);
        DamageResponsibility damageResponsibility =
                this.damageResponsibilityService.findOneByDamageTypeAndInstitutionAndBuilding(damageType.getId(),
                        institutionId, building.getId());
        Business institution = this.businessService.findOne(responsibleInstitutionForDamageTypeDTO.getId());

        if (institution.isIsCompany())
            throw new BadRequestException("You can't delegate responsibility to firm, just on another institution!");

        // uklanjanje odgovornosti za zadati tip kvara u zadatoj zgradi kod trenutno logovane institucije
        currentInstitution.getResponsibleForDamageTypes().remove(damageResponsibility);
        // dodavanje te odgovornosti novoizbranoj instituciji
        damageResponsibility.setResponsibleInstitution(institution);
        institution.getResponsibleForDamageTypes().add(damageResponsibility);
        damageResponsibility = this.damageResponsibilityService.save(damageResponsibility);

        return new ResponseEntity<>(new DamageResponsibilityDTO(damageResponsibility), HttpStatus.OK);
    }



    @RequestMapping(
            value = "/institutions/{institutionId}/responsible_for_damage_types",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get list of all damage type responsibility for institution, in page form.",
            notes = "You have to provide valid institution ID in the URL.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved all damage type responsibilities " +
                    "for selected institution", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to get the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('INSTITUTION')")
    public ResponseEntity getAllDamageTypeResponsibilityForInstitution(
            @ApiParam(value = "The ID of the existing institution resource.", required = true) @PathVariable("institutionId") Long institutionId,
            @ApiParam(value = "The page number for getting damage type responsibilities.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting damage type responsibilities.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting damage type responsibilities.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "The sort property for getting damage type responsibilities.", required = true) @RequestParam String sortProperty) {

        Business currentInstitution = this.businessService.findOne(institutionId);
        if(currentInstitution.isIsCompany()) throw new NotFoundException("Institution not found!");

        this.businessService.checkPermissionForCurrentBusiness(institutionId);

        Page<DamageResponsibility> damageResponsibilities = this.damageResponsibilityService.findAllByInstitutionId(
                institutionId, Integer.valueOf(pageNumber), Integer.valueOf(pageSize), sortDirection, sortProperty);

        List<DamageResponsibilityDTO> damageTypeResponsibilityDTOs = new ArrayList<>();
        damageResponsibilities.getContent().forEach(damageResponsibility ->
            damageTypeResponsibilityDTOs.add(new DamageResponsibilityDTO(
                    damageResponsibility, damageResponsibilities.getTotalElements())) );

        return new ResponseEntity<>(damageTypeResponsibilityDTOs, HttpStatus.OK);
    }




    @RequestMapping(
            value = "/buildings/{buildingId}/damage_types/{damageTypeId}/responsible_person",
            method = RequestMethod.PATCH,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Update responsible person for selected damage type in selected building.",
            notes = "You have to provide valid IDs for building and damage type in the URL.",
            httpMethod = "PATCH",
            produces = "application/json",
            consumes = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated/created damage type responsibility",
                    response = DamageResponsibilityDTO.class),
            @ApiResponse(code = 400, message = "Inappropriate object sent in request body"),
            @ApiResponse(code = 401, message = "You are not authorized to update the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "Request caused some error on server side")
    })
    @PreAuthorize("hasAuthority('COUNCIL_PRESIDENT')")
    public ResponseEntity setResponsiblePersonForDamageType(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The ID of the existing damage type resource.", required = true) @PathVariable("damageTypeId") Long damageTypeId,
            @ApiParam(value = "The object with tenant ID.", required = true) @Valid @RequestBody ResponsibleForDamageTypeDTO responsiblePersonForDamageTypeDTO,
            @ApiParam(value = "The object that contains all errors from validation of DTO object") BindingResult errors) {

        Building building = this.buildingService.findOne(buildingId);
        DamageType damageType = this.damageTypeService.findOne(damageTypeId);

        this.tenantService.checkPermissionForCurrentPresidentInBuilding(building);

        Tenant tenant = this.tenantService.findOne(responsiblePersonForDamageTypeDTO.getId());

        if(!tenant.getApartmentLiveIn().getBuilding().getId().equals(building.getId()))
            throw new BadRequestException("Selected tenant doesn't live in selected building!");

        DamageResponsibility damageResponsibility =
                this.damageResponsibilityService.findOneByDamageTypeAndBuilding(damageTypeId, buildingId);

        if(damageResponsibility != null) {
            if(damageResponsibility.getResponsibleTenant() != null){
                // vec postoji osoba na nivou zgrade sa ID-em buildingId koja je odgovorna za vrstu kvara sa ID-em
                // damageTypeId pa je neophodno osloboditi trenutno zaduzenu osobu te odgovornosti i tek onda tu
                // duznost dodeliti osobi ciji je ID prosledjen u DTO-u
                damageType.getDamageResponsibilities().remove(damageResponsibility);
                this.damageResponsibilityService.remove(damageResponsibility.getId());
            }
            else {
                // vec postoji odgovorna institucija na nivou zgrade sa ID-em buildingID koja je zaduzena za vrstu kvara
                // sa ID-em damageTypeId pa je neophodno azurirati tu torku u bazi time sto ce se dodati i polje
                // odgovornog lica pored odgovorne institucije
                damageResponsibility.setResponsibleTenant(tenant);
                damageResponsibility = this.damageResponsibilityService.save(damageResponsibility);
                return new ResponseEntity<>(new DamageResponsibilityDTO(damageResponsibility), HttpStatus.OK);
            }
        }

        // ukoliko postoji odgovorna institucija za tip kvara za koji vrsimo azuriranje odgovorne osobe,
        // ta institucija ce se naci u novokreiranoj
        // odgovornosti za dati tip kvara
        DamageResponsibility newDamageResponsibility = new DamageResponsibility(tenant,
                (damageResponsibility == null || damageResponsibility.getResponsibleInstitution() == null) ?
                        null : damageResponsibility.getResponsibleInstitution(), damageType, building);
        tenant.getResponsibleForDamageTypes().add(newDamageResponsibility);
        damageType.getDamageResponsibilities().add(newDamageResponsibility);
        this.damageResponsibilityService.save(newDamageResponsibility);

        return new ResponseEntity<>(new DamageResponsibilityDTO(newDamageResponsibility), HttpStatus.OK);
    }



    @RequestMapping(
            value = "/tenants/{tenantId}/damage_types/{damageTypeId}/delegate_responsibility",
            method = RequestMethod.PATCH,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Delegate responsibility to another tenant for selected damage type in selected building.",
            notes = "You have to provide valid IDs for tenant and damage type in the URL.",
            httpMethod = "PATCH",
            produces = "application/json",
            consumes = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated damage type responsibility",
                    response = DamageResponsibilityDTO.class),
            @ApiResponse(code = 400, message = "Inappropriate object sent in request body or tenant's ID sent in URL"),
            @ApiResponse(code = 401, message = "You are not authorized to update the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity delegateResponsibilityToPersonForDamageType(
            @ApiParam(value = "The ID of the existing tenant resource.", required = true) @PathVariable("tenantId") Long tenantId,
            @ApiParam(value = "The ID of the existing damage type resource.", required = true) @PathVariable("damageTypeId") Long damageTypeId,
            @ApiParam(value = "The object with tenant ID.", required = true) @Valid @RequestBody ResponsibleForDamageTypeDTO responsiblePersonForDamageTypeDTO,
            @ApiParam(value = "The object that contains all errors from validation of DTO object") BindingResult errors) {

        Tenant currentTenant = this.tenantService.findOne(tenantId);
        this.tenantService.checkPermissionForCurrentTenant(tenantId);

        DamageType damageType = this.damageTypeService.findOne(damageTypeId);
        DamageResponsibility damageResponsibility =
                this.damageResponsibilityService.findOneByDamageTypeAndTenantAndBuilding(damageType.getId(),
                        tenantId, currentTenant.getApartmentLiveIn().getBuilding().getId());
        Tenant tenant = this.tenantService.findOne(responsiblePersonForDamageTypeDTO.getId());

        // ukoliko stanar kom trenutno logovani stanar zeli da delegira odgovornost ne stanuje u istoj zgradi
        // (kao i trenutno logovani) delegiranj odgovornosti nije moguce
        if (tenant.getApartmentLiveIn().getBuilding().getId() != currentTenant.getApartmentLiveIn().getBuilding().getId())
            throw new BadRequestException("Tenant who you want to delegate your responsibility " +
                    "doesn't live in your building!");

        // uklanjanje odgovornosti za zadati tip kvara kod trenutno logovanog korisnika
        currentTenant.getResponsibleForDamageTypes().remove(damageResponsibility);
        // dodavanje te odgovornosti novoizabranom stanaru
        damageResponsibility.setResponsibleTenant(tenant);
        tenant.getResponsibleForDamageTypes().add(damageResponsibility);
        damageResponsibility = this.damageResponsibilityService.save(damageResponsibility);

        return new ResponseEntity<>(new DamageResponsibilityDTO(damageResponsibility), HttpStatus.OK);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/tenants/{tenantId}/responsible_for_damage_types",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get list of all damage type responsibility for tenant, in page form.",
            notes = "You have to provide valid IDs for building and tenant in the URL.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved all damage type responsibilities for selected tenant",
                    response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to get the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity getAllDamageTypeResponsibilityForTenant(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The ID of the existing tenant resource.", required = true) @PathVariable("tenantId") Long tenantId) {

        Building building = this.buildingService.findOne(buildingId);
        Tenant tenant = this.tenantService.findOne(tenantId);

        this.tenantService.checkPermissionForCurrentTenant(tenantId);
        this.tenantService.checkPermissionForCurrentTenantInBuilding(building.getId());

        List<DamageResponsibilityDTO> damageTypeResponsibilityDTOs = new ArrayList<>();

        tenant.getResponsibleForDamageTypes().forEach(damageResponsibility ->
                damageTypeResponsibilityDTOs.add(new DamageResponsibilityDTO(damageResponsibility)) );

        return new ResponseEntity<>(damageTypeResponsibilityDTOs, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/damage_types_responsibilities",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get list of all damage type responsibility in building, in page form.",
            notes = "You have to provide valid building ID in the URL.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved all damage type responsibilities in selected building",
                    response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to get the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('COUNCIL_PRESIDENT')")
    public ResponseEntity getAllDamageTypeResponsibilityinBuilding(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId) {

        Building building = this.buildingService.findOne(buildingId);

        List<DamageResponsibilityDTO> damageTypeResponsibilityDTOs = new ArrayList<>();

        List<DamageResponsibility> damageResponsibilities = this.damageResponsibilityService.findAllByBuildingId(building);

        damageResponsibilities.forEach(damageResponsibility ->
            damageTypeResponsibilityDTOs.add(new DamageResponsibilityDTO(damageResponsibility)) );

        return new ResponseEntity<>(damageTypeResponsibilityDTOs, HttpStatus.OK);
    }
}
