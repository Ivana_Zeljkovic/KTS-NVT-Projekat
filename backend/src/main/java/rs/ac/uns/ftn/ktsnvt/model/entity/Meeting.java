package rs.ac.uns.ftn.ktsnvt.model.entity;

import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "meeting")
@Where(clause="deleted=0")
public class Meeting {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Column(nullable = false)
    private Date date;

    @Column(nullable = false, columnDefinition = "INTEGER DEFAULT 0")
    @Version
    private int version;

    @Column(nullable = false)
    private int duration;

    @Column(nullable = false, columnDefinition = "BOOL DEFAULT FALSE")
    private boolean deleted;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH, mappedBy = "meeting")
    private List<MeetingItem> meetingItems;

    @ManyToOne
    private Council council;

    @OneToOne
    private Record record;

    public Meeting() {
        this.meetingItems = new ArrayList<>();
    }

    public Meeting(Date date) {
        this.date = date;
        this.meetingItems = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Record getRecord() {
        return record;
    }

    public void setRecord(Record record) {
        this.record = record;
    }

    public List<MeetingItem> getMeetingItems() {
        return meetingItems;
    }

    public void setMeetingItems(List<MeetingItem> meetingItems) {
        this.meetingItems = meetingItems;
    }

    public Council getCouncil() { return council; }

    public void setCouncil(Council council) { this.council = council; }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
}
