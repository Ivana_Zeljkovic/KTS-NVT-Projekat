import { Component, OnInit } from '@angular/core';
import { ToasterConfig, ToasterService } from "angular5-toaster/dist";
import { BsModalService } from "ngx-bootstrap";
import { Router} from "@angular/router";
import * as _ from "lodash";
// service
import { MeetingItemService } from "../../core/services/meeting-item.service";
import { JwtService } from "../../core/services/jwt.service";
import { QuestionnaireService } from "../../core/services/questionnaire.service";
// model
import { MeetingItem } from "../../shared/models/meeting-item";
import { PageAndSort } from "../../shared/models/page-and-sort";
import { Questionnaire } from "../../shared/models/questionnaire";
import { QuestionnaireUpdate } from "../../shared/models/questionnaire-update";
import { VoteQuestionCreate } from "../../shared/models/vote-question-create";
import { Question } from "../../shared/models/question";
import { VoteCreate } from "../../shared/models/vote-create";
// error
import { AppError } from "../../shared/errors/app-error";
import { ForbiddenError } from "../../shared/errors/forbidden-error";
import { NotFoundError } from "../../shared/errors/not-found-error";
import { BadRequestError } from "../../shared/errors/bad-request-error";
// component
import { QuestionnaireComponent } from "../../shared/components/questionnaire/questionnaire.component";
import { VoteComponent } from "../vote/vote.component";
import { ResultsComponent } from "../results/results.component";


@Component({
  selector: 'app-questionnaire-list',
  templateUrl: './questionnaire-list.component.html',
  styleUrls: ['./questionnaire-list.component.css']
})
export class QuestionnaireListComponent implements OnInit {
  meetingItems: Array<MeetingItem>;
  tenantID: number;
  toasterConfig: ToasterConfig;
  isResultsMode: boolean;
  title: string;

  // Elements for bootstrap paging
  currentPage: number;
  totalItems: number;
  maxSize: number;
  itemsPerPage: number;

  // Elements for getting data from server
  pageSize: string;
  sortDirection: string;
  sortProperties: string[];


  constructor(private meetingItemService: MeetingItemService, private toasterService: ToasterService,
              private jwtService: JwtService, private modalService: BsModalService,
              private questionnaireService: QuestionnaireService, private router: Router) {
    // toaster config
    this.toasterConfig = new ToasterConfig({
      timeout: {success: 2000, error: 4000, warning: 4000}
    });

    // Sett up for bootstrap paging
    this.currentPage = 1;
    this.maxSize = 3;
    this.itemsPerPage = 4;

    // Sett up for getting damages request
    this.pageSize = '4';
    this.sortDirection = 'ASC';
    this.sortProperties = ['questionnaire.dateExpire'];
  }

  ngOnInit() {
    this.tenantID = this.jwtService.getIdFromToken();
    this.isResultsMode = this.router.url.indexOf("results") !== -1;
    this.title = this.isResultsMode ? '\n' + 'Expired questionnaires with voting results' : 'Active questionnaires you can vote on';
    this.loadMeetingItems();
  }

  questionnaireReview(questionnaire: Questionnaire) {
    const modalRef = this.modalService.show(QuestionnaireComponent);

    modalRef.content.isReviewMode = true;
    modalRef.content.model = new QuestionnaireUpdate(questionnaire.id, questionnaire.questions);
    modalRef.content.initialValues = _.cloneDeep(modalRef.content.model);
  }

  vote(questionnaire: Questionnaire) {
    let questionVoteList = new Array<VoteQuestionCreate>();
    questionnaire.questions.forEach((question: Question) =>
      questionVoteList.push(new VoteQuestionCreate(question.id, question.answers[0]))
    );

    const modalRef = this.modalService.show(VoteComponent);
    modalRef.content.questionnaire = questionnaire;
    modalRef.content.voteList = new VoteCreate(questionVoteList);
    modalRef.content.initialValues = _.cloneDeep(modalRef.content.voteList);

    modalRef.content.submitted.subscribe(votedSuccessfully => {
      if(votedSuccessfully)
        this.toasterService.pop('success', 'God job!', 'You are successfully post your vote!');
      this.loadMeetingItems();
    });
  }

  reviewVotes(questionnaire: Questionnaire) {
    this.questionnaireService.getResults(questionnaire.buildingId, questionnaire.id)
      .subscribe(results => {
        const modalRef = this.modalService.show(ResultsComponent);
        modalRef.content.questionnaireResults = results;
      }, (error: AppError) => {
        if(error instanceof BadRequestError)
          this.toasterService.pop('error', 'Error', 'The selected questionnaire has not been finished or processed yet');
        else if(error instanceof ForbiddenError)
          this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
        else if(error instanceof NotFoundError)
          this.toasterService.pop('error', 'Error', 'Questionnaire not found!');
        else {
          this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
          throw error;
        }
      });
  }

  private loadMeetingItems() {
    let page = new PageAndSort((this.currentPage - 1).toString(), this.pageSize, this.sortDirection, this.sortProperties);

    if(!this.isResultsMode) {
      // load all questionnaires that are not expired
      this.meetingItemService.getPage(page, 'tenants', this.tenantID, 'meeting_items/active_questionnaires')
        .subscribe(meetingItems => {
          if (meetingItems.length !== 0)
            this.totalItems = meetingItems[0].numberOfElements;
          else this.totalItems = 0;
          this.meetingItems = meetingItems;
        }, (error: AppError) => {
          if (error instanceof ForbiddenError)
            this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
          else if (error instanceof NotFoundError)
            this.toasterService.pop('error', 'Error', 'Tenant not found!');
          else {
            this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
            throw error;
          }
        });
    }
    else {
      // load all expired questionnaires -> for review results option
      this.meetingItemService.getPage(page, 'buildings', this.jwtService.getBuildingIdFromToken(),
        'meeting_items/finished_questionnaires')
        .subscribe(meetingItems => {
          if (meetingItems.length !== 0)
            this.totalItems = meetingItems[0].numberOfElements;
          else this.totalItems = 0;
          this.meetingItems = meetingItems;
        }, (error: AppError) => {
          if(error instanceof ForbiddenError)
            this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
          else if(error instanceof NotFoundError)
            this.toasterService.pop('error', 'Error', 'Building not found');
          else {
            this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
            throw error;
          }
        });
    }
  }
}
