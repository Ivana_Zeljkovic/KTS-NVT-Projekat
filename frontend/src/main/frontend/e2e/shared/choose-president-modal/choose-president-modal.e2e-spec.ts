import {browser, by, element} from "protractor";
// page
import {LoginPage} from "../../page-objects/login.po";
import {BuildingListPage} from "../../buildings/building-list/page-objects/building-list.po";
import {BuildingRegistrationPage} from "../../buildings/building-registration/page-objects/building-registration.po";
import {ChoosePresidentModalPage} from "./page-objects/choose-president-modal.po";

describe('Choose president modal', () => {
  let loginPage: LoginPage;
  let choosePresidentModalPage: ChoosePresidentModalPage;
  let buildingsListPage: BuildingListPage;
  let buildingRegistrationPage: BuildingRegistrationPage;

  beforeEach(() => {
    loginPage = new LoginPage();
    choosePresidentModalPage = new ChoosePresidentModalPage();
    buildingsListPage = new BuildingListPage();
    buildingRegistrationPage = new BuildingRegistrationPage();
    browser.get("/");

    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');
    loginPage.setUsername('ivana');
    loginPage.setPassword('ivana');
    loginPage.getSignInButton().click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/home');
    browser.get('http://localhost:49152/#/buildings');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/buildings');

  });

  afterEach(() => {
    browser.executeScript('window.localStorage.clear();');
  });

  it('should load all elements of modal when there are tenants', () => {
    let chooseButton = element(by.xpath("//tr[3]/td[7]")).element(by.css('.btn.btn-success'));
    chooseButton.click();
    expect(choosePresidentModalPage.getPageTitle().isDisplayed()).toBe(true);
    expect(choosePresidentModalPage.getTenantsTable().isDisplayed()).toBe(true);
    expect(choosePresidentModalPage.getPagination().isDisplayed()).toBe(true);
    expect(choosePresidentModalPage.getCloseButton().isDisplayed()).toBe(true);
  });

  it('should load all elements of modal when there are no tenants', () => {
    let chooseButton = element(by.xpath("//tr[1]/td[7]")).element(by.css('.btn.btn-success'));
    chooseButton.click();
    expect(choosePresidentModalPage.getPageTitle().isDisplayed()).toBe(true);
    expect(choosePresidentModalPage.getNoElements().isDisplayed()).toBe(true);
    expect(choosePresidentModalPage.getPagination().isPresent()).toBe(false);
  });

});
