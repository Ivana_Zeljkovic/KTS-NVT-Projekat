package rs.ac.uns.ftn.ktsnvt.service.unit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Business;
import rs.ac.uns.ftn.ktsnvt.model.entity.Offer;
import rs.ac.uns.ftn.ktsnvt.model.enumeration.BusinessType;
import rs.ac.uns.ftn.ktsnvt.repository.OfferRepository;
import rs.ac.uns.ftn.ktsnvt.service.OfferService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.BDDMockito.given;

/**
 * Created by Katarina Cukurov on 30/11/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class OfferServiceUnitTest {

    @Autowired
    private OfferService offerService;

    @MockBean
    private OfferRepository offerRepository;


    @Before
    public void setUp(){

      Offer offer = new Offer(false, 500);
      Business business = new Business("kacin biznis", "jako dobar", "13245987", "kacinbiznis@gmail.com",
              BusinessType.OTHER, false);

      business.setId(new Long(100));
      offer.setBusiness(business);

      given(
              this.offerRepository.findOne(new Long(100))
      ).willReturn(
              offer
      );

      given(
              this.offerRepository.findOne(new Long(500))
      ).willReturn(
              null
      );
    }

    @Test
    public void testFindOne() {
      /*
         * Test proverava ispravnost rada metode findOne servisa OfferService
         * (dobavljanje ponude na osnovu ID-a)
         * u slucaju kada postoji ponuda sa zadatim ID-em
         * */

        /*
        ponuda sa ID-em = 100 ima podatke:
            id = 100,
            accepted = false,
            price = 500,
            business_id = 100,
            damage_id = 100
         */
        Offer offer = this.offerService.findOne(new Long(100));

        assertNotNull(offer);
        assertEquals(false, offer.isAccepted());
        assertEquals(500, offer.getPrice(), 0);
        assertEquals(new Long(100), offer.getBusiness().getId());
    }

    @Test(expected = NotFoundException.class)
    public void testFindOneNonexistent() {
        /*
         * Test proverava ispravnost rada metode findOne servisa OfferService
         * (dobavljanje ponude na osnovu ID-a)
         * u slucaju kada ne postoji ponuda sa zadatim ID-em - ocekivano NotFoundException
         * */

        // offer sa ID-em = 500 ne postoji
        Offer offer = this.offerService.findOne(new Long(500));
    }
}
