package rs.ac.uns.ftn.ktsnvt.service.unit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Building;
import rs.ac.uns.ftn.ktsnvt.model.entity.Council;
import rs.ac.uns.ftn.ktsnvt.model.entity.Meeting;
import rs.ac.uns.ftn.ktsnvt.model.entity.Record;
import rs.ac.uns.ftn.ktsnvt.repository.BuildingRepository;
import rs.ac.uns.ftn.ktsnvt.repository.CouncilRepository;
import rs.ac.uns.ftn.ktsnvt.repository.MeetingRepository;
import rs.ac.uns.ftn.ktsnvt.repository.RecordRepository;
import rs.ac.uns.ftn.ktsnvt.service.BuildingService;
import rs.ac.uns.ftn.ktsnvt.service.CouncilService;
import rs.ac.uns.ftn.ktsnvt.service.MeetingService;
import rs.ac.uns.ftn.ktsnvt.service.RecordService;

import java.util.ArrayList;
import java.util.Calendar;

import static org.junit.Assert.assertNotNull;
import static org.mockito.BDDMockito.given;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RecordServiceUnitTest {

    @Autowired
    private RecordService recordService;

    @MockBean
    private RecordRepository recordRepository;

    @Autowired
    private MeetingService meetingService;

    @MockBean
    private MeetingRepository meetingRepository;

    @MockBean
    private CouncilRepository councilRepository;

    @Before
    public void setUp() {

        Building building = new Building();
        Council council = new Council();
        Meeting meeting = new Meeting();
        Record record = new Record("FSSFAJIFSJA");
        meeting.setId(100L);

        building.setCouncil(council);
        council.setMeetings(new ArrayList<Meeting>() {{
            add(meeting);
        }});
        meeting.setRecord(record);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, 10);
        calendar.add(Calendar.YEAR, -1);
        meeting.setDate(calendar.getTime());


        given(
                this.meetingRepository.findOne(100L)
        ).willReturn(
                meeting
        );

        given(
                this.councilRepository.findOne(100L)
        ).willReturn(
                council
        );

        given(
                this.recordRepository.findOne(100L)
        ).willReturn(
                record
        );

    }


    @Test
    public void testFindOne(){

        Record record = this.recordService.findOne(100L);
        assertNotNull(record);

    }
}
