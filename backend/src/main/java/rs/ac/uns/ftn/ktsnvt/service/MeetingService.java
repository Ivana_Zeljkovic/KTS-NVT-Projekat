package rs.ac.uns.ftn.ktsnvt.service;

import org.springframework.data.domain.Page;
import rs.ac.uns.ftn.ktsnvt.model.entity.Meeting;

import java.util.List;

/**
 * Created by Nikola Garabandic on 22/11/2017.
 */
public interface MeetingService {

    Meeting save(Meeting meeting);

    Meeting findOne(Long meetingId);

    void checkMeetingFinished(Meeting meeting);

    void checkMeetingStart(Meeting meeting);

    List<Meeting> findAll();

    void clearMeetingItemsList(Meeting meeting);

    boolean meetingIsInTheFuture(Meeting meeting);

    Page<Meeting> findAllByBuildingId(Long buildingId, Integer pageNumber, Integer pageSize, String sortDirection,
                                      String sortProperty);

    Page<Meeting> findAllFinishedByBuildingId(Long buildingId, Integer pageNumber, Integer pageSize, String sortDirection,
                                              String sortProperty);

    Meeting findFirstNextMeetingInBuilding(Long councilId);

    void remove(Meeting meeting);
}
