import {LoginPage} from "../../page-objects/login.po";
import {OfferModal} from "./page-objects/offer-modal.po";
import {DamageRequestsPage} from "../damage-requests/page-objects/damage-requests.po";
import {browser} from "protractor";

describe('View list of all requests for offers.', () => {

  let loginPage: LoginPage;

  let damageRequest: DamageRequestsPage;
  let offerModal: OfferModal;

  beforeAll(() => {
    loginPage = new LoginPage();
    damageRequest = new DamageRequestsPage();
    offerModal = new OfferModal();
    browser.get("/");


    loginPage.setUsername('firm2');
    loginPage.setPassword('firm2');

    loginPage.getSignInButton().submit();

    browser.waitForAngular();
    browser.get('http://localhost:49152/#/damage-requests');

  });

  beforeEach(() =>{
    damageRequest.getPopElement().click().then(function () {
      damageRequest.getFirstSendOfferAnchor().click();
    });

  });

  afterAll(() => {
    browser.executeScript('window.localStorage.clear();');
  });

  afterEach(() =>{
      offerModal.getCloseButton().click();
  });

  it('offer can be sent successfully', () =>{
      offerModal.getInput().sendKeys('200');
      expect(offerModal.getButon().isEnabled()).toBe(true);
  });

  it('Invalid input with letters', ()=>{
    offerModal.getInput().sendKeys('abc');
    offerModal.getModalHeader().click();
    expect(offerModal.getAlertDanger().isDisplayed()).toBe(true);
    expect(offerModal.getButon().isEnabled()).toBe(false);
  });


  it('Invalid input with input field containing spaces', ()=>{
    offerModal.getInput().sendKeys('12 3');
    offerModal.getModalHeader().click();
    expect(offerModal.getContainsSpace().isDisplayed()).toBe(true);
    expect(offerModal.getButon().isEnabled()).toBe(false);
  });


  it('Invalid input with input field having more than 10 numbers', ()=>{
    offerModal.getInput().sendKeys('1234797955212015564545852');
    offerModal.getModalHeader().click();
    expect(offerModal.getPricePattern().isDisplayed()).toBe(true);
    expect(offerModal.getButon().isEnabled()).toBe(false);
  });


});
