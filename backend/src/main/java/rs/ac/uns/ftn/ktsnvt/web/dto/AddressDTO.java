package rs.ac.uns.ftn.ktsnvt.web.dto;

import org.hibernate.validator.constraints.NotEmpty;
import rs.ac.uns.ftn.ktsnvt.model.entity.Address;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by Katarina Cukurov on 07/11/2017.
 */
public class AddressDTO implements Serializable{

    @NotNull
    @NotEmpty
    private String street;
    @NotNull
    private int number;
    @NotNull
    @Valid
    private CityDTO city;

    public AddressDTO(){}

    public AddressDTO(String street, int number, CityDTO city) {
        this.street = street;
        this.number = number;
        this.city = city;
    }

    public  AddressDTO(Address address)
    {
        this.street = address.getStreet();
        this.number = address.getNumber();
        this.city = new CityDTO(address.getCity().getName(), address.getCity().getPostalNumber());
    }

    public String getStreet() { return street; }

    public void setStreet(String street) { this.street = street; }

    public int getNumber() { return number; }

    public void setNumber(int number) { this.number = number; }

    public CityDTO getCity() { return city; }

    public void setCity(CityDTO city) { this.city = city; }
}
