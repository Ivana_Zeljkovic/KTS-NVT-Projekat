import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from "@angular/common/http/testing";
// service
import { DamageService } from './damage.service';
// model
import { PageAndSort } from "../../shared/models/page-and-sort";
import { Damage } from "../../shared/models/damage";


describe('DamageService', () => {
  let damageService: DamageService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [ DamageService ]
    });

    damageService = TestBed.get(DamageService);
    httpMock = TestBed.get(HttpTestingController);
  });

  // make sure that there are no outstanding requests
  afterEach(() => {
    httpMock.verify();
  });



  it('should be created', inject([DamageService], (service: DamageService) => {
    expect(service).toBeTruthy();
  }));


  it('should retrieve all damages on page with given number', () => {
    const responseList = [
      {
        'id': 1,
        'description': 'First description',
        'date': new Date(),
        'typeName': 'Electrical',
        'buildingId': 100,
        'address': {
          'street': 'Janka Veselinovica',
          'number': 8,
          'city': {
            'name': 'Novi Sad',
            'postalNumber': '21000'
          }
        },
        'urgent': true,
        'fixed': false,
        'creator': {
          'firstName': 'Danijela',
          'lastName': 'Zeljkovic',
          'username': 'daca',
          'id': 2
        },
        'responsiblePerson': {
          'firstName': 'Ivana',
          'lastName': 'Zeljkovic',
          'username': 'ivana',
          'id': 1
        },
        'numberOfElements': 2
      },
      {
        'id': 2,
        'description': 'Second description',
        'date': new Date(),
        'typeName': 'Plumbing',
        'buildingId': 100,
        'address': {
          'street': 'Janka Veselinovica',
          'number': 8,
          'city': {
            'name': 'Novi Sad',
            'postalNumber': '21000'
          }
        },
        'urgent': false,
        'fixed': false,
        'creator': {
          'firstName': 'Ivana',
          'lastName': 'Zeljkovic',
          'username': 'ivana',
          'id': 1
        },
        'responsiblePerson': {
          'firstName': 'Ivana',
          'lastName': 'Zeljkovic',
          'username': 'ivana',
          'id': 1
        },
        'numberOfElements': 2
      }
    ];

    let page = new PageAndSort('0', '2', 'ASC', ['urgent', 'date']);
    damageService.getPage(page, 'tenants', 1, 'responsible_for_damages')
      .subscribe((damages: Array<Damage>) => {
        expect(damages).toEqual(responseList);
        expect(damages.length).toEqual(2);
      });
    const httpRequest = httpMock.expectOne(
      '/api/tenants/1/responsible_for_damages?' +
      'pageNumber=0&' +
      'pageSize=2&' +
      'sortDirection=ASC&' +
      'sortProperty=urgent&' +
      'sortPropertySecond=date');
    expect(httpRequest.request.method).toEqual('GET');
    expect(httpRequest.request.params.get('pageNumber')).toEqual('0');
    expect(httpRequest.request.params.get('pageSize')).toEqual('2');
    expect(httpRequest.request.params.get('sortDirection')).toEqual('ASC');
    expect(httpRequest.request.params.get('sortProperty')).toEqual('urgent');
    expect(httpRequest.request.params.get('sortPropertySecond')).toEqual('date');
    httpRequest.flush(responseList);
  });


  it('should retrieve damage with given id', () => {
    const response = {
      'id': 1,
      'description': 'First description',
      'date': new Date(),
      'typeName': 'Electrical',
      'buildingId': 100,
      'address': {
        'street': 'Janka Veselinovica',
        'number': 8,
        'city': {
          'name': 'Novi Sad',
          'postalNumber': '21000'
        }
      },
      'urgent': true,
      'fixed': false,
      'creator': {
        'firstName': 'Danijela',
        'lastName': 'Zeljkovic',
        'username': 'daca',
        'id': 2
      },
      'responsiblePerson': {
        'firstName': 'Ivana',
        'lastName': 'Zeljkovic',
        'username': 'ivana',
        'id': 1
      },
      'numberOfElements': 2
    };

    damageService.get(1, 'tenants', 1, 'responsible_for_damages')
      .subscribe((damage: Damage) => {
        expect(damage).toEqual(response);
      });
    const httpRequest = httpMock.expectOne('/api/tenants/1/responsible_for_damages/1');
    expect(httpRequest.request.method).toEqual('GET');
    httpRequest.flush(response);
  });


  it('should save damage and return saved object with id', () => {
    const damageCrete = {
      'description': 'New description',
      'typeName': 'Electrical',
      'urgent': false
    };

    const response = {
      'id': 3,
      'description': 'New description',
      'date': new Date(),
      'typeName': 'Electrical',
      'buildingId': 100,
      'address': {
        'street': 'Janka Veselinovica',
        'number': 8,
        'city': {
          'name': 'Novi Sad',
          'postalNumber': '21000'
        }
      },
      'urgent': false,
      'fixed': false,
      'creator': {
        'firstName': 'Danijela',
        'lastName': 'Zeljkovic',
        'username': 'daca',
        'id': 2
      },
      'responsiblePerson': {
        'firstName': 'Ivana',
        'lastName': 'Zeljkovic',
        'username': 'ivana',
        'id': 1
      },
      'numberOfElements': 2
    };

    damageService.save(damageCrete, 'buildings', 100, 'damages')
      .subscribe((damage: Damage) => {
        expect(damage).toEqual(response);
      });
    const httpRequest = httpMock.expectOne('/api/buildings/100/damages');
    expect(httpRequest.request.method).toEqual('POST');
    httpRequest.flush(response);
  });


  it('should update damage and return updated object', () => {
    const damageUpdate = {
      'description': 'Updated description',
      'urgent': true
    };

    const response = {
      'id': 3,
      'description': 'Updated description',
      'date': new Date(),
      'typeName': 'Electrical',
      'buildingId': 100,
      'address': {
        'street': 'Janka Veselinovica',
        'number': 8,
        'city': {
          'name': 'Novi Sad',
          'postalNumber': '21000'
        }
      },
      'urgent': true,
      'fixed': false,
      'creator': {
        'firstName': 'Danijela',
        'lastName': 'Zeljkovic',
        'username': 'daca',
        'id': 2
      },
      'responsiblePerson': {
        'firstName': 'Ivana',
        'lastName': 'Zeljkovic',
        'username': 'ivana',
        'id': 1
      },
      'numberOfElements': 2
    };

    damageService.update(3, damageUpdate, 'tenants', 1, 'my_damages')
      .subscribe((damage: Damage) => {
        expect(damage).toEqual(response);
      });
    const httpRequest = httpMock.expectOne('/api/tenants/1/my_damages/3');
    expect(httpRequest.request.method).toEqual('PUT');
    httpRequest.flush(response);
  });


  it('should remove damage with given id', () => {
    damageService.remove(3, 'tenants', 1, 'my_damages')
      .subscribe(() => {});
    const httpRequest = httpMock.expectOne('/api/tenants/1/my_damages/3');
    expect(httpRequest.request.method).toEqual('DELETE');
    httpRequest.flush({});
  });


  it('should retrieve all damages that are created in given date range', () => {
    const date1 = new Date();
    date1.setFullYear(2018, 1, 1);
    date1.setHours(12, 0, 0);
    const date2 = new Date();
    date2.setFullYear(2018, 1, 2);
    date2.setHours(13, 10,0);

    const responseList = [
      {
        'id': 1,
        'description': 'First description',
        'date': date1,
        'typeName': 'Electrical',
        'buildingId': 100,
        'address': {
          'street': 'Janka Veselinovica',
          'number': 8,
          'city': {
            'name': 'Novi Sad',
            'postalNumber': '21000'
          }
        },
        'urgent': true,
        'fixed': false,
        'creator': {
          'firstName': 'Danijela',
          'lastName': 'Zeljkovic',
          'username': 'daca',
          'id': 2
        },
        'responsiblePerson': {
          'firstName': 'Ivana',
          'lastName': 'Zeljkovic',
          'username': 'ivana',
          'id': 1
        },
        'numberOfElements': 2
      },
      {
        'id': 2,
        'description': 'Second description',
        'date': date2,
        'typeName': 'Plumbing',
        'buildingId': 100,
        'address': {
          'street': 'Janka Veselinovica',
          'number': 8,
          'city': {
            'name': 'Novi Sad',
            'postalNumber': '21000'
          }
        },
        'urgent': false,
        'fixed': false,
        'creator': {
          'firstName': 'Ivana',
          'lastName': 'Zeljkovic',
          'username': 'ivana',
          'id': 1
        },
        'responsiblePerson': {
          'firstName': 'Ivana',
          'lastName': 'Zeljkovic',
          'username': 'ivana',
          'id': 1
        },
        'numberOfElements': 2
      }
    ];

    let page = new PageAndSort('0', '2', 'ASC', ['urgent', 'date']);
    damageService.searchFromTo('tenants', 1, 'responsible_for_damages',
      '2018-01-01 10:00:00', '2018-01-10 10:00:00', page)
      .subscribe((damages: Array<Damage>) => {
        expect(damages).toEqual(responseList);
        expect(damages.length).toEqual(2);
      });
    const httpRequest = httpMock.expectOne(
      '/api/tenants/1/responsible_for_damages_from_to?' +
      'from=2018-01-01%2010:00:00&' +
      'to=2018-01-10%2010:00:00&' +
      'pageNumber=0&' +
      'pageSize=2&' +
      'sortDirection=ASC&' +
      'sortProperty=urgent&' +
      'sortPropertySecond=date');
    expect(httpRequest.request.method).toEqual('GET');
    expect(httpRequest.request.params.get('from')).toEqual('2018-01-01 10:00:00');
    expect(httpRequest.request.params.get('to')).toEqual('2018-01-10 10:00:00');
    expect(httpRequest.request.params.get('pageNumber')).toEqual('0');
    expect(httpRequest.request.params.get('pageSize')).toEqual('2');
    expect(httpRequest.request.params.get('sortDirection')).toEqual('ASC');
    expect(httpRequest.request.params.get('sortProperty')).toEqual('urgent');
    expect(httpRequest.request.params.get('sortPropertySecond')).toEqual('date');
    httpRequest.flush(responseList);
  });
});
