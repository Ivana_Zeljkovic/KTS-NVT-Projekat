package rs.ac.uns.ftn.ktsnvt.web.dto;

import rs.ac.uns.ftn.ktsnvt.model.entity.Building;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ivana Zeljkovic on 06/11/2017.
 */
public class BuildingDTO implements Serializable {

    private Long id;
    private AddressDTO address;
    private List<ApartmentDTO> apartments;
    private TenantDTO presidentOfCouncil;
    private BusinessDTO responsibleCompany;
    private long numberOfElements;
    private boolean hasParking;
    private int numberOfFloors;

    public BuildingDTO(Building building) {
        this.id = building.getId();
        this.address = new AddressDTO(building.getAddress());
        this.apartments = new ArrayList<>();
        building.getApartments().forEach(apartment -> this.apartments.add(new ApartmentDTO(apartment)));
        this.presidentOfCouncil = building.getCouncil().getPresident() == null ? null : new TenantDTO(building.getCouncil().getPresident());
        this.responsibleCompany = building.getCouncil().getResponsibleCompany() == null ? null : new BusinessDTO(building.getCouncil().getResponsibleCompany());
        this.hasParking = building.isHasParking();
        this.numberOfFloors = building.getNumberOfFloors();
    }

    public BuildingDTO(Building building, long totalElements) {

        this.id = building.getId();
        this.address = new AddressDTO(building.getAddress());
        this.apartments = new ArrayList<>();
        building.getApartments().forEach(apartment -> this.apartments.add(new ApartmentDTO(apartment)));
        this.presidentOfCouncil = building.getCouncil().getPresident() == null ? null : new TenantDTO(building.getCouncil().getPresident());
        this.responsibleCompany = building.getCouncil().getResponsibleCompany() == null ? null : new BusinessDTO(building.getCouncil().getResponsibleCompany());
        this.numberOfElements = totalElements;
        this.hasParking = building.isHasParking();
        this.numberOfFloors = building.getNumberOfFloors();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AddressDTO getAddress() {
        return address;
    }

    public void setAddress(AddressDTO address) {
        this.address = address;
    }

    public List<ApartmentDTO> getApartments() {
        return apartments;
    }

    public void setApartments(List<ApartmentDTO> apartments) {
        this.apartments = apartments;
    }

    public TenantDTO getPresidentOfCouncil() {
        return presidentOfCouncil;
    }

    public void setPresidentOfCouncil(TenantDTO presidentOfCouncil) {
        this.presidentOfCouncil = presidentOfCouncil;
    }

    public BusinessDTO getResponsibleCompany() {
        return responsibleCompany;
    }

    public void setResponsibleCompany(BusinessDTO responsibleCompany) {
        this.responsibleCompany = responsibleCompany;
    }

    public long getNumberOfElements() { return numberOfElements; }

    public void setNumberOfElements(long numberOfElements) { this.numberOfElements = numberOfElements; }

    public boolean isHasParking() { return hasParking; }

    public void setHasParking(boolean hasParking) { this.hasParking = hasParking; }

    public int getNumberOfFloors() { return numberOfFloors; }

    public void setNumberOfFloors(int numberOfFloors) { this.numberOfFloors = numberOfFloors; }
}
