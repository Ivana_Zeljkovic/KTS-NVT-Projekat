import {Tenant} from "./tenant";
import {Questionnaire} from "./questionnaire";

export class MeetingItemDetails {
  constructor(private id: number,
              private title: string,
              private content: string,
              private creator: Tenant,
              private questionnaire?: Questionnaire) { }
}
