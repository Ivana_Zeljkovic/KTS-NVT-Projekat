export class MeetingCreate {
  constructor(public meetingDate: Date,
              public duration: number,
              public meetingItemsId: Array<number>) { }
}
