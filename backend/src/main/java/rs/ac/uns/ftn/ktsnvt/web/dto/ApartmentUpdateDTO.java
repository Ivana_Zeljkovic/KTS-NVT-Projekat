package rs.ac.uns.ftn.ktsnvt.web.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by Katarina Cukurov on 16/11/2017.
 */
public class ApartmentUpdateDTO implements Serializable {

    @NotNull
    private Long id;
    @NotNull
    private int number;
    @NotNull
    private int floor;
    @NotNull
    private int surface;

    public ApartmentUpdateDTO() {}

    public ApartmentUpdateDTO(Long id, int number, int floor, int surface) {
        this.id = id;
        this.number = number;
        this.floor = floor;
        this.surface = surface;
    }

    public Long getId() {return id;}

    public void setId(Long id) {this.id = id;}

    public int getNumber() {return number;}

    public void setNumber(int number) {this.number = number;}

    public int getFloor() {return floor;}

    public void setFloor(int floor) {this.floor = floor;}

    public int getSurface() {return surface;}

    public void setSurface(int surface) {this.surface = surface;}
}
