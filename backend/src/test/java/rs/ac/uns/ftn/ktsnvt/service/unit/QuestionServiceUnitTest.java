package rs.ac.uns.ftn.ktsnvt.service.unit;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Question;
import rs.ac.uns.ftn.ktsnvt.model.entity.Questionnaire;
import rs.ac.uns.ftn.ktsnvt.repository.QuestionRepository;
import rs.ac.uns.ftn.ktsnvt.repository.QuestionnaireRepository;
import rs.ac.uns.ftn.ktsnvt.service.QuestionService;
import rs.ac.uns.ftn.ktsnvt.web.dto.VoteQuestionCreateDTO;


import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.BDDMockito.given;

/**
 * Created by Nikola Garabandic on 30/11/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class QuestionServiceUnitTest {

    @Autowired
    private QuestionService questionService;

    @MockBean
    private QuestionnaireRepository questionnaireRepository;

    @MockBean
    private QuestionRepository questionRepository;


    @Before
    public void setUp(){
        Question question = new Question("Changing the council president");
        question.setId(1L);

        question.setAnswers(new ArrayList<String>(){{add("Yes");}});

        Questionnaire questionnaire = new Questionnaire();
        questionnaire.setDateExpire(new Date());

        questionnaire.getQuestions().add(question);

        given(
                this.questionnaireRepository.findOne(1L)
        ).willReturn(
                questionnaire
        );

        given(
                this.questionnaireRepository.findOne(2L)
        ).willReturn(
                null
        );

        given(
                this.questionRepository.findOne(1L)
        ).willReturn(
                question
        );

        given(
                this.questionRepository.findOne(2L)
        ).willReturn(
                null
        );
    }

    @Test
    public void testCheckQuestions(){
        /*
         * Test proverava ispravnost rada metode checkQuestions servisa QuestionService
         * (proverava da li je poslat odgovor koji se nalazi u listi ponudjenih odgovora)
         * pri cemu poslati odgovor postoji u listu ponudjenih odgovora
         * */

        Map<Long, String> map = new HashMap<>();
        map.put(1L, "Yes");

        this.questionService.checkQuestions(map, 1L);
    }

    @Test(expected = BadRequestException.class)
    public void testCheckQuestionsNotFound(){
        /*
         * Test proverava ispravnost rada metode CheckQuestions servisa QuestionService
         * (proverava da li je poslat odgovor koji se nalazi u listi ponudjenih odgovora)
         * pri cemu poslati odgovor ne postoji u listi ponudjenih odgovora - ocekivano BadRequestException
         * */

        Map<Long, String> map = new HashMap<>();
        VoteQuestionCreateDTO voteQuestionCreateDTO = new VoteQuestionCreateDTO(2L,"No");
        map.put(voteQuestionCreateDTO.getQuestionId(), voteQuestionCreateDTO.getAnswer());

        this.questionService.checkQuestions(map, 1L);
    }

    @Test
    @Transactional
    public void testFindOne() {
        /*
         * Test proverava ispravnost metode findOne servisa QuestionService
         * (dobavljanje objekta pitanja na osnovu zadatog ID-a)
         * u slucaju kada postoji pitanje sa zadatim ID-em
         * */

        // postoji pitanje sa ID-em = 1
        Question question = this.questionService.findOne(1L);

        assertNotNull(question);
        assertEquals(Long.valueOf(1), question.getId());
        assertEquals(1, question.getAnswers().size());
    }

    @Test(expected = NotFoundException.class)
    public void testFindOneUnexistentQuestion() {
        /*
         * Test proverava ispravnost metode findOne servisa QuestionService
         * (dobavljanje objekta pitanja na osnovu zadatog ID-a)
         * u slucaju kada postoji ne pitanje sa zadatim ID-em - ocekivano NotFoundException
         * */

        // ne postoji pitanje sa ID-em = 2
        Question question = this.questionService.findOne(2L);
    }
}
