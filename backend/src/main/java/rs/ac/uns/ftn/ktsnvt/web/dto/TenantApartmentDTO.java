package rs.ac.uns.ftn.ktsnvt.web.dto;

import rs.ac.uns.ftn.ktsnvt.model.entity.Apartment;
import rs.ac.uns.ftn.ktsnvt.model.entity.Tenant;

import java.io.Serializable;

/**
 * Created by Katarina Cukurov on 15/11/2017.
 */
public class TenantApartmentDTO implements Serializable {

    private Long tenantId;
    private Long apartmentId;

    public TenantApartmentDTO() {}

    public TenantApartmentDTO(Tenant tenant, Apartment apartment) {
        this.tenantId = tenant.getId();
        this.apartmentId = apartment.getId();
    }

    public TenantApartmentDTO(Long tenantId, Long apartmentId) {
        this.tenantId = tenantId;
        this.apartmentId = apartmentId;
    }

    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    public Long getApartmentId() {
        return apartmentId;
    }

    public void setApartmentId(Long apartmentId) {
        this.apartmentId = apartmentId;
    }
}
