import {by, element} from "protractor";

export class CommentModalPage {

  getComponent() {
    return element(by.tagName('app-comment-modal'));
  }

  getTextArea(){
    return element(by.css('.modal-body textarea'))
  }

  setComment(value: string){
    this.getTextArea().sendKeys(value);
  }

  getSumbitCommentButton(){
    return element(by.css('.modal-footer .btn-success'))
  }

  getComments(){
    return element.all(by.id('commentCreatorUsername'));
  }

  getRemoveFirstComment(){
    return element.all(by.css('.fa-trash')).get(0);
  }

  getFirstPopElement(){
    return element.all(by.css('.fa-ellipsis-v')).get(0);
  }

  getCommentHeader(){
    return element(by.css('.modal-header'));
  }

  getCommentRequired(){
    return element(by.id('commentRequired'));
  }

  getCommentMinimumLength(){
    return element(by.id('commentMinimumLength'));
  }

}
