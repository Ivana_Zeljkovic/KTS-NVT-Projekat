package rs.ac.uns.ftn.ktsnvt.repository.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.model.entity.Record;
import rs.ac.uns.ftn.ktsnvt.repository.RecordRepository;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Ivana Zeljkovic on 07/12/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class RecordRepositoryIntegrationTest {

    @Autowired
    private RecordRepository recordRepository;


    @Test
    @Transactional
    public void testFindAllByBuildingId() {
       /*
        * Test proverava ispravnost rada metode findAllByBuildingId repozitorijuma RecordRepository
        * (pronalazenje svih izvestaja na osnovu ID-a zgrade)
        * */

        /*
        zgrada sa ID-em = 100 ima tacno jedan izvestaj sa sledecim podacima:
           ID = 100,
            content = First record
        */
        Page<Record> records = this.recordRepository.findAllByBuildingId(100L, new Date(),
                        new PageRequest(0, 3, Sort.Direction.ASC, "id"));

        assertEquals(1, records.getTotalElements());
        assertEquals("First record", records.getContent().get(0).getContent());
        assertEquals(Long.valueOf(100), records.getContent().get(0).getId());
    }

/*    @Test
    public void testFindOneByMeetingId() {
        /*
         * Test proverava ispravnost rada metode findOneByMeetingId repozitorijuma RecordRepository
         * (pronalazenje izvestaja na osnovu ID-a sastanka)
         * */

        /*
        sastanku sa ID-em = 100 pripada izvrstaj sa podacima:
            id = 100,
            content = First record

        Record record = this.recordRepository.findOneByMeetingId(100L);

        assertNotNull(record);
        assertEquals(Long.valueOf(100), record.getId());
        assertEquals("First record", record.getContent());
    }
*/
    @Test
    public void testFindOneByIdAndCouncilId() {
        /*
         * Test proverava ispravnost rada metode findOneByMeetingId repozitorijuma RecordRepository
         * (pronalazenje izvestaja na osnovu ID-eva izvestaja i skupstine kojoj pripada sastanak ciji se izvestaj dobavlja)
         * */

        /*
        skupstini stanara sa ID-em = 100 pripada izvrstaj sa ID-em = 100 i podacima:
            id = 100,
            content = First record
        */
        Record record = this.recordRepository.findOneByIdAndCouncilId(Long.valueOf(100), Long.valueOf(100));

        assertNotNull(record);
        assertEquals(Long.valueOf(100), record.getId());
        assertEquals("First record", record.getContent());
    }
}
