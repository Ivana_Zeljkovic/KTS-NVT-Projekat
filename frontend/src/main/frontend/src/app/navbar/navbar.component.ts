import { Component } from '@angular/core';
import { Router } from "@angular/router";
// service
import { JwtService } from "../core/services/jwt.service";
import { AuthService } from "../core/services/auth.service";
import { DamagesCategoryService } from "../core/services/damages-category.service";
import { BsModalService } from "ngx-bootstrap";
import {CouncilMemberComponent} from "../shared/components/council-member/council-member.component";


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {
  constructor(protected jwtService: JwtService, private authService: AuthService,
              private router: Router, private damagesCategory: DamagesCategoryService,
              private modalService: BsModalService) { }

  logout() {
    this.authService.logout();
    this.router.navigate(['login']);
  }

  setCategory(category: string) {
    this.damagesCategory.changeCategory(category);
    this.router.navigate(['damages']);
  }

  notOnHomeRoute(): boolean {
    return this.router.url !== '/home';
  }

  notOnRegisterBuildingRoute(): boolean{
    return this.router.url !== '/buildings/register';
  }

  delegateResponsibilityToRooommate() {
    const modalRef = this.modalService.show(CouncilMemberComponent);
    modalRef.content.choosePresident = false;
  }

  resignate() {
    const modalRef = this.modalService.show(CouncilMemberComponent);
    modalRef.content.choosePresident = true;
  }
}
