package rs.ac.uns.ftn.ktsnvt.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import rs.ac.uns.ftn.ktsnvt.model.entity.Damage;

import java.util.Date;

/**
 * Created by Ivana Zeljkovic on 23/11/2017.
 */
public interface DamageRepository extends JpaRepository<Damage, Long> {

    @Query("SELECT d FROM Damage d WHERE d.building.id = :buildingId")
    Page<Damage> findAllByBuilding(@Param("buildingId") Long buildingId, Pageable pageable);


    @Query(value = "SELECT d FROM Damage d WHERE d.responsibleInstitution.id = :id AND d.fixed = false")
    Page<Damage> getAllForInstitution(@Param("id") Long institutionId, Pageable pageable);


    @Query(value = "SELECT d FROM Damage d WHERE d.responsibleInstitution.id = :id AND " +
            "d.fixed = false AND d.date BETWEEN :fromDate AND :toDate")
    Page<Damage> getAllForInstitutionFromTo(@Param("id") Long institutionId, @Param("fromDate") Date fromDate,
                                            @Param("toDate") Date toDate, Pageable pageable);


    @Query(value = "SELECT d FROM Damage d WHERE d.selectedBusiness.id = :id AND d.fixed = false")
    Page<Damage> getAllForRepair(@Param("id") Long businessId, Pageable pageable);


    @Query(value = "SELECT d FROM Damage d WHERE d.selectedBusiness.id = :id AND " +
            "d.fixed = false AND d.date BETWEEN :fromDate AND :toDate")
    Page<Damage> getAllForRepairFromTo(@Param("id") Long businessId, @Param("fromDate") Date fromDate,
                                       @Param("toDate") Date toDate, Pageable pageable);


    @Query(value = "SELECT d FROM Damage d WHERE d.building.id = :id")
    Page<Damage> getAllInBuilding(@Param("id") Long buildingId, Pageable pageable);

    @Query(value = "SELECT d FROM Damage d WHERE d.building.id = :id AND d.date BETWEEN :fromDate AND :toDate")
    Page<Damage> getAllInBuildingFromTo(@Param("id") Long buildingId, @Param("fromDate") Date fromDate,
                                        @Param("toDate") Date toDate, Pageable pageable);


    @Query(value = "SELECT d FROM Damage d WHERE d.responsiblePerson.id = :id AND d.fixed = false")
    Page<Damage> getAllForTenant(@Param("id") Long tenantId, Pageable pageable);


    @Query(value = "SELECT d FROM Damage d WHERE d.responsiblePerson.id = :id AND " +
            "d.fixed = false AND d.date BETWEEN :fromDate AND :toDate")
    Page<Damage> getAllForTenantFromTo(@Param("id") Long tenantId, @Param("fromDate") Date fromDate,
                                       @Param("toDate") Date toDate, Pageable pageable);


    @Query(value = "SELECT d FROM Damage d WHERE d.apartment.id IN " +
            "(SELECT a.id FROM Apartment a WHERE a.owner.id = :id) AND d.fixed = false")
    Page<Damage> getAllInPersonalApartments(@Param("id") Long tenantId, Pageable pageable);


    @Query(value = "SELECT d FROM Damage d WHERE d.apartment.id IN " +
            "(SELECT a.id FROM Apartment a WHERE a.owner.id = :id) AND d.fixed = false " +
            "AND d.date BETWEEN :fromDate AND :toDate")
    Page<Damage> getAllInPersonalApartmentsFromTo(@Param("id") Long tenantId, @Param("fromDate") Date fromDate,
                                                  @Param("toDate") Date toDate, Pageable pageable);


    @Query(value = "SELECT d FROM Damage d WHERE d.apartment.id = :id AND d.fixed = false")
    Page<Damage> getAllInApartment(@Param("id") Long apartmentId, Pageable pageable);


    @Query(value = "SELECT d FROM Damage d WHERE d.apartment.id = :id AND " +
            "d.fixed = false AND d.date BETWEEN :fromDate AND :toDate")
    Page<Damage> getAllInApartmentFromTo(@Param("id") Long apartmentId, @Param("fromDate") Date fromDate,
                                   @Param("toDate") Date toDate, Pageable pageable);


    @Query(value = "SELECT d FROM Damage d WHERE d.creator.id = :id AND d.fixed = false")
    Page<Damage> getAllMyDamages(@Param("id") Long tenantId, Pageable pageable);


    @Query(value = "SELECT d FROM Damage d WHERE d.creator.id = :id AND " +
            "d.fixed = false AND d.date BETWEEN :fromDate AND :toDate")
    Page<Damage> getAllMyDamagesFromTo(@Param("id") Long tenantId, @Param("fromDate") Date fromDate,
                                       @Param("toDate") Date toDate, Pageable pageable);


    @Query(value ="SELECT * FROM Damage d WHERE d.id = :damage_id AND :damage_id =" +
            "(SELECT damage_id FROM  damage_businesses_damage_sent_to_ids db " +
            "WHERE db.businesses_damage_sent_to_ids = :business_id AND :damage_id = damage_id )" ,nativeQuery = true)
    Damage findDamageIdInOfferRequest(@Param("damage_id")Long damageId,@Param("business_id") Long businessId);



    @Query(value = "SELECT * FROM DAMAGE  d WHERE d.id in (SELECT damage_id FROM damage_businesses_damage_sent_to_ids dbdsti WHERE " +
            ":business_id = dbdsti.businesses_damage_sent_to_ids) AND d.id NOT in (SELECT damage_id FROM Offer o WHERE :business_id = o.business_id) " +
            "ORDER BY ?#{#pageable}" ,
            countQuery = "SELECT * FROM DAMAGE  d WHERE d.id in (SELECT damage_id FROM damage_businesses_damage_sent_to_ids dbdsti WHERE " +
                    ":business_id = dbdsti.businesses_damage_sent_to_ids) AND d.id NOT in (SELECT damage_id FROM Offer o WHERE :business_id = o.business_id)"
            , nativeQuery = true)
    Page<Damage> findAllDamageFixRequests(@Param("business_id") Long businessId, Pageable pageable);


}
