import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
// service
import { DataService } from "./data.service";
// models
import { DamageTypeResponsibility } from "../../shared/models/damage-type-responsibility";


@Injectable()
export class DamageTypeResponsibilityService extends DataService<DamageTypeResponsibility>{
  constructor(http: HttpClient) {
    super('', http);
  }
}
