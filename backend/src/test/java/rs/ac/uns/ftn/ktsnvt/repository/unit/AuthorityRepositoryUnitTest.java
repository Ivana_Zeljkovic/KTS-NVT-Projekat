package rs.ac.uns.ftn.ktsnvt.repository.unit;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.model.entity.Authority;
import rs.ac.uns.ftn.ktsnvt.repository.AuthorityRepository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by Nikola Garabandic on 30/11/2017.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class AuthorityRepositoryUnitTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private AuthorityRepository authorityRepository;

    private Long authority_id;


    @Before
    public void setUp(){
        Authority authority_tenant = new Authority("TENANT");

        this.authority_id = entityManager.persistAndGetId(authority_tenant, Long.class);
    }

    @Test
    public void testFindByName(){
        /*
        * Test proverava ispravnost rada metode FindByName repozitorijuma AuthorityRepository
        * (pronalazenje role na osnovu njenog imena)
        */

        // postoji rola sa name = TENANT
        Authority authority = authorityRepository.findByName("TENANT");

        assertNotNull(authority);
        assertEquals(this.authority_id, authority.getId());
        assertEquals("TENANT", authority.getName());
    }

    @Test
    public void testFindNoneByName(){
        /*
        * Test proverava ispravnost rada metode FindByName repozitorijuma AuthorityRepository
        * (pronalazenje role na osnovu njenog imena)
        *
        */

        // ne postoji rola sa name = TENAANT
        Authority authority = authorityRepository.findByName("TENAANT");

        assertNull(authority);
    }
}
