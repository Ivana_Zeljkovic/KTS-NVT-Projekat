import {by, element, protractor} from "protractor";

export class BusinessAddAcountModalPage{
  getUsernameField(){
    return element(by.id('usernameModal'));
  }

  getPasswordField(){
    return element(by.id('passwordModal'));
  }

  getConfirmPasswordField(){
    return element(by.id('confirmPasswordModal'));
  }

  getAddAccountButton(){
    return element(by.css('.modal-footer .btn-confirm'));
  }

  getCloseButton(){
    return element(by.id('closeButtonAccountModal'))
  }

  getUsernameRequired(){
    return element(by.id('usernameModalRequired'));
  }

  getUsernameContainsSpace(){
    return element(by.id('usernameModalContainSpace'));
  }

  getUsernameIsTaken(){
    return element(by.id('usernameModalTaken'));
  }

  getPasswordRequired(){
    return element(by.id('passwordModalRequired'));
  }

  getPasswordMinimumLength(){
    return element(by.id('passwordModalMinLength'));
  }

  getConfirmPasswordRequired(){
    return element(by.id('confirmPasswordModalRequired'));
  }

  getConfirmPasswordNotMatch(){
    return element(by.id('confirmPasswordModalNotMatch'));
  }

  setUsername(value : string){
    this.clearField(this.getUsernameField());
    this.getUsernameField().sendKeys(value);
  }

  setPassword(value : string){
    this.clearField(this.getPasswordField());
    this.getPasswordField().sendKeys(value);
  }

  setConfirmPassword(value : string){
    this.clearField(this.getConfirmPasswordField());
    this.getConfirmPasswordField().sendKeys(value);
  }

  clearField(field) {
    field.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, "a"));
    field.sendKeys(protractor.Key.BACK_SPACE);
    field.clear();
  }
}
