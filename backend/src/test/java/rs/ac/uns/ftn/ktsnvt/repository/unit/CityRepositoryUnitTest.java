package rs.ac.uns.ftn.ktsnvt.repository.unit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.model.entity.City;
import rs.ac.uns.ftn.ktsnvt.repository.CityRepository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Katarina Cukurov on 28/11/2017.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class CityRepositoryUnitTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private CityRepository cityRepository;


    @Before
    public void setUp(){
        // CITY
        City city = new City("Novi Sad", 21000);
        entityManager.persist(city);
    }

    @Test
    public void testFindByName(){
        /*
         * Test proverava ispravnost rada metode findByName repozitorijuma CityRepository
         * (pronalazenje svih gradova koji su u bazi na osnovu imena)
         * */

        /*
        postoji grad sa ime = Novi Sad i sledecim podacima:
            name = Novi Sad,
            postal_number = 21000
        */
        City city = this.cityRepository.findByName("Novi Sad");

        assertNotNull(city);
        assertEquals(21000, city.getPostalNumber());
        assertEquals("Novi Sad", city.getName());
    }

    @Test
    public void testFindByNameAndPostal_number(){
        /*
         * Test proverava ispravnost rada metode findByNameAndPostalNumber repozitorijuma CityRepository
         * (pronalazenje svih gradova koji su u bazi na osnovu imena i postanskog broja)
         * */

        // postoji grad sa imenom: Novi Sad i postanskim brojem: 21000
        City city = this.cityRepository.findByNameAndPostalNumber("Novi Sad", 21000);

        assertNotNull(city);
        assertEquals(21000, city.getPostalNumber());
        assertEquals("Novi Sad", city.getName());
    }
}
