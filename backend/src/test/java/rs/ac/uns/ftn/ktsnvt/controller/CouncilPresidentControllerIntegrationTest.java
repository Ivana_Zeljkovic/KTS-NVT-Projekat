package rs.ac.uns.ftn.ktsnvt.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import rs.ac.uns.ftn.ktsnvt.controller.util.TestUtil;
import rs.ac.uns.ftn.ktsnvt.web.dto.LoginDTO;
import rs.ac.uns.ftn.ktsnvt.web.dto.TokenDTO;
import rs.ac.uns.ftn.ktsnvt.web.dto.UpdatePresidentDTO;

import javax.annotation.PostConstruct;
import java.nio.charset.Charset;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Created by Ivana Zeljkovic on 09/12/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class CouncilPresidentControllerIntegrationTest {

    private static final String URL_PREFIX = "/api/buildings";

    private MediaType contentType = new MediaType(
            MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    private MockMvc mockMvc;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja stanara
    private String accessTokenTenant;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja predsednika skupstine stanara
    private String accessTokenPresident;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja predsednika skupstine stanara
    private String accessTokenPresident_2;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja firme
    private String accessTokenFirm;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja firme
    private String accessTokenFirm_2;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja administratora
    private String accessTokenAdmin;


    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private TestRestTemplate testRestTemplate;


    @PostConstruct
    public void setup() {
        this.mockMvc = MockMvcBuilders.
                webAppContextSetup(webApplicationContext).apply(springSecurity()).build();
    }

    // pre izvrsavanja testa, neophodno je izvrsiti logovanje u aplikaciju da bismo dobili token
    @Before
    public void login() throws Exception {
        // logovanje predsednika (zgrada 100)
        LoginDTO loginDTOPresident = new LoginDTO("kaca", "kaca");

        ResponseEntity<TokenDTO> responseEntityPresident =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOPresident,
                        TokenDTO.class);
        // preuzmemo token za logovanog predsednika skupstine stanara iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenPresident = responseEntityPresident.getBody().getValue();


        // logovanje predsednika (zgrada 101)
        LoginDTO loginDTOPresident2 = new LoginDTO("leontina", "leontina");

        ResponseEntity<TokenDTO> responseEntityPresident2 =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOPresident2,
                        TokenDTO.class);
        // preuzmemo token za logovanog predsednika skupstine stanara iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenPresident_2 = responseEntityPresident2.getBody().getValue();


        // logovanje stanara
        LoginDTO loginDTOTenant = new LoginDTO("nikola", "nikola");

        ResponseEntity<TokenDTO> responseEntityTenant =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOTenant,
                        TokenDTO.class);
        // preuzmemo token za logovanog stanara iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenTenant = responseEntityTenant.getBody().getValue();


        // logovanje firme
        LoginDTO loginDTOFirm = new LoginDTO("firm1", "firm1");

        ResponseEntity<TokenDTO> responseEntityFirm =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOFirm,
                        TokenDTO.class);
        // preuzmemo token za logovanu firmu iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenFirm = responseEntityFirm.getBody().getValue();


        // logovanje firme
        LoginDTO loginDTOFirm2 = new LoginDTO("firm2", "firm2");

        ResponseEntity<TokenDTO> responseEntityFirm2 =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOFirm2,
                        TokenDTO.class);
        // preuzmemo token za logovanu firmu iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenFirm_2 = responseEntityFirm2.getBody().getValue();


        // logovanje firme
        LoginDTO loginDTOAdmin = new LoginDTO("ivana", "ivana");

        ResponseEntity<TokenDTO> responseEntityAdmin =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOAdmin,
                        TokenDTO.class);
        // preuzmemo token za logovanog administratora iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenAdmin = responseEntityAdmin.getBody().getValue();
    }

    // TESTOVI za setUpPresident
    @Test
    @Transactional
    @Rollback(true)
    public void testSetUpPresidentByCurrentPresident() throws Exception {
        /*
        * Test proverava ispravnost rada metode setUpPresident kontrolera CouncilPresidentController
        * (postavljanje prvog/novog predsednika skupstine stanara na nivou zgrade sa zadatim ID-em)
        * pri cemu akciju izvrsava trenutni predsednik skupstine stanara u zgradi sa zadatim ID-em
        * */

        UpdatePresidentDTO updatePresidentDTO = new UpdatePresidentDTO(null, "Nikola",
                "Garabandic","nikola");
        String updatePresidentDTOJson = TestUtil.json(updatePresidentDTO);

        /*
        stanar sa ID-em = 101 je trenutni predsednik skupstine stanara u zgradi sa ID-em = 100,
        pri cemu se ta odgovornost sada prebacuje na stanara sa ID-em = 102 clan skupstine stanara u posmatranoj zgradi
        i ima sledece podtke:
            id = 102,
            firstName = Nikola,
            lastName = Garabandic,
            building_id = 100,
            council_id = 100 (iz zgrade sa ID-em = 100)
        */
        this.mockMvc.perform(patch(URL_PREFIX + "/100/council_president")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(updatePresidentDTOJson))
                .andExpect(status().isOk());

        this.mockMvc.perform(get(URL_PREFIX + "/100")
                .header("Authentication-Token", this.accessTokenAdmin))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(100))
                .andExpect(jsonPath("$.presidentOfCouncil.id").value(102))
                .andExpect(jsonPath("$.presidentOfCouncil.firstName").value("Nikola"))
                .andExpect(jsonPath("$.presidentOfCouncil.lastName").value("Garabandic"));
    }

    @Test
    @Transactional
    @Rollback(true)
    public void testSetUpPresidentByAdmin() throws Exception {
        /*
        * Test proverava ispravnost rada metode setUpPresident kontrolera CouncilPresidentController
        * (postavljanje prvog/novog predsednika skupstine stanara na nivou zgrade sa zadatim ID-em)
        * pri cemu akciju izvrsava administrator jer zgrada sa zadatim ID-em nema predsednika skupstine stanara niti izabranu firmu
        * koja upravlja zgradom
        * */

        UpdatePresidentDTO updatePresidentDTO = new UpdatePresidentDTO(null, "Nestor",
                "Jovanovic","nestor");
        String updatePresidentDTOJson = TestUtil.json(updatePresidentDTO);

        /*
        zgrada sa ID-em = 102 nema ni predsednika skupstine stanara niti izabranu firmu koja upravlja zgradom
        pri cemu se sada postavlja prvi predsednik skupstine stanara koji ima ID = 110, clan je skupstine stanara u posmatranoj zgradi
        i ima sledece podatke:
            id = 110,
            firstName = Nestor,
            lastName = Jovanovic,
            building_id = 102,
            council_id = 102 (iz zgrade sa ID-em = 102)
        */
        this.mockMvc.perform(patch(URL_PREFIX + "/102/council_president")
                .header("Authentication-Token", this.accessTokenAdmin)
                .contentType(contentType)
                .content(updatePresidentDTOJson))
                .andExpect(status().isOk());

        this.mockMvc.perform(get(URL_PREFIX + "/102")
                .header("Authentication-Token", this.accessTokenAdmin))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(102))
                .andExpect(jsonPath("$.presidentOfCouncil.id").value(110))
                .andExpect(jsonPath("$.presidentOfCouncil.firstName").value("Nestor"))
                .andExpect(jsonPath("$.presidentOfCouncil.lastName").value("Jovanovic"));
    }

    @Test
    @Transactional
    @Rollback(true)
    public void testSetUpPresidentBySelectedFirm() throws Exception {
        /*
        * Test proverava ispravnost rada metode setUpPresident kontrolera CouncilPresidentController
        * (postavljanje prvog/novog predsednika skupstine stanara na nivou zgrade sa zadatim ID-em)
        * pri cemu akciju izvrsava izabrana firma koja upravlja zgradom jer zgrada sa zadatim ID-em nema predsednika skupstine stanara
        * */

        UpdatePresidentDTO updatePresidentDTO = new UpdatePresidentDTO(null, "Mirko",
                "Mirkovic","mirko");
        String updatePresidentDTOJson = TestUtil.json(updatePresidentDTO);

        /*
        zgrada sa ID-em = 103 nema predsednika skupstine stanara ali ima izabranu firmu koja upravlja zgradom
        pri cemu se sada postavlja prvi predsednik skupstine stanara koji ima ID = 112, clan je skupstine stanara u posmatranoj zgradi
        i ima sledece podatke:
            id = 112,
            firstName = Mirko,
            lastName = Mirkovic,
            building_id = 103,
            council_id = 103 (iz zgrade sa ID-em = 103)
        */
        this.mockMvc.perform(patch(URL_PREFIX + "/103/council_president")
                .header("Authentication-Token", this.accessTokenFirm)
                .contentType(contentType)
                .content(updatePresidentDTOJson))
                .andExpect(status().isOk());

        this.mockMvc.perform(get(URL_PREFIX + "/103")
                .header("Authentication-Token", this.accessTokenAdmin))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(103))
                .andExpect(jsonPath("$.presidentOfCouncil.id").value(112))
                .andExpect(jsonPath("$.presidentOfCouncil.firstName").value("Mirko"))
                .andExpect(jsonPath("$.presidentOfCouncil.lastName").value("Mirkovic"));
    }

    @Test
    public void testSetUpPresidentByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode setUpPresident kontrolera CouncilPresidentController
        * (postavljanje prvog/novog predsednika skupstine stanara na nivou zgrade sa zadatim ID-em)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        UpdatePresidentDTO updatePresidentDTO = new UpdatePresidentDTO(null, "Mirko",
                "Mirkovic","mirko");
        String updatePresidentDTOJson = TestUtil.json(updatePresidentDTO);

        this.mockMvc.perform(patch(URL_PREFIX + "/103/council_president")
                .contentType(contentType)
                .content(updatePresidentDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSetUpPresidentByNotAuthenticatedPresidentOrFirmOrAdministrator() throws Exception {
        /*
        * Test proverava ispravnost rada metode setUpPresident kontrolera CouncilPresidentController
        * (postavljanje prvog/novog predsednika skupstine stanara na nivou zgrade sa zadatim ID-em)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu predsednika, firme ili administratora
        * */

        UpdatePresidentDTO updatePresidentDTO = new UpdatePresidentDTO(null, "Mirko",
                "Mirkovic","mirko");
        String updatePresidentDTOJson = TestUtil.json(updatePresidentDTO);

        this.mockMvc.perform(patch(URL_PREFIX + "/103/council_president")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(updatePresidentDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSetUpPresidentNonexistentBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode setUpPresident kontrolera CouncilPresidentController
        * (postavljanje prvog/novog predsednika skupstine stanara na nivou zgrade sa zadatim ID-em)
        * u slucaju kada ne postoji zgrada sa zadatim ID-em
        * */

        UpdatePresidentDTO updatePresidentDTO = new UpdatePresidentDTO(null, "Mirko",
                "Mirkovic","mirko");
        String updatePresidentDTOJson = TestUtil.json(updatePresidentDTO);

        // ne postoji zgrada sa ID-em = 200
        this.mockMvc.perform(patch(URL_PREFIX + "/200/council_president")
                .header("Authentication-Token", this.accessTokenFirm)
                .contentType(contentType)
                .content(updatePresidentDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testSetUpPresidentInBuildingWithoutPresidentWithSelectedFirmByAdministrator() throws Exception{
        /*
        * Test proverava ispravnost rada metode setUpPresident kontrolera CouncilPresidentController
        * (postavljanje prvog/novog predsednika skupstine stanara na nivou zgrade sa zadatim ID-em)
        * u slucaju kada zgrada sa zadatim ID-em nema predsednika skupstine stanara, ima izabranu firmu za upravljanje zgradom
        * i akciju izvrsava administrator
        * */

        UpdatePresidentDTO updatePresidentDTO = new UpdatePresidentDTO(null, "Mirko",
                "Mirkovic","mirko");
        String updatePresidentDTOJson = TestUtil.json(updatePresidentDTO);

        // zgrada sa ID-em = 103 nema predsednika skupstine stanara ali ima izabranu firmu koja upravlja zgradom (sa ID-em = 100)
        this.mockMvc.perform(patch(URL_PREFIX + "/103/council_president")
                .header("Authentication-Token", this.accessTokenAdmin)
                .contentType(contentType)
                .content(updatePresidentDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSetUpPresidentInBuildingWithoutPresidentWithSelectedFirmByPresident() throws Exception {
        /*
        * Test proverava ispravnost rada metode setUpPresident kontrolera CouncilPresidentController
        * (postavljanje prvog/novog predsednika skupstine stanara na nivou zgrade sa zadatim ID-em)
        * u slucaju kada zgrada sa zadatim ID-em nema predsednika skupstine stanara, ima izabranu firmu za upravljanje zgradom
        * i akciju izvrsava logovani predsednik koji nije ujedno i predsednik skupstine stanara zadate zgrade
        * */

        UpdatePresidentDTO updatePresidentDTO = new UpdatePresidentDTO(null, "Mirko",
                "Mirkovic","mirko");
        String updatePresidentDTOJson = TestUtil.json(updatePresidentDTO);

        // zgrada sa ID-em = 103 nema predsednika skupstine stanara ali ima izabranu firmu koja upravlja zgradom (sa ID-em = 100)
        this.mockMvc.perform(patch(URL_PREFIX + "/103/council_president")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(updatePresidentDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSetUpPresidentInBuildingWithoutPresidentWithSelectedFirmByFirm() throws Exception {
        /*
        * Test proverava ispravnost rada metode setUpPresident kontrolera CouncilPresidentController
        * (postavljanje prvog/novog predsednika skupstine stanara na nivou zgrade sa zadatim ID-em)
        * u slucaju kada zgrada sa zadatim ID-em nema predsednika skupstine stanara, ima izabranu firmu za upravljanje zgradom
        * i akciju izvrsava logovana firma koja nije ujedno i izabrana firma za upravljanje zadatom zgradom
        * */

        UpdatePresidentDTO updatePresidentDTO = new UpdatePresidentDTO(null, "Mirko",
                "Mirkovic","mirko");
        String updatePresidentDTOJson = TestUtil.json(updatePresidentDTO);

        // zgrada sa ID-em = 103 nema predsednika skupstine stanara ali ima izabranu firmu koja upravlja zgradom (sa ID-em = 100)
        this.mockMvc.perform(patch(URL_PREFIX + "/103/council_president")
                .header("Authentication-Token", this.accessTokenFirm_2)
                .contentType(contentType)
                .content(updatePresidentDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSetUpPresidentInBuildingWithoutPresidentWithoutSelectedFirmByFirm() throws Exception {
        /*
        * Test proverava ispravnost rada metode setUpPresident kontrolera CouncilPresidentController
        * (postavljanje prvog/novog predsednika skupstine stanara na nivou zgrade sa zadatim ID-em)
        * u slucaju kada zgrada sa zadatim ID-em nema predsednika skupstine stanara, nema izabranu firmu za upravljanje zgradom
        * i akciju izvrsava firma
        * */

        UpdatePresidentDTO updatePresidentDTO = new UpdatePresidentDTO(null, "Nestor",
                "Jovanovic","nestor");
        String updatePresidentDTOJson = TestUtil.json(updatePresidentDTO);

        // zgrada sa ID-em = 102 nema predsednika skupstine stanara niti izabranu firmu koja upravlja zgradom
        this.mockMvc.perform(patch(URL_PREFIX + "/102/council_president")
                .header("Authentication-Token", this.accessTokenFirm)
                .contentType(contentType)
                .content(updatePresidentDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSetUpPresidentInBuildingWithoutPresidentWithoutSelectedFirmByPresident() throws Exception {
        /*
        * Test proverava ispravnost rada metode setUpPresident kontrolera CouncilPresidentController
        * (postavljanje prvog/novog predsednika skupstine stanara na nivou zgrade sa zadatim ID-em)
        * u slucaju kada zgrada sa zadatim ID-em nema predsednika skupstine stanara, nema izabranu firmu za upravljanje zgradom
        * i akciju izvrsava korisnik koji ima rolu predsednika skupstine stanara (za neku drugu zgradu)
        * */

        UpdatePresidentDTO updatePresidentDTO = new UpdatePresidentDTO(null, "Nestor",
                "Jovanovic","nestor");
        String updatePresidentDTOJson = TestUtil.json(updatePresidentDTO);

        // zgrada sa ID-em = 102 nema predsednika skupstine stanara niti izabranu firmu koja upravlja zgradom
        this.mockMvc.perform(patch(URL_PREFIX + "/102/council_president")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(updatePresidentDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSetUpPresidentInBuildingWithPresidentByFirm() throws Exception {
        /*
        * Test proverava ispravnost rada metode setUpPresident kontrolera CouncilPresidentController
        * (postavljanje prvog/novog predsednika skupstine stanara na nivou zgrade sa zadatim ID-em)
        * u slucaju kada zgrada sa zadatim ID-em ima predsednika skupstine stanara
        * i akciju izvrsava firma
        * */

        UpdatePresidentDTO updatePresidentDTO = new UpdatePresidentDTO(null, "Nikola",
                "Garabandic","nikola");
        String updatePresidentDTOJson = TestUtil.json(updatePresidentDTO);

        // zgrada sa ID-em = 100 ima predsednika skupstine stanara (stanar sa ID-em = 101)
        this.mockMvc.perform(patch(URL_PREFIX + "/100/council_president")
                .header("Authentication-Token", this.accessTokenFirm)
                .contentType(contentType)
                .content(updatePresidentDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSetUpPresidentInBuildingWithPresidentByAdministrator() throws Exception {
        /*
        * Test proverava ispravnost rada metode setUpPresident kontrolera CouncilPresidentController
        * (postavljanje prvog/novog predsednika skupstine stanara na nivou zgrade sa zadatim ID-em)
        * u slucaju kada zgrada sa zadatim ID-em ima predsednika skupstine stanara
        * i akciju izvrsava administrator
        * */

        UpdatePresidentDTO updatePresidentDTO = new UpdatePresidentDTO(null, "Nikola",
                "Garabandic","nikola");
        String updatePresidentDTOJson = TestUtil.json(updatePresidentDTO);

        // zgrada sa ID-em = 101 ima predsednika skupstine stanara (stanar sa ID-em = 101)
        this.mockMvc.perform(patch(URL_PREFIX + "/100/council_president")
                .header("Authentication-Token", this.accessTokenAdmin)
                .contentType(contentType)
                .content(updatePresidentDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSetUpPresidentInBuildingWithPresidentByPresident() throws Exception{
        /*
        * Test proverava ispravnost rada metode setUpPresident kontrolera CouncilPresidentController
        * (postavljanje prvog/novog predsednika skupstine stanara na nivou zgrade sa zadatim ID-em)
        * u slucaju kada zgrada sa zadatim ID-em ima predsednika skupstine stanara
        * i akciju izvrsava logovani predsednik koji nije ujedno i predsednik skupstine stanara zadate zgrade
        * */

        UpdatePresidentDTO updatePresidentDTO = new UpdatePresidentDTO(null, "Nikola",
                "Garabandic","nikola");
        String updatePresidentDTOJson = TestUtil.json(updatePresidentDTO);

        // zgrada sa ID-em = 101 ima predsednika skupstine stanara (stanar sa ID-em = 101)
        this.mockMvc.perform(patch(URL_PREFIX + "/100/council_president")
                .header("Authentication-Token", this.accessTokenPresident_2)
                .contentType(contentType)
                .content(updatePresidentDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSetUpPresidentQuestionnaireFromInvalidBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode setUpPresident kontrolera CouncilPresidentController
        * (postavljanje prvog/novog predsednika skupstine stanara na nivou zgrade sa zadatim ID-em)
        * u slucaju kada anketa (za izbor predsednika) ciji je ID zadat u DTO-u ne pripada zgradi sa zadatim ID-em
        * */

        UpdatePresidentDTO updatePresidentDTO = new UpdatePresidentDTO(103L, "Nikola",
                "Garabandic","nikola");
        String updatePresidentDTOJson = TestUtil.json(updatePresidentDTO);

        // anketa sa ID-em = 103 pripada zgradi sa ID-em = 101 (a ne sa ID-em = 100)
        this.mockMvc.perform(patch(URL_PREFIX + "/100/council_president")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(updatePresidentDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testSetUpPresidentExistQuestionnaireInDTOByAdministrator() throws Exception {
        /*
        * Test proverava ispravnost rada metode setUpPresident kontrolera CouncilPresidentController
        * (postavljanje prvog/novog predsednika skupstine stanara na nivou zgrade sa zadatim ID-em)
        * u slucaju kada u DTO-u postoji ID ankete (za izbor predsednika) a pri tom akciju izvrsava firma
        * */

        UpdatePresidentDTO updatePresidentDTO = new UpdatePresidentDTO(100L, "Nikola",
                "Garabandic","nikola");
        String updatePresidentDTOJson = TestUtil.json(updatePresidentDTO);

        // anketa sa ID-em = 100 pripada zgradi sa ID-em = 100
        this.mockMvc.perform(patch(URL_PREFIX + "/100/council_president")
                .header("Authentication-Token", this.accessTokenFirm)
                .contentType(contentType)
                .content(updatePresidentDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSetUpPresidentNonexistentTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode setUpPresident kontrolera CouncilPresidentController
        * (postavljanje prvog/novog predsednika skupstine stanara na nivou zgrade sa zadatim ID-em)
        * u slucaju kada ne postoji stanar koji zadovoljava parametre zadate u DTO-u
        * */

        UpdatePresidentDTO updatePresidentDTO = new UpdatePresidentDTO(100L, "Jovo",
                "Radojcic","jovo");
        String updatePresidentDTOJson = TestUtil.json(updatePresidentDTO);

        // ne postoji stanar koji zadovoljava parametre: ime = Jovo, prezime = Radojcic, korisnicko ime = jovo
        this.mockMvc.perform(patch(URL_PREFIX + "/100/council_president")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(updatePresidentDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testSetUpPresidentTenantFromInvalidBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode setUpPresident kontrolera CouncilPresidentController
        * (postavljanje prvog/novog predsednika skupstine stanara na nivou zgrade sa zadatim ID-em)
        * u slucaju kada stanar koji zadovoljava parametre zadate u DTO ne zivi u zgradi sa zadatim ID-em
        * */

        UpdatePresidentDTO updatePresidentDTO = new UpdatePresidentDTO(null, "Leontina",
                "Simonovic","leontina");
        String updatePresidentDTOJson = TestUtil.json(updatePresidentDTO);

        // stanar sa podacima: ime = Leontina, prezime = Simonovic, korisnicko ime = leontina zivi u zgradi sa ID-em = 101 (a ne sa ID-em = 100)
        this.mockMvc.perform(patch(URL_PREFIX + "/100/council_president")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(updatePresidentDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testSetUpPresidentTenantIsNotCouncilMember() throws Exception {
        /*
        * Test proverava ispravnost rada metode setUpPresident kontrolera CouncilPresidentController
        * (postavljanje prvog/novog predsednika skupstine stanara na nivou zgrade sa zadatim ID-em)
        * u slucaju kada stanar koji zadovoljava parametre zadate u DTO-u nije clan skupstine stanara u zgradi sa zadatim ID-em
        * */

        UpdatePresidentDTO updatePresidentDTO = new UpdatePresidentDTO(null, "Jasmina",
                "Markovic","jasmina");
        String updatePresidentDTOJson = TestUtil.json(updatePresidentDTO);

        // stanar sa podacima: ime = Jasmina, prezime = Markovic, korisnicko ime = jasmina nije clan skupstine stanara
        this.mockMvc.perform(patch(URL_PREFIX + "/100/council_president")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(updatePresidentDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testSetUpPresidentMissingFirstName() throws Exception {
        /*
        * Test proverava ispravnost rada metode setUpPresident kontrolera CouncilPresidentController
        * (postavljanje prvog/novog predsednika skupstine stanara na nivou zgrade sa zadatim ID-em)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne sadrzi sva neophodna polja - fali polje firstName
        * */

        UpdatePresidentDTO updatePresidentDTO = new UpdatePresidentDTO();
        // nedostaje vrednost polja firstName
        updatePresidentDTO.setFirstName(null);
        updatePresidentDTO.setLastName("Garabandic");
        updatePresidentDTO.setUsername("nikola");
        updatePresidentDTO.setQuestionnaireId(null);

        String updatePresidentDTOJson = TestUtil.json(updatePresidentDTO);

        this.mockMvc.perform(patch(URL_PREFIX + "/100/council_president")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(updatePresidentDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testSetUpPresidentInappropriateTypeOfFirstName() throws Exception {
        /*
        * Test proverava ispravnost rada metode setUpPresident kontrolera CouncilPresidentController
        * (postavljanje prvog/novog predsednika skupstine stanara na nivou zgrade sa zadatim ID-em)
        * u slucaju kada je dolazni DTO objekat nevalidan: sadrzi sva neohodna polja, ali je polje firstName nevalidnog tipa
        * */

        // vrednost polja firstName je nevalidnog tipa
        String updatePresidentDTOJson = "{" +
                    "questionnaireId:null," +
                    "firstName:1" +
                    "lastName:'Garabandic'" +
                    "username:'nikola'" +
                "}";

        this.mockMvc.perform(patch(URL_PREFIX + "/100/council_president")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(updatePresidentDTOJson))
                .andExpect(status().isBadRequest());
    }
}
