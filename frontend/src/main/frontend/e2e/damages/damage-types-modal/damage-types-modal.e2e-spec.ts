import { browser } from "protractor";
// page
import { LoginPage } from "../../page-objects/login.po";
import { NavbarPage } from "../../page-objects/navbar.po";
import { DamageListPage } from "../../page-objects/damage-list.po";
import { DamageTypesModalPage } from "../page-objects/damage-types-modal.po";
import { DelegateResponsibilityToPersonPage } from "../../page-objects/delegate-responsibility-to-person.po";
import { DelegateResponsibilityToInstitutionPage } from "../../page-objects/delegate-responsibility-to-institution.po";


describe('Page for delegating responsibility for some damage type', () => {
  let loginPage: LoginPage;
  let navbarPage: NavbarPage;
  let damageListPage: DamageListPage;
  let damageTypesModalPage: DamageTypesModalPage;
  let delegateResponsibilityToPersonPage: DelegateResponsibilityToPersonPage;
  let delegateResponsibilityToInstitutionPage: DelegateResponsibilityToInstitutionPage;
  let indexFindPerson: number;
  let indexEditPerson: number;
  let indexFindInstitution: number;
  let indexEditInstitution: number;


  beforeAll(() => {
    loginPage = new LoginPage();
    navbarPage = new NavbarPage();
    damageListPage = new DamageListPage();
    damageTypesModalPage = new DamageTypesModalPage();
    delegateResponsibilityToInstitutionPage = new DelegateResponsibilityToInstitutionPage();
    delegateResponsibilityToPersonPage = new DelegateResponsibilityToPersonPage();
    indexFindPerson = -1;
    indexEditPerson = -1;
    indexFindInstitution = -1;
    indexEditInstitution = -1;

    browser.get('/');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');

    loginPage.setUsername('kaca');
    loginPage.setPassword('kaca');
    loginPage.getSignInButton().click();

    // before all of these tests we should add damage in list
    browser.wait(function () {
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/home/notifications';
      });
    }).then(function () {
      expect(navbarPage.getNavbarElement().isDisplayed()).toBe(true);
      expect(navbarPage.getDamageDropdownLink().isDisplayed()).toBe(true);
      navbarPage.getDamageDropdownLink().click();

      browser.wait(function () {
        return navbarPage.getContentOfDamageDropdownLink().isDisplayed();
      }).then(function () {
        expect(navbarPage.getDamagesInMyBuildingLink(3).isDisplayed()).toBe(true);
        navbarPage.getDamagesInMyBuildingLink(3).click();

        browser.wait(function () {
          return damageListPage.getDamageListElement().isDisplayed();
        }).then(function () {
          expect(damageListPage.getDamageTypesResponsibilityOption().isDisplayed()).toBe(true);
        });
      });
    });
  });

  afterAll(() => {
     browser.executeScript('window.localStorage.clear();');
  });




  it('should open modal dialog for review existing responsibilities in building for damage types, when ' +
    'president click on Damage types responsibilities in building link', () => {
    damageListPage.getDamageTypesResponsibilityOption().click();

    browser.wait(function() {
      return damageTypesModalPage.getDamageTypesModalElement().isDisplayed();
    }).then(function() {
      // expect to see all elements on this modal page
      expect(damageTypesModalPage.getTitle().isDisplayed()).toBe(true);
      expect(damageTypesModalPage.getTitle().getText()).toEqual('Damage type responsibilities in building' +
        ' where you are president of council');
      expect(damageTypesModalPage.getResponsibilityTable().isDisplayed()).toBe(true);
      damageTypesModalPage.getResponsibilityTableRows().count().then(value => {
        expect(value).toBeGreaterThan(0);

        for(let i:number = 0; i < value; i++) {
          // for every row in table there must be name of damage type and links in responsible person
          // and responsible institution column
          expect(damageTypesModalPage.getDamageTypeName(i).isDisplayed()).toBe(true);
          expect(damageTypesModalPage.getLinkForTenant(i).isDisplayed()).toBe(true);
          expect(damageTypesModalPage.getLinkForInstitution(i).isDisplayed()).toBe(true);
          if(indexFindPerson === -1 || indexEditPerson === -1) {
            damageTypesModalPage.getLinkForTenant(i).getText().then(textValue => {
              if(indexFindPerson === -1 && textValue === 'Find tenant')
                indexFindPerson = i;
              else if(indexEditPerson === -1 && textValue === 'Edit')
                indexEditPerson = i;
            });
          }
          if(indexFindInstitution === -1 || indexEditInstitution === -1) {
            damageTypesModalPage.getLinkForInstitution(i).getText().then(text => {
              if(indexFindInstitution === -1 && text === 'Find institution')
                indexFindInstitution = i;
              else if(indexEditInstitution === -1 && text === 'Edit')
                indexEditInstitution = i;
            });
          }
        }
      });
    });
  });



  it('should set responsible person for damage type that hasn\'t responsible person yet, ' +
    'when president click on Find tenant link and choose some tenant from modal dialog', () => {
    // if index for person has value != -1, then we use this row and click on that link to find new tenant
    // and set him/her as responsible person for damage type from row at given index
    if(indexFindPerson !== -1) {
      expect(damageTypesModalPage.getLinkForTenant(indexFindPerson).isDisplayed()).toBe(true);
      expect(damageTypesModalPage.getLinkForTenant(indexFindPerson).getText()).toEqual('Find tenant');

      damageTypesModalPage.getLinkForTenant(indexFindPerson).click();

      browser.wait(function() {
        return delegateResponsibilityToPersonPage.getDelegateResponsibilityToPersonElement().isDisplayed();
      }).then(function() {
        expect(delegateResponsibilityToPersonPage.getTitle().isDisplayed()).toBe(true);
        // expect to see more then one row and button Delegate in rows
        delegateResponsibilityToPersonPage.getTenantsTableRows().count().then(value => {
          expect(value).toBeGreaterThan(0);

          let nameOfSelectedTenant = {'firstName':'', 'lastName':''};
          delegateResponsibilityToPersonPage.getTenantFirstName(0).getText().then(firstName => {
            nameOfSelectedTenant.firstName = firstName;
          });
          delegateResponsibilityToPersonPage.getTenantLastName(0).getText().then(lastName => {
            nameOfSelectedTenant.lastName = lastName;
          });

          expect(delegateResponsibilityToPersonPage.getButtonDelegate(0).isDisplayed()).toBe(true);
          expect(delegateResponsibilityToPersonPage.getButtonDelegate(0).isEnabled()).toBe(true);
          delegateResponsibilityToPersonPage.getButtonDelegate(0).click();

          expect(damageTypesModalPage.getDamageResponsibleTenant(indexFindPerson).getText())
            .toContain(`${nameOfSelectedTenant.firstName} ${nameOfSelectedTenant.lastName}`);
        });
      });
    }
  });




  it('should change responsible person for damage type that has responsible person, ' +
    'when president click on Edit link and choose some tenant from modal dialog', () => {
    // we change tenant set in previous it section, if index find person has value != -1
    if(indexFindPerson !== -1) {
      expect(damageTypesModalPage.getLinkForTenant(indexFindPerson).isDisplayed()).toBe(true);
      expect(damageTypesModalPage.getLinkForTenant(indexFindPerson).getText()).toEqual('Edit');

      damageTypesModalPage.getLinkForTenant(indexFindPerson).click();

      browser.wait(function() {
        return delegateResponsibilityToPersonPage.getDelegateResponsibilityToPersonElement().isDisplayed();
      }).then(function() {
        expect(delegateResponsibilityToPersonPage.getTitle().isDisplayed()).toBe(true);
        // expect to see more then one row and button Delegate in rows
        delegateResponsibilityToPersonPage.getTenantsTableRows().count().then(value => {
          expect(value).toBeGreaterThan(0);

          // tenant at first position in list doesn't have button because, he is current responsible person
          expect(delegateResponsibilityToPersonPage.getCurrentResponsibleTenant(0).isDisplayed()).toBe(true);
          expect(delegateResponsibilityToPersonPage.getCurrentResponsibleTenant(0).getText()).toEqual('Current responsible');

          // store data about new responsbile person
          let nameOfSelectedTenant = {'firstName':'', 'lastName':''};
          delegateResponsibilityToPersonPage.getTenantFirstName(1).getText().then(firstName => {
            nameOfSelectedTenant.firstName = firstName;
          });
          delegateResponsibilityToPersonPage.getTenantLastName(1).getText().then(lastName => {
            nameOfSelectedTenant.lastName = lastName;
          });

          expect(delegateResponsibilityToPersonPage.getButtonDelegate(1).isDisplayed()).toBe(true);
          expect(delegateResponsibilityToPersonPage.getButtonDelegate(1).isEnabled()).toBe(true);
          delegateResponsibilityToPersonPage.getButtonDelegate(1).click();

          expect(damageTypesModalPage.getDamageResponsibleTenant(indexFindPerson).getText())
            .toContain(`${nameOfSelectedTenant.firstName} ${nameOfSelectedTenant.lastName}`);
        });
      });
    }
    else {
      // we take first damage responsibility from list and change responsible person
      expect(damageTypesModalPage.getLinkForTenant(indexEditPerson).isDisplayed()).toBe(true);
      expect(damageTypesModalPage.getLinkForTenant(indexEditPerson).getText()).toEqual('Edit');
      let currentResponsible = {'firstName':'', 'lastName':''};

      damageTypesModalPage.getDamageResponsibleTenant(indexEditPerson).getText().then(value => {
        currentResponsible.firstName = value.split(' ')[0];
        currentResponsible.lastName = value.split(' ')[1];
      });

      damageTypesModalPage.getLinkForTenant(indexEditPerson).click();

      browser.wait(function() {
        return delegateResponsibilityToPersonPage.getDelegateResponsibilityToPersonElement().isDisplayed();
      }).then(function() {
        expect(delegateResponsibilityToPersonPage.getTitle().isDisplayed()).toBe(true);
        // expect to see more then one row and button Delegate in rows
        delegateResponsibilityToPersonPage.getTenantsTableRows().count().then(value => {
          expect(value).toBeGreaterThan(0);

          // store data about new responsbile person
          let nameOfSelectedTenant = {'firstName': '', 'lastName': '', 'index': -1};

          // find first tenant with enabled button
          delegateResponsibilityToPersonPage.getTenantFirstName(0).getText().then(firstName => {
            delegateResponsibilityToPersonPage.getTenantLastName(0).getText().then(lastName => {
              if (firstName !== currentResponsible.firstName || lastName !== currentResponsible.lastName) {
                nameOfSelectedTenant.firstName = firstName;
                nameOfSelectedTenant.lastName = lastName;
                nameOfSelectedTenant.index = 0;

                expect(delegateResponsibilityToPersonPage.getButtonDelegate(
                  nameOfSelectedTenant.index).isDisplayed()).toBe(true);
                expect(delegateResponsibilityToPersonPage.getButtonDelegate(
                  nameOfSelectedTenant.index).isEnabled()).toBe(true);
                delegateResponsibilityToPersonPage.getButtonDelegate(nameOfSelectedTenant.index).click();

                expect(damageTypesModalPage.getDamageResponsibleTenant(indexEditPerson).getText())
                  .toContain(`${nameOfSelectedTenant.firstName} ${nameOfSelectedTenant.lastName}`);
              }
              else {
                delegateResponsibilityToPersonPage.getTenantFirstName(1).getText().then(nameFirst => {
                  delegateResponsibilityToPersonPage.getTenantLastName(1).getText().then(nameLast => {
                    // store data about new responsbile person
                    nameOfSelectedTenant.firstName = nameFirst;
                    nameOfSelectedTenant.lastName = nameLast;
                    nameOfSelectedTenant.index = 1;

                    expect(delegateResponsibilityToPersonPage.getButtonDelegate(
                      nameOfSelectedTenant.index).isDisplayed()).toBe(true);
                    expect(delegateResponsibilityToPersonPage.getButtonDelegate(
                      nameOfSelectedTenant.index).isEnabled()).toBe(true);
                    delegateResponsibilityToPersonPage.getButtonDelegate(nameOfSelectedTenant.index).click();

                    expect(damageTypesModalPage.getDamageResponsibleTenant(indexEditPerson).getText())
                      .toContain(`${nameOfSelectedTenant.firstName} ${nameOfSelectedTenant.lastName}`);
                  });
                });
              }
            });
          });
        });
      });
    }
  });



  it('should set responsible institution for damage type that hasn\'t responsible institution yet, ' +
    'when president click on Find institution link and choose some institution from modal dialog', () => {
    // if index for institution has value != -1, then we use this row and click on that link to find new institution
    // and set him/her as responsible person for damage type from row at given index
    if(indexFindInstitution !== -1) {
      expect(damageTypesModalPage.getLinkForInstitution(indexFindInstitution).isDisplayed()).toBe(true);
      expect(damageTypesModalPage.getLinkForInstitution(indexFindInstitution).getText()).toEqual('Find institution');

      damageTypesModalPage.getLinkForInstitution(indexFindInstitution).click();

      browser.wait(function() {
        return delegateResponsibilityToInstitutionPage.getDelegateResponsibilityToInstitutionElement().isDisplayed();
      }).then(function() {
        expect(delegateResponsibilityToInstitutionPage.getTitle().isDisplayed()).toBe(true);
        // expect to see more then one row and button Delegate in rows
        delegateResponsibilityToInstitutionPage.getInstitutionsTableRows().count().then(value => {
          expect(value).toBeGreaterThan(0);

          let selectedInstitution = {'name':''};
          delegateResponsibilityToInstitutionPage.getInstitutionName(0).getText().then(name => {
            selectedInstitution.name = name;
          });

          expect(delegateResponsibilityToInstitutionPage.getButtonDelegate(0).isDisplayed()).toBe(true);
          expect(delegateResponsibilityToInstitutionPage.getButtonDelegate(0).isEnabled()).toBe(true);
          delegateResponsibilityToInstitutionPage.getButtonDelegate(0).click();

          expect(damageTypesModalPage.getDamageResponsibleInstitution(indexFindInstitution).getText())
            .toContain(`${selectedInstitution.name}`);
        });
      });
    }
  });




  it('should change responsible institution for damage type that has responsible institution, ' +
    'when president click on Edit link and choose some institution from modal dialog', () => {
    // we change institution set in previous it section, if index find institution has value != -1
    if(indexFindInstitution !== -1) {
      expect(damageTypesModalPage.getLinkForInstitution(indexFindInstitution).isDisplayed()).toBe(true);
      expect(damageTypesModalPage.getLinkForInstitution(indexFindInstitution).getText()).toEqual('Edit');

      damageTypesModalPage.getLinkForInstitution(indexFindInstitution).click();

      browser.wait(function() {
        return delegateResponsibilityToInstitutionPage.getDelegateResponsibilityToInstitutionElement().isDisplayed();
      }).then(function() {
        expect(delegateResponsibilityToInstitutionPage.getTitle().isDisplayed()).toBe(true);
        // expect to see more then one row and button Delegate in rows
        delegateResponsibilityToInstitutionPage.getInstitutionsTableRows().count().then(value => {
          expect(value).toBeGreaterThan(0);

          // institution at first position in list doesn't have button because, he it's current responsible institution
          expect(delegateResponsibilityToInstitutionPage.getCurrentResponsibleInstitution(0).isDisplayed())
            .toBe(true);
          expect(delegateResponsibilityToInstitutionPage.getCurrentResponsibleInstitution(0).getText())
            .toEqual('Current responsible');

          // store data about new responsible institution
          let selectedInstitution = {'name':''};
          delegateResponsibilityToInstitutionPage.getInstitutionName(1).getText().then(name => {
            selectedInstitution.name = name;
          });

          expect(delegateResponsibilityToInstitutionPage.getButtonDelegate(1).isDisplayed()).toBe(true);
          expect(delegateResponsibilityToInstitutionPage.getButtonDelegate(1).isEnabled()).toBe(true);
          delegateResponsibilityToInstitutionPage.getButtonDelegate(1).click();

          expect(damageTypesModalPage.getDamageResponsibleInstitution(indexFindInstitution).getText())
            .toContain(`${selectedInstitution.name}`);
        });
      });
    }
    else {
      // we take first damage responsibility from list and change responsible institution
      expect(damageTypesModalPage.getLinkForInstitution(indexEditInstitution).isDisplayed()).toBe(true);
      expect(damageTypesModalPage.getLinkForInstitution(indexEditInstitution).getText()).toEqual('Edit');
      let currentResponsible = {'name':''};

      damageTypesModalPage.getDamageResponsibleInstitution(indexEditInstitution).getText().then(value => {
        currentResponsible.name = value.substr(0, value.length - 5);
      });

      damageTypesModalPage.getLinkForInstitution(indexEditInstitution).click();

      browser.wait(function() {
        return delegateResponsibilityToInstitutionPage.getDelegateResponsibilityToInstitutionElement().isDisplayed();
      }).then(function() {
        expect(delegateResponsibilityToInstitutionPage.getTitle().isDisplayed()).toBe(true);
        // expect to see more then one row and button Delegate in rows
        delegateResponsibilityToInstitutionPage.getInstitutionsTableRows().count().then(value => {
          expect(value).toBeGreaterThan(0);

          // store data about new responsible institution
          let nameOfSelectedInstitution = {'name':'', 'index': -1};

          // find first institution with enabled button
          delegateResponsibilityToInstitutionPage.getInstitutionName(0).getText().then(name => {
            if (name !== currentResponsible.name) {
              nameOfSelectedInstitution.name = name;
              nameOfSelectedInstitution.index = 0;

              expect(delegateResponsibilityToInstitutionPage.getButtonDelegate(
                nameOfSelectedInstitution.index).isDisplayed()).toBe(true);
              expect(delegateResponsibilityToInstitutionPage.getButtonDelegate(
                nameOfSelectedInstitution.index).isEnabled()).toBe(true);
              delegateResponsibilityToInstitutionPage.getButtonDelegate(nameOfSelectedInstitution.index).click();

              expect(damageTypesModalPage.getDamageResponsibleInstitution(indexEditInstitution).getText())
                .toContain(`${nameOfSelectedInstitution.name}`);
            }
            else {
              delegateResponsibilityToInstitutionPage.getInstitutionName(1).getText().then(nameInstitution => {
                  nameOfSelectedInstitution.name = nameInstitution;
                  nameOfSelectedInstitution.index = 1;

                expect(delegateResponsibilityToInstitutionPage.getButtonDelegate(
                  nameOfSelectedInstitution.index).isDisplayed()).toBe(true);
                expect(delegateResponsibilityToInstitutionPage.getButtonDelegate(
                  nameOfSelectedInstitution.index).isEnabled()).toBe(true);
                delegateResponsibilityToInstitutionPage.getButtonDelegate(nameOfSelectedInstitution.index).click();

                expect(damageTypesModalPage.getDamageResponsibleInstitution(indexEditInstitution).getText())
                  .toContain(`${nameOfSelectedInstitution.name}`);
              });
            }
          });
        });
      });
    }
  });
});
