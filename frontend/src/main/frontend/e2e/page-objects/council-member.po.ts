import { by, element, ElementFinder } from "protractor";

export class CouncilMemberPage {
  //get methods
  getCouncilMemberElement(): ElementFinder {
    return element(by.tagName('app-council-member'));
  }

  getCloseButtonFromHeader(): ElementFinder {
    return element(by.css('.modal-header > button'));
  }
}
