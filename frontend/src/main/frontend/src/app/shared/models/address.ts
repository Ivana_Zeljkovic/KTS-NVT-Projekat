import { City } from "./city";

export interface Address {
  street: string;
  number: number;
  city: City;
}
