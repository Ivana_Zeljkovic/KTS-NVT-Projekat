import { Component, OnInit } from '@angular/core';
// service
import { JwtService } from "../../core/services/jwt.service";
import { DamagesCategoryService } from "../../core/services/damages-category.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  constructor(private jwtService : JwtService, private damagesCategory: DamagesCategoryService) { }

  ngOnInit() {
    if(this.jwtService.hasRole('COMPANY') || this.jwtService.hasRole('INSTITUTION')) {
      this.damagesCategory.changeCategory('');
    }
  }
}
