package rs.ac.uns.ftn.ktsnvt.web.controller;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.ForbiddenException;
import rs.ac.uns.ftn.ktsnvt.model.entity.*;
import rs.ac.uns.ftn.ktsnvt.service.*;
import rs.ac.uns.ftn.ktsnvt.web.controller.util.MessageConstants;
import rs.ac.uns.ftn.ktsnvt.web.dto.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

import static rs.ac.uns.ftn.ktsnvt.web.util.ConverterDTOToModel.convertTenantCreateDTOToTenant;


/**
 * Created by Ivana Zeljkovic on 06/11/2017.
 */
@RestController
@RequestMapping(value = "/api")
@Api(value="tenants", description="Operations pertaining to tenants in application.")
public class    TenantController {

    @Autowired
    private TenantService tenantService;

    @Autowired
    private ApartmentService apartmentService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private AuthorityService authorityService;

    @Autowired
    private AccountAuthorityService accountAuthorityService;

    @Autowired
    private BuildingService buildingService;



    @RequestMapping(
            value = "/tenants",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a list of all tenants in page form.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of tenants", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    })
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('TENANT')")
    public ResponseEntity<List<TenantDTO>> getAllTenants(
            @ApiParam(value = "The page number for getting tenants.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting tenants.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting tenants.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "First sort property for getting tenants.", required = true) @RequestParam String sortProperty) {

        Page<Tenant> tenants = null;
        boolean isTenant = false;
        boolean tenantNotPresident = true;

        for(GrantedAuthority authority : SecurityContextHolder.getContext().getAuthentication().getAuthorities()) {
            if(authority.getAuthority().equals(MessageConstants.ADMIN_ROLE))
                tenants = this.tenantService.findAll(Integer.valueOf(pageNumber), Integer.valueOf(pageSize),
                        sortDirection, sortProperty);
            else if(authority.getAuthority().equals(MessageConstants.TENANT_ROLE))
                isTenant = true;
            else if(authority.getAuthority().equals(MessageConstants.PRESIDENT_ROLE))
                tenantNotPresident = false;
        }

        if(isTenant) {
            Tenant currentTenant = this.accountService.findByUsername(
                    SecurityContextHolder.getContext().getAuthentication().getName()).getTenant();
            if(tenantNotPresident) {
                // ucitavaju se svi stanari osim trenutnog stanara koji je ulogovan
                tenants = this.tenantService.findAllByBuildingIdExceptCurrent(
                        currentTenant.getApartmentLiveIn().getBuilding().getId(), currentTenant.getId(),
                        Integer.valueOf(pageNumber), Integer.valueOf(pageSize), sortDirection, sortProperty);
            }
            else {
                // ucitavaju se svi stanari u zgradi, ukljucujuci i trenutno logovanog (predsednika skupstine te zgrade)
                tenants = this.tenantService.findAllByBuildingId(currentTenant.getApartmentLiveIn().getBuilding().getId(),
                        Integer.valueOf(pageNumber), Integer.valueOf(pageSize), sortDirection, sortProperty);
            }
        }

        List<TenantDTO> tenantsDTO = new ArrayList<>();
        if(tenants != null) {
            for (Tenant tenant : tenants.getContent()) {
                tenantsDTO.add(new TenantDTO(tenant, tenants.getTotalElements()));
            }
        }

        return new ResponseEntity<>(tenantsDTO, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/tenants/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a single tenant.",
            notes = "You have to provide a valid tenant ID in the URL.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved tenant", response = TenantDTO.class),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('COUNCIL_PRESIDENT')")
    public ResponseEntity getTenant(
            @ApiParam(value = "The ID of the existing tenant resource.", required = true) @PathVariable("id") Long id) {

        Tenant tenant = this.tenantService.findOne(id);

        for(GrantedAuthority authority : SecurityContextHolder.getContext().getAuthentication().getAuthorities()) {
            if(authority.getAuthority().equals(MessageConstants.PRESIDENT_ROLE)) {
                // predsednik SS moze da dobavi iskljucivo stanare iz zgrade u kojoj je on PSS
                Tenant currentPresident = this.accountService.findByUsername(
                        SecurityContextHolder.getContext().getAuthentication().getName()).getTenant();
                if(!tenant.getApartmentLiveIn().getBuilding().getId().equals(
                        currentPresident.getApartmentLiveIn().getBuilding().getId()))
                    throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);
            }
        }

        return new ResponseEntity<>(new TenantDTO(tenant), HttpStatus.OK);
    }



    @RequestMapping(
            value = "/tenants",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Create a tenant resource.",
            notes = "Returns the tenant being saved.",
            httpMethod = "POST",
            produces = "application/json",
            consumes = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully created tenant", response = TenantDTO.class),
            @ApiResponse(code = 400, message = "Inappropriate tenant object sent in request body"),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 403, message = "You don't have permission to create resource"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    public ResponseEntity registerTenant(
            @ApiParam(value = "The tenant object", required = true) @Valid @RequestBody TenantCreateDTO tenantCreateDTO,
            @ApiParam(value = "The object that contains all errors from validation of DTO object") BindingResult errors) {

        Boolean canRegister = false;
        Boolean isAdmin = false;
        for(GrantedAuthority authority : SecurityContextHolder.getContext().getAuthentication().getAuthorities())
        {
            if(authority.getAuthority().equals(MessageConstants.ADMIN_ROLE))
                isAdmin = true;
            if(authority.getAuthority().equals("ROLE_ANONYMOUS"))
                canRegister = true;
        }

        if(!canRegister && !isAdmin) throw new ForbiddenException("You need to logout to register or you need to be admin.");

        this.accountService.checkUsername(tenantCreateDTO.getLoginAccount().getUsername());

        Account account = new Account(tenantCreateDTO.getLoginAccount().getUsername(), tenantCreateDTO.getLoginAccount().getPassword());

        //Mapiranje istoimenih atributa iz DTO objekta na objekat koji se snima u bazu
        Tenant tenant = convertTenantCreateDTOToTenant(tenantCreateDTO);
        if(isAdmin)
            tenant.setConfirmed(true);
        else
            tenant.setConfirmed(false);


        Authority authority = this.authorityService.findByName("TENANT");

        AccountAuthority accountAuthority = new AccountAuthority(account, authority);
        account.getAccountAuthorities().add(accountAuthority);
        account = this.accountService.save(account);

        tenant.setAccount(account);
        tenant = this.tenantService.save(tenant);
        account.setTenant(tenant);
        account = this.accountService.save(account);

        accountAuthority.setAccount(account);
        accountAuthority.setAuthority(authority);
        this.accountAuthorityService.save(accountAuthority);

        return new ResponseEntity<>(new TenantDTO(tenant), HttpStatus.CREATED);
    }



    @RequestMapping(
            value = "/tenants/{id}",
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Update a tenant resource.",
            notes = "You have to provide a valid tenant ID in the URL. " +
                    "Method returns the tenant being updated.",
            httpMethod = "PUT",
            produces = "application/json",
            consumes = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated tenant", response = TenantDTO.class),
            @ApiResponse(code = 400, message = "Inappropriate tenant object sent in request body"),
            @ApiResponse(code = 401, message = "You are not authorized to update the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity updateTenant(
            @ApiParam(value = "The ID of the existing tenant resource.", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "The tenant object.", required = true) @Valid @RequestBody TenantUpdateDTO tenantUpdateDTO,
            @ApiParam(value = "The object that contains all errors from validation of DTO object") BindingResult errors) {

        Tenant tenant = this.tenantService.findOne(id);
        this.tenantService.checkPermissionForCurrentTenant(tenant.getId());

        tenant.setFirstName(tenantUpdateDTO.getFirstName());
        tenant.setLastName(tenantUpdateDTO.getLastName());
        tenant.setBirthDate(tenantUpdateDTO.getBirthDate());
        tenant.setEmail(tenantUpdateDTO.getEmail());
        tenant.setPhoneNumber(tenantUpdateDTO.getPhoneNumber());

        this.tenantService.checkPasswordUpdate(tenant, tenantUpdateDTO.getCurrentPassword(),
                tenantUpdateDTO.getNewPassword());

        tenant = this.tenantService.save(tenant);
        return new ResponseEntity<>(new TenantDTO(tenant), HttpStatus.OK);
    }



    @RequestMapping(
            value = "/tenants/{id}",
            method = RequestMethod.DELETE
    )
    @ApiOperation(
            value = "Delete a tenant resource.",
            notes = "You have to provide a valid tenant ID in the URL. " +
                    "Once deleted the resource can not be recovered.",
            httpMethod = "DELETE"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully deleted tenant"),
            @ApiResponse(code = 401, message = "You are not authorized to delete the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity deleteTenant(
            @ApiParam(value = "The ID of the existing tenant resource.", required = true) @PathVariable("id") Long id) {

        Tenant tenant = this.tenantService.findOne(id);

        for(GrantedAuthority authority : SecurityContextHolder.getContext().getAuthentication().getAuthorities()) {
            if(authority.getAuthority().equals(MessageConstants.PRESIDENT_ROLE)) {
                // predsednik SS moze da obrise iskljucivo stanare iz zgrade u kojoj je on PSS
                Account account = this.accountService.findByUsername(
                        SecurityContextHolder.getContext().getAuthentication().getName());
                Tenant currentPresident = account.getTenant();
                if(!currentPresident.getApartmentLiveIn().getBuilding().getId().equals(
                        tenant.getApartmentLiveIn().getBuilding().getId()))
                    throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);
            }
        }
        // nije dovoljeno brisanje predsednika skupstine stanara
        if(tenant.getApartmentLiveIn() != null &&
            tenant.getApartmentLiveIn().getBuilding().getCouncil().getPresident() != null &&
                tenant.getApartmentLiveIn().getBuilding().getCouncil().getPresident().getId().equals(tenant.getId()))
                throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);

        // nije dovoljeno brisanje stanara ukoliko je on odgovoran za neki kvar koji nije resen/vrstu kvara
        if(tenant.sizeOfResponsibleForNotFixedDamages() > 0 || !tenant.getResponsibleForDamageTypes().isEmpty())
            throw new BadRequestException("You can't delete selected tenant!");

        // treba ukloniti nalog za prijavljivanje i odgovarajucu rolu/role stanara kojeg brisemo
        for(AccountAuthority accountAuthority : tenant.getAccount().getAccountAuthorities()) {
            this.accountAuthorityService.remove(accountAuthority.getId());
        }
        Account account = tenant.getAccount();
        account.setTenant(null);
        account.setAccountAuthorities(null);
        tenant.setAccount(null);
        account = this.accountService.save(account);
        tenant = this.tenantService.save(tenant);

        this.accountService.remove(account.getId());
        this.tenantService.remove(tenant.getId());

        return new ResponseEntity(HttpStatus.OK);
    }



    @RequestMapping(
            value = "/tenants/buildings/{buildingId}/apartments/{apartmentId}/council_member",
            method = RequestMethod.PATCH,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Set up tenant as member of the building's council.",
            notes = "You have to provide valid IDs for building and apartment in the URL. " +
                    "Method returns updated tenant object.",
            httpMethod = "PATCH",
            produces = "application/json",
            consumes = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully set up tenant as council's member"),
            @ApiResponse(code = 400, message = "Invalid IDs sent in URL"),
            @ApiResponse(code = 401, message = "You are not authorized to make this changes"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity setTenantAsCouncilMember(
            @ApiParam(value = "The ID of the existing apartment resource.", required = true) @PathVariable("apartmentId") Long apartmentId,
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The tenant object with ID attribute.", required = true) @Valid @RequestBody TenantMemberCouncilDTO tenantMemberCouncilDTO,
            @ApiParam(value = "The object that contains all errors from validation of DTO object") BindingResult errors) {

        Building building = this.buildingService.findOne(buildingId);
        Tenant tenant = this.tenantService.findOne(tenantMemberCouncilDTO.getTenantId());
        Apartment apartment = this.apartmentService.findOne(apartmentId);

        this.tenantService.checkPermissionForCurrentTenantInBuilding(building.getId());
        this.apartmentService.checkBuilding(apartment, building.getId());

        Tenant currentTenant = this.accountService.findByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()).getTenant();

        if(!currentTenant.getApartmentLiveIn().getId().equals(apartment.getId()) || currentTenant.getCouncil() == null)
            throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);

        if(currentTenant.getId().equals(building.getCouncil().getPresident().getId()))
            throw new BadRequestException("You can't choose someone of your roommates to be member of the council" +
                    " for your apartment, because you are the president of the council!");

        if (!tenant.getApartmentLiveIn().getId().equals(apartment.getId()))
            throw new BadRequestException("Selected tenant doesn't live in selected apartment!");

        // trenutno ulogovanom stanaru uklanjamo clanstvo u skupstini stanara u zadatoj zgradi
        // a novoizabranom stanaru dodajemo clanstvo
        currentTenant.setCouncil(null);
        tenant.setCouncil(building.getCouncil());
        building.getCouncil().getMembers().remove(currentTenant);
        building.getCouncil().getMembers().add(tenant);

        this.tenantService.save(tenant);
        this.tenantService.save(currentTenant);

        return new ResponseEntity<>(HttpStatus.OK);
    }



    @RequestMapping(
            value = "/tenants/{id}/confirm",
            method = RequestMethod.PATCH,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Confirmation of tenant's registration.",
            notes = "You have to provide valid tenant ID in the URL.",
            httpMethod = "PATCH",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully confirm tenant's registration", response = TenantDTO.class),
            @ApiResponse(code = 400, message = "Registration of tenant is already confirmed"),
            @ApiResponse(code = 401, message = "You are not authorized to update the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity confirmTenant(
            @ApiParam(value = "The ID of the existing tenant resource.", required = true) @PathVariable Long id) {

        Tenant tenant = this.tenantService.findOne(id);

        if (tenant.getConfirmed()) throw new BadRequestException("Registration of selected tenant is already confirmed!");

        tenant.setConfirmed(true);
        tenant = this.tenantService.save(tenant);

        return new ResponseEntity<>(new TenantDTO(tenant), HttpStatus.OK);
    }



    @RequestMapping(
            value = "/tenants/tenants_in_building",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a list of all tenants inside of current tenant's building with given name and username, " +
                    "in page form.",
            notes = "You have to provide next parameters: apartment ID, name, last name and username. ",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of tenants", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to view these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity getTenantsInMyBuilding(
            @ApiParam(value = "The ID of the existing apartment resource.", required = true) @RequestParam("apartmentId") Long apartmentId,
            @ApiParam(value = "Users name") @RequestParam("name") String name,
            @ApiParam(value = "Users last name") @RequestParam("lastName") String lastName,
            @ApiParam(value = "Users username") @RequestParam("username") String username) {

        if(apartmentId == null)
            throw new BadRequestException("Apartment ID is missing!");

        if ((name == null || name.equals("")) && (lastName == null ||
                lastName.equals("")) && (username == null || username.equals("")))
            throw new BadRequestException("At least one field needs to be not empty!");

        Apartment apartment = this.apartmentService.findOne(apartmentId);

        // stanar moze da pretrazuje druge stanare iskljucivo u okviru svoje zgrade
        this.tenantService.checkPermissionForCurrentTenantInBuilding(apartment.getBuilding().getId());

        List<Tenant> tenants = this.tenantService.findTenantsByBuildingAndNameAndUsername(apartment.getBuilding().getId(),
                !(name == null || name.equals("")) ? "%" + name + "%" : "%",
                !(lastName == null || lastName.equals("")) ? "%" + lastName + "%" : "%",
                !(username == null || username.equals("")) ? "%" + username + "%" : "%");

        ArrayList<TenantDTO> result = new ArrayList<>();
        tenants.forEach(tenant -> result.add(new TenantDTO(tenant)));

        return new ResponseEntity<>(result, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/tenants/tenants_by_username",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a list of tenants with given name or username, in page form.",
            notes = "You have to provide next parameters: name last name and/or username.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of tenants", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to view these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden") })
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity getTenantsByNameOrUsername(
            @ApiParam(value = "User's name") @RequestParam("name") String name,
            @ApiParam(value = "User's last name") @RequestParam("lastName") String lastName,
            @ApiParam(value = "User's username") @RequestParam("username") String username) {

        List<Tenant> tenants = null;

        if ((name == null || name.equals("")) && (lastName == null ||
                lastName.equals("")) && (username == null || username.equals("")))
            throw new BadRequestException("At least one field needs to be not empty!");

        tenants = this.tenantService.findByNameOrUsername(
                !(name == null || name.equals("")) ? "%" + name + "%" : "%",
                !(lastName == null || lastName.equals("")) ? "%" + lastName + "%" : "%",
                !(username == null || username.equals("")) ? "%" + username + "%" : "%");

        List<TenantAdminDTO> result = new ArrayList<>();
        tenants.forEach(tenant -> result.add(new TenantAdminDTO(tenant)));

        return new ResponseEntity<>(result, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/tenants/tenants_by_building_id",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a list of all tenants who live in selected building, in page form.",
            notes = "You have to provide valid building ID in the URL.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of tenants", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to view these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('COMPANY')")
    public ResponseEntity getTenantsByBuildingId(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @RequestParam Long buildingId,
            @ApiParam(value = "The page number for getting tenants.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting tenants.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting tenants.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "First sort property for getting tenants.", required = true) @RequestParam String sortProperty) {

        if(buildingId == null)
            throw new BadRequestException("Building ID is missing!");

        Page<Tenant> tenants = this.tenantService.findAllByBuildingId(buildingId, Integer.valueOf(pageNumber),
                Integer.valueOf(pageSize), sortDirection, sortProperty);

        ArrayList<TenantDTO> result = new ArrayList<>();
        tenants.getContent().forEach(tenant -> result.add(new TenantDTO(tenant, tenants.getTotalElements())));

        return new ResponseEntity<>(result, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/council/members",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a list of all tenants who are current or potential members of building's council, in page form.",
            notes = "You have to provide valid building ID in the URL.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of tenants", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to view these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity getCouncilMembers(
            @ApiParam(value = "The building ID", required = true) @PathVariable Long buildingId,
            @ApiParam(value = "The page number for getting council members.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting council members.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting council members.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "First sort property for getting council members.", required = true) @RequestParam String sortProperty) {

        Building building = this.buildingService.findOne(buildingId);

        Tenant currentTenant = this.accountService.findByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()).getTenant();

        boolean isPresident = false;
        for(GrantedAuthority authority: SecurityContextHolder.getContext().getAuthentication().getAuthorities()) {
            if(authority.getAuthority().equals(MessageConstants.PRESIDENT_ROLE))
                isPresident = true;
        }

        List<TenantDTO> tenantDTOS = new ArrayList<>();
        if(isPresident) {
            this.tenantService.checkPermissionForCurrentPresidentInBuilding(building);
            Page<Tenant> councilMembers = this.tenantService.findAllCouncilMembers(building.getId(),
                    currentTenant.getId(), Integer.valueOf(pageNumber), Integer.valueOf(pageSize),
                    sortDirection, sortProperty);

            councilMembers.forEach(councilMember -> tenantDTOS.add(
                    new TenantDTO(councilMember, councilMembers.getTotalElements())));
        }
        else {
            this.tenantService.checkPermissionForCurrentTenantInBuilding(building.getId());
            if(currentTenant.getCouncil() == null)
                throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);
            Tenant president = building.getCouncil().getPresident();
            Page<Tenant> roommates = null;

            // ako u stanu zivi i predsednik, tada se mora vrsiti pretraga koja ne ukljucuje i njega, jer je on svakako
            // vec clan
            if(president.getApartmentLiveIn().getId().equals(currentTenant.getApartmentLiveIn().getId())) {
                roommates = this.tenantService.findAllInApartmentExceptPresident(
                        currentTenant.getApartmentLiveIn().getId(), currentTenant.getId(),
                        president.getId(), Integer.valueOf(pageNumber), Integer.valueOf(pageSize),
                        sortDirection, sortProperty);
            }
            else {
                roommates = this.tenantService.findAllInApartment(currentTenant.getApartmentLiveIn().getId(),
                        currentTenant.getId(), Integer.valueOf(pageNumber), Integer.valueOf(pageSize),
                        sortDirection, sortProperty);
            }

            for(Tenant roommate : roommates) {
                tenantDTOS.add(new TenantDTO(roommate, roommates.getTotalElements()));
            }
        }

        return new ResponseEntity(tenantDTOS, HttpStatus.OK);
    }
}