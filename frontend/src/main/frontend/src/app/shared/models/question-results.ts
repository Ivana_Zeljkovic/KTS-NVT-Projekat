import { AnswerResults } from "./answer-results";

export interface QuestionResults {
  questionId: number;
  content: string;
  answers: Array<AnswerResults>;
}
