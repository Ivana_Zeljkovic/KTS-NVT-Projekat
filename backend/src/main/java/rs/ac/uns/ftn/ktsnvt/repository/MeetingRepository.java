package rs.ac.uns.ftn.ktsnvt.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import rs.ac.uns.ftn.ktsnvt.model.entity.Meeting;

import java.util.Date;


/**
 * Created by Nikola Garabandic on 22/11/2017.
 */
public interface MeetingRepository extends JpaRepository<Meeting, Long> {

    @Query(value = "SELECT m FROM Meeting m WHERE m.date > :currentDate AND m.council.id = " +
            "(SELECT b.council.id FROM Building b WHERE b.id = :buildingId)")
    Page<Meeting> findAllByBuildingId(@Param("buildingId") Long buildingId, @Param("currentDate") Date currentDate,
                                      Pageable pageable);

    @Query(value = "SELECT m FROM Meeting m WHERE m.date < :currentDate AND m.council.id = " +
            "(SELECT b.council.id FROM Building b WHERE b.id = :buildingId) AND m.record IS NULL")
    Page<Meeting> findAllFinishedByBuildingId(@Param("buildingId") Long buildingId, @Param("currentDate") Date currentDate,
                                              Pageable pageable);

    @Query(value = "SELECT m FROM Meeting m WHERE m.date = " +
            "(SELECT min(me.date) FROM Meeting me WHERE me.council.id = :councilId AND me.date > :currentDate)")
    Meeting findFirstNextMeetingInBuilding(@Param("councilId") Long councilId, @Param("currentDate") Date currentDate);
}


