import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from "@angular/common/http/testing";
// service
import { MeetingItemService } from './meeting-item.service';
// model
import { PageAndSort } from "../../shared/models/page-and-sort";
import { MeetingItem } from "../../shared/models/meeting-item";


describe('MeetingItemService', () => {
  let meetingItemService: MeetingItemService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [ MeetingItemService ]
    });

    meetingItemService = TestBed.get(MeetingItemService);
    httpMock = TestBed.get(HttpTestingController);
  });

  // make sure that there are no outstanding requests
  afterEach(() => {
    httpMock.verify();
  });



  it('should be created', inject([MeetingItemService], (service: MeetingItemService) => {
    expect(service).toBeTruthy();
  }));


  it('should retrieve all meeting items that are created in given date range', () => {
    const responseList = [
      {
        'id': 1,
        'title': 'First title',
        'content': 'First content',
        'date': new Date(),
        'creator': {
          'id': 1,
          'firstName': 'Ivana',
          'lastName': 'Zeljkovic',
          'birthDate': new Date(),
          'email': 'ivana@gmail.com',
          'phoneNumber': '1234567',
          'username': 'ivana',
          'confirmed': true,
          'address': {
            'street': 'Janka Veselinovica',
            'number': 8,
            'city': {
              'name': 'Novi Sad',
              'postalNumber': '21000'
            }
          }
        }
      },
      {
        'id': 2,
        'title': 'Second title',
        'content': 'Second content',
        'date': new Date(),
        'creator': {
          'id': 1,
          'firstName': 'Ivana',
          'lastName': 'Zeljkovic',
          'birthDate': new Date(),
          'email': 'ivana@gmail.com',
          'phoneNumber': '1234567',
          'username': 'ivana',
          'confirmed': true,
          'address': {
            'street': 'Janka Veselinovica',
            'number': 8,
            'city': {
              'name': 'Novi Sad',
              'postalNumber': '21000'
            }
          }
        }
      }
    ];

    let page = new PageAndSort('0', '2', 'ASC', ['date']);
    meetingItemService.searchFromTo(100, '2018-01-01 10:00:00', '2018-01-10 10:00:00', page)
      .subscribe((meetingItems: Array<MeetingItem>) => {
        expect(meetingItems).toEqual(responseList);
        expect(meetingItems.length).toEqual(2);
      });
    const httpRequest = httpMock.expectOne(
      '/api/buildings/100/billboard/meeting_items_from_to?' +
      'from=2018-01-01%2010:00:00&' +
      'to=2018-01-10%2010:00:00&' +
      'pageNumber=0&' +
      'pageSize=2&' +
      'sortDirection=ASC&' +
      'sortProperty=date');
    expect(httpRequest.request.method).toEqual('GET');
    expect(httpRequest.request.params.get('from')).toEqual('2018-01-01 10:00:00');
    expect(httpRequest.request.params.get('to')).toEqual('2018-01-10 10:00:00');
    expect(httpRequest.request.params.get('pageNumber')).toEqual('0');
    expect(httpRequest.request.params.get('pageSize')).toEqual('2');
    expect(httpRequest.request.params.get('sortDirection')).toEqual('ASC');
    expect(httpRequest.request.params.get('sortProperty')).toEqual('date');
    httpRequest.flush(responseList);
  });
});
