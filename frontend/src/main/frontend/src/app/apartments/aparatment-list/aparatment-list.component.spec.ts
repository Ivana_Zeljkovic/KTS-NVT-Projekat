import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AparatmentListComponent } from './aparatment-list.component';

describe('AparatmentListComponent', () => {
  let component: AparatmentListComponent;
  let fixture: ComponentFixture<AparatmentListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AparatmentListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AparatmentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
