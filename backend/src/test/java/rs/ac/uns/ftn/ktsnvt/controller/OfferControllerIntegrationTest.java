package rs.ac.uns.ftn.ktsnvt.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import rs.ac.uns.ftn.ktsnvt.controller.util.TestUtil;
import rs.ac.uns.ftn.ktsnvt.web.dto.LoginDTO;
import rs.ac.uns.ftn.ktsnvt.web.dto.OfferCreateDTO;
import rs.ac.uns.ftn.ktsnvt.web.dto.TokenDTO;

import javax.annotation.PostConstruct;
import java.nio.charset.Charset;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Created by Katarina Cukurov on 06/12/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class OfferControllerIntegrationTest {

    private static final String URL_PREFIX = "/api";

    private MediaType contentType = new MediaType(
        MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    private MockMvc mockMvc;

    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja kompanije
    private String accessTokenCompany;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja institucije
    private String accessTokenInst;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja stanara
    private String accessTokenTenant;


    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private TestRestTemplate testRestTemplate;


    @PostConstruct
    public void setup() {
        this.mockMvc = MockMvcBuilders.
                                webAppContextSetup(webApplicationContext).apply(springSecurity()).build();
    }

    // pre izvrsavanja testa, neophodno je izvrsiti logovanje u aplikaciju da bismo dobili token
    @Before
    public void login() throws Exception {
        // logovanje kompanije
        LoginDTO loginDTO = new LoginDTO("firm2", "firm2");

        ResponseEntity<TokenDTO> responseEntity =
            testRestTemplate.postForEntity("/api/login",
                loginDTO,
                TokenDTO.class);

        this.accessTokenCompany = responseEntity.getBody().getValue();

        // logovanje institucije
        LoginDTO loginDTOInst = new LoginDTO("inst2", "inst2");

        ResponseEntity<TokenDTO> responseEntitInst =
            testRestTemplate.postForEntity("/api/login",
                loginDTOInst,
                TokenDTO.class);
        // preuzmemo token za logovanog stanara iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenInst = responseEntitInst.getBody().getValue();

        // logovanje stanara
        LoginDTO loginDTOTenant = new LoginDTO("jasmina", "jasmina");

        ResponseEntity<TokenDTO> responseEntityTenant =
            testRestTemplate.postForEntity("/api/login",
                loginDTOTenant,
                TokenDTO.class);
        // preuzmemo token za logovanog stanara iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenTenant = responseEntityTenant.getBody().getValue();
      }

    // TESTOVI za sendOfferDamage
    @Test
    @Transactional
    @Rollback(true)
    public void testSendOfferDamage() throws Exception{
        /*
        * Test proverava ispravnost rada metode sendOfferDamage kontrolera OfferController
        * (slanje ponude za postojeci kvar sa zadatim ID-em)
        * */

        OfferCreateDTO offerCreateDTO = new OfferCreateDTO(1000);
        String token = TestUtil.json(offerCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/businesses/101/damages/102/send_offer")
                .header("Authentication-Token", accessTokenCompany)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isCreated());
    }

    @Test
    public void testSendOfferDamageByNotAuthenticatedUser() throws Exception{
        /*
        * Test proverava ispravnost rada metode sendOfferDamage kontrolera OfferController
        * (slanje ponude za postojeci kvar sa zadatim ID-em)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        OfferCreateDTO offerCreateDTO = new OfferCreateDTO(1000);
        String token = TestUtil.json(offerCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/businesses/101/damages/102/send_offer")
                .contentType(contentType)
                .content(token))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSendOfferDamageByNotAuthenticatedTenant() throws Exception{
        /*
        * Test proverava ispravnost rada metode sendOfferDamage kontrolera OfferController
        * (slanje ponude za postojeci kvar sa zadatim ID-em)
        * u slucaju kada akciju izvrsava logovani korisni koji nema nadleznost za nju
        * */

        OfferCreateDTO offerCreateDTO = new OfferCreateDTO(1000);
        String token = TestUtil.json(offerCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/businesses/101/damages/102/send_offer")
                .header("Authentication-Token", accessTokenTenant)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSendOfferDamageNonexistentFirmOrInstitution() throws Exception{
        /*
        * Test proverava ispravnost rada metode sendOfferDamage kontrolera OfferController
        * (slanje ponude za postojeci kvar sa zadatim ID-em)
        * u slucaju kada ne postoji firma/institucija sa zadatim ID-em
        * */

        String token = "{\"price\":1000.0}";
        this.mockMvc.perform(post(URL_PREFIX + "/businesses/501/damages/102/send_offer")
                .header("Authentication-Token", accessTokenTenant)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSendOfferDamageNonexistentDamage() throws Exception{
        /*
        * Test proverava ispravnost rada metode sendOfferDamage kontrolera OfferController
        * (slanje ponude za postojeci kvar sa zadatim ID-em)
        * u slucaju kada ne postoji kvar sa zadatim ID-em
        * */

        String token = "{\"price\":1000.0}";
        this.mockMvc.perform(post(URL_PREFIX + "/businesses/101/damages/502/send_offer")
                .header("Authentication-Token", accessTokenCompany)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testSendOfferDamageBusinessesAreNotSame() throws Exception{
        /*
        * Test proverava ispravnost rada metode sendOfferDamage kontrolera OfferController
        * (slanje ponude za postojeci kvar sa zadatim ID-em)
        * u slucaju kada trenutno ulogovana firma/institucija nema ID koji je isti kao zadati
        * */

        String token = "{\"price\":1000.0}";

        this.mockMvc.perform(post(URL_PREFIX + "/businesses/102/damages/102/send_offer")
                .header("Authentication-Token", accessTokenCompany)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSendOfferDamageMailNotSent() throws Exception{
        /*
        * Test proverava ispravnost rada metode sendOfferDamage kontrolera OfferController
        * (slanje ponude za postojeci kvar sa zadatim ID-em)
        * u slucaju kada je prosledjen ID firme od koje nije trazena ponuda
        * */

        String token = "{\"price\":1000.0}";

        this.mockMvc.perform(post(URL_PREFIX + "/businesses/101/damages/101/send_offer")
                .header("Authentication-Token", accessTokenCompany)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSendOfferDamageMissingPrice() throws Exception{
        /*
        * Test proverava ispravnost rada metode sendOfferDamage kontrolera OfferController
        * (slanje ponude za postojeci kvar sa zadatim ID-em)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne sadrzi sva neophodna polja - nedostaje polje price
        * */

        String token = "{}";
        this.mockMvc.perform(post(URL_PREFIX + "/businesses/102/damages/101/send_offer")
                .header("Authentication-Token", accessTokenCompany)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSendOfferDamageInappropriateTypeOfPrice() throws Exception{
        /*
        * Test proverava ispravnost rada metode sendOfferDamage kontrolera OfferController
        * (slanje ponude za postojeci kvar sa zadatim ID-em)
        * u slucaju kada je dolazni DTO objekat nevalidan: sadrzi sva neophodna poljaa, ali je polje price nevalidnog tipa
        * */

        String token = "{\"price\":\"string\"}";

        this.mockMvc.perform(post(URL_PREFIX + "/businesses/102/damages/101/send_offer")
                .header("Authentication-Token", accessTokenCompany)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testGetOfferDamage() throws Exception{
        /*Test proverava ispravnost rada metode getOfferForDamage kontrolera OfferController.
        * (dobavljanje ponude na osnovu id-a kvara i firme/institucije)
        * u slucaju kada su svi parametri validni
         */

        this.mockMvc.perform(get(URL_PREFIX + "/businesses/101/damages/100/accepted_offer")
                .header("Authentication-Token", accessTokenCompany))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetOfferDamageForbidden() throws Exception{
        /*Test proverava ispravnost rada metode getOfferForDamage kontrolera OfferController.
        * (dobavljanje ponude na osnovu id-a kvara i firme/institucije)
        * u slucaju kada ID firme ne pripada datom kvaru
         */

        this.mockMvc.perform(get(URL_PREFIX + "/businesses/100/damages/100/accepted_offer")
                .header("Authentication-Token", accessTokenCompany))
                .andExpect(status().isForbidden());
    }
}
