package rs.ac.uns.ftn.ktsnvt.web.dto;

import rs.ac.uns.ftn.ktsnvt.model.entity.Questionnaire;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Ivana Zeljkovic on 13/11/2017.
 */
public class QuestionnaireDTO implements Serializable {

    private Long id;
    private List<QuestionDTO> questions;
    private List<VoteDTO> votes;
    private Date dateExpire;
    private Long buildingId;

    public QuestionnaireDTO() {
        this.questions = new ArrayList<>();
        this.votes = new ArrayList<>();
    }

    public QuestionnaireDTO(Questionnaire questionnaire, Long buildingId) {
        this.id = questionnaire.getId();

        List<QuestionDTO> questionDTOS = new ArrayList<>();
        questionnaire.getQuestions().forEach(question -> questionDTOS.add(new QuestionDTO(question)));
        this.questions = questionDTOS;

        this.dateExpire = questionnaire.getDateExpire();

        List<VoteDTO> voteDTOS = new ArrayList<>();
        questionnaire.getVotes().forEach(vote -> voteDTOS.add(new VoteDTO(vote)));
        this.votes = voteDTOS;

        this.buildingId = buildingId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<QuestionDTO> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionDTO> questions) {
        this.questions = questions;
    }

    public List<VoteDTO> getVotes() {
        return votes;
    }

    public void setVotes(List<VoteDTO> votes) {
        this.votes = votes;
    }

    public Date getDateExpire() {
        return dateExpire;
    }

    public void setDateExpire(Date dateExpire) {
        this.dateExpire = dateExpire;
    }

    public Long getBuildingId() { return buildingId; }

    public void setBuildingId(Long buildingId) { this.buildingId = buildingId; }
}
