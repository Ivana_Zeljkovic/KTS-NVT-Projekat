import {Component, EventEmitter, Input, OnInit} from '@angular/core';
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {ResourceId} from "../../shared/models/id";
import {ToasterService} from "angular5-toaster/dist";
// model
import {PageAndSort} from "../../shared/models/page-and-sort";
import {Tenant} from "../../shared/models/tenant";
import {Apartment} from "../../shared/models/apartment";
// service
import {TenantService} from "../../core/services/tenant.service";
import {ApartmentService} from "../../core/services/apartment.service";
// error
import {AppError} from "../../shared/errors/app-error";
import {NotFoundError} from "../../shared/errors/not-found-error";
import {ForbiddenError} from "../../shared/errors/forbidden-error";
import {BadRequestError} from "../../shared/errors/bad-request-error";

@Component({
  selector: 'app-choose-owner-modal',
  templateUrl: './choose-owner-modal.component.html',
  styleUrls: ['./choose-owner-modal.component.css']
})
export class ChooseOwnerModalComponent implements OnInit {

  modalRef: BsModalRef;
  @Input() apartment: Apartment;
  tenants: Array<Tenant>;
  owner: ResourceId;
  ownerSet: EventEmitter<Tenant> = new EventEmitter();
  tenantAdded: EventEmitter<Tenant> = new EventEmitter();
  @Input() addingNewTenant: boolean;

  currentPageTenant: number;
  totalItemsTenant: number;
  maxSizeTenant: number;
  itemsPerPageTenant: number;
  pageSizeTenant: string;
  sortDirectionTenant: string;
  sortPropertiesTenant: string[];

  constructor(private modalService : BsModalService,  private toasterService: ToasterService,
              private tenantService: TenantService, private apartmentService: ApartmentService) {

    this.currentPageTenant = 1;
    this.maxSizeTenant = 3;
    this.itemsPerPageTenant = 6;

    this.pageSizeTenant = '6';
    this.sortDirectionTenant = 'DESC';
    this.sortPropertiesTenant = ['firstName'];
  }

  ngOnInit() {
    this.getTenants();
  }

  pageChanged(event: any) {
    this.currentPageTenant = event.page;
    this.getTenants();
  }

  getTenants(){
    let page = new PageAndSort((this.currentPageTenant - 1).toString(), this.pageSizeTenant, this.sortDirectionTenant,
      this.sortPropertiesTenant);
    this.tenantService.getPage(page)
      .subscribe(data => {
        this.tenants = data;
        if (data.length > 0)
          this.totalItemsTenant = data[0].numberOfElements;
        else this.totalItemsTenant = 0;
      },(error: AppError) => {
        if(error instanceof NotFoundError)
          this.toasterService.pop('error', 'Error', 'Tenants not found!');
        else if(error instanceof ForbiddenError)
          this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
        else if(error instanceof BadRequestError)
          this.toasterService.pop('error', 'Error', 'Bad request!');
        else {
          this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
          throw error;
        }
      });
  }

  chooseTenant(tenant: Tenant){
    if(this.addingNewTenant)
      this.addNewTenant(tenant);
    else
      this.addOwner(tenant);
  }

  addOwner(tenant: Tenant){
    this.owner = new ResourceId(tenant.id);
    this.apartmentService.updatePart(this.owner, 'buildings', this.apartment.buildingId,
      'apartments', this.apartment.id, 'make_tenant_owner').subscribe(data => {
        this.apartment = data;
      this.ownerSet.emit(tenant);
        this.toasterService.pop('success', 'Congratulation', 'Successfully chosen new owner!');
        this.modalRef.hide();
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Tenant not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Error', 'Bad request!');
      else {
        this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
        throw error;
      }
    });
  }

  addNewTenant(tenant: Tenant){
    let newTenant = new ResourceId(tenant.id);
    this.apartmentService.updatePart(newTenant, 'buildings', this.apartment.buildingId,
      'apartments', this.apartment.id, 'connect_tenant_and_apartment').subscribe(data => {
      this.apartment = data;
      this.tenantAdded.emit(tenant);
      this.toasterService.pop('success', 'Congratulation', 'Successfully added new tenant!');
      this.modalRef.hide();
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Tenant not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Error', 'Bad request!');
      else {
        this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
        throw error;
      }
    });
  }
}
