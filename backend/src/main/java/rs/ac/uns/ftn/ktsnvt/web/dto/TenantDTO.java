package rs.ac.uns.ftn.ktsnvt.web.dto;

import rs.ac.uns.ftn.ktsnvt.model.entity.Tenant;

import java.io.Serializable;
import java.util.Date;

/**
* Created by Ivana Zeljkovic on 06/11/2017.
*/
public class TenantDTO implements Serializable {

    private Long id;
    private String firstName;
    private String lastName;
    private Date birthDate;
    private String email;
    private String phoneNumber;
    private String username;
    private boolean confirmed;
    private AddressDTO address;
    private long numberOfElements;

    public TenantDTO() { }

    public TenantDTO(Tenant tenant) {
        this.id = tenant.getId();
        this.firstName = tenant.getFirstName();
        this.lastName = tenant.getLastName();
        this.birthDate = tenant.getBirthDate();
        this.email = tenant.getEmail();
        this.phoneNumber = tenant.getPhoneNumber();
        this.username = tenant.getAccount().getUsername();
        this.confirmed = tenant.getConfirmed();
        if(tenant.getApartmentLiveIn() != null)
            this.address = new AddressDTO(tenant.getApartmentLiveIn().getBuilding().getAddress());
    }

    public TenantDTO(Tenant tenant, long numberOfelements) {
        this.id = tenant.getId();
        this.firstName = tenant.getFirstName();
        this.lastName = tenant.getLastName();
        this.birthDate = tenant.getBirthDate();
        this.email = tenant.getEmail();
        this.phoneNumber = tenant.getPhoneNumber();
        this.username = tenant.getAccount().getUsername();
        this.confirmed = tenant.getConfirmed();
        if(tenant.getApartmentLiveIn() != null)
            this.address = new AddressDTO(tenant.getApartmentLiveIn().getBuilding().getAddress());
        this.numberOfElements = numberOfelements;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getUsername() { return username; }

    public void setUsername(String username) { this.username = username; }

    public boolean isConfirmed() { return confirmed; }

    public void setConfirmed(boolean confirmed) { this.confirmed = confirmed; }

    public AddressDTO getAddress() { return address; }

    public void setAddress(AddressDTO addressDTO) { this.address = addressDTO; }

    public long getNumberOfElements() { return numberOfElements; }

    public void setNumberOfElements(long numberOfElements) { this.numberOfElements = numberOfElements; }
}
