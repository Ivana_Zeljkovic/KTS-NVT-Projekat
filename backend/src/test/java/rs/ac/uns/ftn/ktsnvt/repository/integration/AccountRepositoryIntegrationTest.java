package rs.ac.uns.ftn.ktsnvt.repository.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.model.entity.Account;
import rs.ac.uns.ftn.ktsnvt.repository.AccountRepository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by Nikola Garabandic on 30/11/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class AccountRepositoryIntegrationTest {

    @Autowired
    private AccountRepository accountRepository;


    @Test
    @Transactional
    public void testFindByUsername() {
        /*
         * Test proverava ispravnost rada metode findByUsername repozitorijuma AccountRepository
         * (pronalazenje naloga na osnovu korisnickog imena)
         * */

        /*
        postoji samo jedan stanar koji zadovoljava pretragu sa parametrom:
            korisnicko ime = kaca
         */
        Account account = this.accountRepository.findByUsername("kaca");

        assertNotNull(account);
        assertEquals("Katarina", account.getTenant().getFirstName());
        assertEquals("Cukurov", account.getTenant().getLastName());
        assertEquals("kaca", account.getUsername());
    }

    @Test
    @Transactional
    public void testNotFoundByUsername() {
        /*
         * Test proverava ispravnost rada metode findByUsername repozitorijuma AccountRepository
         * (pronalazenje naloga na osnovu korisnickog imena)
         * */

        // ne postoji nijedan stanar koji ima korisnicko ime = kca
        Account account_1 = this.accountRepository.findByUsername("kca");

        assertNull(account_1);
    }
}
