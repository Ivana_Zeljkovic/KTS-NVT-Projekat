import { Component, OnInit } from '@angular/core';
import { ToasterConfig, ToasterService } from 'angular5-toaster/dist';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Router } from '@angular/router';
// model
import { Building } from '../../shared/models/building';
import { Tenant } from '../../shared/models/tenant';
// service
import { BuildingService } from '../../core/services/building.service';
import { TenantService } from '../../core/services/tenant.service';
import { JwtService } from '../../core/services/jwt.service';
// component
import { ConfirmDeleteModalComponent } from '../../shared/components/confirm-delete-modal/confirm-delete-modal.component';
import { ChoosePresidentModalComponent } from '../../shared/components/choose-president-modal/choose-president-modal.component';
// error
import { AppError } from '../../shared/errors/app-error';
import { NotFoundError } from '../../shared/errors/not-found-error';
import { ForbiddenError } from '../../shared/errors/forbidden-error';
import { BadRequestError } from '../../shared/errors/bad-request-error';
import {PageAndSort} from '../../shared/models/page-and-sort';


@Component({
  selector: 'app-building-list',
  templateUrl: './building-list.component.html',
  styleUrls: ['./building-list.component.css']
})
export class BuildingListComponent implements OnInit {
  buildingsList: Array<Building>;
  tenants: Array<Tenant>;
  modalRef: BsModalRef;
  toasterConfig: ToasterConfig;

  // Elements for bootstrap paging
  currentPage: number;
  totalItems: number;
  maxSize: number;
  itemsPerPage: number;

  currentPageTenant: number;
  totalItemsTenant: number;
  maxSizeTenant: number;
  itemsPerPageTenant: number;

  // Elements for getting data from server
  pageSize: string;
  sortDirection: string;
  sortProperties: string[];
  pageSizeTenant: string;
  sortDirectionTenant: string;
  sortPropertiesTenant: string[];

  constructor(private buildingService: BuildingService, private modalService: BsModalService,
              private toasterService: ToasterService, private router: Router, private tenantService: TenantService,
              private jwtService : JwtService) {
    this.toasterConfig = new ToasterConfig({
      timeout: {success: 2000, error: 4000, warning: 4000}
    });
    this.buildingsList = new Array<Building>();

    // Sett up for bootstrap paging
    this.currentPage = 1;
    this.maxSize = 3;
    this.itemsPerPage = 10;

    this.currentPageTenant = 1;
    this.maxSizeTenant = 3;
    this.itemsPerPageTenant = 10;

    // Sett up for getting notification request
    this.pageSize = '10';
    this.sortDirection = 'DESC';
    this.sortProperties = ['id'];


    this.pageSizeTenant = '4';
    this.sortDirectionTenant = 'DESC';
    this.sortPropertiesTenant = ['firstName'];

  }

  ngOnInit() {
    this.getBuildings();
  }

  getBuildings(){
    let page = new PageAndSort((this.currentPage - 1).toString(), this.pageSize, this.sortDirection, this.sortProperties);

    if(this.jwtService.hasRole('ADMIN')) {
      this.loadBuildings();
    }
    else if(this.jwtService.hasRole('COMPANY')){
      this.buildingService.getPage(page, 'firms', this.jwtService.getIdFromToken(), 'buildings')
        .subscribe(data =>{
          if (data.length > 0)
            this.totalItems = data[0].numberOfElements;
          else this.totalItems = 0;
          this.buildingsList = data;
        }, (error: AppError) => {
          if (error instanceof ForbiddenError)
            this.toasterService.pop('error', 'Error','You are forbidden from getting list of all the buildings!');
          else {
            this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
            throw error;
          }
        });
    }
  }

  deleteBuilding(building: Building){
    this.modalRef = this.modalService.show(
      ConfirmDeleteModalComponent,
      Object.assign({},{ class: 'modal-sm' })
    );

    this.modalRef.content.confirmed.subscribe((value) => {
      if(value) {
        let index = this.buildingsList.indexOf(building, 0);
        this.buildingsList.splice(index, 1);
        this.buildingService.remove(building.id).subscribe(() => {
            this.toasterService.pop('success', 'Congratulation', 'Successfully deleted building!');
            this.loadBuildings();
          },
          (error: AppError) => {
            this.buildingsList.splice(index, 0, building);

            if(error instanceof NotFoundError)
              this.toasterService.pop('error', 'Error', 'Building not found!');
            else if(error instanceof ForbiddenError)
              this.toasterService.pop('error', 'Error', 'You are not allowed to delete this building!');
            else if(error instanceof BadRequestError)
              this.toasterService.pop('error', 'Error', 'This building has apartments with tenants or ' +
                                      'owners and can\'t be deleted!');
            else {
              this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
              throw error;
            }
          });
      }
    });
  }

  searchByCity(city: string){
    if(city == ''){
      this.buildingService.getAll()
        .subscribe(data => {
          this.buildingsList = data;
        });
    } else {
      this.buildingService.getFromAddress(city)
        .subscribe(data => {
          this.buildingsList = data;
        }, (error: AppError) => {
          if (error instanceof ForbiddenError)
            this.toasterService.pop('error', 'Error', 'You are not allowed to search buildings!');
          else if (error instanceof BadRequestError)
            this.toasterService.pop('error', 'Error', 'There are no buildings in this city!');
          else {
            this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
            throw error;
          }
        })
    }
  }

  addPresident(building: Building){
    let page = new PageAndSort((this.currentPageTenant - 1).toString(), this.pageSizeTenant, this.sortDirectionTenant,
      this.sortPropertiesTenant);
    this.tenantService.getTenantsByBuildingId(page, building.id.toString())
      .subscribe(data => {
        this.tenants = data;
        this.modalRef = this.modalService.show(
          ChoosePresidentModalComponent,
          Object.assign({},{ class: 'modal-lg' })
        );

        if (data.length > 0)
          this.totalItemsTenant = data[0].numberOfElements;
        else this.totalItemsTenant = 0;
        this.modalRef.content.modalRef = this.modalRef;
        this.modalRef.content.buildingId = building.id;
        this.modalRef.content.tenants = this.tenants;
        this.modalRef.content.building = building;
        this.modalRef.content.currentPageTenant = this.currentPageTenant;
        this.modalRef.content.totalItemsTenant = this.totalItemsTenant;
        this.modalRef.content.maxSizeTenant = this.maxSizeTenant;
        this.modalRef.content.itemsPerPageTenant = this.itemsPerPageTenant;
        this.modalRef.content.pageSizeTenant = this.pageSizeTenant;
        this.modalRef.content.sortDirectionTenant = this.sortDirectionTenant;
        this.modalRef.content.sortPropertiesTenant = this.sortPropertiesTenant;
    },(error: AppError) => {
        if(error instanceof NotFoundError)
          this.toasterService.pop('error', 'Error', 'Building not found!');
        else if(error instanceof ForbiddenError)
          this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
        else if(error instanceof BadRequestError)
          this.toasterService.pop('error', 'Error', 'Bad request!');
        else {
          this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
          throw error;
        }
      });
  }

  loadBuildings(){
    let page = new PageAndSort((this.currentPage - 1).toString(), this.pageSize, this.sortDirection, this.sortProperties);
    this.buildingService.getPage(page).subscribe(data => {
      this.buildingsList = data;
      if(data.length > 0)
        this.totalItems = data[0].numberOfElements;
      else
        this.totalItems = 0;
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Buildings not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Error', 'Bad request!');
      else {
        this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
        throw error;
      }
    });

  }

  pageChanged(event: any) {
    this.currentPage = event.page;
    this.getBuildings();
  }

  registerBuilding(){
    this.router.navigate(['/buildings/register']);
  }

  moreDetails(building: Building) {
    this.router.navigate([`/buildings/${building.id}`]);
  }
}
