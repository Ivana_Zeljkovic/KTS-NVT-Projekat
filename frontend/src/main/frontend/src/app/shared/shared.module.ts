import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import {
  BsDatepickerModule, BsDropdownModule, TabsModule, ModalModule, PopoverModule,
  ButtonsModule, PaginationModule, TimepickerModule
} from "ngx-bootstrap";
import { AgmCoreModule } from "@agm/core";
import { ToasterModule } from "angular5-toaster/dist";
// validator
import { PasswordValidator } from "./validators/password.validator";
import { UsernameUniqueValidator } from "./validators/username-unique.validator";
// pipe
import { DateFormatPipe } from "./pipes/date-format.pipe";
import { DateTimeFormatPipe } from "./pipes/date-time-format.pipe";
import { BusinessTypePipe } from "./pipes/business-type.pipe";
import { DateTimeFormatServerPipe } from "./pipes/date-time-format-server.pipe";
import { DamageTypeFormatPipe } from "./pipes/damage-type-format.pipe";
import { QuestionContentPipe } from "./components/questionnaire/pipes/question-content.pipe";
// component
import { ConfirmDeleteModalComponent } from './components/confirm-delete-modal/confirm-delete-modal.component';
import { BusinessAddAccountModalComponent } from "./components/business-add-account/bussiness-add-account-modal.component";
import { BusinessProfileAccountsModalComponent } from "./components/business-profile-accounts-modal/business-profile-accounts-modal.component";
import { ChoosePresidentModalComponent } from './components/choose-president-modal/choose-president-modal.component';
import { AddApartmentModalComponent } from './components/add-apartment-modal/add-apartment-modal.component';
import { ChooseManagerModalComponent } from './components/choose-manager-modal/choose-manager-modal.component';
import { DelegateResponsibilityToInstitutionComponent } from "./components/delegate-responsibility-to-institution/delegate-responsibility-to-institution.component";
import { DelegateResponsibilityToPersonComponent } from "./components/delegate-responsibility-to-person/delegate-responsibility-to-person.component";
import { QuestionnaireComponent } from "./components/questionnaire/questionnaire.component";
import { CouncilMemberComponent } from './components/council-member/council-member.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot(),
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBtEDXxVtj8B6Pe_w5S0C7rx8p8rMgaVPU'
    }),
    ModalModule.forRoot(),
    PopoverModule.forRoot(),
    ButtonsModule.forRoot(),
    PaginationModule.forRoot(),
    ToasterModule
  ],
  declarations: [
    DateFormatPipe,
    DateTimeFormatPipe,
    DateTimeFormatServerPipe,
    BusinessTypePipe,
    DamageTypeFormatPipe,
    QuestionContentPipe,
    ConfirmDeleteModalComponent,
    BusinessAddAccountModalComponent,
    BusinessProfileAccountsModalComponent,
    ChoosePresidentModalComponent,
    AddApartmentModalComponent,
    ChooseManagerModalComponent,
    DelegateResponsibilityToInstitutionComponent,
    DelegateResponsibilityToPersonComponent,
    QuestionnaireComponent,
    CouncilMemberComponent
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BsDatepickerModule,
    TimepickerModule,
    BsDropdownModule,
    TabsModule,
    AgmCoreModule,
    ModalModule,
    ToasterModule,
    PopoverModule,
    ButtonsModule,
    PaginationModule,
    BusinessAddAccountModalComponent,
    BusinessProfileAccountsModalComponent,
    ConfirmDeleteModalComponent,
    ChoosePresidentModalComponent,
    AddApartmentModalComponent,
    ChooseManagerModalComponent,
    DelegateResponsibilityToInstitutionComponent,
    DelegateResponsibilityToPersonComponent,
    QuestionnaireComponent,
    CouncilMemberComponent,
    BusinessTypePipe,
    DateFormatPipe,
    DateTimeFormatPipe,
    DamageTypeFormatPipe,
    QuestionContentPipe
  ],
  providers: [
    PasswordValidator,
    UsernameUniqueValidator,
  ],
  entryComponents: [
    BusinessAddAccountModalComponent,
    ChoosePresidentModalComponent,
    AddApartmentModalComponent,
    ChooseManagerModalComponent,
    BusinessProfileAccountsModalComponent,
    CouncilMemberComponent,
    ConfirmDeleteModalComponent
  ]
})
export class SharedModule { }
