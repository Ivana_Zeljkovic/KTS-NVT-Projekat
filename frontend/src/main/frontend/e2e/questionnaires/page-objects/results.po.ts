import {by, element, ElementArrayFinder, ElementFinder} from "protractor";

export class ResultsPage {
  // get methods
  getResultsElement(): ElementFinder {
    return element(by.tagName('app-results'));
  }

  getTitle(): ElementFinder {
    return element(by.css('h4.modal-title'));
  }

  getCloseButton(): ElementFinder {
    return element(by.css('.modal-header > button'));
  }

  getNumberOfVotes(): ElementFinder {
    return element(by.css('.number-of-votes'));
  }

  getQuestions(): ElementArrayFinder {
    return element.all(by.css('ul.question-list > li'));
  }

  getContentOfQuestion(index: number): ElementFinder {
    return element(by.css(`ul.question-list > li:nth-child(${index+1}) > span`));
  }

  getQuestionAnswers(index: number): ElementArrayFinder {
    return element.all(by.css(`ul.question-list > li:nth-child(${index+1}) > ul > li`));
  }

  getAnswerOnQuestion(index: number, indexAnswer: number): ElementFinder {
    return element(by.css(`ul.question-list > li:nth-child(${index+1}) > ul > li:nth-child(${indexAnswer+1}) > span:nth-child(1)`));
  }

  getVoteNumberForAnswerOnQuestion(index: number, indexAnswer: number): ElementFinder {
    return element(by.css(`ul.question-list > li:nth-child(${index+1}) > ul > li:nth-child(${indexAnswer+1}) > span:nth-child(2) > span.vote-number`));
  }

  getVotePercentageForAnswerOnQuestion(index: number, indexAnswer: number): ElementFinder {
    return element(by.css(`ul.question-list > li:nth-child(${index+1}) > ul > li:nth-child(${indexAnswer+1}) > span:nth-child(2) > span.vote-percentage`));
  }
}
