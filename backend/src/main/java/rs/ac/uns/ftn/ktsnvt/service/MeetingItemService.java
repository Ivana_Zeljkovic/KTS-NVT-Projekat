package rs.ac.uns.ftn.ktsnvt.service;

import org.springframework.data.domain.Page;
import rs.ac.uns.ftn.ktsnvt.model.entity.MeetingItem;

import java.util.Date;
import java.util.List;

/**
 * Created by Ivana Zeljkovic on 13/11/2017.
 */
public interface MeetingItemService {

    MeetingItem save(MeetingItem meetingItem);

    List<MeetingItem> findAllOnBillboard(Long billboardId);

    Page<MeetingItem> findAllOnBillboard(Long billboardId, Integer pageNumber,
                                         Integer pageSize, String sortDirection, String sortProperty);

    Page<MeetingItem> findAllOnBillboardFromTo(Long billboardId, Date from, Date to, Integer pageNumber,
                                     Integer pageSize, String sortDirection, String sortProperty);

    Page<MeetingItem> findAllForMeeting(Long meetingId, Integer pageNumber, Integer pageSize,
                                        String sortDirection, String sortProperty);

    Page<MeetingItem> findAllForRecord(Long meetingId, Integer pageNumber, Integer pageSize,
                                       String sortDirection, String sortProperty);

    Page<MeetingItem> findAllWithActiveQuestionnairesForTenant(Long tenantId, Integer pageNumber, Integer pageSize,
                                                               String sortDirection, String sortProperty);

    Page<MeetingItem> findAllFinishedQuestionnairesInBuilding(Long buildingId, Integer pageNumber, Integer pageSize,
                                                              String sortDirection, String sortProperty);

    MeetingItem findOne(Long id);

    void findOneByIdAndBillboard(Long id, Long billboardId);

    void checkMeetingItemInBuilding(MeetingItem meetingItem, List<MeetingItem> items);

    void checkMeetingIsNull(MeetingItem meetingItem);

    Iterable<MeetingItem> save(Iterable<MeetingItem> meetingItems);

    void checkMeetingItemQuestionnaire(MeetingItem meetingItem, Long questionnaireId);

    void remove(Long meetingItemId);

    void containsDamage(Long damageId);
}
