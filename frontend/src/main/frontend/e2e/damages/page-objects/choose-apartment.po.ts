import {by, element, ElementArrayFinder, ElementFinder} from "protractor";

export class ChooseApartmentPage {
  // get methods
  getChooseApartmentElement(): ElementFinder {
    return element(by.tagName('app-choose-apartment-modal'));
  }

  getTitle(): ElementFinder {
    return element(by.css('app-choose-apartment-modal .modal-title'));
  }

  getCloseButton(): ElementFinder {
    return element(by.css('div.modal-header > button'));
  }

  getApartmentsRowInTable(): ElementArrayFinder {
    return element.all(by.css('#apartmentsTable tbody > tr'));
  }

  getApartmentFloor(index: number): ElementFinder {
    return element(by.css(`#apartmentsTable tbody > tr:nth-child(${index+1}) > td:nth-child(1)`));
  }

  getApartmentNumber(index: number): ElementFinder {
    return element(by.css(`#apartmentsTable tbody > tr:nth-child(${index+1}) > td:nth-child(2)`));
  }

  getButtonChooseApartment(index: number): ElementFinder {
    return element(by.css(`#apartmentsTable tbody > tr:nth-child(${index+1}) > td:nth-child(3) > button`));
  }
}
