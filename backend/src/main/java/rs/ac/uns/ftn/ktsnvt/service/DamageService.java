package rs.ac.uns.ftn.ktsnvt.service;

import org.springframework.data.domain.Page;
import rs.ac.uns.ftn.ktsnvt.model.entity.Damage;

import java.util.Date;

/**
 * Created by Ivana Zeljkovic on 23/11/2017.
 */
public interface DamageService {

    Damage save(Damage damage);

    Page<Damage> findAllByBuilding(Long buildingId, Integer pageNumber, Integer pageSize, String sortDirection,
                                   String sortProperty);

    Page<Damage> getDamagesForInstitution(Long institutionId, Integer pageNumber, Integer pageSize,
                                          String sortDirection, String sortProperty, String sortPropertySecond);

    Page<Damage> getDamagesForInstitutionFromTo(Long institutionId, Date fromDate, Date toDate, Integer pageNumber,
                                                Integer pageSize, String sortDirection, String sortProperty,
                                                String sortPropertySecond);

    Page<Damage> getDamagesForRepair(Long businessId, Integer pageNumber, Integer pageSize,
                                          String sortDirection, String sortProperty, String sortPropertySecond);

    Page<Damage> getDamagesForRepairFromTo(Long businessId, Date fromDate, Date toDate, Integer pageNumber,
                                           Integer pageSize, String sortDirection, String sortProperty,
                                           String sortPropertySecond);

    Page<Damage> getDamagesInBuilding(Long buildingId, Integer pageNumber, Integer pageSize,
                                     String sortDirection, String sortProperty, String sortPropertySecond);

    Page<Damage> getDamagesInBuildingFromTo(Long buildingId, Date fromDate, Date toDate, Integer pageNumber,
                                            Integer pageSize, String sortDirection, String sortProperty,
                                            String sortPropertySecond);

    Page<Damage> getDamagesForTenant(Long tenantId, Integer pageNumber, Integer pageSize,
                                      String sortDirection, String sortProperty, String sortPropertySecond);

    Page<Damage> getDamagesForTenantFromTo(Long tenantId, Date fromDate, Date toDate, Integer pageNumber,
                                           Integer pageSize, String sortDirection, String sortProperty,
                                           String sortPropertySecond);

    Page<Damage> getDamagesInPersonalApartments(Long tenantId, Integer pageNumber, Integer pageSize,
                                     String sortDirection, String sortProperty, String sortPropertySecond);

    Page<Damage> getDamagesInPersonalApartmentsFromTo(Long tenantId, Date fromDate, Date toDate, Integer pageNumber,
                                                      Integer pageSize, String sortDirection, String sortProperty,
                                                      String sortPropertySecond);

    Page<Damage> getDamagesInApartment(Long apartmentId, Integer pageNumber, Integer pageSize,
                                                String sortDirection, String sortProperty, String sortPropertySecond);

    Page<Damage> getDamagesInApartmentFromTo(Long apartmentId, Date fromDate, Date toDate, Integer pageNumber,
                                             Integer pageSize, String sortDirection, String sortProperty,
                                             String sortPropertySecond);

    Page<Damage> getMyDamages(Long tenantId, Integer pageNumber, Integer pageSize,
                                       String sortDirection, String sortProperty, String sortPropertySecond);

    Page<Damage> getMyDamagesFromTo(Long tenantId, Date fromDate, Date toDate, Integer pageNumber, Integer pageSize,
                              String sortDirection, String sortProperty, String sortPropertySecond);

    Damage findOne(Long id);

    void checkDamageInBuilding(Damage damage, Long buildingId);

    void checkResponsiblePerson(Damage damage, Long tenantId);

    void remove(Long damageId);

    void isDamageFixed(Damage damage);

    boolean findDamageIdInOfferRequest(Long damageId, Long businessId);

    Page<Damage> findAllDamageFixRequests(Long businessId, Integer pageNumber,
                                          Integer pageSize, String sortDirection, String sortProperty);

}
