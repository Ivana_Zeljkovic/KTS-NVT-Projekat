import { browser } from "protractor";
// page
import { MeetingListPage } from "../../page-objects/meeting-list.po";
import { MeetingAddPage } from "../page-objects/meeting-add.po";
import { LoginPage } from "../../page-objects/login.po";
import { MeetingItemListPage } from "../../home/tenant-home/page-objects/meeting-item-list.po";
import { MeetingItemPage } from "../../page-objects/meeting-item.po";
import { MeetingItemListModalPage } from "../page-objects/meeting-item-list-modal.po";
// helper
import { DateConverter } from "../../util/date-converter";


describe('Add new meeting', () => {
  let loginPage: LoginPage;
  let meetingListPage: MeetingListPage;
  let meetingAddPage: MeetingAddPage;
  let meetingItemListPage: MeetingItemListPage;
  let meetingItemPage: MeetingItemPage;
  let meetingItemListModalPage: MeetingItemListModalPage;
  let newMeetingItem: any;
  let newMeetingItem2: any;


  beforeAll(() => {
    loginPage = new LoginPage();
    meetingListPage = new MeetingListPage();
    meetingAddPage = new MeetingAddPage();
    meetingItemListModalPage = new MeetingItemListModalPage();
    meetingItemListPage = new MeetingItemListPage();
    meetingItemPage = new MeetingItemPage();
    newMeetingItem = {'date':'', 'creator':'', 'title':'First title', 'content':'First content'};
    newMeetingItem2 = {'date':'', 'creator':'', 'title':'Second title', 'content':'Second content'};


    browser.get('/');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');

    loginPage.setUsername('kaca');
    loginPage.setPassword('kaca');
    loginPage.getSignInButton().click();

    browser.wait(function() {
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/home/notifications';
      });
    }).then(function() {
      browser.get('http://localhost:49152/#/home/meeting-details-items');

      browser.wait(function() {
        return browser.getCurrentUrl().then(value => {
          return value === 'http://localhost:49152/#/home/meeting-details-items';
        });
      }).then(function() {
        expect(meetingItemListPage.getMeetingItemListElement().isDisplayed()).toBe(true);
        // before test we should add 2 new meeting items
        expect(meetingItemListPage.getAddButton().isDisplayed()).toBe(true);
        expect(meetingItemListPage.getAddButton().isEnabled()).toBe(true);

        // creating first meeting item
        meetingItemListPage.getAddButton().click();

        browser.wait(function () {
          // expect to show modal - meeting item page for create new meeting item
          return meetingItemPage.getMeetingItemElement().isDisplayed();
        }).then(function () {
          meetingItemPage.setTitle(newMeetingItem.title);
          meetingItemPage.setContent(newMeetingItem.content);
          meetingItemPage.getSaveButton().click();

          // to be sure that is not used old instace
          meetingItemListPage = new MeetingItemListPage();

          browser.wait(function() {
            return meetingItemListPage.getMeetingItemListElement().isDisplayed();
          }).then(function() {
            // expect that new meeting item is first in list include menu with edit and delete links
            expect(meetingItemListPage.getTitleFromMeetingItemPanel(0).getText()).toEqual(newMeetingItem.title);
            expect(meetingItemListPage.getContentFromMeetingItemPanel(0).getText()).toEqual(newMeetingItem.content);
            meetingItemListPage.getDateFromMeetingItemPanel(0).getText().then(value => {
              // difference between current time and time of new meeting item is less then 5sec
              let currentDate = new Date().getTime();
              newMeetingItem.date = value;
              let meetingItemDate = DateConverter.convertStringToDate(value).getTime();
              expect(currentDate - meetingItemDate).toBeLessThan(5000);
            });
            meetingItemListPage.getCreatorNameFromMeetingItemPanel(0).getText().then(value => {
              newMeetingItem.creator = value;
            });

            // creating second meeting item
            meetingItemListPage.getAddButton().click();
            meetingItemPage = new MeetingItemPage();

            browser.wait(function () {
              // expect to show modal - meeting item page for create new meeting item
              return meetingItemPage.getMeetingItemElement().isDisplayed();
            }).then(function () {
              meetingItemPage.setTitle(newMeetingItem2.title);
              meetingItemPage.setContent(newMeetingItem2.content);
              meetingItemPage.getSaveButton().click();

              meetingItemListPage = new MeetingItemListPage();

              browser.wait(function() {
                return meetingItemListPage.getMeetingItemListElement().isDisplayed();
              }).then(function() {
                // expect that new meeting item is first in list include menu with edit and delete links
                expect(meetingItemListPage.getTitleFromMeetingItemPanel(0).getText()).toEqual(newMeetingItem2.title);
                expect(meetingItemListPage.getContentFromMeetingItemPanel(0).getText()).toEqual(newMeetingItem2.content);
                meetingItemListPage.getDateFromMeetingItemPanel(0).getText().then(value => {
                  // difference between current time and time of new meeting item is less then 5sec
                  let currentDate = new Date().getTime();
                  newMeetingItem2.date = value;
                  let meetingItemDate = DateConverter.convertStringToDate(value).getTime();
                  expect(currentDate - meetingItemDate).toBeLessThan(5000);
                });
                meetingItemListPage.getCreatorNameFromMeetingItemPanel(0).getText().then(value => {
                  newMeetingItem2.creator = value;
                });

                browser.get('http://localhost:49152/#/meetings');
              });
            });
          });
        });
      });
    });
  });

  afterAll(() => {
    browser.executeScript('window.localStorage.clear();');
  });




  it('should show all elements of modal dialog for creating new meeting when user click on ' +
    'Create meeting link', () => {
    browser.wait(function() {
      return meetingListPage.getMeetingListElement().isDisplayed();
    }).then(function() {
      expect(meetingListPage.getAddLink().isDisplayed()).toBe(true);
      meetingListPage.getAddLink().click();

      browser.wait(function() {
        return meetingAddPage.getMeetingAddElement().isDisplayed();
      }).then(function() {
        expect(meetingAddPage.getTitle().isDisplayed()).toBe(true);
        expect(meetingAddPage.getMeetingDate().isDisplayed()).toBe(true);
        expect(meetingAddPage.getMeetingTime().isDisplayed()).toBe(true);
        expect(meetingAddPage.getMeetingDuration().isDisplayed()).toBe(true);
        expect(meetingAddPage.getChooseMeetingItemsLink().isDisplayed()).toBe(true);
        expect(meetingAddPage.getSaveButton().isDisplayed()).toBe(true);
        expect(meetingAddPage.getSaveButton().isEnabled()).toBe(false);
        expect(meetingAddPage.getResetButton().isDisplayed()).toBe(true);
        expect(meetingAddPage.getResetButton().isEnabled()).toBe(false);
        expect(meetingAddPage.getCancelButton().isDisplayed()).toBe(true);
        expect(meetingAddPage.getCancelButton().isEnabled()).toBe(true);
      });
    });
  });



  it('should enable Reset button when user change something in form, and reset values of form to ' +
    'initial values when user click on Reset button', () => {
    expect(meetingAddPage.getMeetingDuration().getAttribute('value')).toEqual('0');
    meetingAddPage.getMeetingDuration().clear();
    meetingAddPage.setMeetingDuration('60');

    expect(meetingAddPage.getResetButton().isEnabled()).toBe(true);
    meetingAddPage.getResetButton().click();

    expect(meetingAddPage.getMeetingDuration().getAttribute('value')).toEqual('0');
    expect(meetingAddPage.getResetButton().isEnabled()).toBe(false);
  });



  it('should show selected meeting item when user choose some meeting item from modal dialog ' +
    'that are displayed when he click on Choose meeting items link', () => {
    meetingAddPage.getChooseMeetingItemsLink().click();

    browser.wait(function() {
      return meetingItemListModalPage.getMeetingItemListModalElement().isDisplayed();
    }).then(function() {
      // expect to see first two meeting items with select label
      meetingItemListModalPage.getMeetingItems().count().then(value => {
        // there is for sure two meeting items
        expect(value).toBeGreaterThanOrEqual(2);
        expect(meetingItemListModalPage.getMeetingItemDate(0).getText()).toEqual(newMeetingItem2.date);
        expect(meetingItemListModalPage.getMeetingItemDate(1).getText()).toEqual(newMeetingItem.date);
        expect(meetingItemListModalPage.getLabelSelect(0).isDisplayed()).toBe(true);
        expect(meetingItemListModalPage.getLabelSelect(1).isPresent()).toBe(true);

        meetingItemListModalPage.getLabelSelect(0).click();
        expect(meetingItemListModalPage.getSaveButton().isEnabled()).toBe(true);
        expect(meetingItemListModalPage.getLabelRemove(0).isDisplayed()).toBe(true);

        meetingItemListModalPage.getSaveButton().click();

        meetingAddPage = new MeetingAddPage();

        browser.wait(function() {
          return meetingAddPage.getMeetingAddElement().isDisplayed();
        }).then(function() {
          meetingAddPage.getMeetingItems().count().then(numberOfMeetingItems => {
            expect(numberOfMeetingItems).toEqual(1);
          });
        });
      });
    });
  });



  it('should show remove label on already selected meeting item when user click again on ' +
    'Choose meeting items link, and close modal dialog when user click on Cancel button without any change',
    () => {
    meetingItemListModalPage = new MeetingItemListModalPage();
    expect(meetingAddPage.getChooseMeetingItemsLink().isDisplayed()).toBe(true);
    meetingAddPage.getChooseMeetingItemsLink().click();

    browser.wait(function() {
      return meetingItemListModalPage.getMeetingItemListModalElement().isDisplayed();
    }).then(function() {
      expect(meetingItemListModalPage.getSaveButton().isEnabled()).toBe(false);
      expect(meetingItemListModalPage.getLabelRemove(0).isDisplayed()).toBe(true);

      // select one more meeting item and then click on Cancel
      expect(meetingItemListModalPage.getLabelSelect(1).isDisplayed()).toBe(true);
      meetingItemListModalPage.getLabelSelect(1).click();

      browser.wait(function() {
        return meetingItemListModalPage.getLabelRemove(1).isDisplayed();
      }).then(function() {
        expect(meetingItemListModalPage.getSaveButton().isEnabled()).toBe(true);

        meetingItemListModalPage.getCancelButton().click();

        meetingAddPage = new MeetingAddPage();

        // after cancel, there is still old state of selected meeting items
        browser.wait(function() {
          return meetingAddPage.getMeetingAddElement().isDisplayed();
        }).then(function() {
          meetingAddPage.getMeetingItems().count().then(value => {
            expect(value).toEqual(1);
          });
        });
      });
    });
  });




  it('should remove all selected meeting items when user click on Reset button in modal ' +
    'for creating new meeting', () => {
    expect(meetingAddPage.getResetButton().isEnabled()).toBe(true);

    meetingAddPage.getResetButton().click();

    meetingAddPage.getMeetingItems().count().then(value => {
      expect(value).toEqual(0);
    })
  })
});
