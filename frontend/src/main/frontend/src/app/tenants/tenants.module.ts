import { NgModule } from '@angular/core';
// module
import { SharedModule } from "../shared/shared.module";
import { TenantsRouterModule } from "./tenants-router.module";
// component
import { TenantComponent } from './tenant/tenant.component';
import { TenantListComponent } from './tenant-list/tenant-list.component';

@NgModule({
  imports: [
    SharedModule,
    TenantsRouterModule
  ],
  declarations: [
    TenantComponent,
    TenantListComponent
  ]
})
export class TenantsModule { }
