package rs.ac.uns.ftn.ktsnvt.web.controller;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.ForbiddenException;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.*;
import rs.ac.uns.ftn.ktsnvt.service.*;
import rs.ac.uns.ftn.ktsnvt.web.controller.util.MessageConstants;
import rs.ac.uns.ftn.ktsnvt.web.dto.*;
import org.springframework.security.core.userdetails.User;


import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

import static rs.ac.uns.ftn.ktsnvt.web.util.ConverterDTOToModel.convertAdressDTOToAddress;
import static rs.ac.uns.ftn.ktsnvt.web.util.ConverterDTOToModel.convertBusinessCreateDTOToBusiness;
import static rs.ac.uns.ftn.ktsnvt.web.util.ConverterDTOToModel.convertCityDTOToCity;


/**
 * Created by Ivana Zeljkovic on 06/11/2017.
 */
@RestController
@RequestMapping(value = "/api/businesses")
@Api(value = "businesses", description = "Operations pertaining to firms and institutions in application.")
public class BusinessController {

    @Autowired
    private BusinessService businessService;

    @Autowired
    private CityService cityService;

    @Autowired
    private AddressService addressService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private AuthorityService authorityService;

    @Autowired
    private AccountAuthorityService accountAuthorityService;

    @Autowired
    private OfferService offerService;



    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a list of all firms and institutions in page form.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of firms and institutions", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to view these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    })
    public ResponseEntity getAllBusinesses(
            @ApiParam(value = "The page number for getting businesses.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting businesses.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting businesses.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "The sort property for getting businesses.", required = true) @RequestParam String sortProperty) {

        boolean isBusiness = false;
        Page<Business> institutions = null;

        for(GrantedAuthority authority : SecurityContextHolder.getContext().getAuthentication().getAuthorities()) {
            if(authority.getAuthority().equals(MessageConstants.INSTITUTION_ROLE) ||
                    authority.getAuthority().equals(MessageConstants.FIRM_ROLE)) {
                isBusiness = true;
                break;
            }
        }

        if(!isBusiness)
            institutions = this.businessService.findAll(Integer.valueOf(pageNumber), Integer.valueOf(pageSize),
                    sortDirection, sortProperty);
        else {
            Long currentId = this.accountService.findByUsername(
                    SecurityContextHolder.getContext().getAuthentication().getName()).getBusinessOwner().getId();
            institutions = this.businessService.findAllExceptCurrent(currentId, Integer.valueOf(pageNumber),
                    Integer.valueOf(pageSize),sortDirection, sortProperty);
        }

        List<BusinessDTO> businessDTOS = new ArrayList<>();
        for(Business business : institutions.getContent()) {
            businessDTOS.add(new BusinessDTO(business, institutions.getTotalElements()));
        }

        return new ResponseEntity<>(businessDTOS, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/institutions",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a list of all institutions in page form.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of institutions", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to view these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    })
    public ResponseEntity getAllInstitutions(
            @ApiParam(value = "The page number for getting institutions.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting institutions.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting institutions.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "The sort property for getting institutions.", required = true) @RequestParam String sortProperty) {

        boolean isInstitution = false;
        boolean isAdmin = false;
        Page<Business> institutions = null;

        for(GrantedAuthority authority : SecurityContextHolder.getContext().getAuthentication().getAuthorities()) {
            if(authority.getAuthority().equals(MessageConstants.INSTITUTION_ROLE))
                isInstitution = true;
            else if(authority.getAuthority().equals(MessageConstants.ADMIN_ROLE))
                isAdmin = true;
        }

        if(!isInstitution) {
            if(isAdmin){
                institutions = this.businessService.findAllInstitutionsOrFirmsByAdmin(
                        false, Integer.valueOf(pageNumber), Integer.valueOf(pageSize),
                        sortDirection, sortProperty);
            }
            else {
                institutions = this.businessService.findAllInstitutionsOrFirms(
                        false, Integer.valueOf(pageNumber), Integer.valueOf(pageSize),
                        sortDirection, sortProperty);
            }
        }
        else {
            Long currentId = this.accountService.findByUsername(
                    SecurityContextHolder.getContext().getAuthentication().getName()).getBusinessOwner().getId();
            institutions = this.businessService.findAllInstitutionsOrFirmsExceptCurrent(
                    false, currentId, Integer.valueOf(pageNumber),
                    Integer.valueOf(pageSize),sortDirection, sortProperty);
        }

        List<BusinessDTO> businessDTOS = new ArrayList<>();
        for(Business business : institutions.getContent()) {
            businessDTOS.add(new BusinessDTO(business, institutions.getTotalElements()));
        }

        return new ResponseEntity<>(businessDTOS, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/firms",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a list of all firms in page form.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of firms", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to view these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    })
    public ResponseEntity getAllFirms(
            @ApiParam(value = "The page number for getting firms.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting firms.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting firms.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "The sort property for getting firms.", required = true) @RequestParam String sortProperty) {

        boolean isFirm = false;
        boolean isAdmin = false;
        Page<Business> firms = null;

        for(GrantedAuthority authority : SecurityContextHolder.getContext().getAuthentication().getAuthorities()) {
            if(authority.getAuthority().equals(MessageConstants.FIRM_ROLE))
                isFirm = true;
            else if(authority.getAuthority().equals(MessageConstants.ADMIN_ROLE))
                isAdmin = true;
        }

        if(!isFirm) {
            if(isAdmin){
                firms = this.businessService.findAllInstitutionsOrFirmsByAdmin(true, Integer.valueOf(pageNumber),
                        Integer.valueOf(pageSize), sortDirection, sortProperty);
            }
            else {
                firms = this.businessService.findAllInstitutionsOrFirms(true, Integer.valueOf(pageNumber),
                        Integer.valueOf(pageSize), sortDirection, sortProperty);
            }
        }
        else {
            Long currentId = this.accountService.findByUsername(
                    SecurityContextHolder.getContext().getAuthentication().getName()).getBusinessOwner().getId();
            firms = this.businessService.findAllInstitutionsOrFirmsExceptCurrent(true, currentId,
                    Integer.valueOf(pageNumber), Integer.valueOf(pageSize),sortDirection, sortProperty);
        }

        List<BusinessDTO> businessDTOS = new ArrayList<>();
        for(Business business : firms.getContent()) {
            businessDTOS.add(new BusinessDTO(business, firms.getTotalElements()));
        }

        return new ResponseEntity<>(businessDTOS, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a single firm or institution.",
            notes = "You have to provide a valid firm/institution ID.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved firm or institution", response = BusinessDTO.class),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    public ResponseEntity getBusiness(
            @ApiParam(value = "The ID of the existing firm/institution resource.", required = true) @PathVariable("id") Long id) {

        Business business = this.businessService.findOne(id);

        return new ResponseEntity<>(new BusinessDTO(business), HttpStatus.OK);
    }



    @RequestMapping(
            value = "/firms",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Create a company resource.",
            notes = "Returns the company being saved.",
            httpMethod = "POST",
            produces = "application/json",
            consumes = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully created company", response = BusinessDTO.class),
            @ApiResponse(code = 400, message = "Inappropriate company object sent in request body"),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 403, message = "You don't have permission to create resource"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    public ResponseEntity registerCompany(
            @ApiParam(value = "The company object", required = true) @RequestBody @Valid BusinessCreateDTO businessCreateDTO,
            @ApiParam(value = "The object that contains all errors from validation of DTO object") BindingResult errors) {
        return registerBusiness(true, businessCreateDTO);
    }



    @RequestMapping(
            value = "/institutions",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Create a institution resource.",
            notes = "Returns the institution being saved.",
            httpMethod = "POST",
            produces = "application/json",
            consumes = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully created institution", response = BusinessDTO.class),
            @ApiResponse(code = 400, message = "Inappropriate institution object sent in request body"),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 403, message = "You don't have permission to create resource"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")})
    public ResponseEntity registerInstitution(
            @ApiParam(value = "The institution object", required = true) @Valid @RequestBody BusinessCreateDTO businessCreateDTO,
            @ApiParam(value = "The object that contains all errors from validation of DTO object") BindingResult errors) {
        return registerBusiness(false, businessCreateDTO);
    }



    @RequestMapping(
            value = "/{id}",
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Update a firm/institution resource.",
            notes = "You have to provide a valid firm/institution ID in the URL. " +
                    "Method returns the firm/institution being updated.",
            httpMethod = "PUT",
            produces = "application/json",
            consumes = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated firm/institution", response = BusinessDTO.class),
            @ApiResponse(code = 400, message = "Inappropriate firm/institution object sent in request body"),
            @ApiResponse(code = 401, message = "You are not authorized to update the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('INSTITUTION') or hasAuthority('COMPANY')")
    public ResponseEntity updateBusiness(
            @ApiParam(value = "The ID of the existing firm/institution resource.", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "The firm/institution object with ID attribute.", required = true) @Valid @RequestBody BusinessUpdateDTO businessUpdateDTO,
            @ApiParam(value = "The object that contains all errors from validation of DTO object") BindingResult errors) {

        Business business = this.businessService.findOne(id);
        this.businessService.checkPermissionForCurrentBusiness(business.getId());

        business.setName(businessUpdateDTO.getName());
        business.setDescription(businessUpdateDTO.getDescription());
        business.setEmail(businessUpdateDTO.getEmail());
        business.setPhoneNumber(businessUpdateDTO.getPhoneNumber());

        City city = this.cityService.findByName(businessUpdateDTO.getAddress().getCity().getName());
        if (city == null) {
            city = convertCityDTOToCity(businessUpdateDTO.getAddress().getCity());
            this.cityService.save(city);
        }

        Address address = this.addressService.findByCityStreetNumber(city.getId(),
                businessUpdateDTO.getAddress().getStreet(), businessUpdateDTO.getAddress().getNumber());
        if (address == null) {
            address = convertAdressDTOToAddress(businessUpdateDTO.getAddress());
            address.setCity(city);
            this.addressService.save(address);
        }

        String username = ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        Account account = this.accountService.findByUsername(username);

        this.businessService.checkPasswordUpdate(account, businessUpdateDTO.getOldPassword(),
                businessUpdateDTO.getNewPassword(), businessUpdateDTO.getConfirmPassword());
        this.accountService.save(account);

        business.setAddress(address);
        business = this.businessService.save(business);
        return new ResponseEntity<>(new BusinessDTO(business), HttpStatus.OK);
    }



    @RequestMapping(
            value = "/{id}/deactivate",
            method = RequestMethod.PATCH,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Declare a firm/institution not active.",
            notes = "You have to provide valid firm/institution ID. " +
                    "Method returns the firm/institution declared inactive.",
            httpMethod = "PATCH",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully declared firm/institution inactive", response = BusinessDTO.class),
            @ApiResponse(code = 400, message = "Impossible update because of object's references"),
            @ApiResponse(code = 401, message = "You are not authorized to update the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('INSTITUTION') or hasAuthority('COMPANY')")
    public ResponseEntity declareBusinessInactive(
            @ApiParam(value = "The ID of the existing firm/institution resource.", required = true) @PathVariable("id") Long id) {

        Business business = this.businessService.findOne(id);
        this.businessService.checkPermissionForCurrentBusiness(id);

        for (Damage damage : business.getDamagesForRepair()) {
            if (!damage.isFixed()) throw new BadRequestException("Business has damages to repair!");
        }

        if (!business.isIsCompany()) {
            // provera da li ima kvarova za koje je institucija odgovorna
            for (Damage damage : business.getResponsibleForDamages()) {
                if (!damage.isFixed())
                    throw new BadRequestException("Business is responsible for some damages that are not fixed yet!");
            }
            // provera da li ima tipova kvarova za koje je institucija odgovorna u nekim zgradama
            if (!business.getResponsibleForDamageTypes().isEmpty())
                throw new BadRequestException("Business is responsible for some damage types in some buildings!");
        }

        business.setDeleted(true);
        business = this.businessService.save(business);

        return new ResponseEntity<>(new BusinessDTO(business), HttpStatus.OK);
    }



    @RequestMapping(
            value = "/{id}/activate",
            method = RequestMethod.PATCH,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Declare a firm/institution active.",
            notes = "You have to provide valid firm/institution ID. " +
                    "Method returns the firm/institution declared active.",
            httpMethod = "PATCH",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully declared firm/institution active", response = BusinessDTO.class),
            @ApiResponse(code = 401, message = "You are not authorized to update the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('INSTITUTION') or hasAuthority('COMPANY')")
    public ResponseEntity declareBusinessActive(
            @ApiParam(value = "The ID of the existing firm/institution resource.", required = true) @PathVariable("id") Long id) {

        Business business = this.businessService.findOneInactive(id);
        this.businessService.checkPermissionForCurrentBusiness(id);

        business.setDeleted(false);

        business = this.businessService.save(business);
        return new ResponseEntity<>(new BusinessDTO(business), HttpStatus.OK);
    }



    @RequestMapping(
            value = "/{id}/edit_accounts",
            method = RequestMethod.PATCH,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Update account list of firm/institution.",
            notes = "You have to provide object with valid accounts. " +
                    "Method returns the firm/institution being updated.",
            httpMethod = "PATCH",
            produces = "application/json",
            consumes = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated firm/institution accounts", response = BusinessDTO.class),
            @ApiResponse(code = 400, message = "Inappropriate firm's/institution's account list object sent in request body"),
            @ApiResponse(code = 401, message = "You are not authorized to update the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('INSTITUTION') or hasAuthority('COMPANY')")
    public ResponseEntity updateBusinessAccounts(
            @ApiParam(value = "The object with firm/institution accounts.", required = true) @Valid @RequestBody BusinessAccountsUpdateDTO businessAccountsUpdateDTO,
            @ApiParam(value = "The ID of the existing firm/institution resource.", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "The object that contains all errors from validation of DTO object") BindingResult errors) {

        Business business = this.businessService.findOne(id);
        this.businessService.checkPermissionForCurrentBusiness(id);

        //predvidjeno je da se sa frontend-a salju Accounts, tako da oni koji su dodati da se dodaju, oni
        //koji se ne nalaze tu, da se obrisu, a oni kojima je lozinka izmenjena, da se uradi update nad njima
        for (LoginDTO loginDTO : businessAccountsUpdateDTO.getLoginAccounts()) {
            boolean oldAccount = false;
            for (Account account : business.getAccounts()) {
                if (account.getUsername().equals(loginDTO.getUsername()))
                    oldAccount = true;
            }
            if (!oldAccount) {
                this.accountService.checkUsername(loginDTO.getUsername());
            }
        }

        List<Account> deletedAccounts = new ArrayList<>();

        //pretraga svih koji su obrisani i izmena lozinke ako je izmenjena
        for (Account account : business.getAccounts()) {
            boolean deleted = true;
            for (LoginDTO loginDTO : businessAccountsUpdateDTO.getLoginAccounts()) {
                if (loginDTO.getUsername().equals(account.getUsername())) {
                    deleted = false;
                    if (!loginDTO.getPassword().equals(account.getPassword())) {
                        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
                        account.setPassword(encoder.encode(loginDTO.getPassword()));
                        this.accountService.save(account);
                    }
                }
            }
            if (deleted)
                deletedAccounts.add(account);
        }
        //brisanje obrisanih
        for (Account account : deletedAccounts) {
            business.getAccounts().remove(account);
            List<AccountAuthority> accountAuthorities = account.getAccountAuthorities();
            for (AccountAuthority accountAuthority : accountAuthorities) {
                if (accountAuthority.getAccount().getUsername().equals(account.getUsername()))
                    this.accountAuthorityService.remove(accountAuthority.getId());
            }
            this.accountService.remove(account.getId());
        }
        Authority authority;
        authority = (business.isIsCompany()) ?
                this.authorityService.findByName("COMPANY") : this.authorityService.findByName("INSTITUTION");
        //dodavanje onih koji su novi
        for (LoginDTO loginDTO : businessAccountsUpdateDTO.getLoginAccounts()) {
            boolean oldAccount = false;
            for (Account account : business.getAccounts()) {
                if (account.getUsername().equals(loginDTO.getUsername()))
                    oldAccount = true;
            }
            if (!oldAccount) {
                Account account = new Account(loginDTO.getUsername(), loginDTO.getPassword());
                account.setBusinessOwner(business);
                AccountAuthority accountAuthority = new AccountAuthority(account, authority);
                account.getAccountAuthorities().add(accountAuthority);
                account = this.accountService.save(account);

                accountAuthority.setAccount(account);
                accountAuthority.setAuthority(authority);
                this.accountAuthorityService.save(accountAuthority);
            }
        }

        return new ResponseEntity<>(new BusinessDTO(business), HttpStatus.OK);
    }



    @RequestMapping(
            value = "/{id}",
            method = RequestMethod.DELETE
    )
    @ApiOperation(
            value = "Delete a firm/institution resource.",
            notes = "You have to provide a valid firm/institution ID in the URL. " +
                    "Once deleted the resource can not be recovered.",
            httpMethod = "DELETE"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully deleted firm/institution"),
            @ApiResponse(code = 401, message = "You are not authorized to delete the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity deleteBusiness(
            @ApiParam(value = "The ID of the existing firm/institution resource.", required = true) @PathVariable("id") Long id) {

        Business business = this.businessService.findOne(id);

        for (Damage damage : business.getDamagesForRepair()) {
            if (!damage.isFixed()) throw new BadRequestException("Business has damages to repair!");
        }

        if (!business.isIsCompany()) {
            // provera da li ima tipova kvarova za koje je institucija odgovorna u nekim zgradama
            if (!business.getResponsibleForDamageTypes().isEmpty())
                throw new BadRequestException("Business is responsible for some damage types in some buildings!");
            // provera da li ima kvarova za koje je institucija odgovorna
            for (Damage damage : business.getResponsibleForDamages()) {
                if (!damage.isFixed())
                    throw new BadRequestException("Business is responsible for some damages that are not fixed yet!");
            }
        }

        if (!business.getDamagesForRepair().isEmpty())
            throw new BadRequestException("Business is responsible for some damages!");

        List<Offer> offers = this.offerService.findAll();
        for (Offer offer : offers) {
            if (offer.getBusiness().getId().equals(business.getId()))
                throw new BadRequestException("Business has sent some offers!");
        }

        for (Account account : business.getAccounts()) {
            // za svaki nalog firme/institucije treba ukloniti povezanu rolu (uvek samo jedna rola)
            this.accountAuthorityService.remove(account.getAccountAuthorities().get(0).getId());
            this.accountService.remove(account.getId());
        }

        this.businessService.remove(business.getId());

        return new ResponseEntity(HttpStatus.OK);
    }



    @RequestMapping(
            value = "/{id}/confirm",
            method = RequestMethod.PATCH,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Confirmation of firm's/institution's registration.",
            notes = "You have to provide valid firm/institution ID in the URL.",
            httpMethod = "PUT",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully confirm firm's/institution's registration", response = BusinessDTO.class),
            @ApiResponse(code = 400, message = "Registration of firm/institution is already confirmed"),
            @ApiResponse(code = 401, message = "You are not authorized to update the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity confirmBusiness(
            @ApiParam(value = "The ID of the existing firm/institution resource.", required = true) @PathVariable("id") Long id) {

        Business business = this.businessService.findOne(id);

        if (business.getConfirmed())
            throw new BadRequestException("Registration of this firm/institution is already confirmed!");

        business.setConfirmed(true);
        business = this.businessService.save(business);

        return new ResponseEntity<>(new BusinessDTO(business), HttpStatus.OK);
    }



    @RequestMapping(
            value = "/firms/address",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a single firm on address.",
            notes = "You have to provide a valid address parameters.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved firm", response = BusinessDTO.class),
            @ApiResponse(code = 400, message = "Some of required parameters in URL is missing"),
            @ApiResponse(code = 401, message = "You are not authorized to view these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    public ResponseEntity getFirmByAddress(
            @ApiParam(value = "The street name of the firm.", required = true) @RequestParam("street") String street,
            @ApiParam(value = "The street number of the firm.", required = true) @RequestParam("number") Integer number,
            @ApiParam(value = "The city name.", required = true) @RequestParam("city_name") String cityName,
            @ApiParam(value = "The city postal number.", required = true) @RequestParam("postal_number") Integer postalNumber) {

        if (street.isEmpty() || number == null || cityName.isEmpty() || postalNumber == null)
            throw new BadRequestException("All fields are required!");

        street = street.replace("%20", " ");
        cityName = cityName.replace("%20", " ");

        Business business = findBussinessFromDTO(new AddressDTO(street, number,
                new CityDTO(cityName, postalNumber)), true);

        return new ResponseEntity<>(new BusinessDTO(business), HttpStatus.OK);
    }



    @RequestMapping(
            value = "/institutions/address",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a single institution on address.",
            notes = "You have to provide a valid address parameters.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved institution", response = BusinessDTO.class),
            @ApiResponse(code = 400, message = "Some of required parameters in URL is missing"),
            @ApiResponse(code = 401, message = "You are not authorized to view these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    public ResponseEntity getInstitutionByAddress(
            @ApiParam(value = "The street name of the institution.", required = true) @RequestParam("street") String street,
            @ApiParam(value = "The street number of the institution.", required = true) @RequestParam("number") Integer number,
            @ApiParam(value = "The city name.", required = true) @RequestParam("city_name") String cityName,
            @ApiParam(value = "The city postal number.", required = true) @RequestParam("postal_number") Integer postalNumber) {

        if (street.isEmpty() || number == null || cityName.isEmpty() || postalNumber == null)
            throw new BadRequestException("All fields are required!");

        street = street.replace("%20", " ");
        cityName = cityName.replace("%20", " ");

        Business business = findBussinessFromDTO(new AddressDTO(street, number,
                new CityDTO(cityName, postalNumber)), false);

        return new ResponseEntity<>(new BusinessDTO(business), HttpStatus.OK);
    }



    @RequestMapping(
            value = "damages/{damageId}/request_not_sent",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a list of all firms and institutions request for repairing the damage wasn't sent to, " +
                    "in page form.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of firms and institutions to whom the offer wasn't sent to", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to view these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    })
    @PreAuthorize("hasAuthority('INSTITUTION') or hasAuthority('TENANT')")
    public ResponseEntity getAllBusinessesRequestNotSentTo(
            @ApiParam(value = "The ID of the existing damage resource.", required = true) @PathVariable("damageId") Long damageId,
            @ApiParam(value = "The page number for getting businesses.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting businesses.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting businesses.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "The sort property for getting businesses.", required = true) @RequestParam String sortProperty) {

        Page<Business> businesses = this.businessService.findAllWhoDidntGetOffer(damageId, Integer.valueOf(pageNumber),
                Integer.valueOf(pageSize), sortDirection, sortProperty);
        ArrayList<BusinessDTO> businessDTOs = new ArrayList<>();

        businesses.getContent().forEach(business ->
                businessDTOs.add(new BusinessDTO(business, businesses.getTotalElements())));

        return new ResponseEntity<>(businessDTOs, HttpStatus.OK);
    }



    @RequestMapping(
            value = "damages/{damageId}/offer_for_damage_sent",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a list of all firms and institutions who've sent the offer for selected damage, in page form.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of firms and institutions who've sent the offer for damage", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to view these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    })
    @PreAuthorize("hasAnyAuthority('TENANT') or hasAnyAuthority('INSTITUTION')")
    public ResponseEntity getAllBusinessesForOffer(
            @ApiParam(value = "The ID of the existing damage resource.", required = true) @PathVariable("damageId") Long damageId,
            @ApiParam(value = "The page number for getting businesses.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting businesses.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting businesses.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "The sort property for getting businesses.", required = true) @RequestParam String sortProperty) {

        Page<Offer> offers = this.offerService.findAllForCurrentDamage(damageId, Integer.valueOf(pageNumber),
                Integer.valueOf(pageSize), sortDirection, sortProperty);

        ArrayList<BusinessDTO> businessDTOS = new ArrayList<>();

        offers.getContent().forEach(offer -> businessDTOS.add(new BusinessDTO(offer.getBusiness(),
                offers.getTotalElements(), new OfferDTO(offer))));

        return new ResponseEntity<>(businessDTOS, HttpStatus.OK);
    }



    private ResponseEntity<?> registerBusiness(Boolean isCompany, BusinessCreateDTO businessCreateDTO) {
        Boolean canRegister = false;
        Boolean isAdmin = false;
        for (GrantedAuthority authority : SecurityContextHolder.getContext().getAuthentication().getAuthorities()) {
            if (authority.getAuthority().equals("ROLE_ANONYMOUS"))
                canRegister = true;
            if (authority.getAuthority().equals(MessageConstants.ADMIN_ROLE))
                isAdmin = true;
        }

        if (!canRegister && !isAdmin)
            throw new ForbiddenException("You need to logout to register or you need to be admin.");

        if (!(businessCreateDTO.getPhoneNumber().matches("[0-9]+"))) {
            throw new BadRequestException("Phone number must only have numbers!");
        }

        for (LoginDTO loginDTO : businessCreateDTO.getLoginAccounts()) {
            this.accountService.checkUsername(loginDTO.getUsername());
        }

        Business business = convertBusinessCreateDTOToBusiness(businessCreateDTO);
        City city = this.cityService.findByName(businessCreateDTO.getAddress().getCity().getName());
        if (city == null) {
            city = convertCityDTOToCity(businessCreateDTO.getAddress().getCity());
            this.cityService.save(city);
        }

        Address address = this.addressService.findByCityStreetNumber(city.getId(), businessCreateDTO.getAddress().getStreet(),
                businessCreateDTO.getAddress().getNumber());
        if (address == null) {
            address = convertAdressDTOToAddress(businessCreateDTO.getAddress());
            address.setCity(city);
            this.addressService.save(address);
        }

        if (isAdmin)
            business.setConfirmed(true);
        else
            business.setConfirmed(false);
        business.setAddress(address);
        business.setIsCompany(isCompany);
        business = this.businessService.save(business);

        Authority authority;
        authority = (isCompany) ?
                this.authorityService.findByName("COMPANY") : this.authorityService.findByName("INSTITUTION");

        for (LoginDTO loginDTO : businessCreateDTO.getLoginAccounts()) {
            Account account = new Account(loginDTO.getUsername(), loginDTO.getPassword());
            AccountAuthority accountAuthority = new AccountAuthority(account, authority);
            account.getAccountAuthorities().add(accountAuthority);
            account.setBusinessOwner(business);
            account = this.accountService.save(account);

            accountAuthority.setAccount(account);
            accountAuthority.setAuthority(authority);
            this.accountAuthorityService.save(accountAuthority);
        }

        return new ResponseEntity<>(new BusinessDTO(business), HttpStatus.CREATED);
    }

    private Address findAddressFromDTO(AddressDTO addressDTO) {
        City city = cityService.findByNameAndPostalNumber(addressDTO.getCity().getName(),
                addressDTO.getCity().getPostalNumber());
        return addressService.findByCityStreetNumber(city.getId(), addressDTO.getStreet(), addressDTO.getNumber());
    }

    private Business findBussinessFromDTO(AddressDTO addressDTO, boolean isCompany) {
        Address address = findAddressFromDTO(addressDTO);
        if (address == null) throw new NotFoundException("Address not found!");
        return this.businessService.findByAddressId(address.getId(), isCompany);
    }
}
