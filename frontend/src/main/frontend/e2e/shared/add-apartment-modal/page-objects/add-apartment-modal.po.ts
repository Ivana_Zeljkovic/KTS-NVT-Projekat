import {by, element, ElementFinder} from "protractor";

export class AddApartmentModalPage {
  // get methods
  getPageTitle(): ElementFinder {
    return element(by.tagName('h4'));
  }

  getNumber(): ElementFinder {
    return element(by.id('numberModal'));
  }

  getFloor(): ElementFinder {
    return element(by.id('floor'));
  }

  getSurface(): ElementFinder {
    return element(by.id('surface'));
  }

  getConfirmButton(): ElementFinder {
    return element(by.css('.btn.btn-confirm'));
  }

  getCloseButton(): ElementFinder {
    return element(by.css('.btn.btn-danger'));
  }

  // set methods
  setNumber(text: number) {
    this.getNumber().sendKeys(text);
  }

  setFloor(text: number) {
    this.getFloor().sendKeys(text);
  }

  setSurface(text: number) {
    this.getSurface().sendKeys(text);
  }

  // get error methods
  getNumberRequiredError(): ElementFinder {
    return element(by.id('numberRequiredError'));
  }

  getFloorRequiredError(): ElementFinder {
    return element(by.id('floorRequiredError'));
  }

  getSurfaceRequiredError(): ElementFinder {
    return element(by.id('surfaceRequiredError'));
  }
}
