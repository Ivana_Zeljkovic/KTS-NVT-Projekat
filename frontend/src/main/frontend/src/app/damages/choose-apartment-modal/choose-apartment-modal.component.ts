import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { BsModalRef } from "ngx-bootstrap";
import { ToasterService } from "angular5-toaster/dist";
// model
import { Apartment } from "../../shared/models/apartment";
// service
import { ApartmentService } from "../../core/services/apartment.service";
import { JwtService } from "../../core/services/jwt.service";
// error
import { AppError } from "../../shared/errors/app-error";
import { ForbiddenError } from "../../shared/errors/forbidden-error";
import { NotFoundError } from "../../shared/errors/not-found-error";
import {PageAndSort} from "../../shared/models/page-and-sort";


@Component({
  selector: 'app-choose-apartment-modal',
  templateUrl: './choose-apartment-modal.component.html',
  styleUrls: ['./choose-apartment-modal.component.css']
})
export class ChooseApartmentModalComponent implements OnInit {
  @Output() apartmentSelected = new EventEmitter<number>();
  apartmentList: Array<Apartment>;
  apartmentId: number;

  // Elements for bootstrap paging
  currentPage: number;
  totalItems: number;
  maxSize: number;
  itemsPerPage: number;

  // Elements for getting data from server
  pageSize: string;
  sortDirection: string;
  sortProperties: string[];


  constructor(public modalRef: BsModalRef, private apartmentService: ApartmentService,
              private jwtService: JwtService, private toasterService: ToasterService) {
    // Sett up for bootstrap paging
    this.currentPage = 1;
    this.maxSize = 3;
    this.itemsPerPage = 4;

    // Sett up for getting damages request
    this.pageSize = '4';
    this.sortDirection = 'ASC';
    this.sortProperties = ['floor', 'number'];
  }

  ngOnInit() {
    this.loadApartmentList();
  }

  pageChanged(event: any) {
    this.currentPage = event.page;
    this.loadApartmentList();
  }

  choose(apartment) {
    this.apartmentSelected.emit(apartment.id);
    this.modalRef.hide();
  }

  private loadApartmentList() {
    let page = new PageAndSort((this.currentPage - 1).toString(), this.pageSize, this.sortDirection, this.sortProperties);
    this.apartmentService.getPage(page, 'buildings', this.jwtService.getBuildingIdFromToken(), 'apartments')
      .subscribe(apartments => {
        if (apartments.length > 0)
          this.totalItems = apartments[0].numberOfElements;
        else this.totalItems = 0;
        this.apartmentList = apartments;
      }, (error: AppError) => {
        if(error instanceof ForbiddenError)
          this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
        else if(error instanceof NotFoundError)
          this.toasterService.pop('error', 'Error', 'Building not found!');
        else {
          this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
          throw error;
        }
      })
  }
}
