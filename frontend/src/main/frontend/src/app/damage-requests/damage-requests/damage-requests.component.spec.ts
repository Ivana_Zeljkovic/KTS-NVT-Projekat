import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DamageRequestsComponent } from './damage-requests.component';

describe('DamageRequestsComponent', () => {
  let component: DamageRequestsComponent;
  let fixture: ComponentFixture<DamageRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DamageRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DamageRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
