package rs.ac.uns.ftn.ktsnvt.web.dto;

import rs.ac.uns.ftn.ktsnvt.model.entity.Tenant;

/**
 * Created by Nikola Garabandic on 17/11/2017.
 */
public class TenantAdminDTO extends TenantDTO {

    private String username;
    private String authorities;

    public TenantAdminDTO(){}

    public TenantAdminDTO(Tenant tenant){
        super(tenant);
        this.username = tenant.getAccount().getUsername();
        StringBuilder sb = new StringBuilder();

        tenant.getAccount().getAccountAuthorities().forEach(accountAuthority ->
                sb.append(accountAuthority.getAuthority().getName()).append("  "));

        authorities = sb.toString();

    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public void setUsername(String username) {
        this.username = username;
    }

    public String getAuthorities() {
        return this.authorities;
    }

    public void setAuthorities(String authorities) {
        this.authorities = authorities;
    }
}
