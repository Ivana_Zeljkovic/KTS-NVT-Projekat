package rs.ac.uns.ftn.ktsnvt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import rs.ac.uns.ftn.ktsnvt.model.entity.City;

/**
 * Created by Katarina Cukurov on 07/11/2017.
 */
public interface CityRepository extends JpaRepository<City, Long> {

    City findByName(String name);

    @Query(value = "SELECT * from City c where c.name = :name and c.postal_number = :postal_code", nativeQuery = true)
    City findByNameAndPostalNumber(@Param("name") String name, @Param("postal_code") int postalCode);
}
