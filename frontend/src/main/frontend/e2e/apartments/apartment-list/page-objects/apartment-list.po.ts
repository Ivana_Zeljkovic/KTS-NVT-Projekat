import {by, element, ElementFinder} from "protractor";

export class ApartmentListPage {
  // get methods
  getHeading(): ElementFinder {
    return element(by.id('headingApartments'));
  }

  getDropDownToggleSort(): ElementFinder {
    return element(by.id('dropDownToggleSort'));
  }

  getSortButton(): ElementFinder {
    return element(by.buttonText('Sort'));
  }

  getDropDownToggleFilter(): ElementFinder {
    return element(by.id('dropDownToggleFilter'));
  }

  getFilterButton(): ElementFinder {
    return element(by.buttonText('Filter'));
  }

  getApartmentsTable(): ElementFinder {
    return element(by.id('apartmentsTable'));
  }

  getPagination(): ElementFinder {
    return element(by.className("col-md-12 div-page"));
  }

  getToaster(): ElementFinder {
    return element(by.tagName("toaster-container"));
  }

  getNoElements(): ElementFinder {
    return element(by.id("noElements"));
  }
}
