package rs.ac.uns.ftn.ktsnvt.service;

import rs.ac.uns.ftn.ktsnvt.model.entity.Council;

/**
 * Created by Ivana Zeljkovic on 16/11/2017.
 */
public interface CouncilService {

    Council save(Council council);

}
