package rs.ac.uns.ftn.ktsnvt.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import rs.ac.uns.ftn.ktsnvt.model.entity.Building;

import java.util.List;

/**
 * Created by Ivana Zeljkovic on 05/11/2017.
 */
public interface BuildingService {

    List<Building> findAll();

    Page<Building> findAll(Pageable page);

    Building findOne(Long id);

    Building save(Building building);

    Building findByAddressId(Long addressId);

    Building findOneByBillboardId(Long billboardId);

    List<Building> findByCityId(Long cityId);

    void remove(Long id);

    Page<Building> findAllByFirm(Long id, Integer pageNumber, Integer pageSize, String sortDirection, String sortProperty);

    Page<Building> getAllBuildings(Integer pageNumber, Integer pageSize,
                                     String sortDirection, String sortProperty);
}
