import {by, element, ElementFinder} from "protractor";

export class AdminRegistrationPage {
  // get methods
  getPageTitle(): ElementFinder {
    return element(by.css('.panel-title'));
  }

  getFirstNameInput(): ElementFinder {
    return element(by.id('firstName'));
  }

  getLastNameInput(): ElementFinder {
    return element(by.id('lastName'));
  }

  getUsernameInput(): ElementFinder {
    return element(by.id('username'));
  }

  getPasswordInput(): ElementFinder {
    return element(by.id('password'));
  }

  getConfirmPasswordInput(): ElementFinder {
    return element(by.id('confirmPassword'));
  }

  getSingUpButton(): ElementFinder {
    return element(by.buttonText('Register'));
  }

  // set methods
  setFirstName(text: string) {
    this.getFirstNameInput().sendKeys(text);
  }

  setLastName(text: string) {
    this.getLastNameInput().sendKeys(text);
  }

  setUsername(text: string) {
    this.getUsernameInput().sendKeys(text);
  }

  setPassword(text: string) {
    this.getPasswordInput().sendKeys(text);
  }

  setConfirmPassword(text: string) {
    this.getConfirmPasswordInput().sendKeys(text);
  }

  // get error methods
  getFirstNameRequiredError(): ElementFinder {
    return element(by.id('firstNameRequiredError'))
  }

  getFirstNameMinimumLengthError(): ElementFinder {
    return element(by.id('firstNameMinimumLengthError'));
  }

  getLastNameRequiredError(): ElementFinder {
    return element(by.id('lastNameRequiredError'));
  }

  getLastNameMinimumLengthError(): ElementFinder {
    return element(by.id('lastNameMinimumLengthError'));
  }

  getUsernameAvailable(): ElementFinder {
    return element(by.id('usernameIsAvailable'));
  }

  getUsernameRequiredError(): ElementFinder {
    return element(by.id('usernameRequiredError'));
  }

  getUsernameContainSpaceError(): ElementFinder {
    return element(by.id('usernameContainSpaceError'));
  }

  getUsernameTakenError(): ElementFinder {
    return element(by.id('usernameTakenError'));
  }

  getPasswordRequiredError(): ElementFinder {
    return element(by.id('passwordRequiredError'));
  }

  getPasswordMinimumLengthError(): ElementFinder {
    return element(by.id('passwordMinimumLengthError'));
  }

  getConfirmPasswordRequiredError(): ElementFinder {
    return element(by.id('confirmPasswordRequiredError'));
  }

  getPasswordDoNotMatchError(): ElementFinder {
    return element(by.id('passwordDoNotMatchError'));
  }

}
