package rs.ac.uns.ftn.ktsnvt.web.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ivana Zeljkovic on 19/01/2018.
 */
public class QuestionResultsDTO implements Serializable {

    private Long questionId;
    private List<AnswerResultDTO> answers;
    private String content;

    public QuestionResultsDTO() {
        this.answers = new ArrayList<>();
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    public List<AnswerResultDTO> getAnswers() {
        return answers;
    }

    public void setAnswers(List<AnswerResultDTO> answers) {
        this.answers = answers;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
