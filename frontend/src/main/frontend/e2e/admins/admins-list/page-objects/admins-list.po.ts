import {by, element, ElementArrayFinder, ElementFinder} from "protractor";

export class AdminsListPage {
  // get methods
  getPageTitle(): ElementFinder {
    return element(by.id('heading'));
  }

  getDropdownToggle(): ElementFinder {
    return element(by.id('dropdownButton'));
  }

  getSortButton(): ElementFinder {
    return element(by.buttonText("Sort"));
  }

  getAddButton(): ElementFinder {
    return element(by.className("btn btn-add btn-warning"));
  }

  getAdminsTable(): ElementFinder {
    return element(by.id("adminsTable"));
  }

  getPagination(): ElementFinder {
    return element(by.className("col-md-12 div-page"));
  }

  getNoElements(): ElementFinder {
    return element(by.name("noElements"));
  }

  getToaster(): ElementFinder {
    return element(by.tagName("toaster-container"));
  }
}
