package rs.ac.uns.ftn.ktsnvt.repository.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.model.entity.Questionnaire;
import rs.ac.uns.ftn.ktsnvt.repository.QuestionnaireRepository;

import static org.junit.Assert.*;

/**
 * Created by Ivana Zeljkovic on 07/12/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class QuestionnaireRepositoryIntegrationTest {

    @Autowired
    private QuestionnaireRepository questionnaireRepository;


    @Test
    @Transactional
    public void testFindOneByIdAndBillboardId() {
        /*
        * Test proverava ispravnost rada metode findOneByIdAndBillboardId repozitorijuma QuestionnaireRepository
        * (dobavljanje ankete na osnovu njenog ID-a i ID-a zgrade kojoj bi ta anketa trebala da pripada)
        * */

        /*
        anketa sa ID-em = 101 pripada zgradi sa ID-em = 100 (bilbord ID = 100) i ima sledece podatke:
            id = 101,
            meeting_item_id = 104
        */
        Questionnaire questionnaire = this.questionnaireRepository.findOneByIdAndBillboardId(Long.valueOf(101), Long.valueOf(100));

        assertNotNull(questionnaire);
        assertEquals(Long.valueOf(101), questionnaire.getId());
        assertEquals(Long.valueOf(104), questionnaire.getMeetingItem().getId());
    }
}
