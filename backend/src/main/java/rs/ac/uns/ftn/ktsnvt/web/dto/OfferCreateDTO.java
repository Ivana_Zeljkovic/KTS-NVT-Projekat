package rs.ac.uns.ftn.ktsnvt.web.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by Katarina Cukurov on 24/11/2017.
 */
public class OfferCreateDTO implements Serializable {

    @NotNull
    private float price;

    public OfferCreateDTO() { }

    public OfferCreateDTO(float price) {
        this.price = price;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
