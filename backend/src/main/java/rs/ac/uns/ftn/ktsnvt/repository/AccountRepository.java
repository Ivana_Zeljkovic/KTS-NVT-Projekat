package rs.ac.uns.ftn.ktsnvt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.ktsnvt.model.entity.Account;

/**
 * Created by Ivana Zeljkovic on 05/11/2017.
 */
public interface AccountRepository extends JpaRepository<Account, Long> {

    Account findByUsername(String username);

}
