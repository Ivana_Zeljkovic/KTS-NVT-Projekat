import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuildingRegistrationComponent } from './building-registration.component';

describe('BuildingRegistrationComponent', () => {
  let component: BuildingRegistrationComponent;
  let fixture: ComponentFixture<BuildingRegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuildingRegistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuildingRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
