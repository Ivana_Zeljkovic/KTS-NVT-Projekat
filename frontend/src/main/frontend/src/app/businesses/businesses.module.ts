import { NgModule } from '@angular/core';
// module
import { SharedModule } from "../shared/shared.module";
import { BusinessesRouterModule } from "./businesses-router.module";
// component
import { BusinessesListComponent } from './businesses-list/businesses-list.component';
import { BusinessComponent } from './business/business.component';


@NgModule({
  imports: [
    SharedModule,
    BusinessesRouterModule
  ],
  declarations: [
    BusinessesListComponent,
    BusinessComponent
  ]
})
export class BusinessesModule { }
