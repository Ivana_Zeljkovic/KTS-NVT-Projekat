package rs.ac.uns.ftn.ktsnvt.web.dto;


import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by Ivana Zeljkovic on 13/11/2017.
 */
public class MeetingItemCreateDTO implements Serializable {

    @NotNull
    @NotEmpty
    private String title;
    @NotNull
    @NotEmpty
    private String content;
    @Valid
    private QuestionnaireCreateDTO questionnaireCreate;
    private Long damageId;

    public MeetingItemCreateDTO() { }

    public MeetingItemCreateDTO(String title, String content, QuestionnaireCreateDTO questionnaireCreate, Long damageId) {
        this.title = title;
        this.content = content;
        this.questionnaireCreate = questionnaireCreate;
        this.damageId = damageId;
    }

    public String getTitle() { return title; }

    public void setTitle(String title) { this.title = title; }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public QuestionnaireCreateDTO getQuestionnaireCreate() {
        return questionnaireCreate;
    }

    public void setQuestionnaireCreate(QuestionnaireCreateDTO questionnaireCreate) {
        this.questionnaireCreate = questionnaireCreate;
    }

    public Long getDamageId() { return damageId; }

    public void setDamageId(Long damageId) { this.damageId = damageId; }
}
