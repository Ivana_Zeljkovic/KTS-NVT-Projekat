import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooseOwnerModalComponent } from './choose-owner-modal.component';

describe('ChooseOwnerModalComponent', () => {
  let component: ChooseOwnerModalComponent;
  let fixture: ComponentFixture<ChooseOwnerModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChooseOwnerModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseOwnerModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
