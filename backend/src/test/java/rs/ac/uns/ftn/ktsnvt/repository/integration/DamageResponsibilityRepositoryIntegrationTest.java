package rs.ac.uns.ftn.ktsnvt.repository.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.model.entity.*;
import rs.ac.uns.ftn.ktsnvt.repository.*;

import static org.junit.Assert.*;

/**
 * Created by Ivana Zeljkovic on 28/11/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class DamageResponsibilityRepositoryIntegrationTest {

    @Autowired
    private DamageResponsibilityRepository damageResponsibilityRepository;

    @Autowired
    private BuildingRepository buildingRepository;

    @Autowired
    private BusinessRepository businessRepository;


    @Test
    @Transactional
    public void testFindOneByDamageTypeAndTenantAndBuilding() {
        /*
         * Test proverava ispravnost rada metode findOneByDamageTypeAndTenantAndBuilding repozitorijuma DamageResponsibilityRepository
         * (pronalazenje odgovornosti za dati tip kvara, datog stanara i datu zgradu)
         * u slucaju kada postoji odgovornost za zadati tip kvara, stanara i zgradu
         * */

        Building building = this.buildingRepository.findOne(new Long(100));

        // stanar sa ID-em = 101 je odgovoran za tip kvara sa ID-em = 1 u zgradi sa ID-em = 100
        DamageResponsibility damageResponsibility = this.damageResponsibilityRepository.findOneByDamageTypeAndTenantAndBuilding(
                new Long(1), new Long(101), new Long(100));

        assertNotNull(damageResponsibility);
        assertEquals("Katarina", damageResponsibility.getResponsibleTenant().getFirstName());
        assertEquals("Cukurov", damageResponsibility.getResponsibleTenant().getLastName());
        assertEquals("PLUMBING_DAMAGE", damageResponsibility.getDamageType().getName());
        assertEquals(building.getAddress(), damageResponsibility.getBuilding().getAddress());
    }

    @Test
    @Transactional
    public void testFindOneByDamageTypeAndTenantAndBuildingNonexistent() {
        /*
         * Test proverava ispravnost rada metode findOneByDamageTypeAndTenantAndBuilding repozitorijuma DamageResponsibilityRepository
         * (pronalazenje odgovornosti za dati tip kvara, datog stanara i datu zgradu)
         * u slucaju kada ne postoji odgovornost za zadati tip kvara, stanara i zgradu
         * */

        // stanar sa ID-em = 101 nije odgovoran za tip kvara sa ID-em = 2 u zgradi sa ID-em = 100
        DamageResponsibility damageResponsibility = this.damageResponsibilityRepository.findOneByDamageTypeAndTenantAndBuilding(
                new Long(2), new Long(101), new Long(100));

        assertNull(damageResponsibility);
    }

    @Test
    @Transactional
    public void testFindOneByDamageTypeAndBuilding() {
        /*
         * Test proverava ispravnost rada metode findOneByDamageTypeAndBuilding repozitorijuma DamageResponsibilityRepository
         * (pronalazenje odgovornosti za dati tip kvara i datu zgradu)
         * u slucaju kada postoji odgovornost za zadati tip kvara i zgradu
         * */

        Building building = this.buildingRepository.findOne(new Long(100));

        // za tip kvara sa ID-em = 1 u zgradi sa ID-em = 100 postoji osgovorna osoba/institucija
        DamageResponsibility damageResponsibility = this.damageResponsibilityRepository.findOneByDamageTypeAndBuilding(
                new Long(1), new Long(100));

        assertNotNull(damageResponsibility);
        assertEquals("PLUMBING_DAMAGE", damageResponsibility.getDamageType().getName());
        assertEquals(building.getAddress(), damageResponsibility.getBuilding().getAddress());
    }

    @Test
    @Transactional
    public void testFindOneByDamageTypeAndBuildingNonexistent() {
        /*
         * Test proverava ispravnost rada metode findOneByDamageTypeAndBuilding repozitorijuma DamageResponsibilityRepository
         * (pronalazenje odgovornosti za dati tip kvara i datu zgradu)
         * u slucaju kada ne postoji odgovornost za zadati tip kvara i zgradu
         * */

        // ne postoji odgovorna osoba/institucija za tip kvara sa ID-em = 1 u zgradi sa ID-em = 101
        DamageResponsibility damageResponsibility = this.damageResponsibilityRepository.findOneByDamageTypeAndBuilding(
                new Long(1), new Long(101));

        assertNull(damageResponsibility);
    }

    @Test
    @Transactional
    public void testFindOneByDamageTypeAndInstitutionAndBuilding() {
        /*
         * Test proverava ispravnost rada metode findOneByDamageTypeAndInstitutionAndBuilding repozitorijuma DamageResponsibilityRepository
         * (pronalazenje odgovornosti za dati tip kvara, datu instituciju i datu zgradu)
         * u slucaju kada postoji odgovornost za zadati tip kvara, instituciju i zgradu
         * */

        Building building = this.buildingRepository.findOne(new Long(100));
        Business institution = this.businessRepository.findOne(new Long(102));

        // institucija sa ID-em = 102 je odgovorna za tip kvara sa ID-em = 1 u zgradi sa ID-em = 100
        DamageResponsibility damageResponsibility = this.damageResponsibilityRepository.findOneByDamageTypeAndInstitutionAndBuilding(
                new Long(1), new Long(102), new Long(100));

        assertNotNull(damageResponsibility);
        assertEquals("PLUMBING_DAMAGE", damageResponsibility.getDamageType().getName());
        assertEquals(building.getAddress(), damageResponsibility.getBuilding().getAddress());
        assertFalse(damageResponsibility.getResponsibleInstitution().isIsCompany());
        assertEquals(institution.getName(), damageResponsibility.getResponsibleInstitution().getName());
        assertEquals(institution.getAddress(), damageResponsibility.getResponsibleInstitution().getAddress());
    }

    @Test
    @Transactional
    public void testFindOneByDamageTypeAndInstitutionAndBuildingNonexistent() {
        /*
         * Test proverava ispravnost rada metode findOneByDamageTypeAndInstitutionAndBuilding repozitorijuma DamageResponsibilityRepository
         * (pronalazenje odgovornosti za dati tip kvara, datu instituciju i datu zgradu)
         * u slucaju kada ne postoji odgovornost za zadati tip kvara, instituciju i zgradu
         * */

        // institucija sa ID-em = 102 nije odgovorna za tip kvara sa ID-em = 2 u zgradi sa ID-em = 100
        DamageResponsibility damageResponsibility = this.damageResponsibilityRepository.findOneByDamageTypeAndInstitutionAndBuilding(
                new Long(2), new Long(102), new Long(100));

        assertNull(damageResponsibility);
    }
}
