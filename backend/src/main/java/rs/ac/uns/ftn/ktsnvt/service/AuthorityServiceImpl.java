package rs.ac.uns.ftn.ktsnvt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Authority;
import rs.ac.uns.ftn.ktsnvt.repository.AuthorityRepository;

/**
 * Created by Nikola Garabandic on 06/11/2017.
 */
@Service
public class AuthorityServiceImpl implements AuthorityService {

    @Autowired
    private AuthorityRepository authorityRepository;


    @Override
    @Transactional(readOnly = true)
    public Authority findByName(String name) {
        Authority authority = this.authorityRepository.findByName(name);
        if(authority == null) throw new NotFoundException("Authority with name: " + name + " not found!");
        return authority;
    }
}
