package rs.ac.uns.ftn.ktsnvt.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;
import rs.ac.uns.ftn.ktsnvt.controller.util.TestUtil;
import rs.ac.uns.ftn.ktsnvt.web.dto.*;

import javax.annotation.PostConstruct;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Created by Nikola Garabandic on 06/12/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class DamageControllerIntegrationTest {

    private static final String URL_PREFIX = "/api";

    private MediaType contentType = new MediaType(
            MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    private MockMvc mockMvc;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja firme
    private String accessTokenFirm_1;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja institucije
    private String accessTokenInstitution_1, accessTokenInstitution_2;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja stanara
    private String accessTokenTenant_1, accessTokenTenant_2, accessTokenTenant_3, accessTokenTenant_4, accessTokenTenant_5;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja stanara koji je ujedno i predsednik skupstine stanara
    private String accessTokenPresident;
    private MultiValueMap<String, String> params;


    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private TestRestTemplate testRestTemplate;


    @PostConstruct
    public void setup() {
        this.mockMvc = MockMvcBuilders.
                webAppContextSetup(webApplicationContext).apply(springSecurity()).build();
    }

    // pre izvrsavanja testa, neophodno je izvrsiti logovanje u aplikaciju da bismo dobili token
    @Before
    public void login() throws Exception {
        // logovanje firme
        LoginDTO loginDTOFirm = new LoginDTO("firm2", "firm2");

        ResponseEntity<TokenDTO> responseEntityFirm =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOFirm,
                        TokenDTO.class);
        // preuzmemo token za logovanu firmu iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenFirm_1 = responseEntityFirm.getBody().getValue();


        // logovanje institucije
        LoginDTO loginDTOInstitution = new LoginDTO("inst2", "inst2");

        ResponseEntity<TokenDTO> responseEntityInstitution =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOInstitution,
                        TokenDTO.class);
        // preuzmemo token za logovanu instituciju iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenInstitution_1 = responseEntityInstitution.getBody().getValue();


        // logovanje institucije 2
        LoginDTO loginDTOInstitution2 = new LoginDTO("inst1", "inst1");

        ResponseEntity<TokenDTO> responseEntityInstitution2 =
            testRestTemplate.postForEntity("/api/login",
                loginDTOInstitution2,
                TokenDTO.class);
        // preuzmemo token za logovanu instituciju iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenInstitution_2 = responseEntityInstitution2.getBody().getValue();


        // logovanje stanara
        LoginDTO loginDTOTenant = new LoginDTO("jasmina", "jasmina");

        ResponseEntity<TokenDTO> responseEntityTenant =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOTenant,
                        TokenDTO.class);
        // preuzmemo token za logovanog stanara iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenTenant_1 = responseEntityTenant.getBody().getValue();


        // logovanje stanara 2
        LoginDTO loginDTOTenant2 = new LoginDTO("kaca", "kaca");

        ResponseEntity<TokenDTO> responseEntityTenant2 =
            testRestTemplate.postForEntity("/api/login",
                loginDTOTenant2,
                TokenDTO.class);
        // preuzmemo token za logovanog stanara iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenTenant_2 = responseEntityTenant2.getBody().getValue();


        // logovanje stanara 3
        LoginDTO loginDTOTenant3 = new LoginDTO("jovanka", "jovanka");

        ResponseEntity<TokenDTO> responseEntityTenant3 =
            testRestTemplate.postForEntity("/api/login",
                loginDTOTenant3,
                TokenDTO.class);
        // preuzmemo token za logovanog stanara iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenTenant_3 = responseEntityTenant3.getBody().getValue();


        // logovanje stanara 4
        LoginDTO loginDTOTenant4 = new LoginDTO("leontina", "leontina");

        ResponseEntity<TokenDTO> responseEntityTenant4 =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOTenant4,
                        TokenDTO.class);
        // preuzmemo token za logovanog stanara iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenTenant_4 = responseEntityTenant4.getBody().getValue();


        // logovanje stanara 5
        LoginDTO loginDTOTenant5 = new LoginDTO("nestor", "nestor");

        ResponseEntity<TokenDTO> responseEntityTenant5 =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOTenant5,
                        TokenDTO.class);
        // preuzmemo token za logovanog stanara iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenTenant_5 = responseEntityTenant5.getBody().getValue();

        // logovanje stanara koji je ujedno i predsednik skupstine stanara
        LoginDTO loginDTOPresident = new LoginDTO("kaca", "kaca");

        ResponseEntity<TokenDTO> responseEntityPresident =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOPresident,
                        TokenDTO.class);
        // preuzmemo token za logovanog stanara koji je ujedno i predsednik skupstine stanara iz zaglavlja odgovora
        // i setujemo ga na globalni nivo, jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenPresident = responseEntityPresident.getBody().getValue();

        // za pageable atribut
        params = TestUtil.getParams("0","10", "ASC", "urgent", "date");
    }


    // TESTOVI za selectFirmForFixingDamage
    @Test
    @Transactional
    @Rollback(true)
    public void testSelectFirmForFixingDamage() throws Exception{
        /*
        * Test proverava ispravnost rada metode selectFirmForFixingDamage kontrolera DamageController
        * (biranje firme za popravljanje kvara)
        * u slucaju kada su je prosledjen objekat sa validnim podacima kada je ulogovana odgovorna institucija
        * */

        SelectedBusinessForDamageRepairDTO selectedBusiness = new SelectedBusinessForDamageRepairDTO(new Long(102), new Long(104));
        String token = TestUtil.json(selectedBusiness);

        // kvar sa ID-em = 105 u zgradi sa ID-em = 101 ima odgovornu instituciju sa ID-em =102
        mockMvc.perform(patch(URL_PREFIX + "/buildings/101/damages/105/firm_or_institution_for_fixing")
                .header("Authentication-Token", this.accessTokenInstitution_2)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType));
    }

    @Test
    @Transactional
    @Rollback(true)
    public void testSelectFirmForFixingDamageResponsiblePerson() throws Exception{
        /*
        * Test proverava ispravnost rada metode selectFirmForFixingDamage kontrolera DamageController
        * (biranje firme za popravljanje kvara)
        * u slucaju kada su je prosledjen objekat sa validnim podacima kada je ulogovana odgovorna osoba (stanar)
        * */

        SelectedBusinessForDamageRepairDTO selectedBusiness = new SelectedBusinessForDamageRepairDTO(new Long(100), new Long(101));
        String token = TestUtil.json(selectedBusiness);

        // kvar sa ID-em = 101 u zgradi sa ID-em = 100 ima odgovornu osobu sa ID-em = 101
        mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/101/firm_or_institution_for_fixing")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType));
    }

    @Test
    public void testSelectFirmForFixingDamageUnauthenticatedUser() throws Exception{
        /*
        * Test proverava ispravnost rada metode selectFirmForFixingDamage kontrolera DamageController
        * (biranje firme za popravljanje kvara)
        * u slucaju kada akciju izvrsava nelogovan korisnik
        * */

        SelectedBusinessForDamageRepairDTO selectedBusiness = new SelectedBusinessForDamageRepairDTO(new Long(100), new Long(101));
        String token = TestUtil.json(selectedBusiness);

        mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/101/firm_or_institution_for_fixing")
                .contentType(contentType)
                .content(token))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSelectFirmForFixingDamageUnauthorizedUser() throws Exception{
        /*
        * Test proverava ispravnost rada metode selectFirmForFixingDamage kontrolera DamageController
        * (biranje firme za popravljanje kvara)
        * u slucaju kada akciju izvrsava ulogovani korisnik koji nema ulogu stanara ili institucije
        * */

        SelectedBusinessForDamageRepairDTO selectedBusiness = new SelectedBusinessForDamageRepairDTO(new Long(100), new Long(101));
        String token = TestUtil.json(selectedBusiness);

        mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/101/firm_or_institution_for_fixing")
                .header("Authentication-Token", this.accessTokenFirm_1)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSelectFirmForFixingDamageNonExistingBuildingId() throws Exception{
        /*
        * Test proverava ispravnost rada metode selectFirmForFixingDamage kontrolera DamageController
        * (biranje firme za popravljanje kvara)
        * u slucaju kada ne postoji zgrada sa zadatim ID-em
        * */

        SelectedBusinessForDamageRepairDTO selectedBusiness = new SelectedBusinessForDamageRepairDTO(new Long(102), new Long(102));
        String token = TestUtil.json(selectedBusiness);

        // ne postoji zgrada sa ID-em = 200
        mockMvc.perform(patch(URL_PREFIX + "/buildings/200/damages/101/firm_or_institution_for_fixing")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testSelectFirmForFixingDamageNonExistingDamageId() throws Exception{
        /*
        * Test proverava ispravnost rada metode selectFirmForFixingDamage kontrolera DamageController
        * (biranje firme za popravljanje kvara)
        * u slucaju kada ne postoji kvar sa zadatim ID-em
        * */

        SelectedBusinessForDamageRepairDTO selectedBusiness = new SelectedBusinessForDamageRepairDTO(new Long(102), new Long(102));
        String token = TestUtil.json(selectedBusiness);

        // ne postoji kvar sa ID-em = 200
        mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/200/firm_or_institution_for_fixing")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testSelectFirmForFixingDamageInvalidBuilding() throws Exception{
        /*
        * Test proverava ispravnost rada metode selectFirmForFixingDamage kontrolera DamageController
        * (biranje firme za popravljanje kvara)
        * u slucaju kada kvar sa zadatim ID-em ne pripada zgradi sa zadatim ID-em
        * */

        SelectedBusinessForDamageRepairDTO selectedBusiness = new SelectedBusinessForDamageRepairDTO(new Long(102), new Long(102));
        String token = TestUtil.json(selectedBusiness);

        // kvar sa ID-em = 105 pripada zgradi sa ID-em = 101 (a ne sa ID-em = 100)
        mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/105/firm_or_institution_for_fixing")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testSelectFirmForFixingDamageHasAlreadySelectedFirm() throws Exception{
        /*
        * Test proverava ispravnost rada metode selectFirmForFixingDamage kontrolera DamageController
        * (biranje firme za popravljanje kvara)
        * u slucaju kada kvar sa zadatim ID-em vec ima izabranu firmu/instituciju koja je zaduzena za njegovo otklanjanje
        * */

        SelectedBusinessForDamageRepairDTO selectedBusiness = new SelectedBusinessForDamageRepairDTO(new Long(101), new Long(103));
        String token = TestUtil.json(selectedBusiness);

        // kvar sa ID-em = 103 u zgradi sa ID-em = 100 ima izabranu firmu sa ID-em = 101
        mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/103/firm_or_institution_for_fixing")
                .header("Authentication-Token", this.accessTokenInstitution_1)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testSelectFirmForFixingDamageNonExistingFirm() throws Exception{
        /*
        * Test proverava ispravnost rada metode selectFirmForFixingDamage kontrolera DamageController
        * (biranje firme za popravljanje kvara)
        * u slucaju kada firma/institucija sa ID-em koji je poslat u DTO-u ne postoji
        * */

        SelectedBusinessForDamageRepairDTO selectedBusiness = new SelectedBusinessForDamageRepairDTO(new Long(200), new Long(102));
        String token = TestUtil.json(selectedBusiness);

        // ne postoji firma/institucija sa ID-em = 200
        mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/101/firm_or_institution_for_fixing")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(token))
               .andExpect(status().isNotFound());
    }

    @Test
    public void testSelectFirmForFixingDamageNoResponsibleInstitution() throws Exception{
        /*
        * Test proverava ispravnost rada metode selectFirmForFixingDamage kontrolera DamageController
        * (biranje firme za popravljanje kvara)
        * u slucaju kada trenutno logovana institucija, ali zadati kvar nema odgovornu instituciju
        * */

        SelectedBusinessForDamageRepairDTO selectedBusiness = new SelectedBusinessForDamageRepairDTO(new Long(102), new Long(100));
        String token = TestUtil.json(selectedBusiness);

        // kvar sa ID-em = 100 u zgradi sa ID-em = 100 nema odgovornu instituciju
        mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/100/firm_or_institution_for_fixing")
                .header("Authentication-Token", this.accessTokenInstitution_2)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSelectFirmForFixingDamageNotResponsibleInstitution() throws Exception{
        /*
        * Test proverava ispravnost rada metode selectFirmForFixingDamage kontrolera DamageController
        * (biranje firme za popravljanje kvara)
        * u slucaju kada je trenutno logovana institucija, ali ona nije ujedno i odgovorna institucija za zadati kvar
        * */

        SelectedBusinessForDamageRepairDTO selectedBusiness = new SelectedBusinessForDamageRepairDTO(new Long(102), new Long(102));
        String token = TestUtil.json(selectedBusiness);

        // kvar sa ID-em = 105 u zgradi sa ID-em = 101 ima odgovornu instituciju sa ID-em = 102
        mockMvc.perform(patch(URL_PREFIX + "/buildings/101/damages/105/firm_or_institution_for_fixing")
                .header("Authentication-Token", this.accessTokenInstitution_1)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSelectFirmForFixingDamageTenantWithoutApartment() throws Exception{
        /*
        * Test proverava ispravnost rada metode selectFirmForFixingDamage kontrolera DamageController
        * (biranje firme za popravljanje kvara)
        * u slucaju kada trenutno ulogovani stanar nije povezan ni sa jednim stanom
        * */

        SelectedBusinessForDamageRepairDTO selectedBusiness = new SelectedBusinessForDamageRepairDTO(new Long(102), new Long(102));
        String token = TestUtil.json(selectedBusiness);

        // stanar sa ID-em = 108 nije povezan ni sa jednim stanom
        mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/104/firm_or_institution_for_fixing")
                .header("Authentication-Token", this.accessTokenTenant_3)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testSelectFirmForFixingDamageTenantFromInvalidBuilding() throws Exception{
        /*
        * Test proverava ispravnost rada metode selectFirmForFixingDamage kontrolera DamageController
        * (biranje firme za popravljanje kvara)
        * u slucaju kada trenutno ulogovani stanar ne zivi u zgradi sa zadatim ID-em
        * */

        SelectedBusinessForDamageRepairDTO selectedBusiness = new SelectedBusinessForDamageRepairDTO(new Long(102), new Long(102));
        String token = TestUtil.json(selectedBusiness);

        // stanar sa ID-em = 101 zivi u zgradi sa ID-em = 100 (a ne sa ID-em = 101)
        mockMvc.perform(patch(URL_PREFIX + "/buildings/101/damages/105/firm_or_institution_for_fixing")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSelectFirmForFixingDamageNoResponsiblePerson() throws Exception{
        /*
        * Test proverava ispravnost rada metode selectFirmForFixingDamage kontrolera DamageController
        * (biranje firme za popravljanje kvara)
        * u slucaju kada je trenutno ulogovan stanar ali zadati kvar nema odgovornu osobu
        * */

        SelectedBusinessForDamageRepairDTO selectedBusiness = new SelectedBusinessForDamageRepairDTO(new Long(102), new Long(102));
        String token = TestUtil.json(selectedBusiness);

        // kvar sa ID-em = 105 u zgradi sa ID-em = 101 nema odgovornu osobu
        mockMvc.perform(patch(URL_PREFIX + "/buildings/101/damages/105/firm_or_institution_for_fixing")
                .header("Authentication-Token", this.accessTokenTenant_4)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSelectFirmForFixingDamageNotResponsiblePerson() throws Exception{
        /*
        * Test proverava ispravnost rada metode selectFirmForFixingDamage kontrolera DamageController
        * (biranje firme za popravljanje kvara)
        * u slucaju kada je trenutno ulogovan stanar ali on nije ujedno i odgovorna osoba za zadati kvar
        * */

        SelectedBusinessForDamageRepairDTO selectedBusiness = new SelectedBusinessForDamageRepairDTO(new Long(100), new Long(100));
        String token = TestUtil.json(selectedBusiness);

        // kvar sa ID-em = 100 u zgradi sa ID-em 100 ima odgovornu osobu sa ID-em = 104
        mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/100/firm_or_institution_for_fixing")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSelectFirmForFixingDamageNonexistentOffer() throws Exception{
        /*
        * Test proverava ispravnost rada metode selectFirmForFixingDamage kontrolera DamageController
        * (biranje firme za popravljanje kvara)
        * u slucaju kada je dolazni DTO  objekat nevalidan: sadrzi sva polja - ne postoji nijedna ponuda sa ID-em offerId
        * */

        SelectedBusinessForDamageRepairDTO selectedBusiness = new SelectedBusinessForDamageRepairDTO(new Long(102), new Long(402));
        String token = TestUtil.json(selectedBusiness);

        // kvar sa ID-em = 104 u zgradi sa ID-em = 100 ima odgovornu instituciju sa ID-em = 102
        mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/104/firm_or_institution_for_fixing")
                .header("Authentication-Token", this.accessTokenInstitution_2)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testSelectFirmForFixingDamageInvalidOffer() throws Exception{
        /*
        * Test proverava ispravnost rada metode selectFirmForFixingDamage kontrolera DamageController
        * (biranje firme za popravljanje kvara)
        * u slucaju kada je dolazni DTO objekat validan - sadrzi sva polja, ali ponuda sa ID-em offerId ne pripada zadatom kvaru
        * */

        SelectedBusinessForDamageRepairDTO selectedBusiness = new SelectedBusinessForDamageRepairDTO(new Long(102), new Long(100));
        String token = TestUtil.json(selectedBusiness);

        // kvar sa ID-em = 104 u zgradi sa ID-em = 100 ima odgovornu instituciju sa ID-em = 102
        mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/104/firm_or_institution_for_fixing")
                .header("Authentication-Token", this.accessTokenInstitution_2)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testSelectFirmForFixingDamageDifferentCompanyInDTOAndOffer() throws Exception{
        /*
        * Test proverava ispravnost rada metode selectFirmForFixingDamage kontrolera DamageController
        * (biranje firme za popravljanje kvara)
        * u slucaju kada je dolazni DTO objekat nevalidan: firma sa ID-em id (iz DTO-a) i firma koja se nalazi u offer-u se razlikuju
        * */

        SelectedBusinessForDamageRepairDTO selectedBusiness = new SelectedBusinessForDamageRepairDTO(new Long(101), new Long(102));
        String token = TestUtil.json(selectedBusiness);

        // kvar sa ID-em = 104 u zgradi sa ID-em = 100 ima odgovornu instituciju sa ID-em = 102 i ponudu sa ID-em 102 (ID firme = 102)
        mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/104/firm_or_institution_for_fixing")
                .header("Authentication-Token", this.accessTokenInstitution_2)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testSelectFirmForFixingDamageMissingId() throws Exception{
        /*
        * Test proverava ispravnost rada metode selectFirmForFixingDamage kontrolera DamageController
        * (biranje firme za popravljanje kvara)
        * u slucaju kada je dolazni DTO objekat nevalidan - nedostaje polje id
        * */

        String token = "{\"offerId\":102}";

        // kvar sa ID-em = 104 u zgradi sa ID-em = 100 ima odgovornu instituciju sa ID-em = 102
        mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/104/firm_or_institution_for_fixing")
                .header("Authentication-Token", this.accessTokenInstitution_2)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testSelectFirmForFixingDamageInappropriateTypeOfId() throws Exception{
        /*
        * Test proverava ispravnost rada metode selectFirmForFixingDamage kontrolera DamageController
        * (biranje firme za popravljanje kvara)
        * u slucaju kada je dolazni DTO objekat nevalidan - sadrzi sva neophodna polja, ali je tip polja id nevalidan
        * */

        String token = "{\"id\":\"string\", \"offerId\":102}";

        // kvar sa ID-em = 104 u zgradi sa ID-em = 100 ima odgovornu instituciju sa ID-em = 102
        mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/104/firm_or_institution_for_fixing")
                .header("Authentication-Token", this.accessTokenInstitution_2)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    //TESTOVI za sendDamageMailToBusinesses
    @Test
    @Transactional
    @Rollback(true)
    public void testSendDamageMailToBusinesses () throws Exception{
        /*
        * Test proverava ispravnost rada metode sendDamageMailToBusinesses kontrolera DamageController
        * (slanje kvara firmama mejlom)
        * u slucaju kada su je prosledjen objekat sa validnim podacima kada je ulogovana odgovorna institucija
        * */

        List<Long> ids = new ArrayList<>();
        ids.add(102L);
        ids.add(103L);
        DamageBusinessDTO damageBusinessDTO = new DamageBusinessDTO();
        damageBusinessDTO.setBusinessId(ids);
        String token = TestUtil.json(damageBusinessDTO);

        // kvar sa ID-em = 104 u zgradi sa ID-em = 100 ima odgovornu instituciju sa ID-em = 102
        mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/104/send_email_to_firm")
                .header("Authentication-Token", this.accessTokenInstitution_2)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isOk());
    }

    @Test
    @Transactional
    @Rollback(true)
    public void testSendDamageMailToBusinessesResponsiblePerson() throws Exception{
        /*
        * Test proverava ispravnost rada metode sendDamageMailToBusinesses kontrolera DamageController
        * (slanje kvara firmama mejlom)
        * u slucaju kada su je prosledjen objekat sa validnim podacima kada je ulogovana odgovorna osoba(stanar)
        * */

        List<Long> ids = new ArrayList<>();
        ids.add(102L);
        ids.add(103L);
        DamageBusinessDTO damageBusinessDTO = new DamageBusinessDTO();
        damageBusinessDTO.setBusinessId(ids);
        String token = TestUtil.json(damageBusinessDTO);

        // kvar sa ID-em = 101 u zgradi sa ID-em = 100 ima odgovornu odobu sa ID-em = 102
        mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/101/send_email_to_firm")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isOk());
    }

    @Test
    public void testSendDamageMailToBusinessesUnauthenticatedUser() throws Exception{
        /*
        * Test proverava ispravnost rada metode sendDamageMailToBusinesses  kontrolera DamageController
        * (slanje kvara firmama mejlom)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        List<Long> ids = new ArrayList<>();
        ids.add(102L);
        ids.add(103L);
        DamageBusinessDTO damageBusinessDTO = new DamageBusinessDTO();
        damageBusinessDTO.setBusinessId(ids);
        String token = TestUtil.json(damageBusinessDTO);

        mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/101/send_email_to_firm")
                .contentType(contentType)
                .content(token))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSendDamageMailToBusinessesUnauthorizedTenantOrInstitution() throws Exception{
        /*
        * Test proverava ispravnost rada metode sendDamageMailToBusinesses  kontrolera DamageController
        * (slanje kvara firmama mejlom)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu ni stanara ni institucije
        * */

        List<Long> ids = new ArrayList<>();
        ids.add(102L);
        ids.add(103L);
        DamageBusinessDTO damageBusinessDTO = new DamageBusinessDTO();
        damageBusinessDTO.setBusinessId(ids);
        String token = TestUtil.json(damageBusinessDTO);

        mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/101/send_email_to_firm")
                .header("Authentication-Token", this.accessTokenFirm_1)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSendDamageMailToBusinessesNonExistingBuilding() throws Exception{
        /*
        * Test proverava ispravnost rada metode sendDamageMailToBusinesses  kontrolera DamageController
        * (slanje kvara firmama mejlom)
        * u slucaju kada ne postoji zgrada sa zadatim ID-em
        * */

        List<Long> ids = new ArrayList<>();
        ids.add(102L);
        ids.add(103L);
        DamageBusinessDTO damageBusinessDTO = new DamageBusinessDTO();
        damageBusinessDTO.setBusinessId(ids);
        String token = TestUtil.json(damageBusinessDTO);

        // ne postoji zgrada sa ID-em = 200
        mockMvc.perform(patch(URL_PREFIX + "/buildings/200/damages/101/send_email_to_firm")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testSendDamageMailToBusinessesNonExistingDamage() throws Exception{
        /*
        * Test proverava ispravnost rada metode sendDamageMailToBusinesses  kontrolera DamageController
        * (slanje kvara firmama mejlom)
        * u slucaju kada ne postoji kvar sa zadatim ID-em
        * */

        List<Long> ids = new ArrayList<>();
        ids.add(102L);
        ids.add(103L);
        DamageBusinessDTO damageBusinessDTO = new DamageBusinessDTO();
        damageBusinessDTO.setBusinessId(ids);
        String token = TestUtil.json(damageBusinessDTO);

        // ne postoji kvar sa ID-em = 200
        mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/200/send_email_to_firm")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testSendDamageMailToBusinessesDamageInvalidBuilding() throws Exception{
        /*
        * Test proverava ispravnost rada metode sendDamageMailToBusinesses  kontrolera DamageController
        * (slanje kvara firmama mejlom)
        * u slucaju kada kvar sa zadatim ID-em ne pripada zgradi sa zadatim ID-em
        * */

        List<Long> ids = new ArrayList<>();
        ids.add(102L);
        ids.add(103L);
        DamageBusinessDTO damageBusinessDTO = new DamageBusinessDTO();
        damageBusinessDTO.setBusinessId(ids);
        String token = TestUtil.json(damageBusinessDTO);

        // kvar sa ID-em = 101 pripada zgradi sa ID-em = 100 (a ne sa ID-em = 101)
        mockMvc.perform(patch(URL_PREFIX + "/buildings/101/damages/101/send_email_to_firm")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testSendDamageMailToBusinessesNoResponsibleInstitution() throws Exception{
        /*
        * Test proverava ispravnost rada metode sendDamageMailToBusinesses  kontrolera DamageController
        * (slanje kvara firmama mejlom)
        * u slucaju kada je logovana institucija, ali zadati kvar nema odgovornu instituciju
        * */

        List<Long> ids = new ArrayList<>();
        ids.add(102L);
        ids.add(103L);
        DamageBusinessDTO damageBusinessDTO = new DamageBusinessDTO();
        damageBusinessDTO.setBusinessId(ids);
        String token = TestUtil.json(damageBusinessDTO);

        // kvar sa ID-em = 101 u zgradi sa ID-em = 100 nema odgovornu instituciju
        mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/101/send_email_to_firm")
                .header("Authentication-Token", this.accessTokenInstitution_2)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSendDamageMailToBusinessesNotResponsibleInstitution() throws Exception{
        /*
        * Test proverava ispravnost rada metode sendDamageMailToBusinesses  kontrolera DamageController
        * (slanje kvara firmama mejlom)
        * u slucaju kada je logovana institucija ali ona nije ujedno i odgovorna institucija za zadati kvar
        * */

        List<Long> ids = new ArrayList<>();
        ids.add(102L);
        ids.add(103L);
        DamageBusinessDTO damageBusinessDTO = new DamageBusinessDTO();
        damageBusinessDTO.setBusinessId(ids);
        String token = TestUtil.json(damageBusinessDTO);

        // kvar sa ID-em = 104 u zgradi sa ID-em = 100 ima odgovornu instituciju sa ID-em = 102
        mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/104/send_email_to_firm")
                .header("Authentication-Token", this.accessTokenInstitution_1)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSendDamageMailToBusinessesTenantWithoutApartment() throws Exception{
        /*
        * Test proverava ispravnost rada metode sendDamageMailToBusinesses  kontrolera DamageController
        * (slanje kvara firmama mejlom)
        * u slucaju kada trenutno logovani stanar nije povezan ni sa jednim stanom
        * */

        List<Long> ids = new ArrayList<>();
        ids.add(102L);
        ids.add(103L);
        DamageBusinessDTO damageBusinessDTO = new DamageBusinessDTO();
        damageBusinessDTO.setBusinessId(ids);
        String token = TestUtil.json(damageBusinessDTO);

        // stanar sa ID-em = 108 nije povezan ni sa jednim stanom
        mockMvc.perform(patch(URL_PREFIX + "/buildings/101/damages/101/send_email_to_firm")
                .header("Authentication-Token", this.accessTokenTenant_3)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testSendDamageMailToBusinessesDamageTenantFromInvalidBuilding() throws Exception{
        /*
        * Test proverava ispravnost rada metode sendDamageMailToBusinesses  kontrolera DamageController
        * (slanje kvara firmama mejlom)
        * u slucaju kada trenutno logovani stanar ne zivi u zgradi sa zadatim ID-em
        * */

        List<Long> ids = new ArrayList<>();
        ids.add(102L);
        ids.add(103L);
        DamageBusinessDTO damageBusinessDTO = new DamageBusinessDTO();
        damageBusinessDTO.setBusinessId(ids);
        String token = TestUtil.json(damageBusinessDTO);

        // stanar sa ID-em = 102 zivi u zgradi sa ID-em = 100 (a ne sa ID-em = 101)
        mockMvc.perform(patch(URL_PREFIX + "/buildings/101/damages/105/send_email_to_firm")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSendDamageMailToBusinessesDamageNoResponsiblePerson() throws Exception{
        /*
        * Test proverava ispravnost rada metode sendDamageMailToBusinesses  kontrolera DamageController
        * (slanje kvara firmama mejlom)
        * u slucaju kada je trenutno ulogovan stanar, ali kvar sa zadatim ID-em nema odgovornu osobu
        * */

        List<Long> ids = new ArrayList<>();
        ids.add(102L);
        ids.add(103L);
        DamageBusinessDTO damageBusinessDTO = new DamageBusinessDTO();
        damageBusinessDTO.setBusinessId(ids);
        String token = TestUtil.json(damageBusinessDTO);

        // kvar sa ID-em = 105 u zgradi sa ID-em = 101 nema odgovornu osobu
        mockMvc.perform(patch(URL_PREFIX + "/buildings/101/damages/105/send_email_to_firm")
                .header("Authentication-Token", this.accessTokenTenant_4)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSendDamageMailToBusinessesDamageNotResponsiblePerson() throws Exception{
        /*
        * Test proverava ispravnost rada metode sendDamageMailToBusinesses  kontrolera DamageController
        * (slanje kvara firmama mejlom)
        * u slucaju kada je trenutno ulogovan stanar ali on nije ujedno i odgovorna osoba za kvar sa zadatim ID-em
        * */

        List<Long> ids = new ArrayList<>();
        ids.add(102L);
        ids.add(103L);
        DamageBusinessDTO damageBusinessDTO = new DamageBusinessDTO();
        damageBusinessDTO.setBusinessId(ids);
        String token = TestUtil.json(damageBusinessDTO);

        // kvar sa ID-em = 101 u zgradi sa ID-em = 100 ima odgovornu osobu sa ID-em = 101
        mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/101/send_email_to_firm")
                .header("Authentication-Token", this.accessTokenTenant_1)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSendDamageMailToBusinessesListIsEmpty() throws Exception{
        /*
        * Test proverava ispravnost rada metode sendDamageMailToBusinesses  kontrolera DamageController
        * (slanje kvara firmama mejlom)
        * u slucaju kada je dolazni DTO objekat nevalidan - sadrzi sve neophodna polja, ali je lista sa id-evima firmi prazna
        * */

        List<Long> ids = new ArrayList<>();
        DamageBusinessDTO damageBusinessDTO = new DamageBusinessDTO();
        damageBusinessDTO.setBusinessId(ids);
        String token = TestUtil.json(damageBusinessDTO);

        mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/101/send_email_to_firm")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testSendDamageMailToBusinessWrongIdInList() throws Exception{
        /*
        * Test proverava ispravnost rada metode sendDamageMailToBusinesses  kontrolera DamageController
        * (slanje kvara firmama mejlom)
        * u slucaju kada je dolazni DTO objekat nevalidan: sadrzi sva neophodna polja ali se u listi id-eva firmi nalazi bar jedan nepostojeci
        * */

        List<Long> ids = new ArrayList<>();
        ids.add(102L);
        ids.add(500L);
        DamageBusinessDTO damageBusinessDTO = new DamageBusinessDTO();
        damageBusinessDTO.setBusinessId(ids);
        String token = TestUtil.json(damageBusinessDTO);

        // ne postoji firma/institucija sa ID-em = 500
        mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/101/send_email_to_firm")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testSendDamageMailToBusinessesInappropriateTypeOfListElement() throws Exception{
        /*
        * Test proverava ispravnost rada metode sendDamageMailToBusinesses  kontrolera DamageController
        * (slanje kvara firmama mejlom)
        * u slucaju kada je dolazni DTO objekat nevalidan: sadrzi sva neophodna polja ali se u listi id-eva
         * nalazi bar jedan nevalidnog tipa
        * */
        String token = "{\"businessId\":[102L, \"srting\"]}";

        mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/101/send_email_to_firm")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za declareDamageFixed
    @Test
    @Transactional
    @Rollback(true)
    public void testDeclareDamageFixed() throws Exception{
        /*
        * Test proverava ispravnost rada metode declareDamageFixed kontrolera DamageController
        * (proglasavanje kvara popravljenim)
        * u slucaju kada su je prosledjen objekat sa validnim podacima kada je ulogovana odgovorna institucija
        * */

        DamageFixedDTO damageFixedDTO = new DamageFixedDTO(102L);
        String token = TestUtil.json(damageFixedDTO);

        // kvar sa ID-em = 104 u zgradi sa ID-em = 100 ima odgovornu instituciju sa ID-em = 102
        mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/104/declare_damage_fixed")
                .header("Authentication-Token", this.accessTokenInstitution_2)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeclareDamageFixedByUnauthenticatedUser() throws Exception{
        /*
        * Test proverava ispravnost rada metode declareDamageFixed kontrolera DamageController
        * (proglasavanje kvara popravljenim)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        DamageFixedDTO damageFixedDTO = new DamageFixedDTO(102L);
        String token = TestUtil.json(damageFixedDTO);

        mockMvc.perform(put(URL_PREFIX + "/buildings/100/damages/104/declare_damage_fixed")
                .contentType(contentType)
                .content(token))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDeclareDamageFixedUnauthorizedUser() throws Exception{
        /*
        * Test proverava ispravnost rada metode declareDamageFixed kontrolera DamageController
        * (proglasavanje kvara popravljenim)
        * u slucaju kada akciju izvrsava ulogovani korisnik koji nema ulogu stanara ili institucije
        * */

        DamageFixedDTO damageFixedDTO = new DamageFixedDTO(102L);
        String token = TestUtil.json(damageFixedDTO);

        mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/104/declare_damage_fixed")
                .header("Authentication-Token", this.accessTokenFirm_1)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDeclareDamageFixedNonexistentDamage() throws Exception{
        /*
        * Test proverava ispravnost rada metode declareDamageFixed kontrolera DamageController
        * (proglasavanje kvara popravljenim)
        * u slucaju kada ne postoji kvar sa zadatim ID-em
        * */

        DamageFixedDTO damageFixedDTO = new DamageFixedDTO(102L);
        String token = TestUtil.json(damageFixedDTO);

        // ne postoji kvar sa ID-em = 200
        mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/200/declare_damage_fixed")
                .header("Authentication-Token", this.accessTokenInstitution_2)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeclareDamageFixedNonexistentBuilding() throws Exception{
        /*
        * Test proverava ispravnost rada metode declareDamageFixed kontrolera DamageController
        * (proglasavanje kvara popravljenim)
        * u slucaju kada ne postoji zgrada sa zadatim ID-em
        * */

        DamageFixedDTO damageFixedDTO = new DamageFixedDTO(102L);
        String token = TestUtil.json(damageFixedDTO);

        // ne postoji zgrada sa ID-em = 200
        mockMvc.perform(patch(URL_PREFIX + "/buildings/200/damages/104/declare_damage_fixed")
                .header("Authentication-Token", this.accessTokenInstitution_2)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeclareDamageFixedDamageInvalidBuilding() throws Exception{
        /*
        * Test proverava ispravnost rada metode declareDamageFixed kontrolera DamageController
        * (proglasavanje kvara popravljenim)
        * u slucaju kada kvar sa zadatim ID-em ne pripada zgradi sa zadtaim ID-em
        * */

        DamageFixedDTO damageFixedDTO = new DamageFixedDTO(102L);
        String token = TestUtil.json(damageFixedDTO);

        // kvar sa ID-em = 104 pripada zgradi sa Id-em = 100 (a ne ID-em = 101)
        mockMvc.perform(patch(URL_PREFIX + "/buildings/101/damages/104/declare_damage_fixed")
                .header("Authentication-Token", this.accessTokenInstitution_2)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testDeclareDamageFixedNoSelectedBusiness() throws Exception{
        /*
        * Test proverava ispravnost rada metode declareDamageFixed kontrolera DamageController
        * (proglasavanje kvara popravljenim)
        * u slucaju kada je trenutno logovana institucija ali kvar sa zadatim ID-em nema izabranu firmu/instituciju za njegovo popravljanje
        * */

        DamageFixedDTO damageFixedDTO = new DamageFixedDTO(102L);
        String token = TestUtil.json(damageFixedDTO);

        // kvar sa ID-em = 105 u zgradi sa ID-em = 101 nema izabranu firmu/instituciju
        mockMvc.perform(patch(URL_PREFIX + "/buildings/101/damages/105/declare_damage_fixed")
                .header("Authentication-Token", this.accessTokenInstitution_2)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDeclareDamageFixedInstitutionIsNotSelected() throws Exception{
        /*
        * Test proverava ispravnost rada metode declareDamageFixed kontrolera DamageController
        * (proglasavanje kvara popravljenim)
        * u slucaju kada je trenutno ulogovana institucija ali nona nije ujedno i izabrana za otklanjanje kvara sa zadatim ID-em
        * */

        DamageFixedDTO damageFixedDTO = new DamageFixedDTO(102L);
        String token = TestUtil.json(damageFixedDTO);

        // kvar sa ID-em = 104 u zgradi sa ID-em = 100 ima izabranu firmu sa ID-em = 102
        mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/104/declare_damage_fixed")
                .header("Authentication-Token", this.accessTokenInstitution_1)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDeclareDamageFixedNonexistentOffer() throws Exception{
        /*
        * Test proverava ispravnost rada metode declareDamageFixed kontrolera DamageController
        * (proglasavanje kvara popravljenim)
        * u slucaju kada je dolazni DTO  objekat nevalidan: sadrzi sva polja - ne postoji nijedna ponuda sa ID-em offerId
        * */

        DamageFixedDTO damageFixedDTO = new DamageFixedDTO(200L);
        String token = TestUtil.json(damageFixedDTO);

        // ne postoji ponuda sa ID-em = 200
        mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/104/declare_damage_fixed")
                .header("Authentication-Token", this.accessTokenInstitution_2)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeclareDamageFixedInvalidOffer() throws Exception{
        /*
        * Test proverava ispravnost rada metode declareDamageFixed kontrolera DamageController
        * (proglasavanje kvara popravljenim)
        * u slucaju kada je dolazni DTO  objekat nevalidan: sadrzi sva polja, ali ponuda sa ID-em offerId ne pripada zadatom kvaru
        * */

        DamageFixedDTO damageFixedDTO = new DamageFixedDTO(101L);
        String token = TestUtil.json(damageFixedDTO);

        // ponuda sa ID-em = 101 pripada kvaru sa ID-em = 101 (a ne sa ID-em = 104)
        mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/104/declare_damage_fixed")
                .header("Authentication-Token", this.accessTokenInstitution_2)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testDeclareDamageFixedInappropriateTypeOfId() throws Exception{
        /*
        * Test proverava ispravnost rada metode declareDamageFixed kontrolera DamageController
        * (proglasavanje kvara popravljenim)
        * u slucaju kada je dolazni DTO objekat nevalidan - sadrzi sva polja, ali je polje offerId nevalidnog tipa
        * */

        String token = "{\"offerId\":\"string\"}";

        mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/104/declare_damage_fixed")
                .header("Authentication-Token", this.accessTokenInstitution_2)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za getAllDamagesForInstitution
    @Test
    public void testGetAllDamagesForInstitution() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllDamagesForInstitution kontrolera DamageController
        * (dobavljanje liste svih kvarova za koje je institucija odgovorna)
        * */

        /*
        institucija sa ID-em = 102 je odgovorna za sledece kvarove:
            1 - id = 104
                description = fifth damage description
                buidlingId = 100,
                apartmentId = 101
            2 - id = 105
                description = sixth damage description
                buildingId = 100,
                apartmentId = 103
        */
        mockMvc.perform(get(URL_PREFIX + "/institutions/102/responsible_for_damages")
                .header("Authentication-Token", this.accessTokenInstitution_2)
                .params(params))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.[*]", hasSize(2)))
                .andExpect(jsonPath("$.[*].id", containsInAnyOrder(104, 105)))
                .andExpect(jsonPath("$.[*].description",
                        containsInAnyOrder("fifth damage description", "sixth damage description")));
    }

    @Test
    public void testGetAllDamagesForInstitutionByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllDamagesForInstitution kontrolera DamageController
        * (dobavljanje liste svih kvarova za koje je institucija odgovorna)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        mockMvc.perform(get(URL_PREFIX + "/institutions/102/responsible_for_damages")
                .params(params))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetAllDamagesForInstitutionByNotAuthenticatedInstitution() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllDamagesForInstitution kontrolera DamageController
        * (dobavljanje liste svih kvarova za koje je institucija odgovorna)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu institucije
        * */

        mockMvc.perform(get(URL_PREFIX + "/institutions/102/responsible_for_damages")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .params(params))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetAllDamagesForInstitutionNonexistentInstitution() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllDamagesForInstitution kontrolera DamageController
        * (dobavljanje liste svih kvarova za koje je institucija odgovorna)
        * u slucaju kada ne postoji institucija sa zadatim Id-em
        * */

        // ne postoji institucija sa ID-em = 200
        mockMvc.perform(get(URL_PREFIX + "/institutions/200/responsible_for_damages")
                .header("Authentication-Token", this.accessTokenInstitution_2)
                .params(params))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetAllDamagesForInstitutionInvalidInstitution() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllDamagesForInstitution kontrolera DamageController
        * (dobavljanje liste svih kvarova za koje je institucija odgovorna)
        * u slucaju kada ulogovana institucija nema isti ID kao sto je ID zadat u URL-u
        * */

        mockMvc.perform(get(URL_PREFIX + "/institutions/103/responsible_for_damages")
                .header("Authentication-Token", this.accessTokenInstitution_2)
                .params(params))
                .andExpect(status().isForbidden());
    }

    // TESTOVI za searchAllDamagesForInstitution
    @Test
    public void tesSearchAllDamagesForInstitution() throws Exception {
        /*
        * Test proverava ispravnost rada metode searchAllDamagesForInstitution kontrolera DamageController
        * (dobavljanje liste svih kvarova za koje je institucija odgovorna koji su kreirani u periodu koji je zadat)
        * */

        /*
        institucija sa ID-em = 102 je odgovorna za sledece kvarove koji su kreirani u periodu od 25.11.2017. do 01.12.2017.:
            1 - id = 104
                description = fifth damage description
                buidlingId = 100,
                apartmentId = 101
            2 - id = 105
                description = sixth damage description
                buildingId = 100,
                apartmentId = 103
        */
        mockMvc.perform(get(URL_PREFIX + "/institutions/102/responsible_for_damages_from_to")
                .header("Authentication-Token", this.accessTokenInstitution_2)
                .params(params)
                .param("from", "2017-11-25 00:00:01")
                .param("to", "2017-12-01 23:59:59"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.[*]", hasSize(2)))
                .andExpect(jsonPath("$.[*].id", containsInAnyOrder(104, 105)))
                .andExpect(jsonPath("$.[*].description",
                        containsInAnyOrder("fifth damage description", "sixth damage description")));
    }

    @Test
    public void testSearchAllDamagesForInstitutionByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode searchAllDamagesForInstitution kontrolera DamageController
        * (dobavljanje liste svih kvarova za koje je institucija odgovorna koji su kreirani u periodu koji je zadat)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        mockMvc.perform(get(URL_PREFIX + "/institutions/102/responsible_for_damages_from_to")
                .params(params)
                .param("from", "2017-11-25 00:00:01")
                .param("to", "2017-12-01 23:59:59"))
                .andExpect(status().isForbidden());
    }

    // TESTOVI za getDamageForBusiness
    @Test
    public void testGetDamageForBusiness() throws Exception {
        /*
        * Test proverava ispravnost rada metode getDamageForBusiness kontrolera DamageController
        * (dobavljanje konkretnog kvara za cije je resavanje zaduzena trenutno ulogovana institucija/firma ili
        * kvara za koji je odgovorna trenutno ulogovana institucija)
        * u slucaju kada metodu izvrsava institucija i dobavlja kvar za koji je odgovorna
        * */

        /*
        institucija sa ID-em = 102 je odgovorna za kvar sa ID-em = 104 i sledecim podacima:
            id = 104,
            description = fifth damage description,
            building_id = 100,
            responsible_person_id = 103,
            selected_business_id = 102
        */

        mockMvc.perform(get(URL_PREFIX + "/businesses/102/damages/104")
                .header("Authentication-Token", this.accessTokenInstitution_2))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(104))
                .andExpect(jsonPath("$.description").value("fifth damage description"))
                .andExpect(jsonPath("$.buildingId").value(100))
                .andExpect(jsonPath("$.responsiblePerson.id").value(103))
                .andExpect(jsonPath("$.selectedBusiness.id").value(102));
    }

    @Test
    public void testGetDamageForBusinessByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode getDamageForBusiness kontrolera DamageController
        * (dobavljanje konkretnog kvara za cije je resavanje zaduzena trenutno ulogovana institucija/firma ili
        * kvara za koji je odgovorna trenutno ulogovana institucija)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        mockMvc.perform(get(URL_PREFIX + "/businesses/102/damages/104"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetDamageForBusinessByNotAuthenticatedInstitution() throws Exception {
        /*
        * Test proverava ispravnost rada metode getDamageForBusiness kontrolera DamageController
        * (dobavljanje konkretnog kvara za cije je resavanje zaduzena trenutno ulogovana institucija/firma ili
        * kvara za koji je odgovorna trenutno ulogovana institucija)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu institucije ili firme
        * */

        mockMvc.perform(get(URL_PREFIX + "/businesses/102/damages/104")
                .header("Authentication-Token", this.accessTokenTenant_2))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetDamageForBusinessNonexistentInstitution() throws Exception {
        /*
        * Test proverava ispravnost rada metode getDamageForBusiness kontrolera DamageController
        * (dobavljanje konkretnog kvara za cije je resavanje zaduzena trenutno ulogovana institucija/firma ili
        * kvara za koji je odgovorna trenutno ulogovana institucija)
        * u slucaju kada ne postoji institucija/firma sa zadatim ID-em
        * */

        // ne postoji institucija/firma sa ID-em = 200
        mockMvc.perform(get(URL_PREFIX + "/businesses/200/damages/104")
                .header("Authentication-Token", this.accessTokenInstitution_2))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetDamageForBusinessesNonexistentDamage() throws Exception {
        /*
        * Test proverava ispravnost rada metode getDamageForBusiness kontrolera DamageController
        * (dobavljanje konkretnog kvara za cije je resavanje zaduzena trenutno ulogovana institucija/firma ili
        * kvara za koji je odgovorna trenutno ulogovana institucija)
        * u slucaju kada ne postoji kvar sa zadatim ID-em
        * */

        // ne postoji kvar sa ID-em = 200
        mockMvc.perform(get(URL_PREFIX + "/businesses/102/damages/200")
                .header("Authentication-Token", this.accessTokenInstitution_2))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetDamageForBusinessesInvalidInstitution() throws Exception {
        /*
        * Test proverava ispravnost rada metode getDamageForBusiness kontrolera DamageController
        * (dobavljanje konkretnog kvara za cije je resavanje zaduzena trenutno ulogovana institucija/firma ili
        * kvara za koji je odgovorna trenutno ulogovana institucija)
        * u slucaju kada ulogovana institucija nema isti ID kao sto je ID zadat u URL-u
        * */

        mockMvc.perform(get(URL_PREFIX + "/businesses/103/damages/104")
                .header("Authentication-Token", this.accessTokenInstitution_2))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetDamageForBusinessesNotResponsibleInstitution() throws Exception {
        /*
        * Test proverava ispravnost rada metode getDamageForBusiness kontrolera DamageController
        * (dobavljanje konkretnog kvara za cije je resavanje zaduzena trenutno ulogovana institucija/firma ili
        * kvara za koji je odgovorna trenutno ulogovana institucija)
        * u slucaju kada ulogovana institucija nije ujedno i odgovorna institucija za kvar sa zadatim ID-em,
        * niti je zaduzena za njegovo resavanje
        * */

        // za kvar sa ID-em = 104 odgovorna je institucija sa ID-em = 102
        mockMvc.perform(get(URL_PREFIX + "/businesses/103/damages/104")
                .header("Authentication-Token", this.accessTokenInstitution_1))
                .andExpect(status().isForbidden());
    }

    // TESTOVI za getAllDamagesForRepair
    @Test
    public void testGetAllDamagesForRepair() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllDamagesForRepair kontrolera DamageController
        * (dobavljanje liste svih kvarova za cije je resavanje izabrana institucija/firma sa zadatim ID-em)
        * */

        /*
        institucija sa ID-em = 102 je izabrana za resavanje sledecih kvarova:
            1 - id = 104,
                description = fifth damage description,
                building_id = 100
        */
        mockMvc.perform(get(URL_PREFIX + "/businesses/102/damages_for_repair")
                .header("Authentication-Token", this.accessTokenInstitution_2)
                .params(params))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.[*]", hasSize(1)))
                .andExpect(jsonPath("$.[*].id", containsInAnyOrder(104)))
                .andExpect(jsonPath("$.[*].description", containsInAnyOrder("fifth damage description")))
                .andExpect(jsonPath("$.[*].buildingId", containsInAnyOrder(100)));
    }

    @Test
    public void testGetAllDamagesForRepairByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllDamagesForRepair kontrolera DamageController
        * (dobavljanje liste svih kvarova za cije je resavanje izabrana institucija/firma sa zadatim ID-em)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        mockMvc.perform(get(URL_PREFIX + "/businesses/102/damages_for_repair")
                .params(params))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetAllDamagesForRepairByNotAuthenticatedInstitutionOrFirm() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllDamagesForRepair kontrolera DamageController
        * (dobavljanje liste svih kvarova za cije je resavanje izabrana institucija/firma sa zadatim ID-em)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu institucije niti firme
        * */

        mockMvc.perform(get(URL_PREFIX + "/businesses/102/damages_for_repair")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .params(params))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetAllDamagesForRepairNonexistentInstitutionOrFirm() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllDamagesForRepair kontrolera DamageController
        * (dobavljanje liste svih kvarova za cije je resavanje izabrana institucija/firma sa zadatim ID-em)
        * u slucaju kada ne postoji institucija/firma sa zadatim ID-em
        * */

        // ne postoji institucija niti firma sa ID-em = 200
        mockMvc.perform(get(URL_PREFIX + "/businesses/200/damages_for_repair")
                .header("Authentication-Token", this.accessTokenInstitution_2)
                .params(params))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetAllDamagesForRepairInvalidInstitutionOrFirm() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllDamagesForRepair kontrolera DamageController
        * (dobavljanje liste svih kvarova za cije je resavanje izabrana institucija/firma sa zadatim ID-em)
        * u slucaju kada ulogovana institucija nema isti ID kao sto je ID zadat u URL-u
        * */

        mockMvc.perform(get(URL_PREFIX + "/businesses/103/damages_for_repair")
                .header("Authentication-Token", this.accessTokenInstitution_2)
                .params(params))
                .andExpect(status().isForbidden());
    }

    // TESTOVI za searchAllDamagesForRepair
    @Test
    public void testSearchAllDamagesForRepair() throws Exception{
        /*
        * Test proverava ispravnost rada metode searchAllDamagesForRepair kontrolera DamageController
        * (dobavljanje liste svih kvarova za cije je resavanje izabrana institucija/firma sa zadatim ID-em,
        * koji su kreirani u zadatom periodu)
        * */

        /*
        institucija sa ID-em = 102 je izabrana za resavanje sledecih kvarova koji su kreirani u periodu od 5.11.2017. do 5.12.2017.:
            1 - id = 104,
                description = fifth damage description,
                building_id = 100
        */
        mockMvc.perform(get(URL_PREFIX + "/businesses/102/damages_for_repair_from_to")
                .header("Authentication-Token", this.accessTokenInstitution_2)
                .params(params)
                .param("from", "2017-11-05 00:00:01")
                .param("to", "2018-01-05 23:59:59"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.[*]", hasSize(1)))
                .andExpect(jsonPath("$.[*].id", containsInAnyOrder(104)))
                .andExpect(jsonPath("$.[*].description", containsInAnyOrder("fifth damage description")))
                .andExpect(jsonPath("$.[*].buildingId", containsInAnyOrder(100)));
    }

    // TESTOVI za getAllDamagesForTenant
    @Test
    public void testGetAllDamagesForTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllDamagesForTenant kontrolera DamageController
        * (dobavljanje liste svih kvarova za koje je stanar sa zadatim ID-em odgovorna osoba)
        * */

        /*
        stanar sa ID-em = 101 je odogovorna osoba za sledece kvarove:
            1 - id = 101,
                description = second damage description,
                apartment_id = 101,
                building_id = 100
        */
        mockMvc.perform(get(URL_PREFIX + "/tenants/101/responsible_for_damages")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .params(params))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.[*]", hasSize(1)))
                .andExpect(jsonPath("$.[*].id", containsInAnyOrder(101)))
                .andExpect(jsonPath("$.[*].description", containsInAnyOrder("second damage description")))
                .andExpect(jsonPath("$.[*].apartmentId", containsInAnyOrder(101)));
    }

    @Test
    public void testGetAllDamagesForTenantByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllDamagesForTenant kontrolera DamageController
        * (dobavljanje liste svih kvarova za koje je stanar sa zadatim ID-em odgovorna osoba)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        mockMvc.perform(get(URL_PREFIX + "/tenants/101/responsible_for_damages")
                .params(params))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetAllDamagesForTenantByNotAuthenticatedTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllDamagesForTenant kontrolera DamageController
        * (dobavljanje liste svih kvarova za koje je stanar sa zadatim ID-em odgovorna osoba)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu stanara
        * */

        mockMvc.perform(get(URL_PREFIX + "/tenants/101/responsible_for_damages")
                .header("Authentication-Token", this.accessTokenInstitution_1)
                .params(params))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetAllDamagesForTenantNonexistentInstitutionOrFirm() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllDamagesForTenant kontrolera DamageController
        * (dobavljanje liste svih kvarova za koje je stanar sa zadatim ID-em odgovorna osoba)
        * u slucaju kada ne postoji stanar sa zadatim ID-em
        * */

        // ne postoji stanar sa ID-em = 200
        mockMvc.perform(get(URL_PREFIX + "/tenants/200/responsible_for_damages")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .params(params))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetAllDamagesForTenantInvalidTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllDamagesForTenant kontrolera DamageController
        * (dobavljanje liste svih kvarova za koje je stanar sa zadatim ID-em odgovorna osoba)
        * u slucaju kada ulogovani stanar nema isti ID kao sto je ID zadat u URL-u
        * */

        mockMvc.perform(get(URL_PREFIX + "/tenants/102/responsible_for_damages")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .params(params))
                .andExpect(status().isForbidden());
    }

    // TESTOVI za getDamageForTenant
    @Test
    public void testGetDamageForTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode getDamageForTenant kontrolera DamageController
        * (dobavljanje konkretnog kvara za koji je stanar sa zadatim ID-em odgovorna osoba
        * ili je taj kvar prijavljen u nekom od stanova ciji je on vlasnik
        * ili je taj kvar prijavljen u stanu u kom zivi posmatrani stanar
        * ili je u pitanju kvar kog je kreirao posmatrani stanar)
        * u slucaju kada je stanar odgovorna osoba za izabrani kvar
        * */

        /*
        stanar sa ID-em = 101 je odgovorna osoba za kvar sa ID-em = 101 i sledecim podacima:
            id = 101,
            description = second damage description,
            building_id = 100,
            apartment_id = 101
        */
        mockMvc.perform(get(URL_PREFIX + "/tenants/101/damages/101")
                .header("Authentication-Token", this.accessTokenTenant_2))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(101))
                .andExpect(jsonPath("$.description").value("second damage description"))
                .andExpect(jsonPath("$.buildingId").value(100))
                .andExpect(jsonPath("$.apartmentId").value(101));
    }

    @Test
    public void testGetDamageForTenantByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode getDamageForTenant kontrolera DamageController
        * (dobavljanje konkretnog kvara za koji je stanar sa zadatim ID-em odgovorna osoba
        * ili je taj kvar prijavljen u nekom od stanova ciji je on vlasnik
        * ili je taj kvar prijavljen u stanu u kom zivi posmatrani stanar
        * ili je u pitanju kvar kog je kreirao posmatrani stanar)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        mockMvc.perform(get(URL_PREFIX + "/tenants/101/damages/101"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetDamageForTenantByNotAuthenticatedTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode getDamageForTenant kontrolera DamageController
        * (dobavljanje konkretnog kvara za koji je stanar sa zadatim ID-em odgovorna osoba
        * ili je taj kvar prijavljen u nekom od stanova ciji je on vlasnik
        * ili je taj kvar prijavljen u stanu u kom zivi posmatrani stanar
        * ili je u pitanju kvar kog je kreirao posmatrani stanar)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu stanara
        * */

        mockMvc.perform(get(URL_PREFIX + "/tenants/101/damages/101")
                .header("Authentication-Token", this.accessTokenInstitution_1))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetDamageForTenantNonexistentTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode getDamageForTenant kontrolera DamageController
        * (dobavljanje konkretnog kvara za koji je stanar sa zadatim ID-em odgovorna osoba
        * ili je taj kvar prijavljen u nekom od stanova ciji je on vlasnik
        * ili je taj kvar prijavljen u stanu u kom zivi posmatrani stanar
        * ili je u pitanju kvar kog je kreirao posmatrani stanar)
        * u slucaju kada ne postoji stanar sa zadatim ID-em
        * */

        // ne postoji stanar sa ID-em = 200
        mockMvc.perform(get(URL_PREFIX + "/tenants/200/damages/101")
                .header("Authentication-Token", this.accessTokenTenant_2))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetDamageForTenantNonexistentDamage() throws Exception {
        /*
        * Test proverava ispravnost rada metode getDamageForTenant kontrolera DamageController
        * (dobavljanje konkretnog kvara za koji je stanar sa zadatim ID-em odgovorna osoba
        * ili je taj kvar prijavljen u nekom od stanova ciji je on vlasnik
        * ili je taj kvar prijavljen u stanu u kom zivi posmatrani stanar
        * ili je u pitanju kvar kog je kreirao posmatrani stanar)
        * u slucaju kada ne postoji kvar sa zadatim Id-em
        * */

        // ne postoji kvar sa ID-em = 200
        mockMvc.perform(get(URL_PREFIX + "/tenants/101/damages/200")
                .header("Authentication-Token", this.accessTokenTenant_2))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetDamageForTenantInvalidTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode getDamageForTenant kontrolera DamageController
        * (dobavljanje konkretnog kvara za koji je stanar sa zadatim ID-em odgovorna osoba
        * ili je taj kvar prijavljen u nekom od stanova ciji je on vlasnik
        * ili je taj kvar prijavljen u stanu u kom zivi posmatrani stanar
        * ili je u pitanju kvar kog je kreirao posmatrani stanar)
        * u slucaju kada ulogovani stanar nema isti ID kao sto je ID zadat u URL-u
        * */

        mockMvc.perform(get(URL_PREFIX + "/tenants/102/damages/101")
                .header("Authentication-Token", this.accessTokenTenant_2))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetDamageForTenantNotInAnyOfCollections() throws Exception {
        /*
        * Test proverava ispravnost rada metode getDamageForTenant kontrolera DamageController
        * (dobavljanje konkretnog kvara za koji je stanar sa zadatim ID-em odgovorna osoba
        * ili je taj kvar prijavljen u nekom od stanova ciji je on vlasnik
        * ili je taj kvar prijavljen u stanu u kom zivi posmatrani stanar
        * ili je u pitanju kvar kog je kreirao posmatrani stanar)
        * u slucaju kada ulogovani stanar nije ujedno i odgovorna osoba za kvar sa zadatim ID-em niti
        * pripada stanu u kom zivi/licnim stanovima niti ga je stanar kreirao
        * */

        /*
        za kvar sa ID-em = 105 nema odgovornu osobau, kreirao ga je stanar sa ID-em = 107,
        nalazi se u stanu sa ID-em = 103 ciji vlasnik/stanar nije stanar sa ID-em = 101
        */
        mockMvc.perform(get(URL_PREFIX + "/tenants/101/damages/105")
                .header("Authentication-Token", this.accessTokenTenant_2))
                .andExpect(status().isForbidden());
    }

    // TESTOVI za getAllDamagesInPersonalApartmentsForTenant
    @Test
    public void testGetAllDamagesInPersonalApartmentsForTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllDamagesInPersonalApartmentsForTenant kontrolera DamageController
        * (dobavljanje liste svih kvarova u stanovima korisnika sa zadatim ID-em)
        * */

        /*
        stanar sa ID-em = 101 ima 3 stana sa ID-evima: 100,101 i 102, kojima pripadaju kvarovi
            1 - id = 100,
                description = first damage description,
                apartment_id = 102
                building_id = 100
            2 - id = 101,
                description = second damage description,
                apartment_id = 101,
                building_id = 100,
            3 - id = 102,
                description = third damage description,
                apartment_id = 101,
                building_id = 100
            4 - id = 103
                description = forth damage description,
                apartment_id = 101,
                building_id = 100
            5 - id = 104,
                description = fifth damage description,
                apartment_id = 101,
                building_id = 100
        */
        mockMvc.perform(get(URL_PREFIX + "/tenants/101/personal_apartments/damages")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .params(params))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.[*]", hasSize(5)))
                .andExpect(jsonPath("$.[*].id", containsInAnyOrder(100,101,102,103,104)))
                .andExpect(jsonPath("$.[*].description",
                        containsInAnyOrder("first damage description", "second damage description",
                        "third damage description", "forth damage description", "fifth damage description")));
    }

    @Test
    public void testGetAllDamagesInPersonalApartmentsForTenantByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllDamagesInPersonalApartmentsForTenant kontrolera DamageController
        * (dobavljanje liste svih kvarova u stanovima korisnika sa zadatim ID-em)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        mockMvc.perform(get(URL_PREFIX + "/tenants/101/personal_apartments/damages")
                .params(params))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetAllDamagesInPersonalApartmentsForTenantByNotAuthenticatedTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllDamagesInPersonalApartmentsForTenant kontrolera DamageController
        * (dobavljanje liste svih kvarova u stanovima korisnika sa zadatim ID-em)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu stanara
        * */

        mockMvc.perform(get(URL_PREFIX + "/tenants/101/personal_apartments/damages")
                .header("Authentication-Token", this.accessTokenInstitution_1)
                .params(params))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetAllDamagesInPersonalApartmentsForTenantNonexistentTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllDamagesInPersonalApartmentsForTenant kontrolera DamageController
        * (dobavljanje liste svih kvarova u stanovima korisnika sa zadatim ID-em)
        * u slucaju kada ne postoji stanar sa zadatim Id-em
        * */

        // ne postoji stanar sa ID-em = 200
        mockMvc.perform(get(URL_PREFIX + "/tenants/200/personal_apartments/damages")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .params(params))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetAllDamagesInPersonalApartmentsForTenantInvalidTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllDamagesInPersonalApartmentsForTenant kontrolera DamageController
        * (dobavljanje liste svih kvarova u stanovima korisnika sa zadatim ID-em)
        * u slucaju kada ulogovani stanar nema isti ID kao sto je ID zadat u URL-u
        * */

        mockMvc.perform(get(URL_PREFIX + "/tenants/102/personal_apartments/damages")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .params(params))
                .andExpect(status().isForbidden());
    }

    // TESTOVI za getAllDamagesInApartmentForTenant
    @Test
    public void testGetAllDamagesInApartmentForTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllDamagesInApartmentForTenant kontrolera DamageController
        * (dobavljanje liste svih kvarova u stanu u kom trenutno zivi stanar sa zadatim ID-em)
        * */

        /*
        stanar sa ID-em = 101 zivi u stanu sa ID-em = 101 kom pripadaju kvarovi
            1 - id = 101,
                description = second damage description,
                apartment_id = 101,
                building_id = 100,
            2 - id = 102,
                description = third damage description,
                apartment_id = 101,
                building_id = 100
            3 - id = 103
                description = forth damage description,
                apartment_id = 101,
                building_id = 100
            4 - id = 104,
                description = fifth damage description,
                apartment_id = 101,
                building_id = 100
        */
        mockMvc.perform(get(URL_PREFIX + "/tenants/101/apartment_live_in/damages")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .params(params))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.[*]", hasSize(4)))
                .andExpect(jsonPath("$.[*].id", containsInAnyOrder(101,102,103,104)))
                .andExpect(jsonPath("$.[*].description", containsInAnyOrder("second damage description",
                        "third damage description", "forth damage description", "fifth damage description")));
    }

    @Test
    public void testGetAllDamagesInApartmentForTenantByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllDamagesInApartmentForTenant kontrolera DamageController
        * (dobavljanje liste svih kvarova u stanu u kom trenutno zivi stanar sa zadatim ID-em)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        mockMvc.perform(get(URL_PREFIX + "/tenants/101/apartment_live_in/damages")
                .params(params))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetAllDamagesInApartmentForTenantByNotAuthenticatedTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllDamagesInApartmentForTenant kontrolera DamageController
        * (dobavljanje liste svih kvarova u stanu u kom trenutno zivi stanar sa zadatim ID-em)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu stanara
        * */

        mockMvc.perform(get(URL_PREFIX + "/tenants/101/apartment_live_in/damages")
                .header("Authentication-Token", this.accessTokenInstitution_1)
                .params(params))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetAllDamagesInApartmentForTenantNonexistentTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllDamagesInApartmentForTenant kontrolera DamageController
        * (dobavljanje liste svih kvarova u stanu u kom trenutno zivi stanar sa zadatim ID-em)
        * u slucaju kada ne postoji stanar sa zadatim Id-em
        * */

        // ne postoji stanar sa ID-em = 200
        mockMvc.perform(get(URL_PREFIX + "/tenants/200/apartment_live_in/damages")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .params(params))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetAllDamagesInApartmentForTenantInvalidTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllDamagesInApartmentForTenant kontrolera DamageController
        * (dobavljanje liste svih kvarova u stanu u kom trenutno zivi stanar sa zadatim ID-em)
        * u slucaju kada ulogovani stanar nema isti ID kao sto je ID zadat u URL-u
        * */

        mockMvc.perform(get(URL_PREFIX + "/tenants/102/apartment_live_in/damages")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .params(params))
                .andExpect(status().isForbidden());
    }

    // TESTOVI za getMyDamages
    @Test
    public void testGetMyDamages() throws Exception {
        /*
        * Test proverava ispravnost rada metode getMyDamages kontrolera DamageController
        * (dobavljanje liste svih kvarova koje je prijavio stanar sa zadatim ID-em)
        * */

        /*
        stanar sa ID-em = 101 je prijavio sledece kvarove:
            1 - id = 101,
                description = second damage description,
                apartment_id = 101,
                building_id = 100,
            2 - id = 102,
                description = third damage description,
                apartment_id = 101,
                building_id = 100
            3 - id = 104,
                description = fifth damage description,
                apartment_id = 101,
                building_id = 100
        */
        mockMvc.perform(get(URL_PREFIX + "/tenants/101/my_damages")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .params(params))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.[*]", hasSize(3)))
                .andExpect(jsonPath("$.[*].id", containsInAnyOrder(101,102,104)))
                .andExpect(jsonPath("$.[*].description", containsInAnyOrder("second damage description",
                        "third damage description", "fifth damage description")));
    }

    @Test
    public void testGetMyDamagesByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode getMyDamages kontrolera DamageController
        * (dobavljanje liste svih kvarova koje je prijavio stanar sa zadatim ID-em)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        mockMvc.perform(get(URL_PREFIX + "/tenants/101/my_damages")
                .params(params))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetMyDamagesByNotAuthenticatedTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode getMyDamages kontrolera DamageController
        * (dobavljanje liste svih kvarova koje je prijavio stanar sa zadatim ID-em)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu stanara
        * */

        mockMvc.perform(get(URL_PREFIX + "/tenants/101/my_damages")
                .header("Authentication-Token", this.accessTokenInstitution_1)
                .params(params))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetMyDamagesNonexistentTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode getMyDamages kontrolera DamageController
        * (dobavljanje liste svih kvarova koje je prijavio stanar sa zadatim ID-em)
        * u slucaju kada ne postoji stanar sa zadatim Id-em
        * */

        // ne postoji stanar sa ID-em = 200
        mockMvc.perform(get(URL_PREFIX + "/tenants/200/my_damages")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .params(params))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetMyDamagesInvalidTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode getMyDamages kontrolera DamageController
        * (dobavljanje liste svih kvarova koje je prijavio stanar sa zadatim ID-em)
        * u slucaju kada ulogovani stanar nema isti ID kao sto je ID zadat u URL-u
        * */

        mockMvc.perform(get(URL_PREFIX + "/tenants/102/my_damages")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .params(params))
                .andExpect(status().isForbidden());
    }

    // TESTOVI za deleteMyDamage
    @Test
    @Transactional
    @Rollback(true)
    public void testDeleteMyDamage() throws Exception {
        /*
        * Test proverava ispravnost rada metode deleteMyDamage kontrolera DamageController
        * (brisanje konkretnog kvara koji je prijavio stanar sa zadatim ID-em)
        * */

        /*
        stanar sa ID-em = 101 je prijavio kvar sa ID-em = 101 i sledecim podacima:
            id = 101,
            description = second damage description,
            building_id = 100,
            apartment_id = 101
        */
        mockMvc.perform(delete(URL_PREFIX + "/tenants/101/my_damages/101")
                .header("Authentication-Token", this.accessTokenTenant_2))
                .andExpect(status().isOk());
        // obrisani resurs vise ne postoji u bazi
        mockMvc.perform(get(URL_PREFIX + "/tenants/101/damages/101")
                .header("Authentication-Token", this.accessTokenTenant_2))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeleteMyDamageByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode deleteMyDamage kontrolera DamageController
        * (brisanje konkretnog kvara koji je prijavio stanar sa zadatim ID-em)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        mockMvc.perform(delete(URL_PREFIX + "/tenants/101/my_damages/101"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDeleteMyDamageByNotAuthenticatedTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode deleteMyDamage kontrolera DamageController
        * (brisanje konkretnog kvara koji je prijavio stanar sa zadatim ID-em)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu stanara
        * */

        mockMvc.perform(delete(URL_PREFIX + "/tenants/101/my_damages/101")
                .header("Authentication-Token", this.accessTokenInstitution_1))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDeleteMyDamageNonexistentTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode deleteMyDamage kontrolera DamageController
        * (brisanje konkretnog koji je prijavio stanar sa zadatim ID-em)
        * u slucaju kada ne postoji stanar sa zadatim ID-em
        * */

        // ne postoji stanar sa ID-em = 200
        mockMvc.perform(delete(URL_PREFIX + "/tenants/200/my_damages/101")
                .header("Authentication-Token", this.accessTokenTenant_2))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeleteMyDamageNonexistentDamage() throws Exception {
        /*
        * Test proverava ispravnost rada metode deleteMyDamage kontrolera DamageController
        * (brisanje konkretnog kvara koji je prijavio stanar sa zadatim ID-em)
        * u slucaju kada ne postoji kvar sa zadatim ID-em
        * */

        // ne postoji kvar sa ID-em = 200
        mockMvc.perform(delete(URL_PREFIX + "/tenants/101/my_damages/200")
                .header("Authentication-Token", this.accessTokenTenant_2))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeleteMyDamageInvalidTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode deleteMyDamage kontrolera DamageController
        * (brisanje konkretnog kvara koji je prijavio stanar sa zadatim ID-em)
        * u slucaju kada ulogovani stanar nema isti ID kao sto je ID zadat u URL-u
        * */

        mockMvc.perform(delete(URL_PREFIX + "/tenants/102/my_damages/101")
                .header("Authentication-Token", this.accessTokenTenant_2))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDeleteMyDamageTenantNotCreator() throws Exception {
        /*
        * Test proverava ispravnost rada metode deleteMyDamage kontrolera DamageController
        * (brisanje konkretnog kvara koji je prijavio stanar sa zadatim ID-em)
        * u slucaju kada ulogovani stanar nije prijavio kvar sa zadatim ID-em
        * */

        // kvar sa ID-em = 103 nije prijavio stanar sa ID-em = 101
        mockMvc.perform(delete(URL_PREFIX + "/tenants/101/my_damages/103")
                .header("Authentication-Token", this.accessTokenTenant_2))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDeleteMyDamageAlreadyInRepairProccess() throws Exception {
        /*
        * Test proverava ispravnost rada metode deleteMyDamage kontrolera DamageController
        * (brisanje konkretnog kvara koji je prijavio stanar sa zadatim ID-em)
        * u slucaju kada je kvar sa zadatim ID-em vec u procesu otklanjanja - dodeljena mu je firma/institucija za resavanje
        * */

        // kvar sa ID-em = 104 je vec u procesu otklanjanja, dodeljena mu je firma sa ID-em = 102 zaduzena za otklanjanje
        mockMvc.perform(delete(URL_PREFIX + "/tenants/101/my_damages/104")
                .header("Authentication-Token", this.accessTokenTenant_2))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za updateMyDamage
    @Test
    @Transactional
    @Rollback(true)
    public void testUpdateMyDamage() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateMyDamage kontrolera DamageController
        * (azuriranje konkretnog kvara koji je prijavio stanar sa zadatim ID-em)
        * */

        DamageUpdateDTO damageUpdateDTO = new DamageUpdateDTO("Description updated!", false);

        String damageUpdateDTOJson = TestUtil.json(damageUpdateDTO);
        /*
        stanar sa ID-em = 101 je prijavio kvar sa ID-em = 101 i sledecim podacima:
            id = 101,
            description = second damage description,
            urgent = false,
            building_id = 100,
            apartment_id = 101
        */
        mockMvc.perform(put(URL_PREFIX + "/tenants/101/my_damages/101")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(damageUpdateDTOJson))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(101))
                .andExpect(jsonPath("$.description").value("Description updated!"))
                .andExpect(jsonPath("$.buildingId").value(100))
                .andExpect(jsonPath("$.apartmentId").value(101))
                .andExpect(jsonPath("$.creator.id").value(101));
    }

    @Test
    public void testUpdateMyDamageByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateMyDamage kontrolera DamageController
        * (azuriranje konkretnog kvara koji je prijavio stanar sa zadatim ID-em)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        DamageUpdateDTO damageUpdateDTO = new DamageUpdateDTO("Description updated!", false);

        String damageUpdateDTOJson = TestUtil.json(damageUpdateDTO);

        mockMvc.perform(put(URL_PREFIX + "/tenants/101/my_damages/101")
                .contentType(contentType)
                .content(damageUpdateDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testUpdateMyDamageByNotAuthenticatedTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateMyDamage kontrolera DamageController
        * (azuriranje konkretnog kvara koji je prijavio stanar sa zadatim ID-em)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu stanara
        * */

        DamageUpdateDTO damageUpdateDTO = new DamageUpdateDTO("Description updated!", false);

        String damageUpdateDTOJson = TestUtil.json(damageUpdateDTO);

        mockMvc.perform(put(URL_PREFIX + "/tenants/101/my_damages/101")
                .header("Authentication-Token", this.accessTokenInstitution_1)
                .contentType(contentType)
                .content(damageUpdateDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testUpdateMyDamageNonexistentTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateMyDamage kontrolera DamageController
        * (azuriranje konkretnog koji je prijavio stanar sa zadatim ID-em)
        * u slucaju kada ne postoji stanar sa zadatim ID-em
        * */

        DamageUpdateDTO damageUpdateDTO = new DamageUpdateDTO("Description updated!", false);

        String damageUpdateDTOJson = TestUtil.json(damageUpdateDTO);

        // ne postoji stanar sa ID-em = 200
        mockMvc.perform(put(URL_PREFIX + "/tenants/200/my_damages/101")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(damageUpdateDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testUpdateMyDamageNonexistentDamage() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateMyDamage kontrolera DamageController
        * (azuriranje konkretnog kvara koji je prijavio stanar sa zadatim ID-em)
        * u slucaju kada ne postoji kvar sa zadatim ID-em
        * */

        DamageUpdateDTO damageUpdateDTO = new DamageUpdateDTO("Description updated!", false);

        String damageUpdateDTOJson = TestUtil.json(damageUpdateDTO);

        // ne postoji kvar sa ID-em = 200
        mockMvc.perform(put(URL_PREFIX + "/tenants/101/my_damages/200")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(damageUpdateDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testUpdateMyDamageInvalidTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateMyDamage kontrolera DamageController
        * (azuriranje konkretnog kvara koji je prijavio stanar sa zadatim ID-em)
        * u slucaju kada ulogovani stanar nema isti ID kao sto je ID zadat u URL-u
        * */

        DamageUpdateDTO damageUpdateDTO = new DamageUpdateDTO("Description updated!", false);

        String damageUpdateDTOJson = TestUtil.json(damageUpdateDTO);

        mockMvc.perform(put(URL_PREFIX + "/tenants/102/my_damages/101")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(damageUpdateDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testUpdateMyDamageTenantNotCreator() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateMyDamage kontrolera DamageController
        * (azuriranje konkretnog kvara koji je prijavio stanar sa zadatim ID-em)
        * u slucaju kada ulogovani stanar nije prijavio kvar sa zadatim ID-em
        * */

        DamageUpdateDTO damageUpdateDTO = new DamageUpdateDTO("Description updated!", false);

        String damageUpdateDTOJson = TestUtil.json(damageUpdateDTO);

        // kvar sa ID-em = 103 nije prijavio stanar sa ID-em = 101
        mockMvc.perform(put(URL_PREFIX + "/tenants/101/my_damages/103")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(damageUpdateDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testUpdateMyDamageAlreadyInRepairProccess() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateMyDamage kontrolera DamageController
        * (azuriranje konkretnog kvara koji je prijavio stanar sa zadatim ID-em)
        * u slucaju kada je kvar sa zadatim ID-em vec u procesu otklanjanja - dodeljena mu je firma/institucija za resavanje
        * */

        DamageUpdateDTO damageUpdateDTO = new DamageUpdateDTO("Description updated!", false);

        String damageUpdateDTOJson = TestUtil.json(damageUpdateDTO);

        // kvar sa ID-em = 104 je vec u procesu otklanjanja, dodeljena mu je firma sa ID-em = 102 zaduzena za otklanjanje
        mockMvc.perform(put(URL_PREFIX + "/tenants/101/my_damages/104")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(damageUpdateDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateMyDamageMissingDescription() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateMyDamage kontrolera DamageController
        * (azuriranje konkretnog kvara koji je prijavio stanar sa zadatim ID-em)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne sadrzi sva neophodna polja - nedostaje polje description
        * */

        // nedostaje vrednost polja description
        DamageUpdateDTO damageUpdateDTO = new DamageUpdateDTO(null, false);

        String damageUpdateDTOJson = TestUtil.json(damageUpdateDTO);

        mockMvc.perform(put(URL_PREFIX + "/tenants/101/my_damages/101")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(damageUpdateDTOJson))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za getAllDamagesForPresident
    @Test
    public void testGetAllDamagesForPresident() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllDamagesForPresident kontrolera DamageController
        * (dobavljanje liste svih kvarova u zgradi u kojoj je trenutno logovani korisnik predsednik skupstine stanara)
        * */

        /*
        stanar sa ID-em = 101 je predsednik skupstine stanara u zgradi sa ID-em = 100, kojoj pripadaju sledeci kvarovi
            1 - id = 100,
                description = first damage description,
                apartment_id = 102,
                building_id = 100
            2 - id = 101,
                description = second damage description,
                apartment_id = 101,
                building_id = 100,
            3 - id = 102,
                description = third damage description,
                apartment_id = 101,
                building_id = 100
            4 - id = 103
                description = forth damage description,
                apartment_id = 101,
                building_id = 100
            5 - id = 104,
                description = fifth damage description,
                apartment_id = 101,
                building_id = 100
        */
        mockMvc.perform(get(URL_PREFIX + "/buildings/100/damages")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .params(params))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.[*]", hasSize(5)))
                .andExpect(jsonPath("$.[*].id", containsInAnyOrder(100, 101,102,103,104)))
                .andExpect(jsonPath("$.[*].description", containsInAnyOrder("first damage description", "second damage description",
                        "third damage description", "forth damage description", "fifth damage description")));
    }

    @Test
    public void testGetAllDamagesForPresidentByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllDamagesForPresident kontrolera DamageController
        * (dobavljanje liste svih kvarova u zgradi u kojoj je trenutno logovani korisnik predsednik skupstine stanara)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        mockMvc.perform(get(URL_PREFIX + "/buildings/100/damages")
                .params(params))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetAllDamagesForPresidentByNotAuthenticatedPresident() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllDamagesForPresident kontrolera DamageController
        * (dobavljanje liste svih kvarova u zgradi u kojoj je trenutno logovani korisnik predsednik skupstine stanara)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu predsednika skupstine stanara
        * */

        mockMvc.perform(get(URL_PREFIX + "/buildings/100/damages")
                .header("Authentication-Token", this.accessTokenInstitution_1)
                .params(params))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetAllDamagesForPresidentNonexistentBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllDamagesForPresident kontrolera DamageController
        * (dobavljanje liste svih kvarova u zgradi u kojoj je trenutno logovani korisnik predsednik skupstine stanara)
        * u slucaju kada ne postoji zgrada sa zadatim ID-em
        * */

        // ne postoji zgrada sa ID-em = 200
        mockMvc.perform(get(URL_PREFIX + "/buildings/200/damages")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .params(params))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetAllDamagesForPresidentInvalidTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllDamagesForPresident kontrolera DamageController
        * (dobavljanje liste svih kvarova u zgradi u kojoj je trenutno logovani korisnik predsednik skupstine stanara)
        * u slucaju kada ulogovani predsednik nije predsednik skupstine stanara u zgradi sa zadatim ID-em
        * */

        // stanar sa ID-em = 107 je predsednik skupstine stanara u zgradi sa ID-em = 101 (a ne sa ID-em = 100)
        mockMvc.perform(get(URL_PREFIX + "/buildings/100/damages")
                .header("Authentication-Token", this.accessTokenTenant_4)
                .params(params))
                .andExpect(status().isForbidden());
    }

    // TESTOVI za getDamageForPresident
    @Test
    public void testGetDamageForPresident() throws Exception {
        /*
        * Test proverava ispravnost rada metode getDamageForPresident kontrolera DamageController
        * (dobavljanje konkretnog kvara za koji je stanar sa zadatim ID-em odgovorna osoba
        * ili je taj kvar prijavljen u nekom od stanova ciji je on vlasnik
        * ili je taj kvar prijavljen u zgradi u kojoj je posmatrani stanar predsednik skupstine stanara
        * ili je u pitanju kvar kog je kreirao posmatrani stanar)
        * u slucaju kada trenutno logovani predsednik dobavlja kvar koji se nalazi u zgradi ciji je on
        * predsednik skupstine stanara
        * */

        /*
        stanar sa ID-em = 101 je predsednik skupstine stanara u zgradi sa ID-em = 100 kojoj pripada kvar sa ID-em = 101 i sledecim podacima:
            id = 101,
            description = second damage description,
            building_id = 100,
            apartment_id = 101 (stan ciji je vlasnik stanar sa ID-em = 101)
        */
        mockMvc.perform(get(URL_PREFIX + "/presidents/101/damages/101")
                .header("Authentication-Token", this.accessTokenTenant_2))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(101))
                .andExpect(jsonPath("$.description").value("second damage description"))
                .andExpect(jsonPath("$.buildingId").value(100))
                .andExpect(jsonPath("$.apartmentId").value(101));
    }

    @Test
    public void testGetDamageForPresidentByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode getDamageForPresident kontrolera DamageController
        * (dobavljanje konkretnog kvara za koji je stanar sa zadatim ID-em odgovorna osoba
        * ili je taj kvar prijavljen u nekom od stanova ciji je on vlasnik
        * ili je taj kvar prijavljen u zgradi u kojoj je posmatrani stanar predsednik skupstine stanara
        * ili je u pitanju kvar kog je kreirao posmatrani stanar)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        mockMvc.perform(get(URL_PREFIX + "/presidents/101/damages/101"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetDamageForPresidentByNotAuthenticatedPresident() throws Exception {
        /*
        * Test proverava ispravnost rada metode getDamageForPresident kontrolera DamageController
        * (dobavljanje konkretnog kvara za koji je stanar sa zadatim ID-em odgovorna osoba
        * ili je taj kvar prijavljen u nekom od stanova ciji je on vlasnik
        * ili je taj kvar prijavljen u zgradi u kojoj je posmatrani stanar predsednik skupstine stanara
        * ili je u pitanju kvar kog je kreirao posmatrani stanar)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu predsednik skupstine stanara
        * */

        mockMvc.perform(get(URL_PREFIX + "/presidents/101/damages/101")
                .header("Authentication-Token", this.accessTokenInstitution_1))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetDamageForPresidentNonexistentPresident() throws Exception {
        /*
        * Test proverava ispravnost rada metode getDamageForPresident kontrolera DamageController
        * (dobavljanje konkretnog kvara za koji je stanar sa zadatim ID-em odgovorna osoba
        * ili je taj kvar prijavljen u nekom od stanova ciji je on vlasnik
        * ili je taj kvar prijavljen u zgradi u kojoj je posmatrani stanar predsednik skupstine stanara
        * ili je u pitanju kvar kog je kreirao posmatrani stanar)
        * u slucaju kada ne postoji stanar (predsednik skupstine stanara) sa zadatim ID-em
        * */

        // ne postoji stanar (predsednik skupstine stanara) sa ID-em = 200
        mockMvc.perform(get(URL_PREFIX + "/presidents/200/damages/101")
                .header("Authentication-Token", this.accessTokenTenant_2))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetDamageForPresidentNonexistentDamage() throws Exception {
        /*
        * Test proverava ispravnost rada metode getDamageForPresident kontrolera DamageController
        * (dobavljanje konkretnog kvara za koji je stanar sa zadatim ID-em odgovorna osoba
        * ili je taj kvar prijavljen u nekom od stanova ciji je on vlasnik
        * ili je taj kvar prijavljen u zgradi u kojoj je posmatrani stanar predsednik skupstine stanara
        * ili je u pitanju kvar kog je kreirao posmatrani stanar)
        * u slucaju kada ne postoji kvar sa zadatim ID-em
        * */

        // ne postoji kvar sa ID-em = 200
        mockMvc.perform(get(URL_PREFIX + "/presidents/101/damages/200")
                .header("Authentication-Token", this.accessTokenTenant_2))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetDamageForPresidentNotInAnyOfCollections() throws Exception {
        /*
        * Test proverava ispravnost rada metode getDamageForPresident kontrolera DamageController
        * (dobavljanje konkretnog kvara za koji je stanar sa zadatim ID-em odgovorna osoba
        * ili je taj kvar prijavljen u nekom od stanova ciji je on vlasnik
        * ili je taj kvar prijavljen u zgradi u kojoj je posmatrani stanar predsednik skupstine stanara
        * ili je u pitanju kvar kog je kreirao posmatrani stanar)
        * u slucaju kada ulogovani predsednik nije ujedno i odgovorna osoba za kvar sa zadatim ID-em niti kvar
        * pripada licnim stanovima predsednika niti ga je predsednik kreirao niti se nalazi u zgradi ciji je predsednik
        * trenutno logovani stanar
        * */

        /*
        kvar sa ID-em = 105 pripada zgradi sa ID-em = 101 (ciji predsednik nije stanar sa ID-em = 101),
        kreirao ga je stanar sa ID-em = 107 i nalazi se u stanu sa ID-em = 103 i nema odgovornu osobu
        */
        mockMvc.perform(get(URL_PREFIX + "/presidents/101/damages/105")
                .header("Authentication-Token", this.accessTokenTenant_2))
                .andExpect(status().isForbidden());
    }

    // TESTOVI za reportDamage
    @Test
    @Transactional
    @Rollback(true)
    public void testReportDamageResponsiblePersonForDamageTypeExists() throws Exception {
        /*
        * Test proverava ispravnost rada metode reportDamage kontrolera DamageController
        * (prijavljivanje novog kvara na nivou zgrade sa zadatim ID-em)
        * u slucaju kada vec postoji stanar na nivou zgrade sa zadatim ID-em koji je odgovorna osoba za odredjeni tip kvara
        * pa ce se taj stanar proglasiti odgovornom osobom za novokreirani kvar
        * */

        DamageCreateDTO damageCreateDTO = new DamageCreateDTO("Description for some damage", "GAS_DAMAGE",
                null, false);

        String damageDTOJson = TestUtil.json(damageCreateDTO);

        /*
        na nivou zgrade sa ID-em = 100 stanar sa ID-em = 102 (firstName = Nikola, lastName = Garabandic)
        je odgovorna osoba za tip kvara sa ID-em = 3 (name = GAS_DAMAGE)
        */
        this.mockMvc.perform(post(URL_PREFIX + "/buildings/100/damages")
                .header("Authentication-Token", this.accessTokenTenant_1)
                .contentType(contentType)
                .content(damageDTOJson))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.description").value("Description for some damage"))
                .andExpect(jsonPath("$.typeName").value("GAS_DAMAGE"))
                .andExpect(jsonPath("$.buildingId").value(100))
                .andExpect(jsonPath("$.responsiblePerson.id").value(102));
    }

    @Test
    @Transactional
    @Rollback(true)
    public void testReportDamageResponsibleInstitutionForDamageTypeExists() throws Exception {
        /*
        * Test proverava ispravnost rada metode reportDamage kontrolera DamageController
        * (prijavljivanje novog kvara na nivou zgrade sa zadatim ID-em)
        * u slucaju kada vec postoji instittucija koja je odgovorna za odredjeni tip kvara na nivou zgrade sa zadatim ID-em
        * pa ce se ta institucija proglasiti odgovornom za novokreirani kvar
        * */

        DamageCreateDTO damageCreateDTO = new DamageCreateDTO("Description for some damage", "ELECTRICAL_DAMAGE",
                null, false);

        String damageDTOJson = TestUtil.json(damageCreateDTO);

        /*
        na nivou zgrade sa ID-em = 100 institucija sa ID-em = 103
        je odgovorna institucija za tip kvara sa ID-em = 2 (name = ELECTRICAL_DAMAGE)
        */
        this.mockMvc.perform(post(URL_PREFIX + "/buildings/100/damages")
                .header("Authentication-Token", this.accessTokenTenant_1)
                .contentType(contentType)
                .content(damageDTOJson))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.description").value("Description for some damage"))
                .andExpect(jsonPath("$.typeName").value("ELECTRICAL_DAMAGE"))
                .andExpect(jsonPath("$.buildingId").value(100))
                .andExpect(jsonPath("$.responsibleInstitution.id").value(103));
    }

    @Test
    @Transactional
    @Rollback(true)
    public void testReportDamageNoResponsiblePersonAndInstitutionForDamageType() throws Exception {
        /*
        * Test proverava ispravnost rada metode reportDamage kontrolera DamageController
        * (prijavljivanje novog kvara na nivou zgrade sa zadatim ID-em)
        * u slucaju kada ne postoje ni stanar ni institucija koja je odgovorna za odredjeni tip kvara na nivou zgrade
        * sa zadatim ID-em, ali zgrada sa zadatim ID-em ima svog predsednika skupstine stanara koji ce se proglasiti
        * odgovornom osobom za novo kreirani kvar
        * */

        DamageCreateDTO damageCreateDTO = new DamageCreateDTO("Description for some damage", "OTHER",
                null, false);

        String damageDTOJson = TestUtil.json(damageCreateDTO);

        /*
        na nivou zgrade sa ID-em = 100 ne postoji odgovorna osoba niti odgovorna institucija
        tip kvara sa ID-em = 5 (name = OTHER), pa je odgovorna osoba za novokreirani kvar predsednik skupstine stanara
        u zgradi, a to je stanar sa ID-em = 101
        */
        this.mockMvc.perform(post(URL_PREFIX + "/buildings/100/damages")
                .header("Authentication-Token", this.accessTokenTenant_1)
                .contentType(contentType)
                .content(damageDTOJson))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.description").value("Description for some damage"))
                .andExpect(jsonPath("$.typeName").value("OTHER"))
                .andExpect(jsonPath("$.buildingId").value(100))
                .andExpect(jsonPath("$.responsiblePerson.id").value(101));
    }

    @Test
    @Transactional
    @Rollback(true)
    public void testReportDamageNoResponsiblePersonAndInstitutionAndPresident() throws Exception {
        /*
        * Test proverava ispravnost rada metode reportDamage kontrolera DamageController
        * (prijavljivanje novog kvara na nivou zgrade sa zadatim ID-em)
        * u slucaju kada ne postoje ni stanar ni institucija koja je odgovorna za odredjeni tip kvara na nivou zgrade
        * sa zadatim ID-em, a pri tom zgrada jos uvek nema ni svog predsednika skupstine stanara, pa ce se sam
        * stanar koji prijavljuje kvar proglasiti odgovornom osobom za novokreirani kvar
        * */

        DamageCreateDTO damageCreateDTO = new DamageCreateDTO("Description for some damage", "PLUMBING_DAMAGE",
                null, false);

        String damageDTOJson = TestUtil.json(damageCreateDTO);

        /*
        na nivou zgrade sa ID-em = 102 ne postoji odgovorna osoba niti odgovorna institucija
        tip kvara sa ID-em = 1 (name = PLUMBING_DAMAGE), kao ni predsednik skuptine stanara
        pa je odgovorna osoba za novokreirani kvar sam stanar koji prijavljuje kvar, stanar sa ID-em = 110
        */
        this.mockMvc.perform(post(URL_PREFIX + "/buildings/102/damages")
                .header("Authentication-Token", this.accessTokenTenant_5)
                .contentType(contentType)
                .content(damageDTOJson))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.description").value("Description for some damage"))
                .andExpect(jsonPath("$.typeName").value("PLUMBING_DAMAGE"))
                .andExpect(jsonPath("$.buildingId").value(102))
                .andExpect(jsonPath("$.responsiblePerson.id").value(110));
    }

    @Test
    public void testReportDamageByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode reportDamage kontrolera DamageController
        * (prijavljivanje novog kvara na nivou zgrade sa zadatim ID-em)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        DamageCreateDTO damageCreateDTO = new DamageCreateDTO("Description for some damage", "PLUMBING_DAMAGE",
                null, false);

        String damageDTOJson = TestUtil.json(damageCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/buildings/100/damages")
                .contentType(contentType)
                .content(damageDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testReportDamageByNotAuthenticatedTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode reportDamage kontrolera DamageController
        * (prijavljivanje novog kvara na nivou zgrade sa zadatim ID-em)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu stanara
        * */

        DamageCreateDTO damageCreateDTO = new DamageCreateDTO("Description for some damage", "PLUMBING_DAMAGE",
                null, false);

        String damageDTOJson = TestUtil.json(damageCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/buildings/100/damages")
                .header("Authentication-Token", this.accessTokenInstitution_1)
                .contentType(contentType)
                .content(damageDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testReportDamageNonexistentBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode reportDamage kontrolera DamageController
        * (prijavljivanje novog kvara na nivou zgrade sa zadatim ID-em)
        * u slucaju kada ne postoji zgrada sa zadatim ID-em
        * */

        DamageCreateDTO damageCreateDTO = new DamageCreateDTO("Description for some damage", "PLUMBING_DAMAGE",
                null, false);

        String damageDTOJson = TestUtil.json(damageCreateDTO);

        // ne postoji zgrada sa ID-em = 200
        this.mockMvc.perform(post(URL_PREFIX + "/buildings/200/damages")
                .header("Authentication-Token", this.accessTokenTenant_1)
                .contentType(contentType)
                .content(damageDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testReportDamageTenantWithoutApartment() throws Exception {
        /*
        * Test proverava ispravnost rada metode reportDamage kontrolera DamageController
        * (prijavljivanje novog kvara na nivou zgrade sa zadatim ID-em)
        * u slucaju kada trenutno logovani stanar nije povezan ni sa jednim stanom
        * */

        DamageCreateDTO damageCreateDTO = new DamageCreateDTO("Description for some damage", "PLUMBING_DAMAGE",
                null, false);

        String damageDTOJson = TestUtil.json(damageCreateDTO);

        // stanar sa ID-em = 109 nije povezan ni sa jednim stanom
        this.mockMvc.perform(post(URL_PREFIX + "/buildings/100/damages")
                .header("Authentication-Token", this.accessTokenTenant_3)
                .contentType(contentType)
                .content(damageDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testReportDamageTenantFromInvalidBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode reportDamage kontrolera DamageController
        * (prijavljivanje novog kvara na nivou zgrade sa zadatim ID-em)
        * u slucaju kada trenutno logovani stanar ne zivi u zgradi sa zadatim ID-em
        * */

        DamageCreateDTO damageCreateDTO = new DamageCreateDTO("Description for some damage", "PLUMBING_DAMAGE",
                null, false);

        String damageDTOJson = TestUtil.json(damageCreateDTO);

        // stanar sa ID-em = 103 zivi u zgradi sa ID-em = 100 (a ne sa ID-em = 101)
        this.mockMvc.perform(post(URL_PREFIX + "/buildings/101/damages")
                .header("Authentication-Token", this.accessTokenTenant_1)
                .contentType(contentType)
                .content(damageDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testReportDamageNonexistentApartment() throws Exception {
        /*
        * Test proverava ispravnost rada metode reportDamage kontrolera DamageController
        * (prijavljivanje novog kvara na nivou zgrade sa zadatim ID-em)
        * u slucaju kada se u dolaznom DTO objektu nalazi polje apartmentId i pri tom ne postoji
        * nijedan stan sa zadatim ID-em
        * */

        // ne postoji stan sa ID-em = 200
        DamageCreateDTO damageCreateDTO = new DamageCreateDTO("Description for some damage", "PLUMBING_DAMAGE",
                200L, false);

        String damageDTOJson = TestUtil.json(damageCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/buildings/100/damages")
                .header("Authentication-Token", this.accessTokenTenant_1)
                .contentType(contentType)
                .content(damageDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testReportDamageApartmentFromInvalidBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode reportDamage kontrolera DamageController
        * (prijavljivanje novog kvara na nivou zgrade sa zadatim ID-em)
        * u slucaju kada se u dolaznom DTO objektu nalazi polje apartmentId i pri tom stan
        * sa zadatim ID-em ne pripada zgradi sa zadatim ID-em
        * */

        // stan sa ID-em = 103 pripada zgradi sa ID-em = 101 (a ne sa ID-em = 100)
        DamageCreateDTO damageCreateDTO = new DamageCreateDTO("Description for some damage", "PLUMBING_DAMAGE",
                103L, false);

        String damageDTOJson = TestUtil.json(damageCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/buildings/100/damages")
                .header("Authentication-Token", this.accessTokenTenant_1)
                .contentType(contentType)
                .content(damageDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testReportDamageNonexistentDamageType() throws Exception {
        /*
        * Test proverava ispravnost rada metode reportDamage kontrolera DamageController
        * (prijavljivanje novog kvara na nivou zgrade sa zadatim ID-em)
        * u slucaju kada ne postoji tip kvara sa imenom koje je prosledjeno u dolaznom DTO objektu
        * */

        // ne postoji tip kvara sa nazivom GRESKA
        DamageCreateDTO damageCreateDTO = new DamageCreateDTO("Description for some damage", "GRESKA",
                null, false);

        String damageDTOJson = TestUtil.json(damageCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/buildings/100/damages")
                .header("Authentication-Token", this.accessTokenTenant_1)
                .contentType(contentType)
                .content(damageDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testReportDamageMissingDescription() throws Exception {
        /*
        * Test proverava ispravnost rada metode reportDamage kontrolera DamageController
        * (prijavljivanje novog kvara na nivou zgrade sa zadatim ID-em)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne sadrzi sva neophodna polja - nedostaje polje description
        * */

        // nedostaje vrednost polja description
        DamageCreateDTO damageCreateDTO = new DamageCreateDTO(null, "PLUMBING_DAMAGE",
                null, false);

        String damageDTOJson = TestUtil.json(damageCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/buildings/100/damages")
                .header("Authentication-Token", this.accessTokenTenant_1)
                .contentType(contentType)
                .content(damageDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testReportDamageMissingTypeName() throws Exception {
        /*
        * Test proverava ispravnost rada metode reportDamage kontrolera DamageController
        * (prijavljivanje novog kvara na nivou zgrade sa zadatim ID-em)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne sadrzi sva neophodna polja - nedostaje polje typeName
        * */

        // nedostaje vrednost polja typeName
        DamageCreateDTO damageCreateDTO = new DamageCreateDTO("Description for some damage", null,
                null, false);

        String damageDTOJson = TestUtil.json(damageCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/buildings/100/damages")
                .header("Authentication-Token", this.accessTokenTenant_1)
                .contentType(contentType)
                .content(damageDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testReportDamageInappropriateTypeOfDescription() throws Exception {
        /*
        * Test proverava ispravnost rada metode reportDamage kontrolera DamageController
        * (prijavljivanje novog kvara na nivou zgrade sa zadatim ID-em)
        * u slucaju kada je dolazni DTO objekat nevalidan: sadrzi sva neophodna polja, ali je tip polja description nevalidan
        * */

        String damageDTOJson = "{" +
                    "description:1," +
                    "typeName:'OTHER'," +
                    "apartmentId:null," +
                    "urgent:false" +
                "}";

        this.mockMvc.perform(post(URL_PREFIX + "/buildings/100/damages")
                .header("Authentication-Token", this.accessTokenTenant_1)
                .contentType(contentType)
                .content(damageDTOJson))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za setResponsibleInstitutionForDamage
    @Test
    @Transactional
    @Rollback(true)
    public void testSetResponsibleInstitutionForDamage() throws Exception {
       /*
        * Test proverava ispravnost rada metode setResponsibleInstitutionForDamage kontrolera DamageController
        * (postavljanje odgovorne institucije za odredjeni kvar na nivou zadate zgrade od strane predsednika skupstine stanara)
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(102L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        /*
        u zgradi sa ID-em = 100 ne postoji odgovorna institucija za kvar sa ID-em = 100 i sledecim podacima:
            id = 100,
            description = first damage description,
            building_id = 100,
            responsible_institution_id = null
        pa se sada ta odgovornost dodeljuje instituciji sa ID-em 102
        */
        this.mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/100/responsible_institution")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(100))
                .andExpect(jsonPath("$.description").value("first damage description"))
                .andExpect(jsonPath("$.buildingId").value(100))
                .andExpect(jsonPath("$.responsibleInstitution.id").value(102));
    }

    @Test
    public void testSetResponsibleInstitutionForDamageByNotNotAuthenticatedUser() throws Exception {
       /*
        * Test proverava ispravnost rada metode setResponsibleInstitutionForDamage kontrolera DamageController
        * (postavljanje odgovorne institucije za odredjeni kvar na nivou zadate zgrade od strane predsednika skupstine stanara)
        * u slucaju kada akciju obavlja nelogovani korisnik
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(102L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        this.mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/100/responsible_institution")
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSetResponsibleInstitutionForDamageByNotAuthenticatedPresident() throws Exception {
       /*
        * Test proverava ispravnost rada metode setResponsibleInstitutionForDamage kontrolera DamageController
        * (postavljanje odgovorne institucije za odredjeni kvar na nivou zadate zgrade od strane predsednika skupstine stanara)
        * u slucaju kada akciju obavlja ulogovani korisnik koji nema ulogu predsednika skupstine stanara
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(102L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        this.mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/100/responsible_institution")
                .header("Authentication-Token", this.accessTokenInstitution_1)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSetResponsibleInstitutionForDamageNonexistentBuilding() throws Exception {
       /*
        * Test proverava ispravnost rada metode setResponsibleInstitutionForDamage kontrolera DamageController
        * (postavljanje odgovorne institucije za odredjeni kvar na nivou zadate zgrade od strane predsednika skupstine stanara)
        * u slucaju kada ne postoji zgrada sa zadatim ID-em
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(102L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // ne postoji zgrada sa ID-em = 200
        this.mockMvc.perform(patch(URL_PREFIX + "/buildings/200/damages/100/responsible_institution")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testSetResponsibleInstitutionForDamageNonexistentDamage() throws Exception {
       /*
        * Test proverava ispravnost rada metode setResponsibleInstitutionForDamage kontrolera DamageController
        * (postavljanje odgovorne institucije za odredjeni kvar na nivou zadate zgrade od strane predsednika skupstine stanara)
        * u slucaju kada ne postoji kvar sa zadatim ID-em
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(102L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // ne postoji kvar sa ID-em = 200
        this.mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/200/responsible_institution")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testSetResponsibleInstitutionForDamageInvalidBuilding() throws Exception {
       /*
        * Test proverava ispravnost rada metode setResponsibleInstitutionForDamage kontrolera DamageController
        * (postavljanje odgovorne institucije za odredjeni kvar na nivou zadate zgrade od strane predsednika skupstine stanara)
        * u slucaju kada kvar sa zadatim ID-em ne pripada zgradi sa zadatim ID-em
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(102L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // kvar sa ID-em = 105 pripada zgradi sa ID-em = 101 (a ne sa ID-em = 100)
        this.mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/105/responsible_institution")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testSetResponsibleInstitutionForDamageAlreadyHasResponsibleInstitution() throws Exception {
       /*
        * Test proverava ispravnost rada metode setResponsibleInstitutionForDamage kontrolera DamageController
        * (postavljanje odgovorne institucije za odredjeni kvar na nivou zadate zgrade od strane predsednika skupstine stanara)
        * u slucaju kada kvar sa zadatim ID-em vec poseduje odgovornu instituciju
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(102L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // kvar sa ID-em = 103 u zgradi sa ID-em = 103 vec poseduje odgovornu instituciju (sa ID-em = 103)
        this.mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/103/responsible_institution")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testSetResponsibleInstitutionForDamagePresidentFromInvalidBuilding() throws Exception {
       /*
        * Test proverava ispravnost rada metode setResponsibleInstitutionForDamage kontrolera DamageController
        * (postavljanje odgovorne institucije za odredjeni kvar na nivou zadate zgrade od strane predsednika skupstine stanara)
        * u slucaju kada trenutno ulogovani predsednik skupstine stanara nije ujedno i predsednik skupstine stanara u zgradi sa zadatim ID-em
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(102L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // stanar koji je trenutno logovan je predsednik skupstine stanara u zgradi sa ID-em = 101 (a ne ID-em = 100)
        this.mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/100/responsible_institution")
                .header("Authentication-Token", this.accessTokenTenant_4)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSetResponsibleInstitutionForDamageNonexistentInstitution() throws Exception {
       /*
        * Test proverava ispravnost rada metode setResponsibleInstitutionForDamage kontrolera DamageController
        * (postavljanje odgovorne institucije za odredjeni kvar na nivou zadate zgrade od strane predsednika skupstine stanara)
        * u slucaju kada ne postoji institucija sa zadatim ID-em u DTO-u
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(200L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // ne postoji institucija sa ID-em = 200
        this.mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/100/responsible_institution")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testSetResponsibleInstitutionForDamageNotInstitution() throws Exception {
       /*
        * Test proverava ispravnost rada metode setResponsibleInstitutionForDamage kontrolera DamageController
        * (postavljanje odgovorne institucije za odredjeni kvar na nivou zadate zgrade od strane predsednika skupstine stanara)
        * u slucaju kada torka kojoj pripada ID zadat u DTO-u nije institucija, nego firma
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(100L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // business torka sa ID-em = 100 je firma, nije institucija
        this.mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/100/responsible_institution")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testSetResponsibleInstitutionForDamageMissingId() throws Exception {
       /*
        * Test proverava ispravnost rada metode setResponsibleInstitutionForDamage kontrolera DamageController
        * (postavljanje odgovorne institucije za odredjeni kvar na nivou zadate zgrade od strane predsednika skupstine stanara)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne poseduje sva neophodna polja - nedostaje polje id (nove odgovorne institucije)
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO();
        // nedostaje vrednost polja id
        responsibilityDTO.setId(null);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        this.mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/100/responsible_institution")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testSetResponsibleInstitutionForDamageInappropriateTypeOfId() throws Exception {
       /*
        * Test proverava ispravnost rada metode setResponsibleInstitutionForDamage kontrolera DamageController
        * (postavljanje odgovorne institucije za odredjeni kvar na nivou zadate zgrade od strane predsednika skupstine stanara)
        * u slucaju kada je dolazni DTO objekat nevalidan: poseduje sva neophodna polja - ali je polje id nevalidnog tipa
        */

        // neodgovarajuca vrednost polja id
        String responsibilityDTOJson = "{id:'greska'}";

        this.mockMvc.perform(patch(URL_PREFIX + "/buildings/100/damages/100/responsible_institution")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za delegateResponsibilityToInstitutionForDamage
    @Test
    @Transactional
    @Rollback(true)
    public void testDelegateResponsibilityToInstitutionForDamage() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToInstitutionForDamage kontrolera DamageController
        * (delegiranje odgovornosti nekoj drugoj instituciji za zadati kvar od trenutno odgovorne institucije)
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(103L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        /*
        u zgradi sa ID-em = 100 institucija sa ID-em = 102 je odgovorna za kvar sa ID-em = 104 i sledecim podacima:
            id = 104,
            description = fifth damage description,
            building_id = 100,
            responsible_institution_id = 102
        pri cemu se odgovornost sada delegira na instituciju sa ID-em = 103
        */
        this.mockMvc.perform(patch(URL_PREFIX + "/institutions/102/responsible_for_damages/104/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenInstitution_2)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(104))
                .andExpect(jsonPath("$.description").value("fifth damage description"))
                .andExpect(jsonPath("$.buildingId").value(100))
                .andExpect(jsonPath("$.responsibleInstitution.id").value(103));
    }

    @Test
    public void testDelegateResponsibilityToInstitutionForDamageByNotAuthenticatedUser() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToInstitutionForDamage kontrolera DamageController
        * (delegiranje odgovornosti nekoj drugoj instituciji za zadati kvar od trenutno odgovorne institucije)
        * u slucaju kada akciju obavlja nelogovani korisnik
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(103L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        this.mockMvc.perform(patch(URL_PREFIX + "/institutions/102/responsible_for_damages/104/delegate_responsibility")
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDelegateResponsibilityToInstitutionForDamageByNotAuthenticatedInstitution() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToInstitutionForDamage kontrolera DamageController
        * (delegiranje odgovornosti nekoj drugoj instituciji za zadati kvar od trenutno odgovorne institucije)
        * u slucaju kada akciju obavlja logovani korisnik koji nema ulogu institucije
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(103L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        this.mockMvc.perform(patch(URL_PREFIX + "/institutions/102/responsible_for_damages/104/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDelegateResponsibilityToInstitutionForDamageNonexistentInstitution() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToInstitutionForDamage kontrolera DamageController
        * (delegiranje odgovornosti nekoj drugoj instituciji za zadati kvar od trenutno odgovorne institucije)
        * u slucaju kada ne postoji institucija sa zadatim ID-em
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(103L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // ne postoji institucija sa ID-em = 200
        this.mockMvc.perform(patch(URL_PREFIX + "/institutions/200/responsible_for_damages/104/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenInstitution_2)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDelegateResponsibilityToInstitutionForDamageInvalidInstitution() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToInstitutionForDamage kontrolera DamageController
        * (delegiranje odgovornosti nekoj drugoj instituciji za zadati kvar od trenutno odgovorne institucije)
        * u slucaju kada trenutno ulogovana institucija nema ID koji je zadat u URL-u
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(103L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // trenutno ulogovana institucija ima ID = 102 (ne ID = 103)
        this.mockMvc.perform(patch(URL_PREFIX + "/institutions/103/responsible_for_damages/104/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenInstitution_2)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDelegateResponsibilityToInstitutionForDamageNonexistentDamage() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToInstitutionForDamage kontrolera DamageController
        * (delegiranje odgovornosti nekoj drugoj instituciji za zadati kvar od trenutno odgovorne institucije)
        * u slucaju kada ne postoji kvar sa zadatim ID-em
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(103L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // ne postoji kvar sa ID-em = 200
        this.mockMvc.perform(patch(URL_PREFIX + "/institutions/102/responsible_for_damages/200/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenInstitution_2)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDelegateResponsibilityToInstitutionForDamageNotResponsibleInstitution() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToInstitutionForDamage kontrolera DamageController
        * (delegiranje odgovornosti nekoj drugoj instituciji za zadati kvar od trenutno odgovorne institucije)
        * u slucaju kada trenutno ulogovana institucija nije odgovorna za kvar sa zadatim ID-em
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(103L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // za kvar sa ID-em = 103 u zgradi sa ID-em = 100 odgovorna je institucija sa ID-em = 103 (a ne sa ID-em = 102)
        this.mockMvc.perform(patch(URL_PREFIX + "/institutions/102/responsible_for_damages/103/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenInstitution_2)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDelegateResponsibilityToInstitutionForDamageNonexistentInstitutionFromDTO() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToInstitutionForDamage kontrolera DamageController
        * (delegiranje odgovornosti nekoj drugoj instituciji za zadati kvar od trenutno odgovorne institucije)
        * u slucaju kada ne postoji institucija sa ID-em zadatim u DTO-u (nova odgovorna institucija)
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(200L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // ne postoji institucija sa ID-em = 200
        this.mockMvc.perform(patch(URL_PREFIX + "/institutions/102/responsible_for_damages/104/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenInstitution_2)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDelegateResponsibilityToInstitutionForDamageNotInstitution() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToInstitutionForDamage kontrolera DamageController
        * (delegiranje odgovornosti nekoj drugoj instituciji za zadati kvar od trenutno odgovorne institucije)
        * u slucaju kada torka kojoj pripada ID zadat u DTO-u nije institucija nego firma
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(100L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // business torka sa ID-em = 100 nije institucija, nego firma
        this.mockMvc.perform(patch(URL_PREFIX + "/institutions/102/responsible_for_damages/104/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenInstitution_2)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testDelegateResponsibilityToInstitutionForDamageMissingId() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToInstitutionForDamage kontrolera DamageController
        * (delegiranje odgovornosti nekoj drugoj instituciji za zadati kvar od trenutno odgovorne institucije)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne poseduje sva neophodna polja - nedostaje polje id (nove odgovorne institucije)
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO();
        // nedostaje polje id
        responsibilityDTO.setId(null);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        this.mockMvc.perform(patch(URL_PREFIX + "/institutions/102/responsible_for_damages/104/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenInstitution_2)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testDelegateResponsibilityToInstitutionForDamageInappropriateTypeOfId() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToInstitutionForDamage kontrolera DamageController
        * (delegiranje odgovornosti nekoj drugoj instituciji za zadati kvar trenutno odgovorne institucije)
        * u slucaju kada je dolazni DTO objekat nevalidan: poseduje sva neophodna polja - ali je polje id nevalidnog tipa
        */

        // polje id nevalidnog tipa
        String responsibilityDTOJson = "{id:'greska'}";

        this.mockMvc.perform(patch(URL_PREFIX + "/institutions/102/responsible_for_damages/104/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenInstitution_2)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za delegateResponsibilityToPersonForDamage
    @Test
    @Transactional
    @Rollback(true)
    public void testDelegateResponsibilityToPersonForDamage() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToPersonForDamage kontrolera DamageController
        * (delegiranje odgovornosti nekom drugom stanaru za zadati kvar od trenutno odgovornog lica - stanara)
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(103L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        /*
        u zgradi sa ID-em = 100 stanar sa ID-em = 101 je odgovorna osoba za kvar sa ID-em = 101 i sledecim podacima:
            id = 101,
            description = second damage description,
            building_id = 100,
            responsible_person_id = 101
        pri cemu se sada ta odgovornost delegira stanaru sa ID-em = 103
        */
        this.mockMvc.perform(patch(URL_PREFIX + "/tenants/101/responsible_for_damages/101/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(101))
                .andExpect(jsonPath("$.description").value("second damage description"))
                .andExpect(jsonPath("$.buildingId").value(100))
                .andExpect(jsonPath("$.responsiblePerson.id").value(103));
    }

    @Test
    public void testDelegateResponsibilityToPersonForDamageByNotAuthenticatedUser() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToPersonForDamage kontrolera DamageController
        * (delegiranje odgovornosti nekom drugom stanaru za zadati kvar od trenutno odgovornog lica - stanara)
        * u slucaju kada akciju obavlja nelogovani korisnik
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(103L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/tenants/101/responsible_for_damages/101/delegate_responsibility")
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDelegateResponsibilityToPersonForDamageByNotAuthenticatedTenant() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToPersonForDamage kontrolera DamageController
        * (delegiranje odgovornosti nekom drugom stanaru za zadati kvar od trenutno odgovornog lica - stanara)
        * u slucaju kada akciju obavlja logovani korisnik koji nema ulogu stanara
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(103L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        this.mockMvc.perform(patch(URL_PREFIX + "/tenants/101/responsible_for_damages/101/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenInstitution_1)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDelegateResponsibilityToPersonForDamageNonexistentTenant() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToPersonForDamage kontrolera DamageController
        * (delegiranje odgovornosti nekom drugom stanaru za zadati kvar od trenutno odgovornog lica - stanara)
        * u slucaju kada ne postoji stanar sa zadatim ID-em
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(103L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // ne postoji stanar sa ID-em = 200
        this.mockMvc.perform(patch(URL_PREFIX + "/tenants/200/responsible_for_damages/101/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDelegateResponsibilityToPersonForDamageInvalidTenant() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToPersonForDamage kontrolera DamageController
        * (delegiranje odgovornosti nekom drugom stanaru za zadati kvar od trenutno odgovornog lica - stanara)
        * u slucaju kada trenutno ulogovan stanar nema ID koji je zadat u URL-u
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(103L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // trenutno logovani stanar ima ID = 101
        this.mockMvc.perform(patch(URL_PREFIX + "/tenants/102/responsible_for_damages/101/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDelegateResponsibilityToPersonForDamageTypeNonexistentDamage() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToPersonForDamage kontrolera DamageController
        * (delegiranje odgovornosti nekom drugom stanaru za zadati kvar od trenutno odgovornog lica - stanara)
        * u slucaju kada ne postoji kvar sa zadatim ID-em
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(103L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // ne postoji kvar sa ID-em = 200
        this.mockMvc.perform(patch(URL_PREFIX + "/tenants/101/responsible_for_damages/200/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDelegateResponsibilityToPersonForDamageNotResponsiblePerson() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToPersonForDamage kontrolera DamageController
        * (delegiranje odgovornosti nekom drugom stanaru za zadati kvar od trenutno odgovornog lica - stanara)
        * u slucaju kada trenutno ulogovan stanar nije odgovoran za zadati kvar u sopstvenoj zgradi
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(103L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // ne postoji odgovornost stanara sa ID-em = 101 za kvar sa ID-em = 102 u zgradi sa ID-em = 100
        this.mockMvc.perform(patch(URL_PREFIX + "/tenants/101/responsible_for_damages/102/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDelegateResponsibilityToPersonForDamageNonexistentTenantFromDTO() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToPersonForDamage kontrolera DamageController
        * (delegiranje odgovornosti nekom drugom stanaru za zadati kvar od trenutno odgovornog lica - stanara)
        * u slucaju kada ne postoji stanar sa ID-em zadatim u DTO-u (novo odgovorno lice - stanar)
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(200L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // ne postoji stanar sa ID-em = 200
        this.mockMvc.perform(patch(URL_PREFIX + "/tenants/101/responsible_for_damages/101/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDelegateResponsibilityToPersonForDamageTenantFromInvalidBuilding() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToPersonForDamage kontrolera DamageController
        * (delegiranje odgovornosti nekom drugom stanaru za zadati kvar od trenutno odgovornog lica - stanara)
        * u slucaju kada stanar ciji je ID zadat u DTO-u ne zivi u istoj zgradi kao i trenutno logovan stanar
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO(107L);
        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        // stanar sa ID-em = 107 ne zivi u istoj zgradi kao i trenutno ulogovani stanar (vec zivi u zgradi sa ID-em = 101)
        this.mockMvc.perform(patch(URL_PREFIX + "/tenants/101/responsible_for_damages/101/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testDelegateResponsibilityPersonForDamageMissingId() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToPersonForDamage kontrolera DamageController
        * (delegiranje odgovornosti nekom drugom stanaru za zadati kvar od trenutno odgovornog lica - stanara)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne poseduje sva neophodna polja - nedostaje polje id (novog odgovornog lica - stanara)
        */

        ResponsibleForDamageTypeDTO responsibilityDTO = new ResponsibleForDamageTypeDTO();
        // nedostaje polje id
        responsibilityDTO.setId(null);

        String responsibilityDTOJson = TestUtil.json(responsibilityDTO);

        this.mockMvc.perform(patch(URL_PREFIX + "/tenants/101/responsible_for_damages/101/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testDelegateResponsibilityToPersonForDamageInappropriateTypeOfId() throws Exception {
       /*
        * Test proverava ispravnost rada metode delegateResponsibilityToPersonForDamage kontrolera DamageController
        * (delegiranje odgovornosti nekom drugom stanaru za zadati kvar od trenutno odgovornog lica - stanara)
        * u slucaju kada je dolazni DTO objekat nevalidan: poseduje sva neophodna polja - ali je polje id nevalidnog tipa
        */

        // nevalidan tip polja id
        String responsibilityDTOJson = "{id:'greska'}";

        this.mockMvc.perform(patch(URL_PREFIX + "/tenants/101/responsible_for_damages/101/delegate_responsibility")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(responsibilityDTOJson))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za searchAllDamagesForPresident
    @Test
    public void testSearchAllDamagesForPresident() throws Exception{
        /*
        * Test proverava ispravnost rada metode searchAllDamagesForPresident kontrolera DamageController
        * (dobavljanje liste svih kvarova u zgradi u kojoj je trenutno logovani korisnik predsednik skupstine stanara,
        * koji su kreirani u zadatom periodu)
        * */

        /*
        stanar sa ID-em = 101 je predsednik skupstine stanara u zgradi sa ID-em = 100, kojoj pripadaju sledeci kvarovi
        koji su kreirani u periodu od 5.11.2017. do 5.01.2018.
            1 - id = 100,
                description = first damage description,
                apartment_id = 102,
                building_id = 100
            2 - id = 101,
                description = second damage description,
                apartment_id = 101,
                building_id = 100,
            3 - id = 102,
                description = third damage description,
                apartment_id = 101,
                building_id = 100
            4 - id = 103
                description = forth damage description,
                apartment_id = 101,
                building_id = 100
            5 - id = 104,
                description = fifth damage description,
                apartment_id = 101,
                building_id = 100
        */
        mockMvc.perform(get(URL_PREFIX + "/buildings/100/damages")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .params(params)
                .param("from", "2017-11-05 00:00:01")
                .param("to", "2018-01-05 23:59:59"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.[*]", hasSize(5)))
                .andExpect(jsonPath("$.[*].id", containsInAnyOrder(100, 101,102,103,104)))
                .andExpect(jsonPath("$.[*].description", containsInAnyOrder("first damage description", "second damage description",
                        "third damage description", "forth damage description", "fifth damage description")));
    }

    // TESTOVI za searchAllDamagesForTenant
    @Test
    public void testSearchAllDamagesForTenant() throws Exception{
        /*
        * Test proverava ispravnost rada metode searchAllDamagesForTenant kontrolera DamageController
        * (dobavljanje liste svih kvarova za koje je stanar sa zadatim ID-em odgovorna osoba koji su kreirani u
        * zadatom periodu)
        * */

        /*
        stanar sa ID-em = 101 je odogovorna osoba za sledece kvarove koji su kreirani u periodu od 5.11.2017. do 5.1.2018.:
            1 - id = 101,
                description = second damage description,
                apartment_id = 101,
                building_id = 100
        */
        this.mockMvc.perform(get("/api/tenants/101/responsible_for_damages_from_to")
                .params(params)
                .param("from","2017-11-05 00:00:01")
                .param("to","2018-01-05 23:59:59")
                .header("Authentication-Token", this.accessTokenTenant_2))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.[*]", hasSize(1)))
                .andExpect(jsonPath("$.[*].id", containsInAnyOrder(101)))
                .andExpect(jsonPath("$.[*].description", containsInAnyOrder("second damage description")))
                .andExpect(jsonPath("$.[*].apartmentId", containsInAnyOrder(101)));
    }

    // TESTOVI za searchAllDamagesInPersonalApartmentsForTenant
    @Test
    public void testSearchAllDamagesInPersonalApartmentsForTenant() throws Exception{
        /*
        * Test proverava ispravnost rada metode searchAllDamagesInPersonalApartmentsForTenant kontrolera DamageController
        * (dobavljanje liste svih kvarova u stanovima korisnika sa zadatim ID-em koji su kreirani u zadatom periodu)
        * */

        /*
        stanar sa ID-em = 101 ima 3 stana sa ID-evima: 100,101 i 102, kojima pripadaju kvarovi koji su kreirani
        u periodu od 5.11.2017. do 5.1.2018.
            1 - id = 100,
                description = first damage description,
                apartment_id = 102
                building_id = 100
            2 - id = 101,
                description = second damage description,
                apartment_id = 101,
                building_id = 100,
            3 - id = 102,
                description = third damage description,
                apartment_id = 101,
                building_id = 100
            4 - id = 103
                description = forth damage description,
                apartment_id = 101,
                building_id = 100
            5 - id = 104,
                description = fifth damage description,
                apartment_id = 101,
                building_id = 100
        */
        this.mockMvc.perform(get("/api/tenants/101/personal_apartments/damages_from_to")
                .params(params)
                .param("from","2017-11-05 00:00:01")
                .param("to","2018-01-05 23:59:59")
                .header("Authentication-Token", this.accessTokenTenant_2))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.[*]", hasSize(5)))
                .andExpect(jsonPath("$.[*].id", containsInAnyOrder(100,101,102,103,104)))
                .andExpect(jsonPath("$.[*].description",
                        containsInAnyOrder("first damage description", "second damage description",
                                "third damage description", "forth damage description", "fifth damage description")));
    }

    // TESTOVI za searchAllDamagesInApartmentForTenant
    @Test
    public void testSearchAllDamagesInApartmentForTenant() throws Exception{
        /*
        * Test proverava ispravnost rada metode searchAllDamagesInApartmentForTenant kontrolera DamageController
        * (dobavljanje liste svih kvarova u stanu u kom trenutno zivi stanar sa zadatim ID-em koji su kreirani u zadatom
        * periodu)
        * */

        /*
        stanar sa ID-em = 101 zivi u stanu sa ID-em = 101 kom pripadaju kvarovi koji su kreirani u periodu od 5.11.2017.
        do 5.1.2018.
            1 - id = 101,
                description = second damage description,
                apartment_id = 101,
                building_id = 100,
            2 - id = 102,
                description = third damage description,
                apartment_id = 101,
                building_id = 100
            3 - id = 103
                description = forth damage description,
                apartment_id = 101,
                building_id = 100
            4 - id = 104,
                description = fifth damage description,
                apartment_id = 101,
                building_id = 100
        */
        this.mockMvc.perform(get("/api/tenants/101/apartment_live_in/damages_from_to")
                .params(params)
                .param("from","2017-11-05 00:00:01")
                .param("to","2018-01-05 23:59:59")
                .header("Authentication-Token", this.accessTokenTenant_2))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.[*]", hasSize(4)))
                .andExpect(jsonPath("$.[*].id", containsInAnyOrder(101,102,103,104)))
                .andExpect(jsonPath("$.[*].description", containsInAnyOrder("second damage description",
                        "third damage description", "forth damage description", "fifth damage description")));
    }

    // TESTOVI za searchMyDamages
    @Test
    public void testSearchMyDamages() throws Exception{
        /*
        * Test proverava ispravnost rada metode searchMyDamages kontrolera DamageController
        * (dobavljanje liste svih kvarova koje je prijavio stanar sa zadatim ID-em koji su kreirani u zadatom periodu)
        * */

        /*
        stanar sa ID-em = 101 je prijavio sledece kvarove koji su kreirani u periodu od 5.11.2017. do 5.1.2018.:
            1 - id = 101,
                description = second damage description,
                apartment_id = 101,
                building_id = 100,
            2 - id = 102,
                description = third damage description,
                apartment_id = 101,
                building_id = 100
            3 - id = 104,
                description = fifth damage description,
                apartment_id = 101,
                building_id = 100
        */
        this.mockMvc.perform(get("/api/tenants/101/my_damages_from_to")
                .params(params)
                .param("from","2017-11-05 00:00:01")
                .param("to","2018-01-05 23:59:59")
                .header("Authentication-Token", this.accessTokenTenant_2))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.[*]", hasSize(3)))
                .andExpect(jsonPath("$.[*].id", containsInAnyOrder(101,102,104)))
                .andExpect(jsonPath("$.[*].description", containsInAnyOrder("second damage description",
                        "third damage description", "fifth damage description")));
    }
}

