import { Component, EventEmitter, Input, Output } from '@angular/core';
import { BsModalRef } from "ngx-bootstrap";
import { ToasterService } from "angular5-toaster/dist";
import * as _ from "lodash";
// model
import { VoteCreate } from "../../shared/models/vote-create";
import { Questionnaire } from "../../shared/models/questionnaire";
// service
import { QuestionnaireService } from "../../core/services/questionnaire.service";
// error
import { AppError } from "../../shared/errors/app-error";
import { BadRequestError } from "../../shared/errors/bad-request-error";
import { ForbiddenError } from "../../shared/errors/forbidden-error";
import { NotFoundError } from "../../shared/errors/not-found-error";


@Component({
  selector: 'app-vote',
  templateUrl: './vote.component.html',
  styleUrls: ['./vote.component.css']
})
export class VoteComponent {
  @Input() voteList: VoteCreate;
  @Input() initialValues: VoteCreate;
  @Input() questionnaire: Questionnaire;
  @Output() submitted = new EventEmitter<boolean>();
  disableReset: boolean;

  constructor(private modalRef: BsModalRef, private questionnaireService: QuestionnaireService,
              private toasterService: ToasterService) {
    this.disableReset = true;
  }

  onSelectionChange(answer: string, indexOfQuestion: number) {
    this.voteList.questionVotes[indexOfQuestion].answer = answer;
    this.changeExist();
  }

  reset() {
    this.voteList = _.cloneDeep(this.initialValues);
    this.disableReset = true;
  }

  save() {
    this.questionnaireService.save(this.voteList, 'buildings', this.questionnaire.buildingId,
      'questionnaires', this.questionnaire.id, 'vote')
      .subscribe(() => {
        this.submitted.emit(true);
        this.modalRef.hide();
      }, (error: AppError) => {
        if(error instanceof BadRequestError)
          this.toasterService.pop('error', 'Error', 'The questionnaire has already expired or ' +
            'you are already voted once for every of your personal apartment in that building!');
        else if(error instanceof ForbiddenError)
          this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action, because ' +
            'you are not owner of any apatrment in this building or selected questionnaire is not connected with any meeting!');
        else if(error instanceof NotFoundError)
          this.toasterService.pop('error', 'Error', 'Questionnaire or some question/answer not found!');
        else {
          this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
          throw error;
        }
      });
  }

  changeExist() {
    this.disableReset = (JSON.stringify(this.voteList) === JSON.stringify(this.initialValues));
  }
}
