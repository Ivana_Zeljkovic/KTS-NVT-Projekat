package rs.ac.uns.ftn.ktsnvt.web.controller;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.model.entity.*;
import rs.ac.uns.ftn.ktsnvt.service.*;
import rs.ac.uns.ftn.ktsnvt.web.dto.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Nikola Garabandic on 22/11/2017.
 */
@RestController
@RequestMapping(value = "/api")
@Api(value="meetings", description="Operations pertaining to meetings in application.")
public class MeetingController {

    @Autowired
    private BuildingService buildingService;

    @Autowired
    private MeetingItemService meetingItemService;

    @Autowired
    private MeetingService meetingService;

    @Autowired
    private TenantService tenantService;

    @Autowired
    private QuestionnaireService questionnaireService;



    @RequestMapping(
            value = "/buildings/{buildingId}/council/meetings",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a list of all meetings in selected building which didn't start in page form.",
            notes = "You have to provide a valid building ID in the URL.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of meetings", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to get the resource"),
            @ApiResponse(code = 403, message = "You don't have permission to get the resource"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity getMeetings(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The number of page for getting meeting items.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The size of page for getting meeting items.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting notifications.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "The sort property for getting notifications.", required = true) @RequestParam String sortProperty){

        Building building = this.buildingService.findOne(buildingId);
        this.tenantService.checkPermissionForCurrentTenantInBuilding(building.getId());

        ArrayList<MeetingDTO> meetingsDTO = new ArrayList<>();
        Page<Meeting> meetings = this.meetingService.findAllByBuildingId(buildingId, Integer.valueOf(pageNumber),
                Integer.valueOf(pageSize), sortDirection, sortProperty);

        meetings.forEach(meeting -> meetingsDTO.add(new MeetingDTO(meeting, meetings.getTotalElements())));

        return new ResponseEntity<>(meetingsDTO, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/council/finished_meetings",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a list of all finished meetings without created record in selected building, in page form.",
            notes = "You have to provide a valid building ID in the URL.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of meetings", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to get the resource"),
            @ApiResponse(code = 403, message = "You don't have permission to get the resource"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('COUNCIL_PRESIDENT')")
    public ResponseEntity getFinishedMeetingsWithoutRecord(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The number of page for getting meeting items.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The size of page for getting meeting items.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting notifications.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "The sort property for getting notifications.", required = true) @RequestParam String sortProperty){

        Building building = this.buildingService.findOne(buildingId);
        this.tenantService.checkPermissionForCurrentPresidentInBuilding(building);

        ArrayList<MeetingDTO> meetingsDTO = new ArrayList<>();
        Page<Meeting> meetings = this.meetingService.findAllFinishedByBuildingId(buildingId, Integer.valueOf(pageNumber),
                Integer.valueOf(pageSize), sortDirection, sortProperty);

        meetings.forEach(meeting -> meetingsDTO.add(new MeetingDTO(meeting, meetings.getTotalElements())));

        return new ResponseEntity<>(meetingsDTO, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/council/meetings/{meetingId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a single meeting in selected building which didn't start.",
            notes = "You have to provide valid IDs for building and meeting in the URL.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved meeting", response = MeetingDTO.class),
            @ApiResponse(code = 401, message = "You are not authorized to get the resource"),
            @ApiResponse(code = 403, message = "You don't have permission to get the resource"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity getMeeting(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The ID of the existing meeting resource.", required = true) @PathVariable("meetingId") Long meetingId) {

        Building building = this.buildingService.findOne(buildingId);
        this.tenantService.checkPermissionForCurrentTenantInBuilding(building.getId());

        Meeting meeting = this.meetingService.findOne(meetingId);
        if (!meeting.getCouncil().getId().equals(building.getCouncil().getId()))
            throw new BadRequestException("Selected meeting doesn't belong to selected building!");

        if(meeting.getRecord() != null)
            throw new BadRequestException("Meeting is already processed, see details in record from this meeting!");

        return new ResponseEntity<>(new MeetingDTO(meeting), HttpStatus.OK);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/council/meetings",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Create a meeting resource.",
            notes = "You have to provide a valid building ID in the URL. " +
                    "Method returns the meeting being saved.",
            httpMethod = "POST",
            produces = "application/json",
            consumes = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully created meeting", response = MeetingDTO.class),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 403, message = "You don't have permission to create resource"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('COUNCIL_PRESIDENT')")
    public ResponseEntity createMeeting(
            @ApiParam(value = "The meeting object.", required = true) @Valid @RequestBody MeetingCreateDTO meetingCreateDTO,
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The object that contains all errors from validation of DTO object") BindingResult bindingResult) {

        Building building = this.buildingService.findOne(buildingId);

        this.tenantService.checkPermissionForCurrentPresidentInBuilding(building);

        List<MeetingItem> meetingItems = new ArrayList<>();
        meetingCreateDTO.getMeetingItemsId().forEach(
                meetingItemId -> meetingItems.add(this.meetingItemService.findOne(meetingItemId))
        );

        List<MeetingItem> meetingItemsOnBillboard =
                this.meetingItemService.findAllOnBillboard(building.getBillboard().getId());
        meetingItems.forEach(
                meetingItem -> this.meetingItemService.checkMeetingItemInBuilding(meetingItem, meetingItemsOnBillboard)
        );

        Meeting meeting = new Meeting(meetingCreateDTO.getMeetingDate());
        meeting.setDuration(meetingCreateDTO.getDuration());
        meeting.setCouncil(building.getCouncil());
        meeting.setMeetingItems(meetingItems);

        meeting = this.meetingService.save(meeting);

        for(MeetingItem meetingItem : meeting.getMeetingItems()) {
            meetingItem.setMeeting(meeting);
            this.meetingItemService.save(meetingItem);
            if(meetingItem.getQuestionnaire() != null) {
                // ukoliko tacka dnevnog reda sadrzi anketu, treba postaviti vreme do kad se moze glasati na anketu
                Questionnaire questionnaire =
                        this.questionnaireService.setDateExpired(meetingItem.getQuestionnaire(), meeting);
                this.questionnaireService.save(questionnaire);
            }
        }

        MeetingDTO meetingDTO = new MeetingDTO(meeting);
        return new ResponseEntity<>(meetingDTO, HttpStatus.CREATED);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/council/meetings/{id}",
            method = RequestMethod.DELETE
    )
    @ApiOperation(
            value = "Delete meeting resource.",
            notes = "You have to provide a valid IDs for building and meeting in the URL. " +
                    "Once deleted the resource can not be recovered.",
            httpMethod = "DELETE"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully deleted meeting"),
            @ApiResponse(code = 401, message = "You are not authorized to delete the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('COUNCIL_PRESIDENT')")
    public ResponseEntity deleteMeeting(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The ID of the existing meeting resource.", required = true) @PathVariable("id") Long id){

        Building building = this.buildingService.findOne(buildingId);
        this.tenantService.checkPermissionForCurrentTenantInBuilding(building.getId());

        Meeting meeting = this.meetingService.findOne(id);
        // provera da li sastanak sa zadatim ID-em pripada zadatoj zgradi
        if(!meeting.getCouncil().getId().equals(building.getCouncil().getId()))
            throw new BadRequestException("Selected meeting doesn't belong to selected building!");

        this.meetingService.checkMeetingStart(meeting);

        this.meetingService.remove(meeting);

        return new ResponseEntity(HttpStatus.OK);
    }
}
