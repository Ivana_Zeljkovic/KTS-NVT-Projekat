package rs.ac.uns.ftn.ktsnvt.repository.unit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.model.entity.*;
import rs.ac.uns.ftn.ktsnvt.repository.BuildingRepository;
import rs.ac.uns.ftn.ktsnvt.repository.TenantRepository;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by Ivana Zeljkovic on 28/11/2017.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class TenantRepositoryUnitTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private TenantRepository tenantRepository;

    @Autowired
    private BuildingRepository buildingRepository;

    private Long tenant_1_id;
    private Long tenant_2_id;
    private Long tenant_3_id;
    private Long building_id;


    @Before
    public void setUp() {
        // ACCOUNTS, TENANTS
        Account account_1 = new Account("kaca", "kaca");
        Tenant tenant_1 = new Tenant("Katarina", "Cukurov", new Date(),
                "kaca@gmail.com", "123456789", true, account_1);
        account_1.setTenant(tenant_1);
        Account account_2 = new Account("cuki", "kaca");
        Tenant tenant_2 = new Tenant("Katarina", "Cukurov", new Date(),
                "cukurov@gmail.com", "987654321", true, account_2);
        account_2.setTenant(tenant_2);
        Account account_3 = new Account("marko", "marko");
        Tenant tenant_3 = new Tenant("Marko", "Markovic", new Date(),
                "marko@gmail.com", "192837465", true, account_3);
        account_3.setTenant(tenant_3);

        entityManager.persist(account_1);
        entityManager.persist(account_2);
        entityManager.persist(account_3);
        this.tenant_1_id = (Long) entityManager.persistAndGetId(tenant_1);
        this.tenant_2_id = (Long) entityManager.persistAndGetId(tenant_2);
        this.tenant_3_id = (Long) entityManager.persistAndGetId(tenant_3);


        // BUILDING (WITH ADDRESS)
        City city = new City("Novi Sad", 21000);
        Address address = new Address("Janka Veselinovica", 8, city);
        Building building = new Building(5, true, address);
        entityManager.persist(city);
        entityManager.persist(address);
        this.building_id = (Long)entityManager.persistAndGetId(building);

        // APARTMENTS
        Apartment apartment_1 = new Apartment(1, 1, 60);
        apartment_1.setBuilding(building);
        Apartment apartment_2 = new Apartment(2, 1, 100);
        apartment_2.setBuilding(building);

        apartment_1.getTenants().add(tenant_1);
        apartment_1.getTenants().add(tenant_3);
        apartment_2.getTenants().add(tenant_2);
        entityManager.persist(apartment_1);
        entityManager.persist(apartment_2);

        tenant_1.setApartmentLiveIn(apartment_1);
        tenant_3.setApartmentLiveIn(apartment_1);
        tenant_2.setApartmentLiveIn(apartment_2);
        entityManager.merge(tenant_1);
        entityManager.merge(tenant_2);
        entityManager.merge(tenant_3);

        building.getApartments().add(apartment_1);
        building.getApartments().add(apartment_2);
        entityManager.merge(building);
    }

    @Test
    public void testFindTenantsByBuildingAndNameAndUsername() {
        /*
         * Test proverava ispravnost rada metode findTenantsByBuildingAndNameAndUsername repozitorijuma TenantRepository
         * (pronalazenje stanara na osnovu zgrade, imena, prezimena i korisnickog imena)
         * u slucaju kada postoji bar jedan stanar koji zadovoljava parametre pretrage
         * */

        Building building = this.buildingRepository.findOne(this.building_id);

        /*
        u zgradi sa ID-em = building_id postoji jedan stanar koji zadovoljava parametre:
            ime = Katarina,
            prezime = Cukurov,
            korisnicko ime = kaca
         */
        List<Tenant> tenants = this.tenantRepository.findTenantsByBuildingAndNameAndUsername(
                this.building_id, "Katarina", "Cukurov", "kaca");

        assertEquals(1, tenants.size());
        assertEquals("Katarina", tenants.get(0).getFirstName());
        assertEquals("Cukurov", tenants.get(0).getLastName());
        assertEquals("kaca", tenants.get(0).getAccount().getUsername());
        assertEquals(building.getAddress(), tenants.get(0).getApartmentLiveIn().getBuilding().getAddress());
    }

    @Test
    public void testFindTenantsByBuildingAndNameAndUsernameNonexistent() {
        /*
         * Test proverava ispravnost rada metode findTenantsByBuildingAndNameAndUsername repozitorijuma TenantRepository
         * (pronalazenje stanara na osnovu zgrade, imena, prezimena i korisnickog imena)
         * u slucaju kada ne postoji nijedan stanar koji zadovoljava parametre pretrage
         * */

        /*
        u zgradi sa ID-em = building_id ne postoji nijedan stanar koji zadovoljava parametre:
            ime = Katarina,
            prezime = Markovic,
            korisnicko ime = marko
         */
        List<Tenant> tenants = this.tenantRepository.findTenantsByBuildingAndNameAndUsername(
                this.building_id, "Katarina", "Markovic", "marko");

        assertEquals(0, tenants.size());
    }

    @Test
    public void testFindByNameOrUsername() {
        /*
         * Test proverava ispravnost rada metode findByNameOrUsername repozitorijuma TenantRepository
         * (pronalazenje stanara na imena i prezimena ili korisnickog imena)
         * u slucaju kada postoji bar jedan stanar koji zadovoljava parametre pretrage
         * */

        /*
        postoje dva stanara koji zadovoljavaju pretragu sa parametrima:
            (ime = Marko && prezime = Markovic) || korisnicko ime = cuki
         */
        List<Tenant> tenants_list = this.tenantRepository.findByNameOrUsername("Marko", "Markovic", "cuki");

        assertEquals(2, tenants_list.size());
    }

    @Test
    public void testFindByNameOrUsernameNonExistent() {
        /*
         * Test proverava ispravnost rada metode findByNameOrUsername repozitorijuma TenantRepository
         * (pronalazenje stanara na imena i prezimena ili korisnickog imena)
         * u slucaju kada ne postoji nijedan stanar koji zadovoljava parametre pretrage
         * */

        /*
        ne postoji nijedan stanar koji zadovoljava pretragu sa parametrima:
            (ime = Ivan && prezime = Juric) || korisnicko ime = dalibor
         */
        List<Tenant> tenants = this.tenantRepository.findByNameOrUsername("Ivan", "Juric", "dalibor");

        assertEquals(0, tenants.size());
    }

    @Test
    public void testFindByNameAndUsername() {
        /*
         * Test proverava ispravnost rada metode findByNameAndUsername repozitorijuma TenantRepository
         * (pronalazenje stanara na imena i prezimena i korisnickog imena)
         * u slucaju kada postoji bar jedan stanar koji zadovoljava parametre pretrage
         * */

        /*
        postoji samo jedan stanar koji zadovoljava pretragu sa parametrima:
            ime = Marko && prezime = Markovic && korisnicko ime = marko
        i to je stanar ciji je ID = tenant_3_id
         */
        Tenant tenant = this.tenantRepository.findByNameAndUsername("Marko", "Markovic", "marko");

        assertNotNull(tenant);
        assertEquals(this.tenant_3_id, tenant.getId());
        assertEquals("Marko", tenant.getFirstName());
        assertEquals("Markovic", tenant.getLastName());
        assertEquals("marko", tenant.getAccount().getUsername());
    }

    @Test
    public void testFindByNameAndUsernameNonexistent() {
        /*
         * Test proverava ispravnost rada metode findByNameAndUsername repozitorijuma TenantRepository
         * (pronalazenje stanara na imena i prezimena i korisnickog imena)
         * u slucaju kada ne postoji nijedan stanar koji zadovoljava parametre pretrage
         * */

        /*
        ne postoji nijedan stanar koji zadovoljava pretragu sa parametrima:
            ime = Sasa && prezime = Ivkovic && korisnicko ime = sasa
         */
        Tenant tenant = this.tenantRepository.findByNameAndUsername("Sasa", "Ivkovic", "sasa");

        assertNull(tenant);
    }

    @Test
    public void testFindAllByBuildingId() {
        /*
         * Test proverava ispravnost rada metode findAllByBuildingId repozitorijuma TenantRepository
         * (pronalazenje stanara na osnovu ID-a zgrade)
         * */
        Building building = this.buildingRepository.findOne(this.building_id);

        // u zgradi sa ID-em = building_id zivi ukupno 3 stanara
        Page<Tenant> tenants = this.tenantRepository.findAllByBuildingId(this.building_id, new PageRequest(1,3));

        for(Tenant t : tenants) {
            assertEquals(building.getAddress(), t.getApartmentLiveIn().getBuilding().getAddress());
        }
    }
}
