import {by, element, ElementFinder} from "protractor";

export class BuildingListPage {
  // get methods
  getPageTitle(): ElementFinder {
    return element(by.id('headingBuildings'));
  }

  getCityLabel(): ElementFinder {
    return element(by.id('cityLabel'));
  }

  getSerachInput(): ElementFinder {
    return element(by.id('searchInput'));
  }

  getSearchButton(): ElementFinder {
    return element(by.id('searchButton'));
  }

  getAddButton(): ElementFinder {
    return element(by.buttonText("Add"));
  }

  getBuildingsTable(): ElementFinder {
    return element(by.id("buildingsTable"));
  }

  getPagination(): ElementFinder {
    return element(by.className("col-md-12 div-page"));
  }

  getToaster(): ElementFinder {
    return element(by.tagName("toaster-container"));
  }

  getNoElements(): ElementFinder {
    return element(by.id("noElements"));
  }

  // set methods
  setSeachInput(text: string) {
    this.getSerachInput().sendKeys(text);
  }

}
