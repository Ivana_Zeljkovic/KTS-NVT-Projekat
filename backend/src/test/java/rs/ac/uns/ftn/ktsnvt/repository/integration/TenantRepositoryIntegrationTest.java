package rs.ac.uns.ftn.ktsnvt.repository.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.model.entity.Building;
import rs.ac.uns.ftn.ktsnvt.model.entity.Tenant;
import rs.ac.uns.ftn.ktsnvt.repository.BuildingRepository;
import rs.ac.uns.ftn.ktsnvt.repository.TenantRepository;

import static org.junit.Assert.*;

import java.util.List;

/**
 * Created by Ivana Zeljkovic on 28/11/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class TenantRepositoryIntegrationTest {

    @Autowired
    private TenantRepository tenantRepository;

    @Autowired
    private BuildingRepository buildingRepository;

    @Test
    @Transactional
    public void testFindTenantsByBuildingAndNameAndUsername() {
        /*
         * Test proverava ispravnost rada metode findTenantsByBuildingAndNameAndUsername repozitorijuma TenantRepository
         * (pronalazenje stanara na osnovu zgrade, imena, prezimena i korisnickog imena)
         * u slucaju kada postoji bar jedan stanar koji zadovoljava parametre pretrage
         * */

        Building building = this.buildingRepository.findOne(new Long(100));

        /*
        u zgradi sa ID-em = 100 postoji jedan stanar koji zadovoljava parametre:
            ime = Katarina,
            prezime = Cukurov,
            korisnicko ime = kaca
         */
        List<Tenant> tenants = this.tenantRepository.findTenantsByBuildingAndNameAndUsername(
                new Long(100), "Katarina", "Cukurov", "kaca");

        assertEquals(1, tenants.size());
        assertEquals(Long.valueOf(101), tenants.get(0).getId());
        assertEquals("Katarina", tenants.get(0).getFirstName());
        assertEquals("Cukurov", tenants.get(0).getLastName());
        assertEquals("kaca", tenants.get(0).getAccount().getUsername());
        assertEquals(building.getAddress(), tenants.get(0).getApartmentLiveIn().getBuilding().getAddress());
    }

    @Test
    @Transactional
    public void testFindTenantsByBuildingAndNameAndUsernameNonexistent() {
        /*
         * Test proverava ispravnost rada metode findTenantsByBuildingAndNameAndUsername repozitorijuma TenantRepository
         * (pronalazenje stanara na osnovu zgrade, imena, prezimena i korisnickog imena)
         * u slucaju kada ne postoji nijedan stanar koji zadovoljava parametre pretrage
         * */

        /*
        u zgradi sa ID-em = 100 ne postoji nijedan stanar koji zadovoljava parametre:
            ime = Katarina,
            prezime = Markovic,
            korisnicko ime = marko
         */
        List<Tenant> tenants = this.tenantRepository.findTenantsByBuildingAndNameAndUsername(
                new Long(100), "Katarina", "MArkovic", "marko");

        assertEquals(0, tenants.size());
    }

    @Test
    @Transactional
    public void testFindByNameOrUsername() {
        /*
         * Test proverava ispravnost rada metode findByNameOrUsername repozitorijuma TenantRepository
         * (pronalazenje stanara na imena i prezimena ili korisnickog imena)
         * u slucaju kada postoji bar jedan stanar koji zadovoljava parametre pretrage
         * */

        /*
        postoje dva stanara koji zadovoljavaju pretragu sa parametrima:
            (ime = Jasmina && prezime = Markovic) || korisnicko ime = jasmina
         */
        List<Tenant> tenants = this.tenantRepository.findByNameOrUsername("Jasmina", "Markovic", "jasmina");

        assertEquals(2, tenants.size());
        assertEquals(Long.valueOf(106), tenants.get(0).getId());
        assertEquals(Long.valueOf(102), tenants.get(0).getApartmentLiveIn().getId());
        assertEquals("markovic", tenants.get(0).getAccount().getUsername());
        assertEquals(Long.valueOf(103), tenants.get(1).getId());
        assertEquals(Long.valueOf(101), tenants.get(1).getApartmentLiveIn().getId());
        assertEquals("jasmina", tenants.get(1).getAccount().getUsername());
    }

    @Test
    @Transactional
    public void testFindByNameOrUsernameNonExistent() {
        /*
         * Test proverava ispravnost rada metode findByNameOrUsername repozitorijuma TenantRepository
         * (pronalazenje stanara na imena i prezimena ili korisnickog imena)
         * u slucaju kada ne postoji nijedan stanar koji zadovoljava parametre pretrage
         * */

        /*
        ne postoji nijedan stanar koji zadovoljava pretragu sa parametrima:
            (ime = Ivan && prezime = Juric) || korisnicko ime = dalibor
         */
        List<Tenant> tenants = this.tenantRepository.findByNameOrUsername("Ivan", "Juric", "dalibor");

        assertEquals(0, tenants.size());
    }

    @Test
    @Transactional
    public void testFindByNameAndUsername() {
        /*
         * Test proverava ispravnost rada metode findByNameAndUsername repozitorijuma TenantRepository
         * (pronalazenje stanara na imena i prezimena i korisnickog imena)
         * u slucaju kada postoji bar jedan stanar koji zadovoljava parametre pretrage
         * */

        /*
        postoji samo jedan stanar koji zadovoljava pretragu sa parametrima:
            ime = Jasmina && prezime = Markovic && korisnicko ime = jasmina
        i to je stanar ciji je ID = 103
         */
        Tenant tenant = this.tenantRepository.findByNameAndUsername("Jasmina", "Markovic", "jasmina");

        assertNotNull(tenant);
        assertEquals(Long.valueOf(103), tenant.getId());
        assertEquals("Jasmina", tenant.getFirstName());
        assertEquals("Markovic", tenant.getLastName());
        assertEquals("jasmina", tenant.getAccount().getUsername());
    }

    @Test
    @Transactional
    public void testFindByNameAndUsernameNonexistent() {
        /*
         * Test proverava ispravnost rada metode findByNameAndUsername repozitorijuma TenantRepository
         * (pronalazenje stanara na imena i prezimena i korisnickog imena)
         * u slucaju kada ne postoji nijedan stanar koji zadovoljava parametre pretrage
         * */

        /*
        ne postoji nijedan stanar koji zadovoljava pretragu sa parametrima:
            ime = Marko && prezime = Ivkovic && korisnicko ime = marko
         */
        Tenant tenant = this.tenantRepository.findByNameAndUsername("Marko", "Ivkovic", "marko");

        assertNull(tenant);
    }

    @Test
    @Transactional
    public void testFindAllByBuildingId() {
        /*
         * Test proverava ispravnost rada metode findAllByBuildingId repozitorijuma TenantRepository
         * (pronalazenje stanara na osnovu ID-a zgrade)
         * */

        Building building = this.buildingRepository.findOne(new Long(100));

        // u zgradi sa ID-em = 100 zivi ukupno 6 stanara
        Page<Tenant> tenants = this.tenantRepository.findAllByBuildingId(100L, new PageRequest(1, 6));

        assertEquals(0, tenants.getContent().size());
        for(Tenant t : tenants.getContent()) {
            assertEquals(building.getAddress(), t.getApartmentLiveIn().getBuilding().getAddress());
        }
    }

    @Test
    @Transactional
    public void testFindAllByBuildingIdExceptCurrent() {
        /*
         * Test proverava ispravnost rada metode findAllByBuildingId repozitorijuma TenantRepository
         * (pronalazenje stanara na osnovu ID-a zgrade)
         * */

        Building building = this.buildingRepository.findOne(new Long(100));

        // u zgradi sa ID-em = 100 zivi ukupno 6 stanara
        Page<Tenant> tenants = this.tenantRepository.findAllByBuildingIdExceptCurrent(100L, 101L, new PageRequest(1, 6));

        assertEquals(0, tenants.getContent().size());
        for(Tenant t : tenants.getContent()) {
            assertEquals(building.getAddress(), t.getApartmentLiveIn().getBuilding().getAddress());
        }
    }

    @Test
    @Transactional
    public void testFindAll() {
        /*
         * Test proverava ispravnost rada metode findAll repozitorijuma TenantRepository
         * (pronalazenje svih stanara)
         * */

        Page<Tenant> tenants = this.tenantRepository.findAll(new PageRequest(1, 6));

        assertEquals(6, tenants.getContent().size());

    }
}
