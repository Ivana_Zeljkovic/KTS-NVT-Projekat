package rs.ac.uns.ftn.ktsnvt.service.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Offer;
import rs.ac.uns.ftn.ktsnvt.service.OfferService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Katarina Cukurov on 30/11/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class OfferServiceIntegrationTest {

    @Autowired
    private OfferService offerService;




    @Test
    public void testFindOne() {
        /*
         * Test proverava ispravnost rada metode findOne servisa OfferService
         * (dobavljanje ponude na osnovu ID-a)
         * u slucaju kada postoji ponuda sa zadatim ID-em
         * */

        /*
        ponuda sa ID-em = 100 ima podatke:
            id = 100,
            accepted = false,
            price = 500,
            business_id = 100,
            damage_id = 100
         */

        Offer offer = this.offerService.findOne(new Long(100));

        assertNotNull(offer);
        assertEquals(Long.valueOf(100), offer.getId());
        assertEquals(false, offer.isAccepted());
        assertEquals(500, offer.getPrice(), 0);
        assertEquals(new Long(100), offer.getBusiness().getId());
    }

    @Test(expected = NotFoundException.class)
    public void testFindOneNonexistent() {
        /*
         * Test proverava ispravnost rada metode findOne servisa OfferService
         * (dobavljanje ponude na osnovu ID-a)
         * u slucaju kada ne postoji ponuda sa zadatim ID-em - ocekivano NotFoundException
         * */

        // offer sa ID-em = 500 ne postoji
        Offer offer = this.offerService.findOne(new Long(500));
    }


    @Test
    public void testFindForCurrentDamage(){
        /*
        Test proverava rad ispravnosti metode findaAllForCurrentDamage servisa OfferService.
        (dobavljanje svih ponuda za trenutni kvar)
        kvar sa Id-em 100 ima tacno jednu ponudu
         */

        Page<Offer> offers = this.offerService.findAllForCurrentDamage(100L, 0, 3, "ASC", "id");

        assertEquals(1L, offers.getTotalElements());
    }


    @Test
    public void testFindNoneForCurrentDamage(){
        /*
        Test proverava rad ispravnosti metode findaAllForCurrentDamage servisa OfferService.
        (dobavljanje svih ponuda za trenutni kvar)
        kvar sa Id-em 200 ne postoji te ce imati 0 ponuda
         */

        Page<Offer> offers = this.offerService.findAllForCurrentDamage(200L,0, 3, "ASC", "id");

        assertEquals(0L, offers.getTotalElements());
    }


}
