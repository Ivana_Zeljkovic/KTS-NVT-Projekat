import { browser } from "protractor";
// page
import { NotificationPage } from "../page-objects/notification.po";
import { NotificationListPage } from "../page-objects/notification-list.po";
import { LoginPage } from "../../../page-objects/login.po";


describe('Notification page', () => {
  let notificationPage: NotificationPage;
  let notificationListPage: NotificationListPage;
  let loginPage: LoginPage;

  beforeAll(() => {
    loginPage = new LoginPage();
    notificationPage = new NotificationPage();
    notificationListPage = new NotificationListPage();

    browser.get('/');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');

    loginPage.setUsername('kaca');
    loginPage.setPassword('kaca');
    loginPage.getSignInButton().click();

    browser.wait(function() {
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/home/notifications';
      });
    }).then(function() {
      expect(notificationListPage.getNotificationListElement().isDisplayed()).toBe(true);
    });
  });

  afterAll(() => {
    browser.executeScript('window.localStorage.clear();');
  });



  it('should show empty form when user click on Add button on page for review ' +
    'notification list', () => {
    expect(notificationListPage.getAddButton().isDisplayed()).toBe(true);
    notificationListPage.getAddButton().click();

    browser.wait(function() {
      // expect to show modal dialog - page for creating new notification
      return notificationPage.getNotificationElement().isDisplayed();
    }).then(function() {
      // expected view of modal dialog - input fields should be empty
      expect(notificationPage.getNotificationElement().isDisplayed()).toBe(true);
      expect(notificationPage.getTitleInput().isDisplayed()).toBe(true);
      expect(notificationPage.getTitleInput().getAttribute('value')).toEqual('');
      expect(notificationPage.getContentInput().isDisplayed()).toBe(true);
      expect(notificationPage.getContentInput().getAttribute('value')).toEqual('');
      expect(notificationPage.getSaveButton().isDisplayed()).toBe(true);
      expect(notificationPage.getSaveButton().isEnabled()).toBe(false);
      expect(notificationPage.getResetButton().isDisplayed()).toBe(true);
      expect(notificationPage.getResetButton().isEnabled()).toBe(false);
      expect(notificationPage.getCancelButton().isDisplayed()).toBe(true);
      expect(notificationPage.getCancelButton().isEnabled()).toBe(true);
    });
  });



  it('should enable Reset button when user change something in form and reset all values to initial ' +
    'when user click on that button', () => {
    notificationPage.setTitle('Some change');
    notificationPage.setContent('Some other change');

    // expect to Reset button is enabled
    expect(notificationPage.getResetButton().isEnabled()).toBe(true);

    notificationPage.getResetButton().click();

    expect(notificationPage.getTitleInput().getAttribute('value')).toEqual('');
    expect(notificationPage.getContentInput().getAttribute('value')).toEqual('');
    expect(notificationPage.getResetButton().isEnabled()).toBe(false);
  });



  it('should show errors message that both input fields must be filled (for title and content) ' +
    'when user put focus on field and then leave it', () => {
    notificationPage.setTitle('');
    notificationPage.setContent('');

    // expect to display error message about required values for title and content
    expect(notificationPage.getTitleRequiredError().isDisplayed()).toBe(true);
    expect(notificationPage.getContentRequiredError().isDisplayed()).toBe(true);
  });



  it('should enable Save button when whole form is filled (both title and content input ' +
    'have non-empty value) and close modal when click on Save button', () => {
    notificationPage.setTitle('Some title value');
    notificationPage.setContent('Some content value');

    expect(notificationPage.getSaveButton().isEnabled()).toBe(true);
    notificationPage.getSaveButton().click();
  });



  it('should show filled form when user click on Edit link in menu option on page for ' +
    'review notification list', () => {
    //store data from first notification in list
    let existingNotification = {'title':'', 'content':''};
    notificationListPage.getTitleFromNotificationPanel(0).getText().then(value => {
      existingNotification.title = value;
    });
    notificationListPage.getContentFromNotificationPanel(0).getText().then(value => {
      existingNotification.content = value;
    });

    expect(notificationListPage.getMenuFromNotificationPanel(0).isDisplayed()).toBe(true);
    notificationListPage.getMenuFromNotificationPanel(0).click();

    browser.wait(function() {
      return notificationListPage.getContentOfMenuFromNotificationPanel(0).isDisplayed();
    }).then(function() {
      notificationListPage.getEditMenuOptionFromNotificationPanel(0).click();

      browser.wait(function() {
        // expected to open modal dialog - page for editing existing notification
        return notificationPage.getNotificationElement().isDisplayed();
      }).then(function() {
        // expected view of modal dialog - input fields should have values same as property values of selected notification
        notificationPage.getTitleInput().getAttribute('value').then(value => {
          expect(value).toEqual(existingNotification.title);
        });
        notificationPage.getContentInput().getAttribute('value').then(value => {
          expect(value).toEqual(existingNotification.content);
        });
        expect(notificationPage.getSaveButton().isDisplayed()).toBe(true);
        expect(notificationPage.getSaveButton().isEnabled()).toBe(false);
        expect(notificationPage.getResetButton().isDisplayed()).toBe(true);
        expect(notificationPage.getResetButton().isEnabled()).toBe(false);
        expect(notificationPage.getCancelButton().isDisplayed()).toBe(true);
        expect(notificationPage.getCancelButton().isEnabled()).toBe(true);
      });
    });
   });
});
