import {Component, EventEmitter, Input, OnInit} from '@angular/core';
import {ApartmentCreate} from "../../models/apartment-create";
import {BsModalRef} from "ngx-bootstrap";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ApartmentService} from "../../../core/services/apartment.service";
import {Building} from "../../models/building";
import {NotFoundError} from "../../errors/not-found-error";
import {BadRequestError} from "../../errors/bad-request-error";
import {AppError} from "../../errors/app-error";
import {ToasterService} from "angular5-toaster/dist";
import {Apartment} from "../../models/apartment";

@Component({
  selector: 'app-add-apartment-modal',
  templateUrl: './add-apartment-modal.component.html',
  styleUrls: ['./add-apartment-modal.component.css']
})
export class AddApartmentModalComponent implements OnInit {

  @Input() apartmentList: ApartmentCreate[] = [];
  form: FormGroup;
  bsModalRef: BsModalRef;
  building: Building;
  buildingRegistration: boolean;
  apartmentCreated: EventEmitter<Apartment> = new EventEmitter();

  constructor(private fb: FormBuilder, private apartmentService: ApartmentService, private toasterService: ToasterService) {
    this.form = this.fb.group({
      numberModal: ['',
        Validators.required
      ],
      floor: ['',
        Validators.required
      ],
      surface: ['',
        Validators.required
      ]
    });
  }

  ngOnInit() {
  }

  get number() {
    return this.form.get('numberModal');
  }

  get floor() {
    return this.form.get('floor');
  }

  get surface() {
    return this.form.get('surface');
  }

  addApartment() {
    if(this.buildingRegistration)
      this.addApartmentRegistration();
    else
      this.addApartmentUpdate();
  }

  addApartmentRegistration() {
    this.apartmentList.push(new ApartmentCreate(this.number.value, this.floor.value, this.surface.value));

    this.number.setValue('');
    this.floor.setValue('');
    this.surface.setValue('');
    this.number.markAsUntouched();
    this.floor.markAsUntouched();
    this.surface.markAsUntouched();

    this.form.updateValueAndValidity();

  }

  addApartmentUpdate(){
    let apartmentCreate = new ApartmentCreate(this.number.value, this.floor.value, this.surface.value);
    this.apartmentService.save(apartmentCreate, 'buildings', this.building.id, 'apartments')
      .subscribe(data => {
        this.toasterService.pop('success', 'Congratulation', 'Successfully registered new tenant!');
        this.number.setValue('');
        this.floor.setValue('');
        this.surface.setValue('');
        this.number.markAsUntouched();
        this.floor.markAsUntouched();
        this.surface.markAsUntouched();

        this.form.updateValueAndValidity();
        this.apartmentCreated.emit(data);

    }, (error: AppError) => {
        if (error instanceof BadRequestError)
          this.toasterService.pop('error', 'Error', 'Given data have bad format!');
        else if (error instanceof NotFoundError)
          this.toasterService.pop('error', 'Error', 'Registration can\'t be done, because tenant role is missing!');
        else {
          this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
          throw error;
        }
      });
  }

}
