import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
// service
import { DataService } from "./data.service";
// models
import { Notification } from "../../shared/models/notification";
import { PageAndSort } from "../../shared/models/page-and-sort";


@Injectable()
export class NotificationService extends DataService<Notification> {
  constructor(http: HttpClient) {
    super('', http);
  }

  searchFromTo(buildingId: number, from: string, to: string, pageAndSort: PageAndSort): Observable<Array<Notification>> {
    let params: HttpParams = new HttpParams()
      .append('from', from)
      .append('to', to)
      .append('pageNumber', pageAndSort.pageNumber)
      .append('pageSize', pageAndSort.pageSize)
      .append('sortDirection', pageAndSort.sortDirection)
      .append('sortProperty', pageAndSort.sortProperties[0]);

    return this.http.get<Array<Notification>>(`${this.urlBase}/buildings/${buildingId}/billboard/notifications_from_to`, {params})
      .catch(this.handleErrors);
  }
}
