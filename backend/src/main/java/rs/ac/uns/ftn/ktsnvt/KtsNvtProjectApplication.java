package rs.ac.uns.ftn.ktsnvt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class KtsNvtProjectApplication {
	public static void main(String[] args) {
		SpringApplication.run(KtsNvtProjectApplication.class, args);
	}

}
