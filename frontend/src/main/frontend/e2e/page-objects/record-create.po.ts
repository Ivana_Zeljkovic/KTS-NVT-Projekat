import {by, element, ElementFinder} from "protractor";

export class RecordCreatePage {
  // get methods
  getRecordCreateElement(): ElementFinder {
    return element(by.tagName('app-record-create'));
  }

  getTitle(): ElementFinder {
    return element(by.css('app-record-create div.modal-title'));
  }

  getContentInput(): ElementFinder {
    return element(by.id('content'));
  }

  getSaveButton(): ElementFinder {
    return element(by.css('app-record-create div.buttons > button:nth-child(1)'));
  }

  getResetButton(): ElementFinder {
    return element(by.css('app-record-create div.buttons > button:nth-child(2)'));
  }

  getCancelButton(): ElementFinder {
    return element(by.css('app-record-create div.buttons > button:nth-child(3)'));
  }

  // set methods
  setContent(text: string) {
    this.getContentInput().sendKeys(text);
  }

  // get error methods
  getContentRequiredError(): ElementFinder {
    return element(by.id('contentRequiredError'));
  }
}
