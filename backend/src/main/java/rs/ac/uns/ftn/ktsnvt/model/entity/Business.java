package rs.ac.uns.ftn.ktsnvt.model.entity;

import org.hibernate.validator.constraints.Email;
import rs.ac.uns.ftn.ktsnvt.model.enumeration.BusinessType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "business")
public class Business {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private String phoneNumber;

    @Column(nullable = false)
    @Email
    private String email;

    @Column(nullable = false)
    private BusinessType businessType;

    @Column(nullable = false)
    private boolean isCompany;

    @Column(nullable = false, columnDefinition = "INTEGER DEFAULT 0")
    @Version
    private int version;

    @Column(nullable = false, columnDefinition = "BOOL DEFAULT FALSE")
    private boolean deleted;

    @Column(nullable = false)
    private boolean confirmed;

    @OneToMany(mappedBy = "businessOwner", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private List<Account> accounts;

    @OneToOne
    private Address address;

    @OneToMany(mappedBy = "responsibleInstitution", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private List<Damage> responsibleForDamages;

    @OneToMany(mappedBy = "responsibleInstitution", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<DamageResponsibility> responsibleForDamageTypes;

    @OneToMany(mappedBy = "selectedBusiness", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Damage> damagesForRepair;

    public Business() {
        this.accounts = new ArrayList<>();
        this.responsibleForDamages = new ArrayList<>();
        this.responsibleForDamageTypes = new ArrayList<>();
        this.damagesForRepair = new ArrayList<>();
        this.confirmed = false;
    }

    public Business(String name, String description, String phoneNumber, String email,
                    BusinessType businessType, boolean isCompany) {
        this.name = name;
        this.description = description;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.businessType = businessType;
        this.isCompany = isCompany;
        this.confirmed = false;
        this.accounts = new ArrayList<>();
        this.responsibleForDamages = new ArrayList<>();
        this.responsibleForDamageTypes = new ArrayList<>();
        this.damagesForRepair = new ArrayList<>();
    }

    public Business(String name, String description, String phoneNumber, String email,
                    BusinessType businessType, boolean isCompany, List<Account> accounts) {
        this.name = name;
        this.description = description;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.businessType = businessType;
        this.isCompany = isCompany;
        this.confirmed = false;
        this.accounts = accounts;
        this.accounts = new ArrayList<>();
        this.responsibleForDamages = new ArrayList<>();
        this.responsibleForDamageTypes = new ArrayList<>();
        this.damagesForRepair = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public BusinessType getBusinessType() { return businessType; }

    public void setBusinessType(BusinessType businessType) { this.businessType = businessType; }

    public boolean isIsCompany() {
        return isCompany;
    }

    public void setIsCompany(boolean isCompany) {
        this.isCompany = isCompany;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean getConfirmed() { return confirmed; }

    public void setConfirmed(Boolean confirmed) { this.confirmed = confirmed; }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    public Address getAddress() { return address; }

    public void setAddress(Address address) { this.address = address; }

    public List<Damage> getResponsibleForDamages() { return responsibleForDamages; }

    public void setResponsibleForDamages(List<Damage> responsibleForDamages) { this.responsibleForDamages = responsibleForDamages; }

    public List<DamageResponsibility> getResponsibleForDamageTypes() { return responsibleForDamageTypes; }

    public void setResponsibleForDamageTypes(List<DamageResponsibility> responsibleForDamageTypes) { this.responsibleForDamageTypes = responsibleForDamageTypes; }

    public List<Damage> getDamagesForRepair() { return damagesForRepair; }

    public void setDamagesForRepair(List<Damage> damagesForRepair) { this.damagesForRepair = damagesForRepair; }

    public boolean findDamageWithIdInResponsibleForList(Long damageId) {
        for(Damage damage : this.responsibleForDamages) {
            if(damage.getId().equals(damageId))
                return true;
        }
        return false;
    }

    public boolean findDamageWithIdInRepairForList(Long damageId) {
        for(Damage damage : this.damagesForRepair) {
            if(damage.getId().equals(damageId))
                return true;
        }
        return false;
    }
}
