import { Component } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { ToasterConfig, ToasterService } from "angular5-toaster/dist";
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
// service
import { DamageService } from "../../core/services/damage.service";
import { JwtService } from "../../core/services/jwt.service";
import { CommentService } from "../../core/services/comment.service";
// model
import { Damage } from "../../shared/models/damage";
import { Comment } from "../../shared/models/comment";
import { PageAndSort } from "../../shared/models/page-and-sort";
// error
import { AppError } from "../../shared/errors/app-error";
import { ForbiddenError } from "../../shared/errors/forbidden-error";
import { NotFoundError } from "../../shared/errors/not-found-error";
import { BadRequestError } from "../../shared/errors/bad-request-error";
// component
import { CommentModalComponent } from '../comment-modal/comment-modal.component';


@Component({
  selector: 'app-damage-details',
  templateUrl: './damage-details.component.html',
  styleUrls: ['./damage-details.component.css']
})
export class DamageDetailsComponent {
  roles: string[];
  damageId: number;
  damage: Damage;
  commentList: Array<Comment>;
  toasterConfig: ToasterConfig;

  // Elements for bootstrap paging
  currentPage: number;
  totalItems: number;
  maxSize: number;
  itemsPerPage: number;

  // Elements for getting data from server
  pageSize: string;
  sortDirection: string;
  sortProperties: string[];

  bsModalRef: BsModalRef;
  imageShow: boolean;


  constructor(private route: ActivatedRoute, private damageService: DamageService,
              private jwtService: JwtService, private toasterService: ToasterService,
              private commentService: CommentService, private modalService: BsModalService) {
    this.roles = jwtService.getRolesFromToken();
    this.toasterConfig = new ToasterConfig({
      timeout: {success: 2000, error: 4000}
    });

    this.route.params.subscribe(params => {
      if (params['id']) {
        this.damageId = params['id'];
        this.checkRoleAndSetUrl();
      }
    });

    this.imageShow = false;

    // Sett up for bootstrap paging
    this.currentPage = 1;
    this.maxSize = 3;
    this.itemsPerPage = 4;

    // Sett up for getting damages request
    this.pageSize = '4';
    this.sortDirection = 'ASC';
    this.sortProperties = ['date'];
  }

  add() {
    this.bsModalRef = this.modalService.show(CommentModalComponent);
    this.bsModalRef.content.damageId = this.damageId;
    this.bsModalRef.content.buildingId = this.damage.buildingId;
    this.bsModalRef.content.commentPosted.subscribe(comment =>{
      if(!comment) return;
      this.currentPage = Math.ceil((this.totalItems + 1) / this.itemsPerPage);
      this.loadComments();
    });
  }

  edit(comment) {
    this.bsModalRef = this.modalService.show(CommentModalComponent);
    this.bsModalRef.content.damageId = this.damageId;
    this.bsModalRef.content.content = comment.content;
    this.bsModalRef.content.buildingId = this.damage.buildingId;
    this.bsModalRef.content.commentId = comment.id;
    this.bsModalRef.content.commentPosted.subscribe(comment =>{
      this.loadComments();
    });
  }

  remove(comment) {
    this.commentService.remove(comment.id, 'damages', this.damageId, 'comments').subscribe(data =>{
      this.loadComments();
    }, error => {
      if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Error', 'Selected comment doesn\'t belong to selected damage!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
      else {
        this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
        throw error;
      }
    });
  }

  toggleImage() {
    this.imageShow = !this.imageShow;
  }

  havePermissionForComment() {
    return ((this.damage.responsiblePerson && this.damage.responsiblePerson.username === this.jwtService.getUsernameFromToken()) ||
      (this.damage.responsibleInstitution && this.damage.responsibleInstitution.usernames.indexOf(this.jwtService.getUsernameFromToken()) !== -1 ||
      (this.damage.selectedBusiness && this.damage.selectedBusiness.usernames.indexOf(this.jwtService.getUsernameFromToken()) !== -1)));
  }

  pageChanged(event: any) {
    this.currentPage = event.page;
    this.loadComments();
  }

  private checkRoleAndSetUrl() {
    let urlFirstSuffix = '';
    let urlSecondSuffix = 'damages';
    let id = this.jwtService.getIdFromToken();

    if(this.roles.indexOf('TENANT') != -1)
      urlFirstSuffix = (this.roles.indexOf('COUNCIL_PRESIDENT') != -1) ? 'presidents' : 'tenants';
    else if((this.roles.indexOf('COMPANY') != -1) || (this.roles.indexOf('INSTITUTION') != -1))
      urlFirstSuffix = 'businesses';

    this.loadDamage(urlFirstSuffix, urlSecondSuffix, id);
  }

  private loadDamage(urlFirstSuffix: string, urlSecondSuffix: string, id: number) {
    this.damageService.get(this.damageId, urlFirstSuffix, id, urlSecondSuffix)
      .subscribe(damage => {
        this.damage = damage;
        this.loadComments();
      }, (error: AppError) => {
        if(error instanceof BadRequestError)
          this.toasterService.pop('error', 'Error', 'Damage is already fixed!');
        else if(error instanceof ForbiddenError)
          this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
        else if(error instanceof NotFoundError)
          this.toasterService.pop('error', 'Error', 'Damage not found!');
        else {
          this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
          throw error;
        }
      });
  }

  private loadComments() {
    let page = new PageAndSort((this.currentPage - 1).toString(), this.pageSize, this.sortDirection, this.sortProperties);

    this.commentService.getPage(page, 'damages', this.damageId, 'comments')
      .subscribe(comments => {
        if (comments.length > 0)
          this.totalItems = comments[0].numberOfElements;
        else this.totalItems = 0;
        this.commentList = comments;
      }, (error: AppError) => {
        if(error instanceof NotFoundError)
          this.toasterService.pop('error', 'Error', 'Damage not found!');
        else {
          this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
          throw error;
        }
      })
  }
}
