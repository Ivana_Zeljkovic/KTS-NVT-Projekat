import {by, element, ElementFinder} from "protractor";

export class NotificationPage {
  // get methods
  getNotificationElement(): ElementFinder {
    return element(by.tagName('app-notification'));
  }

  getTitleInput(): ElementFinder {
    return element(by.id('title'));
  }

  getContentInput(): ElementFinder {
    return element(by.id('content'));
  }

  getSaveButton(): ElementFinder {
    return element(by.buttonText('Save'));
  }

  getResetButton(): ElementFinder {
    return element(by.buttonText('Reset'));
  }

  getCancelButton(): ElementFinder {
    return element(by.buttonText('Cancel'));
  }

  // set methods
  setTitle(text: string) {
    this.getTitleInput().sendKeys(text);
  }

  setContent(text: string) {
    this.getContentInput().sendKeys(text);
  }

  // get errors method
  getTitleRequiredError(): ElementFinder {
    return element(by.id('titleRequiredError'));
  }

  getContentRequiredError(): ElementFinder {
    return element(by.id('contentRequiredError'));
  }
}
