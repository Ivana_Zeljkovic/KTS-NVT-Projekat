import {by, element, ElementFinder} from "protractor";

export class MeetingItemPage {
  // get methods
  getMeetingItemElement(): ElementFinder {
    return element(by.tagName('app-meeting-item'));
  }

  getTitleInput(): ElementFinder {
    return element(by.id('title'));
  }

  getContentInput(): ElementFinder {
    return element(by.id('content'));
  }

  getChooseDamageLink(): ElementFinder {
    return element(by.id('chooseDamageLink'));
  }

  getSelectedDamagePanel(): ElementFinder {
    return element(by.css('div.selected-damage-details'));
  }

  getDescriptionFromSelectedDamagePanel(): ElementFinder {
    return element(by.css('div.selected-damage-details div.panel-body'))
  }

  getRemoveDamageLinkFromSelectedDamagePanel(): ElementFinder {
    return element(by.css('div.selected-damage-details a'));
  }

  getQuestionnaireLink(): ElementFinder {
    return element(by.css('div.questionnaire-create-update > a'));
  }

  getSaveButton(): ElementFinder {
    return element(by.buttonText('Save'));
  }

  getResetButton(): ElementFinder {
    return element(by.buttonText('Reset'));
  }

  getCancelButton(): ElementFinder {
    return element(by.buttonText('Cancel'));
  }

  // set methods
  setTitle(text: string) {
    this.getTitleInput().sendKeys(text);
  }

  setContent(text: string) {
    this.getContentInput().sendKeys(text);
  }

  // get errors method
  getTitleRequiredError(): ElementFinder {
    return element(by.id('titleRequiredError'));
  }

  getContentRequiredError(): ElementFinder {
    return element(by.id('contentRequiredError'));
  }
}
