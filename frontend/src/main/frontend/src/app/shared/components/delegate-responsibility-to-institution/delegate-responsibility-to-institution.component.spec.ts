import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DelegateResponsibilityToInstitutionComponent } from './delegate-responsibility-to-institution.component';

describe('DelegateResponsibilityToInstitutionComponent', () => {
  let component: DelegateResponsibilityToInstitutionComponent;
  let fixture: ComponentFixture<DelegateResponsibilityToInstitutionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DelegateResponsibilityToInstitutionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DelegateResponsibilityToInstitutionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
