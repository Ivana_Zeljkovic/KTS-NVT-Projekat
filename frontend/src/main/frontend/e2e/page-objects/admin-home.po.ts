import {by, element, ElementFinder} from "protractor";

export class AdminHomePage {
  getAdminHomeElement(): ElementFinder {
    return element(by.tagName('app-admin-home'));
  }

  getAdminPanel(): ElementFinder {
    return element(by.id('adminsLink'));
  }

  getTenantPanel(): ElementFinder {
    return element(by.id('tenantsLink'));
  }

  getBuildingsPanel(): ElementFinder {
    return element(by.id('buildingsLink'));
  }

  getBusinessesPanel(): ElementFinder {
    return element(by.id('businessesLink'));
  }

  getApartmentsPanel(): ElementFinder {
    return element(by.id('apartmentsLink'));
  }

}
