package rs.ac.uns.ftn.ktsnvt.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import rs.ac.uns.ftn.ktsnvt.model.entity.Administrator;

import java.util.List;

/**
 * Created by Ivana Zeljkovic on 05/11/2017.
 */
public interface AdministratorService {

    Administrator save(Administrator administrator);

    Administrator findOne(Long id);

    List<Administrator> findAll();

    Page<Administrator> findAll(Pageable pageable);

    void remove(Long id);

    boolean checkPasswordUpdate(Administrator administrator, String currentPassword, String newPassword);

    void checkPermissionForCurrentAdmin(Long id);

    Page<Administrator> getAllAdmins(Integer pageNumber, Integer pageSize,
                                       String sortDirection, String sortProperty);
}
