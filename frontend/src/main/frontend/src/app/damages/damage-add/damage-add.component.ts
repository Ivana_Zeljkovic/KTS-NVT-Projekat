import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { ToasterService } from "angular5-toaster/dist";
import * as _ from "lodash";
// model
import { DamageCreate } from "../../shared/models/damage-create";
// service
import { DamageService } from "../../core/services/damage.service";
import { JwtService } from "../../core/services/jwt.service";
// error
import { AppError } from "../../shared/errors/app-error";
import { BadRequestError } from "../../shared/errors/bad-request-error";
import { ForbiddenError } from "../../shared/errors/forbidden-error";
import { NotFoundError } from "../../shared/errors/not-found-error";
// component
import { ChooseApartmentModalComponent } from "../choose-apartment-modal/choose-apartment-modal.component";


@Component({
  selector: 'app-damage-add',
  templateUrl: './damage-add.component.html',
  styleUrls: ['./damage-add.component.css']
})
export class DamageAddComponent implements OnInit {
  @Input() model: DamageCreate;
  @Input() initialValues: DamageCreate;
  @Output() submited = new EventEmitter<boolean>();

  @ViewChild('file') fileInput: any;
  fileToUpload: File;

  apartmentId: number;
  damageLocation: string;
  disableSaveAndReset: boolean;
  damageTypeList: string[] = ['OTHER', 'ELECTRICAL_DAMAGE', 'EXTERIOR_DAMAGE', 'GAS_DAMAGE', 'PLUMBING_DAMAGE'];


  constructor(public modalRef: BsModalRef, private damageService: DamageService,
              private jwtService: JwtService, private toasterService: ToasterService,
              private modalService: BsModalService) { }

  ngOnInit() {
    this.disableSaveAndReset = true;
    this.damageLocation = 'building';
    this.apartmentId = -1;
  }

  onSubmit() {
    this.model.apartmentId = (this.apartmentId !== -1) ? this.apartmentId : null;

    this.damageService.save(this.model, 'buildings', this.jwtService.getBuildingIdFromToken(), 'damages')
      .subscribe(() => {
        this.submited.emit(true);
        this.modalRef.hide();
      }, (error: AppError) => {
        if(error instanceof BadRequestError)
          this.toasterService.pop('error', 'Error',
            'Given data have bad format/Selected apartment doesn\'t belong to selected building!');
        else if(error instanceof ForbiddenError)
          this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
        else if(error instanceof NotFoundError)
          this.toasterService.pop('error', 'Error', 'Building not found!');
        else {
          this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
          throw error;
        }
      });
  }

  chooseApartment() {
    const modalRef = this.modalService.show(ChooseApartmentModalComponent);

    modalRef.content.apartmentSelected.subscribe(apartmentId => {
      this.apartmentId = apartmentId;
    });
  }

  chooseFile(event: any) {
    let files = event.target.files;
    if(files && files.length > 0) {
      this.fileToUpload = event.target.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(this.fileToUpload);
      reader.onload = () => {
        this.model.image = reader.result.split(',')[1];
      };
    }
    else {
      this.model.image = '';
      this.fileToUpload = null;
    }
  }

  reset() {
    this.model = _.cloneDeep(this.initialValues);
    this.disableSaveAndReset = true;
    this.damageLocation = 'building';
    this.apartmentId = -1;
    this.fileInput.nativeElement.value = "";
    this.fileToUpload = null;
  }

  changeExist(newValues) {
    let urgentBool = newValues.urgent === 'true';
    this.disableSaveAndReset = (newValues.description === this.initialValues.description &&
      newValues.typeName === this.initialValues.typeName &&
      urgentBool === this.initialValues.urgent &&
      newValues.locationOfDamage === 'building');
  }

  apartmentIsNotSelected(): boolean {
    return (this.damageLocation === 'apartment' && this.apartmentId === -1);
  }
}
