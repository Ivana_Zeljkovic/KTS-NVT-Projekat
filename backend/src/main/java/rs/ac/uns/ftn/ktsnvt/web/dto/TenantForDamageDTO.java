package rs.ac.uns.ftn.ktsnvt.web.dto;

import rs.ac.uns.ftn.ktsnvt.model.entity.Tenant;

import java.io.Serializable;

/**
 * Created by Ivana Zeljkovic on 11/01/2018
 */
public class TenantForDamageDTO implements Serializable {

    private String firstName;
    private String lastName;
    private Long id;
    private String username;

    public TenantForDamageDTO() { }

    public TenantForDamageDTO(String firstName, String lastName, Long id, String username) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.id = id;
        this.username = username;
    }

    public TenantForDamageDTO(Tenant tenant) {
        this.firstName = tenant.getFirstName();
        this.lastName = tenant.getLastName();
        this.id = tenant.getId();
        this.username = tenant.getAccount().getUsername();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() { return username; }

    public void setUsername(String username) { this.username = username; }
}
