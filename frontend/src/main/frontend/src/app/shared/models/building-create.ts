import { AddressCreate } from "./address-create";
import { ApartmentCreate } from "./apartment-create";

export class BuildingCreate {
  constructor(private numberOfFloors: number,
              private hasParking: boolean,
              private address: AddressCreate,
              private apartments: Array<ApartmentCreate>,
              private firmManagerId?: number) { }
}
