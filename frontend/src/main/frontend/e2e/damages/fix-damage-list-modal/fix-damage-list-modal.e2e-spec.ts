import {browser} from "protractor";
import {BusinessListModalPage} from "../business-list-modal/page-objects/buisness-list-modal.po";
import {DamageListPage} from "../../page-objects/damage-list.po";
import {LoginPage} from "../../page-objects/login.po";
import {FixDamageModalPage} from "./page-objects/fix-damage-modal.po";

describe('Modal for choosing business which will fix the damage', () => {

  let loginPage: LoginPage;
  let damageList: DamageListPage;
  let fixDamageListModal: FixDamageModalPage;

  beforeAll(() => {
    loginPage = new LoginPage();
    damageList = new DamageListPage();
    fixDamageListModal = new FixDamageModalPage();

    browser.get('/');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');

    loginPage.setUsername('kaca');
    loginPage.setPassword('kaca');
    loginPage.getSignInButton().click();

    browser.wait(function () {
      return browser.getCurrentUrl().then(value => {
        return value == 'http://localhost:49152/#/home/notifications'
      });
    }).then(function () {
      browser.get('http://localhost:49152/#/damages');
      browser.wait(function () {
        return browser.getCurrentUrl().then(value => {
          return value == 'http://localhost:49152/#/damages'
        });
      });
    });
  });

  it('Should open modal, and choose a business which will fix the damage, then that damage will be labeled as in fixing process'
    , () => {

      let damageListCount = null;

      browser.wait(function () {
        return damageList.getPanelsFromDamageList().get(0).isDisplayed();
      }).then(() => {
        damageList.getPanelsFromDamageList().count().then(function (value) {
          damageListCount = value;
        });

        damageList.getMenuFromDamagePanel(0).click().then(() => {
          damageList.getChooseBusiness().click().then(() => {

            browser.wait(function () {
              return fixDamageListModal.getComponent().isDisplayed();
            }).then(function () {
                fixDamageListModal.getButton().click().then(function () {
                  browser.wait(function(){
                    return damageList.getLabelInFixingProcessFromDamagePanel(0).isDisplayed()
                  }).then(function(){
                    expect(damageList.getLabelInFixingProcessFromDamagePanel(0).isDisplayed()).toBe(true);
                  });
                });
              }
            );
          });
        });
      });

    });


  afterAll(() => {
    browser.executeScript('window.localStorage.clear();');
  })


});
