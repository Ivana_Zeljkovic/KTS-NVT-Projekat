import { Component, Input, OnInit } from '@angular/core';
import { Administrator } from "../../shared/models/administrator";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ToasterConfig, ToasterService } from "angular5-toaster/dist";
// service
import { JwtService } from "../../core/services/jwt.service";
import { AdminService } from "../../core/services/admin.service";
import { AuthService } from "../../core/services/auth.service";
// model
import { AdministratorUpdate } from "../../shared/models/administrator-update";
// validator
import { PasswordValidator } from "../../shared/validators/password.validator";
//error
import { BadRequestError } from "../../shared/errors/bad-request-error";
import { NotFoundError } from "../../shared/errors/not-found-error";
import { AppError } from "../../shared/errors/app-error";


@Component({
  selector: 'app-admin-profile',
  templateUrl: './admin-profile.component.html',
  styleUrls: ['./admin-profile.component.css']
})
export class AdminProfileComponent implements OnInit {
  adminData: Administrator;
  form: FormGroup;
  initialValues;
  disableSaveOrReset: boolean;
  @Input() toasterConfig: ToasterConfig;

  constructor(private fb: FormBuilder, private passwordValidator: PasswordValidator,
              private jwtService: JwtService, private adminService: AdminService,
              private authService: AuthService, private toasterService: ToasterService) { }

  ngOnInit() {
    this.authService.currentUser()
      .subscribe(data => {
        let admin = (data as Administrator);
        this.adminData = admin;

        this.form = this.fb.group({
          firstName: [this.adminData.firstName, [
            Validators.required,
            Validators.minLength(2)
          ]],
          lastName: [this.adminData.lastName, [
            Validators.required,
            Validators.minLength(2)
          ]],
          username: this.jwtService.getUsernameFromToken(),
          currentPassword: '',
          passwords: this.fb.group({
            password: '',
            confirmPassword: '',
          })
        });

        this.initialValues = this.form.value;
        this.disableSaveOrReset = true;

        this.form.valueChanges.subscribe(newValues => {
          // disable the save and reset button if there are no changes
          this.disableSaveOrReset = !this.changeExist(newValues);
        });

        this.password.valueChanges.subscribe((newValue) => {
          // if user write some value in new password field, fields current password and confirm password
          // become required too
          // else, all validators from all password fields are removed
          (newValue.trim() === '') ? this.removeValidators() : this.addValidators();
        });
      }, (error: AppError) => {
        throw error
      });
  }

  get firstName() {
    return this.form.get('firstName');
  }

  get lastName() {
    return this.form.get('lastName');
  }

  get username() {
    return this.form.get('username');
  }

  get currentPassword() {
    return this.form.get('currentPassword');
  }

  get passwords() {
    return this.form.get('passwords');
  }

  get password() {
    return this.form.get('passwords').get('password');
  }

  get confirmPassword() {
    return this.form.get('passwords').get('confirmPassword');
  }

  private changeExist(newValues): boolean {
    return !(JSON.stringify(newValues) === JSON.stringify(this.initialValues));
  }

  private addValidators() {
    if(this.password.value.length === 1) {
      this.password.setValidators([Validators.required, Validators.minLength(5)]);
      this.currentPassword.setValidators([Validators.required]);
      this.confirmPassword.setValidators([Validators.required]);
      this.passwords.setValidators([this.passwordValidator.matchPasswords.bind(this.passwordValidator)]);
      this.updateAndValidity();
    }
  }

  private removeValidators() {
    this.password.clearValidators();
    this.currentPassword.clearValidators();
    this.confirmPassword.clearValidators();
    this.passwords.clearValidators();
    this.updateAndValidity();
  }

  private updateAndValidity() {
    this.password.updateValueAndValidity({emitEvent : false});
    this.currentPassword.updateValueAndValidity();
    this.confirmPassword.updateValueAndValidity();
  }

  save() {
    if((this.currentPassword.value.trim() !== '' || this.confirmPassword.value.trim() !== '') && this.password.value === '')
      this.toasterService.pop('warning', 'Warning', 'New password value is missing!');
    else {
      let currentPasswordVal = null;
      let newPasswordVal = null;
      if (this.password.value !== '') {
        currentPasswordVal = this.currentPassword.value;
        newPasswordVal = this.password.value;
      }

      let adminUpdate = new AdministratorUpdate(this.firstName.value,
        this.lastName.value, currentPasswordVal, newPasswordVal);

      this.adminService.update(this.adminData.id, adminUpdate)
        .subscribe(() => {
          this.form.get('currentPassword').setValue('');
          this.form.get('passwords').get('password').setValue('');
          this.form.get('passwords').get('confirmPassword').setValue('');
          this.initialValues = this.form.value;
          this.disableSaveOrReset = true;
          this.toasterService.pop('success', '', 'Update of personal data successfully done!');
        }, (error: AppError) => {
          if (error instanceof BadRequestError)
            this.toasterService.pop('error', 'Error', 'Given data have bad format/Current password is invalid!');
          else if (error instanceof NotFoundError)
            this.toasterService.pop('error', 'Error', 'Wrong ID of administrator!');
          else {
            this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
            throw error;
          }
        });
    }
  }

  reset() {
    this.form.setValue(this.initialValues);
  }
}
