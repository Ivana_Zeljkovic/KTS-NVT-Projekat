package rs.ac.uns.ftn.ktsnvt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Meeting;
import rs.ac.uns.ftn.ktsnvt.model.entity.MeetingItem;
import rs.ac.uns.ftn.ktsnvt.model.entity.Questionnaire;
import rs.ac.uns.ftn.ktsnvt.repository.MeetingItemRepository;
import rs.ac.uns.ftn.ktsnvt.repository.MeetingRepository;
import rs.ac.uns.ftn.ktsnvt.repository.QuestionnaireRepository;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * Created by Nikola Garabandic on 22/11/2017.
 */
@Service
public class MeetingServiceImpl implements MeetingService {

    @Autowired
    private MeetingRepository meetingRepository;

    @Autowired
    private MeetingItemRepository meetingItemRepository;

    @Autowired
    private QuestionnaireRepository questionnaireRepository;


    @Override
    @Transactional(readOnly = false)
    public Meeting save(Meeting meeting) {
        return meetingRepository.save(meeting);
    }

    @Override
    @Transactional(readOnly = true)
    public Meeting findOne(Long meetingId) {
        Meeting meeting = this.meetingRepository.findOne(meetingId);
        if(meeting == null) throw new NotFoundException("Meeting not found!");
        return meeting;
    }

    @Override
    public void checkMeetingFinished(Meeting meeting) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(meeting.getDate());
        cal.add(Calendar.MINUTE, meeting.getDuration());

        //Ako sastanak jos nije prosao
        if(Calendar.getInstance().getTime().before(cal.getTime()))
            throw new BadRequestException("Selected meeting hasn't finished yet!");
    }

    @Override
    public void checkMeetingStart(Meeting meeting) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(meeting.getDate());
        cal.add(Calendar.MINUTE, meeting.getDuration());

        if(meeting.getDate().before(new Date()) && Calendar.getInstance().getTime().before(cal.getTime()))
            throw new BadRequestException("Selected meeting has already started!");
        else if(!Calendar.getInstance().getTime().before(cal.getTime()))
            throw new BadRequestException("Selected meeting is finished!");
    }

    @Override
    public List<Meeting> findAll() {
        return this.meetingRepository.findAll();
    }

    @Override
    public void clearMeetingItemsList(Meeting meeting) {
        meeting.getMeetingItems().forEach(meetingItem -> {
            meetingItem.setMeeting(null);
            if(meetingItem.getQuestionnaire() != null && meetingItem.getQuestionnaire().getDateExpire() != null) {
                meetingItem.getQuestionnaire().setDateExpire(null);
                this.questionnaireRepository.save(meetingItem.getQuestionnaire());
            }
        });
        meeting.getMeetingItems().clear();
    }

    @Override
    public boolean meetingIsInTheFuture(Meeting meeting) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(meeting.getDate());
        cal.add(Calendar.MINUTE, meeting.getDuration());

        return cal.getTimeInMillis() > System.currentTimeMillis();
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Meeting> findAllByBuildingId(Long buildingId, Integer pageNumber, Integer pageSize,
                                             String sortDirection, String sortProperty) {
        return this.meetingRepository.findAllByBuildingId(buildingId, new Date(), new PageRequest(pageNumber, pageSize,
                (sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortProperty));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Meeting> findAllFinishedByBuildingId(Long buildingId, Integer pageNumber, Integer pageSize,
                                                     String sortDirection, String sortProperty) {
        return this.meetingRepository.findAllFinishedByBuildingId(buildingId, new Date(), new PageRequest(pageNumber, pageSize,
                (sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortProperty));
    }

    @Override
    @Transactional(readOnly = true)
    public Meeting findFirstNextMeetingInBuilding(Long councilId) {
        return this.meetingRepository.findFirstNextMeetingInBuilding(councilId, new Date());
    }

    @Override
    @Transactional(readOnly = false)
    public void remove(Meeting meeting) {
        if(!meeting.getMeetingItems().isEmpty()) {
            List<MeetingItem> meetingItems = new ArrayList<>();
            for(MeetingItem meetingItem : meeting.getMeetingItems()) {
                meetingItems.add(meetingItem);
            }

            if(!meetingItems.isEmpty()) {
                meetingItems.forEach(meetingItem -> {
                    meetingItem.setMeeting(null);
                    this.meetingItemRepository.save(meetingItem);
                    if(meetingItem.getQuestionnaire() != null) {
                       Questionnaire questionnaire = meetingItem.getQuestionnaire();
                       questionnaire.setDateExpire(null);
                       this.questionnaireRepository.save(questionnaire);
                    }
                });
            }
        }
        this.meetingRepository.delete(meeting.getId());
    }
}
