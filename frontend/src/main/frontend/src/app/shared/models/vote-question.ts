export interface VoteQuestion {
  id: number;
  questionId: number;
  answer: string;
}
