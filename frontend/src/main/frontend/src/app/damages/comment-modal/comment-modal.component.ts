import { ChangeDetectorRef, Component, EventEmitter, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// service
import { CommentService } from '../../core/services/comment.service';
// model
import { CommentCreateOrUpdate } from '../../shared/models/comment-create-or-update';
import { Comment } from '../../shared/models/comment';
import {ToasterService} from "angular5-toaster/dist";
import {BadRequestError} from "../../shared/errors/bad-request-error";


@Component({
  selector: 'app-comment-modal',
  templateUrl: './comment-modal.component.html',
  styleUrls: ['./comment-modal.component.css']
})
export class CommentModalComponent implements OnInit {
  formGroup: FormGroup;
  content: string;
  damageId: number;
  commentId: number;
  buildingId: number;
  commentPosted : EventEmitter<Comment> = new EventEmitter();

  constructor(public modalRef: BsModalRef, private fb: FormBuilder,
              private commentService: CommentService, private change : ChangeDetectorRef,
              private toasterService: ToasterService) {
    this.formGroup = this.fb.group({
      comment: [this.content, [
        Validators.required,
        Validators.minLength(5)
      ]]
    });
  }

  ngOnInit() {
    this.change.detectChanges();
  }

  get comment() {
    return this.formGroup.get('comment');
  }

  postComment() {

    if(!this.commentId) {
      this.commentService.save(new CommentCreateOrUpdate(this.comment.value), 'buildings',
        this.buildingId, 'damages', this.damageId, 'comments').subscribe((data) => {
        this.commentPosted.emit(data);
        this.modalRef.hide();
      }, error => {
        if(error instanceof BadRequestError)
        {
          this.toasterService.pop('error', 'Error', 'Selected comment doesn\'t belong to selected damage!');
          return;
        }
        this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
        throw error;
      });
    }
    else {
      this.commentService.update(this.commentId, new CommentCreateOrUpdate(this.comment.value),
        'damages', this.damageId, 'comments').subscribe(data => {
        this.commentPosted.emit(data);
        this.modalRef.hide();
      }, error => {
          if(error instanceof BadRequestError)
          {
            this.toasterService.pop('error', 'Error', 'Selected comment doesn\'t belong to selected damage!');
            return;
          }
          this.toasterService.pop('error', 'Error', 'Something unexpected happened! \n See information about error in console.');
          throw error;
      });
    }
  }
}
