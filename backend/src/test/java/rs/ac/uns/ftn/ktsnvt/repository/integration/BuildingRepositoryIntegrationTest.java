package rs.ac.uns.ftn.ktsnvt.repository.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.model.entity.Building;
import rs.ac.uns.ftn.ktsnvt.repository.BuildingRepository;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Katarina Cukurov on 28/11/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class BuildingRepositoryIntegrationTest {

    @Autowired
    private BuildingRepository buildingRepository;

    private Pageable page = new PageRequest(0, 10, Sort.Direction.ASC, "id");

    @Test
    @Transactional
    public void testFindByAddress_Id(){
       /*
        * Test proverava ispravnost rada metode findBAddressId repozitorijuma BuildingRepository
        * (pronalazenje zgrade na osnovu zadatog ID-a)
        * */

        /*
        podaci koji postoje u bazi za zgradu sa ID-em 101:
            id = 101,
            number_of_floors = 5,
            has_parking = true,
            address_id = 101
        */
        Building building = this.buildingRepository.findByAddressId(new Long("101"));

        assertNotNull(building);
        assertEquals(Long.valueOf(101), building.getAddress().getId());
        assertEquals(Long.valueOf(101), building.getId());
        assertEquals(5, building.getNumberOfFloors());
        assertTrue(building.isHasParking());
    }

    @Test
    @Transactional
    public void testFindByCity(){
       /*
        * Test proverava ispravnost rada metode findByCity repozitorijuma BuildingRepository
        * (pronalazenje zgrade na osnovu zadatog ID-a grada)
        * */

        List<Building> building = this.buildingRepository.findByCity(new Long("100"));

        assertEquals(5, building.size());
    }

    @Test
    @Transactional
    public void testFindByFirm(){
       /*
        * Test proverava ispravnost rada metode findByFirm repozitorijuma BuildingRepository
        * (pronalazenje zgrade na osnovu zadatog ID-a firme)
        * */

        Page<Building> building = this.buildingRepository.findAllByFirm(new Long("100"), this.page);

        assertEquals(2, building.getContent().size());
    }
}
