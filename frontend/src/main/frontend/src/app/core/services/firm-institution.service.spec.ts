import { TestBed, inject } from '@angular/core/testing';

import { FirmInstitutionService } from './firm-institution.service';

describe('FirmInstitutionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FirmInstitutionService]
    });
  });

  it('should be created', inject([FirmInstitutionService], (service: FirmInstitutionService) => {
    expect(service).toBeTruthy();
  }));
});
