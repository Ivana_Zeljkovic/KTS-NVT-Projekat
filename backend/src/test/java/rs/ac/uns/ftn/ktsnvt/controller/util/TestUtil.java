package rs.ac.uns.ftn.ktsnvt.controller.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.IOException;

/**
 * Created by Ivana Zeljkovic on 02/12/2017.
 */
public class TestUtil {
    public static String json(Object object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        return mapper.writeValueAsString(object);
    }

    public static MultiValueMap<String, String> getParams(String pageNumber, String pageSize,
                                                          String sortDirection, String sortProperty,
                                                          String secondSortProperty) {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("pageNumber", pageNumber);
        params.add("pageSize", pageSize);
        params.add("sortDirection", sortDirection);
        params.add("sortProperty", sortProperty);
        if(!secondSortProperty.isEmpty())
            params.add("sortPropertySecond", secondSortProperty);
        return params;
    }
}
