package rs.ac.uns.ftn.ktsnvt.web.dto;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by Ivana Zeljkovic on 10/12/2017.
 */
public class DamageUpdateDTO implements Serializable {

    @NotNull
    @NotEmpty
    private String description;
    @NotNull
    private boolean urgent;

    public DamageUpdateDTO() { }

    public DamageUpdateDTO(String description, boolean urgent) {
        this.description = description;
        this.urgent = urgent;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isUrgent() {
        return urgent;
    }

    public void setUrgent(boolean urgent) {
        this.urgent = urgent;
    }
}
