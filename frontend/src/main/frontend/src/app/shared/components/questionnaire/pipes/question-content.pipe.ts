import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
  name: 'questionContent'
})
export class QuestionContentPipe implements PipeTransform {
  transform(value: any, args?: any[]) {
    if(!value)
      return null;

    let str = value.toString().trim();
    str = str.toLowerCase();
    str = str[0].toUpperCase() + str.slice(1);
    if(str[str.length-1] !== '?')
      str += '?';
    return str;
  }
}
