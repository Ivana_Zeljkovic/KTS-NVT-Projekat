import { TestBed, inject } from '@angular/core/testing';

import { FirmService } from './firm.service';
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {Business} from "../../shared/models/business";

describe('FirmService', () => {

  let firmService: FirmService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule],
      providers: [FirmService]
    });

    firmService = TestBed.get(FirmService);
    httpMock = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', inject([FirmService], (service: FirmService) => {
    expect(service).toBeTruthy();
  }));

  it('should get by address', () => {
    const response = {
      'id' : 1,
      'name' : 'firm name',
      'description' : 'description',
      'phoneNumber' : '13245',
      'email' : 'firm@gmail.com',
      'businessType' : 'Police',
      'address' : {
        'street': 'Janka Veselinovica',
        'number': 18,
        'city': {
          'name': 'Novi Sad',
          'postalNumber': '21000'
        }
      },
      'isCompany' : false,
      'confirmed' : true
    };

    firmService.getByAddress('Janka Veselinovica', '18', 'Novi Sad', '21000')
      .subscribe((firm: Business) => {
        expect(firm).toEqual(response);
      });
    const httpRequest = httpMock.expectOne( request => {
      return '/api/businesses/firms/address?' +
      'street=Janka%20Veselinovica&' +
      'number=18&' +
      'city_name=Novi%20Sad' +
      'postal_number=21000'}
      );
    expect(httpRequest.request.method).toEqual('GET');
    expect(httpRequest.request.params.get('street')).toEqual('Janka Veselinovica');
    expect(httpRequest.request.params.get('number')).toEqual('18');
    expect(httpRequest.request.params.get('city_name')).toEqual('Novi Sad');
    expect(httpRequest.request.params.get('postal_number')).toEqual('21000');
    httpRequest.flush(response);
  });
});
