import { Component, ViewChild } from '@angular/core';
import { AgmMap } from "@agm/core";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { ToasterService } from "angular5-toaster/dist";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { BsModalRef, BsModalService } from "ngx-bootstrap";
// model
import { AddressCreate } from "../../shared/models/address-create";
import { CityCreate } from "../../shared/models/city-create";
import { BuildingUpdate } from "../../shared/models/building-update";
import { ApartmentUpdate } from "../../shared/models/apartment-update";
import { Building } from "../../shared/models/building";
// service
import { LocationService } from "../../core/services/location.service";
import { BuildingService } from "../../core/services/building.service";
// component
import { AddApartmentModalComponent } from "../../shared/components/add-apartment-modal/add-apartment-modal.component";
//error
import {AppError} from "../../shared/errors/app-error";
import {NotFoundError} from "../../shared/errors/not-found-error";
import {ForbiddenError} from "../../shared/errors/forbidden-error";
import {BadRequestError} from "../../shared/errors/bad-request-error";
import {PageAndSort} from "../../shared/models/page-and-sort";
import {ApartmentService} from "../../core/services/apartment.service";
import {ConfirmDeleteModalComponent} from "../../shared/components/confirm-delete-modal/confirm-delete-modal.component";
import {ComponentValidator} from "codelyzer/walkerFactory/walkerFn";
import {SpaceValidator} from "../../shared/validators/space.validator";


@Component({
  selector: 'app-building',
  templateUrl: './building.component.html',
  styleUrls: ['./building.component.css']
})
export class BuildingComponent{

  form: FormGroup;
  building: Building;
  buildingUpdate: BuildingUpdate;
  id: number;
  longitude: number;
  latitude: number;
  zoom: number;
  @ViewChild(AgmMap) map: AgmMap;
  floorsList: number[];
  bsModalRef: BsModalRef;
  filterList: string[];
  selectedFilter: string;
  allApartments: boolean;
  min: number;
  max: number;

  // Elements for bootstrap paging
  currentPage: number;
  totalItems: number;
  maxSize: number;
  itemsPerPage: number;

  // Elements for getting data from server
  pageSize: string;
  sortDirection: string;
  sortProperties: string[];

  constructor(private buildingService: BuildingService, private route: ActivatedRoute, private fb: FormBuilder,
              private router: Router,  private locationService: LocationService, private toasterService: ToasterService,
              private modalService : BsModalService, private apartmentService: ApartmentService) {
    this.zoom = 14;
    // default coordinates if there are no coordinates for given address
    this.latitude = 45.2461;
    this.longitude = 19.8517;
    this.route.params.subscribe((param: Params) => {
      this.id = param['id'];
      this.getBuilding();
    });
    this.floorsList = [];
    for(let i = 1; i < 200; i++)
      this.floorsList.push(i);
    this.filterList = ['None', 'Empty apartments', 'Empty ap. by size'];
    this.selectedFilter = 'None';
    this.allApartments = true;

    // Sett up for bootstrap paging
    this.currentPage = 1;
    this.maxSize = 3;
    this.itemsPerPage = 10;

    this.pageSize = '10';
    this.sortDirection = 'ASC';
    this.sortProperties = ['number'];
  }

  getBuilding(){
    this.buildingService.get(this.id).subscribe(data => {
      let buildingData = data as Building;
      this.building = buildingData;

      this.form = this.fb.group({
        street: [this.building.address.street, [
          Validators.required,
          Validators.minLength(5)
        ]],
        number: [this.building.address.number, [
          Validators.required,
        ]],
        city: [this.building.address.city.name, [
          Validators.required,
        ]],
        postalCode: [this.building.address.city.postalNumber,[
          Validators.required,
          Validators.pattern('[0-9]{5}')
        ]]
      });
      this.building.hasParking = buildingData.hasParking;
      if(buildingData.address != null){
        this.locationService.getCoordinates(`${buildingData.address.street} ${buildingData.address.city.name}`)
          .subscribe(response => {
            if (response.status == 'OK') {
              this.latitude = response.results[0].geometry.location.lat;
              this.longitude = response.results[0].geometry.location.lng;
              this.map.triggerResize();
            } else if (response.status == 'ZERO_RESULTS') {
              this.toasterService.pop('error', 'Error', 'Address couldn\'t be found!');
            } else {
              this.toasterService.pop('error','Error','Error in detecting coordinates of given address!');
            }
            this.getAllApartments();
          }, (error: AppError) => {
            this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
            throw error;
          });
      }
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Building not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Error', 'Building does not exist!');
      else {
        this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
        throw error;
      }
    });
  }

  get street() {
    return this.form.get('street');
  }

  get number() {
    return this.form.get('number');
  }

  get city() {
    return this.form.get('city');
  }

  get postalCode() {
    return this.form.get('postalCode');
  }

  setFloors(floor: number) {
    this.building.numberOfFloors = floor;
  }

  removeApartment(apartment) {
    this.bsModalRef = this.modalService.show(
      ConfirmDeleteModalComponent,
      Object.assign({},{ class: 'modal-sm' })
    );

    this.bsModalRef.content.confirmed.subscribe((value) => {
      if(value) {
        this.apartmentService.remove(apartment.id, 'buildings', this.building.id, 'apartments')
          .subscribe(data => {
              this.toasterService.pop('success', 'Congratulation', 'Successfully deleted apartment!');
              this.getAllApartments();
            },
            (error: AppError) => {
              if(error instanceof NotFoundError)
                this.toasterService.pop('error', 'Error', 'Apartment not found!');
              else if(error instanceof ForbiddenError)
                this.toasterService.pop('error', 'Error', 'You are not allowed to delete this aoartment!');
              else if(error instanceof BadRequestError)
                this.toasterService.pop('error', 'Error', 'This apartments has tenants and can\'t be deleted!');
              else {
                this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
                throw error;
              }
            });
      }
    });
  }

  displayAddApartment() {
    this.bsModalRef = this.modalService.show(AddApartmentModalComponent);
    this.bsModalRef.content.apartmentList = this.building.apartments;
    this.bsModalRef.content.bsModalRef = this.bsModalRef;
    this.bsModalRef.content.building = this.building;
    this.bsModalRef.content.apartmentCreated.subscribe( data => {
      this.getAllApartments();
    })

  }

  updateBuilding(){

    let address = new AddressCreate(this.street.value, this.number.value, new CityCreate(this.city.value, this.postalCode.value));
    let apartmentsUpdate: Array<ApartmentUpdate> = [];
    this.buildingUpdate = new BuildingUpdate(this.building.hasParking, address, apartmentsUpdate, this.building.numberOfFloors);

    this.buildingService.update(this.building.id, this.buildingUpdate).subscribe(data => {
      this.toasterService.pop('success', 'Congratulation', 'Successfully updated building!');
      this.getBuilding();
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Building not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Error', 'Data is not good!');
      else {
        this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
        throw error;
      }
    });
  }

  hasParking(parking: boolean){
    this.building.hasParking = parking;
  }

  setFilter(filter: string){
    this.selectedFilter = filter;
  }

  callFilter(){
    this.currentPage = 1;
    this.sortProperties = ['number'];
    if(this.selectedFilter == 'Empty apartments')
      this.getEmptyApartments();
    else if(this.selectedFilter == 'Empty ap. by size')
      this.getBySize();
    else{
      this.getAllApartments();
    }

  }

  getEmptyApartments(){
    this.allApartments = false;
    let page = new PageAndSort((this.currentPage - 1).toString(), this.pageSize, this.sortDirection, this.sortProperties);
    this.apartmentService.getFreeInBuilding(page, this.building.address.street, this.building.address.number.toString(),
      this.building.address.city.name, this.building.address.city.postalNumber).subscribe(data => {
      this.allApartments = false;
      this.building.apartments = data;
      if(data.length > 0)
        this.totalItems = data[0].numberOfElements;
      else
        this.totalItems = 0;
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Apartments not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Error', 'Bad request!');
      else {
        this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
        throw error;
      }
    });
  }

  getBySize(){
    let page = new PageAndSort((this.currentPage - 1).toString(), this.pageSize, this.sortDirection, this.sortProperties);
    this.apartmentService.getFreeInRange(page, this.min.toString(), this.max.toString(), this.building.id.toString())
      .subscribe(data => {
      this.allApartments = false;
      this.building.apartments = data;
      if(data.length > 0)
        this.totalItems = data[0].numberOfElements;
      else
        this.totalItems = 0;
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Apartments not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Error', 'Bad request!');
      else {
        this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
        throw error;
      }
    });


  }

  getAllApartments(){
    this.sortProperties = ['number', 'floor'];
    let page = new PageAndSort((this.currentPage - 1).toString(), this.pageSize, this.sortDirection, this.sortProperties);
    this.apartmentService.getPage(page, 'buildings', this.building.id, 'apartments').subscribe(data => {
      this.allApartments = true;
      this.building.apartments = data;
      if(data.length > 0)
        this.totalItems = data[0].numberOfElements;
      else
        this.totalItems = 0;
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Apartments not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Error', 'Bad request!');
       else {
        this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
        throw error;
      }
    });
  }

  pageChanged(event: any) {
    this.currentPage = event.page;
    if(this.selectedFilter == 'Empty apartments')
      this.getEmptyApartments();
    else if(this.selectedFilter == 'Empty ap. by size')
      this.getBySize();
    else
      this.getAllApartments();
  }
}
