import {by, element, ElementFinder, protractor} from "protractor";

export class BuildingPage {
  // get methods
  getHeading(): ElementFinder {
    return element(by.css('.panel-heading-location'))
  }

  getMap(): ElementFinder {
    return element(by.tagName('agm-map'));
  }

  getPanelHeading(): ElementFinder {
    return element(by.css('.panel-heading.panel-heading-personal'));
  }

  getNumberOfFloorsLabel(): ElementFinder{
    return element(by.id('numberOfFloors'));
  }

  getDropdownToogle(): ElementFinder {
    return element(by.id('initialFloor'));
  }

  getRadioParking(): ElementFinder {
    return element(by.id('radioParking'));
  }

  getSelectedFilter(): ElementFinder {
    return element(by.id('selectedFilter'));
  }

  getDropdownMenuFilter(): ElementFinder {
    return element(by.id('dropDownMenuFilter'));
  }

  getFilterButton(): ElementFinder {
    return element(by.buttonText("Filter"));
  }

  getMinMaxDiv(): ElementFinder {
    return element(by.id('minMaxDiv'));
  }

  getMin(): ElementFinder {
    return element(by.id('min'));
  }

  getMax(): ElementFinder {
    return element(by.id('max'));
  }

  getApartTable(): ElementFinder {
    return element(by.id('apartTable'));
  }

  getPagination(): ElementFinder {
    return element(by.tagName('pagination'));
  }

  getNoElements(): ElementFinder {
    return element(by.tagName('h3'));
  }

  getAddApartButton(): ElementFinder {
    return element(by.css('.btn.btn-primary.account'));
  }

  getStreet(): ElementFinder {
    return element(by.id('street'));
  }

  getNumber(): ElementFinder {
    return element(by.id('number'));
  }

  getCity(): ElementFinder {
    return element(by.id('city'));
  }

  getPostalCode(): ElementFinder {
    return element(by.id('postalCode'));
  }

  getUpdateButton(): ElementFinder {
    return element(by.css('.btn.save'))
  }

  // set methods
  setStreet(text: string) {
    this.clearField(this.getStreet());
    this.getStreet().sendKeys(text);
  }

  setNumber(text: string) {
    this.clearField(this.getNumber());
    this.getNumber().sendKeys(text);
  }

  setCity(text: string) {
    this.clearField(this.getCity());
    this.getCity().sendKeys(text);
  }

  setPostalCode(text: string) {
    this.clearField(this.getPostalCode());
    this.getPostalCode().sendKeys(text);
  }

  setMin(text: number) {
    this.clearField(this.getMin());
    this.getMin().sendKeys(text);
  }

  setMax(text: number) {
    this.clearField(this.getMax());
    this.getMax().sendKeys(text);
  }

  // get errors methods
  getMinGreaterError(): ElementFinder {
    return element(by.id('minGreaterError'));
  }

  getStreetRequiredError(): ElementFinder {
    return element(by.id('streetRequiredError'));
  }

  getStreetMinLengthError(): ElementFinder {
    return element(by.id('streetMinLengthError'));
  }

  getNumberRequiredError(): ElementFinder {
    return element(by.id('numberRequiredError'));
  }

  getNumberSpaceError(): ElementFinder {
    return element(by.id('numberSpaceError'));
  }

  getCityRequiredError(): ElementFinder {
    return element(by.id('cityRequiredError'));
  }

  getPostalCodeRequiredError(): ElementFinder {
    return element(by.id('postalCodeRequiredError'));
  }

  getPostalCodeLengthError(): ElementFinder {
    return element(by.id('postalCodeLengthError'));
  }

  clearField(field) {
    field.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, "a"));
    field.sendKeys(protractor.Key.BACK_SPACE);
    field.clear();
  }

}
