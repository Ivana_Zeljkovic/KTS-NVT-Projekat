import { browser } from "protractor";
// page
import { TenantRegistrationPage } from "../../page-objects/tenant-registration.po";
import { LoginPage } from "../../page-objects/login.po";


describe('Tenant registration', () => {
  let loginPage: LoginPage;
  let tenantRegistrationPage: TenantRegistrationPage;

  beforeEach(() => {
    loginPage = new LoginPage();
    tenantRegistrationPage = new TenantRegistrationPage();

    browser.get("http://localhost:49152/#/registration/tenant");
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/registration/tenant');

    // expected view of registration page
    expect(tenantRegistrationPage.getPageTitle().isDisplayed()).toBe(true);
    expect(tenantRegistrationPage.getFirstNameInput().isDisplayed()).toBe(true);
    expect(tenantRegistrationPage.getLastNameInput().isDisplayed()).toBe(true);
    expect(tenantRegistrationPage.getUsernameInput().isDisplayed()).toBe(true);
    expect(tenantRegistrationPage.getPasswordInput().isDisplayed()).toBe(true);
    expect(tenantRegistrationPage.getConfirmPasswordInput().isDisplayed()).toBe(true);
    expect(tenantRegistrationPage.getEmailInput().isDisplayed()).toBe(true);
    expect(tenantRegistrationPage.getPhoneNumberInput().isDisplayed()).toBe(true);
    expect(tenantRegistrationPage.getBirthDateInput().isDisplayed()).toBe(true);
    expect(tenantRegistrationPage.getSingUpButton().isDisplayed()).toBe(true);
    expect(tenantRegistrationPage.getSingUpButton().isEnabled()).toBe(false);
    expect(tenantRegistrationPage.getLoginLink().isDisplayed()).toBe(true);
  });



  it('should successfully register new tenant and redirect to login page', () => {
    // enter registration data
    tenantRegistrationPage.setFirstName('Danijela');
    tenantRegistrationPage.setLastName('Zeljkovic');
    tenantRegistrationPage.setUsername(Math.random().toString(36).substring(7));
    tenantRegistrationPage.setPassword('dacaz');
    tenantRegistrationPage.setConfirmPassword('dacaz');
    tenantRegistrationPage.setEmail('daca@gmail.com');
    tenantRegistrationPage.setPhoneNumber('1234567');

    // expected view of registration page after input data
    expect(tenantRegistrationPage.getSingUpButton().isEnabled()).toBe(true);

    tenantRegistrationPage.getSingUpButton().click();

    browser.wait(function() {
      // expect that browser redirect user to login page
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/login?returnUrl=%2Fhome';
      });
    });
  });



  it('should show error for required first name when user put focus on first name input and leave it', () => {
    // enter registration data
    tenantRegistrationPage.setFirstName('');
    tenantRegistrationPage.setLastName('Zeljkovic');

    // expected view of registration page after input data
    expect(tenantRegistrationPage.getSingUpButton().isEnabled()).toBe(false);
    expect(tenantRegistrationPage.getFirstNameRequiredError().isDisplayed()).toBe(true);
  });



  it('should show error that first name must have length 5 or more characters when user put word ' +
    'with less then 5 characters in first name input and leave it', () => {
    // enter registration data
    tenantRegistrationPage.setFirstName('D');
    tenantRegistrationPage.setLastName('Zeljkovic');

    // expected view of registration page after input data
    expect(tenantRegistrationPage.getSingUpButton().isEnabled()).toBe(false);
    expect(tenantRegistrationPage.getFirstNameMinimumLengthError().isDisplayed()).toBe(true);
  });



  it('should show error that username can not contain space when user put two words ' +
    'in username input and leave it', () => {
    // enter registration data
    tenantRegistrationPage.setFirstName('Danijela');
    tenantRegistrationPage.setUsername('daca z');
    tenantRegistrationPage.setLastName('Zeljkovic');

    // expected view of registration page after input data
    expect(tenantRegistrationPage.getSingUpButton().isEnabled()).toBe(false);
    expect(tenantRegistrationPage.getUsernameContainSpaceError().isDisplayed()).toBe(true);
  });



  it('should show error that username is already taken when user put some already used username ' +
    'in username input and leave it', () => {
    // enter registration data
    tenantRegistrationPage.setFirstName('Danijela');
    tenantRegistrationPage.setLastName('Zeljkovic');
    tenantRegistrationPage.setUsername('ivana');
    tenantRegistrationPage.setPassword('dacaz');

    // expected view of registration page after input data
    expect(tenantRegistrationPage.getSingUpButton().isEnabled()).toBe(false);
    expect(tenantRegistrationPage.getUsernameTakenError().isDisplayed()).toBe(true);
  });



  it('should show message that username is well created (not used already) when user put some new username ' +
    'in username input and leave it', () => {
    // enter registration data
    tenantRegistrationPage.setFirstName('Danijela');
    tenantRegistrationPage.setLastName('Zeljkovic');
    tenantRegistrationPage.setUsername(Math.random().toString(36).substring(5));
    tenantRegistrationPage.setPassword('dacaz');
    tenantRegistrationPage.setConfirmPassword('dacaz');
    tenantRegistrationPage.setEmail('daca@gmail.com');
    tenantRegistrationPage.setPhoneNumber('1234567');

    // expected view of registration page after input data
    expect(tenantRegistrationPage.getSingUpButton().isEnabled()).toBe(true);
    expect(tenantRegistrationPage.getUsernameAvailable().isDisplayed()).toBe(true);
  });



  it('should show error passwords are not equals when user put different values ' +
    'in password and confirm password input and leave it', () => {
    // enter registration data
    tenantRegistrationPage.setFirstName('Danijela');
    tenantRegistrationPage.setLastName('Zeljkovic');
    tenantRegistrationPage.setUsername(Math.random().toString(36).substring(7));
    tenantRegistrationPage.setPassword('dacaz');
    tenantRegistrationPage.setConfirmPassword('zdaca');
    tenantRegistrationPage.setEmail('daca@gmail.com');
    tenantRegistrationPage.setPhoneNumber('1234567');

    // expected view of registration page after input data
    expect(tenantRegistrationPage.getSingUpButton().isEnabled()).toBe(false);
    expect(tenantRegistrationPage.getPasswordDontMatchError().isDisplayed()).toBe(true);
  });



  it('should show error about invalid email when user put some invalid value ' +
    'in email input and leave it', () => {
    // enter registration data
    tenantRegistrationPage.setFirstName('Danijela');
    tenantRegistrationPage.setLastName('Zeljkovic');
    tenantRegistrationPage.setUsername(Math.random().toString(36).substring(7));
    tenantRegistrationPage.setPassword('dacaz');
    tenantRegistrationPage.setConfirmPassword('dacaz');
    tenantRegistrationPage.setEmail('d');
    tenantRegistrationPage.setPhoneNumber('1234567');

    // expected view of registration page after input data
    expect(tenantRegistrationPage.getSingUpButton().isEnabled()).toBe(false);
    expect(tenantRegistrationPage.getEmailValidError().isDisplayed()).toBe(true);
  });



  it('should show error about wrong phone number pattern when user put some invalid value ' +
    'in phone number input and leave it', () => {
    // enter registration data
    tenantRegistrationPage.setFirstName('Danijela');
    tenantRegistrationPage.setLastName('Zeljkovic');
    tenantRegistrationPage.setUsername('dacaz');
    tenantRegistrationPage.setPassword('dacaz');
    tenantRegistrationPage.setConfirmPassword('dacaz');
    tenantRegistrationPage.setPhoneNumber('ddjbjh');
    tenantRegistrationPage.setEmail('daca@gmail.com');

    // expected view of registration page after input data
    expect(tenantRegistrationPage.getSingUpButton().isEnabled()).toBe(false);
    expect(tenantRegistrationPage.getPhoneNumberPatternError().isDisplayed()).toBe(true);
  });
});
