export class AdministratorUpdate {
  constructor(private firstName: string,
              private lastName: string,
              private currentPassword?: string,
              private newPassword?: string) { }
}
