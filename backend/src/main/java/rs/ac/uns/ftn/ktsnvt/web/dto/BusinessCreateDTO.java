package rs.ac.uns.ftn.ktsnvt.web.dto;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Katarina Cukurov on 08/11/2017.
 */
public class BusinessCreateDTO implements Serializable {

    @NotNull
    @NotEmpty
    private String name;
    @NotNull
    @NotEmpty
    private String description;
    @NotNull
    @NotEmpty
    private String phoneNumber;
    @NotNull
    @NotEmpty
    private String email;
    @NotNull
    private String businessType;
    @NotNull
    @Valid
    private AddressDTO address;
    @NotNull
    @Size(min = 1)
    private List<LoginDTO> loginAccounts;

    public BusinessCreateDTO(){}

    public BusinessCreateDTO(String name, String description, String phoneNumber, String email,
                             String businessType, AddressDTO address, List<LoginDTO> loginAccounts) {
        this.name = name;
        this.description = description;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.businessType = businessType;
        this.address = address;
        this.loginAccounts = loginAccounts;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public AddressDTO getAddress() {
        return address;
    }

    public void setAddress(AddressDTO address) {
        this.address = address;
    }

    public List<LoginDTO> getLoginAccounts() {
        return loginAccounts;
    }

    public void setLoginAccounts(List<LoginDTO> loginAccounts) {
        this.loginAccounts = loginAccounts;
    }
}
