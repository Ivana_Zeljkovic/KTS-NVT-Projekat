package rs.ac.uns.ftn.ktsnvt.repository.unit;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.model.entity.*;
import rs.ac.uns.ftn.ktsnvt.repository.VoteRepository;

import java.util.ArrayList;
import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * Created by Nikola Garabandic on 30/11/2017.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class VoteRepositoryUnitTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private VoteRepository voteRepository;

    private Long tenant_id, questionnaire_id;


    @Before
    public void setUp(){
        Account account = new Account("nikola", "nikola");
        Tenant tenant = new Tenant("Nikola", "Garabandic", new Date(), "kantagara@gmail.com",
                "06314153131", true, account);

        Questionnaire questionnaire = new Questionnaire();

        questionnaire.setDateExpire(new Date());

        Vote vote = new Vote();
        Vote vote1 = new Vote();

        vote.setQuestionnaire(questionnaire);
        vote.setTenant(tenant);

        vote1.setQuestionnaire(questionnaire);
        vote1.setTenant(tenant);

        questionnaire.setVotes(new ArrayList<Vote>(){{add(vote); add(vote1);}});

        entityManager.persist(account);
        this.tenant_id = entityManager.persistAndGetId(tenant, Long.class);
        this.questionnaire_id = entityManager.persistAndGetId(questionnaire, Long.class);
        entityManager.persist(vote);
    }

    @Test
    public void testCountPersonalVotes(){
        /*
        * Test proverava ispravnost rada metode countPersonalVotes repozitorijuma VoteRepository
        * (dobavljanje broja postojecih glasova stanara sa zadatim ID-em u anketi sa zadatim ID-em)
        * */

        // stanar sa ID-em = tenant_id je 2 puta glasao na anketu sa ID-em = questionnaire_id
        int number = this.voteRepository.countPersonalVotes(this.tenant_id, this.questionnaire_id);

        assertEquals(2, number);
    }
}
