import {by, element, ElementFinder} from "protractor";

export class LoginPage {
  // get methods
  getLoginPageElement(): ElementFinder {
    return element(by.tagName('app-login'));
  }

  getPageTitle(): ElementFinder {
    return element(by.css('.panel-title'));
  }

  getUsernameInput(): ElementFinder {
    return element(by.id('username'));
  }

  getPasswordInput(): ElementFinder {
    return element(by.id('password'));
  }

  getSignInButton(): ElementFinder {
    return element(by.buttonText('Sign in'));
  }

  getSignUpAsTenantLink(): ElementFinder {
    return element(by.css('div > a:first-of-type'));
  }

  getSignUpAsFirmLink(): ElementFinder {
    return element(by.css('div > a:nth-child(2)'));
  }

  getSignUpAsInstitutionLink(): ElementFinder {
    return element(by.css('div > a:last-of-type'));
  }

  // set methods
  setUsername(text: string) {
    this.getUsernameInput().sendKeys(text);
  }

  setPassword(text: string) {
    this.getPasswordInput().sendKeys(text);
  }

  // get errors method
  getUsernameRequiredError(): ElementFinder {
    return element(by.id('usernameRequiredError'));
  }

  getUsernameContainSpaceError(): ElementFinder {
    return element(by.id('usernameContainSpaceError'));
  }

  getPasswordRequiredError(): ElementFinder {
    return element(by.id('passwordRequiredError'));
  }
}
