export class TenantUpdate {
  constructor(private firstName: string, private lastName: string,
              private email: string, private phoneNumber: string,
              private birthDate: Date, private currentPassword?: string,
              private newPassword?: string) { }
}
