import {browser, by, element} from "protractor";
// page
import {LoginPage} from "../../page-objects/login.po";
import {AddApartmentModalPage} from "../../shared/add-apartment-modal/page-objects/add-apartment-modal.po";
import {BuildingPage} from "./page-objects/building.po";
import {ConfirmDeletePage} from "../../page-objects/confirm-delete.po";

describe('Building page', () => {
  let loginPage: LoginPage;
  let buildingPage: BuildingPage;
  let addApartmentModalPage: AddApartmentModalPage;
  let confirmDeletePage: ConfirmDeletePage;
  let newApartment : any;

  beforeAll(() => {
    newApartment = {
      'number' : 1,
      'surface' : 50,
      'floor' : 1
    }
  });

  beforeEach(() => {
    loginPage = new LoginPage();
    buildingPage = new BuildingPage();
    browser.get("/");

    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');
    loginPage.setUsername('ivana');
    loginPage.setPassword('ivana');
    loginPage.getSignInButton().click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/home');
    browser.get('http://localhost:49152/#/buildings/104');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/buildings/104');
    browser.waitForAngularEnabled(false);
  });

  afterEach(() => {
    browser.executeScript('window.localStorage.clear();');
    browser.waitForAngularEnabled(true);
  });

  it('should successfully load page and all its components', () => {
    expect(buildingPage.getHeading().isDisplayed()).toBe(true);
    expect(buildingPage.getMap().isDisplayed()).toBe(true);
    expect(buildingPage.getPanelHeading().isDisplayed()).toBe(true);
    expect(buildingPage.getNumberOfFloorsLabel().isDisplayed()).toBe(true);
    expect(buildingPage.getDropdownToogle().isDisplayed()).toBe(true);
    expect(buildingPage.getRadioParking().isDisplayed()).toBe(true);
    expect(buildingPage.getSelectedFilter().isDisplayed()).toBe(true);
    expect(buildingPage.getDropdownMenuFilter().isPresent()).toBe(false);
    expect(buildingPage.getMinMaxDiv().isPresent()).toBe(false);
    expect(buildingPage.getApartTable().isPresent()).toBe(false);
    expect(buildingPage.getPagination().isPresent()).toBe(false);
    expect(buildingPage.getNoElements().isDisplayed()).toBe(true);
    expect(buildingPage.getAddApartButton().isDisplayed()).toBe(true);
    expect(buildingPage.getStreet().isDisplayed()).toBe(true);
    expect(buildingPage.getNumber().isDisplayed()).toBe(true);
    expect(buildingPage.getCity().isDisplayed()).toBe(true);
    expect(buildingPage.getPostalCode().isDisplayed()).toBe(true);
    expect(buildingPage.getUpdateButton().isDisplayed()).toBe(true);

    expect(buildingPage.getUpdateButton().isEnabled()).toBe(true);
    expect(buildingPage.getAddApartButton().isEnabled()).toBe(true);
  });

  it('should successfully update building information', () => {
    buildingPage.setNumber('28');
    buildingPage.setPostalCode('21000');
    buildingPage.setStreet('Puskinova');
    buildingPage.setCity('Novi Sad');

    buildingPage.getUpdateButton().click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/buildings/104');
  });

  it('should show error for required street when user put focus and leaves it blank', () =>{

    buildingPage.setStreet("");
    buildingPage.getNumber().click();
    expect(buildingPage.getStreetRequiredError().isDisplayed()).toBe(true);
    expect(buildingPage.getUpdateButton().isEnabled()).toBe(false);
  });

  it('should show error for required street minimum length when street name is too short', () =>{

    buildingPage.setStreet("a");
    buildingPage.getNumber().click();
    expect(buildingPage.getStreetMinLengthError().isDisplayed()).toBe(true);
    expect(buildingPage.getUpdateButton().isEnabled()).toBe(false);
  });

  it('should show error for required number when user put focus and leaves it blank', () =>{

    buildingPage.setNumber("");
    buildingPage.getStreet().click();
    expect(buildingPage.getNumberRequiredError().isDisplayed()).toBe(true);
    expect(buildingPage.getUpdateButton().isEnabled()).toBe(false);
  });

  it('should show error for required city when user put focus and leaves it blank', () =>{

    buildingPage.setCity("");
    buildingPage.getStreet().click();
    expect(buildingPage.getCityRequiredError().isDisplayed()).toBe(true);
    expect(buildingPage.getUpdateButton().isEnabled()).toBe(false);
  });

  it('should show error for required postal code when user put focus and leaves it blank', () =>{

    buildingPage.setPostalCode("");
    buildingPage.getStreet().click();
    expect(buildingPage.getPostalCodeRequiredError().isDisplayed()).toBe(true);
    expect(buildingPage.getUpdateButton().isEnabled()).toBe(false);
  });

  it('should show error for length of postal code when user put shorter or longer postal code', () =>{

    buildingPage.setPostalCode("123");
    buildingPage.getStreet().click();
    expect(buildingPage.getPostalCodeLengthError().isDisplayed()).toBe(true);
    expect(buildingPage.getUpdateButton().isEnabled()).toBe(false);
  });

  it('should add new apartment to building', () => {

    buildingPage.getAddApartButton().click().then(function () {
      addApartmentModalPage = new AddApartmentModalPage();
      addApartmentModalPage.setNumber(newApartment.number);
      addApartmentModalPage.setFloor(newApartment.floor);
      addApartmentModalPage.setSurface(newApartment.surface);
      addApartmentModalPage.getConfirmButton().click();
      addApartmentModalPage.getCloseButton().click().then(function () {
        browser.wait(function() {
          return buildingPage.getApartTable().isDisplayed();
        }).then(function(){
          expect(buildingPage.getApartTable().isDisplayed()).toBe(true);
        });
      });
    });
  });

  it('should filter by empty apartments', () => {
    buildingPage.getSelectedFilter().click();
    element(by.css('.dropdown-menu li:nth-child(2)')).click();
    expect(buildingPage.getSelectedFilter().getText()).toBe("Empty apartments");
    buildingPage.getFilterButton().click();
    expect(buildingPage.getApartTable().isDisplayed()).toBe(true);
  });

  it('should show error when filter is by size and minimum is greater then maximum', () => {
    buildingPage.getSelectedFilter().click();
    element(by.css('.dropdown-menu li:nth-child(3)')).click();
    expect(buildingPage.getSelectedFilter().getText()).toBe("Empty ap. by size");
    expect(buildingPage.getMinMaxDiv().isDisplayed()).toBe(true);
    expect(buildingPage.getMin().isDisplayed()).toBe(true);
    expect(buildingPage.getMax().isDisplayed()).toBe(true);

    buildingPage.setMin(50);
    buildingPage.setMax(10);

    expect(buildingPage.getMinGreaterError().isDisplayed()).toBe(true);
  });

  it('should show no elements when there are no apartments in range', () => {
    buildingPage.getSelectedFilter().click();
    element(by.css('.dropdown-menu li:nth-child(3)')).click();
    expect(buildingPage.getSelectedFilter().getText()).toBe("Empty ap. by size");
    expect(buildingPage.getMinMaxDiv().isDisplayed()).toBe(true);
    expect(buildingPage.getMin().isDisplayed()).toBe(true);
    expect(buildingPage.getMax().isDisplayed()).toBe(true);

    buildingPage.setMin(5);
    buildingPage.setMax(10);
    buildingPage.getFilterButton().click();
    expect(buildingPage.getApartTable().isPresent()).toBe(false);

  });

  it('should delete apartment when clicked on button', () => {
    browser.waitForAngularEnabled(true);
    let deleteButton = element(by.css('.btn.btn-xs.danger'));
    deleteButton.click();
    confirmDeletePage = new ConfirmDeletePage();
    browser.wait(function() {

      return confirmDeletePage.getConfirmDeleteElement().isDisplayed();
    }).then(function() {

      expect(confirmDeletePage.getConfirmButton().isDisplayed()).toBe(true);
      expect(confirmDeletePage.getConfirmButton().isEnabled()).toBe(true);
      expect(confirmDeletePage.getDeclineButton().isDisplayed()).toBe(true);
      expect(confirmDeletePage.getDeclineButton().isEnabled()).toBe(true);

      confirmDeletePage.getConfirmButton().click();
      expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/buildings/104');
      expect(buildingPage.getNoElements().isDisplayed()).toBe(true);
    });
  });
});
