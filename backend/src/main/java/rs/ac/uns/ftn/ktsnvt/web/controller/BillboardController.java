package rs.ac.uns.ftn.ktsnvt.web.controller;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.ForbiddenException;
import rs.ac.uns.ftn.ktsnvt.model.entity.*;
import rs.ac.uns.ftn.ktsnvt.service.*;
import rs.ac.uns.ftn.ktsnvt.web.controller.util.MessageConstants;
import rs.ac.uns.ftn.ktsnvt.web.dto.*;

import static rs.ac.uns.ftn.ktsnvt.web.util.ConverterDTOToModel.convertNotificationCreateUpdateDTOToNotification;
import static rs.ac.uns.ftn.ktsnvt.web.util.ConverterDTOToModel.convertQuestionCreateDTOToQuestion;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Created by Ivana Zeljkovic on 13/11/2017.
 */
@RestController
@RequestMapping(value = "/api")
@Api(value="billboards", description="Operations pertaining to building's billboard in application.")
public class BillboardController {

    @Autowired
    private AccountService accountService;

    @Autowired
    private QuestionnaireService questionnaireService;

    @Autowired
    private QuestionService questionService;

    @Autowired
    private BuildingService buildingService;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private MeetingItemService meetingItemService;

    @Autowired
    private TenantService tenantService;

    @Autowired
    private DamageService damageService;

    @Autowired
    private MeetingService meetingService;



    @RequestMapping(
            value = "/buildings/{buildingId}/billboard/notifications",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a list of all notification in building in page form.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of notification", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to view these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity getNotifications(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable Long buildingId,
            @ApiParam(value = "The page number for getting notifications.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting notifications.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting notifications.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "The sort property for getting notifications.", required = true) @RequestParam String sortProperty) {

        Building building = this.buildingService.findOne(buildingId);

        this.tenantService.checkPermissionForCurrentTenantInBuilding(building.getId());

        return getAllBillboardItems(building, true, Integer.valueOf(pageNumber),
                Integer.valueOf(pageSize), sortDirection, sortProperty, null, null);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/billboard/notifications_from_to",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a list of all notification in building that are created in given period, in page form.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of notification", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to view these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity searchNotifications(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "From date") @RequestParam("from") String from,
            @ApiParam(value = "To date") @RequestParam("to") String to,
            @ApiParam(value = "The page number for getting notifications.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting notifications.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting notifications.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "The sort property for getting notifications.", required = true) @RequestParam String sortProperty) {

        Building building = this.buildingService.findOne(buildingId);
        this.tenantService.checkPermissionForCurrentTenantInBuilding(building.getId());

        SimpleDateFormat sdf = new SimpleDateFormat(MessageConstants.DATE_FORMAT);
        Date fromDate = null;
        Date toDate = null;
        try {
            fromDate = sdf.parse(from);
            toDate = sdf.parse(to);
        } catch (Exception e) {
            throw new BadRequestException(MessageConstants.INVALID_FORMAT_OF_DATE_PARAMETER);
        }

        return getAllBillboardItems(building, true, Integer.valueOf(pageNumber),
                Integer.valueOf(pageSize), sortDirection, sortProperty, fromDate, toDate);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/billboard/notifications/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a single notification.",
            notes = "You have to provide a valid IDs for building and notification.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved public information", response = NotificationDTO.class),
            @ApiResponse(code = 400, message = "Invalid object IDs sent in URL"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity getNotification(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The ID of the existing notification resource.", required = true) @PathVariable("id") Long id) {

        Building building = this.buildingService.findOne(buildingId);
        this.tenantService.checkPermissionForCurrentTenantInBuilding(building.getId());

        Notification notification = this.notificationService.findOne(id);
        // provera da li obavestenje sa zadatim ID-em pripada zadatoj zgradi
        this.notificationService.findOneByIdAndBillboard(notification.getId(), building.getBillboard().getId());

        return new ResponseEntity<>(new NotificationDTO(notification), HttpStatus.OK);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/billboard/notifications/{id}",
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Update a notification resource.",
            notes = "You have to provide valid IDs for building and notification in the URL. " +
                    "Method returns the notification being updated.",
            httpMethod = "PUT",
            consumes = "application/json",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated notification", response = MeetingItemDTO.class),
            @ApiResponse(code = 400, message = "Inappropriate notification object sent in request body"),
            @ApiResponse(code = 401, message = "You are not authorized to update the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity updateNotification(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The ID of the existing notification resource.", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "The notification object.", required = true) @Valid @RequestBody NotificationCreateUpdateDTO notificationUpdateDTO,
            @ApiParam(value = "The object that contains all errors from validation of DTO object") BindingResult errors) {

        Building building = this.buildingService.findOne(buildingId);
        this.tenantService.checkPermissionForCurrentTenantInBuilding(building.getId());

        Notification notification = this.notificationService.findOne(id);
        // provera da li obavestenje sa zadatim ID-em pripada zadatoj zgradi
        this.notificationService.findOneByIdAndBillboard(notification.getId(), building.getBillboard().getId());

        Tenant currentTenant = this.accountService.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).getTenant();
        // trenutno ulogovani korisnik nije kreator obavestenja koje se azurira
        if(!currentTenant.getId().equals(notification.getCreator().getId()))
            throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);

        notification.setTitle(notificationUpdateDTO.getTitle());
        notification.setContent(notificationUpdateDTO.getContent());
        notification = this.notificationService.save(notification);

        return new ResponseEntity<>(new NotificationDTO(notification), HttpStatus.OK);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/billboard/notifications",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Create a notification resource.",
            notes = "Returns the administrator being saved. Method returns the notification being saved.",
            httpMethod = "POST",
            produces = "application/json",
            consumes = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully created notification", response = NotificationDTO.class),
            @ApiResponse(code = 400, message = "Inappropriate notification object sent in request body"),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 403, message = "You don't have permission to create resource"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity createNotification(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The public information DTO object", required = true) @Valid @RequestBody NotificationCreateUpdateDTO notificationCreateUpdateDTO,
            @ApiParam(value = "The object that contains all errors from validation of DTO object") BindingResult errors) {

        Building building = this.buildingService.findOne(buildingId);
        this.tenantService.checkPermissionForCurrentTenantInBuilding(building.getId());

        Tenant currentTenant = this.accountService.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).getTenant();

        Notification notification = convertNotificationCreateUpdateDTOToNotification(notificationCreateUpdateDTO);
        notification.setCreator(currentTenant);
        notification.setDate(new Date());

        Billboard billboard = building.getBillboard();
        billboard.getNotifications().add(notification);
        notification.setBillboard(billboard);
        notification = this.notificationService.save(notification);

        return new ResponseEntity<>(new NotificationDTO(notification), HttpStatus.CREATED);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/billboard/notifications/{id}",
            method = RequestMethod.DELETE
    )
    @ApiOperation(
            value = "Delete notification resource.",
            notes = "You have to provide a valid IDs for building and notification in the URL. " +
                    "Once deleted the resource can not be recovered.",
            httpMethod = "DELETE"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully deleted notification"),
            @ApiResponse(code = 401, message = "You are not authorized to delete the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity deleteNotification(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The ID of the existing notification resource.", required = true) @PathVariable("id") Long id){

        Building building = this.buildingService.findOne(buildingId);
        this.tenantService.checkPermissionForCurrentTenantInBuilding(building.getId());

        Notification notification = this.notificationService.findOne(id);
        // provera da li obavestenje sa zadatim ID-em pripada zadatoj zgradi
        this.notificationService.findOneByIdAndBillboard(notification.getId(), building.getBillboard().getId());

        Tenant currentTenant = this.accountService.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).getTenant();
        // trenutno ulogovani korisnik nije kreator obavestenja koje se brise
        if(!currentTenant.getId().equals(notification.getCreator().getId()))
            throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);

        building.getBillboard().getNotifications().remove(notification);
        this.notificationService.remove(notification.getId());

        return new ResponseEntity(HttpStatus.OK);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/billboard/meeting_items",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a list of all meeting items in building in page form.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of meeting items", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to view these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity getMeetingItems(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The number of page for getting meeting items.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The size of page for getting meeting items.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting notifications.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "The sort property for getting notifications.", required = true) @RequestParam String sortProperty) {

        Building building = this.buildingService.findOne(buildingId);

        this.tenantService.checkPermissionForCurrentTenantInBuilding(building.getId());

        return getAllBillboardItems(building, false, Integer.valueOf(pageNumber),
                Integer.valueOf(pageSize), sortDirection, sortProperty, null, null);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/meetings/{meetingId}/meeting_items",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a list of all meeting items that are chosen for selected meeting, in page form.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of meeting items", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to view these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity getMeetingItemsForMeeting(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The ID of the existing meeting resource.", required = true) @PathVariable("meetingId") Long meetingId,
            @ApiParam(value = "The number of page for getting meeting items.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The size of page for getting meeting items.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting notifications.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "The sort property for getting notifications.", required = true) @RequestParam String sortProperty) {

        Building building = this.buildingService.findOne(buildingId);
        this.tenantService.checkPermissionForCurrentTenantInBuilding(building.getId());

        Meeting meeting = this.meetingService.findOne(meetingId);
        if (!meeting.getCouncil().getId().equals(building.getCouncil().getId()))
            throw new BadRequestException("Selected meeting doesn't belong to selected building!");

        boolean isPresident = false;

        for(GrantedAuthority authority : SecurityContextHolder.getContext().getAuthentication().getAuthorities()) {
            if(authority.getAuthority().equals(MessageConstants.PRESIDENT_ROLE))
                isPresident = true;
        }

        // ukoliko logovani stanar nije i predsednik skupstine, njemu nece biti vidljivi detalji (tacke dnevnog reda) sastanka
        // vec tek nakon sto je kreiran izvestaj za isti
        if(!isPresident)
            this.meetingService.checkMeetingStart(meeting);

        else if(meeting.getRecord() != null) {
            // ukooliko je stanar ujedno i predsednik skusptine, moci ce da vidi detalje sastanka (tacke dnecnog reda)
            // i nakon njegovog zavrsetka kako bi mogao da kreira izvestaj za taj sastanak
            throw new BadRequestException("Meeting is already processed, see details in record from this meeting!");
        }

        ArrayList<MeetingItemDTO> meetingItemDTOS = new ArrayList<>();
        Page<MeetingItem> meetingItems = this.meetingItemService.findAllForMeeting(meeting.getId(), Integer.valueOf(pageNumber),
                Integer.valueOf(pageSize), sortDirection, sortProperty);

        meetingItems.forEach(meetingItem -> meetingItemDTOS.add(new MeetingItemDTO(meetingItem,
                meetingItems.getTotalElements(), building.getId())));

        return new ResponseEntity(meetingItemDTOS, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/meetings/{meetingId}/record/meeting_items",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a list of all meeting items that are discussed in selected meeting, in page form.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of meeting items", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to view these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity getMeetingItemsForRecord(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The ID of the existing meeting resource.", required = true) @PathVariable("meetingId") Long meetingId,
            @ApiParam(value = "The number of page for getting meeting items.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The size of page for getting meeting items.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting notifications.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "The sort property for getting notifications.", required = true) @RequestParam String sortProperty) {

        Building building = this.buildingService.findOne(buildingId);
        this.tenantService.checkPermissionForCurrentTenantInBuilding(building.getId());

        Meeting meeting = this.meetingService.findOne(meetingId);
        if (!meeting.getCouncil().getId().equals(building.getCouncil().getId()))
            throw new BadRequestException("Selected meeting doesn't belong to selected building!");

        if(meeting.getRecord() == null)
            throw new BadRequestException("Record for this meeting isn't created yet!");

        ArrayList<MeetingItemDTO> meetingItemDTOS = new ArrayList<>();
        Page<MeetingItem> meetingItems = this.meetingItemService.findAllForRecord(meeting.getId(), Integer.valueOf(pageNumber),
                Integer.valueOf(pageSize), sortDirection, sortProperty);

        meetingItems.forEach(meetingItem -> meetingItemDTOS.add(new MeetingItemDTO(meetingItem,
                meetingItems.getTotalElements(), building.getId())));

        return new ResponseEntity(meetingItemDTOS, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/billboard/meeting_items_from_to",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a list of all meeting items in building that are created in given period, in page form.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of meeting items", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to view these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity searchMeetingItems(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "From date") @RequestParam("from") String from,
            @ApiParam(value = "To date") @RequestParam("to") String to,
            @ApiParam(value = "The page number for getting meeting items.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting meeting items.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting meeting items.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "The sort property for getting meeting items.", required = true) @RequestParam String sortProperty) {

        Building building = this.buildingService.findOne(buildingId);
        this.tenantService.checkPermissionForCurrentTenantInBuilding(building.getId());

        SimpleDateFormat sdf = new SimpleDateFormat(MessageConstants.DATE_FORMAT);
        Date fromDate = null;
        Date toDate = null;
        try {
            fromDate = sdf.parse(from);
            toDate = sdf.parse(to);
        } catch (Exception e) {
            throw new BadRequestException(MessageConstants.INVALID_FORMAT_OF_DATE_PARAMETER);
        }

        return getAllBillboardItems(building, false, Integer.valueOf(pageNumber),
                Integer.valueOf(pageSize), sortDirection, sortProperty, fromDate, toDate);
    }



    @RequestMapping(
            value = "/tenants/{tenantId}/meeting_items/active_questionnaires",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a list of meeting items with active questionnaire for selected tenant, in page form.",
            notes = "You have to provide valid tenant ID in the URL",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of meeting items with active questionnaire", response = List.class),
            @ApiResponse(code = 400, message = "Selected tenant isn't owner of any apartment"),
            @ApiResponse(code = 401, message = "You are not authorized to view these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity getActiveQuestionnaires(
            @ApiParam(value = "The ID of the existing tenant resource.", required = true) @PathVariable("tenantId") Long tenantId,
            @ApiParam(value = "The page number for getting questionnaires.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting questionnaires.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting questionnaires.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "First sort property for getting questionnaires.", required = true) @RequestParam String sortProperty) {

        Tenant currentTenant = this.tenantService.findOne(tenantId);

        // provera da li je trenutno logovani stanar vlasnik bar jednog stana
        if(currentTenant.getPersonalApartments().isEmpty())
            throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);

        List<MeetingItemDTO> meetingItemDTOS = new ArrayList<>();

        Page<MeetingItem> meetingItems = this.meetingItemService.findAllWithActiveQuestionnairesForTenant(currentTenant.getId(),
                Integer.valueOf(pageNumber), Integer.valueOf(pageSize), sortDirection, sortProperty);

        meetingItems.forEach(meetingItem -> meetingItemDTOS.add(new MeetingItemDTO(meetingItem, meetingItems.getTotalElements(),
                this.buildingService.findOneByBillboardId(meetingItem.getBillboard().getId()).getId())));

        return new ResponseEntity<>(meetingItemDTOS, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/meeting_items/finished_questionnaires",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a list of meeting items with questionnaire that is finished and have results of voting, in page form.",
            notes = "You have to provide valid building ID in the URL",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of meeting items with finished questionnaire", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to view these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity getFinishedQuestionnaires(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The page number for getting questionnaires.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting questionnaires.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting questionnaires.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "First sort property for getting questionnaires.", required = true) @RequestParam String sortProperty) {

        Building building = this.buildingService.findOne(buildingId);
        this.tenantService.checkPermissionForCurrentTenantInBuilding(building.getId());

        Tenant currentTenant = this.accountService.findByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()).getTenant();
        // provera da li je trenutno logovani stanar clan skupstine stanara u ovoj zgradi
        if(currentTenant.getCouncil() == null)
            throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);

        List<MeetingItemDTO> meetingItemDTOS = new ArrayList<>();

        Page<MeetingItem> meetingItems = this.meetingItemService.findAllFinishedQuestionnairesInBuilding(building.getId(),
                Integer.valueOf(pageNumber), Integer.valueOf(pageSize), sortDirection, sortProperty);

        meetingItems.forEach(meetingItem -> meetingItemDTOS.add(new MeetingItemDTO(meetingItem, meetingItems.getTotalElements(),
                this.buildingService.findOneByBillboardId(meetingItem.getBillboard().getId()).getId())));

        return new ResponseEntity<>(meetingItemDTOS, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/billboard/meeting_items/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a single meeting item.",
            notes = "You have to provide a valid IDs for building and meeting item.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved meeting item", response = MeetingItemDTO.class),
            @ApiResponse(code = 400, message = "Invalid object IDs sent in URL"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity getMeetingItem(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The ID of the existing meeting item resource.", required = true) @PathVariable("id") Long id) {

        Building building = this.buildingService.findOne(buildingId);
        this.tenantService.checkPermissionForCurrentTenantInBuilding(building.getId());

        MeetingItem meetingItem = this.meetingItemService.findOne(id);
        // provera da li tacka dnevnog reda sa zadatim ID-em pripada zadatoj zgradi (da li se trenutno nalazi na bilboardu te zgrade)
        this.meetingItemService.findOneByIdAndBillboard(meetingItem.getId(), building.getBillboard().getId());

        return new ResponseEntity<>(new MeetingItemDTO(meetingItem, building.getId()), HttpStatus.OK);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/billboard/meeting_items/{id}",
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Update a meeting item resource.",
            notes = "You have to provide valid IDs for building and meeting item in the URL. " +
                    "Method returns the meeting item being updated.",
            httpMethod = "PUT",
            consumes = "application/json",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated meeting item", response = MeetingItemDTO.class),
            @ApiResponse(code = 400, message = "Inappropriate meeting item object sent in request body"),
            @ApiResponse(code = 401, message = "You are not authorized to update the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity updateMeetingItem(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The ID of the existing meeting item resource.", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "The meeting item object.", required = true) @Valid @RequestBody MeetingItemUpdateDTO meetingItemUpdateDTO,
            @ApiParam(value = "The object that contains all errors from validation of DTO object") BindingResult errors) {

        Building building = this.buildingService.findOne(buildingId);
        this.tenantService.checkPermissionForCurrentTenantInBuilding(building.getId());

        MeetingItem meetingItem = this.meetingItemService.findOne(id);
        // provera da li tacka dnevnog reda sa zadatim ID-em pripada zadatoj zgradi (da li se trenutno nalazi na bilboardu te zgrade)
        this.meetingItemService.findOneByIdAndBillboard(meetingItem.getId(), building.getBillboard().getId());

        Tenant currentTenant = this.accountService.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).getTenant();
        // trenutno ulogovani korisnik nije kreator tacke dnevnog reda koja se azurira
        if(!currentTenant.getId().equals(meetingItem.getCreator().getId()))
            throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);

        meetingItem.setContent(meetingItemUpdateDTO.getContent());
        meetingItem.setTitle(meetingItemUpdateDTO.getTitle());
        if(meetingItemUpdateDTO.getQuestionnaireUpdate() != null) {
            // u DTO objektu postoji objekat ankete
            QuestionnaireUpdateDTO questionnaireUpdateDTO = meetingItemUpdateDTO.getQuestionnaireUpdate();

            if(meetingItem.getQuestionnaire() == null) {
                // tacka dnevnog reda koja se azurira do sada nije sadrzala nikakvu anketu, pa se snima
                // objekat ankete poslat u DTO-u
                persistNewQuestionnaireToMeetingItem(meetingItem, questionnaireUpdateDTO);
            }
            else {
                // tacka dnevnog reda koja se azurira sadrzi anketu, pa je neophodno proveriti poklapanje ID-eva ankete koja postoji
                // na nivou tacke dnevnog red i ankete koja se nalazi u DTO-u
                this.questionnaireService.findOne(questionnaireUpdateDTO.getId());
                this.meetingItemService.checkMeetingItemQuestionnaire(meetingItem, questionnaireUpdateDTO.getId());
                updateExistingQuestionnaireInMeetingItem(meetingItem, questionnaireUpdateDTO);
            }
        }
        else if(meetingItemUpdateDTO.getQuestionnaireUpdate() == null && meetingItem.getQuestionnaire() != null) {
            // ukoliko tacka dnevnog reda koja se azurira sadrzi anketu, a u DTO-u nema objekta ankete, brisemo postojecu anketu iz tacke dnevnog reda
            removeQuestionnaireFromMeetingItem(meetingItem);
        }

        if(meetingItemUpdateDTO.getDamageId() == null && meetingItem.getDamage() != null) {
            // ukoliko tacka dnevnog reda koja se azurira referencira neki kvar, a u DTO-u nema id-a kvara,
            // uklanjamo referencu ka kvaru iz posmatrane tacke dnevnog reda
            meetingItem.setDamage(null);
        }
        else if(meetingItemUpdateDTO.getDamageId() != null) {
            // u DTO-u postoji id kvara na koji treba da se referencira tacka dnevnog reda koja se azurira
            Damage damageFromMeetingItem = meetingItem.getDamage();
            if(damageFromMeetingItem == null || !damageFromMeetingItem.getId().equals(meetingItemUpdateDTO.getDamageId())) {
                // ukoliko tacka dnevnog reda do sada nije referencirala nikakav kvar ili je referencirala neki ciji se ID
                // razlikuje od ID-a kvara koji se nalazi u DTO objektu, neophodno je izvrsiti provere: postojanje kvara, pripadnost zgradi, odgovornu osobu
                Damage damage = this.damageService.findOne(meetingItemUpdateDTO.getDamageId());
                this.damageService.checkDamageInBuilding(damage, building.getId());
                this.damageService.checkResponsiblePerson(damage, currentTenant.getId());

                damage.getMeetingItems().add(meetingItem);
                meetingItem.setDamage(damage);
            }
        }

        meetingItem = this.meetingItemService.save(meetingItem);
        return new ResponseEntity<>(new MeetingItemDTO(meetingItem, building.getId()), HttpStatus.OK);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/billboard/meeting_items",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Create a meeting item resource.",
            notes = "Returns the administrator being saved. Method returns the meeting item being saved.",
            httpMethod = "POST",
            produces = "application/json",
            consumes = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully created meeting item", response = MeetingItemDTO.class),
            @ApiResponse(code = 400, message = "Inappropriate meeting item object sent in request body"),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 403, message = "You don't have permission to create resource"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity createMeetingItem(
            @ApiParam(value = "The meeting item DTO object", required = true) @Valid @RequestBody MeetingItemCreateDTO meetingItemCreateDTO,
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The object that contains all errors from validation of DTO object") BindingResult errors) {

        Building building = this.buildingService.findOne(buildingId);
        this.tenantService.checkPermissionForCurrentTenantInBuilding(building.getId());

        Tenant currentTenant = this.accountService.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).getTenant();

        MeetingItem meetingItem = new MeetingItem(meetingItemCreateDTO.getTitle(), meetingItemCreateDTO.getContent(), new Date());
        meetingItem.setCreator(currentTenant);

        Questionnaire questionnaire = null;
        if(meetingItemCreateDTO.getQuestionnaireCreate() != null) {
            questionnaire = new Questionnaire();

            for(QuestionCreateDTO questionCreateDTO : meetingItemCreateDTO.getQuestionnaireCreate().getQuestions()) {
                Question question = convertQuestionCreateDTOToQuestion(questionCreateDTO);
                question.setQuestionnaire(questionnaire);
                questionnaire.getQuestions().add(question);
            }
            questionnaire = this.questionnaireService.save(questionnaire);
        }
        meetingItem.setQuestionnaire(questionnaire);

        Damage damage = null;
        if(meetingItemCreateDTO.getDamageId() != null) {
            // ukoliko se predlog tacke dnevnog reda tice nekog kvara, neophodno je da ga referenciramo
            damage = this.damageService.findOne(meetingItemCreateDTO.getDamageId());
            // provera da li izabrani kvar pripada zadatoj zgradi
            this.damageService.checkDamageInBuilding(damage, building.getId());
            // provera da li je trenutno logovani stanar odgovorna osoba za dati kvar
            this.damageService.checkResponsiblePerson(damage, currentTenant.getId());

            damage.getMeetingItems().add(meetingItem);
        }
        meetingItem.setDamage(damage);

        Billboard billboard = building.getBillboard();
        billboard.getMeetingItems().add(meetingItem);
        meetingItem.setBillboard(billboard);
        meetingItem = this.meetingItemService.save(meetingItem);

        if(questionnaire != null) {
            questionnaire.setMeetingItem(meetingItem);
            this.questionnaireService.save(questionnaire);
        }

        return new ResponseEntity<>(new MeetingItemDTO(meetingItem, building.getId()), HttpStatus.CREATED);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/billboard/meeting_items/{id}",
            method = RequestMethod.DELETE
    )
    @ApiOperation(
            value = "Delete meeting item resource.",
            notes = "You have to provide a valid IDs for building and meeting item in the URL. " +
                    "Once deleted the resource can not be recovered.",
            httpMethod = "DELETE"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully deleted meeting item"),
            @ApiResponse(code = 401, message = "You are not authorized to delete the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity deleteMeetingItem(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The ID of the existing meeting item resource.", required = true) @PathVariable("id") Long id){

        Building building = this.buildingService.findOne(buildingId);
        this.tenantService.checkPermissionForCurrentTenantInBuilding(building.getId());

        MeetingItem meetingItem = this.meetingItemService.findOne(id);
        // provera da li tacka dnevnog reda sa zadatim ID-em pripada zadatoj zgradi
        // (da li se trenutno nalazi na bilboardu te zgrade)
        this.meetingItemService.findOneByIdAndBillboard(meetingItem.getId(), building.getBillboard().getId());

        Tenant currentTenant = this.accountService.findByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()).getTenant();
        // trenutno ulogovani korisnik nije kreator tacke dnevnog reda koja se brise
        if(!currentTenant.getId().equals(meetingItem.getCreator().getId()))
            throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);

        if(meetingItem.getQuestionnaire() != null) {
            removeQuestionnaireFromMeetingItem(meetingItem);
        }
        building.getBillboard().getMeetingItems().remove(meetingItem);
        this.meetingItemService.remove(meetingItem.getId());

        return new ResponseEntity(HttpStatus.OK);
    }



    private ResponseEntity<?> getAllBillboardItems(Building building, boolean areNotifications, Integer pageNumber,
                                                   Integer pageSize, String sortDirection, String sortProperty,
                                                   Date from, Date to) {
        ResponseEntity responseEntity = null;

        if(areNotifications) {
            List<NotificationDTO> notificationDTOS = new ArrayList<>();

            if(from == null && to == null) {
                Page<Notification> notifications = this.notificationService.findAll(building.getBillboard().getId(),
                        pageNumber, pageSize, sortDirection, sortProperty);
                notifications.forEach(notification -> notificationDTOS.add(new NotificationDTO(notification,
                        notifications.getTotalElements())));
                responseEntity = new ResponseEntity<>(notificationDTOS, HttpStatus.OK);
            }
            else {
                Page<Notification> notifications = this.notificationService.findAllFromTo(
                        building.getBillboard().getId(), from, to, pageNumber, pageSize, sortDirection, sortProperty);
                notifications.forEach(notification -> notificationDTOS.add(new NotificationDTO(notification,
                        notifications.getTotalElements())));
                responseEntity = new ResponseEntity<>(notificationDTOS, HttpStatus.OK);
            }
        }
        else {
            List<MeetingItemDTO> meetingItemDTOS = new ArrayList<>();

            if(from == null && to == null) {
                Page<MeetingItem> meetingItems = this.meetingItemService.findAllOnBillboard(
                        building.getBillboard().getId(), pageNumber, pageSize, sortDirection, sortProperty);
                meetingItems.forEach(meetingItem -> meetingItemDTOS.add(new MeetingItemDTO(meetingItem,
                        meetingItems.getTotalElements(), building.getId())));
                responseEntity = new ResponseEntity<>(meetingItemDTOS, HttpStatus.OK);
            }
            else {
                Page<MeetingItem> meetingItems = this.meetingItemService.findAllOnBillboardFromTo(
                        building.getBillboard().getId(), from, to, pageNumber, pageSize, sortDirection, sortProperty);
                meetingItems.forEach(notification -> meetingItemDTOS.add(new MeetingItemDTO(notification,
                        meetingItems.getTotalElements(), building.getId())));
                responseEntity = new ResponseEntity<>(meetingItemDTOS, HttpStatus.OK);
            }
        }

        return responseEntity;
    }

    private MeetingItem updateExistingQuestionnaireInMeetingItem(MeetingItem meetingItem,
                                                                 QuestionnaireUpdateDTO questionnaireUpdateDTO) {
        Questionnaire questionnaire = meetingItem.getQuestionnaire();

        // provera da li sva pitanja koja su u DTO-u a imaju ID != -1 (nisu novokreirana) pripadaju bas zadatoj anketi
        for(QuestionUpdateDTO questionUpdateDTO : questionnaireUpdateDTO.getQuestions()) {
            if(!questionUpdateDTO.getId().toString().equals("-1")) {
                boolean questionFound = false;
                for(Question question : meetingItem.getQuestionnaire().getQuestions()) {
                    if(question.getId().equals(questionUpdateDTO.getId())) {
                        questionFound = true;
                        break;
                    }
                }

                if(!questionFound) throw new BadRequestException("Some question doesn't belong to " +
                        "questionnaire of selected meeting item!");
            }
        }

        List<Long> questionsForRemove = new ArrayList<>();
        // sva pitanja kojih nema u listi pitanja iz DTO-a belezimo u listu kako bismo ih obrisali
        for(Question question : meetingItem.getQuestionnaire().getQuestions()) {
            boolean questionFound = false;
            for(QuestionUpdateDTO questionUpdateDTO : questionnaireUpdateDTO.getQuestions()) {
                if(question.getId().equals(questionUpdateDTO.getId())) {
                    questionFound = true;
                    break;
                }
            }

            if(!questionFound) questionsForRemove.add(question.getId());
        }
        for(Long id : questionsForRemove) {
            Question questionRemove = null;
            for(Question question : questionnaire.getQuestions()) {
                if(question.getId().equals(id)) {
                    questionRemove = question;
                    break;
                }
            }
            questionnaire.getQuestions().remove(questionRemove);
            this.questionService.removeAnswers(id);
            this.questionService.remove(id);
        }


        List<Question> questions = new ArrayList<>();
        // prolazimo kroz pitanja iz DTO-a pri cemu postojeca azuriramo a nova snimamo
        for(QuestionUpdateDTO questionUpdateDTO : questionnaireUpdateDTO.getQuestions()) {
            if(questionUpdateDTO.getId().toString().equals("-1")) {
                // pitanje treba snimiti
                Question question = new Question();
                question.setQuestionnaire(questionnaire);
                question.setContent(questionUpdateDTO.getContent());
                question.setAnswers(questionUpdateDTO.getAnswers());
                questions.add(question);
            }
            else {
                // pitanje treba azurirati
                Question question = this.questionService.findOne(questionUpdateDTO.getId());
                this.questionService.removeAnswers(question.getId());
                question.setContent(questionUpdateDTO.getContent());
                question.setAnswers(questionUpdateDTO.getAnswers());
                questions.add(question);
            }
        }

        questionnaire.setQuestions(questions);
        questionnaire = this.questionnaireService.save(questionnaire);
        meetingItem.setQuestionnaire(questionnaire);
        return meetingItem;
    }

    private MeetingItem persistNewQuestionnaireToMeetingItem(MeetingItem meetingItem,
                                                             QuestionnaireUpdateDTO questionnaireUpdateDTO) {
        Questionnaire questionnaire = new Questionnaire();

        List<Question> questions = new ArrayList<>();
        for(QuestionUpdateDTO questionUpdateDTO : questionnaireUpdateDTO.getQuestions()) {
            Question question = new Question();
            question.setContent(questionUpdateDTO.getContent());
            question.setAnswers(questionUpdateDTO.getAnswers());
            question.setQuestionnaire(questionnaire);
            questions.add(question);
        }

        questionnaire.setQuestions(questions);
        questionnaire.setMeetingItem(meetingItem);
        questionnaire = this.questionnaireService.save(questionnaire);

        meetingItem.setQuestionnaire(questionnaire);
        return meetingItem;
    }

    private void removeQuestionnaireFromMeetingItem(MeetingItem meetingItem) {
        for (Question question : meetingItem.getQuestionnaire().getQuestions()) {
            this.questionService.removeAnswers(question.getId());
        }

        Questionnaire questionnaire = meetingItem.getQuestionnaire();
        questionnaire.setMeetingItem(null);
        questionnaire.getQuestions().removeAll(questionnaire.getQuestions());
        questionnaire = this.questionnaireService.save(questionnaire);
        this.questionService.removeQuestions(meetingItem.getQuestionnaire().getId());

        meetingItem.setQuestionnaire(null);
        this.meetingItemService.save(meetingItem);

        this.questionnaireService.remove(questionnaire.getId());
    }
}
