package rs.ac.uns.ftn.ktsnvt.service.unit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Account;
import rs.ac.uns.ftn.ktsnvt.repository.AccountRepository;
import rs.ac.uns.ftn.ktsnvt.service.AccountService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.BDDMockito.given;

/**
 * Created by Katarina Cukurov on 30/11/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AccountServiceUnitTest {

    @Autowired
    private AccountService accountService;

    @MockBean
    private AccountRepository accountRepository;


    @Before
    public void setUp() {
        Account account = new Account("kaca", "kaca");

        given(
                this.accountRepository.findOne(new Long(100))
        ).willReturn(
                account
        );

        given(
                this.accountRepository.findOne(new Long(200))
        ).willReturn(
                null
        );

        given(
                this.accountRepository.findByUsername("kaca")
        ).willReturn(
                account
        );

        given(
                this.accountRepository.findByUsername("acak")
        ).willReturn(
                null
        );

    }

    @Test
    public void testFindOne() {
        /*
         * Test proverava ispravnost rada metode findOne servisa AccountService
         * (dobavljanje naloga na osnovu ID-a)
         * u slucaju kada postoji nalog sa zadatim ID-em
         * */

        /*
        nalog sa ID-em = 100 ima podatke:
            id = 100,
            username = kaca,
            password = kaca ($2a$10$FXALP0p8VlmzCbbCTDTV8uIS46lpCbCpeIq74qulCyxzhCsOazAZS)
         */
        Account account = this.accountService.findOne(new Long(100));
        assertNotNull(account);
        assertEquals("kaca", account.getUsername());
    }

    @Test(expected = NotFoundException.class)
    public void testFindOneNonexistent() {
        /*
         * Test proverava ispravnost rada metode findOne servisa AccountService
         * (dobavljanje naloga na osnovu ID-a)
         * u slucaju kada ne postoji nalog sa zadatim ID-em - ocekivano NotFoundException
         * */

        // account sa ID-em 200 ne postoji
        Account account = this.accountService.findOne(new Long(200));
    }

    @Test
    public void testFindByUsername() {
        /*
         * Test proverava ispravnost rada metode findByUsername servisa AccountService
         * (dobavljanje naloga na osnovu korisnickog imena)
         * u slucaju kada postoji nalog sa zadatim korisnickim imenom
         * */

        /*
        nalog sa korisnickim imenom "kaca" ima od podataka:
            id = 100,
            username = kaca,
            password = kaca
        */
        Account account = this.accountService.findByUsername("kaca");
        assertNotNull(account);
        assertEquals("kaca", account.getUsername());
    }

    @Test(expected = NotFoundException.class)
    public void testFindByUsernameNonexistent() {
        /*
         * Test proverava ispravnost rada metode findByUsername servisa AccountService
         * (dobavljanje naloga na osnovu korisnickog imena)
         * u slucaju kada ne postoji nalog sa zadatim korisnickim imenom - ocekivano NotFoundException
         * */

        // ne postoji account sa korisnickim imenom acak
        Account account = this.accountService.findByUsername("acak");
    }

    @Test(expected = BadRequestException.class)
    public void testCheckUsername(){
         /*
         * Test proverava ispravnost rada metode checkUsername servisa AccountService
         * (provera da li je vec zauzeto neko korisnicko ime)
         * u slucaju kada postoji nalog sa zadatim korisnickim imenom - ocekivano BadRequestException
         * */

        this.accountService.checkUsername("kaca");
    }
}
