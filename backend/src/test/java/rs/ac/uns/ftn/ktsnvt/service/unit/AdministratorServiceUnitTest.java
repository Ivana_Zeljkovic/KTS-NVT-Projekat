package rs.ac.uns.ftn.ktsnvt.service.unit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Account;
import rs.ac.uns.ftn.ktsnvt.model.entity.Administrator;
import rs.ac.uns.ftn.ktsnvt.repository.AccountRepository;
import rs.ac.uns.ftn.ktsnvt.repository.AdministratorRepository;
import rs.ac.uns.ftn.ktsnvt.service.AdministratorService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;

/**
 * Created by Ivana Zeljkovic on 28/11/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AdministratorServiceUnitTest {

    @Autowired
    private AdministratorService administratorService;

    @MockBean
    private AdministratorRepository administratorRepository;


    @Before
    public void setUp() {
        // ADMINISTRATOR, ACCOUNT
        Account account = new Account("ivana", "ivana");
        Administrator administrator = new Administrator("Ivana", "Zeljkovic", account);

        given(
                this.administratorRepository.findOne(new Long(99))
        ).willReturn(
                administrator
        );

        given(
                this.administratorRepository.findOne(new Long(200))
        ).willReturn(
                null
        );
    }

    @Test
    public void testFindOne() {
        /*
         * Test proverava ispravnost rada metode findOne servisa AdministratorService
         * (dobavljanje administrator objekta na osnovu ID-a)
         * u slucaju kada postoji administrator sa zadatim ID-em
         * */

        /*
        administrator sa ID-em = 99 ima podatke:
            ime = Ivana,
            prezime = Zeljkovic,
            korisnicko ime = ivana
         */
        Administrator administrator = this.administratorService.findOne(new Long(99));

        assertNotNull(administrator);
        assertEquals("ivana", administrator.getAccount().getUsername());
        assertEquals("Ivana", administrator.getFirstName());
        assertEquals("Zeljkovic", administrator.getLastName());
    }

    @Test(expected = NotFoundException.class)
    public void testFindOneNonexistent() {
        /*
         * Test proverava ispravnost rada metode findOne servisa AdministratorService
         * (dobavljanje administrator objekta na osnovu ID-a)
         * u slucaju kada ne postoji administrator sa zadatim ID-em - ocekivano NotFoundException
         * */

        // administrator sa ID-em = 200 ne postoji
        Administrator administrator = this.administratorService.findOne(new Long(200));
    }

    @Test
    public void testCheckPasswordUpdate() {
        /*
         * Test proverava ispravnost rada metode checkPasswordUpdate servisa AdministratorService
         * (provera prava izmene trenutne vrednosti lozinke)
         * u slucaju kada su vrednosti trenutne lozinke i nove lozinke ispravne (nisu null, trenutna vrednost lozinke
         * odgovara vrednosti lozinke koja je prezistenta)
         * */

        /*
        administrator sa ID-em = 99 ima sledece podatke:\
            ime = Ivana,
            prezime = Zeljkovic,
            korisnicko ime = ivana,
            lozinka = ivana
         */
        Administrator administrator = this.administratorService.findOne(new Long(99));
        String current_password = "ivana";
        String new_password = "novi";

        boolean password_changed = this.administratorService.checkPasswordUpdate(administrator, current_password, new_password);

        BCryptPasswordEncoder crypt = new BCryptPasswordEncoder();
        boolean password_matched = crypt.matches(new_password, administrator.getAccount().getPassword());

        assertTrue(password_changed);
        assertTrue(password_matched);
    }

    @Test(expected = BadRequestException.class)
    public void testCheckPasswordUpdateMissingNewPass() {
        /*
         * Test proverava ispravnost rada metode checkPasswordUpdate servisa AdministratorService
         * (provera prava izmene trenutne vrednosti lozinke)
         * u slucaju kada je vrednost trenutne lozinke ispravna (nije null i odgovara vrednosti lozinke koja je prezistenta)
         * i nedostaje vrednost nove lozinke - ocekivano BadRequestException
         * */

        /*
        administrator sa ID-em = 99 ima sledece podatke:
            ime = Ivana,
            prezime = Zeljkovic,
            korisnicko ime = ivana,
            lozinka = ivana
         */
        Administrator administrator = this.administratorService.findOne(new Long(99));
        String current_password = "ivana";

        boolean password_changed = this.administratorService.checkPasswordUpdate(administrator, current_password, null);
    }

    @Test(expected = BadRequestException.class)
    public void testCheckPasswordUpdateMissingCurrentPass() {
        /*
         * Test proverava ispravnost rada metode checkPasswordUpdate servisa AdministratorService
         * (provera prava izmene trenutne vrednosti lozinke)
         * u slucaju kada je vrednost nove lozinke ispravna (nije null)
         * i nedostaje vrednost trenutne lozinke - ocekivano BadRequestException
         * */

        /*
        administrator sa ID-em = 99 ima sledece podatke:
            ime = Ivana,
            prezime = Zeljkovic,
            korisnicko ime = ivana,
            lozinka = ivana
         */
        Administrator administrator = this.administratorService.findOne(new Long(99));
        String new_password = "novi";

        boolean password_changed = this.administratorService.checkPasswordUpdate(administrator, null, new_password);
    }

    @Test(expected = BadRequestException.class)
    public void testCheckPasswordUpdateInvalidCurrentPass() {
        /*
         * Test proverava ispravnost rada metode checkPasswordUpdate servisa AdministratorService
         * (provera prava izmene trenutne vrednosti lozinke)
         * u slucaju kada je vrednost trenutne lozinke neispravna (nije null ali ne odgovara vrednosti lozinke koja je prezistenta)
         * i vrednost nove lozinke ispravna (nije null) - ocekivano BadRequestException
         * */

        /*
        administrator sa ID-em = 99 ima sledece podatke:
            ime = Ivana,
            prezime = Zeljkovic,
            username = ivana,
            password = ivana
         */
        Administrator administrator = this.administratorService.findOne(new Long(99));
        String current_password = "nevalidno";
        String new_password = "novi";

        boolean password_changed = this.administratorService.checkPasswordUpdate(administrator, current_password, new_password);
    }

    @Test(expected = NullPointerException.class)
    public void testCheckPermissionForCurrentAdmin() {
        /*
         * Test proverava ispravnost rada metode checkPermissionForCurrentAdmin servisa AdministratorService
         * (provera poklapanja ID-a trenutno logovanog administratora i prosledjenog ID-a)
         * u slucaju kada nema trenutno ulogovanog administratora na nivou aplikacije - ocekivano NullPointerException
         * */

        // trenutno nema administratora na sesiji
        this.administratorService.checkPermissionForCurrentAdmin(new Long(99));
    }
}
