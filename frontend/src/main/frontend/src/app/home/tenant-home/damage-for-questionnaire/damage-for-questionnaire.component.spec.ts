import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DamageForQuestionnaireComponent } from './damage-for-questionnaire.component';

describe('DamageForQuestionnaireComponent', () => {
  let component: DamageForQuestionnaireComponent;
  let fixture: ComponentFixture<DamageForQuestionnaireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DamageForQuestionnaireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DamageForQuestionnaireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
