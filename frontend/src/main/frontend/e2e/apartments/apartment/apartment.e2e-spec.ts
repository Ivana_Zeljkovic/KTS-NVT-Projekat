import {browser, by, element} from "protractor";
// page
import {LoginPage} from "../../page-objects/login.po";
import {ApartmentPage} from "./page-objects/apartment.po";
import {ChooseOwnerModalPage} from "../choose-owner-modal/page-objects/choose-owner-modal.po";
import {ConfirmDeletePage} from "../../page-objects/confirm-delete.po";

describe('Apartment details', () => {
  let loginPage: LoginPage;
  let apartmentPage: ApartmentPage;
  let chooseOwnerModalPage: ChooseOwnerModalPage;
  let confirmDeletePage: ConfirmDeletePage;

  beforeEach(() => {
    loginPage = new LoginPage();
    apartmentPage = new ApartmentPage();
    browser.get("/");

    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');
    loginPage.setUsername('ivana');
    loginPage.setPassword('ivana');
    loginPage.getSignInButton().click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/home');
    browser.get('http://localhost:49152/#/apartments/104');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/apartments/104');

  });

  afterEach(() => {
    browser.executeScript('window.localStorage.clear();');
  });

  it("should successfully load page and all of its components", () => {
    expect(apartmentPage.getHeading().isDisplayed()).toBe(true);
    expect(apartmentPage.getNoElements().isDisplayed()).toBe(true);
    expect(apartmentPage.getTenantTable().isPresent()).toBe(false);
    expect(apartmentPage.getAddTenantButton().isDisplayed()).toBe(true);
    expect(apartmentPage.getApartmentTable().isDisplayed()).toBe(true);
    expect(apartmentPage.getChangeOwnerButton().isDisplayed()).toBe(true);

    expect(apartmentPage.getChangeOwnerButton().isEnabled()).toBe(true);
    expect(apartmentPage.getAddTenantButton().isEnabled()).toBe(true);
  });

  it("should successfully add new tenant after  clicked on button Add new tenant", () => {

    apartmentPage.getAddTenantButton().click().then(function () {
      chooseOwnerModalPage = new ChooseOwnerModalPage();
      let tenant = element(by.xpath("//tr[1]/td[4]")).element(by.css('.btn.btn-info'));
      tenant.click();
    });
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/apartments/104');
    expect(apartmentPage.getTenantTable().isDisplayed()).toBe(true);
  });

  it("should delete apartment and remove it from table", () => {
    let deleteButton = element(by.css('.btn.btn-xs.danger'));
    deleteButton.click();
    confirmDeletePage = new ConfirmDeletePage();
    browser.wait(function() {

      return confirmDeletePage.getConfirmDeleteElement().isDisplayed();
    }).then(function() {

      expect(confirmDeletePage.getConfirmButton().isDisplayed()).toBe(true);
      expect(confirmDeletePage.getConfirmButton().isEnabled()).toBe(true);
      expect(confirmDeletePage.getDeclineButton().isDisplayed()).toBe(true);
      expect(confirmDeletePage.getDeclineButton().isEnabled()).toBe(true);

      confirmDeletePage.getConfirmButton().click();
      expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/apartments/104');
      expect(apartmentPage.getNoElements().isDisplayed()).toBe(true);
    });
  });

  it("should successfully choose new owner after clicked on button Choose owner", () => {

    let oldOwner = apartmentPage.getApartmentTable().element(by.xpath("//tr[4]/td[2]"));
    let newOwner: any
    apartmentPage.getChangeOwnerButton().click().then(function () {
      chooseOwnerModalPage = new ChooseOwnerModalPage();
      newOwner = element.all(by.xpath("//tr[1]/td[1]")).get(1).getText() + " " + element(by.xpath("//tr[1]/td[2]"));
      let tenant = element(by.xpath("//tr[1]/td[4]")).element(by.css('.btn.btn-info'));
      tenant.click();
    });

    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/apartments/104');
    expect(oldOwner != newOwner);
  });

});
