package rs.ac.uns.ftn.ktsnvt.model.entity;

import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "council")
@Where(clause="deleted=0")
public class Council {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Column(nullable = false, columnDefinition = "INTEGER DEFAULT 0")
    @Version
    private int version;

    @Column(nullable = false, columnDefinition = "BOOL DEFAULT FALSE")
    private boolean deleted;

    @OneToMany(mappedBy = "council", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private List<Tenant> members;

    @OneToOne
    private Tenant president;

    @OneToOne
    private Business responsibleCompany;

    @OneToMany(mappedBy = "council", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private List<Meeting> meetings;

    public Council() {
        this.members = new ArrayList<>();
        this.meetings = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public List<Tenant> getMembers() {
        return members;
    }

    public void setMembers(List<Tenant> members) {
        this.members = members;
    }

    public Tenant getPresident() {
        return president;
    }

    public void setPresident(Tenant president) { this.president = president; }

    public Business getResponsibleCompany() { return responsibleCompany; }

    public void setResponsibleCompany(Business responsibleCompany) { this.responsibleCompany = responsibleCompany; }

    public List<Meeting> getMeetings() { return meetings; }

    public void setMeetings(List<Meeting> meetings) {
        this.meetings = meetings;
    }
}
