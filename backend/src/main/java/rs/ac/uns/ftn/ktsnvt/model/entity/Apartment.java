package rs.ac.uns.ftn.ktsnvt.model.entity;

import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "apartment")
@Where(clause="deleted=0")
public class Apartment {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Column(nullable = false)
    private int number;

    @Column(nullable = false)
    private int floor;

    @Column(nullable = false)
    private int surface;

    @Column(nullable = false, columnDefinition = "INTEGER DEFAULT 0")
    @Version
    private int version;

    @Column(nullable = false, columnDefinition = "BOOL DEFAULT FALSE")
    private boolean deleted;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private Tenant owner;

    @OneToMany(mappedBy = "apartmentLiveIn", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private List<Tenant> tenants;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private Building building;

    @OneToMany(mappedBy = "apartment", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private List<Damage> damages;

    public Apartment() {
        this.tenants = new ArrayList<>();
        this.damages = new ArrayList<>();
    }

    public Apartment(int number, int floor, int surface) {
        this.number = number;
        this.floor = floor;
        this.surface = surface;
        this.tenants = new ArrayList<>();
        this.damages = new ArrayList<>();
    }

    public Long getId() { return id; }

    public void setId(Long id) {
        this.id = id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public int getSurface() {
        return surface;
    }

    public void setSurface(int surface) {
        this.surface = surface;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Tenant getOwner() { return owner; }

    public void setOwner(Tenant owner) { this.owner = owner; }

    public List<Tenant> getTenants() { return tenants; }

    public void setTenants(List<Tenant> tenants) { this.tenants = tenants; }

    public Building getBuilding() { return building; }

    public void setBuilding(Building building) { this.building = building; }

    public List<Damage> getDamages() { return damages; }

    public void setDamages(List<Damage> damages) { this.damages = damages; }
}
