import { Component, Input, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { ToasterService } from "angular5-toaster/dist";
// service
import { DamageTypeResponsibilityService } from "../../core/services/damage-type-responsibility.service";
import { JwtService } from "../../core/services/jwt.service";
// model
import { DamageTypeResponsibility } from "../../shared/models/damage-type-responsibility";
// error
import { AppError } from "../../shared/errors/app-error";
import { ForbiddenError } from "../../shared/errors/forbidden-error";
import { NotFoundError } from "../../shared/errors/not-found-error";
// component
import { DelegateResponsibilityToPersonComponent } from "../../shared/components/delegate-responsibility-to-person/delegate-responsibility-to-person.component";
import { DelegateResponsibilityToInstitutionComponent } from "../../shared/components/delegate-responsibility-to-institution/delegate-responsibility-to-institution.component";


@Component({
  selector: 'app-damage-types-modal',
  templateUrl: './damage-types-modal.component.html',
  styleUrls: ['./damage-types-modal.component.css']
})
export class DamageTypesModalComponent implements OnInit {
  @Input() category: string;
  @Input() isTenant: boolean;
  damageTypeResponsibilityList: Array<DamageTypeResponsibility>;


  constructor(public modalRef: BsModalRef, private modalService: BsModalService,
              private damageTypeResponsibilityService: DamageTypeResponsibilityService,
              private jwtService: JwtService, private toasterService: ToasterService) { }

  ngOnInit() {
    setTimeout(() => {
      this.isTenant ? this.loadDamageTypesForTenant() : this.loadDamageTypesInBuilding();
    }, 1);
  }

  setResponsibilityForTenant(damageTypeResponsibility: DamageTypeResponsibility) {
    const modalRef = this.modalService.show(DelegateResponsibilityToPersonComponent);
    modalRef.content.damageTypeResponsibility = damageTypeResponsibility;
    modalRef.content.setResponsibility = true;
    modalRef.content.isDelegatingDamage = false;
  }

  setResponsibilityForInstitution(damageTypeResponsibility: DamageTypeResponsibility) {
    let index = this.damageTypeResponsibilityList.indexOf(damageTypeResponsibility);

    const modalRef = this.modalService.show(DelegateResponsibilityToInstitutionComponent);
    modalRef.content.damageTypeResponsibility = damageTypeResponsibility;
    modalRef.content.setResponsibility = true;
    modalRef.content.isDelegatingDamage = false;

    modalRef.content.damageTypeResponsibilitySet.subscribe(updatedResponsibility => {
      this.damageTypeResponsibilityList[index] = updatedResponsibility;
    })
  }

  delegateResponsibility(damageTypeResponsibility: DamageTypeResponsibility) {
    let index = this.damageTypeResponsibilityList.indexOf(damageTypeResponsibility);

    const modalRef = this.modalService.show(DelegateResponsibilityToPersonComponent);
    modalRef.content.damageTypeResponsibility = damageTypeResponsibility;
    modalRef.content.setResponsibility = false;
    modalRef.content.isDelegatingDamage = false;

    modalRef.content.responsibilitySubmitted.subscribe(responsibilityDelegated => {
      if(responsibilityDelegated)
        this.damageTypeResponsibilityList.splice(index, 1);
    })
  }

  private loadDamageTypesForTenant() {
    this.damageTypeResponsibilityService.getAll('buildings', this.jwtService.getBuildingIdFromToken(),
      'tenants', this.jwtService.getIdFromToken(), 'responsible_for_damage_types')
      .subscribe((damageTypeReponsibilities) => {
        this.damageTypeResponsibilityList = damageTypeReponsibilities;
      }, (error: AppError) => {
        if(error instanceof ForbiddenError)
          this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
        else if(error instanceof NotFoundError)
          this.toasterService.pop('error', 'Error', 'Building/Tenant not found!');
        else {
          this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
          throw error;
        }
      });
  }

  private loadDamageTypesInBuilding() {
    this.damageTypeResponsibilityService.getAll('buildings', this.jwtService.getBuildingIdFromToken(),
      'damage_types_responsibilities')
      .subscribe((damageTypeResponsibilities) => {
        this.damageTypeResponsibilityList = damageTypeResponsibilities;
      }, (error: AppError) => {
        if (error instanceof NotFoundError)
          this.toasterService.pop('error', 'Error', 'Building not found!');
        else {
          this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
          throw error;
        }
      });
  }
}
