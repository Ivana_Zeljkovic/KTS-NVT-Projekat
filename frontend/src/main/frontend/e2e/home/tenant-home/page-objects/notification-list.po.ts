import {by, element, ElementArrayFinder, ElementFinder} from "protractor";

export class NotificationListPage {
  // get methods
  getNotificationListElement(): ElementFinder {
    return element(by.tagName('app-notification-list'));
  }

  getSearchDateRangeInput(): ElementFinder {
    return element(by.id('fromToDate'));
  }

  getSearchDateRangeClearButton(): ElementFinder {
    return element(by.buttonText('Clear'));
  }

  getSearchDateRangeSearchButton(): ElementFinder {
    return element(by.buttonText('Search'));
  }

  getAddButton(): ElementFinder {
    return element(by.buttonText('Add'));
  }

  getNotificationList(): ElementFinder {
    return element(by.id('notificationList'));
  }

  getMessageNoNotificationInList(): ElementFinder {
    return element(by.id('messageEmptyList'));
  }

  getPanelsFromNotificationList(): ElementArrayFinder {
    return element.all(by.css('div.panel'));
  }

  getPaginatorInNotificationList(): ElementFinder {
    return element(by.tagName('pagination'));
  }

  getTitleFromNotificationPanel(index: number): ElementFinder {
    return element(by.css(`#notificationList > .panel:nth-child(${index+1}) .notification-title`));
  }

  getCreatorNameFromNotificationPanel(index: number): ElementFinder {
    return element(by.css(`#notificationList > .panel:nth-child(${index+1}) .notification-metadata span:nth-child(2)`));
  }

  getDateFromNotificationPanel(index: number): ElementFinder {
    return element(by.css(`#notificationList > .panel:nth-child(${index+1}) .notification-metadata span:nth-child(4)`));
  }

  getContentFromNotificationPanel(index: number): ElementFinder {
    return element(by.css(`#notificationList > .panel:nth-child(${index+1}) .panel-body`));
  }

  getMenuFromNotificationPanel(index: number): ElementFinder {
    return element(by.css(`#notificationList > .panel:nth-child(${index+1}) .notification-menu`));
  }

  getContentOfMenuFromNotificationPanel(index: number): ElementFinder {
    return element(by.css(`#notificationList > .panel:nth-child(${index+1}) .notification-menu #notificationPopover`))
  }

  getEditMenuOptionFromNotificationPanel(index: number): ElementFinder {
    return element(by.css(`#notificationList > .panel:nth-child(${index+1}) .notification-menu div:first-of-type > a`));
  }

  getDeleteMenuOptionFromNotificationPanel(index: number): ElementFinder {
    return element(by.css(`#notificationList > .panel:nth-child(${index+1}) .notification-menu div:last-of-type > a`));
  }
}
