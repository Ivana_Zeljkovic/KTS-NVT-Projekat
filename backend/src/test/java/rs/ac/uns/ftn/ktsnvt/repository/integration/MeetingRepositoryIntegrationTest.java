package rs.ac.uns.ftn.ktsnvt.repository.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.model.entity.Meeting;
import rs.ac.uns.ftn.ktsnvt.repository.MeetingRepository;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Ivana Zeljkovic on 07/12/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class MeetingRepositoryIntegrationTest {

    @Autowired
    private MeetingRepository meetingRepository;


    @Test
    @Transactional
    public void testFindFirstNextMeetingInBuilding() {
        /*
        * Test proverava ispravnost rada metode findFirstNextMeetingInBuilding repozitorijuma MeetingRepository
        * (dobavljanje prvog sledeceg sastanka na nivou zgrade od cije je skupstine id zadat)
        * */

        /*
        prvi sledeci sastanak za zgradu sa ID-em = 101 (skupstinu stanara sa ID-em = 101) je sastanak:
            id = 105,
            duration = 45
            council_id = 101
        */
        Meeting meeting = this.meetingRepository.findFirstNextMeetingInBuilding(Long.valueOf(101), new Date());

        assertNotNull(meeting);
        assertEquals(Long.valueOf(105), meeting.getId());
        assertEquals(45, meeting.getDuration());
        assertEquals(Long.valueOf(101), meeting.getCouncil().getId());
    }

    @Test
    @Transactional
    public void testFindAllByBuildingId() {
        /*
        * Test proverava ispravnost rada metode findAllByBuildingId repozitorijuma MeetingRepository
        * (dobavljanje liste svih sastanaka koji ce se odrzati u buducnosti na nivou zgrade sa zadatim ID-em)
        * */

        /*
        zgrada sa ID-em = 101 ima 2 sastanka koji ce se odrzati u buducnosti:
            1 - id = 104,
                duration = 60,
                date = 2018-12-30 10:00:00
            2 - id = 105,
                duration = 45,
                date = 2018-12-30 07:00:00
        */
        Page<Meeting> meetingList = this.meetingRepository.findAllByBuildingId(101L, new Date(),
                new PageRequest(0, 2, Sort.Direction.ASC, "id"));

        assertEquals(2, meetingList.getContent().size());
        assertEquals(Long.valueOf(104), meetingList.getContent().get(0).getId());
        assertEquals(60, meetingList.getContent().get(0).getDuration());
        assertEquals(Long.valueOf(105), meetingList.getContent().get(1).getId());
        assertEquals(45, meetingList.getContent().get(1).getDuration());
    }
}
