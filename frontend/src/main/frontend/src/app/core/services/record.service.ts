import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
// service
import { DataService } from "./data.service";
// models
import { Record } from "../../shared/models/record";


@Injectable()
export class RecordService extends DataService<Record>{

  constructor(http: HttpClient) {
    super('', http);
  }

}
