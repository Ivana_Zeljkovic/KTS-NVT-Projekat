package rs.ac.uns.ftn.ktsnvt.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import rs.ac.uns.ftn.ktsnvt.model.entity.Business;

/**
 * Created by Ivana Zeljkovic on 05/11/2017.
 */
public interface BusinessRepository extends JpaRepository<Business, Long> {

    @Override
    @Query(value = "select b FROM Business b WHERE deleted = FALSE AND b.confirmed = TRUE")
    Page<Business> findAll(Pageable pageable);

    @Query(value = "select b FROM Business b WHERE b.id != :id AND deleted = FALSE AND b.confirmed = TRUE")
    Page<Business> findAllExceptCurrent(@Param("id") Long currentId, Pageable pageable);

    @Query("SELECT b FROM Business b WHERE deleted = FALSE AND id = :id")
    Business findOne(@Param("id") Long id);

    @Query(value = "SELECT * from Business b WHERE b.address_id = :id and b.is_company = :is_firm AND b.deleted = FALSE",
            nativeQuery = true)
    Business findByAddressId(@Param("id") Long id, @Param("is_firm") boolean isCompany);

    @Query("SELECT b FROM Business b WHERE id = :id AND deleted = TRUE")
    Business findOneInactive(@Param("id") Long id);

    @Query( value = "SELECT b FROM Business b WHERE b.isCompany = :isCompany AND b.deleted = FALSE AND b.confirmed = TRUE")
    Page<Business> findAllInstitutionsOrFirms(@Param("isCompany") boolean isCompany, Pageable pageable);

    @Query( value = "SELECT b FROM Business b WHERE b.isCompany = :isCompany AND b.id != :id AND b.deleted = FALSE " +
            "AND b.confirmed = TRUE")
    Page<Business> findAllInstitutionsOrFirmsExceptCurrent(@Param("isCompany") boolean isCompany,
                                                           @Param("id") Long currentId, Pageable pageable);

    @Query( value = "SELECT b FROM Business b WHERE b.isCompany = :isCompany AND b.deleted = FALSE")
    Page<Business> findAllInstitutionsOrFirmsByAdmin(@Param("isCompany") boolean isCompany, Pageable pageable);


    @Query(value = "SELECT * FROM BUSINESS b WHERE b.id NOT IN (SELECT dbdsti.businesses_damage_sent_to_ids " +
            "FROM damage_businesses_damage_sent_to_ids dbdsti WHERE dbdsti.damage_id = :damageId) AND b.confirmed = TRUE " +
            "ORDER BY ?#{#pageable}",
            countQuery ="SELECT * FROM BUSINESS b WHERE b.id NOT IN (SELECT dbdsti.businesses_damage_sent_to_ids " +
                    "FROM damage_businesses_damage_sent_to_ids dbdsti WHERE dbdsti.damage_id = :damageId) AND b.confirmed = TRUE",
            nativeQuery = true)
    Page<Business> findAllWhoDidntGetOffer(@Param("damageId") Long damageId, Pageable pageable);
}
