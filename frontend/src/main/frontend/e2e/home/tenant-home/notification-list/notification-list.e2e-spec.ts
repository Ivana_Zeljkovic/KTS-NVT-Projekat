import { browser } from "protractor";
// page
import { NotificationListPage } from "../page-objects/notification-list.po";
import { NotificationPage } from "../page-objects/notification.po";
import { ConfirmDeletePage } from "../../../page-objects/confirm-delete.po";
import { LoginPage } from "../../../page-objects/login.po";
// helper
import { DateConverter } from "../../../util/date-converter";


describe('Notification list page', () => {
  let loginPage: LoginPage;
  let notificationListPage: NotificationListPage;
  let notificationPage: NotificationPage;
  let confirmDeletePage: ConfirmDeletePage;
  let newNotification: any;

  beforeAll(() => {
    loginPage = new LoginPage();
    notificationListPage = new NotificationListPage();
    notificationPage = new NotificationPage();
    confirmDeletePage = new ConfirmDeletePage();
    newNotification = {'title': 'Title of new notification', 'content': 'Content of new notification',
      'date': '', creator: ''};

    browser.get('/');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');

    loginPage.setUsername('kaca');
    loginPage.setPassword('kaca');
    loginPage.getSignInButton().click();

    browser.wait(function() {
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/home/notifications';
      });
    }).then(function() {
      expect(notificationListPage.getNotificationListElement().isDisplayed()).toBe(true);
    });
  });

  afterAll(() => {
    browser.executeScript('window.localStorage.clear();');
  });



  it('should show list of notifications when user is logged in and redirect to home page', () => {
    expect(notificationListPage.getNotificationListElement().isDisplayed()).toBe(true);

    // expected view of page
    expect(notificationListPage.getSearchDateRangeInput().isDisplayed()).toBe(true);
    expect(notificationListPage.getSearchDateRangeSearchButton().isDisplayed()).toBe(true);
    expect(notificationListPage.getSearchDateRangeSearchButton().isEnabled()).toBe(true);
    expect(notificationListPage.getSearchDateRangeClearButton().isDisplayed()).toBe(true);
    expect(notificationListPage.getSearchDateRangeClearButton().isEnabled()).toBe(true);
    expect(notificationListPage.getAddButton().isDisplayed()).toBe(true);
    expect(notificationListPage.getAddButton().isEnabled()).toBe(true);

    // if list of notification is not empty, list is displayed
    if(notificationListPage.getNotificationList().isPresent() && notificationListPage.getNotificationList().isDisplayed()) {
      expect(notificationListPage.getMessageNoNotificationInList().isPresent()).toBe(false);
      expect(notificationListPage.getPanelsFromNotificationList().isPresent()).toBe(true);
      expect(notificationListPage.getPaginatorInNotificationList().isDisplayed()).toBe(true);

      let numberOfNotifications = {'value':0};
      notificationListPage.getPanelsFromNotificationList().count().then(
        function(value) {
          numberOfNotifications.value = value;
        });

      for(let i: number = 0; i<numberOfNotifications.value; i++) {
        expect(notificationListPage.getTitleFromNotificationPanel(i).isDisplayed()).toBe(true);
        expect(notificationListPage.getCreatorNameFromNotificationPanel(i).isDisplayed()).toBe(true);
        expect(notificationListPage.getDateFromNotificationPanel(i).isDisplayed()).toBe(true);
        expect(notificationListPage.getContentFromNotificationPanel(i).isDisplayed()).toBe(true);

        // if exist notification menu on panel (if logged user is creator of that notification)
        // there must be two links - for edit and delete
        if(notificationListPage.getMenuFromNotificationPanel(i).isPresent()
          && notificationListPage.getMenuFromNotificationPanel(i).isDisplayed()) {
          notificationListPage.getMenuFromNotificationPanel(i).click();

          browser.wait(function() {
            notificationListPage.getContentOfMenuFromNotificationPanel(i).isDisplayed();
          }).then(function() {
            expect(notificationListPage.getEditMenuOptionFromNotificationPanel(i).isDisplayed()).toBe(true);
            expect(notificationListPage.getDeleteMenuOptionFromNotificationPanel(i).isDisplayed()).toBe(true);
          });
        }
      }
    }
    // opposite, list is not displayed
    else {
      expect(notificationListPage.getMessageNoNotificationInList().isDisplayed()).toBe(true);
      expect(notificationListPage.getMessageNoNotificationInList().getText()).toEqual('There is still no notification on this billboard...');
    }
  });



  it('should add new notification at first position in list', () => {
    expect(notificationListPage.getNotificationListElement().isDisplayed()).toBe(true);

    // expected view of page
    expect(notificationListPage.getAddButton().isDisplayed()).toBe(true);
    expect(notificationListPage.getAddButton().isEnabled()).toBe(true);

    notificationListPage.getAddButton().click();

    browser.wait(function () {
      // expect to show modal - notification page for create new notification
      return notificationPage.getNotificationElement().isDisplayed();
    }).then(function () {
      // expect to modal has all elements
      expect(notificationPage.getNotificationElement().isDisplayed()).toBe(true);
      expect(notificationPage.getTitleInput().isDisplayed()).toBe(true);
      expect(notificationPage.getContentInput().isDisplayed()).toBe(true);
      expect(notificationPage.getSaveButton().isDisplayed()).toBe(true);
      expect(notificationPage.getSaveButton().isEnabled()).toBe(false);

      notificationPage.setTitle(newNotification.title);
      notificationPage.setContent(newNotification.content);
      notificationPage.getSaveButton().click();

      notificationListPage = new NotificationListPage();

      browser.wait(function() {
        return notificationListPage.getNotificationListElement().isDisplayed();
      }).then(function() {
        // expect that new notification is first in list include menu with edit and delete links
        expect(notificationListPage.getTitleFromNotificationPanel(0).getText()).toEqual(newNotification.title);
        expect(notificationListPage.getContentFromNotificationPanel(0).getText()).toEqual(newNotification.content);
        notificationListPage.getDateFromNotificationPanel(0).getText().then(value => {
          // difference between current time and time of new notification is less then 5sec
          let currentDate = new Date().getTime();
          newNotification.date = value;
          let notificationDate = DateConverter.convertStringToDate(value).getTime();
          expect(currentDate - notificationDate).toBeLessThan(5000);
        });
        notificationListPage.getCreatorNameFromNotificationPanel(0).getText().then(value => {
          newNotification.creator = value;
        });
      });
    });
  });



  it('should open modal view for editing previous added meeting item when user click on Edit option ' +
    'from menu in meeting item panel', () => {
    expect(notificationListPage.getMenuFromNotificationPanel(0).isDisplayed()).toBe(true);
    notificationListPage.getMenuFromNotificationPanel(0).click();

    browser.wait(function() {
      return notificationListPage.getContentOfMenuFromNotificationPanel(0).isDisplayed();
    }).then(function() {
      expect(notificationListPage.getEditMenuOptionFromNotificationPanel(0).isDisplayed()).toBe(true);
      expect(notificationListPage.getDeleteMenuOptionFromNotificationPanel(0).isDisplayed()).toBe(true);

      notificationListPage.getEditMenuOptionFromNotificationPanel(0).click();

      browser.wait(function () {
        // expect to show modal - notification page for edit selected notification
        return notificationPage.getNotificationElement().isDisplayed();
      }).then(function () {
        expect(notificationPage.getNotificationElement().isDisplayed()).toBe(true);
        expect(notificationPage.getTitleInput().isDisplayed()).toBe(true);
        expect(notificationPage.getContentInput().isDisplayed()).toBe(true);
        expect(notificationPage.getSaveButton().isDisplayed()).toBe(true);
        expect(notificationPage.getSaveButton().isEnabled()).toBe(false);

        let updatedNotification = {'title': 'Updated title', 'content': 'Updated content'};

        notificationPage.getTitleInput().clear();
        notificationPage.setTitle(updatedNotification.title);
        notificationPage.getContentInput().clear();
        notificationPage.setContent(updatedNotification.content);
        notificationPage.getSaveButton().click().then(function() {
          // expect that modal is closed and updated notification has updated values for title and content fields,
          // but date and creator fields must be same as in newNotification object
          expect(notificationListPage.getTitleFromNotificationPanel(0).getText()).toEqual(updatedNotification.title);
          expect(notificationListPage.getContentFromNotificationPanel(0).getText()).toEqual(updatedNotification.content);
          notificationListPage.getDateFromNotificationPanel(0).getText().then(value => {
            expect(value).toEqual(newNotification.date);
          });
          notificationListPage.getCreatorNameFromNotificationPanel(0).getText().then(value => {
            expect(value).toEqual(newNotification.creator);
          });
        });
      });
    });
  });



  it('should remove previous added and updated meeting item from list when user click on Delete option ' +
    'from menu in meeting item panel', () => {
    expect(notificationListPage.getMenuFromNotificationPanel(0).isDisplayed()).toBe(true);
    notificationListPage.getMenuFromNotificationPanel(0).click();

    browser.wait(function() {
      return notificationListPage.getContentOfMenuFromNotificationPanel(0).isDisplayed();
    }).then(function() {
      notificationListPage.getDeleteMenuOptionFromNotificationPanel(0).click();

      browser.wait(function() {
        // expect to show modal dialog for delete confirmation
        return confirmDeletePage.getConfirmDeleteElement().isDisplayed();
      }).then(function() {
        expect(confirmDeletePage.getConfirmDeleteElement().isDisplayed()).toBe(true);
        expect(confirmDeletePage.getConfirmButton().isDisplayed()).toBe(true);
        expect(confirmDeletePage.getConfirmButton().isEnabled()).toBe(true);
        expect(confirmDeletePage.getDeclineButton().isDisplayed()).toBe(true);
        expect(confirmDeletePage.getDeclineButton().isEnabled()).toBe(true);

        confirmDeletePage.getConfirmButton().click();

        // expect to first notification in list has date less then date of new notification (that we removed in previous line)
        // title, content, creator possibly can be equals
        notificationListPage.getDateFromNotificationPanel(0).getText().then(value => {
          let date = DateConverter.convertStringToDate(value).getTime();
          let removedNotificationDate = DateConverter.convertStringToDate(newNotification.date).getTime();
          expect(removedNotificationDate - date).toBeGreaterThan(0);
        });
      });
    });
  });
});
