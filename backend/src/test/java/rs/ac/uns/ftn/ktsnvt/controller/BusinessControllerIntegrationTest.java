package rs.ac.uns.ftn.ktsnvt.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;
import rs.ac.uns.ftn.ktsnvt.controller.util.TestUtil;
import rs.ac.uns.ftn.ktsnvt.web.dto.*;

import javax.annotation.PostConstruct;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Created by Nikola Garabandic on 06/12/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class BusinessControllerIntegrationTest {

    private static final String URL_PREFIX = "/api/businesses";

    private MediaType contentType = new MediaType(
            MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    private MockMvc mockMvc;
    private MultiValueMap<String, String> params;
    private String accessTokenTenant;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja administratora
    private String accessTokenAdmin;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja firme
    private String accessTokenCompany, accessTokenCompany1;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja institucije
    private String accessTokenInstitution, accessTokenInstitution1, accessTokenInstitution2;


    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private TestRestTemplate testRestTemplate;


    @PostConstruct
    public void setup() {
        this.mockMvc = MockMvcBuilders.
                webAppContextSetup(webApplicationContext).apply(springSecurity()).build();
    }

    // pre izvrsavanja testa, neophodno je izvrsiti logovanje u aplikaciju da bismo dobili token
    @Before
    public void login() throws Exception {

        // logovanje stanara
        LoginDTO loginDTOTenant = new LoginDTO("jasmina", "jasmina");

        ResponseEntity<TokenDTO> responseEntityTenant =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOTenant,
                        TokenDTO.class);
        // preuzmemo token za logovanog stanara iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenTenant = responseEntityTenant.getBody().getValue();


        // logovanje administratora
        LoginDTO loginDTO = new LoginDTO("ivana", "ivana");

        ResponseEntity<TokenDTO> responseEntity =
            testRestTemplate.postForEntity("/api/login",
                loginDTO,
                TokenDTO.class);
        // preuzmemo token za logovanog administratora iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenAdmin = responseEntity.getBody().getValue();


        // logovanje firme
        LoginDTO loginDTOCompany = new LoginDTO("firm2", "firm2");

        ResponseEntity<TokenDTO> responseEntityFirm =
            testRestTemplate.postForEntity("/api/login",
                loginDTOCompany,
                TokenDTO.class);
        // preuzmemo token za logovanu firmu iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenCompany = responseEntityFirm.getBody().getValue();


        // logovanje firme 2
        LoginDTO loginDTOCompany2 = new LoginDTO("firm3", "firm2");

        ResponseEntity<TokenDTO> responseEntityFirm2 =
            testRestTemplate.postForEntity("/api/login",
                loginDTOCompany2,
                TokenDTO.class);
        // preuzmemo token za logovanu firmu iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenCompany1 = responseEntityFirm2.getBody().getValue();


        // logovanje institucije
        LoginDTO loginDTOInstitution = new LoginDTO("inst2", "inst2");

        ResponseEntity<TokenDTO> responseEntityInst =
            testRestTemplate.postForEntity("/api/login",
                loginDTOInstitution,
                TokenDTO.class);
        // preuzmemo token za logovanu instituciju iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenInstitution = responseEntityInst.getBody().getValue();


        // logovanje institucije 2
        LoginDTO loginDTOInstitution2 = new LoginDTO("inst3", "inst2");

        ResponseEntity<TokenDTO> responseEntityInst2 =
            testRestTemplate.postForEntity("/api/login",
                loginDTOInstitution2,
                TokenDTO.class);
        // preuzmemo token za logovanu instituciju iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenInstitution2 = responseEntityInst2.getBody().getValue();


        // logovanje institucije 3
        LoginDTO loginDTOInstitution1 = new LoginDTO("inst1", "inst1");

        ResponseEntity<TokenDTO> responseEntityInst1 =
            testRestTemplate.postForEntity("/api/login",
                loginDTOInstitution1,
                TokenDTO.class);
        // preuzmemo token za logovanu instituciju iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenInstitution1 = responseEntityInst1.getBody().getValue();

        // za pageable atribut
        params = TestUtil.getParams(
                "0","10","ASC", "id", "");
    }

    // TESTOVI za getFirmByAddress
    @Test
    public void testGetFirmByAddress() throws Exception{
        /*
        * Test proverava ispravnost rada metode getFirmByAddress kontrolera BusinessController
        * (dobavljanje firme na osnovu adrese)
        * */

        /*
        na adresi sa parametrima: ulica = Janka Veselinovica, broj = 12, grad = Novi Sad, postanski broj = 21000
        nalazi se firma sa sledecim podacima:
            id = 100,
            name = First firm,
            description = Description for first firm,
            address_id = 102
        */
        this.mockMvc.perform(get(URL_PREFIX + "/firms/address")
                .header("Authentication-Token", this.accessTokenTenant)
                .param("street", "Janka Veselinovica")
                .param("number", "12")
                .param("city_name", "Novi Sad")
                .param("postal_number", "21000"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(100))
                .andExpect(jsonPath("$.name").value("First firm"))
                .andExpect(jsonPath("$.description").value("Description for first firm"))
                .andExpect(jsonPath("$.address.street").value("Janka Veselinovica"))
                .andExpect(jsonPath("$.address.number").value(12))
                .andExpect(jsonPath("$.address.city.name").value("Novi Sad"))
                .andExpect(jsonPath("$.address.city.postalNumber").value(21000));
    }

    @Test
    public void testGetFirmByNotAuthenticatedUser() throws Exception{
        /*
        * Test proverava ispravnost rada metode getFirmByAddress kontrolera BusinessController
        * (dobavljanje firme na osnovu adrese)
        * u slucaju akciju izvrsava nelogovani korisnik
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/firms/address")
                .param("street", "Janka Veselinovica")
                .param("number", "12")
                .param("city_name", "Novi Sad")
                .param("postal_number", "21000"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetFirmByNonexistentAddress() throws Exception{
        /*
        * Test proverava ispravnost rada metode getFirmByAddress kontrolera BusinessController
        * (dobavljanje firme na osnovu adrese)
        * u slucaju kada ne postoji zadata adresa
        * */

        // ne postoji adresa sa parametrima: ulica = Marka Veselinovica, broj = 12, grad = Novi Sad, postanski broj = 21000
        this.mockMvc.perform(get(URL_PREFIX + "/firms/address")
                .header("Authentication-Token", this.accessTokenTenant)
                .param("street", "Marka Veselinovica")
                .param("number", "12")
                .param("city_name", "Novi Sad")
                .param("postal_number", "21000"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetFirmByAddressStreetMissing() throws Exception{
        /*
        * Test proverava ispravnost rada metode getFirmByAddress kontrolera BusinessController
        * (dobavljanje firme na osnovu adrese)
        * u slucaju kada nedostaje parametar pretrage: naziv ulice
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/firms/address")
                .header("Authentication-Token", this.accessTokenTenant)
                .param("street", "")
                .param("number", "12")
                .param("city_name", "Novi Sad")
                .param("postal_number", "21000"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testGetFirmByAddressNumberMissing() throws Exception{
        /*
        * Test proverava ispravnost rada metode getFirmByAddress kontrolera BusinessController
        * (dobavljanje firme na osnovu adrese)
        * u slucaju kada nedostaje parametar pretrage: broj ulice
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/firms/address")
                .header("Authentication-Token", this.accessTokenTenant)
                .param("street", "Janka Veselinovica")
                .param("number", "")
                .param("city_name", "Novi Sad")
                .param("postal_number", "21000"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testGetFirmByAddressCityNameMissing() throws Exception{
        /*
        * Test proverava ispravnost rada metode getFirmByAddress kontrolera BusinessController
        * (dobavljanje firme na osnovu adrese)
        * u slucaju kada nedostaje parametar pretrage: naziv grada
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/firms/address")
                .header("Authentication-Token", this.accessTokenTenant)
                .param("street", "Janka Veselinovica")
                .param("number", "12")
                .param("city_name", "")
                .param("postal_number", "21000"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testGetFirmByAddressCityPostalCodeMissing() throws Exception{
        /*
        * Test proverava ispravnost rada metode getFirmByAddress kontrolera BusinessController
        * (dobavljanje firme na osnovu adrese)
        * u slucaju kada nedostaje parametar pretrage: postanski broj grada
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/firms/address")
                .header("Authentication-Token", this.accessTokenTenant)
                .param("street", "Janka Veselinovica")
                .param("number", "12")
                .param("city_name", "Novi Sad")
                .param("postal_number", ""))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za getInstitutionByAddress
    @Test
    public void testGetInstitutionByAddress() throws Exception{
        /*
        * Test proverava ispravnost rada metode getInstitutionByAddress kontrolera BusinessController
        * (dobavljanje institucije na osnovu adrese)
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/institutions/address")
                .header("Authentication-Token", this.accessTokenTenant)
                .param("street", "Janka Veselinovica")
                .param("number", "20")
                .param("city_name", "Novi Sad")
                .param("postal_number", "21000"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType));
    }

    @Test
    public void testGetInstitutionByAddressByNonAuthenticatedUser() throws Exception{
        /*
        * Test proverava ispravnost rada metode getInstitutionByAddress kontrolera BusinessController
        * (dobavljanje institucije na osnovu adrese) kada niko nije ulogovan
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/institutions/address")
                .param("street", "Janka Veselinovica")
                .param("number", "20")
                .param("city_name", "Novi Sad")
                .param("postal_number", "21000"))
                .andExpect(status().isForbidden());
    }


    // TESTOVI za registerCompany
    @Test
    @Transactional
    @Rollback(true)
    public void testRegisterCompany() throws Exception {
       /*
        * Test proverava ispravnost rada metode registerCompany kontrolera BusinessController
        * (registracija nove firme)
        * u slucaju kada akciju izvrsava administrator
        * */

        CityDTO cityDTO = new CityDTO("Novi Knezevac", 23330);
        AddressDTO addressDTO = new AddressDTO("Branka Radicevica", 68, cityDTO);
        LoginDTO loginDTO = new LoginDTO("acak", "acak");
        List<LoginDTO> loginDTOS = new ArrayList<>();
        loginDTOS.add(loginDTO);
        BusinessCreateDTO businessCreateDTO = new BusinessCreateDTO("Some company", "description", "0231545", "kaca.cukurov@gmail.com",
                "COMMUNAL_POLICE",addressDTO, loginDTOS);

        String businessCreateDTOJson = TestUtil.json(businessCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/firms")
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(businessCreateDTOJson))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").exists());
    }

    @Test
    @Transactional
    @Rollback(true)
    public void testRegisterCompanyByNotAuthenticatedUser() throws Exception {
       /*
        * Test proverava ispravnost rada metode registerCompany kontrolera BusinessController
        * (registracija nove firme)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        CityDTO cityDTO = new CityDTO("Novi Knezevac", 23330);
        AddressDTO addressDTO = new AddressDTO("Branka Radicevica", 68, cityDTO);
        LoginDTO loginDTO = new LoginDTO("acak", "acak");
        List<LoginDTO> loginDTOS = new ArrayList<>();
        loginDTOS.add(loginDTO);
        BusinessCreateDTO businessCreateDTO = new BusinessCreateDTO("Some company", "description", "0231545", "kaca.cukurov@gmail.com",
                "COMMUNAL_POLICE",addressDTO, loginDTOS);

        String businessCreateDTOJson = TestUtil.json(businessCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/firms")
                .contentType(contentType)
                .content(businessCreateDTOJson))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").exists());
    }

    @Test
    public void testRegisterCompanyByNotAuthenticatedAdmin() throws Exception {
       /*
        * Test proverava ispravnost rada metode registerCompany kontrolera BusinessController
        * (registracija nove firme)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu administratora
        * */

        CityDTO cityDTO = new CityDTO("Novi Knezevac", 23330);
        AddressDTO addressDTO = new AddressDTO("Branka Radicevica", 68, cityDTO);
        LoginDTO loginDTO = new LoginDTO("acak", "acak");
        List<LoginDTO> loginDTOS = new ArrayList<>();
        loginDTOS.add(loginDTO);
        BusinessCreateDTO businessCreateDTO = new BusinessCreateDTO("Some company", "description", "0231545", "kaca.cukurov@gmail.com",
                "COMMUNAL_POLICE",addressDTO, loginDTOS);

        String businessCreateDTOJson = TestUtil.json(businessCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/firms")
                .header("Authentication-Token", accessTokenTenant)
                .contentType(contentType)
                .content(businessCreateDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testRegisterCompanyMissingName() throws Exception {
       /*
        * Test proverava ispravnost rada metode registerCompany kontrolera BusinessController
        * (registracija nove firme)
        * u slucaju kada dolazni DTO objekat nevalidan: ne sadrzi sva neophodna polja - nedostaje polje name
        * */

        String businessCreateDTOJson = "{\"description\":\"description\","
                + "\"phoneNumber\":\"0231545\",\"email\":\"kaca.cukurov@gmail.com\","
                + "\"businessType\":\"COMMUNAL_POLICE\","
                + "\"address\":{\"street\":\"Branka Radicevica\",\"number\":68,"
                + "\"city\":{\"name\":\"Novi Knezevac\",\"postalNumber\":23330}},"
                + "\"loginAccounts\":[{\"username\":\"acak\",\"password\":\"acak\"}]}";

        this.mockMvc.perform(post(URL_PREFIX + "/firms")
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(businessCreateDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testRegisterCompanyMissingDescription() throws Exception {
       /*
        * Test proverava ispravnost rada metode registerCompany kontrolera BusinessController
        * (registracija nove firme)
        * u slucaju kada dolazni DTO objekat nevalidan: ne sadrzi sva neophodna polja - nedostaje polje description
        * */

        String businessCreateDTOJson = "{\"name\":\"some name\","
                + "\"phoneNumber\":\"0231545\",\"email\":\"kaca.cukurov@gmail.com\","
                + "\"businessType\":\"COMMUNAL_POLICE\","
                + "\"address\":{\"street\":\"Branka Radicevica\",\"number\":68,"
                + "\"city\":{\"name\":\"Novi Knezevac\",\"postalNumber\":23330}},"
                + "\"loginAccounts\":[{\"username\":\"acak\",\"password\":\"acak\"}]}";

        this.mockMvc.perform(post(URL_PREFIX + "/firms")
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(businessCreateDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testRegisterCompanyMissingPhoneNumber() throws Exception {
       /*
        * Test proverava ispravnost rada metode registerCompany kontrolera BusinessController
        * (registracija nove firme)
        * u slucaju kada dolazni DTO objekat nevalidan: ne sadrzi sva neophodna polja - nedostaje polje phoneNumber
        * */

        String businessCreateDTOJson = "{\"name\":\"some name\",\"description\":\"description\","
                + "\"email\":\"kaca.cukurov@gmail.com\","
                + "\"businessType\":\"COMMUNAL_POLICE\","
                + "\"address\":{\"street\":\"Branka Radicevica\",\"number\":68,"
                + "\"city\":{\"name\":\"Novi Knezevac\",\"postalNumber\":23330}},"
                + "\"loginAccounts\":[{\"username\":\"acak\",\"password\":\"acak\"}]}";

        this.mockMvc.perform(post(URL_PREFIX + "/firms")
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(businessCreateDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testRegisterCompanyMissingEmail() throws Exception {
       /*
        * Test proverava ispravnost rada metode registerCompany kontrolera BusinessController
        * (registracija nove firme)
        * u slucaju kada dolazni DTO objekat nevalidan: ne sadrzi sva noephodna polja - nedostaje polje email
        * */

        String businessCreateDTOJson = "{\"name\":\"some name\",\"description\":\"description\","
                + "\"phoneNumber\":\"0231545\","
                + "\"businessType\":\"COMMUNAL_POLICE\","
                + "\"address\":{\"street\":\"Branka Radicevica\",\"number\":68,"
                + "\"city\":{\"name\":\"Novi Knezevac\",\"postalNumber\":23330}},"
                + "\"loginAccounts\":[{\"username\":\"acak\",\"password\":\"acak\"}]}";

        this.mockMvc.perform(post(URL_PREFIX + "/firms")
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(businessCreateDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testRegisterCompanyMissingBusinessType() throws Exception {
       /*
        * Test proverava ispravnost rada metode registerCompany kontrolera BusinessController
        * (registracija nove firme)
        * u slucaju kada dolazni DTO objekat nevalidan: ne sadrzi sva neophodna polja - nedostaje polje businessType
        * */

        String businessCreateDTOJson = "{\"name\":\"some name\",\"description\":\"description\","
                + "\"phoneNumber\":\"0231545\",\"email\":\"kaca.cukurov@gmail.com\","
                + "\"address\":{\"street\":\"Branka Radicevica\",\"number\":68,"
                + "\"city\":{\"name\":\"Novi Knezevac\",\"postalNumber\":23330}},"
                + "\"loginAccounts\":[{\"username\":\"acak\",\"password\":\"acak\"}]}";

        this.mockMvc.perform(post(URL_PREFIX + "/firms")
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(businessCreateDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testRegisterCompanyMissingAddress() throws Exception {
       /*
        * Test proverava ispravnost rada metode registerCompany kontrolera BusinessController
        * (registracija nove firme)
        * u slucaju kada dolazni DTO objekat nevalidan: ne sadrzi sva neophodna polja - nedostaje polje addressDTO
        * */

        String businessCreateDTOJson = "{\"name\":\"some name\",\"description\":\"description\","
                + "\"phoneNumber\":\"0231545\",\"email\":\"kaca.cukurov@gmail.com\","
                + "\"businessType\":\"COMMUNAL_POLICE\","
                + "\"loginAccounts\":[{\"username\":\"acak\",\"password\":\"acak\"}]}";

        this.mockMvc.perform(post(URL_PREFIX + "/firms")
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(businessCreateDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testRegisterCompanyMissingCity() throws Exception {
       /*
        * Test proverava ispravnost rada metode registerCompany kontrolera BusinessController
        * (registracija nove firme)
        * u slucaju kada dolazni DTO objekat nevalidan: ne sadrzi sva neophodna polja - nedostaje polje cityDTO
        * */

        String businessCreateDTOJson = "{\"name\":\"some name\",\"description\":\"description\","
                + "\"phoneNumber\":\"0231545\",\"email\":\"kaca.cukurov@gmail.com\","
                + "\"businessType\":\"COMMUNAL_POLICE\","
                + "\"address\":{\"street\":\"Branka Radicevica\",\"number\":68},"
                + "\"loginAccounts\":[{\"username\":\"acak\",\"password\":\"acak\"}]}";

        this.mockMvc.perform(post(URL_PREFIX + "/firms")
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(businessCreateDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testRegisterCompanyUsernameTaken() throws Exception {
       /*
        * Test proverava ispravnost rada metode registerCompany kontrolera BusinessController
        * (registracija nove firme)
        * u slucaju kada dolazni DTO objekat nevalidan: sadrzi sva neophodna polja, ali je bar jedno polje username
        * takvo da je to korisnicko ime vec zauzeto
        * */

        String businessCreateDTOJson = "{\"name\":\"some name\",\"description\":\"description\","
                + "\"phoneNumber\":\"0231545\",\"email\":\"kaca.cukurov@gmail.com\","
                + "\"businessType\":\"COMMUNAL_POLICE\","
                + "\"address\":{\"street\":\"Branka Radicevica\",\"number\":68,"
                + "\"city\":{\"name\":\"Novi Knezevac\",\"postalNumber\":23330}},"
                + "\"loginAccounts\":[{\"username\":\"kaca\",\"password\":\"acak\"}]}";

        this.mockMvc.perform(post(URL_PREFIX + "/firms")
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(businessCreateDTOJson))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za updateBusiness
    @Test
    @Transactional
    @Rollback(true)
    public void  testUpdateBusiness() throws Exception{
        /*
        * Test proverava ispravnost rada metode updateBusiness kontrolera BusinessController
        * (izmena podataka firme/institucije)
        * */

        CityDTO cityDTO = new CityDTO("Novi Sad", 21000);
        AddressDTO addressDTO = new AddressDTO("Srpska", 62, cityDTO);
        BusinessUpdateDTO businessUpdateDTO = new BusinessUpdateDTO("some name", "some description",
            "123150", "kaca.cukurov@gmail.com", addressDTO, null, null, null);

        String token = TestUtil.json(businessUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/101")
                .header("Authentication-Token", accessTokenCompany)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(101))
                .andExpect(jsonPath("$.name").value("some name"))
                .andExpect(jsonPath("$.description").value("some description"))
                .andExpect(jsonPath("$.phoneNumber").value(123150))
                .andExpect(jsonPath("$.address.street").value("Srpska"))
                .andExpect(jsonPath("$.address.number").value(62))
                .andExpect(jsonPath("$.address.city.name").value("Novi Sad"))
                .andExpect(jsonPath("$.address.city.postalNumber").value(21000));
    }

    @Test
    public void testUpdateBusinessByNotAuthenticatedUser() throws Exception{
        /*
        * Test proverava ispravnost rada metode updateBusiness kontrolera BusinessController
        * (izmena podataka firme/institucije)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        CityDTO cityDTO = new CityDTO("Novi Sad", 21000);
        AddressDTO addressDTO = new AddressDTO("Srpska", 62, cityDTO);
        BusinessUpdateDTO businessUpdateDTO = new BusinessUpdateDTO("some name", "some description",
            "123150", "kaca.cukurov@gmail.com", addressDTO, null, null, null);

        String token = TestUtil.json(businessUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/101")
                .contentType(contentType)
                .content(token))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testUpdateBusinessByNotAuthenticatedFirmOrInstitution() throws Exception{
        /*
        * Test proverava ispravnost rada metode updateBusiness kontrolera BusinessController
        * (izmena podataka firme/institucije)
        * u slucaju kada logovani korisnik nema ulogu firme niti institucije
        * */

        CityDTO cityDTO = new CityDTO("Novi Sad", 21000);
        AddressDTO addressDTO = new AddressDTO("Srpska", 62, cityDTO);
        BusinessUpdateDTO businessUpdateDTO = new BusinessUpdateDTO("some name", "some description",
            "123150", "kaca.cukurov@gmail.com", addressDTO, null, null, null);

        String token = TestUtil.json(businessUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/101")
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testUpdateBusinessNonexistentFirmOrInstitution() throws Exception{
        /*
        * Test proverava ispravnost rada metode updateBusiness kontrolera BusinessController
        * (izmena podataka firme/institucije)
        * u slucaju kada ne postoji firma ili institucija sa zadatim ID-em
        * */

        CityDTO cityDTO = new CityDTO("Novi Sad", 21000);
        AddressDTO addressDTO = new AddressDTO("Srpska", 62, cityDTO);
        BusinessUpdateDTO businessUpdateDTO = new BusinessUpdateDTO("some name", "some description",
            "123150", "kaca.cukurov@gmail.com", addressDTO, null, null, null);

        String token = TestUtil.json(businessUpdateDTO);

        // ne postoji firma niti institucija sa ID-em = 200
        this.mockMvc.perform(put(URL_PREFIX + "/200")
                .header("Authentication-Token", accessTokenCompany)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testUpdateBusinessInvalidFirmOrInstitution() throws Exception{
        /*
        * Test proverava ispravnost rada metode updateBusiness kontrolera BusinessController
        * (izmena podataka firme/institucije)
        * u slucaju kada trenutno logovana firma/institucija nema isti ID kao sto je zadat ID u DTO-u
        * */

        CityDTO cityDTO = new CityDTO("Novi Sad", 21000);
        AddressDTO addressDTO = new AddressDTO("Srpska", 62, cityDTO);
        BusinessUpdateDTO businessUpdateDTO = new BusinessUpdateDTO("some name", "some description",
            "123150", "kaca.cukurov@gmail.com", addressDTO, null, null, null);

        String token = TestUtil.json(businessUpdateDTO);

        // trenutno je logovana firma sa ID-em = 101
        this.mockMvc.perform(put(URL_PREFIX + "/105")
                .header("Authentication-Token", accessTokenCompany)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testUpdateBusinessMissingName() throws Exception{
        /*
        * Test proverava ispravnost rada metode updateBusiness kontrolera BusinessController
        * (izmena podataka firme/institucije)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne sadrzi sve neophodne podatke - nedostaje polje name
        * */

        // nedostaje polje name
        String token = "{\"description\":\"some description\","
            + "\"phoneNumber\":\"123150\",\"email\":\"kaca.cukurov@gmail.com\","
            + "\"address\":{\"street\":\"Srpska\",\"number\":62,"
            + "\"city\":{\"name\":\"Novi Sad\",\"postalNumber\":21000}}}";

        this.mockMvc.perform(put(URL_PREFIX + "/101")
                .header("Authentication-Token", accessTokenCompany)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateBusinessMissingDescription() throws Exception{
        /*
        * Test proverava ispravnost rada metode updateBusiness kontrolera BusinessController
        * (izmena podataka firme/institucije)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne sadrzi sve neophodne podatke - nedostaje polje description
        * */

        // nedostaje polje description
        String token = "{\"name\":\"some name\","
            + "\"phoneNumber\":\"123150\",\"email\":\"kaca.cukurov@gmail.com\","
            + "\"address\":{\"street\":\"Srpska\",\"number\":62,"
            + "\"city\":{\"name\":\"Novi Sad\",\"postalNumber\":21000}}}";

        this.mockMvc.perform(put(URL_PREFIX + "/101")
                .header("Authentication-Token", accessTokenCompany)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateBusinessMissingPhoneNumber() throws Exception{
        /*
        * Test proverava ispravnost rada metode updateBusiness kontrolera BusinessController
        * (izmena podataka firme/institucije)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne sadrzi sve neophodne podatke - nedostaje polje number
        * */

        // nedostaje polje number
        String token = "{\"name\":\"some name\",\"description\":\"some description\","
            + "\"email\":\"kaca.cukurov@gmail.com\","
            + "\"address\":{\"street\":\"Srpska\",\"number\":62,"
            + "\"city\":{\"name\":\"Novi Sad\",\"postalNumber\":21000}}}";

        this.mockMvc.perform(put(URL_PREFIX + "/101")
                .header("Authentication-Token", accessTokenCompany)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateBusinessMissingEmail() throws Exception{
        /*
        * Test proverava ispravnost rada metode updateBusiness kontrolera BusinessController
        * (izmena podataka firme/institucije)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne sadrzi sve neophodne podatke - nedostaje polje email
        * */

        // nedostaje polje email
        String token = "{\"name\":\"some name\",\"description\":\"some description\","
            + "\"phoneNumber\":\"123150\","
            + "\"address\":{\"street\":\"Srpska\",\"number\":62,"
            + "\"city\":{\"name\":\"Novi Sad\",\"postalNumber\":21000}}}";

        this.mockMvc.perform(put(URL_PREFIX + "/101")
                .header("Authentication-Token", accessTokenCompany)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateBusinessMissingAddress() throws Exception{
        /*
        * Test proverava ispravnost rada metode updateBusiness kontrolera BusinessController
        * (izmena podataka firme/institucije)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne sadrzi sve neophodne podatke - nedostaje polje addressDTO
        * */

        // nedostaje polje addressDTO
        String token = "{\"name\":\"some name\",\"description\":\"some description\","
            + "\"phoneNumber\":\"123150\",\"email\":\"kaca.cukurov@gmail.com\"}";

        this.mockMvc.perform(put(URL_PREFIX + "/101")
                .header("Authentication-Token", accessTokenCompany)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateBusinessMissingCity() throws Exception{
        /*
        * Test proverava ispravnost rada metode updateBusiness kontrolera BusinessController
        * (izmena podataka firme/institucije)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne sadrzi sve neophodne podatke - nedostaje polje cityDTO
        * */

        // nedostaje polje cityDTO
        String token = "{\"name\":\"some name\",\"description\":\"some description\","
            + "\"phoneNumber\":\"123150\",\"email\":\"kaca.cukurov@gmail.com\","
            + "\"address\":{\"street\":\"Srpska\",\"number\":62}}";

        this.mockMvc.perform(put(URL_PREFIX + "/101")
                .header("Authentication-Token", accessTokenCompany)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateBusinessInappropriateTypeOfName() throws Exception{
        /*
        * Test proverava ispravnost rada metode updateBusiness kontrolera BusinessController
        * (izmena podataka firme/institucije)
        * u slucaju kada je dolazni DTO objekat nevalidan: sadrzi sve neophodne podatke, ali je polje name nevalidnog tipa
        * */

        String token = "{\"name\":012,\"description\":\"some description\","
            + "\"phoneNumber\":\"123150\",\"email\":\"kaca.cukurov@gmail.com\","
            + "\"address\":{\"street\":\"Srpska\",\"number\":62,"
            + "\"city\":{\"name\":\"Novi Sad\",\"postalNumber\":21000}}}";

        this.mockMvc.perform(put(URL_PREFIX + "/101")
                .header("Authentication-Token", accessTokenCompany)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za confirmBusiness
    @Test
    @Transactional
    @Rollback(true)
    public void testConfirmBusiness() throws Exception{
        /*
        * Test proverava ispravnost rada metode confirmBusiness kontrolera BusinessController
        * (potvrda registracije firme/institucije od strane same administratora)
        * */

        // registracija firme sa ID-em = 106 nije potvrdjena
        this.mockMvc.perform(patch(URL_PREFIX + "/106/confirm")
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType))
                .andExpect(status().isOk());
    }

    @Test
    public void testConfirmBusinessByNotAuthenticatedUser() throws Exception{
        /*
        * Test proverava ispravnost rada metode confirmBusiness kontrolera BusinessController
        * (potvrda registracije firme/institucije od strane neulogovanog korisnika)
        * u slucaju kada akciju zivrsava nelogovani korisnik
        * */

        this.mockMvc.perform(patch(URL_PREFIX + "/106/confirm")
                .contentType(contentType))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testConfirmBusinessByNotAuthenticatedAdministrator() throws Exception{
        /*
        * Test proverava ispravnost rada metode confirmBusiness kontrolera BusinessController
        * (potvrda registracije firme/institucije od strane korisnika koji nema dozvolu, npr.firma)
        * u slucaju kada akciju zivrsava logovani korisnik koji nema ulogu administratora
        * */

        this.mockMvc.perform(patch(URL_PREFIX + "/106/confirm")
                .header("Authentication-Token", accessTokenCompany)
                .contentType(contentType))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testConfirmBusinessNonexistentFirmOrInstitution() throws Exception{
        /*
        * Test proverava ispravnost rada metode confirmBusiness kontrolera BusinessController
        * (potvrda registracije firme/institucije od strane administratora)
        * u slucaju kada ne postoji firma/institucija sa zadatim ID-em
        * */

        // ne postoji firma/institucija sa ID-em = 200
        this.mockMvc.perform(patch(URL_PREFIX + "/200/confirm")
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testConfirmBusinessAlreadyConfirmed() throws Exception{
        /*
        * Test proverava ispravnost rada metode confirmBusiness kontrolera BusinessController
        * (potvrda registracije firme.institucije od strane administratora)
        * u slucaju kada je registracija firme/institucije sa zadatim ID-em vec potvrdjena
        * */

        // registracija firme sa ID-em = 100 je vec potvrdjena
        this.mockMvc.perform(patch(URL_PREFIX + "/100/confirm")
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za declareBusinessInactive
    @Test
    @Transactional
    @Rollback(true)
    public void testDeclareBusinessInactive() throws Exception{
        /*
        * Test proverava ispravnost rada metode declareBusinessInactive kontrolera BusinessController
        * (proglasenje firme/institucije neaktivnom)
        * */

        // institucija sa ID-em = 104 je aktivna
        this.mockMvc.perform(patch(URL_PREFIX + "/104/deactivate")
                .header("Authentication-Token", accessTokenInstitution2))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeclareBusinessInactiveByNotAuthenticatedUser() throws Exception{
        /*
        * Test proverava ispravnost rada metode declareBusinessInactive kontrolera BusinessController
        * (proglasenje firme/institucije neaktivnom)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        this.mockMvc.perform(patch(URL_PREFIX + "/104/deactivate"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDeclareBusinessInactiveByNotAuthenticatedFirmOrInstitution() throws Exception{
        /*
        * Test proverava ispravnost rada metode declareBusinessInactive kontrolera BusinessController
        * (proglasenje firme/institucije neaktivnom)
        * u slucaju kada trenutno logovani korisnik nema ulogu firme niti institucije
        * */

        this.mockMvc.perform(patch(URL_PREFIX + "/104/deactivate")
                .header("Authentication-Token", accessTokenTenant))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDeclareBusinessInactiveNonexistentFirmOrInstitution() throws Exception{
        /*
        * Test proverava ispravnost rada metode declareBusinessInactive kontrolera BusinessController
        * (proglasenje firme/institucije neaktivnom)
        * u slucaju kada ne postoji firma/institucija sa zadatim ID-em
        * */

        // ne postoji firma/institucija sa ID-em = 200
        this.mockMvc.perform(patch(URL_PREFIX + "/200/deactivate")
                .header("Authentication-Token", accessTokenInstitution2))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeclareBusinessInactiveInvalidFirmOrInstitution() throws Exception{
        /*
        * Test proverava ispravnost rada metode declareBusinessInactive kontrolera BusinessController
        * (proglasenje firme/institucije neaktivnom)
        * u slucaju kada trenutno logovana firma/institucija nema isti ID kao sto je zadati ID
        * */

        // trenutno je logovana institucija sa ID-em = 104
        this.mockMvc.perform(patch(URL_PREFIX + "/106/deactivate")
                .header("Authentication-Token", accessTokenInstitution2))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDeclareBusinessInactiveInstitutionIsResponsibleForDamages() throws Exception{
        /*
        * Test proverava ispravnost rada metode declareBusinessInactive kontrolera BusinessController
        * (proglasenje firme/institucije neaktivnom)
        * u slucaju kada je institucija sa zadatim ID-em odgovorna za neke kvarove koji jos uvek nisu reseni
        * */

        // institucija sa ID-em = 103 je odgovorna za kvar sa ID-em = 103 koji jos uvek nije otklonjen
        this.mockMvc.perform(patch(URL_PREFIX + "/103/deactivate")
                .header("Authentication-Token", accessTokenInstitution))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testDeclareBusinessInactiveInstitutionHasDamagesToFix() throws Exception{
        /*
        * Test proverava ispravnost rada metode declareBusinessInactive kontrolera BusinessController
        * (proglasenje firme/institucije neaktivnom)
        * u slucaju kada je institucija sa zadatim ID-em izabrana za otklanjanje nekih kvarova koji jos uvek nisu reseni
        * */

        // institucija sa ID-em = 102 je izabrana da resi kvar sa ID-em = 104 koji jos uvek nije otklonjen
        this.mockMvc.perform(patch(URL_PREFIX + "/102/deactivate")
                .header("Authentication-Token", accessTokenInstitution1))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za declareBusinessActive
    @Test
    @Transactional
    @Rollback(true)
    public void testDeclareBusinessActive() throws Exception{
        /*
        * Test proverava ispravnost rada metode declareBusinessActive kontrolera BusinessController
        * (proglasenje firme/institucije aktivnom)
        * */

        // firma sa ID-em = 105 je neaktivna
        this.mockMvc.perform(patch(URL_PREFIX + "/105/activate")
                .header("Authentication-Token", accessTokenCompany1))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeclareBusinessActiveByNotAuthenticatedUser() throws Exception{
        /*
        * Test proverava ispravnost rada metode declareBusinessActive kontrolera BusinessController
        * (proglasenje firme/institucije aktivnom)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        this.mockMvc.perform(patch(URL_PREFIX + "/105/activate"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDeclareBusinessActiveByNotAuthenticatedFirmOrInstitution() throws Exception{
        /*
        * Test proverava ispravnost rada metode declareBusinessActive kontrolera BusinessController
        * (proglasenje firme/institucije aktivnom)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu firme niti institucije
        * */

        this.mockMvc.perform(patch(URL_PREFIX + "/105/activate")
                .header("Authentication-Token", accessTokenTenant))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDeclareBusinessActiveInvalidFirmOrInstitution() throws Exception{
        /*
        * Test proverava ispravnost rada metode declareBusinessActive kontrolera BusinessController
        * (proglasenje firme/institucije aktivnom)
        * u slucaju kada trenutno logovana firma/institucija nema isti ID kao sto je zadati ID
        * */

        // trenutno je logovana firma sa ID-em = 101
        this.mockMvc.perform(patch(URL_PREFIX + "/105/activate")
                .header("Authentication-Token", accessTokenCompany))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDeclareBusinessActiveNonexistentFirmOrInstitution() throws Exception{
        /*
        * Test proverava ispravnost rada metode declareBusinessActive kontrolera BusinessController
        * (proglasenje firme/institucije aktivnom)
        * u slucaju kada ne postoji neaktivna institucija sa zadatim ID-em
        * */

        // ne postoji neaktivna firma/institucija sa ID-em = 200
        this.mockMvc.perform(patch(URL_PREFIX + "/200/activate")
                .header("Authentication-Token", accessTokenCompany1))
                .andExpect(status().isNotFound());
    }

    // TESTOVI za updateBusinessAccounts
    @Test
    @Transactional
    @Rollback(true)
    public void testUpdateBusinessAccounts() throws Exception{
        /*
        * Test proverava ispravnost rada metode updateBusinessAccounts kontrolera BusinessController
        * (izmena naloga firme/institucije)
        * */

        String token = "{\"loginAccounts\":[{\"username\":\"inst2\", \"password\":\"nova\"}]}";

        this.mockMvc.perform(patch(URL_PREFIX + "/103/edit_accounts")
                .header("Authentication-Token", accessTokenInstitution)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isOk());
    }

    @Test
    public void testUpdateBusinessAccountsByNotAuthenticatedUser() throws Exception{
        /*
        * Test proverava ispravnost rada metode updateBusinessAccounts kontrolera BusinessController
        * (izmena naloga firme/institucije)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        String token = "{\"loginAccounts\":[{\"username\":\"inst2\", \"password\":\"nova\"}]}";

        this.mockMvc.perform(patch(URL_PREFIX + "/103/edit_accounts")
                .contentType(contentType)
                .content(token))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testUpdateBusinessAccountsByNotAuthenticatedFirmOrInstitution() throws Exception{
        /*
        * Test proverava ispravnost rada metode updateBusinessAccounts kontrolera BusinessController
        * (izmena naloga firme/institucije)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu firme ili institucije
        * */

        String token = "{\"loginAccounts\":[{\"username\":\"inst2\", \"password\":\"nova\"}]}";

        this.mockMvc.perform(patch(URL_PREFIX + "/103/edit_accounts")
                .header("Authentication-Token", accessTokenTenant)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testUpdateBusinessAccountsNonexistentFirmOrInstitution() throws Exception{
        /*
        * Test proverava ispravnost rada metode updateBusinessAccounts kontrolera BusinessController
        * (izmena naloga firme/institucije)
        * u slucaju kada ne postoji firma/institucija sa zadatim ID-em
        * */

        String token = "{\"loginAccounts\":[{\"username\":\"inst2\", \"password\":\"nova\"}]}";

        // ne postoji firma/institucija sa ID-em = 200
        this.mockMvc.perform(patch(URL_PREFIX + "/200/edit_accounts")
                .header("Authentication-Token", accessTokenInstitution)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testUpdateBusinessAccountsListLoginAccountsIsEmpty() throws Exception{
        /*
        * Test proverava ispravnost rada metode updateBusinessAccounts kontrolera BusinessController
        * (izmena naloga firme/institucije)
        * u slucaju kada je dolazni DTO objekat nevalidan: sadrzi sva neophodna polja, ali je polje loginAccounts prazna lista
        * */

        // lista loginAccounts je prazna
        String token = "{\"loginAccounts\":[]}";

        this.mockMvc.perform(patch(URL_PREFIX + "/103/edit_accounts")
                .header("Authentication-Token", accessTokenInstitution)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateBusinessAccountsTakenUsername() throws Exception{
        /*
        * Test proverava ispravnost rada metode updateBusinessAccounts kontrolera BusinessController
        * (izmena naloga firme/institucije)
        * u slucaju kada je dolazni DTO objekat nevalidan: sadrzi sva neophodna polja, ali u listi loginAccounts postoji
        * bar jedan nalog koji poseduje korisnicko ime koje je vec zauzeto
        * */

        String token = "{\"loginAccounts\":[{\"username\":\"inst2\", \"password\":\"nova\"},{\"username\":\"firm2\", \"password\":\"firm2\"}]}";

        this.mockMvc.perform(patch(URL_PREFIX + "/103/edit_accounts")
                .header("Authentication-Token", accessTokenInstitution)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateBusinessAccountsMissingUsername() throws Exception{
        /*
        * Test proverava ispravnost rada metode updateBusinessAccounts kontrolera BusinessController
        * (izmena naloga firme/institucije)
        * u slucaju kada je dolazni DTO objekat nevalidan: sadrzi sva neophodna polja, ali u listi loginAccounts postoji
        * bar jedan nalog koji ne poseduje polje username
        * */

        String token = "{\"loginAccounts\":[{\"username\":\"inst2\", \"password\":\"nova\"},{\"password\":\"firm2\"}]}";

        this.mockMvc.perform(patch(URL_PREFIX + "/103/edit_accounts")
                .header("Authentication-Token", accessTokenTenant)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateBusinessAccountsMissingPassword() throws Exception{
        /*
        * Test proverava ispravnost rada metode updateBusinessAccounts kontrolera BusinessController
        * (izmena naloga firme/institucije)
        * u slucaju kada je dolazni DTO objekat nevalidan: sadrzi sva neophodna polja, ali u listi loginAccounts postoji
        * bar jedan nalog koji ne poseduje polje password
        * */

        String token = "{\"loginAccounts\":[{\"username\":\"inst2\", \"password\":\"nova\"},{\"username\":\"nova\"}]}";

        this.mockMvc.perform(patch(URL_PREFIX + "/103/edit_accounts")
                .header("Authentication-Token", accessTokenTenant)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za getAllBusinesses
    @Test
    public void testGetAllBusinesses() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllBusinesses kontrolera BusinessController
        * (dobavljanje liste svih firmi/institucija)
        * */

        /*
        postoji 6 firmi i institucija (3 firme 3 institucije) sa sledecim podacima:
            1 - id = 100,
                name = First firm,
                description = Description for first firm
            2 - id = 101,
                name = Second firm,
                description = Description for first firm
            3 - id = 106,
                name = Fourth firm,
                description = Description for fourth firm
            4 - id = 102,
                name = First institution,
                description = Description for first institution
            5 - id = 103,
                name = Second institution,
                description = Description for second institution
            6 - id = 104,
                name = Third institution,
                description = Description for third institution
         */

        this.mockMvc.perform(get(URL_PREFIX)
                .header("Authentication-Token", this.accessTokenAdmin)
                .params(params))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.[*]", hasSize(5)))
                .andExpect(jsonPath("$.[*].id", containsInAnyOrder(100,101,102,103,104)));
    }

    @Test
    public void testGetAllBusinessesByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllBusinesses kontrolera BusinessController
        * (dobavljanje liste svih firmi/institucija)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        this.mockMvc.perform(get(URL_PREFIX)
                .params(params))
                .andExpect(status().isForbidden());
    }

    // TESTOVI za getAllBusinessForOffer
    @Test
    public void testGetAllBusinessesForOffer() throws Exception{
        /*
        * Test proverava ispravnost rada metode getAllBusinessesForOffer kontrolera BusinessController
        * (dobavljanje liste svih firmi/institucija koje su poslale ponudu za dati kvar)
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/damages/101/offer_for_damage_sent")
                    .header("Authentication-Token", this.accessTokenTenant)
                    .params(params))
                    .andExpect(status().isOk());
    }

    // TESTOVI za getAllInstitutions
    @Test
    public void testGetAllInstitutions() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllInstitutions kontrolera BusinessController
        * (dobavljanje liste svih institucija) kada su svi parametri validni
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/institutions")
                .header("Authentication-Token", this.accessTokenAdmin)
                .params(params))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.[*]", hasSize(3)));
    }


    @Test
    public void testGetAllInstitutionsByUnauthanticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllInstitutions kontrolera BusinessController
        * (dobavljanje liste svih institucija) kada niko nije ulogovan
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/institutions")
                .params(params))
                .andExpect(status().isForbidden());
    }

    // TESTOVI za getAllFirms
    @Test
    public void testGetAllFirms() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllFirms kontrolera BusinessController
        * (dobavljanje liste svih firmi)
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/firms")
                .header("Authentication-Token", this.accessTokenAdmin)
                .params(params))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.[*]", hasSize(3)));
    }

    @Test
    public void testGetAllFirmsByUnauthanticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllFirms kontrolera BusinessController
        * (dobavljanje liste svih institucija) kada niko nije ulogovan
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/firms")
                .params(params))
                .andExpect(status().isForbidden());
    }


    // TESTOVI za getBusiness
    @Test
    public void testGetBusiness() throws Exception {
        /*
        * Test proverava ispravnost rada metode getBusiness kontrolera BusinessController
        * (dobavljanje konkretne firme/institucije na osnovu zadatog ID-a)
        * */

        /*
        firma sa ID-em = 100 ima sledece podatke:
            id = 100,
            name = first firm,
            description = Description for first firm
        */
        this.mockMvc.perform(get(URL_PREFIX + "/100")
                .header("Authentication-Token", this.accessTokenAdmin))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(100))
                .andExpect(jsonPath("$.name").value("First firm"))
                .andExpect(jsonPath("$.description").value("Description for first firm"));
    }

    @Test
    public void testGetBusinessByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode getBusiness kontrolera BusinessController
        * (dobavljanje konkretne firme/institucije na osnovu zadatog ID-a)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/100"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetBusinessNonexistentFirmOrInstitution() throws Exception {
        /*
        * Test proverava ispravnost rada metode getBusiness kontrolera BusinessController
        * (dobavljanje konkretne firme/institucije na osnovu zadatog ID-a)
        * u slucaju kada ne postoji firma/institucija sa zadatim ID-em
        * */

        // ne postoji firma/institucija sa ID-em = 200
        this.mockMvc.perform(get(URL_PREFIX + "/200")
                .header("Authentication-Token", this.accessTokenAdmin))
                .andExpect(status().isNotFound());
    }

    // TESTOVI za deleteBusiness
    @Test
    @Transactional
    @Rollback(true)
    public void testDeleteBusiness() throws Exception {
        /*
        * Test proverava ispravnost rada metode deleteBusiness kontrolera BusinessController
        * (brisanje konkretne firme/institucije na osnovu zadatog ID-a)
        * */

        /*
        institucija sa ID-em nije odgovorna ni za kakve neresene kvarove, nema odgovornost ni za kakve tipove kvarova
        u nekim zgradama, niti je izabrana za resavanje nekih kvarova koji jos uvek nisu reseni
        */
        this.mockMvc.perform(delete(URL_PREFIX + "/104")
                .header("authentication-Token", this.accessTokenAdmin))
                .andExpect(status().isOk());
        // uklonjeni resurs se ne nalazi u bazi
        this.mockMvc.perform(get(URL_PREFIX + "/104")
                .header("authentication-Token", this.accessTokenAdmin))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeleteBusinessByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode deleteBusiness kontrolera BusinessController
        * (brisanje konkretne firme/institucije na osnovu zadatog ID-a)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        this.mockMvc.perform(delete(URL_PREFIX + "/104"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDeleteBusinessByNotAuthenticatedAdministrator() throws Exception {
        /*
        * Test proverava ispravnost rada metode deleteBusiness kontrolera BusinessController
        * (brisanje konkretne firme/institucije na osnovu zadatog ID-a)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu administratora
        * */

        this.mockMvc.perform(delete(URL_PREFIX + "/104")
                .header("authentication-Token", this.accessTokenCompany))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDeleteBusinessNonexistentFirmOrInstitution() throws Exception {
        /*
        * Test proverava ispravnost rada metode deleteBusiness kontrolera BusinessController
        * (brisanje konkretne firme/institucije na osnovu zadatog ID-a)
        * u slucaju kada ne postoji firma/institucija sa zadatim ID-em
        * */

        // ne postoji firma/institucija sa ID-em = 200
        this.mockMvc.perform(delete(URL_PREFIX + "/200")
                .header("authentication-Token", this.accessTokenAdmin))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeleteBusinessResponsibleForSomeNotFixedDamages() throws Exception {
        /*
        * Test proverava ispravnost rada metode deleteBusiness kontrolera BusinessController
        * (brisanje konkretne firme/institucije na osnovu zadatog ID-a)
        * u slucaju kada je intitucija sa zadatim ID-em odgovorna za neke kvarove koji jos uvek nisu reseni
        * */

        // institucija sa ID-em = 103 je odgovorna za kvar sa ID-em = 103 koji jos uvek nije resen
        this.mockMvc.perform(delete(URL_PREFIX + "/103")
                .header("authentication-Token", this.accessTokenAdmin))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testDeleteBusinessResponsibleForSomeDamageTypes() throws Exception {
        /*
        * Test proverava ispravnost rada metode deleteBusiness kontrolera BusinessController
        * (brisanje konkretne firme/institucije na osnovu zadatog ID-a)
        * u slucaju kada je institucija sa zadatim ID-em odgovorna za neke tipove kvarova u nekim zgradama
        * */

        // institucija sa ID-em = 102 je odgovorna za tip kvara sa ID-em = 1 u zgradi sa ID-em = 100
        this.mockMvc.perform(delete(URL_PREFIX + "/102")
                .header("authentication-Token", this.accessTokenAdmin))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testDeleteBusinessSelectedForRepairSomeNotFixedDamages() throws Exception {
        /*
        * Test proverava ispravnost rada metode deleteBusiness kontrolera BusinessController
        * (brisanje konkretne firme/institucije na osnovu zadatog ID-a)
        * u slucaju kada je firma/institucija sa zadatim ID-em izabrana za resavanje nekih kvarova koji jos uvek nisu reseni
        * */

        // firma sa ID-em = 101 je izabrana za resavanje kvara sa ID-em = 103 koji jos uvek nije resen
        this.mockMvc.perform(delete(URL_PREFIX + "/101")
                .header("authentication-Token", this.accessTokenAdmin))
                .andExpect(status().isBadRequest());
    }
 }
