import { NgModule } from '@angular/core';
// module
import { SharedModule } from "../shared/shared.module";
import { DamageRequestsRouterModule } from "./damage-requests-router.module";
// component
import { DamageRequestsComponent } from "./damage-requests/damage-requests.component";
import { OfferModalComponent } from "./offer-modal/offer-modal.component";


@NgModule({
  imports: [
    SharedModule,
    DamageRequestsRouterModule
  ],
  declarations: [
    DamageRequestsComponent,
    OfferModalComponent
  ],
  entryComponents: [
    OfferModalComponent
  ]
})
export class DamageRequestsModule { }
