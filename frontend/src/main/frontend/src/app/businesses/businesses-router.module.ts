import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";
// component
import { BusinessesListComponent } from "./businesses-list/businesses-list.component";
import { BusinessComponent } from "./business/business.component";

const routes: Routes = [
  { path: '', component: BusinessesListComponent },
  { path: ':id', component: BusinessComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class BusinessesRouterModule { }
