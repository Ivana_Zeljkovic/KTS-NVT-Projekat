import {by, element, ElementArrayFinder, ElementFinder} from "protractor";

export class QuestionnaireListPage {
  // get methods
  getQuestionnaireListElement(): ElementFinder {
    return element(by.tagName('app-questionnaire-list'));
  }

  getTitle(): ElementFinder {
    return element(by.css('div.container > div.title'));
  }

  getQuestionnaires(): ElementArrayFinder {
    return element.all(by.css('#questionnaireList .panel'));
  }

  getCreatorFromQuestionnairePanel(index: number): ElementFinder {
    return element(by.css(`#questionnaireList .panel:nth-child(${index+1}) .meeting-item-metadata span:nth-child(2)`));
  }

  getDateFromQuestionnairePanel(index: number): ElementFinder {
    return element(by.css(`#questionnaireList .panel:nth-child(${index+1}) .meeting-item-metadata span:nth-child(4)`));
  }

  getMenuFromQuestionnairePanel(index: number): ElementFinder {
    return element(by.css(`#questionnaireList .panel:nth-child(${index+1}) .meeting-item-menu > span `));
  }

  getContentOfMenuFromQuestionnairePanel(index: number): ElementFinder {
    return element(by.css(`#questionnaireList .panel:nth-child(${index+1}) .meeting-item-menu #questionnairePopover`));
  }

  getReviewOptionFromMenu(index: number): ElementFinder {
    return element(by.css(`#questionnaireList .panel:nth-child(${index+1}) .meeting-item-menu #questionnairePopover > div:nth-child(1) > a`));
  }

  getVoteOptionFromMenu(index: number): ElementFinder {
    return element(by.css(`#questionnaireList .panel:nth-child(${index+1}) .meeting-item-menu #questionnairePopover > div:nth-child(2) > a`));
  }

  getResultsOptionFromMenu(index: number): ElementFinder {
    return element(by.css(`#questionnaireList .panel:nth-child(${index+1}) .meeting-item-menu #questionnairePopover > div:nth-child(3) > a`));
  }

  getDateExpiredForQuestionnaire(index: number): ElementFinder {
    return element(by.css(`#questionnaireList .panel:nth-child(${index+1}) .panel-body i.date-expired`));
  }

  getNumberOFQuestions(index: number): ElementFinder {
    return element(by.css(`#questionnaireList .panel:nth-child(${index+1}) .panel-body > span`));
  }

  getQuestions(index: number): ElementArrayFinder {
    return element.all(by.css(`#questionnaireList .panel:nth-child(${index+1}) ul.list-group > li`));
  }
}
