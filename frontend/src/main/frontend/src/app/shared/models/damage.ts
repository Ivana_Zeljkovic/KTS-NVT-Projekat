import { TenantForDamage } from "./tenant-for-damage";
import { BusinessForDamage } from "./business-for-damage";
import { Address } from "./address";

export interface Damage {
  id: number;
  description: string;
  date: Date;
  typeName: string;
  urgent: boolean;
  fixed: boolean;
  buildingId: number;
  address: Address;
  creator: TenantForDamage;
  image?: string;
  apartmentId?: number;
  floorNumber?: number;
  apartmentNumber?: number;
  responsiblePerson?: TenantForDamage;
  responsibleInstitution?: BusinessForDamage;
  selectedBusiness?: BusinessForDamage;
  numberOfElements?: number;
}
