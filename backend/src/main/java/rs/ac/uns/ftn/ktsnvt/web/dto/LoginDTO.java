package rs.ac.uns.ftn.ktsnvt.web.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
* Created by Ivana Zeljkovic on 06/11/2017.
*/
public class LoginDTO implements Serializable{

    @NotNull
    private String username;
    @NotNull
    private String password;

    public LoginDTO() { }

    public LoginDTO(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() { return username; }

    public void setUsername(String username) { this.username = username; }

    public String getPassword() { return password; }

    public void setPassword(String password) { this.password = password; }
}
