package rs.ac.uns.ftn.ktsnvt.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;
import rs.ac.uns.ftn.ktsnvt.controller.util.TestUtil;
import rs.ac.uns.ftn.ktsnvt.web.dto.*;

import javax.annotation.PostConstruct;
import java.nio.charset.Charset;

import static org.hamcrest.Matchers.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Created by Nikola Garabandic on 06/12/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class ApartmentControllerIntegrationTest {

    private MediaType contentType = new MediaType(
            MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    private MockMvc mockMvc;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja stanara
    private String accessTokenTenant;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja administratora
    private String accessTokenAdmin;
    private MultiValueMap<String, String> params;


    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private TestRestTemplate testRestTemplate;


    @PostConstruct
    public void setup() {
        this.mockMvc = MockMvcBuilders.
                webAppContextSetup(webApplicationContext).apply(springSecurity()).build();
    }

    // pre izvrsavanja testa, neophodno je izvrsiti logovanje u aplikaciju da bismo dobili token
    @Before
    public void login() throws Exception {

        // logovanje stanara
        LoginDTO loginDTOTenant = new LoginDTO("jasmina", "jasmina");

        ResponseEntity<TokenDTO> responseEntityTenant =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOTenant,
                        TokenDTO.class);
        // preuzmemo token za logovanog stanara iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenTenant = responseEntityTenant.getBody().getValue();

        //logovanje administratora
        LoginDTO loginDTO = new LoginDTO("ivana", "ivana");

        ResponseEntity<TokenDTO> responseEntity =
            testRestTemplate.postForEntity("/api/login",
                loginDTO,
                TokenDTO.class);
        // preuzmemo token za logovanog administratora iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenAdmin = responseEntity.getBody().getValue();

        // za pageable atribut
        params = TestUtil.getParams(
                "0", "10", "ASC", "id", "");
    }

    // TESTOVI za getAllAparmtnents
    @Test
    public void testGetAllApartments() throws Exception{
        /*
        * Test proverava ispravnost rada metode getAllAparmtnents kontrolera ApartmentsController
        * (dobavljanje svih stanova)
        * */

        this.mockMvc.perform(get("/api")
                .header("Authentication-Token", this.accessTokenAdmin)
                .params(params))
                .andExpect(status().isOk());
    }

    @Test
    public void testAllApartmentsByNotAuthenticatedUser() throws Exception{
        /*
        * Test proverava ispravnost rada metode getAllAparmtnents kontrolera ApartmentsController
        * (dobavljanje svih stanova) kada je poziva korisnik koji nije ulogovan
        * */

        this.mockMvc.perform(get("/api")
                .params(params))
                .andExpect(status().isForbidden());
    }

    // TESTOVI za getFreeApartments
    @Test
    public void testGetFreeApartments() throws Exception{
        /*
        * Test proverava ispravnost rada metode getFreeApartments kontrolera ApartmentsController
        * (dobavljanje svih slobodnih stanova)
        * */

        this.mockMvc.perform(get("/api/apartments/free_apartments")
                .header("Authentication-Token", this.accessTokenAdmin)
                .params(params))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetFreeApartmentsByNotAuthenticatedUser() throws Exception{
        /*
        * Test proverava ispravnost rada metode getFreeApartments kontrolera ApartmentsController
        * (dobavljanje svih slobodnih stanova)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        this.mockMvc.perform(get("/api/apartments/free_apartments")
                .params(params))
                .andExpect(status().isForbidden());
    }

    // TESTOVI za getFreeApartmentsByBuilding
    @Test
    public void testGetFreeApartmentsByBuilding() throws Exception{
        /*
        * Test proverava ispravnost rada metode getFreeApartmentsByBuilding kontrolera ApartmentsController
        * (dobavljanje svih slobodnih stanova u zgradi na zadatoj adresi)
        * */

        this.mockMvc.perform(get(
                "/api/apartments/free_apartments_building")
                .header("Authentication-Token", this.accessTokenAdmin)
                .params(params)
                .param("street", "Janka Veselinovica")
                .param("number", "10")
                .param("city_name", "Novi Sad")
                .param("postal_number", "21000"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.[*]", hasSize(0)));
    }

    @Test
    public void testGetFreeApartmentsByBuildingByNotAuthenticatedUser() throws Exception{
        /*
        * Test proverava ispravnost rada metode getFreeApartmentsByBuilding kontrolera ApartmentsController
        * (dobavljanje svih slobodnih stanova u zgradi  na zadatoj adresi)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        this.mockMvc.perform(get(
                "/api/apartments/free_apartments_building")
                .params(params)
                .param("street", "Janka Veselinovica")
                .param("number", "10")
                .param("city_name", "Novi Sad")
                .param("postal_number", "21000"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetFreeApartmentsByBuildingNonexistentAddress() throws Exception{
        /*
        * Test proverava ispravnost rada metode getFreeApartmentsByBuilding kontrolera ApartmentsController
        * (dobavljanje svih slobodnih stanova u zgradi  na zadatoj adresi)
        * u slucaju kada ne postoji trazena adresa
        * */

        this.mockMvc.perform(get(
                "/api/apartments/free_apartments_building")
                .header("Authentication-Token", this.accessTokenAdmin)
                .params(params)
                .param("street", "Marka Veselinovica")
                .param("number", "10")
                .param("city_name", "Novi Sad")
                .param("postal_number", "21000"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetFreeApartmentsByBuildingMissingStreet() throws Exception{
        /*
        * Test proverava ispravnost rada metode getFreeApartmentsByBuilding kontrolera ApartmentsController
        * (dobavljanje svih slobodnih stanova u zgradi  na zadatoj adresi)
        * u slucaju kada nedostaje vrednost parametra steet
        * */

        this.mockMvc.perform(get(
                "/api/apartments/free_apartments_building")
                .header("Authentication-Token", this.accessTokenAdmin)
                .params(params)
                .param("street", "")
                .param("number", "10")
                .param("city_name", "Novi Sad")
                .param("postal_number", "21000"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testGetFreeApartmentsByBuildingMissingNumber() throws Exception{
        /*
        * Test proverava ispravnost rada metode getFreeApartmentsByBuilding kontrolera ApartmentsController
        * (dobavljanje svih slobodnih stanova u zgradi  na zadatoj adresi)
        * u slucaju kada nedostaje vrednost parametra number
        * */

        this.mockMvc.perform(get(
                "/api/apartments/free_apartments_building")
                .header("Authentication-Token", this.accessTokenAdmin)
                .params(params)
                .param("street", "Janka Veselinovica")
                .param("number", "")
                .param("city_name", "Novi Sad")
                .param("postal_number", "21000"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testFreeApartmentsByBuildingMissingCityName() throws Exception{
        /*
        * Test proverava ispravnost rada metode getFreeApartmentsByBuilding kontrolera ApartmentsController
        * (dobavljanje svih slobodnih stanova u zgradi  na zadatoj adresi)
        * u slucaju kada nedostaje vrednost parametra cityName
        * */

        this.mockMvc.perform(get(
                "/api/apartments/free_apartments_building")
                .header("Authentication-Token", this.accessTokenAdmin)
                .params(params)
                .param("street", "Janka Veselinovica")
                .param("number", "10")
                .param("city_name", "")
                .param("postal_number", "21000"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testGetFreeApartmentsByBuildingMissingPostalNumber() throws Exception{
        /*
        * Test proverava ispravnost rada metode getFreeApartmentsByBuilding kontrolera ApartmentsController
        * (dobavljanje svih slobodnih stanova u zgradi  na zadatoj adresi)
        * u slucaju kada nedostaje vrednost parametra postalNumber
        * */

        this.mockMvc.perform(get(
                "/api/apartments/free_apartments_building")
                .header("Authentication-Token", this.accessTokenAdmin)
                .params(params)
                .param("street", "Janka Veselinovica")
                .param("number", "10")
                .param("city_name", "Novi Sad")
                .param("postal_number", ""))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za getFreeApartmentsBySize
    @Test
    public void testGetFreeApartmentsBySize() throws Exception{
        /*
        * Test proverava ispravnost rada metode getFreeApartmentsBySize kontrolera ApartmentsController
        * (pronalazenje stanova na osnovu minimalne i maksimalne kvadrature)
        * */

        this.mockMvc.perform(get("/api/apartments/free_apartments_size")
                .header("Authentication-Token", this.accessTokenAdmin)
                .params(params)
                .param("min", "20")
                .param("max", "200")
                .param("buildingId", "100"))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetFreeApartmentsBySizeByNotAuthenticatedUser() throws Exception{
       /*
        * Test proverava ispravnost rada metode getFreeApartmentsBySize kontrolera ApartmentsController
        * (pronalazenje stanova na osnovu minimalne i maksimalne kvadrature)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        this.mockMvc.perform(get("/api/apartments/free_apartments_size")
                .params(params)
                .param("min", "20")
                .param("max", "200")
                .param("building_id", "100"))
                .andExpect(status().isForbidden());
    }

    // TESTOVI za makeTenantOwner
    @Test
    @Transactional
    @Rollback(true)
    public void testMakeTenantOwner() throws Exception{
        /*
        * Test proverava ispravnost rada metode makeTenantOwner kontrolera ApartmentController
        * (postavljanje stanara sa zadatim ID-em da je vlasnik stana sa zadatim ID-em)
        * */

        TenantInApartmentDTO tenantInApartmentDTO = new TenantInApartmentDTO(106L);
        String token = TestUtil.json(tenantInApartmentDTO);

        mockMvc.perform(patch("/api/buildings/100/apartments/102/make_tenant_owner")
                .header("Authentication-Token", this.accessTokenAdmin)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType));
    }

    @Test
    public void testMakeTenantOwnerByNotAuthenticatedUser() throws Exception{
        /*
        * Test proverava ispravnost rada metode makeTenantOwner kontrolera ApartmentController
        * (postavljanje stanara sa zadatim ID-em da je vlasnik stana sa zadatim ID-em)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        TenantInApartmentDTO tenantInApartmentDTO = new TenantInApartmentDTO(106L);
        String token = TestUtil.json(tenantInApartmentDTO);

        mockMvc.perform(patch("/api/buildings/100/apartments/102/make_tenant_owner")
                .contentType(contentType)
                .content(token))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testMakeTenantOwnerByNotAuthenticatedAdministrator() throws Exception{
        /*
        * Test proverava ispravnost rada metode makeTenantOwner kontrolera ApartmentController
        * (postavljanje stanara sa zadatim ID-em da je vlasnik stana sa zadatim ID-em)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu administratora
        * */

        TenantInApartmentDTO tenantInApartmentDTO = new TenantInApartmentDTO(106L);
        String token = TestUtil.json(tenantInApartmentDTO);

        mockMvc.perform(patch("/api/buildings/100/apartments/102/make_tenant_owner")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testMakeTenantOwnerNonexistentTenant() throws Exception{
        /*
        * Test proverava ispravnost rada metode makeTenantOwner kontrolera ApartmentController
        * (postavljanje stanara sa zadatim ID-em da je vlasnik stana sa zadatim ID-em)
        * u slucaju kada ne postoji stanar sa zadatim ID-em
        * */

        // ne postoji stanar sa ID-em = 200
        TenantInApartmentDTO tenantInApartmentDTO = new TenantInApartmentDTO(200L);
        String token = TestUtil.json(tenantInApartmentDTO);

        mockMvc.perform(patch("/api/buildings/100/apartments/102/make_tenant_owner")
                .header("Authentication-Token", this.accessTokenAdmin)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testMakeTenantOwnerNonexistentBuilding() throws Exception{
        /*
        * Test proverava ispravnost rada metode makeTenantOwner kontrolera ApartmentController
        * (postavljanje stanara sa zadatim ID-em da je vlasnik stana sa zadatim ID-em)
        * u slucaju kada ne postij izgrada sa zadatim ID-em
        * */

        TenantInApartmentDTO tenantInApartmentDTO = new TenantInApartmentDTO(106L);
        String token = TestUtil.json(tenantInApartmentDTO);

        // ne postiji zgrada sa ID-em = 200
        mockMvc.perform(patch("/api/buildings/200/apartments/102/make_tenant_owner")
                .header("Authentication-Token", this.accessTokenAdmin)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testMakeTenantOwnerNonexistentApartment() throws Exception{
        /*
        * Test proverava ispravnost rada metode makeTenantOwner kontrolera ApartmentController
        * (postavljanje stanara sa zadatim ID-em da je vlasnik stana sa zadatim ID-em)
        * u slucaju kada ne postoji stan sa zadatim ID-em
        * */

        TenantInApartmentDTO tenantInApartmentDTO = new TenantInApartmentDTO(106L);
        String token = TestUtil.json(tenantInApartmentDTO);

        // ne postoji stan sa ID-em = 200
        mockMvc.perform(patch("/api/buildings/100/apartments/200/make_tenant_owner")
                .header("Authentication-Token", this.accessTokenAdmin)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testMakeTenantOwnerTenantNotInBuilding() throws Exception{
        /*
        * Test proverava ispravnost rada metode makeTenantOwner kontrolera ApartmentController
        * (postavljanje stanara sa zadatim ID-em da je vlasnik stana sa zadatim ID-em)
        * u slucaju kada stanar sa zadatim ID-em ne zivi u zgradi sa zadatim ID-em
        * */

        TenantInApartmentDTO tenantInApartmentDTO = new TenantInApartmentDTO(107L);
        String token = TestUtil.json(tenantInApartmentDTO);

        mockMvc.perform(patch("/api/buildings/100/apartments/103/make_tenant_owner")
                .header("Authentication-Token", this.accessTokenAdmin)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testMakeTenantOwnerMissingId() throws Exception{
        /*
        * Test proverava ispravnost rada metode makeTenantOwner kontrolera ApartmentController
        * (postavljanje stanara sa zadatim ID-em da je vlasnik stana sa zadatim ID-em)
        * u slucaju kada su je prosledjeni DTO objekat nevalidan: ne sadrzi sva neophodna polja - nedostaje polje id
        * */

        String token = "{}";

        mockMvc.perform(patch("/api/buildings/100/apartments/102/make_tenant_owner")
                .header("Authentication-Token", this.accessTokenAdmin)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testMakeTenantOwnerInappropriateTypeOfId() throws Exception{
        /*
        * Test proverava ispravnost rada metode makeTenantOwner kontrolera ApartmentController
        * (postavljanje stanara sa zadatim ID-em da je vlasnik stana sa zadatim ID-em)
        * u slucaju kada su je prosledjeni DTO objekat nevalidan: sadrzi sva neophodna polja, ali je polje id nevalidnog tipa
        * */

        String token = "{\"id\":\"string\"}";

        mockMvc.perform(patch("/api/buildings/100/apartments/102/make_tenant_owner")
                .header("Authentication-Token", this.accessTokenAdmin)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za connectTenantAndApartment
    @Test
    @Transactional
    @Rollback(true)
    public void testConnectTenantAndApartment() throws Exception{
        /*
        * Test proverava ispravnost rada metode connectTenantAndApartment kontrolera ApartmentController
        * (postavljanje stanara sa zadatim ID-em kao stanara u konkretnom stanu sa zadatim ID-em)
        * */

        TenantInApartmentDTO tenantInApartmentDTO = new TenantInApartmentDTO(107L);
        String token = TestUtil.json(tenantInApartmentDTO);

        mockMvc.perform(patch("/api/buildings/100/apartments/101/connect_tenant_and_apartment")
                .header("Authentication-Token", this.accessTokenAdmin)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isOk());
    }

    @Test
    public void testConnectTenantAndApartmentByNotAuthenticatedUser() throws Exception{
        /*
        * Test proverava ispravnost rada metode connectTenantAndApartment kontrolera ApartmentController
        * (postavljanje stanara sa zadatim ID-em kao stanara u konkretnom stanu sa zadatim ID-em)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        TenantInApartmentDTO tenantInApartmentDTO = new TenantInApartmentDTO(107L);
        String token = TestUtil.json(tenantInApartmentDTO);

        mockMvc.perform(patch("/api/buildings/100/apartments/101/connect_tenant_and_apartment")
                .contentType(contentType)
                .content(token))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testConnectTenantAndApartmentByNotAuthenticatedAdministrator() throws Exception{
        /*
        * Test proverava ispravnost rada metode connectTenantAndApartment kontrolera ApartmentController
        * (postavljanje stanara sa zadatim ID-em kao stanara u konkretnom stanu sa zadatim ID-em)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu administratora
        * */

        TenantInApartmentDTO tenantInApartmentDTO = new TenantInApartmentDTO(107L);
        String token = TestUtil.json(tenantInApartmentDTO);

        mockMvc.perform(patch("/api/buildings/100/apartments/101/connect_tenant_and_apartment")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testConnectTenantAndApartmentNonexistentTenant() throws Exception{
        /*
        * Test proverava ispravnost rada metode connectTenantAndApartment kontrolera ApartmentController
        * (postavljanje stanara sa zadatim ID-em kao stanara u konkretnom stanu sa zadatim ID-em)
        * u slucaju kada ne postoji stanar sa zadatim ID-em
        * */

        // ne postoji stanar sa ID-em = 200
        TenantInApartmentDTO tenantInApartmentDTO = new TenantInApartmentDTO(200L);
        String token = TestUtil.json(tenantInApartmentDTO);

        mockMvc.perform(patch("/api/buildings/100/apartments/101/connect_tenant_and_apartment")
                .header("Authentication-Token", this.accessTokenAdmin)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testConnectTenantAndApartmentNonexistentBuilding() throws Exception{
        /*
        * Test proverava ispravnost rada metode connectTenantAndApartment kontrolera ApartmentController
        * (postavljanje stanara sa zadatim ID-em kao stanara u konkretnom stanu sa zadatim ID-em)
        * u slucaju kada ne postoji zgrada sa zadatim ID-em
        * */

        TenantInApartmentDTO tenantInApartmentDTO = new TenantInApartmentDTO(107L);
        String token = TestUtil.json(tenantInApartmentDTO);

        // ne postoji zgrada sa ID-em = 200
        mockMvc.perform(patch("/api/buildings/200/apartments/101/connect_tenant_and_apartment")
                .header("Authentication-Token", this.accessTokenAdmin)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testConnectTenantAndApartmentNonexistentApartment() throws Exception{
        /*
        * Test proverava ispravnost rada metode connectTenantAndApartment kontrolera ApartmentController
        * (postavljanje stanara sa zadatim ID-em kao stanara u konkretnom stanu sa zadatim ID-em)
        * u slucaju kada ne postoji stan sa zadatim ID-em
        * */

        TenantInApartmentDTO tenantInApartmentDTO = new TenantInApartmentDTO(107L);
        String token = TestUtil.json(tenantInApartmentDTO);

        // ne postoji stan sa ID-em = 200
        mockMvc.perform(patch("/api/buildings/100/apartments/501/connect_tenant_and_apartment")
                .header("Authentication-Token", this.accessTokenAdmin)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testConnectTenantAndApartmentWhenApartmentNotInBuilding() throws Exception{
        /*
        * Test proverava ispravnost rada metode connectTenantAndApartment kontrolera ApartmentController
        * (postavljanje stanara sa zadatim ID-em kao stanara u konkretnom stanu sa zadatim ID-em)
        * u slucaju kada stan sa zadatim ID-em ne pripada zgradi sa zadatim ID-em
        * */

        TenantInApartmentDTO tenantInApartmentDTO = new TenantInApartmentDTO(107L);
        String token = TestUtil.json(tenantInApartmentDTO);

        mockMvc.perform(patch("/api/buildings/100/apartments/103/connect_tenant_and_apartment")
                .header("Authentication-Token", this.accessTokenAdmin)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testConnectTenantAndApartmentMissingId() throws Exception{
        /*
        * Test proverava ispravnost rada metode connectTenantAndApartment kontrolera ApartmentController
        * (postavljanje stanara sa zadatim ID-em kao stanara u konkretnom stanu sa zadatim ID-em)
        * u slucaju kada su je prosledjeni DTO objekat nevalidan: ne sadrzi sva neophodna polja - nedostaje polje id
        * */

        String token = "{}";

        mockMvc.perform(patch("/api/buildings/100/apartments/101/connect_tenant_and_apartment")
                .header("Authentication-Token", this.accessTokenAdmin)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testConnectTenantAndApartmentInappropriateTypeOfId() throws Exception{
        /*
        * Test proverava ispravnost rada metode connectTenantAndApartment kontrolera ApartmentController
        * (postavljanje stanara sa zadatim ID-em kao stanara u konkretnom stanu sa zadatim ID-em)
        * u slucaju kada su je prosledjeni DTO objekat nevalidan: sadrzi sva neophodna polja, ali je polje id nevalidnog tipa
        * */

        String token = "{\"id\":\"string\"}";

        mockMvc.perform(patch("/api/buildings/100/apartments/101/connect_tenant_and_apartment")
                .header("Authentication-Token", this.accessTokenAdmin)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za removeTenantFromApartment
    @Test
    @Transactional
    @Rollback(true)
    public void testRemoveTenantFromApartment() throws Exception{
        /*
        * Test proverava ispravnost rada metode removeTenantFromApartment kontrolera ApartmentController
        * (uklanjanje stanara sa zadatim ID-em iz stana sa zadatim ID-em)
        * */

        mockMvc.perform(patch("/api/buildings/100/apartments/101/tenants/101/remove_tenant_from_apartment")
                .header("Authentication-Token", this.accessTokenAdmin)
                .contentType(contentType))
                .andExpect(status().isOk());
    }

    @Test
    public void testRemoveTenantFromApartmentByNotAuthenticatedUser() throws Exception{
        /*
        * Test proverava ispravnost rada metode removeTenantFromApartment kontrolera ApartmentController
        * (uklanjanje stanara sa zadatim ID-em iz stana sa zadatim ID-em)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        mockMvc.perform(patch("/api/buildings/100/apartments/101/tenants/101/remove_tenant_from_apartment")
                .contentType(contentType))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testRemoveTenantFromApartmentByNotAuthenticatedAdministrator() throws Exception{
        /*
        * Test proverava ispravnost rada metode removeTenantFromApartment kontrolera ApartmentController
        * (uklanjanje stanara sa zadatim ID-em iz stana sa zadatim ID-em)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu administratora
        * */

        mockMvc.perform(patch("/api/buildings/100/apartments/101/tenants/101/remove_tenant_from_apartment")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testRemoveTenantFromApartmentNonexistentBuilding() throws Exception{
        /*
        * Test proverava ispravnost rada metode removeTenantFromApartment kontrolera ApartmentController
        * (uklanjanje stanara sa zadatim ID-em iz stana sa zadatim ID-em)
        * u slucaju kada ne postoji zgrada sa zadatim ID-em
        * */

        // ne postoji zgrada sa ID-em = 200
        mockMvc.perform(patch("/api/buildings/200/apartments/101/tenants/101/remove_tenant_from_apartment")
                .header("Authentication-Token", this.accessTokenAdmin)
                .contentType(contentType))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testRemoveTenantFromApartmentNonexistentTenant() throws Exception{
        /*
        * Test proverava ispravnost rada metode removeTenantFromApartment kontrolera ApartmentController
        * (uklanjanje stanara sa zadatim ID-em iz stana sa zadatim ID-em)
        * u slucaju kada ne postoji stanar sa zadatim ID-em
        * */

        // ne postoji stanar sa ID-em = 200
        mockMvc.perform(patch("/api/buildings/100/apartments/101/tenants/200/remove_tenant_from_apartment")
                .header("Authentication-Token", this.accessTokenAdmin)
                .contentType(contentType))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testRemoveTenantFromApartmentNonexistentApartment() throws Exception{
        /*
        * Test proverava ispravnost rada metode removeTenantFromApartment kontrolera ApartmentController
        * (uklanjanje stanara sa zadatim ID-em iz stana sa zadatim ID-em)
        * u slucaju kada ne postoji stan sa zadatim ID-em
        * */

        // ne postoji stan sa ID-em = 200
        mockMvc.perform(patch("/api/buildings/100/apartments/200/tenants/101/remove_tenant_from_apartment")
                .header("Authentication-Token", this.accessTokenAdmin)
                .contentType(contentType))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testRemoveTenantFromApartmentWhenApartmentIsNotInBuilding() throws Exception{
        /*
        * Test proverava ispravnost rada metode removeTenantFromApartment kontrolera ApartmentController
        * (uklanjanje stanara sa zadatim ID-em iz stana sa zadatim ID-em)
        * u slucaju kada stan sa zadatim ID-em ne pripada zgradi sa zadatim ID-em
        * */

        mockMvc.perform(patch("/api/buildings/101/apartments/101/tenants/101/remove_tenant_from_apartment")
                .header("Authentication-Token", this.accessTokenAdmin)
                .contentType(contentType))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testRemoveTenantFromApartmentWhenTenantWithoutApartment() throws Exception{
        /*
        * Test proverava ispravnost rada metode removeTenantFromApartment kontrolera ApartmentController
        * (uklanjanje stanara sa zadatim ID-em iz stana sa zadatim ID-em)
        * u slucaju kada stanar sa zadatim ID-em nije povezan ni sa jednim stanom
        * */

        mockMvc.perform(patch("/api/buildings/100/apartments/101/tenants/109/remove_tenant_from_apartment")
                .header("Authentication-Token", this.accessTokenAdmin)
                .contentType(contentType))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testRemoveTenantFromApartmentWhenTenantFromInvalidApartment() throws Exception{
        /*
        * Test proverava ispravnost rada metode removeTenantFromApartment kontrolera ApartmentController
        * (uklanjanje stanara sa zadatim ID-em iz stana sa zadatim ID-em)
        * u slucaju kada stanar sa zadatim ID-em ne zivi u stanu sa zadatim ID-em
        * */

        mockMvc.perform(patch("/api/buildings/100/apartments/102/tenants/101/remove_tenant_from_apartment")
                .header("Authentication-Token", this.accessTokenAdmin)
                .contentType(contentType))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za getApartments
    @Test
    public void testGetApartments() throws Exception {
        /*
        * Test proverava ispravnost rada metode getApartments kontrolera ApartmentController
        * (dobavljanje liste svih stanova koji pripadaju zgradi sa zadatim ID-em)
        * */

        /*
        zgrada sa ID-em = 100 ima 4 stana sa sledecim podacima:
            1 - id = 100,
                owner_id = 101
            2 - id = 101,
                owner_id = 101,
            3 - id = 102,
                owner_id = 101,
            4 - id = 104,
                owner_id = 102
         */

        MultiValueMap<String, String> params = TestUtil.getParams(
                "0", "10", "ASC", "id", "number");

        this.mockMvc.perform(get("/api/buildings/100/apartments")
                .header("Authentication-Token", this.accessTokenAdmin)
                .params(params))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.[*]", hasSize(4)))
                .andExpect(jsonPath("$.[*].id", containsInAnyOrder(100,101,102,104)))
                .andExpect(jsonPath("$.[*].owner.id", containsInAnyOrder(101,101,101,102)));
    }

    @Test
    public void testGetApartmentsByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode getApartments kontrolera ApartmentController
        * (dobavljanje liste svih stanova koji pripadaju zgradi sa zadatim ID-em)
        * u slucaju da akciju izvrsava nelogovani korisnik
        * */

        MultiValueMap<String, String> params = TestUtil.getParams(
                "0", "10", "ASC", "id", "number");

        this.mockMvc.perform(get("/api/buildings/100/apartments")
                .params(params))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetApartmentsNonexistentBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode getApartments kontrolera ApartmentController
        * (dobavljanje liste svih stanova koji pripadaju zgradi sa zadatim ID-em)
        * u slucaju kada ne postoji zgrada sa zadatim ID-em
        * */

        MultiValueMap<String, String> params = TestUtil.getParams(
                "0", "10", "ASC", "id", "number");

        // ne postoji zgrada sa ID-em = 200
        this.mockMvc.perform(get("/api/buildings/200/apartments")
                .header("Authentication-Token", this.accessTokenAdmin)
                .params(params))
                .andExpect(status().isNotFound());
    }

    // TESTOVI za getApartment
    @Test
    public void testGetApartment() throws Exception {
        /*
        * Test proverava ispravnost rada metode getApartment kontrolera ApartmentController
        * (dobavljanje konkretnog stana sa zadatim ID-em koji pripada zgradi sa zadatim ID-em)
        * */

        /*
        zgrada sa ID-em = 100 ima stan sa sledecim podacima:
            1 - id = 100,
                owner_id = 101 (firstName = Katarina, lastName = Cukurov)
         */
        this.mockMvc.perform(get("/api/apartments/100")
                .header("Authentication-Token", this.accessTokenAdmin))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(100))
                .andExpect(jsonPath("$.owner.id").value(101))
                .andExpect(jsonPath("$.owner.firstName").value("Katarina"))
                .andExpect(jsonPath("$.owner.lastName").value("Cukurov"));
    }

    @Test
    public void testGetApartmentByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode getApartment kontrolera ApartmentController
        * (dobavljanje konkretnog stana sa zadatim ID-em)
        * u slucaju da akciju izvrsava nelogovani korisnik
        * */

        this.mockMvc.perform(get("/api/apartments/100"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetApartmentByNotAuthenticatedAdministrator() throws Exception {
        /*
        * Test proverava ispravnost rada metode getApartment kontrolera ApartmentController
        * (dobavljanje konkretnog stana sa zadatim ID-em)
        * u slucaju da akciju izvrsava logovani korisnik koji nema ulogu administratora
        * */

        this.mockMvc.perform(get("/api/apartments/100")
                .header("Authentication-Token", this.accessTokenTenant))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetApartmentNonexistentApartment() throws Exception {
        /*
        * Test proverava ispravnost rada metode getApartment kontrolera ApartmentController
        * (dobavljanje konkretnog stana sa zadatim ID-em)
        * u slucaju kada ne postoji stan sa zadatim ID-em
        * */

        // ne postoji stan sa ID-em = 200
        this.mockMvc.perform(get("/api/apartments/200")
                .header("Authentication-Token", this.accessTokenAdmin))
                .andExpect(status().isNotFound());
    }

    // TESTOVI za deleteApartment
    @Test
    @Transactional
    @Rollback(true)
    public void testDeleteApartment() throws Exception {
        /*
        * Test proverava ispravnost metode deleteApartment kontrolera ApartmentController
        * u slucaju kada stan sa prosledjenim ID-em postoji i kada je ulogovan administrator
        * */

        this.mockMvc.perform(delete("/api/buildings/100/apartments/104")
                .header("Authentication-Token", this.accessTokenAdmin))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteApartmentByUnauthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost metode deleteApartment kontrolera ApartmentController
        * u slucaju kada stan sa prosledjenim ID-em postoji i kada korisnik nije ulogovan
        * */

        this.mockMvc.perform(delete("/api/buildings/100/apartments/104"))
                .andExpect(status().isForbidden());
    }

    // TESTOVI za createApartment
    @Test
    @Transactional
    @Rollback(true)
    public void testCreateApartment() throws Exception{
        /*
        * Test proverava ispravnost metode createApartment kontrolera ApartmentController
        * kada su prosledjeni podaci validni i kada je ulogovan administrator
        * */

        ApartmentCreateDTO apartmentCreateDTO = new ApartmentCreateDTO( 4, 1, 60);
        String token = TestUtil.json(apartmentCreateDTO);

        this.mockMvc.perform(post("/api/buildings/100/apartments")
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType));
    }

    @Test
    public void testCreateApartmentByUnauthenticatedUser() throws Exception{
        /*
        * Test proverava ispravnost metode createApartment kontrolera ApartmentController
        * kada su prosledjeni podaci validni i kada je niko nije ulogovan
        * */

        ApartmentCreateDTO apartmentCreateDTO = new ApartmentCreateDTO( 4, 1, 60);
        String token = TestUtil.json(apartmentCreateDTO);

        this.mockMvc.perform(post("/api/buildings/100/apartments")
                .contentType(contentType)
                .content(token))
                .andExpect(status().isForbidden());
    }
}