import { AppPage } from './app.po';
import { browser, protractor } from "protractor";

let origFn = browser.driver.controlFlow().execute;

browser.driver.controlFlow().execute = function() {
  let args = arguments;

  // queue 30ms wait
  origFn.call(browser.driver.controlFlow(), function() {
    return protractor.promise.delayed(30);
  });

  return origFn.apply(browser.driver.controlFlow(), args);
};

describe('ktsnvt App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should redirect user to login page', () => {
    page.navigateTo();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');
  });
});
