package rs.ac.uns.ftn.ktsnvt.service;

import rs.ac.uns.ftn.ktsnvt.model.entity.AccountAuthority;

/**
* Created by Ivana Zeljkovic on 06/11/2017.
*/
public interface AccountAuthorityService {

    AccountAuthority save(AccountAuthority accountAuthority);

    void remove(Long id);
}
