import {by, element, ElementArrayFinder, ElementFinder} from "protractor";

export class DamageAddPage {
  // get methods
  getDamageAddElement(): ElementFinder {
    return element(by.tagName('app-damage-add'));
  }

  getTitle(): ElementFinder {
    return element(by.css('.modal-title'));
  }

  getDescriptionInput(): ElementFinder {
    return element(by.id('description'));
  }

  getDamageTypeSelector(): ElementFinder {
    return element(by.id('typeName'));
  }

  getDamageTypeOptions(): ElementArrayFinder {
    return element.all(by.css('#typeName > option'));
  }

  getDamageType(index: number): ElementFinder {
    return element(by.css(`#typeName > option:nth-child(${index+1})`));
  }

  getDamageUrgencySelector(): ElementFinder {
    return element(by.id('urgent'));
  }

  getDamageUrgencyOptions(): ElementArrayFinder {
    return element.all(by.css('#urgent > option'));
  }

  getDamageUrgent(index: number): ElementFinder {
    return element(by.css(`#urgent > option:nth-child(${index+1})`));
  }

  getDamageLocationSelector(): ElementFinder {
    return element(by.id('locationOfDamage'));
  }

  getDamageLocationOptions(): ElementArrayFinder {
    return element.all(by.css('#locationOfDamage > option'));
  }

  getDamageLocation(index: number): ElementFinder {
    return element(by.css(`#locationOfDamage > option:nth-child(${index+1})`));
  }

  getButtonChooseApartment(): ElementFinder {
    return element(by.buttonText('Choose'));
  }

  getImageUploadInput(): ElementFinder {
    return element(by.id('file'));
  }

  getSaveButton(): ElementFinder {
    return element(by.buttonText('Save'));
  }

  getResetButton(): ElementFinder {
    return element(by.buttonText('Reset'));
  }

  getCancelButton(): ElementFinder {
    return element(by.buttonText('Cancel'));
  }

  // set methods
  setDamageDescription(text: string) {
    this.getDescriptionInput().sendKeys(text);
  }

  // get error method
  getDescriptionRequiredError(): ElementFinder {
    return element(by.id('descriptionRequiredError'));
  }
}
