package rs.ac.uns.ftn.ktsnvt.repository.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.model.entity.Offer;
import rs.ac.uns.ftn.ktsnvt.repository.OfferRepository;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class OfferRepositoryIntegrationTest {

    @Autowired
    private OfferRepository offerRepository;

    @Test
    public void testFindForCurrentDamage(){
        /*
        Test proverava rad ispravnosti metode findaAllForCurrentDamage repozitorijuma OfferRepository.
        (dobavljanje svih ponuda za trenutni kvar)
        kvar sa Id-em 100 ima tacno jednu ponudu.
         */

        Page<Offer> offers = this.offerRepository.findAllForCurrentDamage(100L, new PageRequest(0, 3, Sort.Direction.ASC, "id"));

        assertEquals(1L, offers.getTotalElements());
    }

    @Test
    public void testFindNoneForCurrentDamage(){
        /*
        Test proverava rad ispravnosti metode findaAllForCurrentDamage repozitorijuma OfferRepository.
        (dobavljanje svih ponuda za trenutni kvar)
        kvar sa Id-em 200 ne postoji te ce imati 0 ponuda
         */

        Page<Offer> offers = this.offerRepository.findAllForCurrentDamage(200L, new PageRequest(0, 3, Sort.Direction.ASC, "id"));

        assertEquals(0L, offers.getTotalElements());
    }
}
