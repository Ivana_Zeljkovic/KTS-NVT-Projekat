import { browser } from "protractor";
// page
import {LoginPage} from "../../page-objects/login.po";
import {AdminHomePage} from "../../page-objects/admin-home.po";

describe('Admin home page', () => {
  let loginPage: LoginPage;
  let adminHomePage: AdminHomePage;

  beforeEach(() => {
    loginPage = new LoginPage();
    adminHomePage = new AdminHomePage();

    browser.get('/');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');

    loginPage.setUsername('ivana');
    loginPage.setPassword('ivana');
    loginPage.getSignInButton().click();

    browser.wait(function() {
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/home';
      });
    }).then(function() {
      // expect tenant home page is loaded
      expect(adminHomePage.getAdminHomeElement().isDisplayed()).toBe(true);
    });
  });

  afterEach(() => {
    browser.executeScript('window.localStorage.clear();');
  });

  it('should display five panels when is loaded', () => {
    expect(adminHomePage.getAdminPanel().isDisplayed()).toBe(true);
    expect(adminHomePage.getApartmentsPanel().isDisplayed()).toBe(true);
    expect(adminHomePage.getBuildingsPanel().isDisplayed()).toBe(true);
    expect(adminHomePage.getBusinessesPanel().isDisplayed()).toBe(true);
    expect(adminHomePage.getTenantPanel().isDisplayed()).toBe(true);
  });

  it('should redirect on admins list when clicked on admins panel', () => {
    expect(adminHomePage.getAdminPanel().isDisplayed()).toBe(true);
    expect(adminHomePage.getApartmentsPanel().isDisplayed()).toBe(true);
    expect(adminHomePage.getBuildingsPanel().isDisplayed()).toBe(true);
    expect(adminHomePage.getBusinessesPanel().isDisplayed()).toBe(true);
    expect(adminHomePage.getTenantPanel().isDisplayed()).toBe(true);

    adminHomePage.getAdminPanel().click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/admins');
  });

  it('should redirect on tenants list when clicked on tenants panel', () => {
    expect(adminHomePage.getAdminPanel().isDisplayed()).toBe(true);
    expect(adminHomePage.getApartmentsPanel().isDisplayed()).toBe(true);
    expect(adminHomePage.getBuildingsPanel().isDisplayed()).toBe(true);
    expect(adminHomePage.getBusinessesPanel().isDisplayed()).toBe(true);
    expect(adminHomePage.getTenantPanel().isDisplayed()).toBe(true);

    adminHomePage.getTenantPanel().click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/tenants');
  });

  it('should redirect on businesses list when clicked on businesses panel', () => {
    expect(adminHomePage.getAdminPanel().isDisplayed()).toBe(true);
    expect(adminHomePage.getApartmentsPanel().isDisplayed()).toBe(true);
    expect(adminHomePage.getBuildingsPanel().isDisplayed()).toBe(true);
    expect(adminHomePage.getBusinessesPanel().isDisplayed()).toBe(true);
    expect(adminHomePage.getTenantPanel().isDisplayed()).toBe(true);

    adminHomePage.getBusinessesPanel().click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/businesses');
  });

  it('should redirect on buildings list when clicked on buildings panel', () => {
    expect(adminHomePage.getAdminPanel().isDisplayed()).toBe(true);
    expect(adminHomePage.getApartmentsPanel().isDisplayed()).toBe(true);
    expect(adminHomePage.getBuildingsPanel().isDisplayed()).toBe(true);
    expect(adminHomePage.getBusinessesPanel().isDisplayed()).toBe(true);
    expect(adminHomePage.getTenantPanel().isDisplayed()).toBe(true);

    adminHomePage.getBuildingsPanel().click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/buildings');
  });

  it('should redirect on apartments list when clicked on apartments panel', () => {
    expect(adminHomePage.getAdminPanel().isDisplayed()).toBe(true);
    expect(adminHomePage.getApartmentsPanel().isDisplayed()).toBe(true);
    expect(adminHomePage.getBuildingsPanel().isDisplayed()).toBe(true);
    expect(adminHomePage.getBusinessesPanel().isDisplayed()).toBe(true);
    expect(adminHomePage.getTenantPanel().isDisplayed()).toBe(true);

    adminHomePage.getApartmentsPanel().click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/apartments');
  });

});
