import {by, element, ElementArrayFinder, ElementFinder} from "protractor";

export class DelegateResponsibilityToInstitutionPage {
  // get methods
  getDelegateResponsibilityToInstitutionElement(): ElementFinder {
    return element(by.tagName('app-delegate-responsibility-to-institution'));
  }

  getTitle(): ElementFinder {
    return element(by.css('app-delegate-responsibility-to-institution .modal-title'));
  }

  getCloseButton(): ElementFinder {
    return element(by.css('div.modal-header > button'));
  }

  getInstitutionsTable(): ElementFinder {
    return element(by.id('institutionsTable'));
  }

  getInstitutionsTableRows(): ElementArrayFinder {
    return element.all(by.css('#institutionsTable tbody > tr'));
  }

  getInstitutionName(index: number): ElementFinder {
    return element(by.css(`#institutionsTable > tbody > tr:nth-child(${index+1}) td:nth-child(1)`));
  }

  getInstitutionStreet(index: number): ElementFinder {
    return element(by.css(`#institutionsTable > tbody > tr:nth-child(${index+1}) td:nth-child(2)`));
  }

  getInstitutionCity(index: number): ElementFinder {
    return element(by.css(`#institutionsTable > tbody > tr:nth-child(${index+1}) td:nth-child(3)`));
  }

  getCurrentResponsibleInstitution(index: number): ElementFinder {
    return element(by.css(`#institutionsTable > tbody > tr:nth-child(${index+1}) td:nth-child(4) > span`));
  }

  getButtonDelegate(index: number): ElementFinder {
    return element(by.css(`#institutionsTable > tbody > tr:nth-child(${index+1}) button`));
  }
}
