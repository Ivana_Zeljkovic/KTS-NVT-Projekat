import {browser, by, element} from "protractor";
// page
import {LoginPage} from "../../page-objects/login.po";
import {ApartmentListPage} from "./page-objects/apartment-list.po";

describe('Apartments list view', () => {
  let loginPage: LoginPage;
  let apartmentListPage: ApartmentListPage;

  beforeEach(() => {
    loginPage = new LoginPage();
    apartmentListPage = new ApartmentListPage();
    browser.get("/");

    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');
    loginPage.setUsername('ivana');
    loginPage.setPassword('ivana');
    loginPage.getSignInButton().click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/home');
    browser.get('http://localhost:49152/#/apartments');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/apartments');

  });

  afterEach(() => {
    browser.executeScript('window.localStorage.clear();');
  });

  it("should successfully load page and all of its components", () => {
    expect(apartmentListPage.getHeading().isDisplayed()).toBe(true);
    expect(apartmentListPage.getDropDownToggleSort().isDisplayed()).toBe(true);
    expect(apartmentListPage.getSortButton().isDisplayed()).toBe(true);
    expect(apartmentListPage.getDropDownToggleFilter().isDisplayed()).toBe(true);
    expect(apartmentListPage.getFilterButton().isDisplayed()).toBe(true);
    expect(apartmentListPage.getApartmentsTable().isDisplayed()).toBe(true);
    expect(apartmentListPage.getPagination().isDisplayed()).toBe(true);

    expect(apartmentListPage.getFilterButton().isEnabled()).toBe(true);
    expect(apartmentListPage.getSortButton().isEnabled()).toBe(true);
  });

  it("should go to page with more details when clicked on more button", () => {
    let moreButton = element(by.xpath("//tr[1]/td[6]")).element(by.css('.btn.btn-default'));
    moreButton.click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/apartments/100');
  });

  it('should sort apartments by building id', () => {
    apartmentListPage.getDropDownToggleSort().click();
    element(by.css('.dropdown-menu li:nth-child(1)')).click();
    expect(apartmentListPage.getDropDownToggleSort().getText()).toBe("Building");
    apartmentListPage.getSortButton().click();

    let firstRow = element(by.xpath("//tr[1]/td[4]"));
    let lastRow = element(by.xpath("//tr[last()]/td[4]"));
    expect(firstRow <= lastRow);
  });

  it('should sort apartments by surface', () => {
    apartmentListPage.getDropDownToggleSort().click();
    element(by.css('.dropdown-menu li:nth-child(2)')).click();
    expect(apartmentListPage.getDropDownToggleSort().getText()).toBe("Surface");
    apartmentListPage.getSortButton().click();

    let firstRow = element(by.xpath("//tr[1]/td[3]"));
    let lastRow = element(by.xpath("//tr[last()]/td[3]"));
    expect(firstRow <= lastRow);
  });

  it('should sort apartments by floor', () => {
    apartmentListPage.getDropDownToggleSort().click();
    element(by.css('.dropdown-menu li:nth-child(3)')).click();
    expect(apartmentListPage.getDropDownToggleSort().getText()).toBe("Floor");
    apartmentListPage.getSortButton().click();

    let firstRow = element(by.xpath("//tr[1]/td[2]"));
    let lastRow = element(by.xpath("//tr[last()]/td[2]"));
    expect(firstRow <= lastRow);
  });

  it('should filter empty apartments', () => {
    apartmentListPage.getDropDownToggleFilter().click();
    element(by.css('.dropdown-menu li:nth-child(2)')).click();
    expect(apartmentListPage.getDropDownToggleFilter().getText()).toBe("Empty apartments");
    apartmentListPage.getFilterButton().click();

    let firstRow = element(by.xpath("//tr[1]"));
    let lastRow = element(by.xpath("//tr[last()]"));
    expect(firstRow == lastRow);
  });

  it('should filter apartments when criteria is none', () => {
    apartmentListPage.getDropDownToggleFilter().click();
    element(by.css('.dropdown-menu li:nth-child(1)')).click();
    expect(apartmentListPage.getDropDownToggleFilter().getText()).toBe("None");
    apartmentListPage.getFilterButton().click();

    let firstRow = element(by.xpath("//tr[1]"));
    let lastRow = element(by.xpath("//tr[last()]"));
    expect(firstRow != lastRow);
  });

});
