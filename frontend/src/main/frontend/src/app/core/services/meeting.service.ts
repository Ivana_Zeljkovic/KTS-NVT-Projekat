import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
// service
import { DataService } from "./data.service";
// models
import { Meeting } from "../../shared/models/meeting";


@Injectable()
export class MeetingService extends DataService<Meeting> {

  constructor(http: HttpClient) {
    super('', http);
  }
}
