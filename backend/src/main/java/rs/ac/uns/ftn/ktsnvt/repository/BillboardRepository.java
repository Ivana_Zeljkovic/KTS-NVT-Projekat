package rs.ac.uns.ftn.ktsnvt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.ktsnvt.model.entity.Billboard;

/**
 * Created by Ivana Zeljkovic on 13/11/2017.
 */
public interface BillboardRepository extends JpaRepository<Billboard, Long> {

}
