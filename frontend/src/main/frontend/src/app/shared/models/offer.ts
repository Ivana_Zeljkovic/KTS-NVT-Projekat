export interface Offer {
  id: number;
  businessId: number;
  price: number;
}
