import { Component } from '@angular/core';
import { ToasterConfig, ToasterService } from "angular5-toaster/dist";
import { ActivatedRoute } from "@angular/router";
import { BsModalService } from "ngx-bootstrap";
import * as _ from "lodash";
// model
import { Record } from "../../shared/models/record";
import { MeetingItem } from "../../shared/models/meeting-item";
import { PageAndSort } from "../../shared/models/page-and-sort";
import { QuestionnaireUpdate } from "../../shared/models/questionnaire-update";
// service
import { RecordService } from "../../core/services/record.service";
import { MeetingItemService } from "../../core/services/meeting-item.service";
import { JwtService } from "../../core/services/jwt.service";
// error
import { AppError } from "../../shared/errors/app-error";
import { BadRequestError } from "../../shared/errors/bad-request-error";
import { ForbiddenError } from "../../shared/errors/forbidden-error";
import { NotFoundError } from "../../shared/errors/not-found-error";
// component
import { QuestionnaireComponent } from "../../shared/components/questionnaire/questionnaire.component";


@Component({
  selector: 'app-record',
  templateUrl: './record.component.html',
  styleUrls: ['./record.component.css']
})
export class RecordComponent {
  record: Record;
  recordId: number;
  meetingItemList: Array<MeetingItem>;
  toasterConfig: ToasterConfig;

  // Elements for bootstrap paging
  currentPage: number;
  totalItems: number;
  maxSize: number;
  itemsPerPage: number;

  // Elements for getting data from server
  pageSize: string;
  sortDirection: string;
  sortProperties: string[];


  constructor(private route: ActivatedRoute, private recordService: RecordService,
              private meetingItemService: MeetingItemService, private toasterService: ToasterService,
              private jwtService: JwtService, private modalService: BsModalService) {
    this.toasterConfig = new ToasterConfig({
      timeout: {success: 2000, error: 4000}
    });

    // Sett up for bootstrap paging
    this.currentPage = 1;
    this.maxSize = 3;
    this.itemsPerPage = 4;

    // Sett up for getting damages request
    this.pageSize = '4';
    this.sortDirection = 'ASC';
    this.sortProperties = ['date'];

    this.route.params.subscribe(params => {
      if (params['id']) {
        this.recordId = params['id'];
        this.loadRecord();
      }
    });
  }

  viewQuestionnaire(meetingItem) {
    const modalRef = this.modalService.show(QuestionnaireComponent);

    modalRef.content.isReviewMode = true;
    modalRef.content.model = new QuestionnaireUpdate(meetingItem.questionnaire.id, meetingItem.questionnaire.questions);
    modalRef.content.initialValues = _.cloneDeep(modalRef.content.model);
  }

  pageChanged(event: any) {
    this.currentPage = event.page;
    this.loadMeetingItems();
  }

  private loadRecord() {
    this.recordService.get(this.recordId, 'buildings', this.jwtService.getBuildingIdFromToken(), 'council/records')
      .subscribe(record => {
        this.record = record;
        this.loadMeetingItems();
      }, (error: AppError) => {
        if(error instanceof BadRequestError)
          this.toasterService.pop('error', 'Error', 'Selected record doesn\'t belong to selected building!');
        else if(error instanceof ForbiddenError)
          this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
        else if(error instanceof NotFoundError)
          this.toasterService.pop('error', 'Error', 'Record not found');
        else {
          this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
          throw error;
        }
      })
  }

  private loadMeetingItems() {
    let page = new PageAndSort((this.currentPage - 1).toString(), this.pageSize, this.sortDirection, this.sortProperties);

    this.meetingItemService.getPage(page, 'buildings', this.jwtService.getBuildingIdFromToken(), 'meetings',
      this.record.meetingId, 'record/meeting_items')
      .subscribe(meetingItems => {
        if (meetingItems.length > 0)
          this.totalItems = meetingItems[0].numberOfElements;
        else this.totalItems = 0;
        this.meetingItemList = meetingItems;
      }, (error: AppError) => {
        if(error instanceof BadRequestError)
          this.toasterService.pop('error', 'Error', 'Record for this meeting isn\'t created yet!!');
        else if(error instanceof ForbiddenError)
          this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
        else if(error instanceof NotFoundError)
          this.toasterService.pop('error', 'Error', 'Meeting not found');
        else {
          this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
          throw error;
        }
      });
  }
}
