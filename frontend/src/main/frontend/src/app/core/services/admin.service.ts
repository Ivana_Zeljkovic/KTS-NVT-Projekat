import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
// service
import { DataService } from "./data.service";
// models
import { Administrator } from "../../shared/models/administrator";


@Injectable()
export class AdminService extends DataService<Administrator>{

  constructor(http: HttpClient) {
    super('admins', http);
  }

}
