import {by, element, ElementArrayFinder, ElementFinder} from "protractor";

export class MeetingItemListPage {
  // get methods
  getMeetingItemListElement(): ElementFinder {
    return element(by.tagName('app-meeting-item-list'));
  }

  getSearchDateRangeInput(): ElementFinder {
    return element(by.id('fromToDate'));
  }

  getSearchDateRangeClearButton(): ElementFinder {
    return element(by.buttonText('Clear'));
  }

  getSearchDateRangeSearchButton(): ElementFinder {
    return element(by.buttonText('Search'));
  }

  getAddButton(): ElementFinder {
    return element(by.buttonText('Add'));
  }

  getMeetingItemList(): ElementFinder {
    return element(by.id('meetingItemList'));
  }

  getMessageNoMeetingItemInList(): ElementFinder {
    return element(by.id('messageEmptyList'));
  }

  getPanelsFromMeetingItemList(): ElementArrayFinder {
    return element.all(by.css('div.panel'));
  }

  getPaginatorInMeetingItemList(): ElementFinder {
    return element(by.tagName('pagination'));
  }

  getTitleFromMeetingItemPanel(index: number): ElementFinder {
    return element(by.css(`#meetingItemList > .panel:nth-child(${index+1}) .meeting-item-title`));
  }

  getCreatorNameFromMeetingItemPanel(index: number): ElementFinder {
    return element(by.css(`#meetingItemList > .panel:nth-child(${index+1}) .meeting-item-metadata span:nth-child(2)`));
  }

  getDateFromMeetingItemPanel(index: number): ElementFinder {
    return element(by.css(`#meetingItemList > .panel:nth-child(${index+1}) .meeting-item-metadata span:nth-child(4)`));
  }

  getContentFromMeetingItemPanel(index: number): ElementFinder {
    return element(by.css(`#meetingItemList > .panel:nth-child(${index+1}) .panel-body`));
  }

  getMenuFromMeetingItemPanel(index: number): ElementFinder {
    return element(by.css(`#meetingItemList > .panel:nth-child(${index+1}) .meeting-item-menu`));
  }

  getContentOfMenuFromMeetingItemPanel(index: number): ElementFinder {
    return element(by.css(`#meetingItemList > .panel:nth-child(${index+1}) .meeting-item-menu #meetingItemPopover`))
  }

  getMenuOptionsFromMeetingItemPanel(): ElementArrayFinder {
    return element.all(by.css('.meeting-item-menu div.ng-star-inserted > a'));
  }

  getQuestionnaireReviewOptionFromMeetingItemPanel(index: number): ElementFinder {
    return element(by.css(`#meetingItemList > .panel:nth-child(${index+1}) .meeting-item-menu div:first-of-type > a`));
  }

  getEditMenuOptionFromMeetingItemPanel(index: number, orderNumber: number): ElementFinder {
    return element(by.css(`#meetingItemList > .panel:nth-child(${index+1}) .meeting-item-menu div:nth-child(${orderNumber}) > a`));
  }

  getDeleteMenuOptionFromMeetingItemPanel(index: number): ElementFinder {
    return element(by.css(`#meetingItemList > .panel:nth-child(${index+1}) .meeting-item-menu div:last-of-type > a`));
  }
}

