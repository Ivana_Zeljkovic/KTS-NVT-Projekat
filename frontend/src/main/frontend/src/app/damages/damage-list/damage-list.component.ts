import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { Subscription } from "rxjs/Subscription";
import { BsDatepickerConfig, BsLocaleService, BsModalService, defineLocale, enGb } from "ngx-bootstrap";
import { ToasterConfig, ToasterService } from "angular5-toaster/dist";
import * as _ from "lodash";
// service
import { DamagesCategoryService } from "../../core/services/damages-category.service";
import { JwtService } from "../../core/services/jwt.service";
import { DamageService } from "../../core/services/damage.service";
import { OfferService } from '../../core/services/offer.service';
// model
import { Damage } from "../../shared/models/damage";
import { DamageCreate } from "../../shared/models/damage-create";
import { DamageUpdate } from "../../shared/models/damage-update";
import { PageAndSort } from "../../shared/models/page-and-sort";
// error
import { AppError } from "../../shared/errors/app-error";
import { ForbiddenError } from "../../shared/errors/forbidden-error";
import { NotFoundError } from "../../shared/errors/not-found-error";
import { BadRequestError } from "../../shared/errors/bad-request-error";
// component
import { DamageAddComponent } from "../damage-add/damage-add.component";
import { ConfirmDeleteModalComponent } from "../../shared/components/confirm-delete-modal/confirm-delete-modal.component";
import { DamageEditComponent } from "../damage-edit/damage-edit.component";
import { DelegateResponsibilityToPersonComponent } from "../../shared/components/delegate-responsibility-to-person/delegate-responsibility-to-person.component";
import { DelegateResponsibilityToInstitutionComponent } from "../../shared/components/delegate-responsibility-to-institution/delegate-responsibility-to-institution.component";
import { BusinessListModalComponent } from '../business-list-modal/business-list-modal.component';
import { FixDamageListModalComponent } from '../fix-damage-list-modal/fix-damage-list-modal.component';
import { DamageTypesModalComponent } from "../damage-types-modal/damage-types-modal.component";
// pipe
import { DateTimeFormatServerPipe } from "../../shared/pipes/date-time-format-server.pipe";


@Component({
  selector: 'app-damage-list',
  templateUrl: './damage-list.component.html',
  styleUrls: ['./damage-list.component.css']
})
export class DamageListComponent implements OnInit, OnDestroy {
  @Input() insideHomeComponent: boolean;
  category: string;
  damageList: Array<Damage>;
  isSearchMode: boolean;
  dateTimeFormatPipe: DateTimeFormatServerPipe;

  roles: string[];
  urlFirstSuffix: string;
  urlSecondSuffix: string;
  id: number;
  title: string;
  subscription: Subscription;
  toasterConfig: ToasterConfig;
  daterangepickerModel: Date[];

  // Daterangepicker configuration
  bsConfig: Partial<BsDatepickerConfig>;
  maxDate: Date;

  // Elements for bootstrap paging
  currentPage: number;
  totalItems: number;
  maxSize: number;
  itemsPerPage: number;

  // Elements for getting data from server
  pageSize: string;
  sortDirection: string;
  sortProperties: string[];


  constructor(private damagesCategory: DamagesCategoryService, private jwtService: JwtService,
              private damageService: DamageService, private toasterService: ToasterService,
              private localeService: BsLocaleService, private modalService: BsModalService,
              private router: Router, private offerService: OfferService) {
    // Daterangepicker config
    this.bsConfig = Object.assign({}, {containerClass: 'theme-dark-blue'});
    this.maxDate = new Date();
    defineLocale('enGb', enGb);
    this.localeService.use('enGb');
    this.daterangepickerModel = [];
    this.dateTimeFormatPipe = new DateTimeFormatServerPipe();

    // toaster config
    this.toasterConfig = new ToasterConfig({
      timeout: {success: 4000, error: 4000, warning: 4000}
    });

    // Sett up for bootstrap paging
    this.currentPage = 1;
    this.maxSize = 3;
    this.itemsPerPage = 4;

    // Sett up for getting damages request
    this.pageSize = '4';
    this.sortDirection = 'ASC';
    this.sortProperties = ['urgent', 'date'];
  }

  ngOnInit() {
    if(!this.insideHomeComponent)
      this.insideHomeComponent = false;
    this.roles = this.jwtService.getRolesFromToken();

    this.subscription = this.damagesCategory.currentCategory.subscribe(category => {
      if(!(this.insideHomeComponent && category === 'responsibleFor')) {
        if (category !== '') {
          this.category = category;
        }
        else {
          if (this.roles.indexOf('TENANT') != -1)
            this.category = 'responsibleFor';
          else if (this.roles.indexOf('COMPANY') != -1)
            this.category = 'forFixing';
          else if (this.roles.indexOf('INSTITUTION') != -1) {
            if (this.router.url.startsWith('/home'))
              this.category = 'forFixing';
            else if (this.router.url.startsWith('/damages'))
              this.category = 'responsibleFor';
          }
        }
        this.checkCategory();
        // when user change category of damages, go back on first page
        this.currentPage = 1;
      }
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  add() {
    const modalRef = this.modalService.show(DamageAddComponent);
    modalRef.content.model = new DamageCreate('', 'OTHER', false);
    modalRef.content.initialValues = _.cloneDeep(modalRef.content.model);

    modalRef.content.submited.subscribe((damageCreated) => {
      if(damageCreated) {
        this.currentPage = 1;
        this.loadDamageList();
      }
    });
  }

  details(damage) {
    this.router.navigate(['damages', damage.id]);
  }

  edit(damage) {
    let index = this.damageList.indexOf(damage);

    const modalRef = this.modalService.show(DamageEditComponent);
    modalRef.content.model = new DamageUpdate(damage.description, damage.urgent);
    modalRef.content.initialValues = _.cloneDeep(modalRef.content.model);
    modalRef.content.damageId = damage.id;

    modalRef.content.submited.subscribe(updatedDamage => {
      if(damage.urgent !== updatedDamage.urgent)
        this.loadDamageList();
      else
        this.damageList[index] = updatedDamage;
    });
  }

  remove(damage) {
    const modalRef = this.modalService.show(
      ConfirmDeleteModalComponent,
      Object.assign({},{ class: 'modal-sm' })
    );

    let numberOfPages = Math.ceil(damage.numberOfElements / +(this.pageSize));
    let pageShouldDecrement = false;
    if(numberOfPages === this.currentPage && (damage.numberOfElements - 1) % +(this.pageSize) === 0) {
      pageShouldDecrement = true;
    }

    modalRef.content.confirmed.subscribe((value) => {
      if(value) {
        this.damageService.remove(damage.id, 'tenants', this.jwtService.getIdFromToken(), 'my_damages')
          .subscribe(() => {
            if(pageShouldDecrement && this.currentPage !== 1)
              this.currentPage--;
            this.loadDamageList();
          }, (error: AppError) => {
            if(error instanceof BadRequestError)
              this.toasterService.pop('error', 'Error',
                'Selected damage is already in fixing process/connected to some meeting-details items!');
            else if(error instanceof ForbiddenError)
              this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
            else if(error instanceof NotFoundError)
              this.toasterService.pop('error', 'Error', 'Damage not found!');
            else {
              this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
              throw error;
            }
          });
      }
    });
  }

  delegateToInstitution(damage) {
    const modalRef = this.modalService.show(DelegateResponsibilityToInstitutionComponent);
    modalRef.content.setResponsibility = false;
    modalRef.content.isDelegatingDamage = true;
    modalRef.content.damage = damage;

    modalRef.content.responsibilitySubmitted.subscribe(responsibilityDelegated => {
      if(responsibilityDelegated)
        this.loadDamageList();
    });
  }

  delegateToPerson(damage) {
    const modalRef = this.modalService.show(DelegateResponsibilityToPersonComponent);
    modalRef.content.setResponsibility = false;
    modalRef.content.isDelegatingDamage = true;
    modalRef.content.damage = damage;

    modalRef.content.responsibilitySubmitted.subscribe(responsibilityDelegated => {
      if(responsibilityDelegated)
        this.loadDamageList();
    });
  }

  setResponsibleInstitution(damage) {
    let index = this.damageList.indexOf(damage);

    const modalRef = this.modalService.show(DelegateResponsibilityToInstitutionComponent);
    modalRef.content.setResponsibility = true;
    modalRef.content.isDelegatingDamage = true;
    modalRef.content.damage = damage;

    modalRef.content.damageResponsibilitySet.subscribe(updatedDamage => {
      this.damageList[index] = updatedDamage;
    })
  }

  showDamageTypeResponsibilities(isTenant: boolean) {
    const modalRef = this.modalService.show(DamageTypesModalComponent);
    modalRef.content.isTenant = isTenant;
  }

  findFirmOrInstitution(damage) {
    const modalRef = this.modalService.show(FixDamageListModalComponent);
    modalRef.content.damage = damage;

    modalRef.content.businessChosen.subscribe(damage => {
      this.loadDamageList();
    });
  }

  sendDamageToBusinesses(damage: Damage) {
    const modalRef = this.modalService.show(BusinessListModalComponent, Object.assign({damage: damage}));
    modalRef.content.damage = damage;
  }

  declareDamageFixed(damage: Damage) {
    this.offerService.getOfferByBuildingAndDamageId(this.jwtService.getIdFromToken(), damage.id)
        .subscribe(data => {
          this.damageService.updatePart({offerId: data ? data.id : null}, 'buildings', damage.buildingId,
            'damages', damage.id, 'declare_damage_fixed')
            .subscribe(() => {
                this.loadDamageList();
            },(error2: AppError) => {
                if(error2 instanceof ForbiddenError)
                  this.toasterService.pop('error', 'Error','You don\'t have permission for this action!' +
                    '(You are not selected firm/institution for fixing selected damage)' );
                else if(error2 instanceof BadRequestError)
                  this.toasterService.pop('error', 'Error', 'The selected offer doesn\'t belong to the selected damage!');
                else {
                  this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
                  throw error2;
                }
            });
        }, (error: AppError) => {
          if(error instanceof ForbiddenError)
            this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
          else {
            this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
            throw error;
          }
        });
  }

  pageChanged(event: any) {
    this.currentPage = event.page;
    this.loadDamageList();
  }

  reset() {
    this.isSearchMode = false;
    this.daterangepickerModel = [];
    this.currentPage = 1;
    this.loadDamageList();
  }

  search() {
    this.isSearchMode = true;
    if(this.daterangepickerModel.length > 0) {
      this.currentPage = 1;
      this.loadDamageList();
    }
  }

  setTime() {
    if(this.daterangepickerModel.length > 0) {
      this.daterangepickerModel[0].setHours(0, 0, 0);
      this.daterangepickerModel[1].setHours(23, 59, 59);
    }
  }

  private checkCategory() {
    this.urlFirstSuffix = '';
    this.urlSecondSuffix = '';
    this.id = this.jwtService.getIdFromToken();

    switch (this.category) {
      case 'responsibleFor': {
        this.title = 'Damages I am responsible';
        if(this.roles.indexOf('TENANT') != -1) {
          this.urlFirstSuffix = 'tenants';
          this.title += ' person for';
        }
        else if(this.roles.indexOf('INSTITUTION') != -1) {
          this.urlFirstSuffix = 'institutions';
          this.title += ' institution for';
        }
        this.urlSecondSuffix = 'responsible_for_damages';
        break;
      }
      case 'inMyPersonalApartments': {
        this.title = 'Damages in my personal apartments';
        this.urlFirstSuffix = 'tenants';
        this.urlSecondSuffix = 'personal_apartments/damages';
        break;
      }
      case 'inMyApartment': {
        this.title = 'Damages in apartment I live in';
        this.urlFirstSuffix = 'tenants';
        this.urlSecondSuffix = 'apartment_live_in/damages';
        break;
      }
      case 'inMyBuilding': {
        this.title = 'Damages in my building';
        this.urlFirstSuffix = 'buildings';
        this.urlSecondSuffix = 'damages';
        this.id = this.jwtService.getBuildingIdFromToken();
        break;
      }
      case 'reported': {
        this.title = 'Damages that I reported';
        this.urlFirstSuffix = 'tenants';
        this.urlSecondSuffix = 'my_damages';
        break;
      }
      case 'forFixing': {
        this.title = 'Damages that I should repair';
        this.urlFirstSuffix = 'businesses';
        this.urlSecondSuffix = 'damages_for_repair';
        break;
      }
    }
    this.loadDamageList();
  }

  private loadDamageList() {
    let page = new PageAndSort((this.currentPage - 1).toString(), this.pageSize, this.sortDirection, this.sortProperties);

    if(!this.isSearchMode) {
      // getting page, without date search
      this.damageService.getPage(page, this.urlFirstSuffix, this.id, this.urlSecondSuffix)
        .subscribe(damages => {
          if (damages.length > 0)
            this.totalItems = damages[0].numberOfElements;
          else this.totalItems = 0;
          this.damageList = damages;
        }, (error: AppError) => {
          if (error instanceof ForbiddenError)
            this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
          else if (error instanceof NotFoundError)
            this.toasterService.pop('error', 'Error', 'Tenant/Institution/Firm not found!');
          else {
            this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
            throw error;
          }
        });
    }
    else {
      // getting page, but with date search
      let from = this.dateTimeFormatPipe.transform(this.daterangepickerModel[0]);
      let to = this.dateTimeFormatPipe.transform(this.daterangepickerModel[1]);
      this.damageService.searchFromTo(this.urlFirstSuffix, this.id, this.urlSecondSuffix, from, to, page)
        .subscribe(damages => {
          if(damages.length !== 0)
            this.totalItems = damages[0].numberOfElements;
          else this.totalItems = 0;
          this.damageList = damages;
        }, (error: AppError) => {
          if(error instanceof BadRequestError)
            this.toasterService.pop('error', 'Error', 'Bad format of dates in date range!');
          else if(error instanceof ForbiddenError)
            this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
          else if(error instanceof NotFoundError) {
            let message: string;
            if(this.roles.indexOf('TENANT') !== -1) {
              message = this.roles.indexOf('COUNCIL_PRESIDENT') !== -1 ? 'Building' : 'Tenant';
            }
            else if(this.roles.indexOf('INSTITUTION') !== -1)
              message = 'Institution';
            else if(this.roles.indexOf('COMPANY') !== -1)
              message = 'Firm';
            this.toasterService.pop('error', 'Error', `${message} not found!`);
          }
          else {
            this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
            throw error;
          }
        });
    }
  }
}
