package rs.ac.uns.ftn.ktsnvt.web.controller;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Building;
import rs.ac.uns.ftn.ktsnvt.model.entity.Meeting;
import rs.ac.uns.ftn.ktsnvt.model.entity.Record;
import rs.ac.uns.ftn.ktsnvt.service.BuildingService;
import rs.ac.uns.ftn.ktsnvt.service.MeetingService;
import rs.ac.uns.ftn.ktsnvt.service.RecordService;
import rs.ac.uns.ftn.ktsnvt.service.TenantService;
import rs.ac.uns.ftn.ktsnvt.web.dto.RecordCreateDTO;
import rs.ac.uns.ftn.ktsnvt.web.dto.RecordDTO;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping(value = "/api")
@Api(value="records", description="Operations pertaining to records in application.")
public class  RecordController {

    @Autowired
    private MeetingService meetingService;

    @Autowired
    private BuildingService buildingService;

    @Autowired
    private TenantService tenantService;

    @Autowired
    private RecordService recordService;



    @RequestMapping(
            value = "/buildings/{buildingId}/council/meetings/{meetingId}/record",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Create a record resource.",
            notes = "You have to provide valid IDs for building and meeting in the URL. " +
                    "Method returns the meeting with record being saved.",
            httpMethod = "POST",
            produces = "application/json",
            consumes = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully created record", response = RecordDTO.class),
            @ApiResponse(code = 400, message = "Inappropriate object sent in request body"),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 403, message = "You don't have permission to create resource"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('COUNCIL_PRESIDENT')")
    public ResponseEntity createRecord(
            @ApiParam(value = "The record object.", required = true) @Valid @RequestBody RecordCreateDTO recordCreateDTO,
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The ID of the existing meeting resource.", required = true) @PathVariable("meetingId") Long meetingId,
            @ApiParam(value = "The object that contains all errors from validation of DTO object") BindingResult errors) {

        Building building = this.buildingService.findOne(buildingId);
        this.tenantService.checkPermissionForCurrentPresidentInBuilding(building);
        Meeting meeting = this.meetingService.findOne(meetingId);

        if(meeting.getRecord() != null)
            throw new BadRequestException("The record for selected meeting has already been created!");

        this.meetingService.checkMeetingFinished(meeting);

        Record record = new Record();
        record.setContent(recordCreateDTO.getContent());
        record.setMeeting(meeting);
        record = this.recordService.save(record);

        meeting.setRecord(record);
        this.meetingService.save(meeting);

        return new ResponseEntity<>(new RecordDTO(record), HttpStatus.CREATED);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/council/records/{recordId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a single record.",
            notes = "You have to provide valid IDs for building and record in the URL.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved record", response = RecordDTO.class),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 403, message = "You don't have permission to create resource"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity getRecordById(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The ID of the existing record resource.", required = true) @PathVariable("recordId") Long recordId) {

        Building building = this.buildingService.findOne(buildingId);
        this.tenantService.checkPermissionForCurrentTenantInBuilding(building.getId());

        Record record = this.recordService.findOne(recordId);
        this.recordService.checkCouncilForRecord(record.getId(), building.getCouncil().getId());

        return new ResponseEntity<>(new RecordDTO(record), HttpStatus.OK);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/council/records",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get list of all records of the meetings which are finished, in page form.",
            notes = "You have to provide a valid building ID in the URL.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of records", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 403, message = "You don't have permission to create resource"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity getRecords(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The number of page for getting records.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The size of page for getting records.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting records.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "The sort property for getting records.", required = true) @RequestParam String sortProperty) {

        Building building = this.buildingService.findOne(buildingId);
        this.tenantService.checkPermissionForCurrentTenantInBuilding(building.getId());

        Page<Record> records = this.recordService.findAllByBuildingId(buildingId, Integer.valueOf(pageNumber),
                Integer.valueOf(pageSize), sortDirection, sortProperty);

        ArrayList<RecordDTO> recordsDTO = new ArrayList<>();
        records.forEach(record -> recordsDTO.add(new RecordDTO(record, records.getTotalElements())));

        return new ResponseEntity<>(recordsDTO, HttpStatus.OK);
    }
}
