import { browser } from "protractor";
// page
import { DamageListPage } from "../../page-objects/damage-list.po";
import { LoginPage } from "../../page-objects/login.po";
import { NavbarPage } from "../../page-objects/navbar.po";
import { DamageEditPage } from "../page-objects/damage-edit.po";
import { DamageAddPage } from "../page-objects/damage-add.po";
import { ConfirmDeletePage } from "../../page-objects/confirm-delete.po";


describe('Edit existing damage', () => {
  let damageListPage: DamageListPage;
  let damageEditPage: DamageEditPage;
  let damageAddPage: DamageAddPage;
  let loginPage: LoginPage;
  let navbarPage: NavbarPage;
  let confirmDeletePage: ConfirmDeletePage;
  let existingDamage: any;

  beforeAll(() => {
    loginPage = new LoginPage();
    navbarPage = new NavbarPage();
    damageListPage = new DamageListPage();
    damageEditPage = new DamageEditPage();
    damageAddPage = new DamageAddPage();
    confirmDeletePage = new ConfirmDeletePage();

    existingDamage = {'description':'Some description', 'urgency':'true'};

    browser.get('/');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');

    loginPage.setUsername('kaca');
    loginPage.setPassword('kaca');
    loginPage.getSignInButton().click();

    // before all of these tests we should add damage in list
    browser.wait(function() {
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/home/notifications';
      });
    }).then(function() {
      expect(navbarPage.getNavbarElement().isDisplayed()).toBe(true);
      expect(navbarPage.getDamageDropdownLink().isDisplayed()).toBe(true);
      navbarPage.getDamageDropdownLink().click();

      browser.wait(function() {
        return navbarPage.getContentOfDamageDropdownLink().isDisplayed();
      }).then(function() {
        expect(navbarPage.getDamagesIReportedLink().isDisplayed()).toBe(true);
        navbarPage.getDamagesIReportedLink().click();

        browser.wait(function () {
          return damageListPage.getDamageListElement().isDisplayed();
        }).then(function () {
          //expect to see enable button Add on this page
          expect(damageListPage.getAddButton().isDisplayed()).toBe(true);
          expect(damageListPage.getAddButton().isEnabled()).toBe(true);

          damageListPage.getAddButton().click();

          browser.wait(function () {
            return damageAddPage.getDamageAddElement().isDisplayed();
          }).then(function () {
            damageAddPage.setDamageDescription(existingDamage.description);
            damageAddPage.getDamageUrgent(1).click();

            expect(damageAddPage.getSaveButton().isEnabled()).toBe(true);
            damageAddPage.getSaveButton().click();

            // expect to see this damage as first in damage list (with urgent label)
            expect(damageListPage.getDamageDescriptionFromDamagePanel(0).getText()).toEqual(existingDamage.description);
            expect(damageListPage.getLabelUrgentFromDamagePanel(0).isDisplayed()).toBe(true);
          });
        });
      });
    });
  });

  afterAll(() => {
    // after all these tests, we should remove added damage
    // delete option is third in option list (in menu)
    expect(damageListPage.getMenuFromDamagePanel(0).isDisplayed()).toBe(true);
    damageListPage.getMenuFromDamagePanel(0).click();

    browser.wait(function() {
      // delete is third option in damage menu
      return damageListPage.getLinkFromDamagePanel(0, 3).isDisplayed();
    }).then(function() {
      damageListPage.getLinkFromDamagePanel(0, 3).click();

      browser.wait(function() {
        // expect to show modal dialog for delete confirmation
        return confirmDeletePage.getConfirmDeleteElement().isDisplayed();
      }).then(function() {
        // expect to modal has all elements
        expect(confirmDeletePage.getConfirmButton().isDisplayed()).toBe(true);
        expect(confirmDeletePage.getConfirmButton().isEnabled()).toBe(true);
        expect(confirmDeletePage.getDeclineButton().isDisplayed()).toBe(true);
        expect(confirmDeletePage.getDeclineButton().isEnabled()).toBe(true);

        confirmDeletePage.getConfirmButton().click();
        browser.executeScript('window.localStorage.clear();');
      });
    });
  });



  it('should open modal dialog for edit existing damage when user in on page with damages that are created ' +
    'by him/her, and click on option Edit in damage menu', () => {
    expect(damageListPage.getMenuFromDamagePanel(0).isDisplayed()).toBe(true);
    damageListPage.getMenuFromDamagePanel(0).click();

    browser.wait(function() {
      // edit is second option in damage menu
      return damageListPage.getLinkFromDamagePanel(0, 2).isDisplayed();
    }).then(function() {
      damageListPage.getLinkFromDamagePanel(0,2).click();

      browser.wait(function() {
        return damageEditPage.getDamageEditElement().isDisplayed();
      }).then(function() {
        // expect to see all elements on page for updating existing damage
        expect(damageEditPage.getTitle().isDisplayed()).toBe(true);
        expect(damageEditPage.getTitle().getText()).toEqual('Update damage');
        expect(damageEditPage.getDescriptionInput().isDisplayed()).toBe(true);
        expect(damageEditPage.getDescriptionInput().getAttribute('value')).toEqual(existingDamage.description);
        expect(damageEditPage.getDamageUrgencySelector().isDisplayed()).toBe(true);
        damageEditPage.getDamageUrgencySelector().getAttribute('ng-reflect-model').then(value => {
          expect(value).toEqual(existingDamage.urgency);
        });
        expect(damageAddPage.getSaveButton().isDisplayed()).toBe(true);
        expect(damageAddPage.getSaveButton().isEnabled()).toBe(false);
        expect(damageAddPage.getResetButton().isDisplayed()).toBe(true);
        expect(damageAddPage.getResetButton().isEnabled()).toBe(false);
        expect(damageAddPage.getCancelButton().isDisplayed()).toBe(true);
        expect(damageAddPage.getCancelButton().isEnabled()).toBe(true);
      });
    });
  });



  it('should enable Save and Reset button when user change something in form', () => {
    damageEditPage.getDescriptionInput().clear();
    damageEditPage.setDescription('Some change');
    damageEditPage.getDamageUrgent(0).click();

    expect(damageEditPage.getSaveButton().isEnabled()).toBe(true);
    expect(damageEditPage.getResetButton().isEnabled()).toBe(true);
  });



  it('should reset all fields to initial values and disable Reset and Save buttons, when user click on ' +
    'Reset button', () => {
    damageEditPage.getResetButton().click();

    expect(damageEditPage.getDescriptionInput().getAttribute('value')).toEqual(existingDamage.description);
    damageEditPage.getDamageUrgencySelector().getAttribute('ng-reflect-model').then(value => {
      expect(value).toEqual(existingDamage.urgency);
    });
    expect(damageEditPage.getSaveButton().isEnabled()).toBe(false);
    expect(damageEditPage.getResetButton().isEnabled()).toBe(false);

    // click cancel to close modal, so damage can be deleted in afterAll method
    damageEditPage.getCancelButton().click();
  });
});
