package rs.ac.uns.ftn.ktsnvt.web.dto;

import rs.ac.uns.ftn.ktsnvt.model.entity.Record;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Ivana Zeljkovic on 11/12/2017.
 */
public class RecordDTO implements Serializable {

    private Long id;
    private String content;
    private Date meetingDate;
    private Long meetingId;
    private long numberOfElements;

    public RecordDTO() { }

    public RecordDTO(Record record) {
        this.id = record.getId();
        this.content = record.getContent();
        this.meetingDate = record.getMeeting().getDate();
        this.meetingId = record.getMeeting().getId();
    }

    public RecordDTO(Record record, long numberOfElements) {
        this.id = record.getId();
        this.content = record.getContent();
        this.meetingDate = record.getMeeting().getDate();
        this.meetingId = record.getMeeting().getId();
        this.numberOfElements = numberOfElements;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getMeetingDate() { return meetingDate; }

    public void setMeetingDate(Date meetingDate) { this.meetingDate = meetingDate; }

    public Long getMeetingId() { return meetingId; }

    public void setMeetingId(Long meetingId) { this.meetingId = meetingId; }

    public long getNumberOfElements() { return numberOfElements; }

    public void setNumberOfElements(long numberOfElements) { this.numberOfElements = numberOfElements; }
}
