import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DamageTypesModalComponent } from './damage-types-modal.component';

describe('DamageTypesModalComponent', () => {
  let component: DamageTypesModalComponent;
  let fixture: ComponentFixture<DamageTypesModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DamageTypesModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DamageTypesModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
