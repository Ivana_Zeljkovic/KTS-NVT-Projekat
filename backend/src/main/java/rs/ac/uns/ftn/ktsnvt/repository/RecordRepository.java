package rs.ac.uns.ftn.ktsnvt.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import rs.ac.uns.ftn.ktsnvt.model.entity.Record;

import java.util.Date;

/**
 * Created by Nikola Garabandic on 22/11/2017.
 */
public interface RecordRepository extends JpaRepository<Record, Long> {

    @Query(value = "SELECT r FROM Record r WHERE r.meeting.date < :currentDate AND r.meeting.council.id = " +
            "(SELECT b.council.id FROM Building b WHERE b.id = :buildingId)")
    Page<Record> findAllByBuildingId(@Param("buildingId") Long buildingId, @Param("currentDate") Date currentDate,
                                     Pageable pageable);

    @Query(value = "SELECT r FROM Record r WHERE r.id = :recordId AND r.meeting.council.id = :councilId")
    Record findOneByIdAndCouncilId(@Param("recordId") Long recordId, @Param("councilId") Long councilId);
}
