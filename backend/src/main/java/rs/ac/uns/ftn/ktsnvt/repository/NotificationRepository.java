package rs.ac.uns.ftn.ktsnvt.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import rs.ac.uns.ftn.ktsnvt.model.entity.Notification;

import java.util.Date;

/**
 * Created by Ivana Zeljkovic on 13/11/2017.
 */
public interface NotificationRepository extends JpaRepository<Notification, Long> {

    @Query(value = "SELECT n FROM Notification n WHERE n.billboard.id = :id")
    Page<Notification> findAllByBillboard(@Param("id") Long billboardId, Pageable pageable);

    @Query("SELECT n FROM Notification n WHERE n.id = :id AND n.billboard.id = :billboardId")
    Notification findOneByIdAndBillboard(@Param("id") Long id, @Param("billboardId") Long billboardId);

    @Query(value = "SELECT n FROM Notification n WHERE n.billboard.id = :id AND n.date BETWEEN :fromDate AND :toDate")
    Page<Notification> findAllFromDateToDate(@Param("id") Long billboardId, @Param("fromDate") Date fromDate,
                                             @Param("toDate") Date toDate, Pageable pageable);
}
