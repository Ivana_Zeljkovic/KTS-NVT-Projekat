package rs.ac.uns.ftn.ktsnvt.repository.unit;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.model.entity.*;
import rs.ac.uns.ftn.ktsnvt.model.enumeration.BusinessType;
import rs.ac.uns.ftn.ktsnvt.repository.BuildingRepository;
import rs.ac.uns.ftn.ktsnvt.repository.DamageRepository;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Nikola Garabandic on 30/11/2017.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class DamageRepositoryUnitTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private DamageRepository damageRepository;

    private Long building_id, damage_id, inst_id, tenant_id, apartment_id;


    @Before
    public void setUp(){
        Building building = new Building();
        DamageType damageType = new DamageType("Naziv");
        Damage damage = new Damage("Opis", new Date(), damageType, false, null);
        Account account = new Account("gara", "gara");
        Tenant tenant = new Tenant("Nikola", "Garabandic", new Date(), "kantagara@gmail.com", "1280725", true, account);
        Business business = new Business("Inst1", "Opis", "123231", "mail@gmail.com", BusinessType.FIRE_SERVICE, false);

        Apartment apartment = new Apartment(10, 2, 40);
        apartment.setOwner(tenant);
        apartment.setBuilding(building);

        building.setDamages(new ArrayList<Damage>(){{add(damage);}});
        damage.setBuilding(building);

        damage.setResponsibleInstitution(business);
        damage.setSelectedBusiness(business);

        damage.setResponsiblePerson(tenant);
        damage.setApartment(apartment);

        damage.setCreator(tenant);



        entityManager.persist(damageType);
        entityManager.persist(account);
        this.tenant_id = entityManager.persistAndGetId(tenant, Long.class);
        this.inst_id = entityManager.persistAndGetId(business, Long.class);
        this.damage_id = entityManager.persistAndGetId(damage, Long.class);
        this.building_id = entityManager.persistAndGetId(building, Long.class);
        this.apartment_id = entityManager.persistAndGetId(apartment, Long.class);

        damage.setBusinessesDamageSentToIds(new ArrayList<>());
        damage.getBusinessesDamageSentToIds().add(inst_id);
        entityManager.merge(damage);

    }

    @Test
    public void testFindByBuildingId(){
        /*
         * Test proverava ispravnost rada metode findByBuildingId repozitorijuma DamageRepository
         * (pronalazenje neresenih kvarova koji pripadaju zgradi sa zadatim ID-em)
         * */

        // u zgradi sa ID-em building_id postoji jedan nereseni kvar sa ID-em damage_id
        Page<Damage> damages = this.damageRepository.findAllByBuilding(this.building_id,
                new PageRequest(0, 2, Sort.Direction.ASC, "id"));

        assertEquals(this.damage_id, damages.getContent().get(0).getId());
        assertEquals("Opis", damages.getContent().get(0).getDescription());
        assertEquals("Naziv", damages.getContent().get(0).getType().getName());
    }

    @Test
    public void testGetAllForInstitution()
    {
         /*
         * Test proverava ispravnost rada metode getAllForInstitution repozitorijuma DamageRepository
         * (pronalazenje neresenih kvarova koji pripadaju instituciji sa zadatim ID-em)
         * */

        Page<Damage> damages = this.damageRepository.getAllForInstitution(inst_id, new PageRequest(0, 2, Sort.Direction.ASC, "id"));
        assertEquals(this.damage_id, damages.getContent().get(0).getId());
        assertEquals("Opis", damages.getContent().get(0).getDescription());
        assertEquals("Naziv", damages.getContent().get(0).getType().getName());
    }

    @Test
    public void getAllForInstitutionFromTo()
    {
        /*
         * Test proverava ispravnost rada metode ForInstitutionFromTo repozitorijuma DamageRepository
         * (pronalazenje neresenih kvarova koji pripadaju instituciji sa zadatim ID-em, u vremenskom intervalu)
         *
         * */
        Calendar from = Calendar.getInstance();
        from.add(Calendar.YEAR, - 1);
        Calendar to = Calendar.getInstance();
        to.add(Calendar.YEAR, 1);

        Page<Damage> damages = this.damageRepository.getAllForInstitutionFromTo(inst_id,
                from.getTime(), to.getTime(),
                new PageRequest(0, 2, Sort.Direction.ASC, "id"));
        assertEquals(this.damage_id, damages.getContent().get(0).getId());
        assertEquals("Opis", damages.getContent().get(0).getDescription());
        assertEquals("Naziv", damages.getContent().get(0).getType().getName());
    }

    @Test
    public void testGetAllForRepair()
    {
         /*
         * Test proverava ispravnost rada metode getAllForRepair repozitorijuma DamageRepository
         * (pronalazenje kvarova koje firma/institucija sa zadatim id-em treba da popravi)
         * */

        Page<Damage> damages = this.damageRepository.getAllForRepair(this.inst_id,
                new PageRequest(0, 2, Sort.Direction.ASC, "id"));

        assertEquals(this.damage_id, damages.getContent().get(0).getId());
        assertEquals("Opis", damages.getContent().get(0).getDescription());
        assertEquals("Naziv", damages.getContent().get(0).getType().getName());
    }

    @Test
    public void testGetAllForRepairFromTo()
    {
          /*
         * Test proverava ispravnost rada metode getAllForRepairFromTo repozitorijuma DamageRepository
         * (pronalazenje kvarova koje firma/institucija sa zadatim id-em treba da popravi u odredjenom periodu)
         * */

        Calendar from = Calendar.getInstance();
        from.add(Calendar.YEAR, - 1);
        Calendar to = Calendar.getInstance();
        to.add(Calendar.YEAR, 1);

        Page<Damage> damages = this.damageRepository.getAllForRepairFromTo(inst_id,
                from.getTime(), to.getTime(),
                new PageRequest(0, 2, Sort.Direction.ASC, "id"));
        assertEquals(this.damage_id, damages.getContent().get(0).getId());
        assertEquals("Opis", damages.getContent().get(0).getDescription());
        assertEquals("Naziv", damages.getContent().get(0).getType().getName());
    }

    @Test
    public void testGetAllInBuildingFromTo()
    {
         /*
         * Test proverava ispravnost rada metode findByBuildingIdFromTo repozitorijuma DamageRepository
         * (pronalazenje neresenih kvarova koji pripadaju zgradi sa zadatim ID-em u odredjenom vremenskom intervalu)
         * */

        Calendar from = Calendar.getInstance();
        from.add(Calendar.YEAR, - 1);
        Calendar to = Calendar.getInstance();
        to.add(Calendar.YEAR, 1);

        Page<Damage> damages = this.damageRepository.getAllInBuildingFromTo(building_id,
                from.getTime(), to.getTime(),
                new PageRequest(0, 2, Sort.Direction.ASC, "id"));
        assertEquals(this.damage_id, damages.getContent().get(0).getId());
        assertEquals("Opis", damages.getContent().get(0).getDescription());
        assertEquals("Naziv", damages.getContent().get(0).getType().getName());
    }

    @Test
    public void testGeAllForTenant()
    {
        /*
         * Test proverava ispravnost rada metode getAllForTenant repozitorijuma DamageRepository
         * (pronalazenje neresenih kvarova za koje je stanar odgovoran)
         * */

        Page<Damage> damages = this.damageRepository.getAllForTenant(tenant_id,  new PageRequest(0, 2, Sort.Direction.ASC, "id"));
        assertEquals(this.damage_id, damages.getContent().get(0).getId());
        assertEquals("Opis", damages.getContent().get(0).getDescription());
        assertEquals("Naziv", damages.getContent().get(0).getType().getName());
    }

    @Test
    public void testGeAllForTenantFromTo()
    {
        /*
         * Test proverava ispravnost rada metode getAllForTenantFromTo repozitorijuma DamageRepository
         * (pronalazenje neresenih kvarova za koje je stanar odgovoran u odredjenom periodu)
         * */

        Calendar from = Calendar.getInstance();
        from.add(Calendar.YEAR, - 1);
        Calendar to = Calendar.getInstance();
        to.add(Calendar.YEAR, 1);

        Page<Damage> damages = this.damageRepository.getAllForTenantFromTo(tenant_id,from.getTime(), to.getTime(),
                new PageRequest(0, 2, Sort.Direction.ASC, "id"));
        assertEquals(this.damage_id, damages.getContent().get(0).getId());
        assertEquals("Opis", damages.getContent().get(0).getDescription());
        assertEquals("Naziv", damages.getContent().get(0).getType().getName());
    }

    @Test
    public void  testGetAllForPersonalApartments()
    {
        /*
        * Test proverava ispravnost rada metode getAllInPersonalApartments repozitorijuma DamageRepository
         * (pronalazenje neresenih kvarova u stanovima koje posedujem)
         * */

        Page<Damage> damages = this.damageRepository.getAllInPersonalApartments(tenant_id,  new PageRequest(0, 2, Sort.Direction.ASC, "id"));
        assertEquals(this.damage_id, damages.getContent().get(0).getId());
        assertEquals("Opis", damages.getContent().get(0).getDescription());
        assertEquals("Naziv", damages.getContent().get(0).getType().getName());

    }

    @Test
    public void  testGetAllForPersonalApartmentsFromTo()
    {
        /*
        * Test proverava ispravnost rada metode getAllInPersonalApartmentsFromTo repozitorijuma DamageRepository
         * (pronalazenje neresenih kvarova u stanovima koje posedujem u odredjenom vremenskom intervalu)
         * */
        Calendar from = Calendar.getInstance();
        from.add(Calendar.YEAR, - 1);
        Calendar to = Calendar.getInstance();
        to.add(Calendar.YEAR, 1);

        Page<Damage> damages = this.damageRepository.getAllInPersonalApartmentsFromTo(tenant_id,from.getTime(), to.getTime(),
                new PageRequest(0, 2, Sort.Direction.ASC, "id"));
        assertEquals(this.damage_id, damages.getContent().get(0).getId());
        assertEquals("Opis", damages.getContent().get(0).getDescription());
        assertEquals("Naziv", damages.getContent().get(0).getType().getName());

    }

    @Test
    public void testGetAllForApartment()
    {
        /*
        * Test proverava ispravnost rada metode GetAllForApartment repozitorijuma DamageRepository
         * (pronalazenje neresenih kvarova u stanu sa zadatim id-em)
         * */


        Page<Damage> damages = this.damageRepository.getAllInApartment(apartment_id,
                new PageRequest(0, 2, Sort.Direction.ASC, "id"));
        assertEquals(this.damage_id, damages.getContent().get(0).getId());
        assertEquals("Opis", damages.getContent().get(0).getDescription());
        assertEquals("Naziv", damages.getContent().get(0).getType().getName());
    }

    @Test
    public void testGetAllForApartmentFromTo()
    {
        /*
        * Test proverava ispravnost rada metode GetAllForApartmentFromTo repozitorijuma DamageRepository
         * (pronalazenje neresenih kvarova u stanu sa zadatim id-em u odredjenom vremenskom periodu)
         * */
        Calendar from = Calendar.getInstance();
        from.add(Calendar.YEAR, - 1);
        Calendar to = Calendar.getInstance();
        to.add(Calendar.YEAR, 1);

        Page<Damage> damages = this.damageRepository.getAllInApartmentFromTo(apartment_id, from.getTime(), to.getTime(),
                new PageRequest(0, 2, Sort.Direction.ASC, "id"));
        assertEquals(this.damage_id, damages.getContent().get(0).getId());
        assertEquals("Opis", damages.getContent().get(0).getDescription());
        assertEquals("Naziv", damages.getContent().get(0).getType().getName());
    }


    @Test
    public void testGetAllMyDamages()
    {
        /*
         * Test proverava ispravnost rada metode getAllMyDamages repozitorijuma DamageRepository
         * (pronalazenje neresenih kvarova koje sam ja prijavio)
         * */

        Page<Damage> damages = this.damageRepository.getAllMyDamages(tenant_id,  new PageRequest(0, 2, Sort.Direction.ASC, "id"));
        assertEquals(this.damage_id, damages.getContent().get(0).getId());
        assertEquals("Opis", damages.getContent().get(0).getDescription());
        assertEquals("Naziv", damages.getContent().get(0).getType().getName());
    }

    @Test
    public void testGetAllMyDamagesFromTo()
    {
         /*
         * Test proverava ispravnost rada metode getAllMyDamages repozitorijuma DamageRepository
         * (pronalazenje neresenih kvarova koje sam ja prijavio u odredjenom periodu)
         * */

        Calendar from = Calendar.getInstance();
        from.add(Calendar.YEAR, - 1);
        Calendar to = Calendar.getInstance();
        to.add(Calendar.YEAR, 1);

        Page<Damage> damages = this.damageRepository.getAllMyDamagesFromTo(tenant_id,from.getTime(), to.getTime(),
                new PageRequest(0, 2, Sort.Direction.ASC, "id"));
        assertEquals(this.damage_id, damages.getContent().get(0).getId());
        assertEquals("Opis", damages.getContent().get(0).getDescription());
        assertEquals("Naziv", damages.getContent().get(0).getType().getName());
    }

    @Test
    public void testFindDamageIdInOfferRequest()
    {
        /*
         * Test proverava ispravnost rada metode findDamageIdInOfferRequest repozitorijuma DamageRepository
         * (pronalazenje kvara za koji je trazena ponuda od strane neke firme ili institucije)
         * */

        Damage damage = this.damageRepository.findDamageIdInOfferRequest(damage_id, inst_id);

        assertNotNull(damage);
    }


}
