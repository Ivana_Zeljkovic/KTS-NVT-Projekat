package rs.ac.uns.ftn.ktsnvt.model.entity;

import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "meetingitem")
@Where(clause="deleted=0")
public class MeetingItem {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Column(nullable = false)
    private String title;

    @Column(nullable = false)
    private String content;

    @Column(nullable = false)
    private Date date;

    @Column(nullable = false, columnDefinition = "INTEGER DEFAULT 0")
    @Version
    private int version;

    @ManyToOne
    private Meeting meeting;

    @Column(nullable = false, columnDefinition = "BOOL DEFAULT FALSE")
    private boolean deleted;

    @OneToOne
    private Tenant creator;

    @ManyToOne
    private Billboard billboard;

    @OneToOne
    private Questionnaire questionnaire;

    @ManyToOne
    private Damage damage;

    public MeetingItem() { }

    public MeetingItem(String title, String content, Date date) {
        this.title = title;
        this.content = content;
        this.date = date;
    }

    public MeetingItem(String title, String content, Tenant creator, Billboard billboard,
                       Questionnaire questionnaire, Damage damage, Meeting meeting) {
        this.title = title;
        this.content = content;
        this.creator = creator;
        this.billboard = billboard;
        this.questionnaire = questionnaire;
        this.damage = damage;
        this.meeting = meeting;
    }

    public Meeting getMeeting() {
        return meeting;
    }

    public void setMeeting(Meeting meeting) {
        this.meeting = meeting;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() { return title; }

    public void setTitle(String title) { this.title = title; }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getDate() { return date; }

    public void setDate(Date date) { this.date = date; }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Tenant getCreator() {
        return creator;
    }

    public void setCreator(Tenant creator) {
        this.creator = creator;
    }

    public Billboard getBillboard() { return billboard; }

    public void setBillboard(Billboard billboard) { this.billboard = billboard; }

    public Questionnaire getQuestionnaire() { return questionnaire; }

    public void setQuestionnaire(Questionnaire questionnaire) { this.questionnaire = questionnaire; }

    public Damage getDamage() { return damage; }

    public void setDamage(Damage damage) { this.damage = damage; }
}
