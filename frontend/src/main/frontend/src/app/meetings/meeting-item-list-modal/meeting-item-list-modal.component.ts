import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { ToasterService } from "angular5-toaster/dist";
import * as _ from "lodash";
// service
import { MeetingItemService } from "../../core/services/meeting-item.service";
import { JwtService } from "../../core/services/jwt.service";
// model
import { MeetingItem } from "../../shared/models/meeting-item";
import { PageAndSort } from "../../shared/models/page-and-sort";
import { QuestionnaireUpdate } from "../../shared/models/questionnaire-update";
// error
import { AppError } from "../../shared/errors/app-error";
import { NotFoundError } from "../../shared/errors/not-found-error";
import { ForbiddenError } from "../../shared/errors/forbidden-error";
// component
import { QuestionnaireComponent } from "../../shared/components/questionnaire/questionnaire.component";


@Component({
  selector: 'app-meeting-item-list-modal',
  templateUrl: './meeting-item-list-modal.component.html',
  styleUrls: ['./meeting-item-list-modal.component.css']
})
export class MeetingItemListModalComponent implements OnInit {
  @Input() initialValues: Array<MeetingItem>;
  selectedMeetingItems: Array<MeetingItem>;
  selectedIds: Array<number>;
  allMeetingItems: Array<MeetingItem>;
  disableSave: boolean;
  @Output() submitted = new EventEmitter<Array<MeetingItem>>();

  // Elements for bootstrap paging
  currentPage: number;
  totalItems: number;
  maxSize: number;
  itemsPerPage: number;

  // Elements for getting data from server
  pageSize: string;
  sortDirection: string;
  sortProperties: string[];


  constructor(private meetingItemService: MeetingItemService, private toasterService: ToasterService,
              private jwtService: JwtService, private modalRef: BsModalRef,
              private modalService: BsModalService) {
    this.disableSave = true;

    // Sett up for bootstrap paging
    this.currentPage = 1;
    this.totalItems = 0;
    this.maxSize = 3;
    this.itemsPerPage = 3;

    // Sett up for getting damages request
    this.pageSize = '3';
    this.sortDirection = 'DESC';
    this.sortProperties = ['date'];
  }

  ngOnInit() {
    setTimeout(() => {
      this.selectedMeetingItems = _.cloneDeep(this.initialValues);
      this.selectedIds = new Array<number>();
      this.selectedMeetingItems.forEach((meetingItem: MeetingItem) => {
        this.selectedIds.push(meetingItem.id);
      })
    }, 1);
    this.loadMeetingItems();
  }

  select(meetingItem: MeetingItem) {
    this.selectedIds.push(meetingItem.id);
    this.selectedMeetingItems.push(meetingItem);
    this.checkChanges();
  }

  deselect(meetingItem: MeetingItem) {
    let index = this.selectedIds.indexOf(meetingItem.id);
    this.selectedIds.splice(index, 1);
    this.selectedMeetingItems.splice(index, 1);
    this.checkChanges();
  }

  save() {
    this.submitted.emit(this.selectedMeetingItems);
    this.modalRef.hide();
  }

  questionnaireReview(meetingItem) {
    const modalRef = this.modalService.show(QuestionnaireComponent);

    modalRef.content.isReviewMode = true;
    modalRef.content.model = new QuestionnaireUpdate(meetingItem.questionnaire.id, meetingItem.questionnaire.questions);
    modalRef.content.initialValues = _.cloneDeep(modalRef.content.model);
  }

  pageChanged(event: any) {
    this.currentPage = event.page;
    this.loadMeetingItems();
  }

  private checkChanges() {
    this.disableSave = (JSON.stringify(this.selectedMeetingItems) === JSON.stringify(this.initialValues));
  }

  private loadMeetingItems() {
    let page = new PageAndSort((this.currentPage - 1).toString(), this.pageSize, this.sortDirection, this.sortProperties);

    this.meetingItemService.getPage(page, 'buildings', this.jwtService.getBuildingIdFromToken(), 'billboard/meeting_items')
      .subscribe(meetingItems => {
        if(meetingItems.length !== 0)
          this.totalItems = meetingItems[0].numberOfElements;
        else this.totalItems = 0;
        this.allMeetingItems = meetingItems;
      }, (error: AppError) => {
        if(error instanceof ForbiddenError)
          this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
        else if(error instanceof NotFoundError)
          this.toasterService.pop('error', 'Error', 'Building not found!');
        else {
          this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
          throw error;
        }
      });
  }
}
