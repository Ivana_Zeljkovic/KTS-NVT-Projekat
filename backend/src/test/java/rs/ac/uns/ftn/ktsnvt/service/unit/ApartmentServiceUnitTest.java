package rs.ac.uns.ftn.ktsnvt.service.unit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Apartment;
import rs.ac.uns.ftn.ktsnvt.model.entity.Building;
import rs.ac.uns.ftn.ktsnvt.repository.ApartmentRepository;
import rs.ac.uns.ftn.ktsnvt.service.ApartmentService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.BDDMockito.given;

/**
 * Created by Katarina Cukurov on 30/11/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ApartmentServiceUnitTest {

    @Autowired
    private ApartmentService apartmentService;

    @MockBean
    private ApartmentRepository apartmentRepository;


    @Before
    public void setUp(){
        Apartment apartment = new Apartment(1, 1, 60);
        apartment.setId(100L);

        Apartment apartment_2 = new Apartment(2,2,60);
        apartment.setId(103L);

        Building building = new Building();
        building.setId(100L);
        building.getApartments().add(apartment);
        apartment.setBuilding(building);

        Building building_2 = new Building();
        building_2.setId(103L);
        building_2.getApartments().add(apartment_2);
        apartment_2.setBuilding(building_2);

        given(
                this.apartmentRepository.findOne(new Long(100))
        ).willReturn(
                apartment
        );

        given(
                this.apartmentRepository.findOne(new Long(103))
        ).willReturn(
                apartment_2
        );

        given(
                this.apartmentRepository.findOne(new Long(500))
        ).willReturn(
                null
        );
    }

    @Test
    public void testFindOne() {
        /*
         * Test proverava ispravnost rada metode findOne servisa ApartmentService
         * (dobavljanje stana na osnovu ID-a)
         * u slucaju kada postoji stan sa zadatim ID-em
         * */

        /*
        stan sa ID-em = 100 ima podatke:
            id = 100,
            number = 1,
            floor = 1,
            surface = 60
         */
        Apartment apartment = this.apartmentService.findOne(new Long(100));

        assertNotNull(apartment);
        assertEquals(1, apartment.getNumber());
        assertEquals(1, apartment.getFloor());
        assertEquals(60, apartment.getSurface());
    }

    @Test(expected = NotFoundException.class)
    public void testFindOneNonexistent() {
        /*
         * Test proverava ispravnost rada metode findOne servisa ApartmentService
         * (dobavljanje stana na osnovu ID-a)
         * u slucaju kada ne postoji stan sa zadatim ID-em - ocekivano NotFoundException
         * */

        // apartment sa ID-em = 500 ne postoji
        Apartment apartment = this.apartmentService.findOne(new Long(500));
    }

    @Test
    public void checkBuilding() {
        /*
         * Test proverava ispravnost rada metode checkBuilding servisa ApartmentService
         * (provera da li prosledjeni stan pripada zgradi ciji je ID prosledjen)
         * u slucaju kada prosledjeni stan pripada zgradi ciji je ID prosledjen u metodu
         * */

        /*
        stan sa ID-em = 100 pripada zgradi sa ID-em = 100 i ima podatke:
            id = 100,
            number = 1,
            floor = 1,
            surface = 60
            building_id = 100
        */
        Apartment apartment = this.apartmentService.findOne(new Long(100));
        this.apartmentService.checkBuilding(apartment, new Long(100));
    }

    @Test(expected = BadRequestException.class)
    public void checkBuildingInvalid() {
        /*
         * Test proverava ispravnost rada metode checkBuilding servisa ApartmentService
         * (provera da li prosledjeni stan pripada zgradi ciji je ID prosledjen)
         * u slucaju kada prosledjeni stan ne pripada zgradi ciji je ID prosledjen u metodu - ocekivano BadRequestException
         * */

        // stan sa ID-em = 103 ne pripada zgradi sa ID-em = 100
        Apartment apartment = this.apartmentService.findOne(new Long(103));
        this.apartmentService.checkBuilding(apartment, new Long(100));
    }
}
