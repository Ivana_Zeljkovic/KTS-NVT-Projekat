import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";
// component
import { DamageListComponent } from "./damage-list/damage-list.component";
import { DamageDetailsComponent } from "./damage-details/damage-details.component";


const routes: Routes = [
  { path: '', component: DamageListComponent },
  { path: ':id', component: DamageDetailsComponent },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class DamagesRouterModule { }
