package rs.ac.uns.ftn.ktsnvt.web.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by Nikola Garabandic on 24/11/2017.
 */
public class CommentCreateOrUpdateDTO implements Serializable {

    @NotNull
    private String content;

    public CommentCreateOrUpdateDTO() { }

    public CommentCreateOrUpdateDTO(String content) {
        this.content = content;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
