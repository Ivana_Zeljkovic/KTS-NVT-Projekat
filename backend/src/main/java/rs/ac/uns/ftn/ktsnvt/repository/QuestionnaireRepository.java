package rs.ac.uns.ftn.ktsnvt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import rs.ac.uns.ftn.ktsnvt.model.entity.Questionnaire;

/**
 * Created by Ivana Zeljkovic on 13/11/2017.
 */
public interface QuestionnaireRepository extends JpaRepository<Questionnaire, Long> {

    @Query(value = "SELECT q FROM Questionnaire q WHERE q.id = :id AND q.meetingItem.id IN " +
            "(SELECT m.id FROM MeetingItem m WHERE m.billboard.id = :billboardId)")
    Questionnaire findOneByIdAndBillboardId(@Param("id") Long id, @Param("billboardId") Long billboardId);

}
