import { Component } from '@angular/core';
import { ToasterConfig } from "angular5-toaster/dist";
// service
import { JwtService } from "../../core/services/jwt.service";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent {
  toasterConfig: ToasterConfig;

  constructor(private jwtService: JwtService) {
    this.toasterConfig = new ToasterConfig({
      timeout: {success: 2000, error: 4000, warning: 4000}
    });
  }
}
