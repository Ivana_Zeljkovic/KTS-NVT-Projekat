import { browser } from "protractor";
// page
import { NavbarPage } from "../page-objects/navbar.po";
import { LoginPage } from "../page-objects/login.po";
import { TenantProfilePage } from "../page-objects/tenant-profile.po";
import { AdminProfilePage } from "../page-objects/admin-profile.po";


describe('Navbar for user with any role', () => {
  let navbarPage: NavbarPage;
  let loginPage: LoginPage;
  let tenantProfilePage: TenantProfilePage;
  let adminProfilPage: AdminProfilePage;

  beforeEach(() => {
    navbarPage = new NavbarPage();
    loginPage = new LoginPage();

    browser.get('/');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');
  });

  afterEach(() => {
    browser.executeScript('window.localStorage.clear();');
  });



  it('should redirect user to login page when user is logged in and Sign out option from ' +
    'personal dropdown in navbar is selected', () => {
    tenantProfilePage = new TenantProfilePage();
    loginPage.setUsername('kaca');
    loginPage.setPassword('kaca');
    loginPage.getSignInButton().click();

    browser.wait(function() {
      // expect that browser redirect user to home page and navbar page is loaded
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/home/notifications' && navbarPage.getNavbarElement().isDisplayed();
      });
    }).then(function() {
      expect(navbarPage.getPersonalDropdown().isDisplayed()).toBe(true);
      navbarPage.getPersonalDropdown().click();

      expect(navbarPage.getLogoutLink().isDisplayed()).toBe(true);
      navbarPage.getLogoutLink().click();

      browser.wait(function() {
        // expect that browser redirect user to login page
        return browser.getCurrentUrl().then(value => {
          return value === 'http://localhost:49152/#/login';
        });
      }).then(function() {
        // expect to see login page after redirection
        expect(loginPage.getLoginPageElement().isDisplayed()).toBe(true);
      });
    });
  });



  it('should display tenant profile page when tenant is logged in and Profile option from ' +
    'personal dropdown in navbar is selected', () => {
    tenantProfilePage = new TenantProfilePage();
    loginPage.setUsername('kaca');
    loginPage.setPassword('kaca');
    loginPage.getSignInButton().click();

    browser.wait(function() {
      // expect that browser redirect user to home page and navbar page is loaded
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/home/notifications' && navbarPage.getNavbarElement().isDisplayed();
      });
    }).then(function() {
      expect(navbarPage.getPersonalDropdown().isDisplayed()).toBe(true);
      navbarPage.getPersonalDropdown().click();

      expect(navbarPage.getProfileLink().isDisplayed()).toBe(true);
      navbarPage.getProfileLink().click();


      browser.wait(function() {
        // expect that browser redirect user to profile page
        return browser.getCurrentUrl().then(value => {
          return value === 'http://localhost:49152/#/profile';
        });
      }).then(function() {
        // expect to see tenant profile page after redirection
        expect(tenantProfilePage.getTenantProfileElement().isDisplayed()).toBe(true);
      });
    });
  });



  it('should redirect administrator to login page when user is logged in and Sign out option from ' +
    'personal dropdown in navbar is selected', () => {

    loginPage.setUsername('ivana');
    loginPage.setPassword('ivana');
    loginPage.getSignInButton().click();

    browser.wait(function() {

      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/home' && navbarPage.getNavbarElement().isDisplayed();
      });
    }).then(function() {
      expect(navbarPage.getPersonalDropdown().isDisplayed()).toBe(true);
      navbarPage.getPersonalDropdown().click();

      expect(navbarPage.getLogoutLink().isDisplayed()).toBe(true);
      navbarPage.getLogoutLink().click();

      browser.wait(function() {

        return browser.getCurrentUrl().then(value => {
          return value === 'http://localhost:49152/#/login';
        });
      }).then(function() {
        expect(loginPage.getLoginPageElement().isDisplayed()).toBe(true);
      });
    });
  });



  it('should display admin profile page when admin is logged in and Profile option from ' +
    'personal dropdown in navbar is selected', () => {
    adminProfilPage = new AdminProfilePage();
    loginPage.setUsername('ivana');
    loginPage.setPassword('ivana');
    loginPage.getSignInButton().click();

    browser.wait(function() {

      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/home' && navbarPage.getNavbarElement().isDisplayed();
      });
    }).then(function() {
      expect(navbarPage.getPersonalDropdown().isDisplayed()).toBe(true);
      navbarPage.getPersonalDropdown().click();

      expect(navbarPage.getProfileLink().isDisplayed()).toBe(true);
      navbarPage.getProfileLink().click();


      browser.wait(function() {

        return browser.getCurrentUrl().then(value => {
          return value === 'http://localhost:49152/#/profile';
        });
      }).then(function() {

        expect(adminProfilPage.getAdminProfileElement().isDisplayed()).toBe(true);
      });
    });
  });
});
