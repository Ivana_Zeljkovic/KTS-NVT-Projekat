import {by, element, ElementArrayFinder, ElementFinder} from "protractor";

export class DelegateResponsibilityToPersonPage {
  // get methods
  getDelegateResponsibilityToPersonElement(): ElementFinder {
    return element(by.tagName('app-delegate-responsibility-to-person'));
  }

  getTitle(): ElementFinder {
    return element(by.css('app-delegate-responsibility-to-person .modal-title'));
  }

  getCloseButton(): ElementFinder {
    return element(by.css('div.modal-header > button'));
  }

  getTenantsTable(): ElementFinder {
    return element(by.id('tenantsTable'));
  }

  getTenantsTableRows(): ElementArrayFinder {
    return element.all(by.css('#tenantsTable tbody > tr'));
  }

  getTenantFirstName(index: number): ElementFinder {
    return element(by.css(`#tenantsTable > tbody > tr:nth-child(${index+1}) td:nth-child(1)`));
  }

  getTenantLastName(index: number): ElementFinder {
    return element(by.css(`#tenantsTable > tbody > tr:nth-child(${index+1}) td:nth-child(2)`));
  }

  getTenantUsername(index: number): ElementFinder {
    return element(by.css(`#tenantsTable > tbody > tr:nth-child(${index+1}) td:nth-child(3)`));
  }

  getCurrentResponsibleTenant(index: number): ElementFinder {
    return element(by.css(`#tenantsTable > tbody > tr:nth-child(${index+1}) td:nth-child(4) > span`));
  }

  getButtonDelegate(index: number): ElementFinder {
    return element(by.css(`#tenantsTable > tbody > tr:nth-child(${index+1}) button`));
  }
}
