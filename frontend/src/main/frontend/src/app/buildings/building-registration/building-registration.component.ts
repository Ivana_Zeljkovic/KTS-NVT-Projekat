import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {ActivatedRoute, Router} from "@angular/router";
import {ToasterService} from "angular5-toaster/dist";
// model
import {BuildingCreate} from "../../shared/models/building-create";
import {AddressCreate} from "../../shared/models/address-create";
import {CityCreate} from "../../shared/models/city-create";
import {Business} from "../../shared/models/business";
import {ApartmentCreate} from "../../shared/models/apartment-create";
// service
import {BuildingService} from "../../core/services/building.service";
// component
import {ChooseManagerModalComponent} from "../../shared/components/choose-manager-modal/choose-manager-modal.component";
import {AddApartmentModalComponent} from "../../shared/components/add-apartment-modal/add-apartment-modal.component";
//error
import {AppError} from "../../shared/errors/app-error";
import {BadRequestError} from "../../shared/errors/bad-request-error";
import {SpaceValidator} from "../../shared/validators/space.validator";

@Component({
  selector: 'app-building-registration',
  templateUrl: './building-registration.component.html',
  styleUrls: ['./building-registration.component.css']
})
export class BuildingRegistrationComponent implements OnInit {

  form: FormGroup;
  returnURL: string;
  apartments: ApartmentCreate[];
  bsModalRef: BsModalRef;
  floorsList: number[];
  numberOfFloors: number;
  manager: Business[];
  newBuilding: BuildingCreate;
  parking: boolean;
  address: AddressCreate;

  constructor(private fb: FormBuilder, private router: Router, private route: ActivatedRoute,
              private buildingService: BuildingService, private modalService : BsModalService,
              private toasterService: ToasterService) {
    this.floorsList = [];
    this.form = this.fb.group({
      street: ['', [
        Validators.required,
        Validators.minLength(5)
      ]],
      number: ['', [
        Validators.required,
        SpaceValidator.cannotContainSpace
      ]],
      city: ['', [
        Validators.required,
      ]],
      postalCode: ['',[
        Validators.required,
        Validators.pattern('[0-9]{5}')
      ]]
    });
  }

  ngOnInit() {
    this.returnURL = this.route.snapshot.queryParams['returnUrl'] || '/';
    this.apartments = new Array<ApartmentCreate>();
    this.numberOfFloors = 1;
    for(let i = 1; i < 200; i++)
      this.floorsList.push(i);
    this.manager = new Array<Business>();
    this.parking = true;
  }

  get street() {
    return this.form.get('street');
  }

  get number() {
    return this.form.get('number');
  }

  get city() {
    return this.form.get('city');
  }

  get postalCode() {
    return this.form.get('postalCode');
  }

  displayAddApartment() {
    this.bsModalRef = this.modalService.show(AddApartmentModalComponent);
    this.bsModalRef.content.apartmentList = this.apartments;
    this.bsModalRef.content.buildingRegistration = true;
    this.bsModalRef.content.bsModalRef = this.bsModalRef;
  }

  setFloors(floor: number) {
    this.numberOfFloors = floor;
  }

  removeApartment(apartment) {
    let index = this.apartments.indexOf(apartment);
    this.apartments.splice(index, 1);
  }

  removeManager(manager){
    this.manager.pop();
  }

  displayAddManager(){
    this.manager = new Array<Business>();
    this.bsModalRef = this.modalService.show(ChooseManagerModalComponent);
    this.bsModalRef.content.manager = this.manager;
    this.bsModalRef.content.bsModalRef = this.bsModalRef;
  }

  hasParking(has: boolean){
    this.parking = has;
  }

  registerBuilding(){
    this.address = new AddressCreate(this.street.value, this.number.value, new CityCreate(this.city.value, this.postalCode.value));
    let managerId: number;

    if(this.manager.length != 0){
      managerId = this.manager[0].id;
      this.newBuilding = new BuildingCreate(this.numberOfFloors, this.parking, this.address, this.apartments, managerId);
    }
    else{
      this.newBuilding = new BuildingCreate(this.numberOfFloors, this.parking, this.address, this.apartments);
    }

    this.buildingService.save(this.newBuilding).subscribe(data => {
      this.toasterService.pop('success', 'Congratulation', 'Successfully registered new building!');
        setTimeout(() => {
          this.router.navigateByUrl(this.returnURL);
        }, 2000);
      }, (error: AppError) => {
        if(error instanceof BadRequestError)
          this.toasterService.pop('error', 'Error', 'Given data have bad format!');
        else {
          this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
          throw error;
        }
      });
  }
}
