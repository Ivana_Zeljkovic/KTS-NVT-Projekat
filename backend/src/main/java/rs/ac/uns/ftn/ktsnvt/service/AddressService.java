package rs.ac.uns.ftn.ktsnvt.service;

import rs.ac.uns.ftn.ktsnvt.model.entity.Address;

/**
 * Created by Katarina Cukurov on 07/11/2017.
 */
public interface AddressService {

    Address save(Address address);

    Address findOne(Long id);

    Address findByCityStreetNumber(Long cityId, String street, int number);

    void remove(Long id);
}
