package rs.ac.uns.ftn.ktsnvt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.model.entity.Council;
import rs.ac.uns.ftn.ktsnvt.repository.CouncilRepository;

/**
 * Created by Ivana Zeljkovic on 16/11/2017.
 */
@Service
public class CouncilServiceImpl implements CouncilService {

    @Autowired
    private CouncilRepository councilRepository;


    @Override
    @Transactional(readOnly = false)
    public Council save(Council council) {
        return this.councilRepository.save(council);
    }
}
