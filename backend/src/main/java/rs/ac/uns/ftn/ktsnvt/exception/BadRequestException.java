package rs.ac.uns.ftn.ktsnvt.exception;

/**
 * Created by Ivana Zeljkovic on 25/11/2017.
 */
public class BadRequestException extends RuntimeException {

    public BadRequestException() { }

    public BadRequestException(String message) {
        super(message);
    }
}
