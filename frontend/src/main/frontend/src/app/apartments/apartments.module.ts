import { NgModule } from '@angular/core';
import { AparatmentListComponent } from './aparatment-list/aparatment-list.component';
import {SharedModule} from "../shared/shared.module";
import {ApartmentsRouterModule} from "./apartments-router.module";
import { ApartmentComponent } from './apartment/apartment.component';
import { ChooseOwnerModalComponent } from './choose-owner-modal/choose-owner-modal.component';

@NgModule({
  imports: [
    SharedModule,
    ApartmentsRouterModule
  ],
  declarations: [
    AparatmentListComponent,
    ApartmentComponent,
    ChooseOwnerModalComponent
  ],
  entryComponents: [
    ChooseOwnerModalComponent
  ]
})
export class ApartmentsModule { }
