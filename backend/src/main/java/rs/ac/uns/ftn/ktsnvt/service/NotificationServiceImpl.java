package rs.ac.uns.ftn.ktsnvt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Notification;
import rs.ac.uns.ftn.ktsnvt.repository.NotificationRepository;

import java.util.Date;

/**
 * Created by Ivana Zeljkovic on 13/11/2017.
 */
@Service
public class NotificationServiceImpl implements NotificationService {

    @Autowired
    private NotificationRepository notificationRepository;


    @Override
    @Transactional(readOnly = false)
    public Notification save(Notification notification) {
        return this.notificationRepository.save(notification);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Notification> findAllFromTo(Long billboardId, Date from, Date to, Integer pageNumber,
                                            Integer pageSize, String sortDirection, String sortProperty) {
        return this.notificationRepository.findAllFromDateToDate(billboardId, from, to,
                new PageRequest(pageNumber, pageSize, (sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortProperty));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Notification> findAll(Long billboardId, Integer pageNumber, Integer pageSize,
                                      String sortDirection, String sortProperty) {
        return this.notificationRepository.findAllByBillboard(billboardId,
                new PageRequest(pageNumber, pageSize, (sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortProperty));
    }

    @Override
    @Transactional(readOnly = true)
    public Notification findOne(Long id) {
        Notification notification = this.notificationRepository.findOne(id);
        if(notification == null) throw new NotFoundException("Notification not found!");
        return notification;
    }

    @Override
    @Transactional(readOnly = true)
    public void findOneByIdAndBillboard(Long id, Long billboardId) {
        Notification notification = this.notificationRepository.findOneByIdAndBillboard(id, billboardId);
        if(notification == null) throw new BadRequestException("Selected notification doesn't belong to selected building!");
    }

    @Override
    @Transactional(readOnly = false)
    public void remove(Long notificationId) {
        this.notificationRepository.delete(notificationId);
    }
}
