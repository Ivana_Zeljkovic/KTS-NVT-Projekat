package rs.ac.uns.ftn.ktsnvt.web.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ivana Zeljkovic on 19/11/2017.
 */
public class VoteCreateDTO implements Serializable {

    @NotNull
    @Size(min = 1)
    private List<VoteQuestionCreateDTO> questionVotes;

    public VoteCreateDTO() {
        this.questionVotes = new ArrayList<>();
    }

    public List<VoteQuestionCreateDTO> getQuestionVotes() {
        return questionVotes;
    }

    public void setQuestionVotes(List<VoteQuestionCreateDTO> questionVotes) {
        this.questionVotes = questionVotes;
    }
}
