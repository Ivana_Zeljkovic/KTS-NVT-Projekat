import {by, element, ElementArrayFinder, ElementFinder} from "protractor";

export class MeetingItemListModalPage {
  // get methods
  getMeetingItemListModalElement(): ElementFinder {
    return element(by.tagName('app-meeting-item-list-modal'));
  }

  getTitle(): ElementFinder {
    return element(by.css('app-meeting-item-list-modal .modal-title'));
  }

  getSaveButton(): ElementFinder {
    return element(by.css('app-meeting-item-list-modal div.buttons > button:nth-child(1)'));
  }

  getCancelButton(): ElementFinder {
    return element(by.css('app-meeting-item-list-modal div.buttons > button:nth-child(2)'));
  }

  getMeetingItems(): ElementArrayFinder {
    return element.all(by.css('#meetingItemsTable .panel'));
  }

  getMeetingItemTitle(index: number): ElementFinder {
    return element(by.css(`#meetingItemsTable .panel:nth-child(${index+1}) .meeting-item-title`));
  }

  getMeetingItemCreator(index: number): ElementFinder {
    return element(by.css(`#meetingItemsTable .panel:nth-child(${index+1}) .meeting-item-metadata > span:nth-child(2)`));
  }

  getMeetingItemDate(index: number): ElementFinder {
    return element(by.css(`#meetingItemsTable .panel:nth-child(${index+1}) .meeting-item-metadata > span:nth-child(4)`));
  }

  getMeetingItemContent(index: number): ElementFinder {
    return element(by.css(`#meetingItemsTable .panel:nth-child(${index+1}) .panel-body`));
  }

  getLabelSelect(index: number): ElementFinder {
    return element(by.css(`#meetingItemsTable .panel:nth-child(${index+1}) .label-add`));
  }

  getLabelRemove(index: number): ElementFinder {
    return element(by.css(`#meetingItemsTable .panel:nth-child(${index+1}) .label-remove`));
  }
}
