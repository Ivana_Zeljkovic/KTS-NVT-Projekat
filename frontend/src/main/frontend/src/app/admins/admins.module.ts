import { NgModule } from '@angular/core';
// module
import { SharedModule } from "../shared/shared.module";
import { AdminsRouterModule } from "./admins-router.module";
// component
import { AdminsListComponent } from './admins-list/admins-list.component';
import { AdminRegistrationComponent } from "./admin-registration/admin-registration.component";

@NgModule({
  imports: [
    SharedModule,
    AdminsRouterModule
  ],
  declarations: [
    AdminsListComponent,
    AdminRegistrationComponent
  ]
})
export class AdminsModule { }
