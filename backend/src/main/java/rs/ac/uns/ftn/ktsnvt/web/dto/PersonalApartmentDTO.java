package rs.ac.uns.ftn.ktsnvt.web.dto;

import rs.ac.uns.ftn.ktsnvt.model.entity.Apartment;

/**
 * Created by Ivana Zeljkovic on 02/01/2018.
 */
public class PersonalApartmentDTO {

    private Long id;
    private int number;
    private int floor;
    private AddressDTO address;

    public PersonalApartmentDTO() { }

    public PersonalApartmentDTO(Apartment apartment) {
        this.id = apartment.getId();
        this.number = apartment.getNumber();
        this.floor = apartment.getFloor();
        this.address = new AddressDTO(apartment.getBuilding().getAddress());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getFloor() { return floor; }

    public void setFloor(int floor) { this.floor = floor; }

    public AddressDTO getAddress() {
        return address;
    }

    public void setAddress(AddressDTO address) {
        this.address = address;
    }
}
