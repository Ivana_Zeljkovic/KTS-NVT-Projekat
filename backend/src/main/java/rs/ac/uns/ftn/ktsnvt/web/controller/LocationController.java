package rs.ac.uns.ftn.ktsnvt.web.controller;

import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;


/**
 * Created by Ivana Zeljkovic on 25/11/2017.
 */
@RestController
@RequestMapping(value = "/api")
@Api(value="location", description="Getting coordinates on google map for given address")
public class LocationController {

    @RequestMapping(
            value = "/location",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get coordinates for given address",
            notes = "You have to provide address parameter",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of tenants", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to view these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    })
    public ResponseEntity getCoordinates(
            @ApiParam(value = "Address: street and city") @RequestParam("address") String address) {

        final String API_KEY = "AIzaSyBtEDXxVtj8B6Pe_w5S0C7rx8p8rMgaVPU";
        final String API_URL = "https://maps.googleapis.com/maps/api/geocode/json?key=" + API_KEY +
                "&address=" + address;

        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(API_URL, String.class);

        return new ResponseEntity(result, HttpStatus.OK);
    }
}
