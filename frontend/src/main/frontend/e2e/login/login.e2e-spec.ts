import { browser } from "protractor";
// page
import { NavbarPage } from "../page-objects/navbar.po";
import { TenantHomePage } from "../page-objects/tenant-home.po";
import { AdminHomePage } from "../page-objects/admin-home.po";
import { LoginPage } from "../page-objects/login.po";
import { TenantRegistrationPage } from "../page-objects/tenant-registration.po";
import { DamageListPage } from "../page-objects/damage-list.po";
import { BusinessRegistrationPage } from "../page-objects/business-registration.po";


describe('Login to application', () => {
  let loginPage: LoginPage;
  let navbarPage: NavbarPage;
  let tenantHomePage: TenantHomePage;
  let adminHomePage: AdminHomePage;
  let damageListPage: DamageListPage;
  let tenantRegistrationPage: TenantRegistrationPage;
  let businessRegistrationPage: BusinessRegistrationPage;

  beforeEach(() => {
    loginPage = new LoginPage();
    navbarPage = new NavbarPage();

    browser.get("/");
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');

    // expected view of login page
    expect(loginPage.getPageTitle().isDisplayed()).toBe(true);
    expect(loginPage.getUsernameInput().isDisplayed()).toBe(true);
    expect(loginPage.getPasswordInput().isDisplayed()).toBe(true);
    expect(loginPage.getSignInButton().isDisplayed()).toBe(true);
    expect(loginPage.getSignInButton().isEnabled()).toBe(false);
    expect(loginPage.getSignUpAsTenantLink().isDisplayed()).toBe(true);
    expect(loginPage.getSignUpAsFirmLink().isDisplayed()).toBe(true);
    expect(loginPage.getSignUpAsInstitutionLink().isDisplayed()).toBe(true);
  });

  afterEach(() => {
    browser.executeScript('window.localStorage.clear();');
  });


  it('should successfully log in tenant and show navbar and home page for him', () => {
    tenantHomePage = new TenantHomePage();
    // enter login data
    loginPage.setUsername('kaca');
    loginPage.setPassword('kaca');

    // expected view of login page after input credentials
    expect(loginPage.getSignInButton().isEnabled()).toBe(true);

    loginPage.getSignInButton().click();

    browser.wait(function() {
      // expect that browser redirect user to home
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/home/notifications';
      });
    }).then(function() {
      // expect after redirection to see navbar and home page
      expect(navbarPage.getNavbarElement().isDisplayed()).toBe(true);
      expect(tenantHomePage.getTenantHomeElement().isDisplayed()).toBe(true);
    });
  });


  it('should successfully log in admin and show navbar and home page for him', () => {
    adminHomePage = new AdminHomePage();
    // enter login data
    loginPage.setUsername('ivana');
    loginPage.setPassword('ivana');

    // expected view of login page after input credentials
    expect(loginPage.getSignInButton().isEnabled()).toBe(true);

    loginPage.getSignInButton().click();

    browser.wait(function() {
      // expect that browser redirect admin to home
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/home';
      });
    }).then(function() {
      // expect after redirection to see navbar and home page
      expect(navbarPage.getNavbarElement().isDisplayed()).toBe(true);
      expect(adminHomePage.getAdminHomeElement().isDisplayed()).toBe(true);
    });
  });


  it('should successfully log in business (firm or institution) and show navbar and home page for it',
    () => {
    damageListPage = new DamageListPage();
    // enter login data
    loginPage.setUsername('firm1');
    loginPage.setPassword('firm1');

    // expected view of login page after input credentials
    expect(loginPage.getSignInButton().isEnabled()).toBe(true);

    loginPage.getSignInButton().click();

    browser.wait(function() {
      // expect that browser redirect firm to home
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/home';
      });
    }).then(function() {
      // expect after redirection to see navbar and home (damage list) page
      expect(navbarPage.getNavbarElement().isDisplayed()).toBe(true);
      expect(damageListPage.getDamageListElement().isDisplayed()).toBe(true);
    });
  });


  it('should show error for required username when user put focus on username input and leave it', () => {
    // enter login data
    loginPage.setUsername('');
    loginPage.setPassword('kaca');

    // expected view of login page
    expect(loginPage.getUsernameRequiredError().isDisplayed()).toBe(true);
    expect(loginPage.getSignInButton().isEnabled()).toBe(false);
  });


  it('should show error that username can not contain space when user put two words in username input',() => {
    // enter login data
    loginPage.setUsername('kaca kaca');
    loginPage.setPassword('kaca');

    // expected view of login page
    expect(loginPage.getUsernameContainSpaceError().isDisplayed()).toBe(true);
    expect(loginPage.getSignInButton().isEnabled()).toBe(false);
  });


  it('should show error for required password when user put focus on password input and leave it', () => {
    // enter login data
    loginPage.setPassword('');
    loginPage.setUsername('kaca');

    // expected view of login page
    expect(loginPage.getPasswordRequiredError().isDisplayed()).toBe(true);
    expect(loginPage.getSignInButton().isEnabled()).toBe(false);
  });


  it('should redirect to tenant registration page when user click on tenant registration link', () => {
    tenantRegistrationPage = new TenantRegistrationPage();
    loginPage.getSignUpAsTenantLink().click();

    browser.wait(function() {
      // expect that browser redirect user to registration tenant page
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/registration/tenant';
      });
    }).then(function() {
      // expect after redirection to see tenant registration page
      expect(tenantRegistrationPage.getTenantRegistrationElement().isDisplayed()).toBe(true);
    });
  });


  it('should redirect to firm registration page when user click on firm registration link', () => {
    businessRegistrationPage = new BusinessRegistrationPage();
    loginPage.getSignUpAsFirmLink().click();

    browser.wait(function() {
      // expect that browser redirect user to registration firm page
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/registration/firm';
      });
    }) .then(function() {
      // expect after redirection to see firm registration page
      expect(businessRegistrationPage.getBusinessRegistrationElement().isDisplayed()).toBe(true);
    });
  });


  it('should redirect to institution registration page when user click on institution registration link', () => {
    businessRegistrationPage = new BusinessRegistrationPage();
    loginPage.getSignUpAsInstitutionLink().click();

    browser.wait(function() {
      // expect that browser redirect user to registration institution page
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/registration/institution';
      });
    }) .then(function() {
      // expect after redirection to see institution registration page
      expect(businessRegistrationPage.getBusinessRegistrationElement().isDisplayed()).toBe(true);
    });
  });
});
