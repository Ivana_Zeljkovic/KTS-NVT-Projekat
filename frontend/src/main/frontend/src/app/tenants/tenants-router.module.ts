import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";
// component
import { TenantListComponent } from "./tenant-list/tenant-list.component";
import { TenantComponent } from "./tenant/tenant.component";

const routes: Routes = [
  { path: '', component: TenantListComponent },
  { path: ':id', component: TenantComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TenantsRouterModule { }
