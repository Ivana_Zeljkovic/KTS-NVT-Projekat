package rs.ac.uns.ftn.ktsnvt.web.dto;

import java.io.Serializable;

/**
 * Created by Ivana Zeljkovic on 19/01/2018.
 */
public class AnswerResultDTO implements Serializable {

    private String content;
    private int numberOfVotes;

    public AnswerResultDTO() { }

    public AnswerResultDTO(String content, int numberOfVotes) {
        this.content = content;
        this.numberOfVotes = numberOfVotes;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getNumberOfVotes() {
        return numberOfVotes;
    }

    public void setNumberOfVotes(int numberOfVotes) {
        this.numberOfVotes = numberOfVotes;
    }
}
