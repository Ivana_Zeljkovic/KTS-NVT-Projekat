import { TestBed, inject } from '@angular/core/testing';

import { BuildingService } from './building.service';
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {Apartment} from "../../shared/models/apartment";
import {Building} from "../../shared/models/building";

describe('BuildingService', () => {

  let buildingService: BuildingService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [BuildingService]
    });

    buildingService = TestBed.get(BuildingService);
    httpMock = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', inject([BuildingService], (service: BuildingService) => {
    expect(service).toBeTruthy();
  }));

  it('should get all buildings from city', () => {
    const responseList = [{
      'id' : 1,
      'address' : {
        'street': 'Janka Veselinovica',
        'number': 8,
        'city': {
          'name': 'Novi Sad',
          'postalNumber': '21000'
        }
      },
      'apartments' : new Array<Apartment>(),
      'numberOfFloors' : 5,
      'hasParking' : true
    },
      {
        'id' : 2,
        'address' : {
          'street': 'Janka Veselinovica',
          'number': 18,
          'city': {
            'name': 'Novi Sad',
            'postalNumber': '21000'
          }
        },
        'apartments' : new Array<Apartment>(),
        'numberOfFloors' : 5,
        'hasParking' : true
      }
      ];

    buildingService.getFromAddress('Novi Sad')
      .subscribe((buildings: Array<Building>) => {
        expect(buildings).toEqual(responseList);
        expect(buildings.length).toEqual(2);
      });
    const httpRequest = httpMock.expectOne(
      '/api/buildings/building_address?' +
      'city_name=Novi%20Sad');
    expect(httpRequest.request.method).toEqual('GET');
    expect(httpRequest.request.params.get('city_name')).toEqual('Novi Sad');
    httpRequest.flush(responseList);
  });
});
