package rs.ac.uns.ftn.ktsnvt.service;

import org.springframework.data.domain.Page;
import rs.ac.uns.ftn.ktsnvt.model.entity.Record;


/**
 * Created by Nikola Garabandic on 22/11/2017.
 */
public interface RecordService {

    Record save(Record record);

    Page<Record> findAllByBuildingId(Long buildingId, Integer pageNumber, Integer pageSize,
                                     String sortDirection, String sortProperty);

    void checkCouncilForRecord(Long recordId, Long councilId);

    Record findOne(Long recordId);
}
