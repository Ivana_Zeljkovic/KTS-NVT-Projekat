import { Tenant } from "./tenant";
import { VoteQuestion } from "./vote-question";

export interface Vote {
  id: number;
  tenant: Tenant;
  questionsVotes: Array<VoteQuestion>;
}
