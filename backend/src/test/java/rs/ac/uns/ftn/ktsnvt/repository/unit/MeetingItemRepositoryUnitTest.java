package rs.ac.uns.ftn.ktsnvt.repository.unit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.model.entity.Billboard;
import rs.ac.uns.ftn.ktsnvt.model.entity.Meeting;
import rs.ac.uns.ftn.ktsnvt.model.entity.MeetingItem;
import rs.ac.uns.ftn.ktsnvt.model.entity.Tenant;
import rs.ac.uns.ftn.ktsnvt.repository.MeetingItemRepository;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by Ivana Zeljkovic on 28/11/2017.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class MeetingItemRepositoryUnitTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private MeetingItemRepository meetingItemRepository;

    private Long billboard_id;
    private Long creator_id;
    private Long meeting_item_id_1;
    private Long meeting_item_id_2;
    private Long meeting_item_id_3;
    private Long meeting_item_id_4;


    @Before
    public void setUp() {
        // BILLBOARD
        Billboard billboard = new Billboard();
        Billboard billboard_2 = new Billboard();
        this.billboard_id = (Long)entityManager.persistAndGetId(billboard);
        entityManager.persist(billboard_2);

        Tenant creator = new Tenant();
        creator.setFirstName("Ivana");
        creator.setLastName("Zeljkovic");
        creator.setBirthDate(new Date());
        creator.setEmail("ivana@gmail.com");
        creator.setPhoneNumber("123456");
        this.creator_id = (Long)entityManager.persistAndGetId(creator);

        Meeting meeting = new Meeting(new Date());
        entityManager.persist(meeting);

        MeetingItem meetingItem_1 = new MeetingItem(
                "First meeting item", "First meeting item content", creator, billboard, null, null, null);
        MeetingItem meetingItem_2 = new MeetingItem(
                "Second meeting item", "Second meeting item content",  creator, billboard, null, null, null);
        MeetingItem meetingItem_3 = new MeetingItem(
                "Third meeting item", "Third meeting item content", creator, billboard, null, null, meeting);
        MeetingItem meetingItem_4 = new MeetingItem(
                "Fourth meeting item", "Fourth meeting item content", creator, billboard_2, null, null, null);


        Date date = new Date();
        meetingItem_1.setDate(date);
        meetingItem_2.setDate(date);
        meetingItem_3.setDate(date);
        meetingItem_4.setDate(date);

        this.meeting_item_id_1 = (Long)entityManager.persistAndGetId(meetingItem_1);
        this.meeting_item_id_2 = (Long)entityManager.persistAndGetId(meetingItem_2);
        this.meeting_item_id_3 = (Long)entityManager.persistAndGetId(meetingItem_3);
        this.meeting_item_id_4 = (Long)entityManager.persistAndGetId(meetingItem_4);

        billboard.getMeetingItems().add(meetingItem_1);
        billboard.getMeetingItems().add(meetingItem_2);
        billboard.getMeetingItems().add(meetingItem_3);
        billboard_2.getMeetingItems().add(meetingItem_4);
        entityManager.merge(billboard);
        entityManager.merge(billboard_2);

        meeting.getMeetingItems().add(meetingItem_3);
        entityManager.merge(meeting);
    }

    @Test
    public void testFindAllOnBillboard() {
        /*
         * Test proverava ispravnost rada metode findAllOnBillboard repozitorijuma MeetingItemRepository
         * (pronalazenje svih predloga tacaka dnevnog reda sa bilborda zgrade)
         * */

        // na bilbordu sa ID-em = billboard_id nalaze se 2 jos uvek 'nepokupljena' predloga tacke dnevnog reda
        List<MeetingItem> meeting_items = this.meetingItemRepository.findAllOnBillboard(this.billboard_id);

        assertEquals(2, meeting_items.size());
        assertEquals(this.meeting_item_id_1, meeting_items.get(0).getId());
        assertEquals("First meeting item content", meeting_items.get(0).getContent());
        assertEquals("First meeting item", meeting_items.get(0).getTitle());
        assertEquals(this.meeting_item_id_2, meeting_items.get(1).getId());
        assertEquals("Second meeting item content", meeting_items.get(1).getContent());
        assertEquals("Second meeting item", meeting_items.get(1).getTitle());
        for(MeetingItem m : meeting_items)
            assertEquals(this.creator_id, m.getCreator().getId());
    }

    @Test
    public void testFindOneByIdAndBillboard() {
        /*
         * Test proverava ispravnost rada metode findOneByIdAndBillboard repozitorijuma MeetingItemRepository
         * (pronalazenje konkretne tacke dnevnog reda na osnovu zadatog ID-a i ID-a bilborda zgrade kom tacka dnevnog reda pripada,
         * pri cemu ta tacka dnevnog reda nije obradjena jos ni na jednom sastanku skupstine)
         * u slucaju kada postoji tacka dnevnog reda sa zadatim ID-em na bilbordu sa zadatim ID-em i pri tome jos uvek nije
         * obradjena ni na jednom sastanku
         * */

        /*
        oglasnoj tabli sa ID-em = billboard_id pripada tacka dnevnog reda:
            ID = meeting_item_id_1,
            ID kreatora = creator_id
            content = First meeting item
            meeting_id = null
        */
        MeetingItem meetingItem = this.meetingItemRepository.findOneByIdAndBillboard(this.meeting_item_id_1, this.billboard_id);

        assertNotNull(meetingItem);
        assertEquals(this.meeting_item_id_1, meetingItem.getId());
        assertEquals(this.creator_id, meetingItem.getCreator().getId());
        assertEquals("First meeting item content", meetingItem.getContent());
        assertEquals("First meeting item", meetingItem.getTitle());
    }

    @Test
    public void testFindOneByIdAndBillboardNonexistent() {
        /*
         * Test proverava ispravnost rada metode findOneByIdAndBillboard repozitorijuma MeetingItemRepository
         * (pronalazenje konkretne tacke dnevnog reda na osnovu zadatog ID-a i ID-a bilborda zgrade kom tacka dnevnog reda pripada,
         * pri cemu ta tacka dnevnog reda nije obradjena jos ni na jednom sastanku skupstine)
         * u slucaju kada ne postoji tacka dnevnog reda sa zadatim ID-em na bilbordu sa zadatim ID-em
         * */

        // oglasnoj tabli sa ID-em = billboard_id ne pripada tacka dnevnog reda sa ID-em meeting_item_id_4
        MeetingItem meetingItem = this.meetingItemRepository.findOneByIdAndBillboard(this.meeting_item_id_4, this.billboard_id);

        assertNull(meetingItem);
    }

    @Test
    public void testFindOneByIdAndBillboardAlreadyProcessed() {
        /*
         * Test proverava ispravnost rada metode findOneByIdAndBillboard repozitorijuma MeetingItemRepository
         * (pronalazenje konkretne tacke dnevnog reda na osnovu zadatog ID-a i ID-a bilborda zgrade kom tacka dnevnog reda pripada,
         * pri cemu ta tacka dnevnog reda nije obradjena jos ni na jednom sastanku skupstine)
         * u slucaju kada postoji tacka dnevnog reda sa zadatim ID-em na bilbordu sa zadatim ID-em ali je vec obradjena na nekom sastanku
         * */

        /*
        oglasnoj tabli sa ID-em = billboard)id pripada tacka dnevnog reda:
            ID = meeting_item_id_3,
            ID kreatora = creator_id
            content = Third meeting item,
            meeting_id = meeting_id
        */
        MeetingItem meetingItem = this.meetingItemRepository.findOneByIdAndBillboard(this.meeting_item_id_3, this.billboard_id);

        assertNull(meetingItem);
    }
}
