import { NgModule } from '@angular/core';
// module
import { SharedModule } from "../../shared/shared.module";
import { TenantHomeRouterModule } from "./tenant-home-router.module";
// component
import { TenantHomeComponent } from "./tenant-home/tenant-home.component";
import { NotificationComponent } from "./notification/notification.component";
import { NotificationListComponent } from "./notification-list/notification-list.component";
import { MeetingItemComponent } from "./meeting-item/meeting-item.component";
import { MeetingItemListComponent } from "./meeting-item-list/meeting-item-list.component";
import { ConfirmDeleteModalComponent } from "../../shared/components/confirm-delete-modal/confirm-delete-modal.component";
import { QuestionnaireComponent } from "../../shared/components/questionnaire/questionnaire.component";
import { QuestionComponent } from "./question/question.component";
import { DamageForQuestionnaireComponent } from "./damage-for-questionnaire/damage-for-questionnaire.component";


@NgModule({
  imports: [
    SharedModule,
    TenantHomeRouterModule
  ],
  declarations: [
    TenantHomeComponent,
    NotificationComponent,
    NotificationListComponent,
    MeetingItemComponent,
    MeetingItemListComponent,
    DamageForQuestionnaireComponent,
    QuestionComponent
  ],
  exports: [
    TenantHomeComponent
  ],
  entryComponents: [
    ConfirmDeleteModalComponent,
    QuestionnaireComponent,
    QuestionComponent,
    DamageForQuestionnaireComponent,
    NotificationComponent,
    MeetingItemComponent
  ]
})
export class TenantHomeModule { }
