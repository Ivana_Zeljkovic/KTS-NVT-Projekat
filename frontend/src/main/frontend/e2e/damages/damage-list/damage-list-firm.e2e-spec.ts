import { browser } from "protractor";
// page
import { DamageListPage } from "../../page-objects/damage-list.po";
import { DamageDetailsPage } from "../page-objects/damage-details.po";
import { LoginPage } from "../../page-objects/login.po";


describe('Damage list for firm', () => {
  let damageListPage: DamageListPage;
  let damageDetailsPage: DamageDetailsPage;
  let loginPage: LoginPage;


  beforeAll(() => {
    loginPage = new LoginPage();
    damageListPage = new DamageListPage();
    damageDetailsPage = new DamageDetailsPage();

    browser.get('/');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');

    loginPage.setUsername('firm2');
    loginPage.setPassword('firm2');
    loginPage.getSignInButton().click();

    browser.wait(function() {
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/home';
      });
    });
  });

  afterAll(() => {
    browser.executeScript('window.localStorage.clear();');
  });



  it('should show list of damages that current firm should repair, when firm is successfully logged in',
    () => {
    browser.wait(function() {
      return damageListPage.getDamageListElement().isDisplayed();
    }).then(function() {
      // expected view of page
      expect(damageListPage.getTitle().isDisplayed()).toBe(true);
      expect(damageListPage.getTitle().getText()).toEqual('Damages that I should repair');
      expect(damageListPage.getSearchDateRangeInput().isDisplayed()).toBe(true);
      expect(damageListPage.getSearchDateRangeClearButton().isDisplayed()).toBe(true);
      expect(damageListPage.getSearchDateRangeClearButton().isEnabled()).toBe(true);
      expect(damageListPage.getSearchDateRangeSearchButton().isDisplayed()).toBe(true);
      expect(damageListPage.getSearchDateRangeSearchButton().isEnabled()).toBe(true);

      // if list of damages is not empty, list is displayed
      if(damageListPage.getDamageList().isPresent() && damageListPage.getDamageList().isDisplayed()) {
        expect(damageListPage.getMessageNoDamageInList().isPresent()).toBe(false);
        expect(damageListPage.getPanelsFromDamageList().isPresent()).toBe(true);
        expect(damageListPage.getPaginatorInDamageList().isDisplayed()).toBe(true);

        let numberOfDamages = {'value':0};
        damageListPage.getPanelsFromDamageList().count().then(value => {
          numberOfDamages.value = value;

          // for every damage in list, wh should check is there option for damage details and declare
          // damage as fixed
          for(let i: number = 0; i<numberOfDamages.value; i++) {
            expect(damageListPage.getCreatorNameFromDamagePanel(i).isDisplayed()).toBe(true);
            expect(damageListPage.getDateFromDamagePanel(i).isDisplayed()).toBe(true);
            expect(damageListPage.getDamageTypeFromDamagePanel(i).isDisplayed()).toBe(true);
            expect(damageListPage.getDamageDescriptionFromDamagePanel(i).isDisplayed()).toBe(true);

            if(damageListPage.getMenuFromDamagePanel(i).isPresent() &&
              damageListPage.getMenuFromDamagePanel(i).isDisplayed()) {
              damageListPage.getMenuFromDamagePanel(i).click();

              browser.wait(function() {
                return damageListPage.getContentOfMenuFromDamagePanel(i).isDisplayed();
              }).then(function() {
                expect(damageListPage.getDamageDetailsLinkFromDamagePanel(i).isDisplayed()).toBe(true);
                expect(damageListPage.getDamageDetailsLinkFromDamagePanel(i).getText()).toEqual('Damage details');
                // second option in damage menu in this category of damages is declare damage fixed
                expect(damageListPage.getLinkFromDamagePanel(i, 2).isDisplayed()).toBe(true);
                expect(damageListPage.getLinkFromDamagePanel(i,2).getText()).toEqual('Declare fixed');
              });
            }
          }
        });
      }
      // opposite, list is not displayed
      else {
        expect(damageListPage.getMessageNoDamageInList().isDisplayed()).toBe(true);
        expect(damageListPage.getMessageNoDamageInList().getText()).toContain('There is still no damages');
      }
    });
  });
});
