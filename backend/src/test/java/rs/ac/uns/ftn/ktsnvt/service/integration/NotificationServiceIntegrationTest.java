package rs.ac.uns.ftn.ktsnvt.service.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Notification;
import rs.ac.uns.ftn.ktsnvt.service.NotificationService;

import static org.junit.Assert.*;

/**
 * Created by Ivana Zeljkovic on 05/12/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class NotificationServiceIntegrationTest {

    @Autowired
    private NotificationService notificationService;


    @Test
    public void testFindOneByIdAndBillboard() {
        /*
         * Test proverava ispravnost rada metode findOneByIdAndBillboard servisa NotificationService
         * (provera da li postoji konkretno javno obavestenje na osnovu zadatog ID-a i ID-a bilborda zgrade kom obavestenje pripada)
         * u slucaju kada postoji obavestenje sa zadatim ID-em na bilbordu sa zadatim ID-em
         * */

        /*
        oglasnoj tabli zgrade sa ID-em = 100 (oglasna tabla sa ID-em = 100) pripada obavestenje:
            ID = 100,
            ID kreatora = 101,
            content = First notification
        */
        this.notificationService.findOneByIdAndBillboard(new Long(100), new Long(100));
    }

    @Test(expected = BadRequestException.class)
    public void testFindOneByIdAndBillboardNonexistent() {
        /*
         * Test proverava ispravnost rada metode findOneByIdAndBillboard servisa NotificationService
         * (provera da li postoji konkretno javnog obavestenje na osnovu zadatog ID-a i ID-a bilborda zgrade kom obavestenje pripada)
         * u slucaju kada ne postoji obavestenje sa zadatim ID-em na bilbordu sa zadatim ID-em - ocekivano NotFoundException
         * */

        // oglasnoj tabli zgrade sa ID-em = 100 (oglasna tabla sa ID-em = 100) ne pripada obavestenje sa ID-em 102:
        this.notificationService.findOneByIdAndBillboard(new Long(102), new Long(100));
    }

    @Test
    @Transactional
    public void testFindOne() {
        /*
         * Test proverava ispravnost metode findOne servisa NotificationService
         * (dobavljanje objekta obavestenje na osnovu zadatog ID-a)
         * u slucaju kada postoji obavestenje sa zadatim ID-em
         * */

        /*
        u bazi postoji obavestenje sa ID-em = 100 i podacima:
            ID = 100,
            ID kreatora = 101,
            content = First notification
         */
        Notification notification = this.notificationService.findOne(new Long(100));

        assertNotNull(notification);
        assertEquals(Long.valueOf(100), notification.getId());
        assertEquals(Long.valueOf(101), notification.getCreator().getId());
        assertEquals("First notification content", notification.getContent());
    }

    @Test(expected = NotFoundException.class)
    @Transactional
    public void testFindOneNonexistent() {
        /*
         * Test proverava ispravnost metode findOne servisa NotificationService
         * (dobavljanje objekta obavestenja na osnovu zadatog ID-a)
         * u slucaju kada ne postoji obavestenje sa zadatim ID-em - ocekivano NotFoundException
         * */

        // u bazi ne postoji obavestenje sa ID-em = 200
        Notification notification = this.notificationService.findOne(new Long(200));

        assertNull(notification);
    }
}
