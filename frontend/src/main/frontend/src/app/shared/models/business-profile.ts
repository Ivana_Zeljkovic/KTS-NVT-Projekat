import {Address} from "./address";
import {Login} from "./login";

export interface BusinessProfile{
  id: number;
  name: string;
  description: string;
  phoneNumber: string;
  email: string;
  usernamesList: Array<Login>;
  businessType: string;
  address: Address;
  deactivated: boolean;
}
