import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";
// component
import { BuildingListComponent } from "./building-list/building-list.component";
import {BuildingRegistrationComponent} from "./building-registration/building-registration.component";
import {BuildingComponent} from "./building/building.component";

const routes: Routes = [
  { path: '', component: BuildingListComponent },
  { path: 'register', component: BuildingRegistrationComponent },
  { path: ':id', component: BuildingComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class BuildingsRouterModule { }
