package rs.ac.uns.ftn.ktsnvt.repository.unit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.model.entity.Billboard;
import rs.ac.uns.ftn.ktsnvt.model.entity.MeetingItem;
import rs.ac.uns.ftn.ktsnvt.model.entity.Questionnaire;
import rs.ac.uns.ftn.ktsnvt.repository.QuestionnaireRepository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Ivana Zeljkovic on 07/12/2017.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class QuestionnaireRepositoryUnitTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private QuestionnaireRepository questionnaireRepository;

    private Long questionnaire_id;
    private Long meeting_item_id;
    private Long billboard_id;


    @Before
    public void setUp() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Questionnaire questionnaire = new Questionnaire();
        try {
            questionnaire.setDateExpire(sdf.parse("2018-05-14 10:00:00"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.questionnaire_id = (Long)entityManager.persistAndGetId(questionnaire);

        Billboard billboard = new Billboard();

        MeetingItem meetingItem = new MeetingItem();
        meetingItem.setDate(new Date());
        meetingItem.setContent("First meeting item");
        meetingItem.setQuestionnaire(questionnaire);
        meetingItem.setTitle("TITLE");
        this.meeting_item_id = (Long)entityManager.persistAndGetId(meetingItem);

        questionnaire.setMeetingItem(meetingItem);
        entityManager.merge(questionnaire);



        billboard.getMeetingItems().add(meetingItem);
        this.billboard_id = (Long)entityManager.persistAndGetId(billboard);
        meetingItem.setBillboard(billboard);
        entityManager.merge(meetingItem);
    }

    @Test
    @Transactional
    public void testFindOneByIdAndBillboardId() {
        /*
        * Test proverava ispravnost rada metode findOneByIdAndBillboardId repozitorijuma QuestionnaireRepository
        * (dobavljanje ankete na osnovu njenog ID-a i ID-a zgrade kojoj bi ta anketa trebala da pripada)
        * */

        /*
        anketa sa ID-em = questionnaire_id pripada bilbordu sa ID = billboard_id i ima sledece podatke:
            id = questionnaire_id,
            meeting_item_id = meeting_item_id
        */
        Questionnaire questionnaire = this.questionnaireRepository.findOneByIdAndBillboardId(this.questionnaire_id, this.billboard_id);

        assertNotNull(questionnaire);
        assertEquals(this.questionnaire_id, questionnaire.getId());
        assertEquals(this.meeting_item_id, questionnaire.getMeetingItem().getId());
    }
}
