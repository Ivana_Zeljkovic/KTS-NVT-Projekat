package rs.ac.uns.ftn.ktsnvt.service.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;

import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Question;
import rs.ac.uns.ftn.ktsnvt.service.QuestionService;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;


/**
 * Created by Nikola Garabandic on 01/12/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class QuestionServiceIntegrationTest {

    @Autowired
    private QuestionService questionService;


    @Test
    public void testCheckAnswers(){
        /*
        vote_answer objekat sa ID-em 100 i sa "First Tenant" kao odgovorom postoji u bazi, te ce se ova metoda uspesno izvrsiti
         */
        Map<Long, String> map = new HashMap<>();
        map.put(new Long(100), "First tenant");

        this.questionService.checkQuestions(map, 100L);
    }

    @Test(expected = BadRequestException.class)
    public void testCheckAnswersFailed(){
        /* vote_answer objekat sa ID-em 100 i sa "es" kao odgovorom ne postoji u bazi, te ce ova metoda poslati BadRequestException,
           a samim tim, test ce se uspesno izvrsiti
        */
        Map<Long, String> map = new HashMap<>();
        map.put(new Long(100), "es");

        this.questionService.checkQuestions(map, 100L);
    }

    @Test
    @Transactional
    public void testFindOne() {
        /*
         * Test proverava ispravnost metode findOne servisa QuestionService
         * (dobavljanje objekta pitanja na osnovu zadatog ID-a)
         * u slucaju kada postoji pitanje sa zadatim ID-em
         * */

        // u bazi postoji pitanje sa ID-em = 100
        Question question = this.questionService.findOne(100L);

        assertNotNull(question);
        assertEquals(Long.valueOf(100), question.getId());
        assertEquals(2, question.getAnswers().size());
    }

    @Test(expected = NotFoundException.class)
    public void testFindOneUnexistentQuestion() {
        /*
         * Test proverava ispravnost metode findOne servisa QuestionService
         * (dobavljanje objekta pitanja na osnovu zadatog ID-a)
         * u slucaju kada postoji ne pitanje sa zadatim ID-em - ocekivano NotFoundException
         * */

        // u bazi ne postoji pitanje sa ID-em = 200
        Question question = this.questionService.findOne(200L);
    }
}
