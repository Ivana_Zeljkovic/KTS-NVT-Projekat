export class ApartmentUpdate {
  constructor(private id: number,
              private number: number,
              private floor: number,
              private surface: number) { }
}
