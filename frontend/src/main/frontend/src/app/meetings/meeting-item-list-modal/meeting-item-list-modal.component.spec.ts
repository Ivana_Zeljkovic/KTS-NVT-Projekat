import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeetingItemListModalComponent } from './meeting-item-list-modal.component';

describe('MeetingItemListModalComponent', () => {
  let component: MeetingItemListModalComponent;
  let fixture: ComponentFixture<MeetingItemListModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeetingItemListModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeetingItemListModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
