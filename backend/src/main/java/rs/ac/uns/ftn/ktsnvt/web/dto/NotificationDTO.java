package rs.ac.uns.ftn.ktsnvt.web.dto;

import rs.ac.uns.ftn.ktsnvt.model.entity.Notification;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Ivana Zeljkovic on 13/11/2017.
 */
public class NotificationDTO implements Serializable {

    private Long id;
    private String content;
    private String title;
    private Date date;
    private TenantDTO creator;
    private long numberOfElements;

    public NotificationDTO() { }

    public NotificationDTO(Notification notification) {
        this.id = notification.getId();
        this.content = notification.getContent();
        this.title = notification.getTitle();
        this.date = notification.getDate();
        this.creator = new TenantDTO(notification.getCreator());
    }

    public NotificationDTO(Notification notification, long numberOfElements) {
        this.id = notification.getId();
        this.content = notification.getContent();
        this.title = notification.getTitle();
        this.date = notification.getDate();
        this.creator = new TenantDTO(notification.getCreator());
        this.numberOfElements = numberOfElements;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() { return title; }

    public void setTitle(String title) { this.title = title; }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public TenantDTO getCreator() {
        return creator;
    }

    public void setCreator(TenantDTO creator) {
        this.creator = creator;
    }

    public long getNumberOfElements() { return numberOfElements; }

    public void setNumberOfElements(long numberOfElements) { this.numberOfElements = numberOfElements; }
}
