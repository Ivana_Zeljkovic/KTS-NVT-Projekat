import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {BusinessAddAccountModalComponent} from "./bussiness-add-account-modal.component";


describe('BusinessAddAccountModalComponent', () => {
  let component: BusinessAddAccountModalComponent;
  let fixture: ComponentFixture<BusinessAddAccountModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessAddAccountModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessAddAccountModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
