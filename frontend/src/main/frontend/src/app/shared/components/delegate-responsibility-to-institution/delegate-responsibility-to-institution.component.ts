import { Component, EventEmitter, Input, Output } from '@angular/core';
import { BsModalRef } from "ngx-bootstrap";
import { ToasterService } from "angular5-toaster/dist";
// model
import { Business } from "../../models/business";
import { Damage } from "../../models/damage";
import { DamageTypeResponsibility } from "../../models/damage-type-responsibility";
import { PageAndSort } from "../../models/page-and-sort";
import { ResourceId } from "../../models/id";
// service
import { BusinessService } from "../../../core/services/business.service";
import { JwtService } from "../../../core/services/jwt.service";
import { DamageService } from "../../../core/services/damage.service";
import { DamageTypeResponsibilityService } from "../../../core/services/damage-type-responsibility.service";
// error
import { AppError } from "../../errors/app-error";
import { BadRequestError } from "../../errors/bad-request-error";
import { ForbiddenError } from "../../errors/forbidden-error";
import { NotFoundError } from "../../errors/not-found-error";


@Component({
  selector: 'app-delegate-responsibility-to-institution',
  templateUrl: './delegate-responsibility-to-institution.component.html',
  styleUrls: ['./delegate-responsibility-to-institution.component.css']
})
export class DelegateResponsibilityToInstitutionComponent {
  // input model in case if we set/delegate responsibility for damage
  @Input() damage: Damage;
  // input model in case if we set/delegate responsibility for damage type
  @Input() damageTypeResponsibility: DamageTypeResponsibility;

  // input field that indicates are we set (true) or delegate (false) responsibility
  @Input() setResponsibility: boolean;
  // input field that indicates are we delegate damage responsibility (true)
  // or damage type responsibility (false)
  @Input() isDelegatingDamage: boolean;

  // output (event) in case if we delegated some responsibility (for damage or damage type)
  @Output() responsibilitySubmitted = new EventEmitter<boolean>();
  // output (event) in case if we set some responsibility for damage
  @Output() damageResponsibilitySet = new EventEmitter<Damage>();
  // output (event) in case if we set some responsibility for damage type
  @Output() damageTypeResponsibilitySet = new EventEmitter<DamageTypeResponsibility>();

  institutionsList: Array<Business>;

  // Elements for bootstrap paging
  currentPage: number;
  totalItems: number;
  maxSize: number;
  itemsPerPage: number;

  // Elements for getting data from server
  pageSize: string;
  sortDirection: string;
  sortProperties: string[];


  constructor(public modalRef: BsModalRef, private businessService: BusinessService,
              private jwtService: JwtService, private toasterService: ToasterService,
              private damageService: DamageService, private damageTypeResponsibilityService: DamageTypeResponsibilityService) {
    // Sett up for bootstrap paging
    this.currentPage = 1;
    this.maxSize = 3;
    this.itemsPerPage = 4;

    // Sett up for getting damages request
    this.pageSize = '4';
    this.sortDirection = 'ASC';
    this.sortProperties = ['name'];
  }

  ngOnInit() {
    this.loadList();
  }

  pageChanged(event: any) {
    this.currentPage = event.page;
    this.loadList();
  }

  delegateResponsibilityInstitution(institutionId: number) {
    if(this.setResponsibility) {
      if(this.isDelegatingDamage) {
        // in case if council president set responsible institution for some damage
        this.damageService.updatePart(new ResourceId(institutionId), 'buildings', this.jwtService.getBuildingIdFromToken(),
          'damages', this.damage.id, 'responsible_institution')
          .subscribe((updatedDamage) => {
            this.damageResponsibilitySet.emit(updatedDamage);
          }, (error: AppError) => {
            if (error instanceof BadRequestError)
              this.toasterService.pop('error', 'Error', 'Selected damage already has responsible institution!');
            else if (error instanceof ForbiddenError)
              this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
            else if (error instanceof NotFoundError)
              this.toasterService.pop('error', 'Error', 'Institution/Damage not found!');
            else {
              this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
              throw error;
            }
          })
      }
      else {
        // in case if council president set responsible institution for some damage type in some building
        this.damageTypeResponsibilityService.updatePart(new ResourceId(institutionId), 'buildings', this.jwtService.getBuildingIdFromToken(),
          'damage_types', this.damageTypeResponsibility.damageTypeId, 'responsible_institution')
          .subscribe((updatedDamageTypeResponsibility) => {
            this.damageTypeResponsibilitySet.emit(updatedDamageTypeResponsibility);
          }, (error: AppError) => {
            if(error instanceof BadRequestError)
              this.toasterService.pop('error', 'Error', 'You can\'t choose firm as responsible, only institution!');
            else if(error instanceof ForbiddenError)
              this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
            else if(error instanceof NotFoundError)
              this.toasterService.pop('error', 'Error', 'Building/Damage type/Institution not found!');
            else {
              this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
              throw error;
            }
          });
      }
    }
    else {
      if(this.isDelegatingDamage) {
        // in case if current responsible institution for some damage delegate that responsibility to another institution
        this.damageService.updatePart(new ResourceId(institutionId), 'institutions', this.jwtService.getIdFromToken(),
          'responsible_for_damages', this.damage.id, 'delegate_responsibility')
          .subscribe(() => {
            this.responsibilitySubmitted.emit(true);
          }, (error: AppError) => {
            if (error instanceof BadRequestError)
              this.toasterService.pop('error', 'Error', 'You can\'t delegate your responsibility to firm, just to another institution!');
            else if (error instanceof ForbiddenError)
              this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
            else if (error instanceof NotFoundError)
              this.toasterService.pop('error', 'Error', 'Institution/Damage not found!');
            else {
              this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
              throw error;
            }
          });
      }
      else {
        // in case if current responsible institution for some damage type in some building delegate that responsibility
        // to another institution
        this.damageTypeResponsibilityService.updatePart(new ResourceId(institutionId), 'institutions', this.damageTypeResponsibility.institutionId,
          'damage_types', this.damageTypeResponsibility.damageTypeId, 'buildings', this.damageTypeResponsibility.buildingId,
          'delegate_responsibility')
          .subscribe(() => {
            this.responsibilitySubmitted.emit(true);
          }, (error: AppError) => {
            if (error instanceof BadRequestError)
              this.toasterService.pop('error', 'Error', 'You can\'t delegate responsibility to firm, just on another institution!');
            else if (error instanceof ForbiddenError)
              this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
            else if (error instanceof NotFoundError)
              this.toasterService.pop('error', 'Error', 'Institution/Damage responsibility not found!');
            else {
              this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
              throw error;
            }
          });
      }
    }
    this.modalRef.hide();
  }

  private loadList() {
    let page = new PageAndSort((this.currentPage - 1).toString(), this.pageSize, this.sortDirection, this.sortProperties);

    this.businessService.getPage(page, 'businesses/institutions')
      .subscribe(institutions => {
        if(institutions.length > 0)
          this.totalItems = institutions[0].numberOfElements;
        else this.totalItems = 0;
        this.institutionsList = institutions;
      }, (error: AppError) => {
        this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
        throw error;
      });
  }
}
