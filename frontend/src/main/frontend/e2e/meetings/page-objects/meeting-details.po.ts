import {by, element, ElementArrayFinder, ElementFinder} from "protractor";

export class MeetingDetailsPage {
  // get methods
  getMeetingDetailsElement(): ElementFinder {
    return element(by.tagName('app-meeting-details'));
  }

  getTitle(): ElementFinder {
    return element(by.css('div.page-title'));
  }

  getMeetingDate(): ElementFinder {
    return element(by.css('.meeting-info > div.row > div:nth-child(1)'));
  }

  getMeetingDuration(): ElementFinder {
    return element(by.css('.meeting-info > div.row > div:nth-child(2)'));
  }

  getNoMeetingItemsMessage(): ElementFinder {
    return element(by.css('.no-meeting-items'));
  }

  getMeetingItems(): ElementArrayFinder {
    return element.all(by.css('#meetingItemsList > .panel'));
  }

  getMeetingItemTitle(index: number): ElementFinder {
    return element(by.css(`#meetingItemsList > .panel:nth-child(${index+1}) .meeting-item-in-meeting-title`));
  }

  getNumberOfElementInMeetingItemHeader(index: number): ElementArrayFinder {
    return element.all(by.css(`#meetingItemsList > .panel:nth-child(${index+1}) .panel-heading > .row > div`));
  }

  getMenuFromMeetingItem(index: number): ElementFinder {
    return element(by.css(`#meetingItemsList > .panel:nth-child(${index+1}) .meeting-item-in-meeting-menu > span`));
  }

  getViewQuestionnaireOptionFromMeetingItemMenu(index: number): ElementFinder {
    return element(by.css(`#meetingItemsList > .panel:nth-child(${index+1}) .meeting-item-in-meeting-menu a`));
  }

  getCreatorFromMeetingItem(index: number): ElementFinder {
    return element(by.css(`#meetingItemsList > .panel:nth-child(${index+1}) .meeting-item-in-meeting-metadata > span:nth-child(2)`));
  }

  getDateFromMeetingItem(index: number): ElementFinder {
    return element(by.css(`#meetingItemsList > .panel:nth-child(${index+1}) .meeting-item-in-meeting-metadata > span:nth-child(4)`));
  }

  getContentFromMeetingItem(index: number): ElementFinder {
    return element(by.css(`#meetingItemsList > .panel:nth-child(${index+1}) .panel-body`));
  }
}
