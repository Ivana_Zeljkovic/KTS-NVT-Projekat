import { NgModule } from '@angular/core';
// module
import { SharedModule } from "../shared/shared.module";
import { RegistrationRouterModule } from "./registration-router.module";
// component
import { TenantRegistrationComponent } from './tenant-registration/tenant-registration.component';
import { BusinessRegistrationComponent } from './business-registration/business-registration.component';


@NgModule({
  imports: [
    SharedModule,
    RegistrationRouterModule,
  ],
  declarations: [
    TenantRegistrationComponent,
    BusinessRegistrationComponent
  ]
})
export class RegistrationModule { }
