package rs.ac.uns.ftn.ktsnvt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import rs.ac.uns.ftn.ktsnvt.model.entity.Question;

/**
 * Created by Ivana Zeljkovic on 06/12/2017.
 */
public interface QuestionRepository extends JpaRepository<Question, Long> {

    @Modifying
    @Query("DELETE FROM Question WHERE questionnaire.id = :id")
    int removeAllByQuestionnaireId(@Param("id") Long id);

    @Modifying
    @Query(value = "DELETE FROM question_answers WHERE question_id = :id", nativeQuery = true)
    int removeAllAnswersByQuestionId(@Param("id") Long id);
}
