package rs.ac.uns.ftn.ktsnvt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.ForbiddenException;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Account;
import rs.ac.uns.ftn.ktsnvt.model.entity.Building;
import rs.ac.uns.ftn.ktsnvt.model.entity.Business;
import rs.ac.uns.ftn.ktsnvt.repository.BusinessRepository;


/**
 * Created by Ivana Zeljkovic on 05/11/2017.
 */
@Service
public class BusinessServiceImpl implements BusinessService {

    @Autowired
    private BusinessRepository businessRepository;

    @Autowired
    private AccountService accountService;


    @Override
    @Transactional(readOnly = true)
    public Page<Business> findAll(Integer pageNumber, Integer pageSize, String sortDirection, String sortProperty) {
        return this.businessRepository.findAll(new PageRequest(pageNumber, pageSize,
                (sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortProperty));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Business> findAllExceptCurrent(Long currentId, Integer pageNumber, Integer pageSize,
                                               String sortDirection, String sortProperty) {
        return this.businessRepository.findAllExceptCurrent(currentId, new PageRequest(pageNumber, pageSize,
                (sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortProperty));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Business> findAllInstitutionsOrFirms(boolean isFirm, Integer pageNumber, Integer pageSize,
                                                     String sortDirection, String sortProperty) {
        return this.businessRepository.findAllInstitutionsOrFirms(isFirm, new PageRequest(pageNumber, pageSize,
                (sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortProperty));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Business> findAllInstitutionsOrFirmsByAdmin(boolean isFirm, Integer pageNumber, Integer pageSize,
                                                     String sortDirection, String sortProperty) {
        return this.businessRepository.findAllInstitutionsOrFirmsByAdmin(isFirm, new PageRequest(pageNumber, pageSize,
                (sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortProperty));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Business> findAllInstitutionsOrFirmsExceptCurrent(boolean isFirm, Long currentId, Integer pageNumber,
                                                                  Integer pageSize, String sortDirection, String sortProperty) {
        return this.businessRepository.findAllInstitutionsOrFirmsExceptCurrent(isFirm, currentId, new PageRequest(pageNumber,
                    pageSize, (sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortProperty));
    }

    @Override
    @Transactional(readOnly = true)
    public Business findOne(Long id) {
        Business business = this.businessRepository.findOne(id);
        if(business == null) throw new NotFoundException("Business not found!");
        return business;
    }

    @Override
    @Transactional(readOnly = false)
    public Business save(Business business) {
        return this.businessRepository.save(business);
    }

    @Override
    @Transactional(readOnly = false)
    public void remove(Long id) {
        this.businessRepository.delete(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Business findByAddressId(Long id, boolean isCompany) {
        Business business = this.businessRepository.findByAddressId(id, isCompany);
        if(business == null) throw new NotFoundException((isCompany ? "Firm" : "Institution") + " not found!");
        return business;
    }

    @Override
    @Transactional(readOnly = true)
    public void checkPermissionForCurrentBusiness(Long businessId) {
        Business currentBusiness = this.accountService.findByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()).getBusinessOwner();
        // trenutno logovana firma/institucija nema ID business_id
        if(currentBusiness != null && !currentBusiness.getId().equals(businessId))
            throw new ForbiddenException("You don't have permission for this action!");
        else if(currentBusiness == null)
            throw new NullPointerException("There is no business on session");
    }

    @Override
    public boolean checkPasswordUpdate(Account account, String currentPassword, String newPassword, String confirmPassword) {
        if(currentPassword == null && newPassword == null && confirmPassword == null) return true;

        BCryptPasswordEncoder crypt = new BCryptPasswordEncoder();
        if(currentPassword == null) throw new BadRequestException("Value of current password is missing!");
        if(newPassword == null) throw new BadRequestException("Value of new password is missing!");
        if(confirmPassword == null) throw new BadRequestException("Value of confirm password is missing!");

        if(!confirmPassword.equals(newPassword)) throw new BadRequestException("New password not confirmed!");

        if(crypt.matches(currentPassword, account.getPassword())) {
            account.setPassword(crypt.encode(newPassword));
            return true;
        }

        throw new BadRequestException("Invalid value of current password!");
    }

    @Override
    @Transactional(readOnly = true)
    public void checkPermissionForCurrentBusinessInBuilding(Building building) {
        if(building.getCouncil().getResponsibleCompany() == null)
            throw new ForbiddenException("You don't have permission for this action!");

        String currentBusinessUsername = SecurityContextHolder.getContext().getAuthentication().getName();

        if(currentBusinessUsername == null)
            throw new NullPointerException("There is no business on session!");

        boolean companyIsResponsible = false;
        for(Account account : building.getCouncil().getResponsibleCompany().getAccounts()) {
            if(account.getUsername().equals(currentBusinessUsername)) {
                companyIsResponsible = true;
                break;
            }
        }

        if(!companyIsResponsible)
            throw new ForbiddenException("You don't have permission for this action! " +
                    "(Selected building has responsible firm as manager)");
    }

    @Override
    @Transactional(readOnly = true)
    public Business findOneInactive(Long id){
        Business business = this.businessRepository.findOneInactive(id);
        if(business == null) throw new NotFoundException("Business not found!");
        return business;
    }

    @Override
    public Page<Business> findAllWhoDidntGetOffer(Long damageId, Integer pageNumber, Integer pageSize, String sortDirection, String sortProperty) {
        return this.businessRepository.findAllWhoDidntGetOffer(damageId, new PageRequest(pageNumber, pageSize,
                (sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortProperty));
    }


}
