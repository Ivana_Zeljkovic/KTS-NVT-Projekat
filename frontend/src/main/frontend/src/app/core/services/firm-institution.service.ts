import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
// models
import { Business } from "../../shared/models/business";
// error
import { BadRequestError } from "../../shared/errors/bad-request-error";
import { ForbiddenError } from "../../shared/errors/forbidden-error";
import { NotFoundError } from "../../shared/errors/not-found-error";
import { AppError } from "../../shared/errors/app-error";


@Injectable()
export class FirmInstitutionService {
  private readonly urlBase = "/api/businesses";
  constructor(private readonly url: string, protected http: HttpClient) { }

  getByAddress(street: string, number: string, cityName: string, postalNumber: string): Observable<Business> {
    let params: HttpParams = new HttpParams()
      .append('street', street)
      .append('number', number)
      .append('city_name', cityName)
      .append('postal_number', postalNumber);

    return this.http.get<Business>(`${this.urlBase}/${this.url}/address`, {params})
      .catch(this.handleErrors);
  }

  protected handleErrors(response: Response) {
    if(response.status === 400)
      return Observable.throw(new BadRequestError());
    else if(response.status === 403)
      return Observable.throw(new ForbiddenError());
    else if(response.status === 404)
      return Observable.throw(new NotFoundError());
    return Observable.throw(new AppError(response));
  }
}
