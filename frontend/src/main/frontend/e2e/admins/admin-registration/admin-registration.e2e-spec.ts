import { browser } from "protractor";
// page

import {AdminRegistrationPage} from "./page-objects/admin-registration.po";
import {LoginPage} from "../../page-objects/login.po";

describe('Admin registration', () => {
  let loginPage: LoginPage;
  let adminRegistrationPage: AdminRegistrationPage;

  beforeEach(() => {
    loginPage = new LoginPage();
    adminRegistrationPage = new AdminRegistrationPage();
    browser.get("/");

    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');
    loginPage.setUsername('ivana');
    loginPage.setPassword('ivana');
    loginPage.getSignInButton().submit();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/home');
    browser.get("http://localhost:49152/#/admins/register");
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/admins/register');
  });


  afterEach(() => {
    browser.executeScript('window.localStorage.clear();');
  });

  it('should successfully register new admin', () => {

    // expected view of registration page
    expect(adminRegistrationPage.getPageTitle().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getFirstNameInput().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getLastNameInput().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getUsernameInput().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getPasswordInput().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getConfirmPasswordInput().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getSingUpButton().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getSingUpButton().isEnabled()).toBe(false);

    // enter registration data
    adminRegistrationPage.setFirstName('Jasminka');
    adminRegistrationPage.setLastName('Cukurov');
    adminRegistrationPage.setUsername('mamaJaca');
    adminRegistrationPage.setPassword('mamajaca');
    adminRegistrationPage.setConfirmPassword('mamajaca');

    // expected view of registration page after input data
    expect(adminRegistrationPage.getSingUpButton().isEnabled()).toBe(true);

  });

  it('should show error for required first name when user put focus on first name input and leave it', () => {

    // expected view of registration page
    expect(adminRegistrationPage.getPageTitle().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getFirstNameInput().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getLastNameInput().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getUsernameInput().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getPasswordInput().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getConfirmPasswordInput().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getSingUpButton().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getSingUpButton().isEnabled()).toBe(false);

    // enter registration data
    adminRegistrationPage.setFirstName('');
    adminRegistrationPage.setLastName('Cukurov');
    adminRegistrationPage.setUsername('mamaJacaKraljica');
    adminRegistrationPage.setPassword('mamajaca');
    adminRegistrationPage.setConfirmPassword('mamajaca');

    // expected view of registration page after input data
    expect(adminRegistrationPage.getSingUpButton().isEnabled()).toBe(false);
    expect(adminRegistrationPage.getFirstNameRequiredError().isDisplayed()).toBe(true);
  });

  it('should show error that first name must have length 2 or more characters when user put word ' +
    'with less then 2 characters in first name input and leave it', () => {


    // expected view of registration page
    expect(adminRegistrationPage.getPageTitle().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getFirstNameInput().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getLastNameInput().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getUsernameInput().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getPasswordInput().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getConfirmPasswordInput().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getSingUpButton().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getSingUpButton().isEnabled()).toBe(false);

    // enter registration data
    adminRegistrationPage.setFirstName('J');
    adminRegistrationPage.setLastName('Cukurov');
    adminRegistrationPage.setUsername('mamaJacaKraljica');
    adminRegistrationPage.setPassword('mamajaca');
    adminRegistrationPage.setConfirmPassword('mamajaca');

    // expected view of registration page after input data
    expect(adminRegistrationPage.getSingUpButton().isEnabled()).toBe(false);
    expect(adminRegistrationPage.getFirstNameMinimumLengthError().isDisplayed()).toBe(true);
  });

  it('should show error that username can not contain space when user put two words ' +
    'in username input and leave it', () => {

    // expected view of registration page
    expect(adminRegistrationPage.getPageTitle().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getFirstNameInput().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getLastNameInput().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getUsernameInput().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getPasswordInput().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getConfirmPasswordInput().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getSingUpButton().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getSingUpButton().isEnabled()).toBe(false);

    // enter registration data
    adminRegistrationPage.setFirstName('Jasminka');
    adminRegistrationPage.setLastName('Cukurov');
    adminRegistrationPage.setUsername('mama JacaKraljica');
    adminRegistrationPage.setPassword('mamajaca');
    adminRegistrationPage.setConfirmPassword('mamajaca');


    // expected view of registration page after input data
    expect(adminRegistrationPage.getSingUpButton().isEnabled()).toBe(false);
    expect(adminRegistrationPage.getUsernameContainSpaceError().isDisplayed()).toBe(true);
  });

  it('should show error that username is already taken when user put some already used username ' +
    'in username input and leave it', () => {

    // expected view of registration page
    expect(adminRegistrationPage.getPageTitle().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getFirstNameInput().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getLastNameInput().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getUsernameInput().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getPasswordInput().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getConfirmPasswordInput().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getSingUpButton().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getSingUpButton().isEnabled()).toBe(false);

    // enter registration data
    adminRegistrationPage.setFirstName('Jasminka');
    adminRegistrationPage.setLastName('Cukurov');
    adminRegistrationPage.setUsername('ivana');
    adminRegistrationPage.setPassword('mamajaca');
    adminRegistrationPage.setConfirmPassword('mamajaca');

    // expected view of registration page after input data
    expect(adminRegistrationPage.getSingUpButton().isEnabled()).toBe(false);
    expect(adminRegistrationPage.getUsernameTakenError().isDisplayed()).toBe(true);
  });

  it('should show error that username is well created (not used already) when user put some new username ' +
    'in username input and leave it', () => {

    // expected view of registration page
    expect(adminRegistrationPage.getPageTitle().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getFirstNameInput().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getLastNameInput().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getUsernameInput().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getPasswordInput().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getConfirmPasswordInput().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getSingUpButton().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getSingUpButton().isEnabled()).toBe(false);

    // enter registration data
    adminRegistrationPage.setFirstName('Jasminka');
    adminRegistrationPage.setLastName('Cukurov');
    adminRegistrationPage.setUsername('mamaJaca');
    adminRegistrationPage.setPassword('mamajaca');
    adminRegistrationPage.setConfirmPassword('mamajaca');

    // expected view of registration page after input data
    expect(adminRegistrationPage.getSingUpButton().isEnabled()).toBe(true);
    expect(adminRegistrationPage.getUsernameAvailable().isDisplayed()).toBe(true);
  });

  it('should show error passwords are not equals when user put different values ' +
    'in password and confirm password input and leave it', () => {

    // expected view of registration page
    expect(adminRegistrationPage.getPageTitle().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getFirstNameInput().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getLastNameInput().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getUsernameInput().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getPasswordInput().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getConfirmPasswordInput().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getSingUpButton().isDisplayed()).toBe(true);
    expect(adminRegistrationPage.getSingUpButton().isEnabled()).toBe(false);

    // enter registration data
    adminRegistrationPage.setFirstName('Jasminka');
    adminRegistrationPage.setUsername('mamaJacaKraljica');
    adminRegistrationPage.setPassword('mamajaca');
    adminRegistrationPage.setConfirmPassword('mamajacakraljica');
    adminRegistrationPage.setLastName('Cukurov');


    // expected view of registration page after input data
    expect(adminRegistrationPage.getSingUpButton().isEnabled()).toBe(false);
    expect(adminRegistrationPage.getPasswordDoNotMatchError().isDisplayed()).toBe(true);
  });

});
