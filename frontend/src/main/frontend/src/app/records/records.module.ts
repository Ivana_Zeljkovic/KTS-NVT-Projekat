import { NgModule } from '@angular/core';
// module
import { SharedModule } from "../shared/shared.module";
import { RecordsRouterModule } from "./records-router.module";
// component
import { RecordListComponent } from './record-list/record-list.component';
import { RecordComponent } from './record/record.component';
import { QuestionnaireComponent } from "../shared/components/questionnaire/questionnaire.component";


@NgModule({
  imports: [
    SharedModule,
    RecordsRouterModule
  ],
  declarations: [
    RecordListComponent,
    RecordComponent
  ],
  entryComponents: [
    QuestionnaireComponent
  ]
})
export class RecordsModule { }
