import { Record } from "./record";

export interface Meeting {
  id: number;
  date: Date;
  numberOfMeetingItems: number;
  record: Record;
  duration: number;
  numberOfElements?: number;
}
