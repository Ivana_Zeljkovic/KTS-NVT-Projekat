import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { ToasterService } from "angular5-toaster/dist";
// service
import { BusinessService } from "../../core/services/business.service";
// validator
import { SpaceValidator } from "../../shared/validators/space.validator";
// model
import { BusinessType } from "./models/business-type";
import { Login } from "../../shared/models/login";
import { BusinessCreate } from "../../shared/models/business-create";
import { AddressCreate } from "../../shared/models/address-create";
import { CityCreate } from "../../shared/models/city-create";
// component
import { BusinessAddAccountModalComponent } from "../../shared/components/business-add-account/bussiness-add-account-modal.component";
// error
import { BadRequestError } from "../../shared/errors/bad-request-error";
import { AppError } from "../../shared/errors/app-error";


@Component({
  selector: 'app-business-registration',
  templateUrl: './business-registration.component.html',
  styleUrls: ['./business-registration.component.css']
})

export class BusinessRegistrationComponent implements OnInit {
  isFirm: boolean;
  form: FormGroup;
  returnURL: string;
  businessTypes: any[];
  logins : Login[];
  selectedType: string;
  bsModalRef: BsModalRef;

  constructor(private fb: FormBuilder, private router: Router, private route: ActivatedRoute,
              private businessService: BusinessService, private modalService : BsModalService,
              private toasterService: ToasterService) {
    this.form = this.fb.group({
      name: ['', [
        Validators.required,
        Validators.minLength(3)
      ]],
      street: ['', [
        Validators.required,
        Validators.minLength(5)
      ]],
      description: ['', [
        Validators.required,
        Validators.minLength(10)
      ]],
      email: ['', [
        Validators.required, Validators.email
      ]],
      phoneNumber: ['', [
        Validators.required,
        SpaceValidator.cannotContainSpace,
        Validators.pattern('[0-9]{6,10}')
      ]],
      number: ['', [
        Validators.required,
        SpaceValidator.cannotContainSpace
      ]],
      city: ['', [
        Validators.required,
      ]],
      postalCode: ['',[
        Validators.required,
        Validators.pattern('[0-9]{5}')
      ]]
    });

    let everything = Object.keys(BusinessType);
    this.businessTypes = everything.splice(everything.length / 2);
  }

  ngOnInit() {
    this.isFirm = this.router.url === '/registration/firm';
    this.returnURL = this.route.snapshot.queryParams['returnUrl'] || '/';
    this.logins = new Array<Login>();
    this.selectedType = 'POLICE';
  }

  get name() {
    return this.form.get('name');
  }

  get street() {
    return this.form.get('street');
  }

  get description() {
    return this.form.get('description');
  }

  get email() {
    return this.form.get('email');
  }

  get phoneNumber() {
    return this.form.get('phoneNumber');
  }

  get number() {
    return this.form.get('number');
  }

  get city() {
    return this.form.get('city');
  }

  get postalCode() {
    return this.form.get('postalCode');
  }

  displayAddAccount() {
     this.bsModalRef = this.modalService.show(BusinessAddAccountModalComponent);
     this.bsModalRef.content.usersList = this.logins;
     this.bsModalRef.content.bsModalRef = this.bsModalRef;
  }

  onChange(newValue: string) {
    this.selectedType = newValue;
  }

  removeUsername(username) {
      let index = this.logins.indexOf(username);
      this.logins.splice(index, 1);
  }

  register() {
    let businessCreate = new BusinessCreate(this.name.value, this.description.value, this.phoneNumber.value, this.email.value,
                                  this.selectedType.toString(), new AddressCreate(this.street.value, this.number.value,
                                    new CityCreate(this.city.value, this.postalCode.value)), this.logins);
    this.businessService.save(businessCreate, this.isFirm ? 'businesses/firms' : 'businesses/institutions')
      .subscribe(data => {
        this.toasterService.pop('success', 'Congratulation', 'Successfully registered new ' + ((this.isFirm) ? 'firm!' : 'institution!'));
        setTimeout(() => {
          this.router.navigateByUrl(this.returnURL);
        }, 2000);
      }, (error: AppError) => {
        if(error instanceof BadRequestError)
          this.toasterService.pop('error', 'Error', 'Given data have bad format!');
        else {
          this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
          throw error;
        }
      });
  }
}
