import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FixDamageListModalComponent } from './fix-damage-list-modal.component';

describe('FixDamageListModalComponent', () => {
  let component: FixDamageListModalComponent;
  let fixture: ComponentFixture<FixDamageListModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FixDamageListModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FixDamageListModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
