package rs.ac.uns.ftn.ktsnvt.service;

import org.springframework.data.domain.Page;
import rs.ac.uns.ftn.ktsnvt.model.entity.Offer;

import java.util.List;

/**
 * Created by Katarina Cukurov on 24/11/2017.
 */
public interface OfferService {

    Offer save(Offer offer);

    Offer findOne(Long id);

    List<Offer> findAll();

    Page<Offer> findAllForCurrentDamage(Long damageId, Integer integer, Integer integer1, String sortDirection, String sortProperty);

    Offer getOfferByDamageAndBusiness(Long damageId, Long businessId);
}
