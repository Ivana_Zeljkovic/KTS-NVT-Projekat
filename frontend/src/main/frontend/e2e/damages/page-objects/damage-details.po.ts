import {by, element, ElementArrayFinder, ElementFinder} from "protractor";
import {elementEnd} from "@angular/core/src/render3/instructions";

export class DamageDetailsPage {
  // get methods
  getDamageDetailsElement(): ElementFinder {
    return element(by.tagName('app-damage-details'));
  }

  getTitle(): ElementFinder {
    return element(by.css('.page-title > h3'));
  }

  getDamageCreator(): ElementFinder {
    return element(by.css('div.panel-damage-heading > span:nth-child(2)'));
  }

  getDamageDate(): ElementFinder {
    return element(by.css('div.panel-damage-heading > span:nth-child(4)'));
  }

  getDamageDescription(): ElementFinder {
    return element(by.css('div.panel-damage-body > p:nth-child(2)'));
  }

  getDamageType(): ElementFinder {
    return element(by.css('ul.list-group > li:nth-child(1)'));
  }

  getDamageAddress(): ElementFinder {
    return element(by.css('ul.list-group > li:nth-child(2)'));
  }

  getDamageResponsiblePerson(): ElementFinder {
    return element(by.css('ul.list-group > li:nth-child(3)'));
  }

  getDamageResponsibleInstitution(): ElementFinder {
    return element(by.css('ul.list-group > li:nth-child(4)'));
  }

  getDamageSelectedFirm(order: number): ElementFinder {
    return element(by.css(`ul.list-group > li:nth-child(${order})`));
  }

  getLabelUrgent(): ElementFinder {
    return element(by.css('div.label-urgent'));
  }

  getCommentList(): ElementFinder {
    return element(by.id('commentList'));
  }

  getCommentsSectionTitle(): ElementFinder {
    return element(by.css('div.comments-title > h4'));
  }

  getAddButton(): ElementFinder {
    return element(by.buttonText('Add'));
  }

  getCommentListPanels(): ElementArrayFinder {
    return element.all(by.css('#commentList div.panel'));
  }

  getContentFromCommentPanel(index: number): ElementFinder {
    return element(by.css(`#commentList > .panel:nth-child(${index+1}) .panel-comment-body`))
  }

  getCreatorFromCommentPanel(index: number): ElementFinder {
    return element(by.css(`#commentList > .panel:nth-child(${index+1}) div.comment-metadata > span:nth-child(2)`))
  }

  getDateFromCommentPanel(index: number): ElementFinder {
    return element(by.css(`#commentList > .panel:nth-child(${index+1}) div.comment-metadata > span:nth-child(4)`))
  }

  getMenuFromCommentPanel(index: number): ElementFinder {
    return element(by.css(`#commentList > .panel:nth-child(${index+1}) .comment-menu`));
  }

  getContentOfMenuFromCommentPanel(index: number): ElementFinder {
    return element(by.css(`#commentList > .panel:nth-child(${index+1}) .comment-menu #commentPopover`));
  }

  getEditLinkFromCommentPanel(index: number): ElementFinder {
    return element(by.css(`#commentList > .panel:nth-child(${index+1}) .comment-menu #commentPopover > div:nth-child(1) > a`));
  }

  getDeleteLinkFromCommentPanel(index: number): ElementFinder {
    return element(by.css(`#commentList > .panel:nth-child(${index+1}) .comment-menu #commentPopover > div:nth-child(2) > a`));
  }
}
