package rs.ac.uns.ftn.ktsnvt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.City;
import rs.ac.uns.ftn.ktsnvt.repository.CityRepository;

/**
 * Created by Katarina Cukurov on 07/11/2017.
 */
@Service
public class CityServiceImpl implements CityService{

    @Autowired
    private CityRepository cityRepository;


    @Override
    @Transactional(readOnly = true)
    public City findOne(Long id) {
        return this.cityRepository.findOne(id);
    }

    @Override
    @Transactional(readOnly = false)
    public City save(City building) {
        return this.cityRepository.save(building);
    }

    @Override
    @Transactional(readOnly = true)
    public City findByName(String name) {
        return this.cityRepository.findByName(name);
    }

    @Override
    @Transactional(readOnly = true)
    public City findByNameAndPostalNumber(String name, int postalNumber) {
        City city = this.cityRepository.findByNameAndPostalNumber(name, postalNumber);
        if(city == null) throw new NotFoundException("There is no city with name: " + name + " and postal number: " + postalNumber);
        return city;
    }

    @Override
    @Transactional(readOnly = false)
    public void remove(Long id) {
        this.cityRepository.delete(id);
    }
}
