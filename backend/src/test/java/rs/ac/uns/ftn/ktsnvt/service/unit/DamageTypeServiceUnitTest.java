package rs.ac.uns.ftn.ktsnvt.service.unit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Damage;
import rs.ac.uns.ftn.ktsnvt.model.entity.DamageType;
import rs.ac.uns.ftn.ktsnvt.repository.DamageTypeRepository;
import rs.ac.uns.ftn.ktsnvt.service.DamageTypeService;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.BDDMockito.given;

/**
 * Created by Nikola Garabandic on 30/11/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DamageTypeServiceUnitTest {

    @Autowired
    private DamageTypeService damageTypeService;

    @MockBean
    private DamageTypeRepository damageTypeRepository;


    @Before
    public void setUp(){
        DamageType damageType = new DamageType("Vodovodni kvar");

        given(
                this.damageTypeRepository.findOne(200L)
        ).willReturn(
                damageType
        );

        given(
                this.damageTypeRepository.findOne(201L)
        ).willReturn(
                null
        );

        given(
                this.damageTypeRepository.findByName("Vodovodni kvar")
        ).willReturn(
                damageType
        );

        given(
                this.damageTypeRepository.findByName("Vodovodni kva")
        ).willReturn(
                null
        );
    }

    @Test
    public void testFindOne(){
        /*
         * Test proverava ispravnost rada metode FindOne servisa DamageType
         * (dobavljanje damagetype objekta na osnovu address ID-a)
         * u slucaju kada postoji damagetype sa zadatim Id-em
         * */

        DamageType damageType = this.damageTypeService.findOne(200L);

        assertNotNull(damageType);
        assertEquals("Vodovodni kvar", damageType.getName());
    }

    @Test(expected = NotFoundException.class)
    public void testFindNone() {
        /*
         * Test proverava ispravnost rada metode FindOne servisa DamageType
         * (dobavljanje damagetype objekta na osnovu  ID-a)
         * u slucaju kada ne postoji damagetype sa zadatim Id-em - ocekivano NotFoundException
         * */

        DamageType damageType = this.damageTypeService.findOne(201L);
    }

    @Test
    public void testFindByName(){
        /*
         * Test proverava ispravnost rada metode FindByName servisa DamageType
         * (dobavljanje damagetype objekta na osnovu imena)
         * u slucaju kada postoji damagetype sa zadatim Id-em
         * */

        DamageType damageType = this.damageTypeService.findByName("Vodovodni kvar");

        assertNotNull(damageType);
        assertEquals("Vodovodni kvar", damageType.getName());
    }

    @Test(expected = NotFoundException.class)
    public void testFindNoneByName() {
        /*
         * Test proverava ispravnost rada metode FindByName servisa DamageType
         * (dobavljanje damagetype objekta na osnovu imena)
         * u slucaju kada ne postoji damagetype sa zadatim Id-em - ocekivano NotFoundException
         * */

        DamageType damageType = this.damageTypeService.findByName("Vodovodni kva");
    }
}
