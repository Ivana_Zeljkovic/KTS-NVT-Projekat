import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BsModalRef } from "ngx-bootstrap";
import { ToasterService } from "angular5-toaster/dist";
import * as _ from "lodash";
// model
import { DamageUpdate } from "../../shared/models/damage-update";
import { Damage } from "../../shared/models/damage";
// service
import { DamageService } from "../../core/services/damage.service";
import { JwtService } from "../../core/services/jwt.service";
// error
import { AppError } from "../../shared/errors/app-error";
import { BadRequestError } from "../../shared/errors/bad-request-error";
import { ForbiddenError } from "../../shared/errors/forbidden-error";
import { NotFoundError } from "../../shared/errors/not-found-error";


@Component({
  selector: 'app-damage-edit',
  templateUrl: './damage-edit.component.html',
  styleUrls: ['./damage-edit.component.css']
})
export class DamageEditComponent implements OnInit {
  @Input() model: DamageUpdate;
  @Input() initialValues: DamageUpdate;
  @Input() damageId: number;
  @Output() submited = new EventEmitter<Damage>();
  disableSaveAndReset: boolean;


  constructor(public modalRef: BsModalRef, private damageService: DamageService,
              private jwtService: JwtService, private toasterService: ToasterService) { }

  ngOnInit() {
    this.disableSaveAndReset = true;
  }

  onSubmit() {
    this.damageService.update(this.damageId, this.model, 'tenants', this.jwtService.getIdFromToken(), 'my_damages')
      .subscribe(updatedDamage => {
        this.submited.emit(updatedDamage);
        this.modalRef.hide();
      }, (error: AppError) => {
        if(error instanceof BadRequestError)
          this.toasterService.pop('error', 'Error',
            'Selected damage is already in fixing process!');
        else if(error instanceof ForbiddenError)
          this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
        else if(error instanceof NotFoundError)
          this.toasterService.pop('error', 'Error', 'Damage not found!');
        else {
          this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
          throw error;
        }
      });
  }

  reset() {
    this.model = _.cloneDeep(this.initialValues);
    this.disableSaveAndReset = true;
  }

  changeExist(newValues) {
    let urgentBool = newValues.urgent === 'true';
    this.disableSaveAndReset = (newValues.description === this.initialValues.description &&
      urgentBool === this.initialValues.urgent);
  }
}
