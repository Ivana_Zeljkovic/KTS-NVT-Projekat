import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS, Validator, FormControl } from '@angular/forms';

@Directive({
  selector: '[customMaxTime][ngModel]',
  providers: [{provide: NG_VALIDATORS, useExisting: CustomMaxTimeDirective, multi: true}]
})
export class CustomMaxTimeDirective implements Validator {
  @Input()
  customMaxTime: Date;

  validate(c: FormControl): {[key: string]: any} {
    let v = c.value;
    if(!v) return null;
    return ( v.getHours() > this.customMaxTime.getHours() ||
      (v.getHours() === this.customMaxTime.getHours() && v.getMinutes() >= this.customMaxTime.getMinutes())) ?
      {"customMaxTime": true} : null;
  }
}
