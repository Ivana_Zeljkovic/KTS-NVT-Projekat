import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import * as _ from "lodash";
// component
import { QuestionComponent } from "../../../home/tenant-home/question/question.component";
// model
import { QuestionCreate } from "../../models/question-create";
import { QuestionnaireCreate } from "../../models/questionnaire-create";
import { QuestionnaireUpdate } from "../../models/questionnaire-update";
import { QuestionUpdate } from "../../models/question-update";


@Component({
  selector: 'app-questionnaire',
  templateUrl: './questionnaire.component.html',
  styleUrls: ['./questionnaire.component.css']
})
export class QuestionnaireComponent implements OnInit {
  @Input() model: any;
  @Input() initialValues: any;
  @Input() isReviewMode: boolean;
  @Output() submitted = new EventEmitter<void>();

  constructor(private modalRef: BsModalRef, private modalService: BsModalService) { }

  ngOnInit() { }

  createQuestion() {
    const modalRef = this.modalService.show(QuestionComponent);
    if(this.model instanceof QuestionnaireCreate)
      modalRef.content.model = new QuestionCreate('', new Array<string>());
    else if(this.model instanceof QuestionnaireUpdate)
      modalRef.content.model = new QuestionUpdate(-1, '', new Array<string>());
    modalRef.content.initialValues = _.cloneDeep(modalRef.content.model);

    modalRef.content.submitted.subscribe((question) => {
      this.model.questions.push(question);
    });
  }

  removeQuestion(question) {
    let index = this.model.questions.indexOf(question);
    this.model.questions.splice(index, 1);
  }

  changeExist() {
    return !(JSON.stringify(this.model) === JSON.stringify(this.initialValues));
  }

  save() {
    this.submitted.emit();
    this.modalRef.hide();
  }

  reset() {
    this.model = _.cloneDeep(this.initialValues);
  }
}
