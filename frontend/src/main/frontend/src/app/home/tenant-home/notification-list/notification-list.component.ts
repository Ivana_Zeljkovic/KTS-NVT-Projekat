import { Component, OnInit } from '@angular/core';
import { ToasterService } from "angular5-toaster/dist";
import { BsDatepickerConfig, BsLocaleService, BsModalService, defineLocale, enGb } from "ngx-bootstrap";
import * as _ from "lodash";
// model
import { Notification } from "../../../shared/models/notification";
import { NotificationCreateUpdate } from "../../../shared/models/notification-create-update";
import { PageAndSort } from "../../../shared/models/page-and-sort";
// service
import { NotificationService } from "../../../core/services/notification.service";
import { JwtService } from "../../../core/services/jwt.service";
// error
import { AppError } from "../../../shared/errors/app-error";
import { NotFoundError } from "../../../shared/errors/not-found-error";
import { ForbiddenError } from "../../../shared/errors/forbidden-error";
import { BadRequestError } from "../../../shared/errors/bad-request-error";
// component
import { ConfirmDeleteModalComponent } from "../../../shared/components/confirm-delete-modal/confirm-delete-modal.component";
import { NotificationComponent } from "../notification/notification.component";
// pipe
import { DateTimeFormatServerPipe } from "../../../shared/pipes/date-time-format-server.pipe";


@Component({
  selector: 'app-notification-list',
  templateUrl: './notification-list.component.html',
  styleUrls: ['./notification-list.component.css']
})
export class NotificationListComponent implements OnInit {
  notificationList: Array<Notification>;
  buildingID: number;
  daterangepickerModel: Date[];
  isSearchMode: boolean;
  dateTimeFormatPipe: DateTimeFormatServerPipe;

  // Daterangepicker configuration
  bsConfig: Partial<BsDatepickerConfig>;
  maxDate: Date;

  // Elements for bootstrap paging
  currentPage: number;
  totalItems: number;
  maxSize: number;
  itemsPerPage: number;

  // Elements for getting data from server
  pageSize: string;
  sortDirection: string;
  sortProperties: string[];


  constructor(private notificationService: NotificationService, private jwtService: JwtService,
              private toasterService: ToasterService, private modalService: BsModalService,
              private localeService: BsLocaleService) {
    // Daterangepicker config
    this.bsConfig = Object.assign({}, {containerClass: 'theme-dark-blue'});
    this.maxDate = new Date();
    defineLocale('enGb', enGb);
    this.localeService.use('enGb');
    this.daterangepickerModel = [];
    this.dateTimeFormatPipe = new DateTimeFormatServerPipe();

    // Sett up for bootstrap paging
    this.currentPage = 1;
    this.maxSize = 3;
    this.itemsPerPage = 4;

    // Sett up for getting notification request
    this.pageSize = '4';
    this.sortDirection = 'DESC';
    this.sortProperties = ['date'];
  }

  ngOnInit() {
    this.buildingID = this.jwtService.getBuildingIdFromToken();
    // load list of notifications only if tenant is connected to some apartment (and building)
    if (this.buildingID !== -1)
      this.loadNotificationList();
  }

  add() {
    const modalRef = this.modalService.show(NotificationComponent);
    modalRef.content.model = new NotificationCreateUpdate('', '');
    modalRef.content.update = false;
    modalRef.content.initialValues = _.cloneDeep(modalRef.content.model);

    modalRef.content.submited.subscribe(() => {
      this.currentPage = 1;
      this.loadNotificationList();
    });
  }

  edit(notification: Notification) {
    let index = this.notificationList.indexOf(notification);
    const modalRef = this.modalService.show(NotificationComponent);
    modalRef.content.model = new NotificationCreateUpdate(notification.title, notification.content);
    modalRef.content.update = true;
    modalRef.content.notificationID = notification.id;
    modalRef.content.initialValues = _.cloneDeep(modalRef.content.model);

    modalRef.content.submited.subscribe(updatedNotification => {
      this.notificationList[index] = updatedNotification;
    });
  }

  remove(notification: Notification) {
    const modalRef = this.modalService.show(
      ConfirmDeleteModalComponent,
      Object.assign({}, {class: 'modal-sm'})
    );

    let numberOfPages = Math.ceil(notification.numberOfElements / +(this.pageSize));
    let pageShouldDecrement = false;
    if(numberOfPages === this.currentPage && (notification.numberOfElements - 1) % +(this.pageSize) === 0) {
      pageShouldDecrement = true;
    }

    modalRef.content.confirmed.subscribe((value) => {
      if (value) {
        this.notificationService.remove(notification.id, 'buildings', this.buildingID, 'billboard/notifications')
          .subscribe(() => {
            if(pageShouldDecrement && this.currentPage !== 1)
              this.currentPage--;
            this.loadNotificationList();
          }, (error: AppError) => {
            if (error instanceof BadRequestError)
              this.toasterService.pop('error', 'Error', 'Selected notification doesn\'t belong to this building!');
            else if (error instanceof ForbiddenError)
              this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
            else if (error instanceof NotFoundError)
              this.toasterService.pop('error', 'Error', 'Notification not found!');
            else {
              this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
              throw error;
            }
          });
      }
    });
  }

  pageChanged(event: any) {
    this.currentPage = event.page;
    this.loadNotificationList();
  }

  search() {
    this.isSearchMode = true;
    if (this.daterangepickerModel.length > 0) {
      this.setTime();
      this.currentPage = 1;
      this.loadNotificationList();
    }
  }

  reset() {
    this.isSearchMode = false;
    this.daterangepickerModel = [];
    this.currentPage = 1;
    this.loadNotificationList();
  }

  setTime() {
    if (this.daterangepickerModel.length > 0) {
      this.daterangepickerModel[0].setHours(0, 0, 0);
      this.daterangepickerModel[1].setHours(23, 59, 59);
    }
  }

  private loadNotificationList() {
    let page = new PageAndSort((this.currentPage - 1).toString(), this.pageSize, this.sortDirection, this.sortProperties);

    if (!this.isSearchMode) {
      // getting page, without date search
      this.notificationService.getPage(page, 'buildings', this.buildingID, 'billboard/notifications')
        .subscribe(notifications => {
          if (notifications.length !== 0)
            this.totalItems = notifications[0].numberOfElements;
          else this.totalItems = 0;
          this.notificationList = notifications;
        }, (error: AppError) => {
          if(error instanceof ForbiddenError)
            this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
          else if(error instanceof NotFoundError)
            this.toasterService.pop('error', 'Error', 'Building not found!');
          else {
            this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
            throw error;
          }
        });
    }
    else {
      // getting page, but with date search
      let from = this.dateTimeFormatPipe.transform(this.daterangepickerModel[0]);
      let to = this.dateTimeFormatPipe.transform(this.daterangepickerModel[1]);
      this.notificationService.searchFromTo(this.buildingID, from, to, page)
        .subscribe(notifications => {
          if (notifications.length !== 0)
            this.totalItems = notifications[0].numberOfElements;
          else this.totalItems = 0;
          this.notificationList = notifications;
        }, (error: AppError) => {
          if(error instanceof ForbiddenError)
            this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
          else if(error instanceof NotFoundError)
            this.toasterService.pop('error', 'Error', 'Building not found!');
          else {
            this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
            throw error;
          }
        });
    }
  }
}
