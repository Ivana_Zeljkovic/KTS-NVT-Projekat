import { TestBed, inject } from '@angular/core/testing';

import { ApartmentService } from './apartment.service';
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {Apartment} from "../../shared/models/apartment";
import {PageAndSort} from "../../shared/models/page-and-sort";
import {Tenant} from "../../shared/models/tenant";

describe('ApartmentService', () => {

  let apartmentService: ApartmentService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [ApartmentService]
    });

    apartmentService = TestBed.get(ApartmentService);
    httpMock = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
    console.log(httpMock);
  });

  it('should be created', inject([ApartmentService], (service: ApartmentService) => {
    expect(service).toBeTruthy();
  }));

  it('should get all free apartments in building with specific id', () => {
    const responseList = [
      {
        'id': 1,
        'number' : 1,
        'floor' : 1,
        'surface' : 50,
        'owner': {
          'firstName': 'Danijela',
          'lastName': 'Zeljkovic',
          'username': 'daca',
          'birthDate' : new Date(),
          'email' : 'daca@gmail.com',
          'phoneNumber': '1202568',
          'confirmed' : true,
          'id': 2,
          'address': {
            'street': 'Janka Veselinovica',
            'number': 8,
            'city': {
              'name': 'Novi Sad',
              'postalNumber': '21000'
            }
          },
        },
        'tenants' : new Array<Tenant>(),
        'buildingId' : 1
      },
      {
        'id': 2,
        'number' : 2,
        'floor' : 1,
        'surface' : 20,
        'owner': {
          'firstName': 'Djulinka',
          'lastName': 'Cukurov',
          'username': 'djuli',
          'birthDate' : new Date(),
          'email' : 'djuli@gmail.com',
          'phoneNumber': '1202568',
          'confirmed' : true,
          'id': 2,
          'address': {
            'street': 'Janka Veselinovica',
            'number': 8,
            'city': {
              'name': 'Novi Sad',
              'postalNumber': '21000'
            }
          },
        },
        'tenants' : new Array<Tenant>(),
        'buildingId' : 1
      },
      {
        'id': 3,
        'number' : 3,
        'floor' : 1,
        'surface' : 30,
        'owner': {
          'firstName': 'Verica',
          'lastName': 'Skajovski',
          'username': 'veki',
          'birthDate' : new Date(),
          'email' : 'veki@gmail.com',
          'phoneNumber': '1202568',
          'confirmed' : true,
          'id': 2,
          'address': {
            'street': 'Janka Veselinovica',
            'number': 8,
            'city': {
              'name': 'Novi Sad',
              'postalNumber': '21000'
            }
          },
        },
        'tenants' : new Array<Tenant>(),
        'buildingId' : 1
      },
    ];

    let page = new PageAndSort('0', '2', 'ASC', ['id']);
    apartmentService.getFreeInBuilding(page, 'Janka Veselinovica','8', 'Novi Sad', '21000')
      .subscribe((apartments: Array<Apartment>) => {
        expect(apartments).toEqual(responseList);
        expect(apartments.length).toEqual(3);
      });
    const httpRequest = httpMock.expectOne(req => {
    return req.urlWithParams ===
    '/api/apartments/free_apartments_building?' +
      'street=Janka%20Veselinovica&' +
      'number=8&' +
      'city_name=Novi%20Sad&' +
      'postal_number=21000&' +
      'pageNumber=0&' +
      'pageSize=2&' +
      'sortDirection=ASC&' +
      'sortProperty=id&='});
    expect(httpRequest.request.method).toEqual('GET');
    expect(httpRequest.request.params.get('street')).toEqual('Janka Veselinovica');
    expect(httpRequest.request.params.get('number')).toEqual('8');
    expect(httpRequest.request.params.get('city_name')).toEqual('Novi Sad');
    expect(httpRequest.request.params.get('postal_number')).toEqual('21000');
    expect(httpRequest.request.params.get('pageNumber')).toEqual('0');
    expect(httpRequest.request.params.get('pageSize')).toEqual('2');
    expect(httpRequest.request.params.get('sortDirection')).toEqual('ASC');
    expect(httpRequest.request.params.get('sortProperty')).toEqual('id');
    httpRequest.flush(responseList);
  });

  it('should get all free apartments in range in specific building', () => {
    const responseList = [
      {
        'id': 1,
        'number' : 1,
        'floor' : 1,
        'surface' : 50,
        'owner': {
          'firstName': 'Danijela',
          'lastName': 'Zeljkovic',
          'username': 'daca',
          'birthDate' : new Date(),
          'email' : 'daca@gmail.com',
          'phoneNumber': '1202568',
          'confirmed' : true,
          'id': 2,
          'address': {
            'street': 'Janka Veselinovica',
            'number': 8,
            'city': {
              'name': 'Novi Sad',
              'postalNumber': '21000'
            }
          },
        },
        'tenants' : new Array<Tenant>(),
        'buildingId' : 1
      },
      {
        'id': 2,
        'number' : 2,
        'floor' : 1,
        'surface' : 20,
        'owner': {
          'firstName': 'Djulinka',
          'lastName': 'Cukurov',
          'username': 'djuli',
          'birthDate' : new Date(),
          'email' : 'djuli@gmail.com',
          'phoneNumber': '1202568',
          'confirmed' : true,
          'id': 2,
          'address': {
            'street': 'Janka Veselinovica',
            'number': 8,
            'city': {
              'name': 'Novi Sad',
              'postalNumber': '21000'
            }
          },
        },
        'tenants' : new Array<Tenant>(),
        'buildingId' : 1
      },
      {
        'id': 3,
        'number' : 3,
        'floor' : 1,
        'surface' : 30,
        'owner': {
          'firstName': 'Verica',
          'lastName': 'Skajovski',
          'username': 'veki',
          'birthDate' : new Date(),
          'email' : 'veki@gmail.com',
          'phoneNumber': '1202568',
          'confirmed' : true,
          'id': 2,
          'address': {
            'street': 'Janka Veselinovica',
            'number': 8,
            'city': {
              'name': 'Novi Sad',
              'postalNumber': '21000'
            }
          },
        },
        'tenants' : new Array<Tenant>(),
        'buildingId' : 1
      },
    ];

    let page = new PageAndSort('0', '2', 'ASC', ['id']);
    apartmentService.getFreeInRange(page, '0','80', '1')
      .subscribe((apartments: Array<Apartment>) => {
        expect(apartments).toEqual(responseList);
        expect(apartments.length).toEqual(3);
      });
    const httpRequest = httpMock.expectOne(req => {
      return req.urlWithParams === '/api/apartments/free_apartments_size?' +
      'min=0&' +
      'max=80&' +
      'buildingId=1&' +
      'pageNumber=0&' +
      'pageSize=2&' +
      'sortDirection=ASC&' +
      'sortProperty=id&='
    });
    expect(httpRequest.request.method).toEqual('GET');
    expect(httpRequest.request.params.get('min')).toEqual('0');
    expect(httpRequest.request.params.get('max')).toEqual('80');
    expect(httpRequest.request.params.get('buildingId')).toEqual('1');
    expect(httpRequest.request.params.get('pageNumber')).toEqual('0');
    expect(httpRequest.request.params.get('pageSize')).toEqual('2');
    expect(httpRequest.request.params.get('sortDirection')).toEqual('ASC');
    expect(httpRequest.request.params.get('sortProperty')).toEqual('id');
    httpRequest.flush(responseList);
  })
});
