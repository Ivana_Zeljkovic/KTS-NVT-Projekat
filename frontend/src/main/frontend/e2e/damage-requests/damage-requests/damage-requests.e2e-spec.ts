import {LoginPage} from "../../page-objects/login.po";
import {browser, by, element, protractor} from "protractor";
import {NavbarPage} from "../../page-objects/navbar.po";
import {DamageRequestsPage} from "./page-objects/damage-requests.po";
import {OfferModal} from "../offer-modal/page-objects/offer-modal.po";

describe('View list of all requests for offers.', () => {

  let loginPage: LoginPage;
  let navbarPage: NavbarPage;
  let damageRequest: DamageRequestsPage;
  let offerModal: OfferModal;

  beforeEach(() => {
    loginPage = new LoginPage();
    navbarPage = new NavbarPage();
    damageRequest = new DamageRequestsPage();
    offerModal = new OfferModal();
    browser.get("/");
  });

  afterEach(() => {
    browser.executeScript('window.localStorage.clear();');
  });

  it('should display no damage requests', () => {
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');

    // enter login data
    loginPage.setUsername('firm1');
    loginPage.setPassword('firm1');

    loginPage.getSignInButton().submit();

    browser.waitForAngular();
    browser.get('http://localhost:49152/#/damage-requests');

    expect(damageRequest.getNoDamageRequests().isDisplayed()).toBe(true);
  });

  it('should display damage requests and user can click on three dots to open the modal', () => {

    // enter login data
    loginPage.setUsername('firm2');
    loginPage.setPassword('firm2');

    loginPage.getSignInButton().submit();

    browser.waitForAngular();
    browser.get('http://localhost:49152/#/damage-requests');


    expect(damageRequest.getDamageRequestsTable().isDisplayed()).toBe(true);

    damageRequest.getPopElement().click().then(function () {
      damageRequest.getFirstSendOfferAnchor().click().then(function () {
           expect(offerModal.getModalClass().isDisplayed()).toBe(true);
      });
    });


  });

  it('should display damage requests and user can click on three dots to go to damage details', () => {

    // enter login data
    loginPage.setUsername('firm2');
    loginPage.setPassword('firm2');

    loginPage.getSignInButton().submit();

    browser.waitForAngular();
    browser.get('http://localhost:49152/#/damage-requests');


    expect(damageRequest.getDamageRequestsTable().isDisplayed()).toBe(true);

    damageRequest.getPopElement().click().then(function () {
      damageRequest.getDamageDetailsAnchor().click().then(function () {

        expect(browser.getCurrentUrl()).toMatch(/http:\/\/localhost:49152\/#\/damages\/\d+/);
      });
    });
  });
});
