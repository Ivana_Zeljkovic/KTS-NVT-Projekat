import { Tenant } from "./tenant";

export interface Apartment {
  id: number;
  number: number;
  floor: number;
  surface: number;
  owner: Tenant;
  tenants: Array<Tenant>;
  buildingId: number;
  numberOfElements?: number;
}
