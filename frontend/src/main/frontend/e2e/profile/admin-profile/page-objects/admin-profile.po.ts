import {by, element, ElementFinder, protractor} from "protractor";

export class AdminProfilePage {

  getPanelHeading(): ElementFinder {
    return element(by.css('.panel-heading.panel-heading-personal'));
  }

  getUsernameInput(){
    return element(by.id('username'));
  }

  getFirstNameInput(){
    return element(by.id('firstName'));
  }

  getLastNameInput(){
    return element(by.id('lastName'));
  }

  getCurrentPasswordInput(){
    return element(by.id('currentPassword'));
  }

  getNewPasswordInput(){
    return element(by.id('password'));
  }

  getConfirmPasswordInput(){
    return element(by.id('confirmPassword'));
  }

  getSaveButton(){
    return element(by.css('.btn.save'));
  }

  getResetButton(){
    return element(by.css('.btn.reset'));
  }

  //Setters for input fields.
  setFirstName(value : string){
    this.clearField(this.getFirstNameInput());
    this.getFirstNameInput().sendKeys(value);
  }

  setLastName(value : string){
    this.clearField(this.getLastNameInput());
    this.getLastNameInput().sendKeys(value);
  }

  setCurrentPassword(value : string){
    this.getCurrentPasswordInput().sendKeys(value);
  }

  setNewPassword(value : string){
    this.getNewPasswordInput().sendKeys(value);
  }

  setConfirmPassword(value : string){
    this.getConfirmPasswordInput().sendKeys(value);
  }

  //getters for validation errors
  getfirstNameMinLengthError(){
    return element(by.id('firstNameMinimumLength'));
  }

  getFirstNameRequired(){
    return element(by.id('firstNameRequired'));
  }

  getLastNameMinLengthError(){
    return element(by.id('lastNameMinimumLength'));
  }

  getLastNameRequired(){
    return element(by.id('lastNameRequired'));
  }

  getNewPasswordLength(){
    return element(by.id('newPasswordMinLength'))
  }

  getNewPasswordRequired(){
    return element(by.id('newPasswordRequired'))
  }

  getConfirmPasswordRequired(){
    return element(by.id('confirmPasswordRequired'))
  }

  getConfirmPasswordMatch(){
    return element(by.id('confirmPasswordNotMatch'));
  }

  getCurrentPasswordRequired(){
    return element(by.id('currentPasswordRequired'));
  }

  clearField(field) {
    field.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, "a"));
    field.sendKeys(protractor.Key.BACK_SPACE);
    field.clear();
  }
}
