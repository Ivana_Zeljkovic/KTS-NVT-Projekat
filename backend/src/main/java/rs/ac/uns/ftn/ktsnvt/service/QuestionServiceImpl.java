package rs.ac.uns.ftn.ktsnvt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Question;
import rs.ac.uns.ftn.ktsnvt.model.entity.Questionnaire;
import rs.ac.uns.ftn.ktsnvt.repository.QuestionRepository;
import rs.ac.uns.ftn.ktsnvt.repository.QuestionnaireRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Ivana Zeljkovic on 19/11/2017.
 */
@Service
public class QuestionServiceImpl implements QuestionService {

    @Autowired
    private QuestionnaireRepository questionnaireRepository;

    @Autowired
    private QuestionRepository questionRepository;


    @Override
    @Transactional(readOnly = true)
    public void checkQuestions(Map<Long, String> questionsIdAnswer, Long questionnaireId) {
        Questionnaire questionnaire = this.questionnaireRepository.findOne(questionnaireId);

        Map<Long, List<String>> dbQuestionsIdAnswers = new HashMap<>();
        questionnaire.getQuestions().forEach(question -> dbQuestionsIdAnswers.put(question.getId(), question.getAnswers()));

        for(Long id : questionsIdAnswer.keySet()) {
            // proverava se za svaki dati odgovor na odredjeno pitanje, da li to pitanje i odgovor postoje u odgovarajucoj anketi
            if(!dbQuestionsIdAnswers.containsKey(id) || !dbQuestionsIdAnswers.get(id).contains(questionsIdAnswer.get(id)))
                throw new BadRequestException("There is no some question/some answer on question in selected questionnaire!");
        }
        for(Long questionId : dbQuestionsIdAnswers.keySet()) {
            // proverava se za sva pitanja koja se nalaze u anketi, da li postoji i odgovor na to pitanje u objektu koji predstavlja glasanje stanara
            if(!questionsIdAnswer.containsKey(questionId))
                throw new BadRequestException("You didn't answer on all questions!");
        }
    }

    @Override
    @Transactional(readOnly = false)
    public void removeQuestions(Long questionnaireId) {
        this.questionRepository.removeAllByQuestionnaireId(questionnaireId);
    }

    @Override
    @Transactional(readOnly = false)
    public void remove(Long questionId) {
        this.questionRepository.delete(questionId);
    }

    @Override
    @Transactional(readOnly = true)
    public Question findOne(Long questionId) {
        Question question = this.questionRepository.findOne(questionId);
        if(question == null) throw new NotFoundException("Question not found!");
        return question;
    }

    @Override
    @Transactional(readOnly = false)
    public void removeAnswers(Long questionId) {
        this.questionRepository.removeAllAnswersByQuestionId(questionId);
    }
}
