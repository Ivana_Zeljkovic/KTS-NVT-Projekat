package rs.ac.uns.ftn.ktsnvt.repository.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.model.entity.Business;
import rs.ac.uns.ftn.ktsnvt.model.enumeration.BusinessType;
import rs.ac.uns.ftn.ktsnvt.repository.BusinessRepository;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Katarina Cukurov on 28/11/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class BusinessRepositoryIntegrationTest {

    @Autowired
    private BusinessRepository businessRepository;

    private Pageable page = new PageRequest(0, 10, Sort.Direction.ASC, "id");


    @Test
    @Transactional
    public void testFindByAddressId(){
        /*
         * Test proverava ispravnost rada metode findByAddressId repozitorijuma BusinessRepository
         * (pronalazenje svih kompanija koji su u bazi na osnovu id-a adrese)
         * */

        /*
        u bazi postoji firma cija adresa ima ID = 103
            id = 101,
            name = 'Second firm',
            description = 'Description for second firm',
            phone_number = 987654321,
            email = ivana.zeljkovic995@gmail.com,
            business_type = 5 (FIRM_FOR_ELECTRICAL_INSTALLATIONS),
            is_company = true,
            address_id = 103
        */
        Business business = this.businessRepository.findByAddressId(Long.valueOf(103), true);

        assertNotNull(business);
        assertEquals("Description for second firm", business.getDescription());
        assertEquals("Second firm", business.getName());
        assertEquals("ivana.zeljkovic995@gmail.com", business.getEmail());
    }

    @Test
    @Transactional
    public void testFindAll(){
        /*
         * Test proverava ispravnost rada metode findAll repozitorijuma BusinessRepository
         * (pronalazenje svih firmi/institucija koje se nalaze u bazi)
         * */

        List<Business> business = this.businessRepository.findAll();

        assertNotNull(business);
        assertEquals(7, business.size());
    }

    @Test
    @Transactional
    public void testFindOne(){
        /*
         * Test proverava ispravnost rada metode findOne repozitorijuma BusinessRepository
         * (pronalazenje firme/institucije koje su u bazi na osnovu id-a)
         * */

        /*
        u bazi postoji firma:
            id = 101,
            name = 'Second firm',
            description = 'Description for second firm',
            phone_number = 987654321,
            email = ivana.zeljkovic995@gmail.com,
            business_type = 5 (FIRM_FOR_ELECTRICAL_INSTALLATIONS),
            is_company = true,
            address_id = 103
        */
        Business business = this.businessRepository.findOne(Long.valueOf(101));

        assertNotNull(business);
        assertEquals("Description for second firm", business.getDescription());
        assertEquals("Second firm", business.getName());
        assertEquals("ivana.zeljkovic995@gmail.com", business.getEmail());
    }

    @Test
    @Transactional
    public void testFindOneInactive(){
        /*
         * Test proverava ispravnost rada metode findOneInactive repozitorijuma BusinessRepository
         * (pronalazenje firme/institucije koje su u bazi na osnovu id-a koja je logcki obrisana)
         * */

        /*
        u bazi postoji firma:
            id = 103,
            name = 'Third firm',
            description = 'Description for third firm',
            phone_number = 987654321,
            email = ivana.zeljkovic995@gmail.com,
            business_type = 5 (FIRM_FOR_ELECTRICAL_INSTALLATIONS),
            is_company = true,
            address_id = 103
        */
        Business business = this.businessRepository.findOneInactive(Long.valueOf(105));

        assertNotNull(business);
        assertEquals("Description for third firm", business.getDescription());
        assertEquals("Third firm", business.getName());
        assertEquals("ivana.zeljkovic995@gmail.com", business.getEmail());
    }

    @Test
    @Transactional
    public void testFindAllExceptCurrent(){
        /*
         * Test proverava ispravnost rada metode findAllExceptCurrent repozitorijuma BusinessRepository
         * (pronalazenje svih firmi/institucija koje se nalaze u bazi osim trenutne)
         * */

        Page<Business> business = this.businessRepository.findAllExceptCurrent(100l, this.page);

        assertEquals(4, business.getContent().size());
    }

    @Test
    @Transactional
    public void testFindAllInstitutionsOrFirms(){
        /*
         * Test proverava ispravnost rada metode findAllInstitutionsOrFirms repozitorijuma BusinessRepository
         * (pronalazenje svih firmi ili institucija koje se nalaze u bazi, a potrvdjene su)
         * */

        Page<Business> business = this.businessRepository.findAllInstitutionsOrFirms(true, this.page);

        assertEquals(2, business.getContent().size());
    }

    @Test
    @Transactional
    public void testFindAllInstitutionsOrFirmsExceptCurrent(){
        /*
         * Test proverava ispravnost rada metode findAllInstitutionsOrFirmsExceptCurrent repozitorijuma BusinessRepository
         * (pronalazenje svih firmi i institucija koje se nalaze u bazi, a potrvdjene su)
         * */

        Page<Business> business = this.businessRepository.findAllInstitutionsOrFirmsExceptCurrent(true, 10l, this.page);

        assertEquals(2, business.getContent().size());
    }

    @Test
    @Transactional
    public void testFindAllInstitutionsOrFirmsByAdmin(){
        /*
         * Test proverava ispravnost rada metode findAllInstitutionsOrFirmsByAdmin repozitorijuma BusinessRepository
         * (pronalazenje svih firmi i institucija koje se nalaze u bazi, od strane admina)
         * */

        Page<Business> business = this.businessRepository.findAllInstitutionsOrFirmsByAdmin(true, this.page);

        assertEquals(3, business.getContent().size());
    }

    @Test
    @Transactional
    public void testFindAllWhoDidntGetOffer(){
        /*
         * Test proverava ispravnost rada metode findAllWhoDidntGetOffer repozitorijuma BusinessRepository
         * (pronalazenje svih firmi i institucija koje se nalaze u bazi a nisu dobile ponudu)
         * */

        Page<Business> business = this.businessRepository.findAllInstitutionsOrFirmsByAdmin(true, this.page);

        assertEquals(3, business.getContent().size());
    }
}
