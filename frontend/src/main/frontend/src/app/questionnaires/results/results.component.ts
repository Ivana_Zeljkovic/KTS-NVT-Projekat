import { Component, Input } from '@angular/core';
import { BsModalRef } from "ngx-bootstrap";
// model
import { QuestionnaireResults } from "../../shared/models/questionnaire-results";


@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent {
  @Input() questionnaireResults: QuestionnaireResults;

  constructor(private modalRef: BsModalRef) { }
}
