import {LoginPage} from "../../page-objects/login.po";
import {browser, by, element} from "protractor";
import {DamageListPage} from "../../page-objects/damage-list.po";
import {DamageDetailsPage} from "../page-objects/damage-details.po";
import {CommentModalPage} from "./page-objects/comment-modal.po";

describe('Testing comments functionality', () => {
  let loginPage: LoginPage;
  let damageList: DamageListPage;
  let damageDetail: DamageDetailsPage;
  let commentModal: CommentModalPage;

  beforeAll(() => {
    loginPage = new LoginPage();
    damageList = new DamageListPage();
    commentModal = new CommentModalPage();
    damageDetail = new DamageDetailsPage();
    browser.get('/');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');

    loginPage.setUsername('kaca');
    loginPage.setPassword('kaca');
    loginPage.getSignInButton().click();

    browser.wait(function () {
      return browser.getCurrentUrl().then(value => {
        return value == 'http://localhost:49152/#/home/notifications'
      });
    }).then(function () {
      browser.get('http://localhost:49152/#/damages/101');
      browser.wait(function () {
        return browser.getCurrentUrl().then(value => {
          return value == 'http://localhost:49152/#/damages/101'
        });
      });
    });

  });

  afterAll(() => {
    browser.executeScript('window.localStorage.clear();');
  });


  it('Add new comment and delete it', () => {
    damageDetail.getAddButton().click();
    browser.wait(function () {
      return commentModal.getComponent().isDisplayed();
    }).then(function () {
      commentModal.setComment('this is a comment from e2e test');
      commentModal.getSumbitCommentButton().click().then(function () {
        browser.wait(function () {
          return commentModal.getComponent().isPresent().then(function (value) {
            return !value;
          });
        });
      });
    });

    expect(commentModal.getComments().count()).toBe(2).then(function () {
      commentModal.getFirstPopElement().click().then(function () {
        commentModal.getRemoveFirstComment().click().then(function () {
            expect(commentModal.getComments().count()).toBe(1)
          }
        );
      })
    });
  });

  it('Display comment is required when there is no input in comment but comment text area got and lost focus', () => {
      damageDetail.getAddButton().click();
      browser.wait(function () {
        return commentModal.getComponent().isDisplayed();
      }).then(function () {
         commentModal.getTextArea().click();
         commentModal.getCommentHeader().click();
         expect(commentModal.getCommentRequired().isDisplayed()).toBe(true);
      });
  });

  it('Should display message about comments minimum length', () =>{
    commentModal.setComment('as');
    expect(commentModal.getCommentMinimumLength().isDisplayed()).toBe(true);
  });


});
