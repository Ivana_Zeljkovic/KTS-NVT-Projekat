import {by, element, ElementFinder} from "protractor";

export class TenantRegistrationPage {
  // get methods
  getTenantRegistrationElement(): ElementFinder {
    return element(by.tagName('app-tenant-registration'));
  }

  getPageTitle(): ElementFinder {
    return element(by.css('.panel-title'));
  }

  getFirstNameInput(): ElementFinder {
    return element(by.id('firstName'));
  }

  getLastNameInput(): ElementFinder {
    return element(by.id('lastName'));
  }

  getUsernameInput(): ElementFinder {
    return element(by.id('username'));
  }

  getPasswordInput(): ElementFinder {
    return element(by.id('password'));
  }

  getConfirmPasswordInput(): ElementFinder {
    return element(by.id('confirmPassword'));
  }

  getEmailInput(): ElementFinder {
    return element(by.id('email'));
  }

  getPhoneNumberInput(): ElementFinder {
    return element(by.id('phoneNumber'));
  }

  getBirthDateInput(): ElementFinder {
    return element(by.id('birthDate'));
  }

  getSingUpButton(): ElementFinder {
    return element(by.buttonText('Register'));
  }

  getLoginLink(): ElementFinder {
    return element(by.tagName('a'));
  }

  // set methods
  setFirstName(text: string) {
    this.getFirstNameInput().sendKeys(text);
  }

  setLastName(text: string) {
    this.getLastNameInput().sendKeys(text);
  }

  setUsername(text: string) {
    this.getUsernameInput().sendKeys(text);
  }

  setPassword(text: string) {
    this.getPasswordInput().sendKeys(text);
  }

  setConfirmPassword(text: string) {
    this.getConfirmPasswordInput().sendKeys(text);
  }

  setEmail(text: string) {
    this.getEmailInput().sendKeys(text);
  }

  setPhoneNumber(text: string) {
    this.getPhoneNumberInput().sendKeys(text);
  }

  setBirthDate(text: string) {
    this.getBirthDateInput().sendKeys(text);
  }

  // get errors method
  getFirstNameRequiredError(): ElementFinder {
    return element(by.id('firstNameRequiredError'))
  }

  getFirstNameMinimumLengthError(): ElementFinder {
    return element(by.id('firstNameMinimumLengthError'));
  }

  getLastNameRequiredError(): ElementFinder {
    return element(by.id('lastNameRequiredError'));
  }

  getLastNameMinimumLengthError(): ElementFinder {
    return element(by.id('lastNameMinimumLengthError'));
  }

  getUsernameAvailable(): ElementFinder {
    return element(by.id('usernameAvailable'));
  }

  getUsernameRequiredError(): ElementFinder {
    return element(by.id('usernameRequiredError'));
  }

  getUsernameContainSpaceError(): ElementFinder {
    return element(by.id('usernameContainSpaceError'));
  }

  getUsernameTakenError(): ElementFinder {
    return element(by.id('usernameTakenError'));
  }

  getPasswordRequiredError(): ElementFinder {
    return element(by.id('passwordRequiredError'));
  }

  getPasswordMinimumLengthError(): ElementFinder {
    return element(by.id('passwordMinimumLengthError'));
  }

  getConfirmPasswordRequiredError(): ElementFinder {
    return element(by.id('confirmPasswordRequiredError'));
  }

  getPasswordDontMatchError(): ElementFinder {
    return element(by.id('passwordDontMatchError'));
  }

  getEmailRequiredError(): ElementFinder {
    return element(by.id('emailRequiredError'));
  }

  getEmailValidError(): ElementFinder {
    return element(by.id('emailValidError'));
  }

  getPhoneNumberRequired(): ElementFinder {
    return element(by.id('phoneNumberRequired'));
  }

  getPhoneNumberContainSpaceError(): ElementFinder {
    return element(by.id('phoneNumberContainSpaceError'));
  }

  getPhoneNumberPatternError(): ElementFinder {
    return element(by.id('phoneNumberPatternError'));
  }

  getBirthDateRequiredError(): ElementFinder {
    return element(by.id('birthDateRequiredError'));
  }
}
