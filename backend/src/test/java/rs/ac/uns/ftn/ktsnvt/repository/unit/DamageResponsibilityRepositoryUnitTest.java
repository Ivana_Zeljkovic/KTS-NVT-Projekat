package rs.ac.uns.ftn.ktsnvt.repository.unit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.model.entity.*;
import rs.ac.uns.ftn.ktsnvt.model.enumeration.BusinessType;
import rs.ac.uns.ftn.ktsnvt.repository.BuildingRepository;
import rs.ac.uns.ftn.ktsnvt.repository.BusinessRepository;
import rs.ac.uns.ftn.ktsnvt.repository.DamageResponsibilityRepository;

import java.util.Date;

import static org.junit.Assert.*;

/**
 * Created by Ivana Zeljkovic on 28/11/2017.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class DamageResponsibilityRepositoryUnitTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private DamageResponsibilityRepository damageResponsibilityRepository;

    @Autowired
    private BuildingRepository buildingRepository;

    @Autowired
    private BusinessRepository businessRepository;

    private Long building_id;
    private Long institution_id;
    private Long tenant_id;
    private Long damage_type_id;
    private Long damage_type_id_2;


    @Before
    public void setUp() {
        // AUTHORITIES
        Authority authority_tenant = new Authority("TENANT");
        Authority authority_institution = new Authority("INSTITUTION");
        entityManager.persist(authority_tenant);
        entityManager.persist(authority_institution);

        // ACCOUNTS, TENANT, INSTITUTION
        Account account_1 = new Account("kaca", "kaca");
        Tenant tenant = new Tenant("Katarina", "Cukurov", new Date(),
                "kaca@gmail.com", "123456789", true, account_1);
        Account account_2 = new Account("inst1", "inst1");
        Business institution = new Business("First institution", "Description for first institution",
                "987654321", "inst@gmail.com", BusinessType.valueOf("POLICE"), false);
        institution.getAccounts().add(account_2);
        entityManager.persist(account_1);
        this.tenant_id = (Long)entityManager.persistAndGetId(tenant);
        entityManager.persist(account_2);
        this.institution_id = (Long)entityManager.persistAndGetId(institution);

        // ACCOUNT AUTHORITIES
        AccountAuthority account_1_tenant = new AccountAuthority(account_1, authority_tenant);
        AccountAuthority account_2_institution = new AccountAuthority(account_2, authority_institution);
        entityManager.persist(account_1_tenant);
        entityManager.persist(account_2_institution);

        // azuriranje perzistentnih objekata klase Account, tako da su ovi objekti povezani sa odgovarajucim
        // objektima klase AccountAuthority
        account_1.getAccountAuthorities().add(account_1_tenant);
        account_2.getAccountAuthorities().add(account_2_institution);
        entityManager.merge(account_1);
        entityManager.merge(account_2);



        // BUILDING (WITH ADDRESS)
        City city = new City("Novi Sad", 21000);
        Address address = new Address("Janka Veselinovica", 8, city);
        Building building = new Building(5, true, address);
        entityManager.persist(city);
        entityManager.persist(address);
        this.building_id = (Long)entityManager.persistAndGetId(building);

        // ADDRESS (FOR INSTITUTION)
        Address address_2 = new Address("Janka Veselinovica", 10, city);
        entityManager.persist(address_2);
        institution.setAddress(address_2);
        entityManager.merge(institution);



        // DAMAGE TYPE
        DamageType damage_type = new DamageType("PLUMBING_DAMAGE");
        DamageType damage_type_2 = new DamageType("ELECTRICAL_DAMAGE");
        this.damage_type_id = (Long)entityManager.persistAndGetId(damage_type);
        this.damage_type_id_2 = (Long)entityManager.persistAndGetId(damage_type_2);

        // DAMAGE RESPONSIBILITY
        DamageResponsibility damage_responsibility = new DamageResponsibility(tenant, institution, damage_type, building);
        entityManager.persist(damage_responsibility);
        tenant.getResponsibleForDamageTypes().add(damage_responsibility);
        institution.getResponsibleForDamageTypes().add(damage_responsibility);
        entityManager.merge(tenant);
        entityManager.merge(institution);
    }


    @Test
    public void testFindOneByDamageTypeAndTenantAndBuilding() {
        /*
         * Test proverava ispravnost rada metode findOneByDamageTypeAndTenantAndBuilding repozitorijuma DamageResponsibilityRepository
         * (pronalazenje odgovornosti za dati tip kvara, datog stanara i datu zgradu)
         * u slucaju kada postoji odgovornost za zadati tip kvara, stanara i zgradu
         * */

        Building building = this.buildingRepository.findOne(this.building_id);

        // stanar sa ID-em = tenant_id je odgovoran za tip kvara sa ID-em = damage_type_id u zgradi sa ID-em = building_id
        DamageResponsibility damageResponsibility = this.damageResponsibilityRepository.findOneByDamageTypeAndTenantAndBuilding(
                this.damage_type_id, this.tenant_id, this.building_id);

        assertNotNull(damageResponsibility);
        assertEquals("Katarina", damageResponsibility.getResponsibleTenant().getFirstName());
        assertEquals("Cukurov", damageResponsibility.getResponsibleTenant().getLastName());
        assertEquals("PLUMBING_DAMAGE", damageResponsibility.getDamageType().getName());
        assertEquals(building.getAddress(), damageResponsibility.getBuilding().getAddress());
    }

    @Test
    @Transactional
    public void testFindOneByDamageTypeAndTenantAndBuildingNonexistent() {
        /*
         * Test proverava ispravnost rada metode findOneByDamageTypeAndTenantAndBuilding repozitorijuma DamageResponsibilityRepository
         * (pronalazenje odgovornosti za dati tip kvara, datog stanara i datu zgradu)
         * u slucaju kada ne postoji odgovornost za zadati tip kvara, stanara i zgradu
         * */

        // stanar sa ID-em = tenant_id nije odgovoran za tip kvara sa ID-em = damage_type_id_2 u zgradi sa ID-em = building_id
        DamageResponsibility damageResponsibility = this.damageResponsibilityRepository.findOneByDamageTypeAndTenantAndBuilding(
                this.damage_type_id_2, this.tenant_id, this.building_id);

        assertNull(damageResponsibility);
    }

    @Test
    @Transactional
    public void testFindOneByDamageTypeAndBuilding() {
        /*
         * Test proverava ispravnost rada metode findOneByDamageTypeAndBuilding repozitorijuma DamageResponsibilityRepository
         * (pronalazenje odgovornosti za dati tip kvara i datu zgradu)
         * u slucaju kada postoji odgovornost za zadati tip kvara i zgradu
         * */

        Building building = this.buildingRepository.findOne(this.building_id);

        // za tip kvara sa ID-em = damage_type_id u zgradi sa ID-em = building_id postoji osgovorna osoba/institucija
        DamageResponsibility damageResponsibility = this.damageResponsibilityRepository.findOneByDamageTypeAndBuilding(
                this.damage_type_id, this.building_id);

        assertNotNull(damageResponsibility);
        assertEquals("PLUMBING_DAMAGE", damageResponsibility.getDamageType().getName());
        assertEquals(building.getAddress(), damageResponsibility.getBuilding().getAddress());
    }

    @Test
    @Transactional
    public void testFindOneByDamageTypeAndBuildingNonexistent() {
        /*
         * Test proverava ispravnost rada metode findOneByDamageTypeAndBuilding repozitorijuma DamageResponsibilityRepository
         * (pronalazenje odgovornosti za dati tip kvara i datu zgradu)
         * u slucaju kada ne postoji odgovornost za zadati tip kvara i zgradu
         * */

        // ne postoji odgovorna osoba/institucija za tip kvara sa ID-em = damage_type_id_2 u zgradi sa ID-em = building_id
        DamageResponsibility damageResponsibility = this.damageResponsibilityRepository.findOneByDamageTypeAndBuilding(
                this.damage_type_id_2, this.building_id);

        assertNull(damageResponsibility);
    }

    @Test
    public void testFindOneByDamageTypeAndInstitutionAndBuilding() {
        /*
         * Test proverava ispravnost rada metode findOneByDamageTypeAndInstitutionAndBuilding repozitorijuma DamageResponsibilityRepository
         * (pronalazenje odgovornosti za dati tip kvara, datu instituciju i datu zgradu)
         * u slucaju kada postoji odgovornost za zadati tip kvara, instituciju i zgradu
         * */

        Building building = this.buildingRepository.findOne(this.building_id);
        Business institution = this.businessRepository.findOne(this.institution_id);

        // institucija sa ID-em = institution_id je odgovorna za tip kvara sa ID-em = damage_type_id u zgradi sa ID-em = building_id
        DamageResponsibility damageResponsibility = this.damageResponsibilityRepository.findOneByDamageTypeAndInstitutionAndBuilding(
                this.damage_type_id, this.institution_id, this.building_id);

        assertNotNull(damageResponsibility);
        assertEquals("PLUMBING_DAMAGE", damageResponsibility.getDamageType().getName());
        assertEquals(building.getAddress(), damageResponsibility.getBuilding().getAddress());
        assertFalse(damageResponsibility.getResponsibleInstitution().isIsCompany());
        assertEquals(institution.getName(), damageResponsibility.getResponsibleInstitution().getName());
        assertEquals(institution.getAddress(), damageResponsibility.getResponsibleInstitution().getAddress());
    }

    @Test
    public void testFindOneByDamageTypeAndInstitutionAndBuildingNonexistent() {
        /*
         * Test proverava ispravnost rada metode findOneByDamageTypeAndInstitutionAndBuilding repozitorijuma DamageResponsibilityRepository
         * (pronalazenje odgovornosti za dati tip kvara, datu instituciju i datu zgradu)
         * u slucaju kada ne postoji odgovornost za zadati tip kvara, instituciju i zgradu
         * */

        // institucija sa ID-em = institution_id nije odgovorna za tip kvara sa ID-em = damage_type_id_2 u zgradi sa ID-em = building_id
        DamageResponsibility damageResponsibility = this.damageResponsibilityRepository.findOneByDamageTypeAndInstitutionAndBuilding(
                this.damage_type_id_2, this.institution_id, this.building_id);

        assertNull(damageResponsibility);
    }
}
