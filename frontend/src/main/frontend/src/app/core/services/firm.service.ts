import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
// service
import { FirmInstitutionService } from "./firm-institution.service";


@Injectable()
export class FirmService extends FirmInstitutionService{

  constructor(http: HttpClient) {
    super('firms', http);
  }
}
