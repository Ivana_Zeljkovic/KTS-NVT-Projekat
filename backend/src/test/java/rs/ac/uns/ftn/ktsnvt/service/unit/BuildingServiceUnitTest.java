package rs.ac.uns.ftn.ktsnvt.service.unit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.omg.CosNaming.NamingContextPackage.NotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Address;
import rs.ac.uns.ftn.ktsnvt.model.entity.Building;
import rs.ac.uns.ftn.ktsnvt.model.entity.City;
import rs.ac.uns.ftn.ktsnvt.repository.BuildingRepository;
import rs.ac.uns.ftn.ktsnvt.service.BuildingService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.BDDMockito.given;

/**
 * Created by Nikola Garabandic on 30/11/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BuildingServiceUnitTest {

    @Autowired
    BuildingService buildingService;

    @MockBean
    BuildingRepository buildingRepository;


    @Before
    public void setUp(){
        City city = new City("Novi Sad", 21000);
        Address address = new Address("Sentandrejski put", 75, city);
        Building building = new Building(10, true, address);

        given(
                this.buildingRepository.findOne(100L)
        ).willReturn(
                building
        );

        given(
                this.buildingRepository.findByAddressId(200L)
        ).willReturn(
                building
        );

        given(
                this.buildingRepository.findOne(101L)
        ).willReturn(
                null
        );

        given(
                this.buildingRepository.findByAddressId(100L)
        ).willReturn(
                null
        );
    }

    @Test
    public void testFindOne(){
        /*
         * Test proverava ispravnost rada metode findOne servisa BuildingService
         * (dobavljanje building objekta na osnovu ID-a)
         * u slucaju kada postoji zgrada sa zadatim id-em
         * */

        /*
        podaci o zgradi:
            numberOfFloos: 10,
            hasParking: true,
            Adress: { street = Novi Sad, number = 21000, City: Name = Novi Sad postalNumber = 21000}
        */
        Building building = this.buildingService.findOne(100L);

        assertNotNull(building);
        assertEquals("Sentandrejski put", building.getAddress().getStreet());
        assertEquals(75, building.getAddress().getNumber());
        assertEquals("Novi Sad", building.getAddress().getCity().getName());
        assertEquals(21000, building.getAddress().getCity().getPostalNumber());
    }

    @Test(expected = NotFoundException.class)
    public void testFindNone(){
        /*
         * Test proverava ispravnost rada metode findOne servisa BuildingService
         * (dobavljanje building objekta na osnovu ID-a)
         * u slucaju kada postoji zgrada sa zadatim id-em - ocekivano NotFoundException
         * */

        // Zgrada sa ID-em 101 ne postoji
        Building building = this.buildingService.findOne(101L);
    }

    @Test
    public void testFindByAddressId(){
        /*
         * Test proverava ispravnost rada metode FindByAddressId servisa BuildingService
         * (dobavljanje building objekta na osnovu address ID-a)
         * u slucaju kada postoji address sa zadatim id-em
         * */

        /*
        adresa sa Id-em 200 ima podatke:
            street = Novi Sad,
            number = 21000,
            City: Name = Novi Sad postalNumber = 21000
        */
        Building building = this.buildingService.findByAddressId(200L);

        assertNotNull(building);
        assertEquals("Sentandrejski put", building.getAddress().getStreet());
        assertEquals(75, building.getAddress().getNumber());
        assertEquals("Novi Sad", building.getAddress().getCity().getName());
        assertEquals(21000, building.getAddress().getCity().getPostalNumber());
    }

    @Test(expected = NotFoundException.class)
    public void testFindNoneByAddressId(){
        /*
         * Test proverava ispravnost rada metode FindByAddressId servisa BuildingService
         * (dobavljanje building objekta na osnovu address ID-a)
         * u slucaju kada ne postoji address sa zadatim id-em - ocekivano NotFoundException
         * */

        //adresa sa Id-em 100 ne postoji
        Building building = this.buildingService.findByAddressId(100L);
    }
}
