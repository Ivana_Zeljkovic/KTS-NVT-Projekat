package rs.ac.uns.ftn.ktsnvt.service;

import rs.ac.uns.ftn.ktsnvt.model.entity.Authority;

/**
 * Created by Nikola Garabandic on 06/11/2017.
 */
public interface AuthorityService {

    Authority findByName(String name);
}
