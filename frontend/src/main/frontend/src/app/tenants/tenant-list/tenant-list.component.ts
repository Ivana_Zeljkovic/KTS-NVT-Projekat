import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { ToasterConfig, ToasterService } from "angular5-toaster/dist";
import { Router } from "@angular/router";
// model
import { Tenant } from "../../shared/models/tenant";
//service
import { TenantService } from "../../core/services/tenant.service";
// error
import { AppError } from "../../shared/errors/app-error";
import { BadRequestError } from "../../shared/errors/bad-request-error";
import { NotFoundError } from "../../shared/errors/not-found-error";
import { ForbiddenError } from "../../shared/errors/forbidden-error";
// component
import { ConfirmDeleteModalComponent } from "../../shared/components/confirm-delete-modal/confirm-delete-modal.component";
import {PageAndSort} from "../../shared/models/page-and-sort";


@Component({
  selector: 'app-tenant-list',
  templateUrl: './tenant-list.component.html',
  styleUrls: ['./tenant-list.component.css']
})
export class TenantListComponent implements OnInit {
  tenantList: Array<Tenant>;
  toasterConfig: ToasterConfig;
  modalRef: BsModalRef;
  sortList: string[];
  selectedSort: string;
  currentPage: number;
  maxSize: number;
  itemsPerPage: number;
  pageSize: string;
  sortProperties: string[];
  sortDirection: string;
  totalItems: number;

  constructor(private tenantService: TenantService, private modalService: BsModalService,
              private toasterService: ToasterService, private router: Router) {
    this.toasterConfig = new ToasterConfig({
      timeout: {success: 2000, error: 4000, warning: 4000}
    });
    this.tenantList = new Array<Tenant>();
    this.sortList = ['First name', 'Last name', 'Email','Phone number'];
    this.selectedSort = 'First name';

    // Set up for bootstrap paging
    this.currentPage = 1;
    this.maxSize = 3;
    this.itemsPerPage = 10;

    // Set up for getting notification request
    this.pageSize = '10';
    this.sortDirection = 'ASC';
    this.sortProperties = ['firstName'];
  }

  ngOnInit() {
    this.getTenats();
  }

  getTenats(){
    let page = new PageAndSort((this.currentPage - 1).toString(), this.pageSize, this.sortDirection, this.sortProperties);
    this.tenantService.getPage(page).subscribe(data => {
      this.tenantList = data;
      if (data.length > 0)
        this.totalItems = data[0].numberOfElements;
      else this.totalItems = 0;
      for(let tenant of this.tenantList){
        tenant.birthDate = new Date(tenant.birthDate);
      }
    }, (error: AppError) => {
      if (error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', '"Accessing the resource you were trying to reach is forbidden');
      else {
        this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
        throw error;
      }
    });
  }

  deleteTenant(tenant: Tenant){
    this.modalRef = this.modalService.show(
      ConfirmDeleteModalComponent,
      Object.assign({},{ class: 'modal-sm' })
    );

    this.modalRef.content.confirmed.subscribe((value) => {
      if(value) {
        let index = this.tenantList.indexOf(tenant, 0);
        this.tenantList.splice(index, 1);

        this.tenantService.remove(tenant.id).subscribe(() => {
            this.toasterService.pop('success', 'Congratulation', 'Successfully deleted tenant!');
            this.getTenats();
          },
          (error: AppError) => {
            this.tenantList.splice(index, 0, tenant);

            if(error instanceof NotFoundError)
              this.toasterService.pop('error', 'Error', 'Tenant not found!');
            else if(error instanceof ForbiddenError)
              this.toasterService.pop('error', 'Error', 'This tenant can\'t be deleted!');
            else if(error instanceof BadRequestError)
              this.toasterService.pop('error', 'Error', 'This tenant is responsible for fixing damage and' +
                ' can\'t be deleted!');
            else {
              this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
              throw error;
            }
          });
      }
    });
  }

  confirmTenant(tenant: Tenant){
    let index = this.tenantList.indexOf(tenant, 0);
    tenant.confirmed = true;
    this.tenantList.splice(index, 1, tenant);
    this.tenantService.confirmRegistration(tenant.id).subscribe(data => {
      this.toasterService.pop('success', '', 'Tenant confirmed!');
    }, (error: AppError) => {
      tenant.confirmed = false;
      this.tenantList.splice(index, 1, tenant);
      if (error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Wrong ID of tenant!');
      else {
        this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
        throw error;
      }
    });
  }

  moreDetails(tenant: Tenant){
    this.router.navigate([`/tenants/${tenant.id}`]);
  }

  pageChanged(event: any) {
    this.currentPage = event.page;
    this.getTenats();
  }

  setSort(sort: string) {
    this.selectedSort = sort;
  }

  callSort(){
    if(this.selectedSort == 'First name')
      this.sortProperties = ['firstName'];
    else if(this.selectedSort == 'Last name')
      this.sortProperties = ['lastName'];
    else if(this.selectedSort == 'Phone number')
      this.sortProperties = ['phoneNumber'];
    else if(this.selectedSort == 'Email')
      this.sortProperties = ['email'];
    this.getTenats();
  }

}
