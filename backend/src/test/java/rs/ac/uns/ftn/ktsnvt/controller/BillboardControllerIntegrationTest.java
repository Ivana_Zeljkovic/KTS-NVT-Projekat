package rs.ac.uns.ftn.ktsnvt.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;
import rs.ac.uns.ftn.ktsnvt.controller.util.TestUtil;
import rs.ac.uns.ftn.ktsnvt.web.dto.*;

import javax.annotation.PostConstruct;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;


/**
 * Created by Ivana Zeljkovic on 06/12/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class BillboardControllerIntegrationTest {

    private static final String URL_PREFIX = "/api/buildings";

    private MediaType contentType = new MediaType(
            MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    private MockMvc mockMvc;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja stanara
    private String accessTokenTenant;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja stanara
    private String accessTokentTenant_2;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja stanara
    private String accessTokentTenant_3;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja stanara koji nije povezan ni sa jednim stanom
    private String accessTokenTenantWithoutApartment;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja firme
    private String accessTokenFirm;
    private MultiValueMap<String, String> params;


    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private TestRestTemplate testRestTemplate;


    @PostConstruct
    public void setup() {
        this.mockMvc = MockMvcBuilders.
                webAppContextSetup(webApplicationContext).apply(springSecurity()).build();
    }

    // pre izvrsavanja testa, neophodno je izvrsiti logovanje u aplikaciju da bismo dobili token
    @Before
    public void login() throws Exception {
        // logovanje stanara
        LoginDTO loginDTOTenant = new LoginDTO("kaca", "kaca");

        ResponseEntity<TokenDTO> responseEntityTenant =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOTenant,
                        TokenDTO.class);
        // preuzmemo token za logovanog stanara iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenTenant = responseEntityTenant.getBody().getValue();


        // logovanje stanara
        LoginDTO loginDTOTenant2 = new LoginDTO("jasmina", "jasmina");

        ResponseEntity<TokenDTO> responseEntityTenant2 =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOTenant2,
                        TokenDTO.class);
        // preuzmemo token za logovanog stanara iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokentTenant_2 = responseEntityTenant2.getBody().getValue();


        // logovanje stanara
        LoginDTO loginDTOTenant3 = new LoginDTO("nikola", "nikola");

        ResponseEntity<TokenDTO> responseEntityTenant3 =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOTenant3,
                        TokenDTO.class);
        // preuzmemo token za logovanog stanara iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokentTenant_3 = responseEntityTenant3.getBody().getValue();


        // logovanje stanara koji nije povezan ni sa jednim stanom
        LoginDTO loginDTOTenant_2 = new LoginDTO("jovanka", "jovanka");

        ResponseEntity<TokenDTO> responseEntityTenant_2 =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOTenant_2,
                        TokenDTO.class);
        // preuzmemo token za logovanog stanara iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenTenantWithoutApartment = responseEntityTenant_2.getBody().getValue();


        // logovanje firme
        LoginDTO loginDTOFirm = new LoginDTO("firm1", "firm1");

        ResponseEntity<TokenDTO> responseEntityFirm =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOFirm,
                        TokenDTO.class);
        // preuzmemo token za logovanu firmu iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenFirm = responseEntityFirm.getBody().getValue();

        // za pageable atribut
        params = TestUtil.getParams(
                "0", "10", "ASC", "id", "");
    }

    // TESTOVI za getNotifications
    @Test
    public void testGetNotifications() throws Exception {
        /*
        * Test proverava ispravnost rada metode getNotifications kontrolera BillboardController
        * (dobavljanje liste svih javnih obavestenja koja se nalaze na bilbordu zgrade sa zadatim ID-em)
        * */

        /*
        u zgradi sa ID-em = 100 (na bilbordu sa ID-em = 100) postoje 2 javna obavestenja:
               1 - ID = 100,
                   content = First notification,
                   creator_id = 101
               2 - ID = 101,
                   content = Second notification,
                   creator_id = 101
        */
        this.mockMvc.perform(get(URL_PREFIX + "/100/billboard/notifications")
                .header("Authentication-Token", this.accessTokenTenant)
                .params(params))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[*].id", containsInAnyOrder(100, 101)))
                .andExpect(jsonPath("$.[*].content", containsInAnyOrder(
                        "First notification content", "Second notification content")))
                .andExpect(jsonPath("$.[*].creator.id").value(hasItem(101)));
    }

    @Test
    public void testGetNotificationsByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode getNotifications kontrolera BillboardController
        * (dobavljanje liste svih javnih obavestenja koja se nalaze na bilbordu zgrade sa zadatim ID-em)
        * u slucaju kada akciju obavlja nelogovani korisnik
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/100/billboard/notifications")
                .params(params))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetNotificationsByNotAuthenticatedTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode getNotifications kontrolera BillboardController
        * (dobavljanje liste svih javnih obavestenja koja se nalaze na bilbordu zgrade sa zadatim ID-em)
        * u slucaju kada akciju obavlja logovani korisnik koji nema ulogu stanara
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/100/billboard/notifications")
                .header("Authentication-Token", this.accessTokenFirm)
                .params(params))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetNotificationsNonexistentBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode getNotifications kontrolera BillboardController
        * (dobavljanje liste svih javnih obavestenja koja se nalaze na bilbordu zgrade sa zadatim ID-em)
        * u slucaju kada ne postoji zgrada sa zadatim ID-em
        * */

        // ne postoji zgrada sa ID-em = 200
        this.mockMvc.perform(get(URL_PREFIX + "/200/billboard/notifications")
                .header("Authentication-Token", this.accessTokenTenant)
                .params(params))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetNotificationsByTenantWithoutApartment() throws Exception {
        /*
        * Test proverava ispravnost rada metode getNotifications kontrolera BillboardController
        * (dobavljanje liste svih javnih obavestenja koja se nalaze na bilbordu zgrade sa zadatim ID-em)
        * u slucaju kada trenutno ulogovani stanar nije povezan ni sa jednim stanom
        * */

        /*
        stanar sa ID-em = 108 nije povezan ni sa jednim stanom jos uvek, i ima podatke:
            korisnicko ime = jovanka,
            lozinka = jovanka
            id = 108
        */
        this.mockMvc.perform(get(URL_PREFIX + "/100/billboard/notifications")
                .header("Authentication-Token", this.accessTokenTenantWithoutApartment)
                .params(params))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetNotificationsByTenantFromInvalidBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode getNotifications kontrolera BillboardController
        * (dobavljanje liste svih javnih obavestenja koja se nalaze na bilbordu zgrade sa zadatim ID-em)
        * u slucaju kada trenutno ulogovani korisnik ne zivi u zgradi sa zadatim ID-em
        * */

        /*
        stanar sa ID-em = 101 zivi u zgradi sa ID-em = 100 (a ne sa ID-em = 101) i ima podatke:
            korisnicko ime = kaca,
            lozinka = kaca,
            apartment_id = 101 (building_id = 100)
         */
        this.mockMvc.perform(get(URL_PREFIX + "/101/billboard/notifications")
                .header("Authentication-Token", this.accessTokenTenant)
                .params(params))
                .andExpect(status().isForbidden());
    }

    // TESTOVI za getMeetingItems
    @Test
    public void testGetMeetingItems() throws Exception {
        /*
        * Test proverava ispravnost rada metode getMeetingItems kontrolera BillboardController
        * (dobavljanje liste svih predloga tacaka dnevnog reda koja se nalaze na bilbordu zgrade sa zadatim ID-em,
        * pri cemu se dobavljaju samo one tacke dnevnog reda koje jos uvek nisu obradjene ni na jednom sastanku skupstine stanara)
        * */

        /*
        u zgradi sa ID-em = 100 (na bilbordu sa ID-em = 100) postoji jedan nepokupljen predlog tacke dnevnog reda:
            id = 102,
            creator_id = 102
        */
        this.mockMvc.perform(get(URL_PREFIX + "/100/billboard/meeting_items")
                .header("Authentication-Token", this.accessTokenTenant)
                .params(params))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[*]", hasSize(1)))
                .andExpect(jsonPath("$.[0].id").value(102))
                .andExpect(jsonPath("$.[0].creator.id").value(102));
    }

    @Test
    public void testGetMeetingItemsByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode getMeetingItems kontrolera BillboardController
        * (dobavljanje liste svih predloga tacaka dnevnog reda koja se nalaze na bilbordu zgrade sa zadatim ID-em,
        * pri cemu se dobavljaju samo one tacke dnevnog reda koje jos uvek nisu obradjene ni na jednom sastanku skupstine stanara)
        * u slucaju kada akciju obavlja nelogovani korisnik
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/100/billboard/meeting_items")
                .params(params))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetMeetingItemsByNotAuthenticatedTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode getMeetingItems kontrolera BillboardController
        * (dobavljanje liste svih predloga tacaka dnevnog reda koja se nalaze na bilbordu zgrade sa zadatim ID-em,
        * pri cemu se dobavljaju samo one tacke dnevnog reda koje jos uvek nisu obradjene ni na jednom sastanku skupstine stanara)
        * u slucaju kada akciju obavlja logovani korisnik koji nema ulogu stanara
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/100/billboard/meeting_items")
                .header("Authentication-Token", this.accessTokenFirm)
                .params(params))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetMeetingItemsNonexistentBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode getMeetingItems kontrolera BillboardController
        * (dobavljanje liste svih predloga tacaka dnevnog reda koja se nalaze na bilbordu zgrade sa zadatim ID-em,
        * pri cemu se dobavljaju samo one tacke dnevnog reda koje jos uvek nisu obradjene ni na jednom sastanku skupstine stanara)
        * u slucaju kada ne postoji zgrada sa zadatim ID-em
        * */

        // ne postoji zgrada sa ID-em = 200
        this.mockMvc.perform(get(URL_PREFIX + "/200/billboard/meeting_items")
                .header("Authentication-Token", this.accessTokenTenant)
                .params(params))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetMeetingItemsByTenantWithoutApartment() throws Exception {
        /*
        * Test proverava ispravnost rada metode getMeetingItems kontrolera BillboardController
        * (dobavljanje liste svih predloga tacaka dnevnog reda koja se nalaze na bilbordu zgrade sa zadatim ID-em,
        * pri cemu se dobavljaju samo one tacke dnevnog reda koje jos uvek nisu obradjene ni na jednom sastanku skupstine stanara)
        * u slucaju kada trenutno ulogovani stanar nije povezan ni sa jednim stanom
        * */

        /*
        stanar sa ID-em = 108 nije povezan ni sa jednim stanom jos uvek, i ima podatke:
            korisnicko ime = jovanka,
            lozinka = jovanka
            id = 108
        */
        this.mockMvc.perform(get(URL_PREFIX + "/100/billboard/meeting_items")
                .header("Authentication-Token", this.accessTokenTenantWithoutApartment)
                .params(params))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetMeetingItemsByTenantFromInvalidBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode getMeetingItems kontrolera BillboardController
        * (dobavljanje liste svih predloga tacaka dnevnog reda koja se nalaze na bilbordu zgrade sa zadatim ID-em,
        * pri cemu se dobavljaju samo one tacke dnevnog reda koje jos uvek nisu obradjene ni na jednom sastanku skupstine stanara)
        * u slucaju kada trenutno ulogovani stanar ne zivi u zgradi sa zadatim ID-em
        * */

        /*
        stanar sa ID-em = 101 zivi u zgradi sa ID-em = 100 (a ne sa ID-em = 101) i ima podatke:
            korisnicko ime = kaca,
            lozinka = kaca,
            apartment_id = 101 (building_id = 100)
         */
        this.mockMvc.perform(get(URL_PREFIX + "/101/billboard/meeting_items")
                .header("Authentication-Token", this.accessTokenTenant)
                .params(params))
                .andExpect(status().isForbidden());
    }

    // TESTOVI za searchNotifications
    @Test
    public void testSearchNotifications() throws Exception {
        /*
        * Test proverava ispravnost rada metode searchNotifications kontrolera BillboardController
        * (dobavljanje liste svih javnih obavestenja koja se nalaze na bilbordu zgrade sa zadatim ID-em i pritom su kreirana
        * u periodu od zadatog datuma do zadatog datuma)
        * */

        /*
        u zgradi sa ID-em = 100 (na bilbordu sa ID-em = 100) postoji 1 javno obavestenje kreirano
        u periodu od 2017-11-25 do 2017-11-27 i ima podatke:
               1 - ID = 100,
                   content = First notification,
                   creator_id = 101
        */
        this.mockMvc.perform(get(URL_PREFIX + "/100/billboard/notifications_from_to")
                .header("Authentication-Token", this.accessTokenTenant)
                .params(params)
                .param("from", "2017-11-25 00:00:00")
                .param("to", "2017-11-27 23:59:59"))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[*]", hasSize(1)))
                .andExpect(jsonPath("$.[0].id").value(100))
                .andExpect(jsonPath("$.[0].content").value("First notification content"))
                .andExpect(jsonPath("$.[0].creator.id").value(101));
    }

    @Test
    public void testSearchNotificationsByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode searchNotifications kontrolera BillboardController
        * (dobavljanje liste svih javnih obavestenja koja se nalaze na bilbordu zgrade sa zadatim ID-em i pritom su kreirana
        * u periodu od zadatog datuma do zadatog datuma)
        * u slucaju kada akciju obavlja nelogovani korisnik
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/100/billboard/notifications_from_to")
                .params(params)
                .param("from", "2017-11-25 00:00:00")
                .param("to", "2017-11-27 23:59:59"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSearchNotificationsByNotAuthenticatedTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode searchNotifications kontrolera BillboardController
        * (dobavljanje liste svih javnih obavestenja koja se nalaze na bilbordu zgrade sa zadatim ID-em i pritom su kreirana
        * u periodu od zadatog datuma do zadatog datuma)
        * u slucaju kada akciju obavlja logovani korisnik koji nema ulogu stanara
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/100/billboard/notifications_from_to")
                .header("Authentication-Token", this.accessTokenFirm)
                .params(params)
                .param("from", "2017-11-25 00:00:00")
                .param("to", "2017-11-27 23:59:59"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSearchNotificationsNonexistentBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode searchNotifications kontrolera BillboardController
        * (dobavljanje liste svih javnih obavestenja koja se nalaze na bilbordu zgrade sa zadatim ID-em i pritom su kreirana
        * u periodu od zadatog datuma do zadatog datuma)
        * u slucaju kada ne postoji zgrada sa zadatim ID-em
        * */

        // zgrada sa ID-em = 200 ne postoji
        this.mockMvc.perform(get(URL_PREFIX + "/200/billboard/notifications_from_to")
                .header("Authentication-Token", this.accessTokenTenant)
                .params(params)
                .param("from", "2017-11-25 00:00:00")
                .param("to", "2017-11-27 23:59:59"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testSearchNotificationsByTenantWithoutApartment() throws Exception {
        /*
        * Test proverava ispravnost rada metode searchNotifications kontrolera BillboardController
        * (dobavljanje liste svih javnih obavestenja koja se nalaze na bilbordu zgrade sa zadatim ID-em i pritom su kreirana
        * u periodu od zadatog datuma do zadatog datuma)
        * u slucaju kada trenutno ulogovani stanar nije povezan ni sa jednim stanom
        * */

        /*
        stanar sa ID-em = 108 nije povezan ni sa jednim stanom jos uvek, i ima podatke:
            korisnicko ime = jovanka,
            lozinka = jovanka
            id = 108
        */
        this.mockMvc.perform(get(URL_PREFIX + "/100/billboard/notifications_from_to")
                .header("Authentication-Token", this.accessTokenTenantWithoutApartment)
                .params(params)
                .param("from", "2017-11-25 00:00:00")
                .param("to", "2017-11-27 23:59:59"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSearchNotificationsByTenantFromInvalidBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode searchNotifications kontrolera BillboardController
        * (dobavljanje liste svih javnih obavestenja koja se nalaze na bilbordu zgrade sa zadatim ID-em i pritom su kreirana
        * u periodu od zadatog datuma do zadatog datuma)
        * u slucaju kada trenutno ulogovani korisnik ne zivi u zgradi sa zadatim ID-em
        * */

        /*
        stanar sa ID-em = 101 zivi u zgradi sa ID-em = 100 (a ne sa ID-em = 101) i ima podatke:
            korisnicko ime = kaca,
            lozinka = kaca,
            apartment_id = 101 (building_id = 100)
         */
        this.mockMvc.perform(get(URL_PREFIX + "/101/billboard/notifications_from_to")
                .header("Authentication-Token", this.accessTokenTenant)
                .params(params)
                .param("from", "2017-11-25 00:00:00")
                .param("to", "2017-11-27 23:59:59"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSearchNotificationsInappropriateTypeOfDate() throws Exception {
        /*
        * Test proverava ispravnost rada metode searchNotifications kontrolera BillboardController
        * (dobavljanje liste svih javnih obavestenja koja se nalaze na bilbordu zgrade sa zadatim ID-em i pritom su kreirana
        * u periodu od zadatog datuma do zadatog datuma)
        * u slucaju kada trenutno ulogovani korisnik ne zivi u zgradi sa zadatim ID-em
        * */

        /*
        stanar sa ID-em = 101 zivi u zgradi sa ID-em = 100 (a ne sa ID-em = 101) i ima podatke:
            korisnicko ime = kaca,
            lozinka = kaca,
            apartment_id = 101 (building_id = 100)
         */
        this.mockMvc.perform(get(URL_PREFIX + "/100/billboard/notifications_from_to")
                .header("Authentication-Token", this.accessTokenTenant)
                .params(params)
                .param("from", "greska")
                .param("to", "2017-11-27 23:59:59"))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za searchMeetingItems
    @Test
    public void testSearchMeetingItems() throws Exception {
        /*
        * Test proverava ispravnost rada metode searchMeetingItems kontrolera BillboardController
        * (dobavljanje liste svih predloga tacaka dnevnog reda koja se nalaze na bilbordu
        * zgrade sa zadatim ID-em i pritom su kreirani u periodu od zadatog datuma do zadatog datuma)
        * */

        /*
        u zgradi sa ID-em = 100 (na bilbordu sa ID-em = 100) postoji 1 predlog tacke dnevnog reda
        u periodu od 2017-11-25 do 2017-12-01 i ima podatke:
               1 - ID = 102,
                   content = Third meeting item content,
                   creator_id = 102
        */
        this.mockMvc.perform(get(URL_PREFIX + "/100/billboard/meeting_items_from_to")
                .header("Authentication-Token", this.accessTokenTenant)
                .params(params)
                .param("from", "2017-11-25 00:00:00")
                .param("to", "2017-12-01 23:59:59"))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[*]", hasSize(1)))
                .andExpect(jsonPath("$.[0].id").value(102))
                .andExpect(jsonPath("$.[0].creator.id").value(102));
    }

    @Test
    public void testSearchMeetingItemsByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode searchMeetingItems kontrolera BillboardController
        * (dobavljanje liste svih predloga tacaka dnevnog reda koja se nalaze na bilbordu
        * zgrade sa zadatim ID-em i pritom su kreirani u periodu od zadatog datuma do zadatog datuma)
        * u slucaju kada akciju obavlja nelogovani korisnik
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/100/billboard/meeting_items_from_to")
                .params(params)
                .param("from", "2017-11-25 00:00:00")
                .param("to", "2017-12-01 23:59:59"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSearchMeetingItemsByNotAuthenticatedTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode searchMeetingItems kontrolera BillboardController
        * (dobavljanje liste svih predloga tacaka dnevnog reda koja se nalaze na bilbordu
        * zgrade sa zadatim ID-em i pritom su kreirani u periodu od zadatog datuma do zadatog datuma)
        * u slucaju kada akciju obavlja logovani korisnik koji nema ulogu stanara
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/100/billboard/meeting_items_from_to")
                .header("Authentication-Token", this.accessTokenFirm)
                .params(params)
                .param("from", "2017-11-25 00:00:00")
                .param("to", "2017-12-01 23:59:59"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSearchMeetingItemsNonexistentBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode searchMeetingItems kontrolera BillboardController
        * (dobavljanje liste svih predloga tacaka dnevnog reda koja se nalaze na bilbordu
        * zgrade sa zadatim ID-em i pritom su kreirani u periodu od zadatog datuma do zadatog datuma)
        * u slucaju kada ne postoji zgrada sa zadatim ID-em
        * */

        // zgrada sa ID-em = 200 ne postoji
        this.mockMvc.perform(get(URL_PREFIX + "/200/billboard/meeting_items_from_to")
                .header("Authentication-Token", this.accessTokenTenant)
                .params(params)
                .param("from", "2017-11-25 00:00:00")
                .param("to", "2017-12-01 23:59:59"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testSearchMeetingItemsByTenantWithoutApartment() throws Exception {
        /*
        * Test proverava ispravnost rada metode searchMeetingItems kontrolera BillboardController
        * (dobavljanje liste svih predloga tacaka dnevnog reda koja se nalaze na bilbordu
        * zgrade sa zadatim ID-em i pritom su kreirani u periodu od zadatog datuma do zadatog datuma)
        * u slucaju kada trenutno ulogovani stanar nije povezan ni sa jednim stanom
        * */

        /*
        stanar sa ID-em = 108 nije povezan ni sa jednim stanom jos uvek, i ima podatke:
            korisnicko ime = jovanka,
            lozinka = jovanka
            id = 108
        */
        this.mockMvc.perform(get(URL_PREFIX + "/100/billboard/meeting_items_from_to")
                .header("Authentication-Token", this.accessTokenTenantWithoutApartment)
                .params(params)
                .param("from", "2017-11-25 00:00:00")
                .param("to", "2017-12-01 23:59:59"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSearchMeetingItemsByTenantFromInvalidBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode searchMeetingItems kontrolera BillboardController
        * (dobavljanje liste svih predloga tacaka dnevnog reda koja se nalaze na bilbordu
        * zgrade sa zadatim ID-em i pritom su kreirani u periodu od zadatog datuma do zadatog datuma)
        * u slucaju kada trenutno ulogovani korisnik ne zivi u zgradi sa zadatim ID-em
        * */

        /*
        stanar sa ID-em = 101 zivi u zgradi sa ID-em = 100 (a ne sa ID-em = 101) i ima podatke:
            korisnicko ime = kaca,
            lozinka = kaca,
            apartment_id = 101 (building_id = 100)
         */
        this.mockMvc.perform(get(URL_PREFIX + "/101/billboard/meeting_items_from_to")
                .header("Authentication-Token", this.accessTokenTenant)
                .params(params)
                .param("from", "2017-11-25 00:00:00")
                .param("to", "2017-12-01 23:59:59"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSearchMeetingItemsInappropriateTypeOfDate() throws Exception {
        /*
        * Test proverava ispravnost rada metode searchMeetingItems kontrolera BillboardController
        * (dobavljanje liste svih predloga tacaka dnevnog reda koja se nalaze na bilbordu
        * zgrade sa zadatim ID-em i pritom su kreirani u periodu od zadatog datuma do zadatog datuma)
        * u slucaju kada trenutno ulogovani korisnik ne zivi u zgradi sa zadatim ID-em
        * */

        /*
        stanar sa ID-em = 101 zivi u zgradi sa ID-em = 100 (a ne sa ID-em = 101) i ima podatke:
            korisnicko ime = kaca,
            lozinka = kaca,
            apartment_id = 101 (building_id = 100)
         */
        this.mockMvc.perform(get(URL_PREFIX + "/100/billboard/meeting_items_from_to")
                .header("Authentication-Token", this.accessTokenTenant)
                .params(params)
                .param("from", "greska")
                .param("to", "2017-12-01 23:59:59"))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za getNotification
    @Test
    public void testGetNotification() throws Exception {
        /*
        * Test proverava ispravnost rada metode getNotification kontrolera BillboardController
        * (dobavljanje konkretnog javnog obavestenja (na osnovu zadatog ID-a) koje se nalaze na bilbordu zgrade sa zadatim ID-em)
        * */

        /*
        u zgradi sa ID-em = 100 (na bilbordu sa ID-em = 100) postoji javno obavestenje sa ID-em = 100 i podacima:
               content = First notification,
               creator_id = 101
        */
        this.mockMvc.perform(get(URL_PREFIX + "/100/billboard/notifications/100")
                .header("Authentication-Token", this.accessTokenTenant))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(100))
                .andExpect(jsonPath("$.content").value("First notification content"))
                .andExpect(jsonPath("$.creator.id").value(101));
    }

    @Test
    public void testGetNotificationByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode getNotification kontrolera BillboardController
        * (dobavljanje konkretnog javnog obavestenja (na osnovu zadatog ID-a) koje se nalaze na bilbordu zgrade sa zadatim ID-em)
        * u slucaju kada akciju obavlja nelogovani korisnik
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/100/billboard/notifications/100"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetNotificationByNotAuthenticatedTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode getNotification kontrolera BillboardController
        * (dobavljanje konkretnog javnog obavestenja (na osnovu zadatog ID-a) koje se nalaze na bilbordu zgrade sa zadatim ID-em)
        * u slucaju kada akciju obavlja logovani korisnik koji nema ulogu stanara
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/100/billboard/notifications/100")
                .header("Authentication-Token", this.accessTokenFirm))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetNotificationNonexistentBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode getNotification kontrolera BillboardController
        * (dobavljanje konkretnog javnog obavestenja (na osnovu zadatog ID-a) koje se nalaze na bilbordu zgrade sa zadatim ID-em)
        * u slucaju kada ne postoji zgrada sa zadatim ID-em
        * */

        // ne postoji zgrada sa ID-em = 200
        this.mockMvc.perform(get(URL_PREFIX + "/200/billboard/notifications/100")
                .header("Authentication-Token", this.accessTokenTenant))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetNotificationByTenantWithoutApartment() throws Exception {
        /*
        * Test proverava ispravnost rada metode getNotification kontrolera BillboardController
        * (dobavljanje konkretnog javnog obavestenja (na osnovu zadatog ID-a) koje se nalaze na bilbordu zgrade sa zadatim ID-em)
        * u slucaju kada trenutno ulogovani stanar nije povezan ni sa jednim stanom
        * */

        /*
        stanar sa ID-em = 108 nije povezan ni sa jednim stanom jos uvek, i ima podatke:
            korisnicko ime = jovanka,
            lozinka = jovanka
            id = 108
        */
        this.mockMvc.perform(get(URL_PREFIX + "/100/billboard/notifications/100")
                .header("Authentication-Token", this.accessTokenTenantWithoutApartment))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetNotificationByTenantFromInvalidBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode getNotification kontrolera BillboardController
        * (dobavljanje konkretnog javnog obavestenja (na osnovu zadatog ID-a) koje se nalaze na bilbordu zgrade sa zadatim ID-em)
        * u slucaju kada trenutno ulogovani stanar ne zivi u zgradi sa zadatim ID-em
        * */

        /*
        stanar sa ID-em = 101 zivi u zgradi sa ID-em = 100 (a ne sa ID-em = 101) i ima podatke:
            korisnicko ime = kaca,
            lozinka = kaca,
            apartment_id = 101 (building_id = 100)
         */
        this.mockMvc.perform(get(URL_PREFIX + "/101/billboard/notifications/100")
                .header("Authentication-Token", this.accessTokenTenant))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetNotificationNonexistentNotification() throws Exception {
        /*
        * Test proverava ispravnost rada metode getNotification kontrolera BillboardController
        * (dobavljanje konkretnog javnog obavestenja (na osnovu zadatog ID-a) koje se nalaze na bilbordu zgrade sa zadatim ID-em)
        * u slucaju kada ne postoji javno obavestenje sa zadatim ID-em
        * */

        // u zgradi sa ID-em = 100 (na bilbordu sa ID-em = 100) ne postoji javno obavestenje sa ID-em = 200
        this.mockMvc.perform(get(URL_PREFIX + "/100/billboard/notifications/200")
                .header("Authentication-Token", this.accessTokenTenant))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetNotificationFromInvalidBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode getNotification kontrolera BillboardController
        * (dobavljanje konkretnog javnog obavestenja (na osnovu zadatog ID-a) koje se nalaze na bilbordu zgrade sa zadatim ID-em)
        * u slucaju kada javno obavestenje sa zadatim ID-em ne pripada zgradi sa zadatim ID-em
        * */

        // javno obavestenje sa ID-em = 102 pripada zgradi sa ID-em = 101 (a ne sa ID-em = 100)
        this.mockMvc.perform(get(URL_PREFIX + "/100/billboard/notifications/102")
                .header("Authentication-Token", this.accessTokenTenant))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za getMeetingItem
    @Test
    public void testGetMeetingItem() throws Exception {
        /*
        * Test proverava ispravnost rada metode getMeetingItem kontrolera BillboardController
        * (dobavljanje konkretnog predloga tacke dnevnog reda (na osnovu zadatog ID-a) koji se nalazi na bilbordu zgrade sa zadatim ID-em,
        * pri cemu se obavestenje dobavlja iskljucivo ukoliko ono nije jos uvek obradjeno na nekom sastanku skupstine stanara)
        * */

        /*
        u zgradi sa ID-em = 100 (na bilbordu sa ID-em = 100) postoji jedan nepokupljen predlog tacke dnevnog reda sa ID-em = 102 i podacima:
            content = Third meeting item,
            creator_id = 102
        */
        this.mockMvc.perform(get(URL_PREFIX + "/100/billboard/meeting_items/102")
                .header("Authentication-Token", this.accessTokenTenant)
                .params(params))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(102))
                .andExpect(jsonPath("$.creator.id").value(102));
    }

    @Test
    public void testGetMeetingItemByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode getMeetingItem kontrolera BillboardController
        * (dobavljanje konkretnog predloga tacke dnevnog reda (na osnovu zadatog ID-a) koji se nalazi na bilbordu zgrade sa zadatim ID-em,
        * pri cemu se obavestenje dobavlja iskljucivo ukoliko ono nije jos uvek obradjeno na nekom sastanku skupstine stanara)
        * u slucaju kada akciju obavlja nelogovani korisnik
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/100/billboard/meeting_items/102")
                .params(params))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetMeetingItemByNotAuthenticatedTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode getMeetingItem kontrolera BillboardController
        * (dobavljanje konkretnog predloga tacke dnevnog reda (na osnovu zadatog ID-a) koji se nalazi na bilbordu zgrade sa zadatim ID-em,
        * pri cemu se obavestenje dobavlja iskljucivo ukoliko ono nije jos uvek obradjeno na nekom sastanku skupstine stanara)
        * u slucaju kada akciju obavlja logovani korisnik koji nema ulogu stanara
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/100/billboard/meeting_items/102")
                .header("Authentication-Token", this.accessTokenFirm)
                .params(params))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetMeetingItemNonexistentBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode getMeetingItem kontrolera BillboardController
        * (dobavljanje konkretnog predloga tacke dnevnog reda (na osnovu zadatog ID-a) koji se nalazi na bilbordu zgrade sa zadatim ID-em,
        * pri cemu se obavestenje dobavlja iskljucivo ukoliko ono nije jos uvek obradjeno na nekom sastanku skupstine stanara)
        * u slucaju kada ne postoji zgrada sa zadatim ID-em
        * */

        // ne postoji zgrada sa ID-em = 200
        this.mockMvc.perform(get(URL_PREFIX + "/200/billboard/meeting_items/102")
                .header("Authentication-Token", this.accessTokenTenant)
                .params(params))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetMeetingItemByTenantWithoutApartment() throws Exception {
        /*
        * Test proverava ispravnost rada metode getMeetingItem kontrolera BillboardController
        * (dobavljanje konkretnog predloga tacke dnevnog reda (na osnovu zadatog ID-a) koji se nalazi na bilbordu zgrade sa zadatim ID-em,
        * pri cemu se obavestenje dobavlja iskljucivo ukoliko ono nije jos uvek obradjeno na nekom sastanku skupstine stanara)
        * u slucaju kada trenutno ulogovani stanar nije povezan ni sa jednim stanom
        * */

        /*
        stanar sa ID-em = 108 nije povezan ni sa jednim stanom jos uvek, i ima podatke:
            korisnicko ime = jovanka,
            lozinka = jovanka
            id = 108
        */
        this.mockMvc.perform(get(URL_PREFIX + "/100/billboard/meeting_items/102")
                .header("Authentication-Token", this.accessTokenTenantWithoutApartment)
                .params(params))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetMeetingItemByTenantFromInvalidBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode getMeetingItem kontrolera BillboardController
        * (dobavljanje konkretnog predloga tacke dnevnog reda (na osnovu zadatog ID-a) koji se nalazi na bilbordu zgrade sa zadatim ID-em,
        * pri cemu se obavestenje dobavlja iskljucivo ukoliko ono nije jos uvek obradjeno na nekom sastanku skupstine stanara)
        * u slucaju kada trenutno ulogovani stanar ne zivi u zgradi sa zadatim ID-em
        * */

        /*
        stanar sa ID-em = 101 zivi u zgradi sa ID-em = 100 (a ne sa ID-em = 101) i ima podatke:
            korisnicko ime = kaca,
            lozinka = kaca,
            apartment_id = 101 (building_id = 100)
         */
        this.mockMvc.perform(get(URL_PREFIX + "/101/billboard/meeting_items/102")
                .header("Authentication-Token", this.accessTokenTenant)
                .params(params))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetMeetingItemNonexistentMeetingItem() throws Exception {
        /*
        * Test proverava ispravnost rada metode getMeetingItem kontrolera BillboardController
        * (dobavljanje konkretnog predloga tacke dnevnog reda (na osnovu zadatog ID-a) koji se nalazi na bilbordu zgrade sa zadatim ID-em,
        * pri cemu se obavestenje dobavlja iskljucivo ukoliko ono nije jos uvek obradjeno na nekom sastanku skupstine stanara)
        * u slucaju kada ne postoji predlog tacke dnevnog reda sa zadatim ID-em
        * */

        // u zgradi sa ID-em = 100 (na bilbordu sa ID-em = 100) ne postoji predlog tacke dnevnog reda sa ID-em = 200
        this.mockMvc.perform(get(URL_PREFIX + "/100/billboard/meeting_items/200")
                .header("Authentication-Token", this.accessTokenTenant)
                .params(params))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetMeetingItemFromInvalidBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode getMeetingItem kontrolera BillboardController
        * (dobavljanje konkretnog predloga tacke dnevnog reda (na osnovu zadatog ID-a) koji se nalazi na bilbordu zgrade sa zadatim ID-em,
        * pri cemu se obavestenje dobavlja iskljucivo ukoliko ono nije jos uvek obradjeno na nekom sastanku skupstine stanara)
        * u slucaju kada predlog tacke dnevnog reda sa zadatim ID-em ne pripada zgradi sa zadatim ID-em
        * */

        // predlog tacke dnevnog reda sa ID-em = 103 pripada zgradi sa ID-em = 101 (a ne sa ID-em = 100)
        this.mockMvc.perform(get(URL_PREFIX + "/100/billboard/meeting_items/103")
                .header("Authentication-Token", this.accessTokenTenant)
                .params(params))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testGetMeetingItemAlreadyProcessed() throws Exception {
        /*
        * Test proverava ispravnost rada metode getMeetingItem kontrolera BillboardController
        * (dobavljanje konkretnog predloga tacke dnevnog reda (na osnovu zadatog ID-a) koji se nalazi na bilbordu zgrade sa zadatim ID-em,
        * pri cemu se obavestenje dobavlja iskljucivo ukoliko ono nije jos uvek obradjeno na nekom sastanku skupstine stanara)
        * u slucaju kada je predlog tacke dnevnog reda sa zadatim ID-em vec obradjen na nekom sastanku skupstine stanara
        * */

        /*
        predlog tacke dnevnog reda sa ID-em = 100 je vec obradjen na nekom od sastanaka skupstine stanara:
            id = 100,
            content = First meeting item,
            creator_id = 101,
            meeting_id = 100,
            billboard_id = 100 (building_id = 100)
        */
        this.mockMvc.perform(get(URL_PREFIX + "/100/billboard/meeting_items/100")
                .header("Authentication-Token", this.accessTokenTenant)
                .params(params))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za createNotification
    @Test
    @Transactional
    @Rollback(true)
    public void testCreateNotification() throws Exception {
        /*
        * Test proverava ispravnost rada metode createNotification kontrolera BillboardController
        * (kreiranje novog obavestenja koje se nalazi na bilbordu zgrade sa zadatim ID-em)
        * */

        NotificationCreateUpdateDTO notificationCreateUpdateDTO = new NotificationCreateUpdateDTO(
                "First notification content", "First notification");

        String notificationDTOJson = TestUtil.json(notificationCreateUpdateDTO);

        /*
        novokreirano obavestenje mora da ima setovanog kreatora - stanar sa ID-em = 101 i podacima:
            korisnicko ime = kaca,
            lozinka = kaca,
            id = 101
        */
        this.mockMvc.perform(post(URL_PREFIX + "/100/billboard/notifications")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(notificationDTOJson))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.content").value("First notification content"))
                .andExpect(jsonPath("$.title").value("First notification"))
                .andExpect(jsonPath("$.creator.id").value(101));
    }

    @Test
    public void testCreateNotificationByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode createNotification kontrolera BillboardController
        * (kreiranje novog obavestenja koje se nalazi na bilbordu zgrade sa zadatim ID-em)
        * u slucaju kada akciju obavlja nelogovani korisnik
        * */

        NotificationCreateUpdateDTO notificationCreateUpdateDTO = new NotificationCreateUpdateDTO(
                "First notification content", "First notification");

        String notificationDTOJson = TestUtil.json(notificationCreateUpdateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/100/billboard/notifications")
                .contentType(contentType)
                .content(notificationDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testCreateNotificationByNotAuthenticatedTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode createNotification kontrolera BillboardController
        * (kreiranje novog obavestenja koje se nalazi na bilbordu zgrade sa zadatim ID-em)
        * u slucaju kada akciju obavlja logovani korisnik koji nema ulogu stanara
        * */

        NotificationCreateUpdateDTO notificationCreateUpdateDTO = new NotificationCreateUpdateDTO(
                "First notification content", "First notification");

        String notificationDTOJson = TestUtil.json(notificationCreateUpdateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/100/billboard/notifications")
                .header("Authentication-Token", this.accessTokenFirm)
                .contentType(contentType)
                .content(notificationDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testCreateNotificationNonexistentBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode createNotification kontrolera BillboardController
        * (kreiranje novog obavestenja koje se nalazi na bilbordu zgrade sa zadatim ID-em)
        * u slucaju kada ne postoji zgrada sa zadatim ID-em
        * */

        NotificationCreateUpdateDTO notificationCreateUpdateDTO = new NotificationCreateUpdateDTO(
                "First notification content", "First notification");

        String notificationDTOJson = TestUtil.json(notificationCreateUpdateDTO);

        // ne postoji zgrada sa ID-em = 200
        this.mockMvc.perform(post(URL_PREFIX + "/200/billboard/notifications")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(notificationDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testCreateNotificationByTenantWithoutApartment() throws Exception {
        /*
        * Test proverava ispravnost rada metode createNotification kontrolera BillboardController
        * (kreiranje novog obavestenja koje se nalazi na bilbordu zgrade sa zadatim ID-em)
        * u slucaju kada trenutno ulogovani stanar nije povezan ni sa jednim stanom
        * */

        NotificationCreateUpdateDTO notificationCreateUpdateDTO = new NotificationCreateUpdateDTO(
                "First notification content", "First notification");

        String notificationDTOJson = TestUtil.json(notificationCreateUpdateDTO);

        /*
        stanar sa ID-em = 108 nije povezan ni sa jednim stanom jos uvek, i ima podatke:
            korisnicko ime = jovanka,
            lozinka = jovanka
            id = 108
        */
        this.mockMvc.perform(post(URL_PREFIX + "/100/billboard/notifications")
                .header("Authentication-Token", this.accessTokenTenantWithoutApartment)
                .contentType(contentType)
                .content(notificationDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testCreateNotificationByTenantFromInvalidBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode createNotification kontrolera BillboardController
        * (kreiranje novog obavestenja koje se nalazi na bilbordu zgrade sa zadatim ID-em)
        * u slucaju kada trenutno ulogovani stanar ne zivi u zgradi sa zadatim ID-em
        * */

        NotificationCreateUpdateDTO notificationCreateUpdateDTO = new NotificationCreateUpdateDTO(
                "First notification content", "First notification");

        String notificationDTOJson = TestUtil.json(notificationCreateUpdateDTO);

        /*
        stanar sa ID-em = 101 zivi u zgradi sa ID-em = 100 (a ne sa ID-em = 101) i ima podatke:
            korisnicko ime = kaca,
            lozinka = kaca,
            apartment_id = 101 (building_id = 100)
         */
        this.mockMvc.perform(post(URL_PREFIX + "/101/billboard/notifications")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(notificationDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testCreateNotificationMissingContent() throws Exception {
        /*
        * Test proverava ispravnost rada metode createNotification kontrolera BillboardController
        * (kreiranje novog obavestenja koje se nalazi na bilbordu zgrade sa zadatim ID-em)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne poseduje sva polja neohodna za kreiranje - nedostaje polje content
        * */

        // nedostaje content vrednost
        NotificationCreateUpdateDTO notificationCreateUpdateDTO = new NotificationCreateUpdateDTO();
        notificationCreateUpdateDTO.setTitle("First notification");

        String notificationDTOJson = TestUtil.json(notificationCreateUpdateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/100/billboard/notifications")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(notificationDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateNotificationInappropriateTypeOfContent() throws Exception {
        /*
        * Test proverava ispravnost rada metode createNotification kontrolera BillboardController
        * (kreiranje novog obavestenja koje se nalazi na bilbordu zgrade sa zadatim ID-em)
        * u slucaju kada je dolazni DTO objekat nevalidan: poseduje sva polja
        * ali je tip podatka u polju content nevalidan (nije String nego int)
        * */

        // content vrednost je neodgovarajuceg tipa (int)
        String notificationDTOJson = "{content:1;" +
                "title:'First notification'}";

        this.mockMvc.perform(post(URL_PREFIX + "/101/billboard/notifications")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(notificationDTOJson))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za createMeetingItem
    @Test
    @Transactional
    @Rollback(true)
    public void testCreateMeetingItem() throws Exception {
        /*
        * Test proverava ispravnost rada metode createMeetingItem kontrolera BillboardController
        * (kreiranje novog predloga tacke dnevnog reda koji se nalazi na bilbordu zgrade sa zadatim ID-em)
        * u slucaju kada predlog tacke dnevnog reda ne sadrzi i anketu unutar sebe
        * */

        MeetingItemCreateDTO meetingItemCreateDTO = new MeetingItemCreateDTO();
        meetingItemCreateDTO.setContent("First meeting item content");
        meetingItemCreateDTO.setTitle("First meeting item");

        String meetingItemDTOJson = TestUtil.json(meetingItemCreateDTO);

        /*
        novokreirani predlog tacke dnevnog reda mora da ima setovanog kreatora trenutno ulogovanog stanara - stanar sa podacima:
            korisnicko ime = kaca,
            lozinka = kaca,
            id = 101
        */
        this.mockMvc.perform(post(URL_PREFIX + "/100/billboard/meeting_items")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.content").value("First meeting item content"))
                .andExpect(jsonPath("$.title").value("First meeting item"))
                .andExpect(jsonPath("$.questionnaire").doesNotExist())
                .andExpect(jsonPath("$.creator.id").value(101));
    }

    @Test
    @Transactional
    @Rollback(true)
    public void testCreateMeetingItemWithQuestionnaire() throws Exception {
        /*
        * Test proverava ispravnost rada metode createMeetingItem kontrolera BillboardController
        * (kreiranje novog predloga tacke dnevnog reda koji se nalazi na bilbordu zgrade sa zadatim ID-em)
        * u slucaju kada predlog tacke dnevnog reda sadrzi i anketu unutar sebe
        * */

        List<String> answers = new ArrayList<>();
        answers.add("Yes");
        answers.add("No");

        List<QuestionCreateDTO> questions = new ArrayList<>();
        QuestionCreateDTO question = new QuestionCreateDTO("First question", answers);
        questions.add(question);

        QuestionnaireCreateDTO questionnaire = new QuestionnaireCreateDTO(questions);

        MeetingItemCreateDTO meetingItemCreateDTO = new MeetingItemCreateDTO(
                "First meeting item", "First meeting item content", questionnaire, null);

        String meetingItemDTOJson = TestUtil.json(meetingItemCreateDTO);

        /*
        novokreirani predlog tacke dnevnog reda mora da ima setovanog kreatora trenutno ulogovanog stanara - stanar sa podacima:
            korisnicko ime = kaca,
            lozinka = kaca,
            id = 101
        */
        this.mockMvc.perform(post(URL_PREFIX + "/100/billboard/meeting_items")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.content").value("First meeting item content"))
                .andExpect(jsonPath("$.title").value("First meeting item"))
                .andExpect(jsonPath("$.questionnaire").exists())
                .andExpect(jsonPath("$.questionnaire.questions", hasSize(1)))
                .andExpect(jsonPath("$.questionnaire.questions[0].content").value("First question"))
                .andExpect(jsonPath("$.questionnaire.questions[0].answers", hasSize(2)))
                .andExpect(jsonPath("$.questionnaire.questions[0].answers.[*]", containsInAnyOrder("Yes", "No")))
                .andExpect(jsonPath("$.creator.id").value(101));
    }

    @Test
    public void testCreateMeetingItemByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode createMeetingItem kontrolera BillboardController
        * (kreiranje novog predloga tacke dnevnog reda koji se nalazi na bilbordu zgrade sa zadatim ID-em)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        MeetingItemCreateDTO meetingItemCreateDTO = new MeetingItemCreateDTO();
        meetingItemCreateDTO.setContent("First meeting item content");
        meetingItemCreateDTO.setTitle("First meeting item");

        String meetingItemDTOJson = TestUtil.json(meetingItemCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/100/billboard/meeting_items")
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testCreateMeetingItemByNotAuthenticatedTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode createMeetingItem kontrolera BillboardController
        * (kreiranje novog predloga tacke dnevnog reda koji se nalazi na bilbordu zgrade sa zadatim ID-em)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu stanara
        * */

        MeetingItemCreateDTO meetingItemCreateDTO = new MeetingItemCreateDTO();
        meetingItemCreateDTO.setContent("First meeting item content");
        meetingItemCreateDTO.setTitle("First meeting item");

        String meetingItemDTOJson = TestUtil.json(meetingItemCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/100/billboard/meeting_items")
                .header("Authentication-Token", this.accessTokenFirm)
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testCreateMeetingItemNonexistentBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode createMeetingItem kontrolera BillboardController
        * (kreiranje novog predloga tacke dnevnog reda koji se nalazi na bilbordu zgrade sa zadatim ID-em)
        * u slucaju kada ne postoji zgrada sa zadatim ID-em
        * */

        MeetingItemCreateDTO meetingItemCreateDTO = new MeetingItemCreateDTO();
        meetingItemCreateDTO.setContent("First meeting item content");
        meetingItemCreateDTO.setTitle("First meeting item");

        String meetingItemDTOJson = TestUtil.json(meetingItemCreateDTO);

        // ne postoji zgrada sa ID-em = 200
        this.mockMvc.perform(post(URL_PREFIX + "/200/billboard/meeting_items")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testCreateMeetingItemByTenantWithoutApartment() throws Exception {
        /*
        * Test proverava ispravnost rada metode createMeetingItem kontrolera BillboardController
        * (kreiranje novog predloga tacke dnevnog reda koji se nalazi na bilbordu zgrade sa zadatim ID-em)
        * u slucaju kada trenutno ulogovani stanar nije povezan ni sa jednom zgradom
        * */

        MeetingItemCreateDTO meetingItemCreateDTO = new MeetingItemCreateDTO();
        meetingItemCreateDTO.setContent("First meeting item content");
        meetingItemCreateDTO.setTitle("First meeting item");

        String meetingItemDTOJson = TestUtil.json(meetingItemCreateDTO);

        /*
        stanar sa ID-em = 108 nije povezan ni sa jednim stanom jos uvek, i ima podatke:
            korisnicko ime = jovanka,
            lozinka = jovanka
            id = 108
        */
        this.mockMvc.perform(post(URL_PREFIX + "/100/billboard/meeting_items")
                .header("Authentication-Token", this.accessTokenTenantWithoutApartment)
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testCreateMeetingItemByTenantFromInvalidBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode createMeetingItem kontrolera BillboardController
        * (kreiranje novog predloga tacke dnevnog reda koji se nalazi na bilbordu zgrade sa zadatim ID-em)
        * u slucaju kada trenutno ulogovani stanar ne zivi u zgradi sa zadatim ID-em
        * */

        MeetingItemCreateDTO meetingItemCreateDTO = new MeetingItemCreateDTO();
        meetingItemCreateDTO.setContent("First meeting item content");
        meetingItemCreateDTO.setTitle("First meeting item");

        String meetingItemDTOJson = TestUtil.json(meetingItemCreateDTO);

        /*
        stanar sa ID-em = 101 zivi u zgradi sa ID-em = 100 (a ne sa ID-em = 101) i ima podatke:
            korisnicko ime = kaca,
            lozinka = kaca,
            apartment_id = 101 (building_id = 100)
        */
        this.mockMvc.perform(post(URL_PREFIX + "/101/billboard/meeting_items")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testCreateMeetingItemMissingContent() throws Exception {
        /*
        * Test proverava ispravnost rada metode createMeetingItem kontrolera BillboardController
        * (kreiranje novog predloga tacke dnevnog reda koji se nalazi na bilbordu zgrade sa zadatim ID-em)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne poseduje sva polja neohodna za kreiranje - nedostaje polje content
        * */

        // nedostaje polje content
        MeetingItemCreateDTO meetingItemCreateDTO = new MeetingItemCreateDTO();
        meetingItemCreateDTO.setTitle("First meeting item");

        String meetingItemDTOJson = TestUtil.json(meetingItemCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/100/billboard/meeting_items")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateMeetingItemInappropriateTypeOfContent() throws Exception {
        /*
        * Test proverava ispravnost rada metode createMeetingItem kontrolera BillboardController
        * (kreiranje novog predloga tacke dnevnog reda koji se nalazi na bilbordu zgrade sa zadatim ID-em)
        * u slucaju kada je dolazni DTO objekat nevalidan: poseduje sva polja
        * ali je tip podatka u polju content nevalidan (nije String nego int)
        * */

        // nevalidan tip content polja
        String meetingItemDTOJson = "{content:1; title:'First meeting item'}";

        this.mockMvc.perform(post(URL_PREFIX + "/100/billboard/meeting_items")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateMeetingItemMissingListOfQuestionsInQuestionnaire() throws Exception {
        /*
        * Test proverava ispravnost rada metode createMeetingItem kontrolera BillboardController
        * (kreiranje novog predloga tacke dnevnog reda koji se nalazi na bilbordu zgrade sa zadatim ID-em)
        * u slucaju kada predlog tacke dnevnog reda obuhvata i anketu i pri tom je dolazni DTO objekat nevalidan: ne poseduje sva polja
        * neophodna za kreiranje - nedostaje polje lista pitanja (u anketi)
        * */

        QuestionnaireCreateDTO questionnaire = new QuestionnaireCreateDTO();
        // nedostaje lista pitanja
        questionnaire.setQuestions(null);

        MeetingItemCreateDTO meetingItemCreateDTO = new MeetingItemCreateDTO(
                "First meeting item", "First meeting item content", questionnaire, null);

        String meetingItemDTOJson = TestUtil.json(meetingItemCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/100/billboard/meeting_items")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateMeetingItemListOfQuestionsInQuestionnaireIsEmpty() throws Exception {
        /*
        * Test proverava ispravnost rada metode createMeetingItem kontrolera BillboardController
        * (kreiranje novog predloga tacke dnevnog reda koji se nalazi na bilbordu zgrade sa zadatim ID-em)
        * u slucaju kada predlog tacke dnevnog reda obuhvata i anketu i pri tom je dolazni DTO objekat nevalidan: poseduje sva polja
        * neophodna za kreiranje - ali je lista pitanja prazna (u anketi)
        * */

        QuestionnaireCreateDTO questionnaire = new QuestionnaireCreateDTO();
        // lista pitanja je prazna
        questionnaire.setQuestions(new ArrayList<>());

        MeetingItemCreateDTO meetingItemCreateDTO = new MeetingItemCreateDTO(
                "First meeting item", "First meeting item content", questionnaire, null);

        String meetingItemDTOJson = TestUtil.json(meetingItemCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/100/billboard/meeting_items")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateMeetingItemExistQuestionWithNoContent() throws Exception {
        /*
        * Test proverava ispravnost rada metode createMeetingItem kontrolera BillboardController
        * (kreiranje novog predloga tacke dnevnog reda koji se nalazi na bilbordu zgrade sa zadatim ID-em)
        * u slucaju kada predlog tacke dnevnog reda obuhvata i anketu i pri tom je dolazni DTO objekat nevalidan: ne poseduje sva polja
        * neophodna za kreiranje - nedostaje polje sadrzaj (u bar jednom pitanju ankete)
        * */

        List<String> answers = new ArrayList<>();
        answers.add("Yes");
        answers.add("No");

        List<QuestionCreateDTO> questions = new ArrayList<>();
        QuestionCreateDTO question = new QuestionCreateDTO();
        question.setAnswers(answers);
        // nedostaje sadrzaj pitanja
        question.setContent(null);

        questions.add(question);

        QuestionnaireCreateDTO questionnaire = new QuestionnaireCreateDTO(questions);

        MeetingItemCreateDTO meetingItemCreateDTO = new MeetingItemCreateDTO(
                "First meeting item", "First meeting item content", questionnaire, null);

        String meetingItemDTOJson = TestUtil.json(meetingItemCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/100/billboard/meeting_items")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateMeetingItemExistQuestionWithNoAnswerList() throws Exception {
        /*
        * Test proverava ispravnost rada metode createMeetingItem kontrolera BillboardController
        * (kreiranje novog predloga tacke dnevnog reda koji se nalazi na bilbordu zgrade sa zadatim ID-em)
        * u slucaju kada predlog tacke dnevnog reda obuhvata i anketu i pri tom je dolazni DTO objekat nevalidan: ne poseduje sva polja
        * neophodna za kreiranje - nedostaje polje lista odgovora (u bar jednom pitanju ankete)
        * */

        List<QuestionCreateDTO> questions = new ArrayList<>();
        // nedostaje lista odgovora za pitanje
        QuestionCreateDTO question = new QuestionCreateDTO("First question", null);
        questions.add(question);

        QuestionnaireCreateDTO questionnaire = new QuestionnaireCreateDTO();
        questionnaire.setQuestions(questions);

        MeetingItemCreateDTO meetingItemCreateDTO = new MeetingItemCreateDTO(
                "First meeting item", "First meeting item content", questionnaire, null);

        String meetingItemDTOJson = TestUtil.json(meetingItemCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/100/billboard/meeting_items")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateMeetingItemExistQuestionWithEmptyAnswerList() throws Exception {
        /*
        * Test proverava ispravnost rada metode createMeetingItem kontrolera BillboardController
        * (kreiranje novog predloga tacke dnevnog reda koji se nalazi na bilbordu zgrade sa zadatim ID-em)
        * u slucaju kada predlog tacke dnevnog reda obuhvata i anketu i pri tom je dolazni DTO objekat nevalidan: poseduje sva polja
        * neophodna za kreiranje - ali je lista odgovora prazna (u bar jednom pitanju ankete)
        * */

        List<QuestionCreateDTO> questions = new ArrayList<>();
        // lista odgovora za pitanje je prazna
        QuestionCreateDTO question = new QuestionCreateDTO("First question", new ArrayList<>());
        questions.add(question);

        QuestionnaireCreateDTO questionnaire = new QuestionnaireCreateDTO();
        questionnaire.setQuestions(questions);

        MeetingItemCreateDTO meetingItemCreateDTO = new MeetingItemCreateDTO(
                "First meeting item", "First meeting item content", questionnaire, null);

        String meetingItemDTOJson = TestUtil.json(meetingItemCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/100/billboard/meeting_items")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateMeetingItemNonexistentDamage() throws Exception {
        /*
        * Test proverava ispravnost rada metode createMeetingItem kontrolera BillboardController
        * (kreiranje novog predloga tacke dnevnog reda koji se nalazi na bilbordu zgrade sa zadatim ID-em)
        * u slucaju kada predlog tacke dnevnog reda referencira kvar ciji je ID zadat u DTO-u i pri tom ne postoji kvar
        * sa zadatim ID-em
        * */

        MeetingItemCreateDTO meetingItemCreateDTO = new MeetingItemCreateDTO();
        meetingItemCreateDTO.setContent("First meeting item content");
        meetingItemCreateDTO.setTitle("First meeting item");
        // ne postoji kvar sa ID-em = 200
        meetingItemCreateDTO.setDamageId(200L);

        String meetingItemDTOJson = TestUtil.json(meetingItemCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/100/billboard/meeting_items")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testCreateMeetingItemDamageFromInvalidBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode createMeetingItem kontrolera BillboardController
        * (kreiranje novog predloga tacke dnevnog reda koji se nalazi na bilbordu zgrade sa zadatim ID-em)
        * u slucaju kada predlog tacke dnevnog reda referencira kvar ciji je ID zadat u DTO-u i pri tom
        * kvar sa zadatim ID-em ne pripada zgradi sa zadatim ID-em
        * */

        MeetingItemCreateDTO meetingItemCreateDTO = new MeetingItemCreateDTO();
        meetingItemCreateDTO.setContent("First meeting item content");
        meetingItemCreateDTO.setTitle("First meeting item");
        // kvar sa ID-em = 105 pripada zgradi sa ID-em = 101 (a ne sa ID-em = 100)
        meetingItemCreateDTO.setDamageId(105L);

        String meetingItemDTOJson = TestUtil.json(meetingItemCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/100/billboard/meeting_items")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateMeetingItemNotResponsiblePersonForDamage() throws Exception {
        /*
        * Test proverava ispravnost rada metode createMeetingItem kontrolera BillboardController
        * (kreiranje novog predloga tacke dnevnog reda koji se nalazi na bilbordu zgrade sa zadatim ID-em)
        * u slucaju kada predlog tacke dnevnog reda referencira kvar ciji je ID zadat u DTO-u i pri tom
        * trenutno ulogovani stanar nije odgovorna osoba za kvar sa zadatim Id-em
        * */

        MeetingItemCreateDTO meetingItemCreateDTO = new MeetingItemCreateDTO();
        meetingItemCreateDTO.setTitle("First meeting item");
        meetingItemCreateDTO.setContent("First meeting item content");
        // kvar sa ID-em = 100 ima odgovornu osobu sa ID-em = 104 (a ne sa ID-em = 101 - trenutno ulogovani stanar)
        meetingItemCreateDTO.setDamageId(100L);

        String meetingItemDTOJson = TestUtil.json(meetingItemCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/100/billboard/meeting_items")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isForbidden());
    }

    // TESTOVI za updateNotification
    @Test
    @Transactional
    @Rollback(true)
    public void testUpdateNotification() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateNotification kontrolera BillboardController
        * (azuriranje konkretnog obavestenja sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * */

        NotificationCreateUpdateDTO notificationUpdateDTO = new NotificationCreateUpdateDTO(
                "Updated content", "Updated title");
        String notificationDTOJson = TestUtil.json(notificationUpdateDTO);

        /*
        obavestenje sa ID-em = 100 je kreirao stanar sa ID-em = 101 i ima sledece podatke:
            id = 100,
            content = First notification,
            building_id = 100,
            creator_id = 101
        */
        this.mockMvc.perform(put(URL_PREFIX + "/100/billboard/notifications/100")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(notificationDTOJson))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(100))
                .andExpect(jsonPath("$.content").value("Updated content"))
                .andExpect(jsonPath("$.title").value("Updated title"))
                .andExpect(jsonPath("$.creator.id").value(101))
                .andExpect(jsonPath("$.creator.firstName").value("Katarina"))
                .andExpect(jsonPath("$.creator.lastName").value("Cukurov"));
    }

    @Test
    public void testUpdateNotificationByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateNotification kontrolera BillboardController
        * (azuriranje konkretnog obavestenja sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        NotificationCreateUpdateDTO notificationUpdateDTO = new NotificationCreateUpdateDTO(
                "Updated content", "Updated title");
        String notificationDTOJson = TestUtil.json(notificationUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/100/billboard/notifications/100")
                .contentType(contentType)
                .content(notificationDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testUpdateNotificationByNotAuthenticatedTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateNotification kontrolera BillboardController
        * (azuriranje konkretnog obavestenja sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu stanara
        * */

        NotificationCreateUpdateDTO notificationUpdateDTO = new NotificationCreateUpdateDTO(
                "Updated content", "Updated title");
        String notificationDTOJson = TestUtil.json(notificationUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/100/billboard/notifications/100")
                .header("Authentication-Token", this.accessTokenFirm)
                .contentType(contentType)
                .content(notificationDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testUpdateNotificationNonexistentBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateNotification kontrolera BillboardController
        * (azuriranje konkretnog obavestenja sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * u slucaju kada ne postoji zgrada sa zadatim ID-em
        * */

        NotificationCreateUpdateDTO notificationUpdateDTO = new NotificationCreateUpdateDTO(
                "Updated content", "Updated title");
        String notificationDTOJson = TestUtil.json(notificationUpdateDTO);

        // ne postoji zgrada sa ID-em = 200
        this.mockMvc.perform(put(URL_PREFIX + "/200/billboard/notifications/100")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(notificationDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testUpdateNotificationTenantWithoutApartment() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateNotification kontrolera BillboardController
        * (azuriranje konkretnog obavestenja sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * u slucaju kada trenutno ulogovani stanar nije povezan ni sa jednim stanom
        * */

        NotificationCreateUpdateDTO notificationUpdateDTO = new NotificationCreateUpdateDTO(
                "Updated content", "Updated title");
        String notificationDTOJson = TestUtil.json(notificationUpdateDTO);

        // stanar sa ID-em = 108 nije povezan ni sa jednim stanom
        this.mockMvc.perform(put(URL_PREFIX + "/100/billboard/notifications/100")
                .header("Authentication-Token", this.accessTokenTenantWithoutApartment)
                .contentType(contentType)
                .content(notificationDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testUpdateNotificationTenantFromInvalidBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateNotification kontrolera BillboardController
        * (azuriranje konkretnog obavestenja sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * u slucaju kada trenutno ulogovani stanar ne zivi u zgradi sa zadatim ID-em
        * */

        NotificationCreateUpdateDTO notificationUpdateDTO = new NotificationCreateUpdateDTO(
                "Updated content", "Updated title");
        String notificationDTOJson = TestUtil.json(notificationUpdateDTO);

        // stanar sa ID-em = 101 zivi u zgradi sa ID-em = 100 (a ne sa ID-em = 101)
        this.mockMvc.perform(put(URL_PREFIX + "/101/billboard/notifications/100")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(notificationDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testUpdateNotificationNonexistentNotification() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateNotification kontrolera BillboardController
        * (azuriranje konkretnog obavestenja sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * u slucaju kada ne postoji obavestenje sa zadatim ID-em
        * */

        NotificationCreateUpdateDTO notificationUpdateDTO = new NotificationCreateUpdateDTO(
                "Updated content", "Updated title");
        String notificationDTOJson = TestUtil.json(notificationUpdateDTO);

        // ne postoji obavestenje sa ID-em = 200
        this.mockMvc.perform(put(URL_PREFIX + "/100/billboard/notifications/200")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(notificationDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testUpdateNotificationTenantIsNotCreator() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateNotification kontrolera BillboardController
        * (azuriranje konkretnog obavestenja sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * u slucaju kada trenutno ulogovani stanar nije kreator obavestenja sa zadatim ID-em
        * */

        NotificationCreateUpdateDTO notificationUpdateDTO = new NotificationCreateUpdateDTO(
                "Updated content", "Updated title");
        String notificationDTOJson = TestUtil.json(notificationUpdateDTO);

        // stanar sa ID-em = 103 nije kreator obavestenja sa ID-em = 100
        this.mockMvc.perform(put(URL_PREFIX + "/100/billboard/notifications/100")
                .header("Authentication-Token", this.accessTokentTenant_2)
                .contentType(contentType)
                .content(notificationDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testUpdateNotificationInvalidBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateNotification kontrolera BillboardController
        * (azuriranje konkretnog obavestenja sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * u slucaju kada obavestenje sa zadatim ID-em ne pripada zgradi sa zadatim ID-em
        * */

        NotificationCreateUpdateDTO notificationUpdateDTO = new NotificationCreateUpdateDTO(
                "Updated content", "Updated title");
        String notificationDTOJson = TestUtil.json(notificationUpdateDTO);

        // obavestenje sa ID-em = 102 pripada zgradi sa ID-em = 101 (a ne sa ID-em = 100)
        this.mockMvc.perform(put(URL_PREFIX + "/100/billboard/notifications/102")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(notificationDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateNotificationMissingContent() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateNotification kontrolera BillboardController
        * (azuriranje konkretnog obavestenja sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne sadrzi sva polja neophodna za azuriranje - nedostaje polje content
        * */

        NotificationCreateUpdateDTO notificationUpdateDTO = new NotificationCreateUpdateDTO();
        // nedostaje vrednost polja content
        notificationUpdateDTO.setContent(null);
        notificationUpdateDTO.setTitle("Updated title");

        String notificationDTOJson = TestUtil.json(notificationUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/100/billboard/notifications/100")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(notificationDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateNotificationInappropriateTypeOdConent() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateNotification kontrolera BillboardController
        * (azuriranje konkretnog obavestenja sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * u slucaju kada je dolazni DTO objekat nevalidan: sadrzi sva polja neohodna za azuriranje, ali je polje content nevalidnog tipa
        * */

        // nevalidan tip polja content
        String notificationDTOJson = "{content:3.45;" +
                "title:'Updated title}";

        this.mockMvc.perform(put(URL_PREFIX + "/100/billboard/notifications/100")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(notificationDTOJson))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za updateMeetingItem
    @Test
    @Transactional
    @Rollback(true)
    public void testUpdateMeetingItem() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateMeetingItem kontrolera BillboardController
        * (azuriranje konkretnog predloga tacke dnevnog reda sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * */

        // izmena postojeceg pitanja
        List<String> answersChanged = new ArrayList<>();
        answersChanged.add("Yes");
        answersChanged.add("No");
        QuestionUpdateDTO questionUpdateDTO = new QuestionUpdateDTO(103L, "New content of existing question", answersChanged);

        // kreiranje novog pitanja
        List<String> answersNew = new ArrayList<>();
        answersNew.add("First answer");
        answersNew.add("Second answer");
        QuestionUpdateDTO questionUpdateDTONew = new QuestionUpdateDTO(-1L, "New question", answersNew);

        List<QuestionUpdateDTO> questionUpdateDTOS = new ArrayList<>();
        questionUpdateDTOS.add(questionUpdateDTO);
        questionUpdateDTOS.add(questionUpdateDTONew);

        QuestionnaireUpdateDTO questionnaireUpdateDTO = new QuestionnaireUpdateDTO(102L, questionUpdateDTOS);

        MeetingItemUpdateDTO meetingItemUpdateDTO = new MeetingItemUpdateDTO(
                "New title","New content", questionnaireUpdateDTO, null);
        String meetingItemDTOJson = TestUtil.json(meetingItemUpdateDTO);

        /*
        predlog tacke dnevnog reda sa ID-em = 102 ima sledece podatke:
            id = 102,
            content = Third meeting item,
            building_id = 100,
            creator_id = 102,
            questionnaire: id = 102,
                           questions: 1 - id = 103,
                                          content = Some question,
                                          answers = 'Maybe', 'Sure'
        */
        this.mockMvc.perform(put(URL_PREFIX + "/100/billboard/meeting_items/102")
                .header("Authentication-Token", this.accessTokentTenant_3)
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(102))
                .andExpect(jsonPath("$.content").value("New content"))
                .andExpect(jsonPath("$.title").value("New title"))
                .andExpect(jsonPath("$.creator.id").value(102))
                .andExpect(jsonPath("$.creator.firstName").value("Nikola"))
                .andExpect(jsonPath("$.creator.lastName").value("Garabandic"))
                .andExpect(jsonPath("$.questionnaire.questions.[*]", hasSize(2)))
                .andExpect(jsonPath("$.questionnaire.questions.[*].content", containsInAnyOrder("New content of existing question",
                        "New question")))
                .andExpect(jsonPath("$.questionnaire.questions.[*].answers.[*]", containsInAnyOrder("Yes", "No",
                        "First answer", "Second answer")));
    }

    @Test
    public void testUpdateMeetingItemByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateMeetingItem kontrolera BillboardController
        * (azuriranje konkretnog predloga tacke dnevnog reda sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        MeetingItemUpdateDTO meetingItemUpdateDTO = new MeetingItemUpdateDTO(
                "New title","New content", null, null);
        String meetingItemDTOJson = TestUtil.json(meetingItemUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/100/billboard/meeting_items/102")
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testUpdateMeetingItemByNotAuthenticatedTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateMeetingItem kontrolera BillboardController
        * (azuriranje konkretnog predloga tacke dnevnog reda sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu stanara
        * */

        MeetingItemUpdateDTO meetingItemUpdateDTO = new MeetingItemUpdateDTO(
                "New title","New content", null, null);
        String meetingItemDTOJson = TestUtil.json(meetingItemUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/100/billboard/meeting_items/102")
                .header("Authentication-Token", this.accessTokenFirm)
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testUpdateMeetingItemNonexistentBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateMeetingItem kontrolera BillboardController
        * (azuriranje konkretnog predloga tacke dnevnog reda sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * u slucaju kada ne postoji zgrada sa zadatim ID-em
        * */

        MeetingItemUpdateDTO meetingItemUpdateDTO = new MeetingItemUpdateDTO(
                "New title", "New content", null, null);
        String meetingItemDTOJson = TestUtil.json(meetingItemUpdateDTO);

        // ne postoji zgrada sa ID-em = 200
        this.mockMvc.perform(put(URL_PREFIX + "/200/billboard/meeting_items/102")
                .header("Authentication-Token", this.accessTokentTenant_3)
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testUpdateMeetingItemTenantWithoutApartment() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateMeetingItem kontrolera BillboardController
        * (azuriranje konkretnog predloga tacke dnevnog reda sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * u slucaju kada trenutno ulogovani stanar nije povezan ni sa jednim stanom
        * */

        MeetingItemUpdateDTO meetingItemUpdateDTO = new MeetingItemUpdateDTO(
                "New title", "New content", null, null);
        String meetingItemDTOJson = TestUtil.json(meetingItemUpdateDTO);

        // stanar sa ID-em = 108 nije povezan ni sa jednim stanom
        this.mockMvc.perform(put(URL_PREFIX + "/100/billboard/meeting_items/102")
                .header("Authentication-Token", this.accessTokenTenantWithoutApartment)
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testUpdateMeetingItemTenantFromInvalidBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateMeetingItem kontrolera BillboardController
        * (azuriranje konkretnog predloga tacke dnevnog reda sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * u slucaju kada trenutno ulogovani stanar ne zivi u zgradi sa zadatim ID-em
        * */

        MeetingItemUpdateDTO meetingItemUpdateDTO = new MeetingItemUpdateDTO(
                "New title","New content", null, null);
        String meetingItemDTOJson = TestUtil.json(meetingItemUpdateDTO);

        // stanar sa ID-em = 101 zivi u zgradi sa ID-em = 100 (a ne sa ID-em = 101)
        this.mockMvc.perform(put(URL_PREFIX + "/101/billboard/meeting_items/102")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testUpdateMeetingItemNonexistentMeetingItem() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateMeetingItem kontrolera BillboardController
        * (azuriranje konkretnog predloga tacke dnevnog reda sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * u slucaju kada ne postoji predlog tacke dnevnog reda sa zadatim ID-em
        * */

        MeetingItemUpdateDTO meetingItemUpdateDTO = new MeetingItemUpdateDTO(
                "New title", "New content", null, null);
        String meetingItemDTOJson = TestUtil.json(meetingItemUpdateDTO);

        // ne postoji predlog tacke dnevnog reda sa ID-em = 200
        this.mockMvc.perform(put(URL_PREFIX + "/100/billboard/meeting_items/200")
                .header("Authentication-Token", this.accessTokentTenant_3)
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testUpdateMeetingItemTenantIsNotCreator() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateMeetingItem kontrolera BillboardController
        * (azuriranje konkretnog predloga tacke dnevnog reda sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * u slucaju kada trenutno ulogovani stanar nije kreator predloga tacke dnevnog reda sa zadatim ID-em
        * */

        MeetingItemUpdateDTO meetingItemUpdateDTO = new MeetingItemUpdateDTO(
                "New title", "New content", null, null);
        String meetingItemDTOJson = TestUtil.json(meetingItemUpdateDTO);

        // stanar sa ID-em = 101 nije kreator predloga tacke dnevnog reda sa ID-em = 102
        this.mockMvc.perform(put(URL_PREFIX + "/100/billboard/meeting_items/102")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testUpdateMeetingItemAlreadyConnectWithMeeting() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateMeetingItem kontrolera BillboardController
        * (azuriranje konkretnog predloga tacke dnevnog reda sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * u slucaju kada je predlog tacke dnevnog reda sa zadatim ID-em vec povezan (usvojen kao stavka) sa nekim sastankom skupstine stanara
        * */

        MeetingItemUpdateDTO meetingItemUpdateDTO = new MeetingItemUpdateDTO(
                "New title", "New content", null, null);
        String meetingItemDTOJson = TestUtil.json(meetingItemUpdateDTO);

        // predlog tacke dnevnog reda sa ID-em = 101 je vec povezan sa sastankom sa ID-em = 100
        this.mockMvc.perform(put(URL_PREFIX + "/100/billboard/meeting_items/101")
                .header("Authentication-Token", this.accessTokentTenant_3)
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateMeetingItemInvalidBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateMeetingItem kontrolera BillboardController
        * (azuriranje konkretnog predloga tacke dnevnog reda sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * u slucaju kada predlog tacke dnevnog reda sa zadatim ID-em ne pripada zgradi sa zadatim ID-em
        * */

        MeetingItemUpdateDTO meetingItemUpdateDTO = new MeetingItemUpdateDTO(
                "New title", "New content", null, null);
        String meetingItemDTOJson = TestUtil.json(meetingItemUpdateDTO);

        // predlog tacke dnevnog reda sa ID-em = 103 pripada zgradi sa ID-em = 101 (a ne sa ID-em = 100)
        this.mockMvc.perform(put(URL_PREFIX + "/100/billboard/meeting_items/103")
                .header("Authentication-Token", this.accessTokentTenant_3)
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateMeetingItemMissingContent() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateMeetingItem kontrolera BillboardController
        * (azuriranje konkretnog predloga tacke dnevnog reda sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne sadrzi sva polja neophodna za azuriranje - nedostaje polje content
        * */

        MeetingItemUpdateDTO meetingItemUpdateDTO = new MeetingItemUpdateDTO();
        meetingItemUpdateDTO.setQuestionnaireUpdate(null);
        // nedostaje vrednost polja content
        meetingItemUpdateDTO.setContent(null);
        meetingItemUpdateDTO.setTitle("New title");

        String meetingItemDTOJson = TestUtil.json(meetingItemUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/100/billboard/meeting_items/102")
                .header("Authentication-Token", this.accessTokentTenant_3)
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateMeetingItemInappropriateTypeOfConent() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateMeetingItem kontrolera BillboardController
        * (azuriranje konkretnog predloga tacke dnevnog reda sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * u slucaju kada je dolazni DTO objekat nevalidan: sadrzi sva polja neohodna za azuriranje, ali je polje content nevalidnog tipa
        * */

        // nevalidan tip polja content
        String meetingItemDTOJson = "{content:3.45; title:'New title'}";

        this.mockMvc.perform(put(URL_PREFIX + "/100/billboard/meeting_items/102")
                .header("Authentication-Token", this.accessTokentTenant_3)
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateMeetingItemMissingQuestionnaireId() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateMeetingItem kontrolera BillboardController
        * (azuriranje konkretnog predloga tacke dnevnog reda sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne sadrzi sva polja neophodna za azuriranje - nedostaje polje id u anketi
        * koja pripada/se kreira u postojecem predlogu tacke dnevnog reda
        * */

        // izmena postojeceg pitanja
        List<String> answersChanged = new ArrayList<>();
        answersChanged.add("Yes");
        answersChanged.add("No");
        QuestionUpdateDTO questionUpdateDTO = new QuestionUpdateDTO(103L, "New content of existing question", answersChanged);

        // kreiranje novog pitanja
        List<String> answersNew = new ArrayList<>();
        answersNew.add("First answer");
        answersNew.add("Second answer");
        QuestionUpdateDTO questionUpdateDTONew = new QuestionUpdateDTO(-1L, "New question", answersNew);

        List<QuestionUpdateDTO> questionUpdateDTOS = new ArrayList<>();
        questionUpdateDTOS.add(questionUpdateDTO);
        questionUpdateDTOS.add(questionUpdateDTONew);

        QuestionnaireUpdateDTO questionnaireUpdateDTO = new QuestionnaireUpdateDTO();
        // nedostaje vrednost polja id
        questionnaireUpdateDTO.setId(null);
        questionnaireUpdateDTO.setQuestions(questionUpdateDTOS);

        MeetingItemUpdateDTO meetingItemUpdateDTO = new MeetingItemUpdateDTO(
                "New title", "New content", questionnaireUpdateDTO, null);
        String meetingItemDTOJson = TestUtil.json(meetingItemUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/100/billboard/meeting_items/102")
                .header("Authentication-Token", this.accessTokentTenant_3)
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateMeetingItemMissingQuestionnaireListWithQuestions() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateMeetingItem kontrolera BillboardController
        * (azuriranje konkretnog predloga tacke dnevnog reda sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne sadrzi sva polja neophodna za azuriranje - nedostaje lista pitanja u anketi
        * koja pripada/se kreira u postojecem predlogu tacke dnevnog reda
        * */

        // nedostaje lista pitanja u anketi
        QuestionnaireUpdateDTO questionnaireUpdateDTO = new QuestionnaireUpdateDTO(102L, null);

        MeetingItemUpdateDTO meetingItemUpdateDTO = new MeetingItemUpdateDTO(
                "New title", "New content", questionnaireUpdateDTO, null);
        String meetingItemDTOJson = TestUtil.json(meetingItemUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/100/billboard/meeting_items/102")
                .header("Authentication-Token", this.accessTokentTenant_3)
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateMeetingItemQuestionnaireListWithQuestionsIsEmpty() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateMeetingItem kontrolera BillboardController
        * (azuriranje konkretnog predloga tacke dnevnog reda sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * u slucaju kada je dolazni DTO objekat nevalidan: sadrzi sva polja neophodna za azuriranje, ali je lista pitanja u anketi
        * koja pripada/se kreira u postojecem predlogu tacke dnevnog reda prazna
        * */

        // lista pitanja u anketi je prazna
        QuestionnaireUpdateDTO questionnaireUpdateDTO = new QuestionnaireUpdateDTO(102L, new ArrayList<>());

        MeetingItemUpdateDTO meetingItemUpdateDTO = new MeetingItemUpdateDTO(
                "New title", "New content", questionnaireUpdateDTO, null);
        String meetingItemDTOJson = TestUtil.json(meetingItemUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/100/billboard/meeting_items/102")
                .header("Authentication-Token", this.accessTokentTenant_3)
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateMeetingItemQuestionnaireExistsQuestionWithoutId() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateMeetingItem kontrolera BillboardController
        * (azuriranje konkretnog predloga tacke dnevnog reda sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne sadrzi sva polja neophodna za azuriranje - u listi pitanja u anketi
        * koja pripada/se kreira u postojecem predlogu tacke dnevnog reda postoji bar jedno pitanje koje nema polje ID
        * */

        // izmena postojeceg pitanja
        List<String> answersChanged = new ArrayList<>();
        answersChanged.add("Yes");
        answersChanged.add("No");
        QuestionUpdateDTO questionUpdateDTO = new QuestionUpdateDTO();
        questionUpdateDTO.setAnswers(answersChanged);
        questionUpdateDTO.setContent("New content of existing question");
        // nedostaje vrednost polja id
        questionUpdateDTO.setId(null);

        // kreiranje novog pitanja
        List<String> answersNew = new ArrayList<>();
        answersNew.add("First answer");
        answersNew.add("Second answer");
        QuestionUpdateDTO questionUpdateDTONew = new QuestionUpdateDTO(-1L, "New question", answersNew);

        List<QuestionUpdateDTO> questionUpdateDTOS = new ArrayList<>();
        questionUpdateDTOS.add(questionUpdateDTO);
        questionUpdateDTOS.add(questionUpdateDTONew);

        QuestionnaireUpdateDTO questionnaireUpdateDTO = new QuestionnaireUpdateDTO(102L, questionUpdateDTOS);

        MeetingItemUpdateDTO meetingItemUpdateDTO = new MeetingItemUpdateDTO(
                "New title", "New content", questionnaireUpdateDTO, null);
        String meetingItemDTOJson = TestUtil.json(meetingItemUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/100/billboard/meeting_items/102")
                .header("Authentication-Token", this.accessTokentTenant_3)
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateMeetingItemQuestionnaireExistsQuestionWithoutContent() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateMeetingItem kontrolera BillboardController
        * (azuriranje konkretnog predloga tacke dnevnog reda sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne sadrzi sva polja neophodna za azuriranje - u listi pitanja u anketi
        * koja pripada/se kreira u postojecem predlogu tacke dnevnog reda postoji bar jedno pitanje koje nema polje content
        * */

        // izmena postojeceg pitanja
        List<String> answersChanged = new ArrayList<>();
        answersChanged.add("Yes");
        answersChanged.add("No");
        QuestionUpdateDTO questionUpdateDTO = new QuestionUpdateDTO();
        questionUpdateDTO.setAnswers(answersChanged);
        // nedostaje vrednost polja content
        questionUpdateDTO.setContent(null);
        questionUpdateDTO.setId(103L);

        // kreiranje novog pitanja
        List<String> answersNew = new ArrayList<>();
        answersNew.add("First answer");
        answersNew.add("Second answer");
        QuestionUpdateDTO questionUpdateDTONew = new QuestionUpdateDTO(-1L, "New question", answersNew);

        List<QuestionUpdateDTO> questionUpdateDTOS = new ArrayList<>();
        questionUpdateDTOS.add(questionUpdateDTO);
        questionUpdateDTOS.add(questionUpdateDTONew);

        QuestionnaireUpdateDTO questionnaireUpdateDTO = new QuestionnaireUpdateDTO(102L, questionUpdateDTOS);

        MeetingItemUpdateDTO meetingItemUpdateDTO = new MeetingItemUpdateDTO(
                "New title", "New content", questionnaireUpdateDTO, null);
        String meetingItemDTOJson = TestUtil.json(meetingItemUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/100/billboard/meeting_items/102")
                .header("Authentication-Token", this.accessTokentTenant_3)
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateMeetingItemQuestionnaireExistsQuestionWithoutAnswerList() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateMeetingItem kontrolera BillboardController
        * (azuriranje konkretnog predloga tacke dnevnog reda sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne sadrzi sva polja neophodna za azuriranje - u listi pitanja u anketi
        * koja pripada/se kreira u postojecem predlogu tacke dnevnog reda postoji bar jedno pitanje koje nema listu odgovora
        * */

        // izmena postojeceg pitanja
        QuestionUpdateDTO questionUpdateDTO = new QuestionUpdateDTO();
        // nedostaje lista odgovora na pitanje
        questionUpdateDTO.setAnswers(null);
        questionUpdateDTO.setContent("New content of existing question");
        questionUpdateDTO.setId(103L);

        // kreiranje novog pitanja
        List<String> answersNew = new ArrayList<>();
        answersNew.add("First answer");
        answersNew.add("Second answer");
        QuestionUpdateDTO questionUpdateDTONew = new QuestionUpdateDTO(-1L, "New question", answersNew);

        List<QuestionUpdateDTO> questionUpdateDTOS = new ArrayList<>();
        questionUpdateDTOS.add(questionUpdateDTO);
        questionUpdateDTOS.add(questionUpdateDTONew);

        QuestionnaireUpdateDTO questionnaireUpdateDTO = new QuestionnaireUpdateDTO(102L, questionUpdateDTOS);

        MeetingItemUpdateDTO meetingItemUpdateDTO = new MeetingItemUpdateDTO(
                "New title", "New content", questionnaireUpdateDTO, null);
        String meetingItemDTOJson = TestUtil.json(meetingItemUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/100/billboard/meeting_items/102")
                .header("Authentication-Token", this.accessTokentTenant_3)
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateMeetingItemQuestionnaireExistsQuestionWithEmptyAnswerList() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateMeetingItem kontrolera BillboardController
        * (azuriranje konkretnog predloga tacke dnevnog reda sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * u slucaju kada je dolazni DTO objekat nevalidan: sadrzi sva polja neophodna za azuriranje, ali u listi pitanja u anketi
        * koja pripada/se kreira u postojecem predlogu tacke dnevnog reda postoji bar jedno pitanje cija je lista odgovora prazna
        * */

        // izmena postojeceg pitanja
        QuestionUpdateDTO questionUpdateDTO = new QuestionUpdateDTO();
        // lista odgovora na pitanje je prazna
        questionUpdateDTO.setAnswers(new ArrayList<>());
        questionUpdateDTO.setContent("New content of existing question");
        questionUpdateDTO.setId(103L);

        // kreiranje novog pitanja
        List<String> answersNew = new ArrayList<>();
        answersNew.add("First answer");
        answersNew.add("Second answer");
        QuestionUpdateDTO questionUpdateDTONew = new QuestionUpdateDTO(-1L, "New question", answersNew);

        List<QuestionUpdateDTO> questionUpdateDTOS = new ArrayList<>();
        questionUpdateDTOS.add(questionUpdateDTO);
        questionUpdateDTOS.add(questionUpdateDTONew);

        QuestionnaireUpdateDTO questionnaireUpdateDTO = new QuestionnaireUpdateDTO(102L, questionUpdateDTOS);

        MeetingItemUpdateDTO meetingItemUpdateDTO = new MeetingItemUpdateDTO(
                "New title", "New content", questionnaireUpdateDTO, null);
        String meetingItemDTOJson = TestUtil.json(meetingItemUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/100/billboard/meeting_items/102")
                .header("Authentication-Token", this.accessTokentTenant_3)
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateMeetingItemNonexistentDamage() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateMeetingItem kontrolera BillboardController
        * (azuriranje konkretnog predloga tacke dnevnog reda sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * u slucaju kada dolazni DTO objekat sadrzi polje damageId i pri tom ne postoji kvar sa zadatim ID-em
        * */

        // ne postoji kvar sa ID-em = 200
        MeetingItemUpdateDTO meetingItemUpdateDTO = new MeetingItemUpdateDTO(
                "New title", "New content", null, 200L);

        String meetingItemDTOJson = TestUtil.json(meetingItemUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/100/billboard/meeting_items/102")
                .header("Authentication-Token", this.accessTokentTenant_3)
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testUpdateMeetingItemDamageFromInvalidBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateMeetingItem kontrolera BillboardController
        * (azuriranje konkretnog predloga tacke dnevnog reda sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * u slucaju kada dolazni DTO objekat sadrzi polje damageId i pri tom kvar sa zadatim ID-em ne pripada zgradi sa zadatim ID-em
        * */

        // kvar sa ID-em = 105 pripada zgradi sa ID-em = 101 (a ne sa ID-em = 100)
        MeetingItemUpdateDTO meetingItemUpdateDTO = new MeetingItemUpdateDTO(
                "New title", "New content", null, 105L);

        String meetingItemDTOJson = TestUtil.json(meetingItemUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/100/billboard/meeting_items/102")
                .header("Authentication-Token", this.accessTokentTenant_3)
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateMeetingItemNotResponsiblePersonForDamage() throws Exception {
        /*
        * Test proverava ispravnost rada metode updateMeetingItem kontrolera BillboardController
        * (azuriranje konkretnog predloga tacke dnevnog reda sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * u slucaju kada dolazni DTO objekat sadrzi polje damageId i pri tom trenutno ulogovani stanar nije
        * odgovorna osoba za kvar sa zadatim ID-em
        * */

        // stanar sa ID-em = 104 je odgovrna osoba za kvar sa ID-em = 100 (a ne trenutno logovani stanar)
        MeetingItemUpdateDTO meetingItemUpdateDTO = new MeetingItemUpdateDTO(
                "New title", "New content", null, 100L);

        String meetingItemDTOJson = TestUtil.json(meetingItemUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/100/billboard/meeting_items/102")
                .header("Authentication-Token", this.accessTokentTenant_3)
                .contentType(contentType)
                .content(meetingItemDTOJson))
                .andExpect(status().isForbidden());
    }

    // TESTOVI za deleteNotification
    @Test
    @Transactional
    @Rollback(true)
    public void testDeleteNotification() throws Exception {
        /*
        * Test proverava ispravnost rada metode deleteNotification kontrolera BillboardController
        * (brisanje konkretnog obavestenja sa zadatim ID-em u zgradi sa zadatim ID-em)
        * */

        /*
        obavestenje sa ID-em = 100 je kreirao stanar sa ID-em = 101 i ima sledece podatke:
            id = 100,
            content = First notification,
            building_id = 100,
            creator_id = 101
        */
        this.mockMvc.perform(delete(URL_PREFIX + "/100/billboard/notifications/100")
                .header("Authentication-Token", this.accessTokenTenant))
                .andExpect(status().isOk());

        // nakon brisanja, ne postoji trazeni resurs u bazi
        this.mockMvc.perform(get(URL_PREFIX + "/100/billboard/notifications/100")
                .header("Authentication-Token", this.accessTokenTenant))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeleteNotificationByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode deleteNotification kontrolera BillboardController
        * (brisanje konkretnog obavestenja sa zadatim ID-em u zgradi sa zadatim ID-em)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        this.mockMvc.perform(delete(URL_PREFIX + "/100/billboard/notifications/100"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDeleteNotificationByNotAuthenticatedTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode deleteNotification kontrolera BillboardController
        * (brisanje konkretnog obavestenja sa zadatim ID-em u zgradi sa zadatim ID-em)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu stanara
        * */

        this.mockMvc.perform(delete(URL_PREFIX + "/100/billboard/notifications/100")
                .header("Authentication-Token", this.accessTokenFirm))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDeleteNotificationNonexistentBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode deleteNotification kontrolera BillboardController
        * (brisanje konkretnog obavestenja sa zadatim ID-em u zgradi sa zadatim ID-em)
        * u slucaju kada ne postoji zgrada sa zadatim ID-em
        * */

        // ne postoji zgrada sa ID-em = 200
        this.mockMvc.perform(delete(URL_PREFIX + "/200/billboard/notifications/100")
                .header("Authentication-Token", this.accessTokenTenant))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeleteNotificationTenantWithoutApartment() throws Exception {
        /*
        * Test proverava ispravnost rada metode deleteNotification kontrolera BillboardController
        * (brisanje konkretnog obavestenja sa zadatim ID-em u zgradi sa zadatim ID-em)
        * u slucaju kada trenutno ulogovani stanar nije povezan ni sa jednim stanom
        * */

        // stanar sa ID-em = 108 nije povezan ni sa jednim stanom
        this.mockMvc.perform(delete(URL_PREFIX + "/100/billboard/notifications/100")
                .header("Authentication-Token", this.accessTokenTenantWithoutApartment))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDeleteNotificationTenantFromInvalidBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode deleteNotification kontrolera BillboardController
        * (brisanje konkretnog obavestenja sa zadatim ID-em u zgradi sa zadatim ID-em)
        * u slucaju kada trenutno ulogovani stanar ne zivi u zgradi sa zadatim ID-em
        * */

        // stanar sa ID-em = 103 zivi u zgradi sa ID-em = 100 (a ne sa ID-em = 101)
        this.mockMvc.perform(delete(URL_PREFIX + "/101/billboard/notifications/100")
                .header("Authentication-Token", this.accessTokentTenant_2))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDeleteNotificationNonexistentNotification() throws Exception {
        /*
        * Test proverava ispravnost rada metode deleteNotification kontrolera BillboardController
        * (brisanje konkretnog obavestenja sa zadatim ID-em u zgradi sa zadatim ID-em)
        * u slucaju kada ne postoji obavestenje sa zadatim ID-em
        * */

        // ne postoji obavestenje sa ID-em = 200
        this.mockMvc.perform(delete(URL_PREFIX + "/100/billboard/notifications/200")
                .header("Authentication-Token", this.accessTokenTenant))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeleteNotificationInvalidBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode deleteNotification kontrolera BillboardController
        * (brisanje konkretnog obavestenja sa zadatim ID-em u zgradi sa zadatim ID-em)
        * u slucaju kada obavestenje sa zadatim ID-em ne pripada zgradi sa zadatim ID-em
        * */

        // obavestenje sa ID-em = 102 pripada zgradi sa ID-em = 101 (a ne sa ID-em = 100)
        this.mockMvc.perform(delete(URL_PREFIX + "/100/billboard/notifications/102")
                .header("Authentication-Token", this.accessTokenTenant))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testDeleteNotificationTenantIsNotCreator() throws Exception {
        /*
        * Test proverava ispravnost rada metode deleteNotification kontrolera BillboardController
        * (brisanje konkretnog obavestenja sa zadatim ID-em u zgradi sa zadatim ID-em)
        * u slucaju kada trenutno ulogovani stanar nije kreator obavestenja sa zadatim ID-em
        * */

        // stanar sa ID-em = 103 nije kreator obavestenja sa ID-em = 100
        this.mockMvc.perform(delete(URL_PREFIX + "/100/billboard/notifications/100")
                .header("Authentication-Token", this.accessTokentTenant_2))
                .andExpect(status().isForbidden());
    }

    // TESTOVI za deleteMeetingItem
    @Test
    @Transactional
    @Rollback(true)
    public void testDeleteMeetingItem() throws Exception {
        /*
        * Test proverava ispravnost rada metode deleteMeetingItem kontrolera BillboardController
        * (brisanje konkretnog predloga tacke dnevnog reda sa zadatim ID-em u zgradi sa zadatim ID-em)
        * */

        /*
        predlog tacke dnevnog reda sa ID-em = 102 je kreirao stanar sa ID-em = 102 i ima sledece podatke:
            id = 102,
            content = Third meeting item,
            building_id = 100,
            creator_id = 102,
            questionnaire: id = 102,
                           questions: 1 - id = 103,
                                          content = Some question
                                          answers = 'Maybe', 'Sure'
        */
        this.mockMvc.perform(delete(URL_PREFIX + "/100/billboard/meeting_items/102")
                .header("Authentication-Token", this.accessTokentTenant_3))
                .andExpect(status().isOk());
        // nakon brisanja, ne postoji trazeni resurs u bazi
        this.mockMvc.perform(get(URL_PREFIX + "/100/billboard/meeting_items/102")
                .header("Authentication-Token", this.accessTokentTenant_3))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeleteMeetingItemByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode deleteMeetingItem kontrolera BillboardController
        * (brisanje konkretnog predloga tacke dnevnog reda sa zadatim ID-em u zgradi sa zadatim ID-em)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        this.mockMvc.perform(delete(URL_PREFIX + "/100/billboard/meeting_items/102"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDeleteMeetingItemByNotAuthenticatedTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode deleteMeetingItem kontrolera BillboardController
        * (brisanje konkretnog predloga tacke dnevnog reda sa zadatim ID-em u zgradi sa zadatim ID-em)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu stanara
        * */

        this.mockMvc.perform(delete(URL_PREFIX + "/100/billboard/meeting_items/102")
                .header("Authentication-Token", this.accessTokenFirm))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDeleteMeetingItemNonexistentBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode deleteMeetingItem kontrolera BillboardController
        * (brisanje konkretnog predloga tacke dnevnog reda sa zadatim ID-em u zgradi sa zadatim ID-em)
        * u slucaju kada ne postoji zgrada sa zadatim ID-em
        * */

        // ne postoji zgrada sa ID-em = 200
        this.mockMvc.perform(delete(URL_PREFIX + "/200/billboard/meeting_items/102")
                .header("Authentication-Token", this.accessTokentTenant_3))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeleteMeetingItemTenantWithoutApartment() throws Exception {
        /*
        * Test proverava ispravnost rada metode deleteMeetingItem kontrolera BillboardController
        * (brisanje konkretnog predloga tacke dnevnog reda sa zadatim ID-em u zgradi sa zadatim ID-em)
        * u slucaju kada trenutno ulogovani stanar nije povezan ni sa jednim stanom
        * */

        // stanar sa ID-em = 108 nije povezan ni sa jednim stanom
        this.mockMvc.perform(delete(URL_PREFIX + "/100/billboard/meeting_items/102")
                .header("Authentication-Token", this.accessTokenTenantWithoutApartment))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDeleteMeetingItemTenantFromInvalidBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode deleteMeetingItem kontrolera BillboardController
        * (brisanje konkretnog predloga tacke dnevnog reda sa zadatim ID-em u zgradi sa zadatim ID-em)
        * u slucaju kada trenutno ulogovani stanar ne zivi u zgradi sa zadatim ID-em
        * */

        // stanar sa ID-em = 103 zivi u zgradi sa ID-em = 100 (a ne sa ID-em = 101)
        this.mockMvc.perform(delete(URL_PREFIX + "/101/billboard/meeting_items/102")
                .header("Authentication-Token", this.accessTokentTenant_2))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDeleteMeetingItemNonexistentMeetingItem() throws Exception {
        /*
        * Test proverava ispravnost rada metode deleteMeetingItem kontrolera BillboardController
        * (brisanje konkretnog predloga tacke dnevnog reda sa zadatim ID-em u zgradi sa zadatim ID-em)
        * u slucaju kada ne postoji predlog tacke dnevnog reda sa zadatim ID-em
        * */

        // ne postoji predlog tacke dnevnog reda sa ID-em = 200
        this.mockMvc.perform(delete(URL_PREFIX + "/100/billboard/meeting_items/200")
                .header("Authentication-Token", this.accessTokentTenant_3))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeleteMeetingItemInvalidBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode deleteMeetingItem kontrolera BillboardController
        * (brisanje konkretnog predloga tacke dnevnog reda sa zadatim ID-em u zgradi sa zadatim ID-em)
        * u slucaju kada predlog tacke dnevnog reda sa zadatim ID-em ne pripada zgradi sa zadatim ID-em
        * */

        // predlog tacke dnevnog reda sa ID-em = 103 pripada zgradi sa ID-em = 101 (a ne sa ID-em = 100)
        this.mockMvc.perform(delete(URL_PREFIX + "/100/billboard/meeting_items/103")
                .header("Authentication-Token", this.accessTokentTenant_3))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testDeleteMeetingItemAlreadyInProcessing() throws Exception {
        /*
        * Test proverava ispravnost rada metode deleteMeetingItem kontrolera BillboardController
        * (brisanje konkretnog predloga tacke dnevnog reda sa zadatim ID-em u zgradi sa zadatim ID-em)
        * u slucaju kada je predlog tacke dnevnog reda vec prihvacen kao stavka nekog od narednih sastanaka skupstine stanara
        * */

        // predlog tacke dnevnog reda sa ID-em = 101 je vec povezan sa sastankom sa ID-em = 100
        this.mockMvc.perform(delete(URL_PREFIX + "/100/billboard/meeting_items/101")
                .header("Authentication-Token", this.accessTokentTenant_3))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testDeleteMeetingItemTenantIsNotCreator() throws Exception {
        /*
        * Test proverava ispravnost rada metode deleteMeetingItem kontrolera BillboardController
        * (brisanje konkretnog predloga tacke dnevnog reda sa zadatim ID-em u zgradi sa zadatim ID-em)
        * u slucaju kada trenutno ulogovani stanar nije kreator predloga tacke dnevnog reda sa zadatim ID-em
        * */

        // stanar sa ID-em = 103 nije kreator obavestenja sa ID-em = 102
        this.mockMvc.perform(delete(URL_PREFIX + "/100/billboard/meeting_items/102")
                .header("Authentication-Token", this.accessTokentTenant_2))
                .andExpect(status().isForbidden());
    }

    // TESTOVI za getMeetingItemsForMeeting
    @Test
    public void testGetMeetingItemsForMeeting() throws Exception{
        /*
        * Test proverava ispravnost metode getMeetingItemsForMeeting kontrolera BillboardController
        * (dobavljanje liste tacaka dnevnog reda koje su izabrane za sastanak sa zadatim ID-em),
        * u slucaju kada akciju izvrsava logovani korisnik
        * */

        /*
        za sastanak sa ID-em = 102 u listi izabranih tacaka dnevnog reda je samo jedna sa sledecim podacima:
            id = 104,
            title = Fourth meeting item,
            content = Fourth meeting item content,
            creator_id = 102
        */

        mockMvc.perform(get(URL_PREFIX + "/100/meetings/102/meeting_items")
                .params(params)
                .header("Authentication-Token", this.accessTokenTenant))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.[*]", hasSize(1)))
                .andExpect(jsonPath("$.[0].id").value(104))
                .andExpect(jsonPath("$.[0].title").value("Fourth meeting item"))
                .andExpect(jsonPath("$.[0].content").value("Fourth meeting item content"))
                .andExpect(jsonPath("$.[0].creator.id").value(102));
    }

    @Test
    public void testGetMeetingItemsForMeetingByUnauthorizedUser() throws Exception{
        /*
        * Test proverava ispravnost metode getMeetingItemsForMeeting kontrolera BillboardController
        * (dobavljanje liste tacaka dnevnog reda koje su izabrane za sastanak sa zadatim ID-em),
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        mockMvc.perform(get(URL_PREFIX + "/100/meetings/100/meeting_items")
                .params(params))
                .andExpect(status().isForbidden());
    }

    // TESTOVI za getMeetingItemsForRecord
    @Test
    public void testGetMeetingItemsForRecord() throws Exception{
        /*
        * Test proverava ispravnost metode getMeetingItemsForRecord kontrolera BillboardController
        * (dobavljanje liste tacaka dnevnog reda koje su obradjene na sastanku kom pripada izvestaj ciji je ID zadat),
        * u slucaju kada akciju izvrsava logovani korisnik koji ima ulogu stanara
        * */

        /*
        za izvestaj sa ID-em = 100 je vezan sastanak sa ID-em = 100 kom su pripadale sledece dve tacke dnevnog reda:
            1 - id = 100,
                title = First meeting item,
                content = First meeting item content,
                creator_id = 101
           2 - id = 101
               title = Second meeting item
               content = Second meeting item content
               creator_id = 102
        */
        mockMvc.perform(get(URL_PREFIX + "/100/meetings/100/record/meeting_items")
                .params(params)
                .header("Authentication-Token", this.accessTokenTenant))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.[*]", hasSize(2)))
                .andExpect(jsonPath("$.[0].id").value(100))
                .andExpect(jsonPath("$.[0].title").value("First meeting item"))
                .andExpect(jsonPath("$.[0].content").value("First meeting item content"))
                .andExpect(jsonPath("$.[0].creator.id").value(101))
                .andExpect(jsonPath("$.[1].id").value(101))
                .andExpect(jsonPath("$.[1].title").value("Second meeting item"))
                .andExpect(jsonPath("$.[1].content").value("Second meeting item content"))
                .andExpect(jsonPath("$.[1].creator.id").value(102));
    }

    @Test
    public void testGetMeetingItemsForRecordByUnauthorizedUser() throws Exception{
        /*
        * Test proverava ispravnost metode getMeetingItemsForRecord kontrolera BillboardController
        * (dobavljanje liste tacaka dnevnog reda koje su obradjene na sastanku kom pripada izvestaj ciji je ID zadat),
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        mockMvc.perform(get(URL_PREFIX + "/100/meetings/100/record/meeting_items")
                .params(params))
                .andExpect(status().isForbidden());
    }
}
