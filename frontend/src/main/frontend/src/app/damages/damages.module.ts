import { NgModule } from '@angular/core';
// module
import { SharedModule } from "../shared/shared.module";
import { DamagesRouterModule } from "./damages-router.module";
// component
import { DamageListComponent } from './damage-list/damage-list.component';
import { DamageAddComponent } from './damage-add/damage-add.component';
import { ChooseApartmentModalComponent } from './choose-apartment-modal/choose-apartment-modal.component';
import { DamageEditComponent } from './damage-edit/damage-edit.component';
import { DamageDetailsComponent } from './damage-details/damage-details.component';
import { DelegateResponsibilityToPersonComponent } from '../shared/components/delegate-responsibility-to-person/delegate-responsibility-to-person.component';
import { DelegateResponsibilityToInstitutionComponent } from '../shared/components/delegate-responsibility-to-institution/delegate-responsibility-to-institution.component';
import { CommentModalComponent } from './comment-modal/comment-modal.component';
import { DamageTypesModalComponent } from './damage-types-modal/damage-types-modal.component';
import { BusinessListModalComponent } from './business-list-modal/business-list-modal.component';
import { FixDamageListModalComponent } from './fix-damage-list-modal/fix-damage-list-modal.component';
import { ConfirmDeleteModalComponent } from "../shared/components/confirm-delete-modal/confirm-delete-modal.component";
// pipe
import { DamageTypeFormatPipe } from "./shared/pipes/damage-type-format.pipe";
import { FloorInBuildingPipe } from "./damage-details/pipes/floor-in-building.pipe";


@NgModule({
  imports: [
    SharedModule,
    DamagesRouterModule
  ],
  declarations: [
    DamageListComponent,
    DamageAddComponent,
    ChooseApartmentModalComponent,
    DamageEditComponent,
    CommentModalComponent,
    DamageDetailsComponent,
    DamageTypesModalComponent,
    BusinessListModalComponent,
    FixDamageListModalComponent,
    DamageTypeFormatPipe,
    FloorInBuildingPipe
  ],
  exports: [
    DamageListComponent
  ],
  entryComponents: [
    DamageAddComponent,
    ChooseApartmentModalComponent,
    DamageEditComponent,
    CommentModalComponent,
    DelegateResponsibilityToInstitutionComponent,
    DelegateResponsibilityToPersonComponent,
    DamageTypesModalComponent,
    BusinessListModalComponent,
    FixDamageListModalComponent,
    ConfirmDeleteModalComponent
  ]
})
export class DamagesModule { }
