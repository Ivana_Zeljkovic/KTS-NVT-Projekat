package rs.ac.uns.ftn.ktsnvt.service.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Damage;
import rs.ac.uns.ftn.ktsnvt.service.ApartmentService;
import rs.ac.uns.ftn.ktsnvt.service.BuildingService;
import rs.ac.uns.ftn.ktsnvt.service.DamageService;

import java.util.Calendar;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Katarina Cukurov on 30/11/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class DamageServiceIntegrationTest {

    @Autowired
    private DamageService damageService;


    @Test
    @Transactional
    public void testFindOne() {
        /*
         * Test proverava ispravnost rada metode findOne servisa DamageService
         * (dobavljanje testa na osnovu ID-a)
         * u slucaju kada postoji test sa zadatim ID-em
         * */

        /*
        test sa ID-em = 100 ima podatke:
            id = 100,
            date = 2017-11-27,
            description = 'first damage description',
            fixed = false,
            apartment_id = 102,
            building_id = 100,
            responsible_institution_id = null,
            responsible_person_id = 104
         */

        Damage damage = this.damageService.findOne(new Long(100));

        assertNotNull(damage);
        assertEquals(Long.valueOf(100), damage.getId());
        assertEquals("2017-11-27 00:00:00.0", damage.getDate().toString());
        assertEquals("first damage description", damage.getDescription());
        assertEquals(false, damage.isFixed());
        assertEquals(new Long(102), damage.getApartment().getId());
        assertEquals(new Long(100), damage.getBuilding().getId());
        assertEquals(new Long(104), damage.getResponsiblePerson().getId());
    }

    @Test(expected = NotFoundException.class)
    public void testFindOneNonexistent() {
        /*
         * Test proverava ispravnost rada metode findOne servisa DamageService
         * (dobavljanje testa na osnovu ID-a)
         * u slucaju kada ne postoji test sa zadatim ID-em - ocekivano NotFoundException
         * */

        // damage sa ID-em = 500 ne postoji
        Damage damage = this.damageService.findOne(new Long(500));
    }

    @Test
    @Transactional
    public void testCheckDamageInBuilding() {
        /*
         * Test proverava ispravnost rada metode checkDamageInBuilding servisa DamageService
         * (provera pripadnosti konkretnog kvara zgradi ciji je ID prosledjen)
         * u slucaju kada prosledjeni kvar zaista pripada zgradi sa prosledjenim ID-em
         * */

        // damage sa ID-em = 100 pripada zgradi sa ID-em = 100
        Damage damage = this.damageService.findOne(new Long(100));

        this.damageService.checkDamageInBuilding(damage, new Long(100));
    }

    @Test(expected = BadRequestException.class)
    @Transactional
    public void testCheckDamageInBuildingInvalid() {
        /*
         * Test proverava ispravnost rada metode checkDamageInBuilding servisa DamageService
         * (provera pripadnosti konkretnog kvara zgradi ciji je ID prosledjen)
         * u slucaju kada prosledjeni kvar ne pripada zgradi sa prosledjenim ID-em - ocekivano BadRequestException
         * */

        // damage sa ID-em = 100 ne pripada zgradi sa ID-em = 101
        Damage damage = this.damageService.findOne(new Long(100));

        this.damageService.checkDamageInBuilding(damage, new Long(101));
    }


    @Test
    public void testGetAllForInstitution() {
         /*
         * Test proverava ispravnost rada metode getAllForInstitution servisa damageService
         * (pronalazenje neresenih kvarova koji pripadaju instituciji sa zadatim ID-em)
         * */

        Page<Damage> damages = this.damageService.getDamagesForInstitution(102L,
                0, 2, "ASC", "id", "description");

        assertEquals(2, damages.getTotalElements());

    }

    @Test
    public void getAllForInstitutionFromTo() {
        /*
         * Test proverava ispravnost rada metode ForInstitutionFromTo servisa damageService
         * (pronalazenje neresenih kvarova koji pripadaju instituciji sa zadatim ID-em, u vremenskom intervalu)
         *
         * */
        Calendar from = Calendar.getInstance();
        from.add(Calendar.YEAR, -1);
        Calendar to = Calendar.getInstance();
        to.add(Calendar.YEAR, 1);

        Page<Damage> damages = this.damageService.getDamagesForInstitutionFromTo(102L,
                from.getTime(), to.getTime(),
                0, 2, "asc", "id", "description");

        assertEquals(2, damages.getTotalElements());

    }

    @Test
    public void testGetAllForRepair() {
         /*
         * Test proverava ispravnost rada metode getAllForRepair servisa damageService
         * (pronalazenje kvarova koje firma/institucija sa zadatim id-em treba da popravi)
         * */

        Page<Damage> damages = this.damageService.getDamagesForRepair(101L,
                0, 2, "asc", "id", "description");

        assertEquals(1, damages.getTotalElements());
        assertEquals(new Long(103), damages.getContent().get(0).getId());
    }

    @Test
    public void testGetAllForRepairFromTo() {
          /*
         * Test proverava ispravnost rada metode getAllForRepairFromTo servisa damageService
         * (pronalazenje kvarova koje firma/institucija sa zadatim id-em treba da popravi u odredjenom periodu)
         * */

        Calendar from = Calendar.getInstance();
        from.add(Calendar.YEAR, -1);
        Calendar to = Calendar.getInstance();
        to.add(Calendar.YEAR, 1);

        Page<Damage> damages = this.damageService.getDamagesForRepairFromTo(101L,
                from.getTime(), to.getTime(),
               0, 2, "asc", "id", "description");

        assertEquals(1, damages.getTotalElements());
        assertEquals(new Long(103), damages.getContent().get(0).getId());
    }

    @Test
    public void testGetAllInBuildingFromTo() {
         /*
         * Test proverava ispravnost rada metode findByBuildingIdFromTo servisa damageService
         * (pronalazenje neresenih kvarova koji pripadaju zgradi sa zadatim ID-em u odredjenom vremenskom intervalu)
         * */

        Calendar from = Calendar.getInstance();
        from.add(Calendar.YEAR, -1);
        Calendar to = Calendar.getInstance();
        to.add(Calendar.YEAR, 1);

        Page<Damage> damages = this.damageService.getDamagesInBuildingFromTo(100L,
                from.getTime(), to.getTime(),
               0, 2, "asc", "id", "description");
        assertEquals(5, damages.getTotalElements());

    }

    @Test
    public void testGeAllForTenant() {
        /*
         * Test proverava ispravnost rada metode getAllForTenant servisa damageService
         * (pronalazenje neresenih kvarova za koje je stanar odgovoran)
         * */

        Page<Damage> damages = this.damageService.getDamagesForTenant(
                103L,0, 2, "asc", "id", "description");
        assertEquals(3, damages.getTotalElements());
    }

    @Test
    public void testGeAllForTenantFromTo() {
        /*
         * Test proverava ispravnost rada metode getAllForTenantFromTo servisa damageService
         * (pronalazenje neresenih kvarova za koje je stanar odgovoran u odredjenom periodu)
         * */

        Calendar from = Calendar.getInstance();
        from.add(Calendar.YEAR, -1);
        Calendar to = Calendar.getInstance();
        to.add(Calendar.YEAR, 1);

        Page<Damage> damages = this.damageService.getDamagesForTenantFromTo(103L, from.getTime(), to.getTime(),
               0, 2, "asc", "id", "description");
        assertEquals(3, damages.getTotalElements());
    }

    @Test
    public void testGetAllForPersonalApartments() {
        /*
        * Test proverava ispravnost rada metode getAllInPersonalApartments servisa damageService
         * (pronalazenje neresenih kvarova u stanovima koje posedujem)
         * */

        Page<Damage> damages = this.damageService.getDamagesInPersonalApartments(
                101L,0, 2, "asc", "id", "description");

        assertEquals(5, damages.getTotalElements());

    }

    @Test
    public void testGetAllForPersonalApartmentsFromTo() {
        /*
        * Test proverava ispravnost rada metode getAllInPersonalApartmentsFromTo servisa damageService
         * (pronalazenje neresenih kvarova u stanovima koje posedujem u odredjenom vremenskom intervalu)
         * */
        Calendar from = Calendar.getInstance();
        from.add(Calendar.YEAR, -1);
        Calendar to = Calendar.getInstance();
        to.add(Calendar.YEAR, 1);

        Page<Damage> damages = this.damageService.getDamagesInPersonalApartmentsFromTo(101L, from.getTime(), to.getTime(),
               0, 2, "asc", "id", "description");


        assertEquals(5, damages.getTotalElements());

    }

    @Test
    public void testGetAllForApartment()
    {
        /*
        * Test proverava ispravnost rada metode GetAllForApartment servisa damageService
         * (pronalazenje neresenih kvarova u stanu sa zadatim id-em)
         * */


        Page<Damage> damages = this.damageService.getDamagesInApartment(101L,
               0, 2, "asc", "id", "description");

        assertEquals(4, damages.getTotalElements());
    }

    @Test
    public void testGetAllForApartmentFromTo()
    {
        /*
        * Test proverava ispravnost rada metode GetAllForApartmentFromTo servisa damageService
         * (pronalazenje neresenih kvarova u stanu sa zadatim id-em u odredjenom vremenskom periodu)
         * */
        Calendar from = Calendar.getInstance();
        from.add(Calendar.YEAR, - 1);
        Calendar to = Calendar.getInstance();
        to.add(Calendar.YEAR, 1);

        Page<Damage> damages = this.damageService.getDamagesInApartmentFromTo(101L, from.getTime(), to.getTime(),
               0, 2, "asc", "id", "description");

        assertEquals(4 , damages.getTotalElements());
    }


    @Test
    public void testGetAllMyDamages()
    {
        /*
         * Test proverava ispravnost rada metode getAllMyDamages servisa damageService
         * (pronalazenje neresenih kvarova koje sam ja prijavio)
         * */

        Page<Damage> damages = this.damageService.getMyDamages(101L, 0, 2, "asc", "id", "description");
        assertEquals(3, damages.getTotalElements());
    }

    @Test
    public void testGetAllMyDamagesFromTo()
    {
         /*
         * Test proverava ispravnost rada metode getAllMyDamages servisa damageService
         * (pronalazenje neresenih kvarova koje sam ja prijavio u odredjenom periodu)
         * */

        Calendar from = Calendar.getInstance();
        from.add(Calendar.YEAR, - 1);
        Calendar to = Calendar.getInstance();
        to.add(Calendar.YEAR, 1);

        Page<Damage> damages = this.damageService.getMyDamagesFromTo(101L,from.getTime(), to.getTime(),
               0, 2, "asc", "id", "description");

        assertEquals(3, damages.getTotalElements());

    }

    @Test
    public void testFindDamageIdInOfferRequest()
    {
        /*
         * Test proverava ispravnost rada metode findDamageIdInOfferRequest servisa damageService
         * (pronalazenje kvara za koji je trazena ponuda od strane neke firme ili institucije)
         * */

        boolean damage = this.damageService.findDamageIdInOfferRequest(102L, 101L);

        assertEquals(true, damage);
    }





}
