package rs.ac.uns.ftn.ktsnvt.service.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.exception.ForbiddenException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Building;
import rs.ac.uns.ftn.ktsnvt.model.entity.Business;
import rs.ac.uns.ftn.ktsnvt.model.entity.DamageResponsibility;
import rs.ac.uns.ftn.ktsnvt.model.entity.Tenant;
import rs.ac.uns.ftn.ktsnvt.service.BuildingService;
import rs.ac.uns.ftn.ktsnvt.service.BusinessService;
import rs.ac.uns.ftn.ktsnvt.service.DamageResponsibilityService;
import rs.ac.uns.ftn.ktsnvt.service.TenantService;

import static org.junit.Assert.*;

/**
 * Created by Ivana Zeljkovic on 30/11/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class DamageResponsibilityServiceIntegrationTest {

    @Autowired
    private DamageResponsibilityService damageResponsibilityService;

    @Autowired
    private TenantService tenantService;

    @Autowired
    private BusinessService businessService;

    @Autowired
    private BuildingService buildingService;


    @Test
    @Transactional
    public void testFindOneByDamageTypeAndTenantAndBuilding() {
        /*
         * Test proverava ispravnost metode findOneByDamageTypeAndTenantAndBuilding servisa DamageResponsibilityService
         * (dobavljanje odgovornosti za kvar na osnovu zadatog tipa kvara, stanara i zgrade)
         * u slucaju kada postoji odgovornost zadatog stanara za zadati tip kvara u zadatoj zgradi
         * */
        Tenant responsible_tenant = this.tenantService.findOne(new Long(102));
        Building building = this.buildingService.findOne(new Long(100));

        // stanar sa ID-em = 102 je odgovorna osoba za tip kvara sa ID-em = 3 u zgradi sa ID-em = 100
        DamageResponsibility damage_responsibility = this.damageResponsibilityService.findOneByDamageTypeAndTenantAndBuilding(
                new Long(3), new Long(102), new Long(100));

        assertNotNull(damage_responsibility);
        assertEquals(responsible_tenant.getId(), damage_responsibility.getResponsibleTenant().getId());
        assertEquals(responsible_tenant.getFirstName(), damage_responsibility.getResponsibleTenant().getFirstName());
        assertEquals(responsible_tenant.getLastName(), damage_responsibility.getResponsibleTenant().getLastName());
        assertEquals(building.getId(), damage_responsibility.getBuilding().getId());
        assertEquals(building.getAddress(), damage_responsibility.getBuilding().getAddress());
    }

    @Test(expected = ForbiddenException.class)
    public void testFindOneByDamageTypeAndTenantAndBuildingNonexistent() {
        /*
         * Test proverava ispravnost metode findOneByDamageTypeAndTenantAndBuilding servisa DamageResponsibilityService
         * (dobavljanje odgovornosti za kvar na osnovu zadatog tipa kvara, stanara i zgrade)
         * u slucaju kada ne postoji odgovornost zadatog stanara za zadati tip kvara u zadatoj zgradi - ocekivano ForbiddenException
         * */

        // za tip kvara sa ID-em = 2 u zgradi sa ID-em = 100 postoji odgovorna institucija, ali ne i odgovorno lice
        DamageResponsibility damage_responsibility = this.damageResponsibilityService.findOneByDamageTypeAndTenantAndBuilding(
                new Long(2), new Long(101), new Long(100));
    }

    @Test
    @Transactional
    public void testFindOneByDamageTypeAndInstitutionAndBuilding() {
        /*
         * Test proverava ispravnost metode findOneByDamageTypeAndInstitutionAndBuilding servisa DamageResponsibilityService
         * (dobavljanje odgovornosti za kvar na osnovu zadatog tipa kvara, institucije i zgrade)
         * u slucaju kada postoji odgovornost zadate institucije za zadati tip kvara u zadatoj zgradi
         * */

        Business responsible_institution = this.businessService.findOne(new Long(103));
        Building building = this.buildingService.findOne(new Long(100));

        // institucija sa ID-em = 103 je odgovorna osoba za tip kvara sa ID-em = 2 u zgradi sa ID-em = 100
        DamageResponsibility damage_responsibility = this.damageResponsibilityService.findOneByDamageTypeAndInstitutionAndBuilding(
                new Long(2), new Long(103), new Long(100));

        assertNotNull(damage_responsibility);
        assertEquals(responsible_institution.getId(), damage_responsibility.getResponsibleInstitution().getId());
        assertFalse(damage_responsibility.getResponsibleInstitution().isIsCompany());
        assertEquals(responsible_institution.getAddress(), damage_responsibility.getResponsibleInstitution().getAddress());
        assertEquals(responsible_institution.getName(), damage_responsibility.getResponsibleInstitution().getName());
        assertEquals(building.getId(), damage_responsibility.getBuilding().getId());
        assertEquals(building.getAddress(), damage_responsibility.getBuilding().getAddress());
    }

    @Test(expected = ForbiddenException.class)
    public void testFindOneByDamageTypeAndInstitutionAndBuildingNonexistent() {
        /* Test proverava ispravnost metode findOneByDamageTypeAndInstitutionAndBuilding servisa DamageResponsibilityService
         * (dobavljanje odgovornosti za kvar na osnovu zadatog tipa kvara, institucije i zgrade)
         * u slucaju kada ne postoji odgovornost zadate institucije za zadati tip kvara u zadatoj zgradi - ocekivano ForbiddenException
         * */

        // za tip kvara sa ID-em = 3 u zgradi sa ID-em = 100 postoji odgovorna osoba, ali ne i odgovorno institucija
        DamageResponsibility damage_responsibility = this.damageResponsibilityService.findOneByDamageTypeAndInstitutionAndBuilding(
                new Long(3), new Long(101), new Long(100));
    }
}
