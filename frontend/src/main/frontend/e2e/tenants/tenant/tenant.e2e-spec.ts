import {browser, by, element, ElementFinder} from "protractor";
// page
import {LoginPage} from "../../page-objects/login.po";
import {TenantPage} from "./page-objects/tenant.po";


describe('Tenant more details view', () => {
  let loginPage: LoginPage;
  let tenantPage: TenantPage;
  let tenantWithId105: any;

  beforeEach(() => {
    loginPage = new LoginPage();
    tenantPage = new TenantPage();
    browser.get("/");

    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');
    loginPage.setUsername('ivana');
    loginPage.setPassword('ivana');
    loginPage.getSignInButton().click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/home');
    browser.get('http://localhost:49152/#/tenants/105');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/tenants/105');

    tenantWithId105 = {
      'firstName': 'Isidora',
      'lastName': 'Krajinovic',
      'username': 'isidora',
      'birthday': '02/11/1995',
      'email': 'isidora@gmail.com',
      'phoneNumber': '789654',
      'city': 'Novi Sad',
      'postalNumber': '21000',
      'street': 'Janka Veselinovica',
      'number': '8'
    };

  });

  afterEach(() => {
    browser.executeScript('window.localStorage.clear();');
  });

  it('should load all details which tenant with id:105 has', () => {
    // all elements should be loaded
    expect(tenantPage.getPanelHeading().isDisplayed()).toBe(true);
    expect(tenantPage.getPanelBody().isDisplayed()).toBe(true);
    expect(tenantPage.getMap().isDisplayed()).toBe(true);
    expect(tenantPage.getTenantTable().isDisplayed()).toBe(true);
    expect(tenantPage.getFirstName().isDisplayed()).toBe(true);
    expect(tenantPage.getLastName().isDisplayed()).toBe(true);
    expect(tenantPage.getUsername().isDisplayed()).toBe(true);
    expect(tenantPage.getBirthday().isDisplayed()).toBe(true);
    expect(tenantPage.getEmail().isDisplayed()).toBe(true);
    expect(tenantPage.getPhoneNumber().isDisplayed()).toBe(true);
    expect(tenantPage.getCity().isDisplayed()).toBe(true);
    expect(tenantPage.getPostalNumber().isDisplayed()).toBe(true);
    expect(tenantPage.getStreet().isDisplayed()).toBe(true);
    expect(tenantPage.getNumber().isDisplayed()).toBe(true);

    // table should be filled with data
    expect(tenantPage.getFirstName() == tenantWithId105.firstName);
    expect(tenantPage.getLastName() == tenantWithId105.lastName);
    expect(tenantPage.getUsername() == tenantWithId105.username);
    expect(tenantPage.getBirthday() == tenantWithId105.birthday);
    expect(tenantPage.getEmail() == tenantWithId105.email);
    expect(tenantPage.getPhoneNumber() == tenantWithId105.phoneNumber);
    expect(tenantPage.getCity() == tenantWithId105.city);
    expect(tenantPage.getPostalNumber() == tenantWithId105.postalNumber);
    expect(tenantPage.getStreet() == tenantWithId105.street);
    expect(tenantPage.getNumber() == tenantWithId105.number);
  });

});
