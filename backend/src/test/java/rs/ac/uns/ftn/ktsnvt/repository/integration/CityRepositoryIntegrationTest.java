package rs.ac.uns.ftn.ktsnvt.repository.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.model.entity.City;
import rs.ac.uns.ftn.ktsnvt.repository.CityRepository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Katarina Cukurov on 28/11/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class CityRepositoryIntegrationTest {

    @Autowired
    private CityRepository cityRepository;

    @Test
    @Transactional
    public void testFindByName(){
        /*
         * Test proverava ispravnost rada metode findByName repozitorijuma CityRepository
         * (pronalazenje svih gradova koji su u bazi na osnovu imena)
         * */

        /*
        u bazi postoji grad sa ime = Novi Sad i sledecim podacima:
            id = 100,
            name = Novi Sad,
            postal_number = 21000
        */
        City city = this.cityRepository.findByName("Novi Sad");

        assertNotNull(city);
        assertEquals(21000, city.getPostalNumber());
        assertEquals(Long.valueOf(100), city.getId());
        assertEquals("Novi Sad", city.getName());
    }

    @Test
    @Transactional
    public void testFindByNameAndPostal_number(){
        /*
         * Test proverava ispravnost rada metode findByNameAndPostalNumber repozitorijuma CityRepository
         * (pronalazenje svih gradova koji su u bazi na osnovu imena i postanskog broja)
         * */

        // u bazi za grad sa imenom: Novi Sad i postanskim brojem: 21000 (ID = 100)
        City city = this.cityRepository.findByNameAndPostalNumber("Novi Sad", 21000);

        assertNotNull(city);
        assertEquals(21000, city.getPostalNumber());
        assertEquals(Long.valueOf(100), city.getId());
        assertEquals("Novi Sad", city.getName());
    }
}
