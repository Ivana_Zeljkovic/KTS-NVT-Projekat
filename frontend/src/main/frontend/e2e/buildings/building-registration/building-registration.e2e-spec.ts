import {browser, by, element} from "protractor";
// page
import {LoginPage} from "../../page-objects/login.po";
import {BuildingRegistrationPage} from "./page-objects/building-registration.po";
import {AddApartmentModalPage} from "../../shared/add-apartment-modal/page-objects/add-apartment-modal.po";
import {ChooseManagerModalPage} from "../../shared/choose-manager-modal/page-objects/choose-manager-modal.po";
import {BuildingListPage} from "../building-list/page-objects/building-list.po";

describe('Building registration', () => {
  let loginPage: LoginPage;
  let buildingRegistrationPage: BuildingRegistrationPage;
  let addApartmentModalPage: AddApartmentModalPage;
  let chooseManagerModalPage: ChooseManagerModalPage;
  let buildingsListPage: BuildingListPage;

  beforeEach(() => {
    loginPage = new LoginPage();
    buildingRegistrationPage = new BuildingRegistrationPage();
    buildingsListPage = new BuildingListPage();
    browser.get("/");

    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');
    loginPage.setUsername('ivana');
    loginPage.setPassword('ivana');
    loginPage.getSignInButton().click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/home');
    browser.get('http://localhost:49152/#/buildings');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/buildings');
    buildingsListPage.getAddButton().click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/buildings/register');
  });

  afterEach(() => {
    browser.executeScript('window.localStorage.clear();');
  });

  it('should successfully populate data, add one apartment and choose manager, so building can be created', () => {

    expect(buildingRegistrationPage.getPageTitle().isDisplayed()).toBe(true);
    expect(buildingRegistrationPage.getPanelBody().isDisplayed()).toBe(true);
    expect(buildingRegistrationPage.getNumberOfFloors().isDisplayed()).toBe(true);
    expect(buildingRegistrationPage.getDropdownFloor().isDisplayed()).toBe(true);
    expect(buildingRegistrationPage.getRadioParking().isDisplayed()).toBe(true);
    expect(buildingRegistrationPage.getApartmentHeader().isDisplayed()).toBe(true);
    expect(buildingRegistrationPage.getApartmentTable().isPresent()).toBe(true);
    expect(buildingRegistrationPage.getDisplayAddApartment().isDisplayed()).toBe(true);
    expect(buildingRegistrationPage.getStreet().isDisplayed()).toBe(true);
    expect(buildingRegistrationPage.getNumber().isDisplayed()).toBe(true);
    expect(buildingRegistrationPage.getCity().isDisplayed()).toBe(true);
    expect(buildingRegistrationPage.getPostalCode().isDisplayed()).toBe(true);
    expect(buildingRegistrationPage.getManagerHeader().isDisplayed()).toBe(true);
    expect(buildingRegistrationPage.getManagerTable().isPresent()).toBe(false);
    expect(buildingRegistrationPage.getDisplayAddManager().isDisplayed()).toBe(true);
    expect(buildingRegistrationPage.getSaveButton().isDisplayed()).toBe(true);

    expect(buildingRegistrationPage.getSaveButton().isEnabled()).toBe(false);

    buildingRegistrationPage.getDisplayAddApartment().click().then(function () {
      addApartmentModalPage = new AddApartmentModalPage();
      addApartmentModalPage.setNumber(1);
      addApartmentModalPage.setSurface(50);
      addApartmentModalPage.setFloor(1);
      addApartmentModalPage.getConfirmButton().click();
      addApartmentModalPage.getCloseButton().click().then(function () {
        expect(buildingRegistrationPage.getApartmentTable().isDisplayed()).toBe(true);
      });
    });

    buildingRegistrationPage.setNumber("1");
    buildingRegistrationPage.setCity("Novi Sad");
    buildingRegistrationPage.setPostalCode("21000");
    buildingRegistrationPage.setStreet("Anotna Cehova");

    expect(buildingRegistrationPage.getApartmentTable().isDisplayed()).toBe(true);
    expect(buildingRegistrationPage.getSaveButton().isEnabled()).toBe(true);

    buildingRegistrationPage.getDisplayAddManager().click().then(function () {
      chooseManagerModalPage = new ChooseManagerModalPage();
      let manager = element.all(by.xpath("//tr[1]/td[3]")).get(1).element(by.css('.btn.btn-default'));
      manager.click();
    });

    expect(buildingRegistrationPage.getManagerTable().isDisplayed()).toBe(true);
  });

  it('should show error for required street when user put focus and leaves it blank', () =>{

    buildingRegistrationPage.getStreet().click();
    buildingRegistrationPage.getNumber().click();
    expect(buildingRegistrationPage.getStreetRequiredError().isDisplayed()).toBe(true);
    expect(buildingRegistrationPage.getSaveButton().isEnabled()).toBe(false);
  });

  it('should show error for required street minimum length when street name is too short', () =>{

    buildingRegistrationPage.setStreet("a");
    buildingRegistrationPage.getNumber().click();
    expect(buildingRegistrationPage.getStreetMinLengthError().isDisplayed()).toBe(true);
    expect(buildingRegistrationPage.getSaveButton().isEnabled()).toBe(false);
  });

  it('should show error for required number when user put focus and leaves it blank', () =>{

    buildingRegistrationPage.getNumber().click();
    buildingRegistrationPage.getStreet().click();
    expect(buildingRegistrationPage.getNumberRequiredError().isDisplayed()).toBe(true);
    expect(buildingRegistrationPage.getSaveButton().isEnabled()).toBe(false);
  });

  it('should show error for space in number when user put space in number input', () =>{

    buildingRegistrationPage.setNumber("4 4");
    buildingRegistrationPage.getStreet().click();
    expect(buildingRegistrationPage.getNumberSpaceError().isDisplayed()).toBe(true);
    expect(buildingRegistrationPage.getSaveButton().isEnabled()).toBe(false);
  });

  it('should show error for required city when user put focus and leaves it blank', () =>{

    buildingRegistrationPage.getCity().click();
    buildingRegistrationPage.getStreet().click();
    expect(buildingRegistrationPage.getCityRequiredError().isDisplayed()).toBe(true);
    expect(buildingRegistrationPage.getSaveButton().isEnabled()).toBe(false);
  });

  it('should show error for required postal code when user put focus and leaves it blank', () =>{

    buildingRegistrationPage.getPostalCode().click();
    buildingRegistrationPage.getStreet().click();
    expect(buildingRegistrationPage.getPostalCodeRequiredError().isDisplayed()).toBe(true);
    expect(buildingRegistrationPage.getSaveButton().isEnabled()).toBe(false);
  });

  it('should show error for length of postal code when user put shorter or longer postal code', () =>{

    buildingRegistrationPage.setPostalCode("123")
    buildingRegistrationPage.getStreet().click();
    expect(buildingRegistrationPage.getPostalCodeLengthError().isDisplayed()).toBe(true);
    expect(buildingRegistrationPage.getSaveButton().isEnabled()).toBe(false);
  });

});
