package rs.ac.uns.ftn.ktsnvt.web.dto;

import rs.ac.uns.ftn.ktsnvt.model.entity.MeetingItem;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Ivana Zeljkovic on 13/11/2017.
 */
public class MeetingItemDTO implements Serializable {

    private Long id;
    private String title;
    private String content;
    private Date date;
    private TenantDTO creator;
    private QuestionnaireDTO questionnaire;
    private Long damageId;
    private String damageDescription;
    private long numberOfElements;

    public MeetingItemDTO() { }

    public MeetingItemDTO(MeetingItem meetingItem, Long buildingId) {
        this.id = meetingItem.getId();
        this.title = meetingItem.getTitle();
        this.content = meetingItem.getContent();
        this.date = meetingItem.getDate();
        this.creator = new TenantDTO(meetingItem.getCreator());
        this.questionnaire = (meetingItem.getQuestionnaire() == null) ? null : new QuestionnaireDTO(meetingItem.getQuestionnaire(), buildingId);
        this.damageId = (meetingItem.getDamage() == null) ? null : meetingItem.getDamage().getId();
        this.damageDescription = (meetingItem.getDamage() == null) ? null : meetingItem.getDamage().getDescription();
    }

    public MeetingItemDTO(MeetingItem meetingItem, long numberOfElements, Long buildingId) {
        this.id = meetingItem.getId();
        this.title = meetingItem.getTitle();
        this.content = meetingItem.getContent();
        this.date = meetingItem.getDate();
        this.creator = new TenantDTO(meetingItem.getCreator());
        this.questionnaire = (meetingItem.getQuestionnaire() == null) ? null : new QuestionnaireDTO(meetingItem.getQuestionnaire(), buildingId);
        this.damageId = (meetingItem.getDamage() == null) ? null : meetingItem.getDamage().getId();
        this.damageDescription = (meetingItem.getDamage() == null) ? null : meetingItem.getDamage().getDescription();
        this.numberOfElements = numberOfElements;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() { return title; }

    public void setTitle(String title) { this.title = title; }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getDate() { return date; }

    public void setDate(Date date) { this.date = date; }

    public TenantDTO getCreator() {
        return creator;
    }

    public void setCreator(TenantDTO creator) {
        this.creator = creator;
    }

    public QuestionnaireDTO getQuestionnaire() {
        return questionnaire;
    }

    public void setQuestionnaire(QuestionnaireDTO questionnaire) {
        this.questionnaire = questionnaire;
    }

    public Long getDamageId() { return damageId; }

    public void setDamageId(Long damageId) { this.damageId = damageId; }

    public String getDamageDescription() { return damageDescription; }

    public void setDamageDescription(String damageDescription) { this.damageDescription = damageDescription; }

    public long getNumberOfElements() { return numberOfElements; }

    public void setNumberOfElements(long numberOfElements) { this.numberOfElements = numberOfElements; }
}
