package rs.ac.uns.ftn.ktsnvt.repository.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.model.entity.Damage;
import rs.ac.uns.ftn.ktsnvt.repository.DamageRepository;

import java.util.Calendar;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Nikola Garabandic on 30/11/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class DamageRepositoryIntegrationTest {

    @Autowired
    private DamageRepository damageRepository;


    @Test
    public void findByBuildingId() {
       /*
        * Test proverava ispravnost rada metode findByBuildingId repozitorijuma DamageRepository
        * (pronalazenje neresenih kvarova koji pripadaju zgradi sa zadatim ID-em)
        * */

        /*
        kvarovi u zgradi sa ID-em = 100 su sledeci kvarovi:
            1. ID = 100
            2. ID = 101
            3. ID = 102
            4. ID = 103
            5. ID = 104
        */
        Page<Damage> damages = this.damageRepository.findAllByBuilding(100L, new PageRequest(
                0, 2, Sort.Direction.ASC, "id"
        ));

        assertEquals(5, damages.getTotalElements());
    }

    @Test
    public void testGetAllForInstitution() {
         /*
         * Test proverava ispravnost rada metode getAllForInstitution repozitorijuma DamageRepository
         * (pronalazenje neresenih kvarova koji pripadaju instituciji sa zadatim ID-em)
         * */

        Page<Damage> damages = this.damageRepository.getAllForInstitution(102L, new PageRequest(0, 2, Sort.Direction.ASC, "id"));

        assertEquals(2, damages.getTotalElements());

    }

    @Test
    public void getAllForInstitutionFromTo() {
        /*
         * Test proverava ispravnost rada metode ForInstitutionFromTo repozitorijuma DamageRepository
         * (pronalazenje neresenih kvarova koji pripadaju instituciji sa zadatim ID-em, u vremenskom intervalu)
         *
         * */
        Calendar from = Calendar.getInstance();
        from.add(Calendar.YEAR, -1);
        Calendar to = Calendar.getInstance();
        to.add(Calendar.YEAR, 1);

        Page<Damage> damages = this.damageRepository.getAllForInstitutionFromTo(102L,
                from.getTime(), to.getTime(),
                new PageRequest(0, 2, Sort.Direction.ASC, "id"));

        assertEquals(2, damages.getTotalElements());

    }

    @Test
    public void testGetAllForRepair() {
         /*
         * Test proverava ispravnost rada metode getAllForRepair repozitorijuma DamageRepository
         * (pronalazenje kvarova koje firma/institucija sa zadatim id-em treba da popravi)
         * */

        Page<Damage> damages = this.damageRepository.getAllForRepair(101L,
                new PageRequest(0, 2, Sort.Direction.ASC, "id"));

        assertEquals(1, damages.getTotalElements());
        assertEquals(new Long(103), damages.getContent().get(0).getId());
    }

    @Test
    public void testGetAllForRepairFromTo() {
          /*
         * Test proverava ispravnost rada metode getAllForRepairFromTo repozitorijuma DamageRepository
         * (pronalazenje kvarova koje firma/institucija sa zadatim id-em treba da popravi u odredjenom periodu)
         * */

        Calendar from = Calendar.getInstance();
        from.add(Calendar.YEAR, -1);
        Calendar to = Calendar.getInstance();
        to.add(Calendar.YEAR, 1);

        Page<Damage> damages = this.damageRepository.getAllForRepairFromTo(101L,
                from.getTime(), to.getTime(),
                new PageRequest(0, 2, Sort.Direction.ASC, "id"));

        assertEquals(1, damages.getTotalElements());
        assertEquals(new Long(103), damages.getContent().get(0).getId());
    }

    @Test
    public void testGetAllInBuildingFromTo() {
         /*
         * Test proverava ispravnost rada metode findByBuildingIdFromTo repozitorijuma DamageRepository
         * (pronalazenje neresenih kvarova koji pripadaju zgradi sa zadatim ID-em u odredjenom vremenskom intervalu)
         * */

        Calendar from = Calendar.getInstance();
        from.add(Calendar.YEAR, -1);
        Calendar to = Calendar.getInstance();
        to.add(Calendar.YEAR, 1);

        Page<Damage> damages = this.damageRepository.getAllInBuildingFromTo(100L,
                from.getTime(), to.getTime(),
                new PageRequest(0, 2, Sort.Direction.ASC, "id"));
        assertEquals(5, damages.getTotalElements());

    }

    @Test
    public void testGeAllForTenant() {
        /*
         * Test proverava ispravnost rada metode getAllForTenant repozitorijuma DamageRepository
         * (pronalazenje neresenih kvarova za koje je stanar odgovoran)
         * */

        Page<Damage> damages = this.damageRepository.getAllForTenant(103L, new PageRequest(0, 2, Sort.Direction.ASC, "id"));
        assertEquals(3, damages.getTotalElements());
    }

    @Test
    public void testGeAllForTenantFromTo() {
        /*
         * Test proverava ispravnost rada metode getAllForTenantFromTo repozitorijuma DamageRepository
         * (pronalazenje neresenih kvarova za koje je stanar odgovoran u odredjenom periodu)
         * */

        Calendar from = Calendar.getInstance();
        from.add(Calendar.YEAR, -1);
        Calendar to = Calendar.getInstance();
        to.add(Calendar.YEAR, 1);

        Page<Damage> damages = this.damageRepository.getAllForTenantFromTo(103L, from.getTime(), to.getTime(),
                new PageRequest(0, 2, Sort.Direction.ASC, "id"));
        assertEquals(3, damages.getTotalElements());
    }

    @Test
    public void testGetAllForPersonalApartments() {
        /*
        * Test proverava ispravnost rada metode getAllInPersonalApartments repozitorijuma DamageRepository
         * (pronalazenje neresenih kvarova u stanovima koje posedujem)
         * */

        Page<Damage> damages = this.damageRepository.getAllInPersonalApartments(101L, new PageRequest(0, 2, Sort.Direction.ASC, "id"));

        assertEquals(5, damages.getTotalElements());

    }

    @Test
    public void testGetAllForPersonalApartmentsFromTo() {
        /*
        * Test proverava ispravnost rada metode getAllInPersonalApartmentsFromTo repozitorijuma DamageRepository
         * (pronalazenje neresenih kvarova u stanovima koje posedujem u odredjenom vremenskom intervalu)
         * */
        Calendar from = Calendar.getInstance();
        from.add(Calendar.YEAR, -1);
        Calendar to = Calendar.getInstance();
        to.add(Calendar.YEAR, 1);

        Page<Damage> damages = this.damageRepository.getAllInPersonalApartmentsFromTo(101L, from.getTime(), to.getTime(),
                new PageRequest(0, 2, Sort.Direction.ASC, "id"));


        assertEquals(5, damages.getTotalElements());

    }

    @Test
    public void testGetAllForApartment()
    {
        /*
        * Test proverava ispravnost rada metode GetAllForApartment repozitorijuma DamageRepository
         * (pronalazenje neresenih kvarova u stanu sa zadatim id-em)
         * */


        Page<Damage> damages = this.damageRepository.getAllInApartment(101L,
                new PageRequest(0, 2, Sort.Direction.ASC, "id"));

        assertEquals(4, damages.getTotalElements());
    }

    @Test
    public void testGetAllForApartmentFromTo()
    {
        /*
        * Test proverava ispravnost rada metode GetAllForApartmentFromTo repozitorijuma DamageRepository
         * (pronalazenje neresenih kvarova u stanu sa zadatim id-em u odredjenom vremenskom periodu)
         * */
        Calendar from = Calendar.getInstance();
        from.add(Calendar.YEAR, - 1);
        Calendar to = Calendar.getInstance();
        to.add(Calendar.YEAR, 1);

        Page<Damage> damages = this.damageRepository.getAllInApartmentFromTo(101L, from.getTime(), to.getTime(),
                new PageRequest(0, 2, Sort.Direction.ASC, "id"));

        assertEquals(4 , damages.getTotalElements());
    }


    @Test
    public void testGetAllMyDamages()
    {
        /*
         * Test proverava ispravnost rada metode getAllMyDamages repozitorijuma DamageRepository
         * (pronalazenje neresenih kvarova koje sam ja prijavio)
         * */

        Page<Damage> damages = this.damageRepository.getAllMyDamages(101L,  new PageRequest(0, 2, Sort.Direction.ASC, "id"));
        assertEquals(3, damages.getTotalElements());
    }

    @Test
    public void testGetAllMyDamagesFromTo()
    {
         /*
         * Test proverava ispravnost rada metode getAllMyDamages repozitorijuma DamageRepository
         * (pronalazenje neresenih kvarova koje sam ja prijavio u odredjenom periodu)
         * */

        Calendar from = Calendar.getInstance();
        from.add(Calendar.YEAR, - 1);
        Calendar to = Calendar.getInstance();
        to.add(Calendar.YEAR, 1);

        Page<Damage> damages = this.damageRepository.getAllMyDamagesFromTo(101L,from.getTime(), to.getTime(),
                new PageRequest(0, 2, Sort.Direction.ASC, "id"));

        assertEquals(3, damages.getTotalElements());

    }

    @Test
    public void testFindDamageIdInOfferRequest()
    {
        /*
         * Test proverava ispravnost rada metode findDamageIdInOfferRequest repozitorijuma DamageRepository
         * (pronalazenje kvara za koji je trazena ponuda od strane neke firme ili institucije)
         * */

        Damage damage = this.damageRepository.findDamageIdInOfferRequest(102L, 101L);

        assertNotNull(damage);
    }


}
