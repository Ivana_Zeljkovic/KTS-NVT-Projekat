import { Component } from '@angular/core';
import { JwtService } from "./core/services/jwt.service";
import { Router } from "@angular/router";
import { Title } from "@angular/platform-browser";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(private jwtService: JwtService, private router: Router, private titleService: Title) {
    this.titleService.setTitle('KTS, NWT project');
  }

  isOnRegistrationRoute() {
    return (this.router.url.startsWith('/registration') || this.router.url === '/admins/register');
  }
}
