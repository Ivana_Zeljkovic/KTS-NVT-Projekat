package rs.ac.uns.ftn.ktsnvt.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;
import rs.ac.uns.ftn.ktsnvt.controller.util.TestUtil;
import rs.ac.uns.ftn.ktsnvt.web.dto.CommentCreateOrUpdateDTO;
import rs.ac.uns.ftn.ktsnvt.web.dto.LoginDTO;
import rs.ac.uns.ftn.ktsnvt.web.dto.TokenDTO;

import javax.annotation.PostConstruct;
import java.nio.charset.Charset;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


/**
 * Created by Nikola Garabandic on 16/01/2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class CommentControllerIntegrationTest {

    private static final String URL_PREFIX = "/api";

    private MediaType contentType = new MediaType(
            MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    private MockMvc mockMvc;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja firme
    private String accessTokenFirm_1;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja institucije
    private String accessTokenInstitution_1;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja stanara
    private String accessTokenTenant_1, accessTokenTenant_2, accessTokenTenant_3, accessTokenTenant_4;
    private MultiValueMap<String, String> params;


    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private TestRestTemplate testRestTemplate;


    @PostConstruct
    public void setup() {
        this.mockMvc = MockMvcBuilders.
                webAppContextSetup(webApplicationContext).apply(springSecurity()).build();
    }

    // pre izvrsavanja testa, neophodno je izvrsiti logovanje u aplikaciju da bismo dobili token
    @Before
    public void login() throws Exception {
        // logovanje firme
        LoginDTO loginDTOFirm = new LoginDTO("firm2", "firm2");

        ResponseEntity<TokenDTO> responseEntityFirm =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOFirm,
                        TokenDTO.class);
        // preuzmemo token za logovanu firmu iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenFirm_1 = responseEntityFirm.getBody().getValue();


        // logovanje institucije
        LoginDTO loginDTOInstitution = new LoginDTO("inst2", "inst2");

        ResponseEntity<TokenDTO> responseEntityInstitution =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOInstitution,
                        TokenDTO.class);
        // preuzmemo token za logovanu instituciju iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenInstitution_1 = responseEntityInstitution.getBody().getValue();


        // logovanje stanara
        LoginDTO loginDTOTenant = new LoginDTO("jasmina", "jasmina");

        ResponseEntity<TokenDTO> responseEntityTenant =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOTenant,
                        TokenDTO.class);
        // preuzmemo token za logovanog stanara iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenTenant_1 = responseEntityTenant.getBody().getValue();

        // logovanje stanara 2
        LoginDTO loginDTOTenant2 = new LoginDTO("kaca", "kaca");

        ResponseEntity<TokenDTO> responseEntityTenant2 =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOTenant2,
                        TokenDTO.class);
        // preuzmemo token za logovanog stanara iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenTenant_2 = responseEntityTenant2.getBody().getValue();


        // logovanje stanara 3
        LoginDTO loginDTOTenant3 = new LoginDTO("jovanka", "jovanka");

        ResponseEntity<TokenDTO> responseEntityTenant3 =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOTenant3,
                        TokenDTO.class);
        // preuzmemo token za logovanog stanara iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenTenant_3 = responseEntityTenant3.getBody().getValue();


        // logovanje stanara 4
        LoginDTO loginDTOTenant4 = new LoginDTO("leontina", "leontina");

        ResponseEntity<TokenDTO> responseEntityTenant4 =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOTenant4,
                        TokenDTO.class);
        // preuzmemo token za logovanog stanara iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenTenant_4 = responseEntityTenant4.getBody().getValue();

        // za pageable atribut
        params = TestUtil.getParams(
                "0", "10", "ASC", "id", "");
    }


    // TESTOVI za metodu getAllCommentsForDamage
    @Test
    public void testGetAllCommentsForDamage() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllCommentsForDamage kontrolera CommentController
        * (dobavljanje liste svih komentara koji pripadaju kvaru sa zadatim ID-em)
        * */

        /*
        kvar sa ID-em = 101 ima jedan komentar sa sledecim podacima:
            id = 100
            content = This is comment
            creator_id = 101
        */
        this.mockMvc.perform(get(URL_PREFIX + "/damages/101/comments")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .params(params))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$.[*].id").value(100))
                .andExpect(jsonPath("$.[*].content").value(hasItem("This is comment")))
                .andExpect(jsonPath("$.[*].creatorUsername").value(hasItem("kaca")));
    }

    @Test
    public void testGetAllCommentsForDamageByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllCommentsForDamage kontrolera CommentController
        * (dobavljanje liste svih komentara koji pripadaju kvaru sa zadatim ID-em)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        /*
        kvar sa ID-em = 101 ima jedan komentar sa sledecim podacima:
            id = 100
            content = This is comment
            creator_id = 101
        */
        this.mockMvc.perform(get(URL_PREFIX + "/damages/101/comments")
                .params(params))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetAllCommentsForDamageU() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllCommentsForDamage kontrolera CommentController
        * (dobavljanje liste svih komentara koji pripadaju kvaru sa zadatim ID-em)
        * u slucaju kada ne postoji kvar sa zadatim ID-em
        * */

        // ne postoji kvar sa ID-em = 200
        this.mockMvc.perform(get(URL_PREFIX + "/damages/200/comments")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .params(params))
                .andExpect(status().isNotFound());
    }

    // TESTOVI za metodu postComment
    @Test
    @Transactional
    @Rollback(true)
    public void testPostCommentAsInstitution() throws Exception{
       /*
        * Test proverava ispravnost rada metode postComment kontrolera CommentController
        * (postavljanje komentara na neki kvar)
        * u slucaju kada komentar ostavlja odgovorna institucija za zadati kvar
        * */

        CommentCreateOrUpdateDTO commentCreateOrUpdateDTO = new CommentCreateOrUpdateDTO();
        commentCreateOrUpdateDTO.setContent("This is comment");

        String commentDtoJson = TestUtil.json(commentCreateOrUpdateDTO);

        // institucija sa ID-em = 103 je odgovorna za kvar sa ID-em = 103 u zgradi sa ID-em = 100
        mockMvc.perform(post(URL_PREFIX + "/buildings/100/damages/103/comments")
                .header("Authentication-Token", this.accessTokenInstitution_1)
                .contentType(contentType)
                .content(commentDtoJson))
                .andExpect(status().isCreated());
    }

    @Test
    @Transactional
    @Rollback(true)
    public void testPostCommentAsFirm() throws Exception{
       /*
        * Test proverava ispravnost rada metode postComment kontrolera CommentController
        * (postavljanje komentara na neki kvar)
        * u slucaju kada komentar ostavlja firma koja je izabrana za resavanje tog kvara
        * */

        CommentCreateOrUpdateDTO commentCreateOrUpdateDTO = new CommentCreateOrUpdateDTO();
        commentCreateOrUpdateDTO.setContent("This is comment");

        String commentDtoJson = TestUtil.json(commentCreateOrUpdateDTO);

        // firma sa ID-em = 101 je izabrana da resi kvar sa ID-em = 103 u zgradi sa ID-em = 100
        mockMvc.perform(post(URL_PREFIX + "/buildings/100/damages/103/comments")
                .header("Authentication-Token", this.accessTokenFirm_1)
                .contentType(contentType)
                .content(commentDtoJson))
                .andExpect(status().isCreated());
    }

    @Test
    @Transactional
    @Rollback(true)
    public void testPostCommentAsTenant() throws Exception{
         /*
        * Test proverava ispravnost rada metode postComment kontrolera CommentController
        * (postavljanje komentara na neki kvar)
        * u slucaju kada komentar ostavlja odgovorni stanar za zadati kvar
        * */

        CommentCreateOrUpdateDTO commentCreateOrUpdateDTO = new CommentCreateOrUpdateDTO();
        commentCreateOrUpdateDTO.setContent("This is comment");

        String commentDtoJson = TestUtil.json(commentCreateOrUpdateDTO);

        // stanar sa ID-em = 103 je odgovorno lice za kvar sa ID-em = 103 u zgradi sa ID-em = 100
        mockMvc.perform(post(URL_PREFIX + "/buildings/100/damages/103/comments")
                .header("Authentication-Token", this.accessTokenTenant_1)
                .contentType(contentType)
                .content(commentDtoJson))
                .andExpect(status().isCreated());

    }

    @Test
    public void testPostCommentAsAnonymous() throws Exception{
       /*
        * Test proverava ispravnost rada metode postComment kontrolera CommentController
        * (postavljanje komentara na neki kvar)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        CommentCreateOrUpdateDTO commentCreateOrUpdateDTO = new CommentCreateOrUpdateDTO();
        commentCreateOrUpdateDTO.setContent("This is comment");

        String commentDtoJson = TestUtil.json(commentCreateOrUpdateDTO);

        mockMvc.perform(post(URL_PREFIX + "/buildings/100/damages/103/comments")
                .contentType(contentType)
                .content(commentDtoJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testPostCommentBuildingNotFound() throws Exception{
         /*
        * Test proverava ispravnost rada metode postComment kontrolera CommentController
        * (postavljanje komentara na neki kvar)
        * pri cemu ne postoji nijedna zgrada sa zadatim ID-em
        * */

        CommentCreateOrUpdateDTO commentCreateOrUpdateDTO = new CommentCreateOrUpdateDTO();
        commentCreateOrUpdateDTO.setContent("This is comment");

        String commentDtoJson = TestUtil.json(commentCreateOrUpdateDTO);

        // ne postoji zgrada sa ID-em = 200
        mockMvc.perform(post(URL_PREFIX + "/buildings/200/damages/103/comments")
                .header("Authentication-Token", this.accessTokenTenant_1)
                .contentType(contentType)
                .content(commentDtoJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testPostCommentDamageNotFound() throws Exception{
       /*
        * Test proverava ispravnost rada metode postComment kontrolera CommentController
        * (postavljanje komentara na neki kvar)
        * pri cemu ne postoji nijedan kvar sa zadatim ID-em
        * */

        CommentCreateOrUpdateDTO commentCreateOrUpdateDTO = new CommentCreateOrUpdateDTO();
        commentCreateOrUpdateDTO.setContent("This is comment");

        String commentDtoJson = TestUtil.json(commentCreateOrUpdateDTO);

        // ne postoji kvar sa ID-em = 200
        mockMvc.perform(post(URL_PREFIX + "/buildings/100/damages/200/comments")
                .header("Authentication-Token", this.accessTokenTenant_1)
                .contentType(contentType)
                .content(commentDtoJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testPostCommentDamageNotInBuilding() throws Exception{
         /*
        * Test proverava ispravnost rada metode postComment kontrolera CommentController
        * (postavljanje komentara na neki kvar)
        * pri cemu kvar sa zadatim ID-em ne pripada zgradi sa zadatim ID-em
        * */

        CommentCreateOrUpdateDTO commentCreateOrUpdateDTO = new CommentCreateOrUpdateDTO();
        commentCreateOrUpdateDTO.setContent("This is comment");

        String commentDtoJson = TestUtil.json(commentCreateOrUpdateDTO);

        // kvar sa ID-em = 103 ne pripada zgradi sa ID-em = 101 (vec sa ID-em = 100)
        mockMvc.perform(post(URL_PREFIX + "/buildings/101/damages/103/comments")
                .header("Authentication-Token", this.accessTokenTenant_1)
                .contentType(contentType)
                .content(commentDtoJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testPostCommentDamageNoResponsibleInstitutionAndBusiness() throws Exception{
       /*
        * Test proverava ispravnost rada metode postComment kontrolera CommentController
        * (postavljanje komentara na neki kvar)
        * pri cemu je trenutno logovana institucija, ali izabrani kvar nema odgovornu instituciju niti izabranu firmu/instituciju
        * koja ce taj kvar da resi
        * */

        CommentCreateOrUpdateDTO commentCreateOrUpdateDTO = new CommentCreateOrUpdateDTO();
        commentCreateOrUpdateDTO.setContent("This is comment");

        String commentDtoJson = TestUtil.json(commentCreateOrUpdateDTO);

        // kvar sa ID-em = 102 nema odgovornu instituciju niti izabranu firmu/instituciju koja ce taj kvar otkloniti
        mockMvc.perform(post(URL_PREFIX + "/buildings/100/damages/102/comments")
                .header("Authentication-Token", this.accessTokenInstitution_1)
                .contentType(contentType)
                .content(commentDtoJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testPostCommentDamageNotResponsibleInstitutionOrBusiness() throws Exception{
       /*
        * Test proverava ispravnost rada metode postComment kontrolera CommentController
        * (postavljanje komentara na neki kvar)
        * pri cemu trenutno logovana institucija nije ujedno i odgovorna institucija za izabrani kvar u izabranoj zgradi niti je ujedno izabrana
        * institucija koja ce da resi taj kvar
        * */

        CommentCreateOrUpdateDTO commentCreateOrUpdateDTO = new CommentCreateOrUpdateDTO();
        commentCreateOrUpdateDTO.setContent("This is comment");

        String commentDtoJson = TestUtil.json(commentCreateOrUpdateDTO);

        // kvar sa ID-em = 104 ima odgovornu instituciju sa ID-em = 102 i instituciju koja ce taj kvar da resi = 102
        // pri cemu je trenutno ulogovana institucija sa ID-em = 103
        mockMvc.perform(post(URL_PREFIX + "/buildings/100/damages/104/comments")
                .header("Authentication-Token", this.accessTokenInstitution_1)
                .contentType(contentType)
                .content(commentDtoJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testPostCommentByTenantWithoutApartment() throws Exception {
       /*
        * Test proverava ispravnost rada metode postComment kontrolera CommentController
        * (postavljanje komentara na neki kvar)
        * pri cemu akciju izvrsava stanar koji nije povezan ni sa jednim stanom
        * */

        CommentCreateOrUpdateDTO commentCreateOrUpdateDTO = new CommentCreateOrUpdateDTO();
        commentCreateOrUpdateDTO.setContent("This is comment");

        String commentDtoJson = TestUtil.json(commentCreateOrUpdateDTO);

        /*
        stanar sa ID-em = 108 nije povezan ni sa jednim stanom jos uvek, i ima podatke:
            korisnicko ime = jovanka,
            lozinka = jovanka
            id = 108
        */
        this.mockMvc.perform(post(URL_PREFIX + "/buildings/100/damages/104/comments")
                .header("Authentication-Token", this.accessTokenTenant_3)
                .contentType(contentType)
                .content(commentDtoJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testPostCommentByTenantFromInvalidBuilding() throws Exception {
       /*
        * Test proverava ispravnost rada metode postComment kontrolera CommentController
        * (postavljanje komentara na neki kvar)
        * pri cemu akciju izvrsava stanar koji ne zivi u zadatoj zgradi
        * */

        CommentCreateOrUpdateDTO commentCreateOrUpdateDTO = new CommentCreateOrUpdateDTO();
        commentCreateOrUpdateDTO.setContent("This is comment");

        String commentDtoJson = TestUtil.json(commentCreateOrUpdateDTO);

        /*
        stanar sa ID-em = 107 zivi u zgradi sa ID-em = 101 (a ne sa ID-em = 100) i ima podatke:
            korisnicko ime = leontina,
            lozinka = leontina,
            apartment_id = 107 (building_id = 101)
         */
        this.mockMvc.perform(post(URL_PREFIX + "/buildings/100/damages/104/comments")
                .header("Authentication-Token", this.accessTokenTenant_4)
                .contentType(contentType)
                .content(commentDtoJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testPostCommentDamageNoResponsiblePerson() throws Exception {
       /*
        * Test proverava ispravnost rada metode postComment kontrolera CommentController
        * (postavljanje komentara na neki kvar)
        * pri cemu je trenutno logovan stanar, ali zadati kvar nema odgovornu osobu
        * */

        CommentCreateOrUpdateDTO commentCreateOrUpdateDTO = new CommentCreateOrUpdateDTO();
        commentCreateOrUpdateDTO.setContent("This is comment");

        String commentDtoJson = TestUtil.json(commentCreateOrUpdateDTO);

        // kvar sa ID-em = 105 u zgradi sa ID-em = 101 nema odgovornu osobu
        this.mockMvc.perform(post(URL_PREFIX + "/buildings/101/damages/105/comments")
                .header("Authentication-Token", this.accessTokenTenant_4)
                .contentType(contentType)
                .content(commentDtoJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testPostCommentDamageNotResponsiblePerson() throws Exception{
       /*
        * Test proverava ispravnost rada metode postComment kontrolera CommentController
        * (postavljanje komentara na neki kvar)
        * pri cemu trenutno logovani stanar nije ujedno i odgovorna osoba za izabrani kvar
        * */

        CommentCreateOrUpdateDTO commentCreateOrUpdateDTO = new CommentCreateOrUpdateDTO();
        commentCreateOrUpdateDTO.setContent("This is comment");

        String commentDtoJson = TestUtil.json(commentCreateOrUpdateDTO);

        // kvar sa ID-em = 102 u zgradi sa ID-em = 100 ima odgovornu osobu sa ID-em = 103
        mockMvc.perform(post(URL_PREFIX + "/buildings/100/damages/102/comments")
                .header("Authentication-Token", this.accessTokenTenant_2)
                .contentType(contentType)
                .content(commentDtoJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testPostCommentDamageNoSelectedBusiness() throws Exception {
       /*
        * Test proverava ispravnost rada metode postComment kontrolera CommentController
        * (postavljanje komentara na neki kvar)
        * pri cemu je trenutno logovana firma, ali zadati kvar nema izabranu firmu/instituciju za njegovo otklanjanje
        * */

        CommentCreateOrUpdateDTO commentCreateOrUpdateDTO = new CommentCreateOrUpdateDTO();
        commentCreateOrUpdateDTO.setContent("This is comment");

        String commentDtoJson = TestUtil.json(commentCreateOrUpdateDTO);

        // kvar sa ID-em = 102 u zgradi sa ID-em = 100 nema izabranu firmu za njegovo otklanjanje
        this.mockMvc.perform(post(URL_PREFIX + "/buildings/100/damages/102/comments")
                .header("Authentication-Token", this.accessTokenFirm_1)
                .contentType(contentType)
                .content(commentDtoJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testPostCommentDamageNotSelectedBusiness() throws Exception{
       /*
        * Test proverava ispravnost rada metode postComment kontrolera CommentController
        * (postavljanje komentara na neki kvar)
        * pri cemu trenutno logovana firma nije ujedno i izabrana firma/institucija za otklanjanje izabranog kvara
        * */

        CommentCreateOrUpdateDTO commentCreateOrUpdateDTO = new CommentCreateOrUpdateDTO();
        commentCreateOrUpdateDTO.setContent("This is comment");

        String commentDtoJson = TestUtil.json(commentCreateOrUpdateDTO);

        // kvar sa ID-em = 104 u zgradi sa ID-em = 100 ima izabranu instituciju sa ID-em = 102
        mockMvc.perform(post(URL_PREFIX + "/buildings/100/damages/102/comments")
                .header("Authentication-Token", this.accessTokenFirm_1)
                .contentType(contentType)
                .content(commentDtoJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testPostCommentMissingContent() throws Exception {
       /*
        * Test proverava ispravnost rada metode postComment kontrolera CommentController
        * (postavljanje komentara na neki kvar)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne poseduje sva polja neohodna za kreiranje - nedostaje polje content
        * */

        // nedostaje content vrednost
        CommentCreateOrUpdateDTO commentCreateOrUpdateDTO = new CommentCreateOrUpdateDTO();

        String commentDtoJson = TestUtil.json(commentCreateOrUpdateDTO);

        // stanar sa ID-em = 103 je odgovorno lice za kvar sa ID-em = 103 u zgradi sa ID-em = 100
        mockMvc.perform(post(URL_PREFIX + "/buildings/100/damages/103/comments")
                .header("Authentication-Token", this.accessTokenTenant_1)
                .contentType(contentType)
                .content(commentDtoJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testPostCommentInappropriateTypeOfContent() throws Exception {
        /*
        * Test proverava ispravnost rada metode postComment kontrolera CommentController
        * (postavljanje komentara na neki kvar)
        * u slucaju kada je dolazni DTO objekat nevalidan: poseduje sva polja
        * ali je tip podatka u polju content nevalidan (nije String nego int)
        * */

        // content vrednost je neodgovarajuceg tipa (int)
        String commentDtoJson = "{content:1}";

        // stanar sa ID-em = 103 je odgovorno lice za kvar sa ID-em = 103 u zgradi sa ID-em = 100
        mockMvc.perform(post(URL_PREFIX + "/buildings/100/damages/103/comments")
                .header("Authentication-Token", this.accessTokenTenant_1)
                .contentType(contentType)
                .content(commentDtoJson))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za metodu deleteComment
    @Test
    @Transactional
    @Rollback(true)
    public void testDeleteComment() throws Exception{
        /*
        * Test proverava ispravnost rada metode deleteComment kontrolera CommentController
        * (brisanje komentara sa kvara)
        * kada su svi podaci validni
        * */

        mockMvc.perform(delete(URL_PREFIX + "/damages/101/comments/100")
                        .header("Authentication-Token", accessTokenTenant_2))
                        .andExpect(status().isOk());

    }

    @Test
    public void testDeleteCommentInvalid() throws Exception{
        /*
        * Test proverava ispravnost rada metode deleteComment kontrolera CommentController
        * (brisanje komentara sa kvara)
        *  u slucaju kada ne postoji komentar sa zadatim ID-em
        * */

        // ne postoji komentar sa ID-em = 200
        mockMvc.perform(delete(URL_PREFIX + "/damages/101/comments/200")
                .header("Authentication-Token", accessTokenTenant_2))
                .andExpect(status().isNotFound());

    }

    // TESTOVI za metodu updateComment
    @Test
    @Transactional
    @Rollback(true)
    public void testUpdateComment() throws Exception{
        /*
        * Test proverava ispravnost rada metode updateComment kontrolera CommentController
        * (azuriranje komentara sa kvara)
        * kada su svi podaci validni
        * */

        String content = "{\"content\":  \"Azuriran komentar\"}";

        mockMvc.perform(put(URL_PREFIX + "/damages/101/comments/100")
                .header("Authentication-Token", accessTokenTenant_2)
                .contentType(contentType)
                .content(content))
                .andExpect(status().isOk());
    }

    @Test
    public void testUpdateCommentInvalid() throws Exception{
        /*
        * Test proverava ispravnost rada metode updateComment kontrolera CommentController
        * (azuriranje komentara sa kvara)
        * u slucaju kada dolazni DTO nije validan: nedostaje vrednost polja content
        * */

        String content = "{\"content\":}";

        mockMvc.perform(put(URL_PREFIX + "/damages/101/comments/100")
                .header("Authentication-Token", accessTokenTenant_2)
                .contentType(contentType)
                .content(content))
                .andExpect(status().isBadRequest());
    }
}
