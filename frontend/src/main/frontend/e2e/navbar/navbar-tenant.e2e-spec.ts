import { browser } from "protractor";
// page
import { NavbarPage } from "../page-objects/navbar.po";
import { LoginPage } from "../page-objects/login.po";
import { CouncilMemberPage } from "../page-objects/council-member.po";


describe('Navbar for tenants review', () => {
  let navbarPage: NavbarPage;
  let loginPage: LoginPage;
  let councilMemberPage: CouncilMemberPage;

  beforeEach(() => {
    loginPage = new LoginPage();
    navbarPage = new NavbarPage();

    browser.get('/');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');
  });

  afterEach(() => {
    browser.executeScript('window.localStorage.clear();');
  });



  it('should display navbar for council president when user is logged in', () => {
    loginPage.setUsername('kaca');
    loginPage.setPassword('kaca');
    loginPage.getSignInButton().click();

    browser.wait(function() {
      // expect that browser redirect user to home page and navbar page is loaded
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/home/notifications' && navbarPage.getNavbarElement().isDisplayed();
      });
    }).then(function() {
      /*
      expected view of navbar for president:
        Home link
        Damages dropdown
        Council dropdown
        Questionnaires dropdown
        Personal dropdown (with username of current user as text)
      */
      expect(navbarPage.getHomeLink().isDisplayed()).toBe(true);
      expect(navbarPage.getDamageDropdownLink().isDisplayed()).toBe(true);
      expect(navbarPage.getCouncilDropdownLink().isDisplayed()).toBe(true);
      expect(navbarPage.getQuestionnairesDropdownLink().isDisplayed()).toBe(true);
      expect(navbarPage.getPersonalDropdown().isDisplayed()).toBe(true);
      expect(navbarPage.getPersonalDropdown().getText()).toEqual('kaca');

      /*
      Damages dropdown contains links:
        Damages I am responsible for
        Damages I reported
        Damages in my building
      */
      navbarPage.getDamageDropdownLink().click();
      expect(navbarPage.getDamagesResponsibleForLink().isDisplayed()).toBe(true);
      expect(navbarPage.getDamagesIReportedLink().isDisplayed()).toBe(true);
      expect(navbarPage.getDamagesInMyBuildingLink(3).isDisplayed()).toBe(true);

      /*
      Council dropdown contains links:
        Meetings
        Records
        Choose new president
      */
      navbarPage.getCouncilDropdownLink().click();
      expect(navbarPage.getMeetingsLink().isPresent()).toBe(true);
      expect(navbarPage.getMeetingsLink().getText()).toEqual('Meetings');
      expect(navbarPage.getRecordsLink().isDisplayed()).toBe(true);
      expect(navbarPage.getResignateLink().isDisplayed()).toBe(true);

      /*
      Questionnaires dropdown contains links:
        Results
      */
      navbarPage.getQuestionnairesDropdownLink().click();
      expect(navbarPage.getResultsLink(1).isDisplayed()).toBe(true);

      /*
      Personal dropdown contains links:
        Profile
        Sing out
      */
      navbarPage.getPersonalDropdown().click();
      expect(navbarPage.getProfileLink().isDisplayed()).toBe(true);
      expect(navbarPage.getLogoutLink().isDisplayed()).toBe(true);
    });
  });



  it('should display navbar for council member when user is logged in', () => {
    loginPage.setUsername('sanja');
    loginPage.setPassword('sanja');
    loginPage.getSignInButton().click();

    browser.wait(function() {
      // expect that browser redirect user to home page and navbar page is loaded
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/home/notifications' && navbarPage.getNavbarElement().isDisplayed();
      });
    }).then(function() {
      /*
      expected view of navbar for president:
        Home link
        Damages dropdown
        Council dropdown
        Questionnaires dropdown
        Personal dropdown (with username of current user as text)
      */
      expect(navbarPage.getHomeLink().isDisplayed()).toBe(true);
      expect(navbarPage.getDamageDropdownLink().isDisplayed()).toBe(true);
      expect(navbarPage.getCouncilDropdownLink().isDisplayed()).toBe(true);
      expect(navbarPage.getQuestionnairesDropdownLink().isDisplayed()).toBe(true);
      expect(navbarPage.getPersonalDropdown().isDisplayed()).toBe(true);
      expect(navbarPage.getPersonalDropdown().getText()).toEqual('sanja');

      /*
      Damages dropdown contains links:
        Damages I am responsible for
        Damages I reported
        Damages in my apartment
      */
      navbarPage.getDamageDropdownLink().click();
      expect(navbarPage.getDamagesResponsibleForLink().isDisplayed()).toBe(true);
      expect(navbarPage.getDamagesIReportedLink().isDisplayed()).toBe(true);
      expect(navbarPage.getDamagesInMyApartmentLink().isDisplayed()).toBe(true);

      /*
      Council dropdown contains links:
        Upcomming meetings
        Records
        Delegate membership of council
      */
      navbarPage.getCouncilDropdownLink().click();
      expect(navbarPage.getMeetingsLink().isPresent()).toBe(true);
      expect(navbarPage.getMeetingsLink().getText()).toEqual('Upcomming meetings');
      expect(navbarPage.getRecordsLink().isDisplayed()).toBe(true);
      expect(navbarPage.getDelegateCouncilResponsibilityLink(3).isDisplayed()).toBe(true);

      /*
      Questionnaires dropdown contains links:
        Results
      */
      navbarPage.getQuestionnairesDropdownLink().click();
      expect(navbarPage.getResultsLink(1).isDisplayed()).toBe(true);

      /*
      Personal dropdown contains links:
        Profile
        Sing out
      */
      navbarPage.getPersonalDropdown().click();
      expect(navbarPage.getProfileLink().isDisplayed()).toBe(true);
      expect(navbarPage.getLogoutLink().isDisplayed()).toBe(true);
    });
  });



  it('should display navbar for tenant who is owner of one or more apartments when user is logged in', () => {
    loginPage.setUsername('nikola');
    loginPage.setPassword('nikola');
    loginPage.getSignInButton().click();

    browser.wait(function() {
      // expect that browser redirect user to home page and navbar page is loaded
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/home/notifications' && navbarPage.getNavbarElement().isDisplayed();
      });
    }).then(function() {
      /*
      expected view of navbar for president:
        Home link
        Damages dropdown
        Questionnaires dropdown
        Personal dropdown (with username of current user as text)
      */
      expect(navbarPage.getHomeLink().isDisplayed()).toBe(true);
      expect(navbarPage.getDamageDropdownLink().isDisplayed()).toBe(true);
      expect(navbarPage.getQuestionnairesDropdownLink().isDisplayed()).toBe(true);
      expect(navbarPage.getPersonalDropdown().isDisplayed()).toBe(true);
      expect(navbarPage.getPersonalDropdown().getText()).toEqual('nikola');

      /*
      Damages dropdown contains links:
        Damages I am responsible for
        Damages I reported
        Damages in my apartment
        Damages in my personal apartments
      */
      navbarPage.getDamageDropdownLink().click();
      expect(navbarPage.getDamagesResponsibleForLink().isDisplayed()).toBe(true);
      expect(navbarPage.getDamagesIReportedLink().isDisplayed()).toBe(true);
      expect(navbarPage.getDamagesInMyApartmentLink().isDisplayed()).toBe(true);
      expect(navbarPage.getDamagesInMyPersonalApartmentsLink(4).isDisplayed()).toBe(true);

      /*
      Questionnaires dropdown contains links:
        Active questionnaires
      */
      navbarPage.getQuestionnairesDropdownLink().click();
      expect(navbarPage.getActiveQuestionnairesLink().isDisplayed()).toBe(true);

      /*
      Personal dropdown contains links:
        Profile
        Sing out
      */
      navbarPage.getPersonalDropdown().click();
      expect(navbarPage.getProfileLink().isDisplayed()).toBe(true);
      expect(navbarPage.getLogoutLink().isDisplayed()).toBe(true);
    });
  });



  it('should display navbar for tenant who isn\'t owner of one or more apartments and who isn\'t council member' +
    ' when user is logged in', () => {
    loginPage.setUsername('jasmina');
    loginPage.setPassword('jasmina');
    loginPage.getSignInButton().click();

    browser.wait(function() {
      // expect that browser redirect user to home page and navbar page is loaded
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/home/notifications' && navbarPage.getNavbarElement().isDisplayed();
      });
    }).then(function() {
      /*
      expected view of navbar for president:
        Home link
        Damages dropdown
        Personal dropdown (with username of current user as text)
      */
      expect(navbarPage.getHomeLink().isDisplayed()).toBe(true);
      expect(navbarPage.getDamageDropdownLink().isDisplayed()).toBe(true);
      expect(navbarPage.getPersonalDropdown().isDisplayed()).toBe(true);
      expect(navbarPage.getPersonalDropdown().getText()).toEqual('jasmina');

      /*
      Damages dropdown contains links:
        Damages I am responsible for
        Damages I reported
        Damages in my apartment
      */
      navbarPage.getDamageDropdownLink().click();
      expect(navbarPage.getDamagesResponsibleForLink().isDisplayed()).toBe(true);
      expect(navbarPage.getDamagesIReportedLink().isDisplayed()).toBe(true);
      expect(navbarPage.getDamagesInMyApartmentLink().isDisplayed()).toBe(true);

      /*
      Personal dropdown contains links:
        Profile
        Sing out
      */
      navbarPage.getPersonalDropdown().click();
      expect(navbarPage.getProfileLink().isDisplayed()).toBe(true);
      expect(navbarPage.getLogoutLink().isDisplayed()).toBe(true);
    });
  });



  it('should open modal dialog for choosing one of roommates of current tenant when he is council member and ' +
    'choose option Delegate membership of council in Council dropdown', () => {
    councilMemberPage = new CouncilMemberPage();
    loginPage.setUsername('sanja');
    loginPage.setPassword('sanja');
    loginPage.getSignInButton().click();

    browser.wait(function() {
      // expect that browser redirect user to home page and navbar page is loaded
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/home/notifications' && navbarPage.getNavbarElement().isDisplayed();
      });
    }).then(function() {
      navbarPage.getCouncilDropdownLink().click();
      expect(navbarPage.getDelegateCouncilResponsibilityLink(3).isDisplayed()).toBe(true);
      navbarPage.getDelegateCouncilResponsibilityLink(3).click();

      browser.wait(function() {
        // expect to show modal dialog for choosing roommate - council member page
        return councilMemberPage.getCouncilMemberElement().isDisplayed();
      }).then(function() {
        expect(councilMemberPage.getCloseButtonFromHeader().isDisplayed()).toBe(true);
        councilMemberPage.getCloseButtonFromHeader().click();
      });
    });
  });



  it('should open modal dialog for choosing new president when current council\'s president is logged in ' +
    'and choose option Choose new president in Council dropdown', () => {
    councilMemberPage = new CouncilMemberPage();
    loginPage.setUsername('kaca');
    loginPage.setPassword('kaca');
    loginPage.getSignInButton().click();

    browser.wait(function() {
      // expect that browser redirect user to home page and navbar page is loaded
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/home/notifications' && navbarPage.getNavbarElement().isDisplayed();
      });
    }).then(function() {
      navbarPage.getCouncilDropdownLink().click();
      expect(navbarPage.getResignateLink().isDisplayed()).toBe(true);
      navbarPage.getResignateLink().click();

      browser.wait(function() {
        // expect to show modal dialog for choosing new president - council member page
        return councilMemberPage.getCouncilMemberElement().isDisplayed();
      }).then(function() {
        expect(councilMemberPage.getCloseButtonFromHeader().isDisplayed()).toBe(true);
        councilMemberPage.getCloseButtonFromHeader().click();
      });
    });
  });
});
