import { NgModule } from '@angular/core';
// module
import { SharedModule } from '../shared/shared.module';
import { DamageTypesRouterModule } from "./damage-types-router.module";
// component
import { DelegateResponsibilityToInstitutionComponent } from "../shared/components/delegate-responsibility-to-institution/delegate-responsibility-to-institution.component";
import { DamageTypesComponent } from './damage-types/damage-types.component';


@NgModule({
  imports: [
    SharedModule,
    DamageTypesRouterModule
  ],
  declarations: [
    DamageTypesComponent
  ],
  entryComponents:[
    DelegateResponsibilityToInstitutionComponent
  ]
})
export class DamageTypesModule { }
