import { Component, OnInit, ViewChild } from '@angular/core';
import { AgmMap } from "@agm/core";
import { ToasterService } from "angular5-toaster/dist";
import { ActivatedRoute, Params, Router } from "@angular/router";
// model
import { Business } from "../../shared/models/business";
// service
import { BusinessService } from "../../core/services/business.service";
import { LocationService } from "../../core/services/location.service";
// error
import { AppError } from "../../shared/errors/app-error";
import {NotFoundError} from "../../shared/errors/not-found-error";
import {ForbiddenError} from "../../shared/errors/forbidden-error";
import {BadRequestError} from "../../shared/errors/bad-request-error";


@Component({
  selector: 'app-business',
  templateUrl: './business.component.html',
  styleUrls: ['./business.component.css']
})
export class BusinessComponent implements OnInit {
  business: Business;
  id: number;
  longitude: number;
  latitude: number;
  zoom: number;
  @ViewChild(AgmMap) map: AgmMap;

  constructor(private businessService: BusinessService, private route: ActivatedRoute,
              private router: Router,  private locationService: LocationService, private toasterService: ToasterService) {
    this.zoom = 14;
    // default coordinates if there are no coordinates for given address
    this.latitude = 45.2461;
    this.longitude = 19.8517;
    this.route.params.subscribe((param: Params) => {
      this.id = param['id'];
      this.getBusiness();
    });
  }

  ngOnInit() {
    this.getBusiness();
  }

  getBusiness(){
    this.businessService.get(this.id, 'businesses').subscribe(data => {
      let businessData = data as Business;
      this.business = businessData;

      if(businessData.address != null){
        this.locationService.getCoordinates(`${businessData.address.street} ${businessData.address.city.name}`)
          .subscribe(response => {
            if (response.status == 'OK') {
              this.latitude = response.results[0].geometry.location.lat;
              this.longitude = response.results[0].geometry.location.lng;
              this.map.triggerResize();
            } else if (response.status == 'ZERO_RESULTS') {
              this.toasterService.pop('error', 'Error', 'Address couldn\'t be found!');
            } else {
              this.toasterService.pop('error','Error','Error in detecting coordinates of given address!');
            }
          }, (error: AppError) => {
            this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
            throw error;
          });
      }
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Businesses not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Error', 'Business does not exist!');
      else {
        this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
        throw error;
      }
    });
  }
}
