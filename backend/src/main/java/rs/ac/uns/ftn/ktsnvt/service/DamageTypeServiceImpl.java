package rs.ac.uns.ftn.ktsnvt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.DamageType;
import rs.ac.uns.ftn.ktsnvt.repository.DamageTypeRepository;

/**
 * Created by Ivana Zeljkovic on 23/11/2017.
 */
@Service
public class DamageTypeServiceImpl implements DamageTypeService {

    @Autowired
    private DamageTypeRepository damageTypeRepository;


    @Override
    @Transactional(readOnly = true)
    public DamageType findOne(Long id) {
        DamageType damageType = this.damageTypeRepository.findOne(id);
        if(damageType == null) throw new NotFoundException("Damage type not found!");
        return damageType;
    }

    @Override
    @Transactional(readOnly = true)
    public DamageType findByName(String name) {
        DamageType damageType = this.damageTypeRepository.findByName(name);
        if(damageType == null) throw new NotFoundException("Damage type with name: " + name + " not found!");
        return damageType;
    }
}
