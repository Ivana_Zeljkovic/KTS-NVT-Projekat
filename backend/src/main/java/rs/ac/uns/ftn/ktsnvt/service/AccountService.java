package rs.ac.uns.ftn.ktsnvt.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import rs.ac.uns.ftn.ktsnvt.model.entity.Account;

import java.util.List;

/**
 * Created by Ivana Zeljkovic on 05/11/2017.
 */
public interface AccountService {

    List<Account> findAll();

    Page<Account> findAll(Pageable page);

    Account findOne(Long id);

    Account findByUsername(String username);

    void checkUsername(String username);

    Account save(Account account);

    void remove(Long id);

    boolean isUsernameTaken(String username);
}
