import {by, element, ElementArrayFinder, ElementFinder} from "protractor";

export class DamageEditPage {
  // get methods
  getDamageEditElement(): ElementFinder {
    return element(by.tagName('app-damage-edit'));
  }

  getTitle(): ElementFinder {
    return element(by.css('.modal-title'));
  }

  getDescriptionInput(): ElementFinder {
    return element(by.id('description'));
  }

  getDamageUrgencySelector(): ElementFinder {
    return element(by.id('urgent'));
  }

  getDamageUrgencyOptions(): ElementArrayFinder {
    return element.all(by.css('#urgent > option'));
  }

  getDamageUrgent(index: number): ElementFinder {
    return element(by.css(`#urgent > option:nth-child(${index+1})`));
  }

  getSaveButton(): ElementFinder {
    return element(by.buttonText('Save'));
  }

  getResetButton(): ElementFinder {
    return element(by.buttonText('Reset'));
  }

  getCancelButton(): ElementFinder {
    return element(by.buttonText('Cancel'));
  }

  // set methods
  setDescription(text: string) {
    this.getDescriptionInput().sendKeys(text);
  }

  // get error method
  getDescriptionRequiredError(): ElementFinder {
    return element(by.id('descriptionRequiredError'));
  }
}
