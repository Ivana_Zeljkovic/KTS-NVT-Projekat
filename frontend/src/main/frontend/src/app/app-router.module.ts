import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
// component
import { LoginComponent } from "./login/login.component";
// guard
import { AuthGuard } from "./core/guards/auth.guard";
import { AnonymusGuard } from "./core/guards/anonymus.guard";
import { AdminGuard } from "./core/guards/admin.guard";
import { TenantOrBusinessGuard } from "./core/guards/tenant-or-business.guard";
import { AdminOrFirmGuard } from './core/guards/admin-or-firm.guard';
import { InstitutionGuard } from "./core/guards/institution.guard";
import { CouncilMemberGuard } from "./core/guards/council-member.guard";
import { BusinessGuard } from './core/guards/business.guard';
import { TenantOwnerOrCouncilMemberGuard } from "./core/guards/tenant-owner-or-council-member.guard";


const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: 'app/home/home.module#HomeModule', canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent, canActivate: [AnonymusGuard] },
  { path: 'registration', loadChildren: 'app/registration/registration.module#RegistrationModule' },
  { path: 'profile', loadChildren: 'app/profile/profile.module#ProfileModule', canActivate: [AuthGuard] },
  { path: 'tenants', loadChildren: 'app/tenants/tenants.module#TenantsModule', canActivate: [AuthGuard, AdminGuard] },
  { path: 'buildings', loadChildren: 'app/buildings/buildings.module#BuildingsModule', canActivate: [AuthGuard, AdminOrFirmGuard] },
  { path: 'businesses', loadChildren: 'app/businesses/businesses.module#BusinessesModule', canActivate: [AuthGuard, AdminGuard] },
  { path: 'admins', loadChildren: 'app/admins/admins.module#AdminsModule', canActivate: [AuthGuard, AdminGuard] },
  { path: 'apartments', loadChildren: 'app/apartments/apartments.module#ApartmentsModule', canActivate: [AdminGuard] },
  { path: 'damages', loadChildren: 'app/damages/damages.module#DamagesModule', canActivate: [AuthGuard, TenantOrBusinessGuard] },
  { path: 'damage-types', loadChildren: 'app/damage-types/damage-types.module#DamageTypesModule', canActivate: [AuthGuard, InstitutionGuard] },
  { path: 'damage-requests', loadChildren: 'app/damage-requests/damage-requests.module#DamageRequestsModule', canActivate: [AuthGuard, BusinessGuard] },
  { path: 'questionnaires', loadChildren: 'app/questionnaires/questionnaires.module#QuestionnairesModule', canActivate: [AuthGuard, TenantOwnerOrCouncilMemberGuard] },
  { path: 'meetings', loadChildren: 'app/meetings/meetings.module#MeetingsModule', canActivate: [AuthGuard, CouncilMemberGuard] },
  { path: 'records', loadChildren: 'app/records/records.module#RecordsModule', canActivate: [AuthGuard, CouncilMemberGuard] },
  { path: '**', redirectTo: 'home' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {useHash: true})
  ],
  exports: [RouterModule]
})
export class AppRouterModule { }
