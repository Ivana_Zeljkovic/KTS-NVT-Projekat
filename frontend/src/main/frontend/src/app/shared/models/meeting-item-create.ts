import { QuestionnaireCreate } from "./questionnaire-create";

export class MeetingItemCreate {

  constructor(public content: string,
              public title: string,
              public questionnaireCreate?: QuestionnaireCreate,
              public damageId?: number) { }
}
