package rs.ac.uns.ftn.ktsnvt.web.dto;

/**
 * Created by Katarina Cukurov on 29/11/2017.
 */
public class DamageFixedDTO {

    private Long offerId;

    public DamageFixedDTO() { }

    public DamageFixedDTO(Long offerId) {
        this.offerId = offerId;
    }

    public Long getOfferId() {
        return offerId;
    }

    public void setOfferId(Long offerId) {
        this.offerId = offerId;
    }
}
