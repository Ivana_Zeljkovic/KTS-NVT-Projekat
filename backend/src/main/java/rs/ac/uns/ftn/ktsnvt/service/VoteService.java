package rs.ac.uns.ftn.ktsnvt.service;

import rs.ac.uns.ftn.ktsnvt.model.entity.Vote;

/**
 * Created by Ivana Zeljkovic on 19/11/2017.
 */
public interface VoteService {

    int countPersonalVotes(Long tenantId, Long questionnaireId);

    Vote save(Vote vote);
}
