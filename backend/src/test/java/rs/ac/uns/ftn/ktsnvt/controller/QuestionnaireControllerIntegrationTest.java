package rs.ac.uns.ftn.ktsnvt.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import rs.ac.uns.ftn.ktsnvt.controller.util.TestUtil;
import rs.ac.uns.ftn.ktsnvt.web.dto.LoginDTO;
import rs.ac.uns.ftn.ktsnvt.web.dto.TokenDTO;
import rs.ac.uns.ftn.ktsnvt.web.dto.VoteCreateDTO;
import rs.ac.uns.ftn.ktsnvt.web.dto.VoteQuestionCreateDTO;

import javax.annotation.PostConstruct;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Created by Ivana Zeljkovic on 11/12/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class QuestionnaireControllerIntegrationTest {

    private static final String URL_PREFIX = "/api/buildings";

    private MediaType contentType = new MediaType(
            MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    private MockMvc mockMvc;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja administratora
    private String accessTokenAdmin;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja predsednika skupstine stanara
    private String accessTokenPresident;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja stanara
    private String accessTokenTenantOwner, accessTokenTenant;


    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private TestRestTemplate testRestTemplate;


    @PostConstruct
    public void setup() {
        this.mockMvc = MockMvcBuilders.
                webAppContextSetup(webApplicationContext).apply(springSecurity()).build();
    }

    // pre izvrsavanja testa, neophodno je izvrsiti logovanje u aplikaciju da bismo dobili token
    @Before
    public void login() throws Exception {
        // logovanje administratora
        LoginDTO loginDTOAdmin = new LoginDTO("ivana", "ivana");

        ResponseEntity<TokenDTO> responseEntityAdmin =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOAdmin,
                        TokenDTO.class);
        // preuzmemo token za logovanog administratora iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenAdmin = responseEntityAdmin.getBody().getValue();

        // logovanje predsednika
        LoginDTO loginDTO = new LoginDTO("kaca", "kaca");

        ResponseEntity<TokenDTO> responseEntity =
                testRestTemplate.postForEntity("/api/login",
                        loginDTO,
                        TokenDTO.class);
        // preuzmemo token za logovanog predsednika iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenPresident = responseEntity.getBody().getValue();


        // logovanje stanara (vlasnika stana)
        LoginDTO loginDTOTenant = new LoginDTO("nikola", "nikola");

        ResponseEntity<TokenDTO> responseEntityTenant =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOTenant,
                        TokenDTO.class);
        // preuzmemo token za logovanog stanara iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenTenantOwner = responseEntityTenant.getBody().getValue();


        // logovanje stanara (nije vlasnika stana)
        LoginDTO loginDTOTenant2 = new LoginDTO("jasmina", "jasmina");

        ResponseEntity<TokenDTO> responseEntityTenant2 =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOTenant2,
                        TokenDTO.class);
        // preuzmemo token za logovanog stanara iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenTenant = responseEntityTenant2.getBody().getValue();
    }

    // TESTOVI za vote
    @Test
    @Transactional
    @Rollback(true)
    public void testVote() throws Exception {
        /*
        * Test proverava ispravnost rada metode vote kontrolera QuestionnaireController
        * (glasanje na anketu sa zadatim ID-em koja pripada zgradi sa zadatim ID-em)
        * */

        /*
        anketa sa ID-em = 101 u zgradi sa ID-em = 100 ima sledeca pitanja:
            1 - id = 101,
                content = First question,
                answers: Yes, No
            2 - id = 102,
                content = Second question,
                answers: Confirm, Decline
        pri cemu ne postoji jos nijedan glas na ovu anketu
        */

        List<VoteQuestionCreateDTO> questionVotes = new ArrayList<>();
        questionVotes.add(new VoteQuestionCreateDTO(101L, "Yes"));
        questionVotes.add(new VoteQuestionCreateDTO(102L, "Confirm"));

        VoteCreateDTO voteCreateDTO = new VoteCreateDTO();
        voteCreateDTO.setQuestionVotes(questionVotes);

        String voteDTOJson = TestUtil.json(voteCreateDTO);

        // stanar koji trenutno glasa na anketu je stanar sa ID-em = 102 (firstName = Nikola, lastName = Garabandic)
        this.mockMvc.perform(post(URL_PREFIX + "/100/questionnaires/101/vote")
                .header("Authentication-Token", this.accessTokenTenantOwner)
                .contentType(contentType)
                .content(voteDTOJson))
                .andExpect(status().isCreated());
    }

    @Test
    public void testVoteByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode vote kontrolera QuestionnaireController
        * (glasanje na anketu sa zadatim ID-em koja pripada zgradi sa zadatim ID-em)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        List<VoteQuestionCreateDTO> questionVotes = new ArrayList<>();
        questionVotes.add(new VoteQuestionCreateDTO(101L, "Yes"));
        questionVotes.add(new VoteQuestionCreateDTO(102L, "Confirm"));

        VoteCreateDTO voteCreateDTO = new VoteCreateDTO();
        voteCreateDTO.setQuestionVotes(questionVotes);

        String voteDTOJson = TestUtil.json(voteCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/100/questionnaires/101/vote")
                .contentType(contentType)
                .content(voteDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testVoteByNotAuthenticatedTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode vote kontrolera QuestionnaireController
        * (glasanje na anketu sa zadatim ID-em koja pripada zgradi sa zadatim ID-em)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu stanara
        * */

        List<VoteQuestionCreateDTO> questionVotes = new ArrayList<>();
        questionVotes.add(new VoteQuestionCreateDTO(101L, "Yes"));
        questionVotes.add(new VoteQuestionCreateDTO(102L, "Confirm"));

        VoteCreateDTO voteCreateDTO = new VoteCreateDTO();
        voteCreateDTO.setQuestionVotes(questionVotes);

        String voteDTOJson = TestUtil.json(voteCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/100/questionnaires/101/vote")
                .header("Authentication-Token", this.accessTokenAdmin)
                .contentType(contentType)
                .content(voteDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testVoteNonexistentBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode vote kontrolera QuestionnaireController
        * (glasanje na anketu sa zadatim ID-em koja pripada zgradi sa zadatim ID-em)
        * u slucaju kada ne postoji zgrada sa zadatim ID-em
        * */

        List<VoteQuestionCreateDTO> questionVotes = new ArrayList<>();
        questionVotes.add(new VoteQuestionCreateDTO(101L, "Yes"));
        questionVotes.add(new VoteQuestionCreateDTO(102L, "Confirm"));

        VoteCreateDTO voteCreateDTO = new VoteCreateDTO();
        voteCreateDTO.setQuestionVotes(questionVotes);

        String voteDTOJson = TestUtil.json(voteCreateDTO);

        // ne postoji zgrada sa ID-em = 200
        this.mockMvc.perform(post(URL_PREFIX + "/200/questionnaires/101/vote")
                .header("Authentication-Token", this.accessTokenTenantOwner)
                .contentType(contentType)
                .content(voteDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testVoteNonexistentQuestionnaire() throws Exception {
        /*
        * Test proverava ispravnost rada metode vote kontrolera QuestionnaireController
        * (glasanje na anketu sa zadatim ID-em koja pripada zgradi sa zadatim ID-em)
        * u slucaju kada ne postoji anketa sa zadatim ID-em
        * */

        List<VoteQuestionCreateDTO> questionVotes = new ArrayList<>();
        questionVotes.add(new VoteQuestionCreateDTO(101L, "Yes"));
        questionVotes.add(new VoteQuestionCreateDTO(102L, "Confirm"));

        VoteCreateDTO voteCreateDTO = new VoteCreateDTO();
        voteCreateDTO.setQuestionVotes(questionVotes);

        String voteDTOJson = TestUtil.json(voteCreateDTO);

        // ne postoji anketa sa ID-em = 200
        this.mockMvc.perform(post(URL_PREFIX + "/100/questionnaires/200/vote")
                .header("Authentication-Token", this.accessTokenTenantOwner)
                .contentType(contentType)
                .content(voteDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testVoteQuestionnaireFromInvalidBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode vote kontrolera QuestionnaireController
        * (glasanje na anketu sa zadatim ID-em koja pripada zgradi sa zadatim ID-em)
        * u slucaju kada anketa sa zadatim ID-em ne pripada zgradi sa zadatim ID-em
        * */

        List<VoteQuestionCreateDTO> questionVotes = new ArrayList<>();
        questionVotes.add(new VoteQuestionCreateDTO(101L, "Yes"));
        questionVotes.add(new VoteQuestionCreateDTO(102L, "Confirm"));

        VoteCreateDTO voteCreateDTO = new VoteCreateDTO();
        voteCreateDTO.setQuestionVotes(questionVotes);

        String voteDTOJson = TestUtil.json(voteCreateDTO);

        // anketa sa ID-em = 103 pripada zgradi sa ID-em = 100 (a ne sa ID-em = 101)
        this.mockMvc.perform(post(URL_PREFIX + "/100/questionnaires/103/vote")
                .header("Authentication-Token", this.accessTokenTenantOwner)
                .contentType(contentType)
                .content(voteDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testVoteTimeForVotingExpired() throws Exception {
        /*
        * Test proverava ispravnost rada metode vote kontrolera QuestionnaireController
        * (glasanje na anketu sa zadatim ID-em koja pripada zgradi sa zadatim ID-em)
        * u slucaju kada je vreme za glasanje na anketu sa zadatim ID-em isteklo
        * */

        List<VoteQuestionCreateDTO> questionVotes = new ArrayList<>();
        questionVotes.add(new VoteQuestionCreateDTO(101L, "Yes"));
        questionVotes.add(new VoteQuestionCreateDTO(102L, "Confirm"));

        VoteCreateDTO voteCreateDTO = new VoteCreateDTO();
        voteCreateDTO.setQuestionVotes(questionVotes);

        String voteDTOJson = TestUtil.json(voteCreateDTO);

        // vreme za glasanje na anketu sa ID-em = 100 je isteklo
        this.mockMvc.perform(post(URL_PREFIX + "/100/questionnaires/100/vote")
                .header("Authentication-Token", this.accessTokenTenantOwner)
                .contentType(contentType)
                .content(voteDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testVoteByTenantNotOwner() throws Exception {
        /*
        * Test proverava ispravnost rada metode vote kontrolera QuestionnaireController
        * (glasanje na anketu sa zadatim ID-em koja pripada zgradi sa zadatim ID-em)
        * u slucaju kada akciju izvrsava logovani stanar koji nije vlasnik nijednog stana u zgradi kojoj pripada anketa sa zadatim ID-em
        * */

        List<VoteQuestionCreateDTO> questionVotes = new ArrayList<>();
        questionVotes.add(new VoteQuestionCreateDTO(101L, "Yes"));
        questionVotes.add(new VoteQuestionCreateDTO(102L, "Confirm"));

        VoteCreateDTO voteCreateDTO = new VoteCreateDTO();
        voteCreateDTO.setQuestionVotes(questionVotes);

        String voteDTOJson = TestUtil.json(voteCreateDTO);

        // akciju izvrsava stanar sa ID-em = 103 (nije vlasnik nijednog stana)
        this.mockMvc.perform(post(URL_PREFIX + "/100/questionnaires/101/vote")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(voteDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testVoteAnswerListIsEmpty() throws Exception {
        /*
        * Test proverava ispravnost rada metode vote kontrolera QuestionnaireController
        * (glasanje na anketu sa zadatim ID-em koja pripada zgradi sa zadatim ID-em)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne sadrzi sva neophodna polja - lista odgovora je prazna
        * */

        // lista odgovora je prazna
        List<VoteQuestionCreateDTO> questionVotes = new ArrayList<>();

        VoteCreateDTO voteCreateDTO = new VoteCreateDTO();
        voteCreateDTO.setQuestionVotes(questionVotes);

        String voteDTOJson = TestUtil.json(voteCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/100/questionnaires/101/vote")
                .header("Authentication-Token", this.accessTokenTenantOwner)
                .contentType(contentType)
                .content(voteDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testVoteNonexistentQuestionWithId() throws Exception {
        /*
        * Test proverava ispravnost rada metode vote kontrolera QuestionnaireController
        * (glasanje na anketu sa zadatim ID-em koja pripada zgradi sa zadatim ID-em)
        * u slucaju kada je dolazni DTO nevalidan: sadrzi sva neophodna polja, ali u bar jednom odgovoru se nalazi id pitanja
        * pri cemu ne postoji pitanje sa tim ID-em
        * */

        List<VoteQuestionCreateDTO> questionVotes = new ArrayList<>();
        // ne postoji pitanje sa ID-em = 200
        questionVotes.add(new VoteQuestionCreateDTO(200L, "Yes"));
        questionVotes.add(new VoteQuestionCreateDTO(102L, "Confirm"));

        VoteCreateDTO voteCreateDTO = new VoteCreateDTO();
        voteCreateDTO.setQuestionVotes(questionVotes);

        String voteDTOJson = TestUtil.json(voteCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/100/questionnaires/101/vote")
                .header("Authentication-Token", this.accessTokenTenantOwner)
                .contentType(contentType)
                .content(voteDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testVoteQuestionFromInvalidQuestionnaire() throws Exception {
        /*
        * Test proverava ispravnost rada metode vote kontrolera QuestionnaireController
        * (glasanje na anketu sa zadatim ID-em koja pripada zgradi sa zadatim ID-em)
        * u slucaju kada je dolazni DTO nevalidan: sadrzi sva neophodna polja, ali u bar jednom odgovoru se nalazi id pitanja
        * pri cemu je to pitanje koje pripada nekoj drugoj anketi
        * */

        List<VoteQuestionCreateDTO> questionVotes = new ArrayList<>();
        // pitanje sa ID-em = 100 pripada anketi sa ID-em = 100 (a ne sa ID-em = 101)
        questionVotes.add(new VoteQuestionCreateDTO(100L, "Yes"));
        questionVotes.add(new VoteQuestionCreateDTO(102L, "Confirm"));

        VoteCreateDTO voteCreateDTO = new VoteCreateDTO();
        voteCreateDTO.setQuestionVotes(questionVotes);

        String voteDTOJson = TestUtil.json(voteCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/100/questionnaires/101/vote")
                .header("Authentication-Token", this.accessTokenTenantOwner)
                .contentType(contentType)
                .content(voteDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testVoteNonexistentAnswer() throws Exception {
        /*
        * Test proverava ispravnost rada metode vote kontrolera QuestionnaireController
        * (glasanje na anketu sa zadatim ID-em koja pripada zgradi sa zadatim ID-em)
        * u slucaju kada je dolazni DTO objekat nevalidan: sadrzi sva neophodna polja, ali u bar jednom odgovoru se
        * nalazi nepostojeci odgovor
        * */

        List<VoteQuestionCreateDTO> questionVotes = new ArrayList<>();
        // ne postoji odgovor GRESKA na pitanje sa ID-em = 101
        questionVotes.add(new VoteQuestionCreateDTO(101L, "GRESKA"));
        questionVotes.add(new VoteQuestionCreateDTO(102L, "Confirm"));

        VoteCreateDTO voteCreateDTO = new VoteCreateDTO();
        voteCreateDTO.setQuestionVotes(questionVotes);

        String voteDTOJson = TestUtil.json(voteCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/100/questionnaires/101/vote")
                .header("Authentication-Token", this.accessTokenTenantOwner)
                .contentType(contentType)
                .content(voteDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testVoteMissingQuestionId() throws Exception {
        /*
        * Test proverava ispravnost rada metode vote kontrolera QuestionnaireController
        * (glasanje na anketu sa zadatim ID-em koja pripada zgradi sa zadatim ID-em)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne sadrzi sva noephodna polja - u bar jednom odgovoru
        * se ne nalazi id pitanja na koje se odnosi odgovor
        * */

        List<VoteQuestionCreateDTO> questionVotes = new ArrayList<>();
        // ne postoji id pitanja na koje se odnosi odgovor
        questionVotes.add(new VoteQuestionCreateDTO(null, "Yes"));
        questionVotes.add(new VoteQuestionCreateDTO(102L, "Confirm"));

        VoteCreateDTO voteCreateDTO = new VoteCreateDTO();
        voteCreateDTO.setQuestionVotes(questionVotes);

        String voteDTOJson = TestUtil.json(voteCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/100/questionnaires/101/vote")
                .header("Authentication-Token", this.accessTokenTenantOwner)
                .contentType(contentType)
                .content(voteDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testVoteMissingAnswer() throws Exception {
        /*
        * Test proverava ispravnost rada metode vote kontrolera QuestionnaireController
        * (glasanje na anketu sa zadatim ID-em koja pripada zgradi sa zadatim ID-em)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne sadrzi sva noephodna polja - u bar jednom odgovoru
        * se ne nalazi izabrani odgovor na pitanje
        * */

        List<VoteQuestionCreateDTO> questionVotes = new ArrayList<>();
        // ne postoji izabrani odgovor na pitanje
        questionVotes.add(new VoteQuestionCreateDTO(101L, null));
        questionVotes.add(new VoteQuestionCreateDTO(102L, "Confirm"));

        VoteCreateDTO voteCreateDTO = new VoteCreateDTO();
        voteCreateDTO.setQuestionVotes(questionVotes);

        String voteDTOJson = TestUtil.json(voteCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/100/questionnaires/101/vote")
                .header("Authentication-Token", this.accessTokenTenantOwner)
                .contentType(contentType)
                .content(voteDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testVoteMissingSomeQuestionAndAnswer() throws Exception {
        /*
        * Test proverava ispravnost rada metode vote kontrolera QuestionnaireController
        * (glasanje na anketu sa zadatim ID-em koja pripada zgradi sa zadatim ID-em)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne sadrzi sva noephodna polja - nije dat odgovor na sva pitanja
        * */

        List<VoteQuestionCreateDTO> questionVotes = new ArrayList<>();
        // ne postoji odgovor na pitanje sa ID-em = 101L
        questionVotes.add(new VoteQuestionCreateDTO(102L, "Confirm"));

        VoteCreateDTO voteCreateDTO = new VoteCreateDTO();
        voteCreateDTO.setQuestionVotes(questionVotes);

        String voteDTOJson = TestUtil.json(voteCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/100/questionnaires/101/vote")
                .header("Authentication-Token", this.accessTokenTenantOwner)
                .contentType(contentType)
                .content(voteDTOJson))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za getQuestionnaireResult
    @Test
    public void testGetQuestionnaireResult() throws Exception {
        /*
        * Test proverava ispravnost rada metode getQuestionnaireResult kontrolera QuestionnaireController
        * (dobavljanje rezultata ankete sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * */

        /*
        glasanje ne anketu sa ID-em = 100 je zavrseno i ona ima sledece podatke:
            id = 100,
            meeting_item_id = 100,
            questions: 1 - id = 100,
                           content = Changing the council president,
                           answers: First Tenant, Second Tenant
            votes: 0
        */
        this.mockMvc.perform(get(URL_PREFIX + "/100/questionnaires/100/results")
                .header("Authentication-Token", this.accessTokenPresident))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.questionnaireId").value(100))
                .andExpect(jsonPath("$.numberOfVotes").value(1))
                .andExpect(jsonPath("$.results.[*]", hasSize(1)))
                .andExpect(jsonPath("$.results.[0].questionId").value(100))
                .andExpect(jsonPath("$.results.[0].content").value("Changing the council president"))
                .andExpect(jsonPath("$.results.[0].answers.[*]", hasSize(2)))
                .andExpect(jsonPath("$.results.[0].answers.[0].numberOfVotes").value(1))
                .andExpect(jsonPath("$.results.[0].answers.[1].numberOfVotes").value(0));
    }

    @Test
    public void testGetQuestionnaireResultByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode getQuestionnaireResult kontrolera QuestionnaireController
        * (dobavljanje rezultata ankete sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/100/questionnaires/100/results"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetQuestionnaireResultByNotAuthenticatedTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode getQuestionnaireResult kontrolera QuestionnaireController
        * (dobavljanje rezultata ankete sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu stanara
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/100/questionnaires/100/results")
                .header("Authentication-Token", this.accessTokenAdmin))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetQuestionnaireResultNonexistentBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode getQuestionnaireResult kontrolera QuestionnaireController
        * (dobavljanje rezultata ankete sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * u slucaju kada ne postoji zgrada sa zadatim ID-em
        * */

        // ne postoji zgrada sa ID-em = 200
        this.mockMvc.perform(get(URL_PREFIX + "/200/questionnaires/100/results")
                .header("Authentication-Token", this.accessTokenPresident))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetQuestionnaireResultNonexistentQuestionnaire() throws Exception {
        /*
        * Test proverava ispravnost rada metode getQuestionnaireResult kontrolera QuestionnaireController
        * (dobavljanje rezultata ankete sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * u slucaju kada ne postoji anketa sa zadatim ID-em
        * */

        // ne postoji anketa sa ID-em = 200
        this.mockMvc.perform(get(URL_PREFIX + "/100/questionnaires/200/results")
                .header("Authentication-Token", this.accessTokenPresident))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetQuestionnaireResultQuestionnaireFromInvalidBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode getQuestionnaireResult kontrolera QuestionnaireController
        * (dobavljanje rezultata ankete sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * u slucaju kada anketa sa zadatim ID-em ne pripada zgradi sa zadatim ID-em
        * */

        // anketa sa ID-em = 103 pripada zgradi sa ID-em = 101 (a ne sa ID-em = 100)
        this.mockMvc.perform(get(URL_PREFIX + "/100/questionnaires/103/results")
                .header("Authentication-Token", this.accessTokenPresident))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testGetQuestionnaireResultTimeForVotingIsNotExpired() throws Exception {
        /*
        * Test proverava ispravnost rada metode getQuestionnaireResult kontrolera QuestionnaireController
        * (dobavljanje rezultata ankete sa zadatim ID-em na nivou zgrade sa zadatim ID-em)
        * u slucaju kada jos uvek nije proslo vreme glasanja na anketu sa zadatim ID-em
        * */

        // vreme glasanja na anketu sa ID-em = 101 jos nije zavrseno
        this.mockMvc.perform(get(URL_PREFIX + "/100/questionnaires/101/results")
                .header("Authentication-Token", this.accessTokenPresident))
                .andExpect(status().isBadRequest());
    }
}
