import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
// service
import { DataService } from "./data.service";
// models
import { Business } from "../../shared/models/business";


@Injectable()
export class BusinessService extends DataService<Business>{
  constructor(http: HttpClient) {
    super('', http);
  }
}
