package rs.ac.uns.ftn.ktsnvt.service.unit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Council;
import rs.ac.uns.ftn.ktsnvt.model.entity.Meeting;
import rs.ac.uns.ftn.ktsnvt.model.entity.MeetingItem;
import rs.ac.uns.ftn.ktsnvt.repository.MeetingRepository;
import rs.ac.uns.ftn.ktsnvt.service.MeetingService;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import static org.mockito.BDDMockito.given;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Ivana Zeljkovic on 01/12/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MeetingServiceUnitTest {

    @Autowired
    private MeetingService meetingService;

    @MockBean
    private MeetingRepository meetingRepository;


    @Before
    public void setUp() {
        // COUNCIL
        Council council = new Council();
        council.setId(new Long(100));

        // MEETING ITEMS FOR MEETING 1
        MeetingItem meeting_item_1 = new MeetingItem();
        meeting_item_1.setContent("First meeting item");

        MeetingItem meeting_item_2 = new MeetingItem();
        meeting_item_2.setContent("Second meeting item");

        // MEETING 1
        Meeting meeting = null;
        try {
            meeting = new Meeting(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse("2017-11-28 10:00:00"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        meeting.setId(new Long(101));
        meeting.setCouncil(council);
        meeting.setDuration(60);

        meeting_item_1.setMeeting(meeting);
        meeting_item_2.setMeeting(meeting);
        meeting.getMeetingItems().add(meeting_item_1);
        meeting.getMeetingItems().add(meeting_item_2);

        // MEETING ITEMS FOR MEETING 2
        MeetingItem meeting_item_3 = new MeetingItem();
        meeting_item_3.setContent("Third meeting item");

        // MEETING 2
        Meeting meeting_2 = null;
        try {
            meeting_2 = new Meeting(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse("2018-12-01 10:00:00"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        meeting_2.setId(new Long(102));
        meeting_2.setCouncil(council);
        meeting_2.setDuration(45);

        meeting_item_3.setMeeting(meeting_2);
        meeting_2.getMeetingItems().add(meeting_item_3);


        given(
                this.meetingRepository.findOne(new Long(101))
        ).willReturn(
                meeting
        );

        given(
                this.meetingRepository.findOne(new Long(102))
        ).willReturn(
                meeting_2
        );

        given(
                this.meetingRepository.findOne(new Long(103))
        ).willReturn(
                null
        );
    }


    @Test
    public void testFindOne() {
        /*
         * Test proverava ispravnost metode findOne servisa MeetingService
         * (dobavljanje objekta sastanka na osnovu zadatog ID-a)
         * u slucaju kada postoji sastanak sa zadatim ID-em
         * */

        /*
        postoji sastanak ciji je ID = 101 sa podacima:
            trajanje sastanka = 60min,
            ID skupstine stanara kojoj pripada = 100,
            sadrzi 2 tacke dnevnog reda
         */
        Meeting meeting = this.meetingService.findOne(new Long(101));

        assertNotNull(meeting);
        assertEquals(Long.valueOf(101), meeting.getId());
        assertEquals(60, meeting.getDuration());
        assertEquals(Long.valueOf(100), meeting.getCouncil().getId());
        assertEquals(2, meeting.getMeetingItems().size());
    }

    @Test(expected = NotFoundException.class)
    public void testFindOneNonexistent() {
        /*
         * Test proverava ispravnost metode findOne servisa MeetingService
         * (dobavljanje objekta sastanka na osnovu zadatog ID-a)
         * u slucaju kada ne postoji sastanak sa zadatim ID-em - ocekivano NotFoundException
         * */

        // u bazi ne postoji sastanak sa ID-em = 103
        Meeting meeting = this.meetingService.findOne(new Long(103));
    }

    @Test
    public void testCheckMeetingFinished() {
        /*
         * Test proverava ispravnost metode checkMeetingFinished servisa MeetingService
         * (provera da li je sastanak sa zadatim ID-em zavrsen)
         * u slucaju kada se sastanak sa zadatim ID-em zavrsio
         * */

        Meeting meeting = this.meetingService.findOne(new Long(101));

        // sastanak sa ID-em = 101 se zavrsio, pa metoda servisa nece vratiti nikakvu gresku
        this.meetingService.checkMeetingFinished(meeting);
    }

    @Test(expected = BadRequestException.class)
    public void testCheckMeetingFinishedNotYet() {
        /*
         * Test proverava ispravnost metode checkMeetingFinished servisa MeetingService
         * (provera da li je sastanak sa zadatim ID-em zavrsen)
         * u slucaju kada se sastanak sa zadatim ID-em jos uvek nije zavrsio - ocekivano BadRequestException
         * */

        Meeting meeting = this.meetingService.findOne(new Long(102));

        // sastanak sa ID-em = 102 se jos uvek nije zavrsio, pa metoda servisa baca izuzetak
        this.meetingService.checkMeetingFinished(meeting);
    }

    @Test
    @Transactional
    @Rollback(true)
    public void testClearMeetingItemsList() {
        /*
         * Test proverava ispravnost metode clearMeetingItemsList servisa MeetingService
         * (ciscenje liste tacaka dnevnog reda u prosledjenom objektu sastanka)
         * u slucaju kada postoji sastanak sa zadatim ID-em
         * */

        /*
        postoji sastanak ciji je ID = 101 sa podacima:
            trajanje sastanka = 60min,
            ID skupstine stanara kojoj pripada = 100,
            sadrzi 2 tacke dnevnog reda
         */
        Meeting meeting = this.meetingService.findOne(new Long(101));

        this.meetingService.clearMeetingItemsList(meeting);

        assertEquals(0, meeting.getMeetingItems().size());
    }

    @Test
    public void testCheckMeetingStart() {
        /*
         * Test proverava ispravnost metode checkMeetingStart servisa MeetingService
         * (provera da li je sastanak sa zadatim ID-em u toku svog trajanja ili se zavrsio vec)
         * u slucaju kada sastanak sa zadatim ID-em jos nije poceo
         * */

        Meeting meeting = this.meetingService.findOne(new Long(102));

        // sastanak sa ID-em = 102 jos nije poceo, pa metoda servisa ne baca izuzetak
        this.meetingService.checkMeetingStart(meeting);
    }

    @Test(expected = BadRequestException.class)
    public void testCheckMeetingStartMeetingFinished() {
        /*
         * Test proverava ispravnost metode checkMeetingStart servisa MeetingService
         * (provera da li je sastanak sa zadatim ID-em u toku svog trajanja ili se zavrsio vec)
         * u slucaju kada se sastanak sa zadatim ID-em zavrsio - ocekivano BadRequestException
         * */
        Meeting meeting = this.meetingService.findOne(new Long(101));

        // sastanak sa ID-em = 101 je zavrsen, pa metoda servisa baca izuzetak
        this.meetingService.checkMeetingStart(meeting);
    }

    @Test
    public void testMeetingIsInTheFutureTrue(){
           /*
         * Test proverava ispravnost metode MeetingIsInTheFuture servisa MeetingService
         * (provera da li je sastanak u buducnosti)
         * u slucaju da je sastanak u buducnosti, vraca true, u suportnom vraca false
         * */


        Meeting meeting = this.meetingService.findOne(new Long(102L));
        assertEquals(true, this.meetingService.meetingIsInTheFuture(meeting));

    }

    @Test
    public void testMeetingIsInTheFutureFalse(){
         /*
         * Test proverava ispravnost metode MeetingIsInTheFuture servisa MeetingService
         * (provera da li je sastanak u buducnosti)
         * u slucaju da je sastanak u buducnosti, vraca true, u suportnom vraca false
         * */


        Meeting meeting = this.meetingService.findOne(new Long(101L));
        assertEquals(false, this.meetingService.meetingIsInTheFuture(meeting));

    }
}
