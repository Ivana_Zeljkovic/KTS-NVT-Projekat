import { Component, Input, OnInit } from '@angular/core';
import { ToasterConfig, ToasterService } from "angular5-toaster/dist";
import { BsModalRef } from "ngx-bootstrap";
import { Router } from "@angular/router";
// model
import { Tenant} from "../../models/tenant";
import { PageAndSort } from "../../models/page-and-sort";
import { PresidentUpdate } from "../../models/president-update";
import { TenantCouncilMember } from "../../models/tenant-council-member";
// service
import { TenantService } from "../../../core/services/tenant.service";
import { JwtService } from "../../../core/services/jwt.service";
import { CouncilPresidentService } from "../../../core/services/council-president.service";
import { AuthService } from "../../../core/services/auth.service";
// error
import { AppError} from "../../errors/app-error";
import { ForbiddenError} from "../../errors/forbidden-error";
import { NotFoundError} from "../../errors/not-found-error";
import { BadRequestError} from "../../errors/bad-request-error";


@Component({
  selector: 'app-council-member',
  templateUrl: './council-member.component.html',
  styleUrls: ['./council-member.component.css']
})
export class CouncilMemberComponent implements OnInit {
  // input field that indicates are we choose new president or council member
  @Input() choosePresident: boolean;
  tenantsList: Array<Tenant>;
  toasterConfig: ToasterConfig;

  // Elements for bootstrap paging
  currentPage: number;
  totalItems: number;
  maxSize: number;
  itemsPerPage: number;

  // Elements for getting data from server
  pageSize: string;
  sortDirection: string;
  sortProperties: string[];


  constructor(public modalRef: BsModalRef, private tenantService: TenantService,
              private jwtService: JwtService, private toasterService: ToasterService,
              private councilPresidentService: CouncilPresidentService, private authService: AuthService,
              private router: Router) {
    // Sett up for bootstrap paging
    this.currentPage = 1;
    this.maxSize = 3;
    this.itemsPerPage = 4;

    // Sett up for getting damages request
    this.pageSize = '4';
    this.sortDirection = 'ASC';
    this.sortProperties = ['firstName'];

    this.toasterConfig = new ToasterConfig({timeout:4000});
  }

  ngOnInit() {
    this.loadList();
  }

  delegateResponsibility(tenant: Tenant) {
    if(this.choosePresident) {
      let newPresident = new PresidentUpdate(tenant.firstName, tenant.lastName, tenant.username);
      this.councilPresidentService.updatePart(newPresident,'buildings',
        this.jwtService.getBuildingIdFromToken(), 'council_president')
          .subscribe(() => {
            // after successfully delegating responsibility, this user hasn't COUNCIL_PRESIDENT role anymore
            // so we need to logout him
            this.modalRef.hide();
            this.authService.logout();
            this.router.navigate(['login']);
          }, (error: AppError) => {
            if(error instanceof BadRequestError)
              this.showMessage('error', 'Error', 'Selected tenant is not member of building\'s ' +
                'council or tenant doesn\'t live in this building!');
            else if(error instanceof ForbiddenError)
              this.showMessage('error', 'Error', 'You don\'t have permission for this action!');
            else if(error instanceof NotFoundError)
              this.showMessage('error', 'Error', 'Tenant not found!');
            else {
              this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
              throw error;
            }
          });
    }
    else {
      let newCouncilMember = new TenantCouncilMember(tenant.id);
      this.tenantService.updatePart(newCouncilMember, 'buildings', this.jwtService.getBuildingIdFromToken(),
        'apartments', this.jwtService.getApartmentIdFromToken(), 'council_member')
        .subscribe(() => {
          // after successfully delegating responsibility, this user hasn't membership in building's council
          // so we need to logout him
          this.modalRef.hide();
          this.authService.logout();
          this.router.navigate(['login']);
        }, (error: AppError) => {
          if(error instanceof BadRequestError)
            this.showMessage('error', 'Error', 'Tenant doesn\'t live in this building or you can\'t delegate' +
              ' that responsibility!');
          else if(error instanceof ForbiddenError)
            this.showMessage('error', 'Error', 'You don\'t have permission for this action!');
          else if(error instanceof NotFoundError)
            this.showMessage('error', 'Error', 'Tenant not found!');
          else {
            this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
            throw error;
          }
        });
    }
  }

  pageChanged(event: any) {
    this.currentPage = event.page;
    this.loadList();
  }

  private loadList() {
    let page = new PageAndSort((this.currentPage - 1).toString(), this.pageSize, this.sortDirection, this.sortProperties);
    this.tenantService.getCouncilMembers(page, this.jwtService.getBuildingIdFromToken())
      .subscribe(tenants => {
        if (tenants.length > 0)
          this.totalItems = tenants[0].numberOfElements;
        else this.totalItems = 0;
        this.tenantsList = tenants;
      }, (error: AppError) => {
        if (error instanceof ForbiddenError)
          this.showMessage('error', 'Error', 'You don\'t have permission for this action!');
        else if (error instanceof NotFoundError)
          this.showMessage('error', 'Error', 'Building not found!');
        else {
          this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
          throw error;
        }
      })
  }

  private showMessage(type: string, title: string, message: string) {
    this.toasterService.pop(`${type}`, `${title}`, `${message}`);
  }
}
