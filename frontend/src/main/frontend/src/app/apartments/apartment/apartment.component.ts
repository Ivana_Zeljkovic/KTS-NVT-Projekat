import {Component} from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {FormBuilder} from "@angular/forms";
import {ToasterService} from "angular5-toaster/dist";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
// model
import {Apartment} from "../../shared/models/apartment";
// service
import {TenantService} from "../../core/services/tenant.service";
import {ApartmentService} from "../../core/services/apartment.service";
// error
import {AppError} from "../../shared/errors/app-error";
import {NotFoundError} from "../../shared/errors/not-found-error";
import {ForbiddenError} from "../../shared/errors/forbidden-error";
import {BadRequestError} from "../../shared/errors/bad-request-error";
// component
import {ChooseOwnerModalComponent} from "../choose-owner-modal/choose-owner-modal.component";
import {Tenant} from "../../shared/models/tenant";
import {ConfirmDeleteModalComponent} from "../../shared/components/confirm-delete-modal/confirm-delete-modal.component";

@Component({
  selector: 'app-apartment',
  templateUrl: './apartment.component.html',
  styleUrls: ['./apartment.component.css']
})
export class ApartmentComponent{

  id: number;
  apartment: Apartment;
  modalRef: BsModalRef;

  constructor(private route: ActivatedRoute, private fb: FormBuilder, private tenantService: TenantService,
              private router: Router,  private toasterService: ToasterService,
              private modalService : BsModalService, private apartmentService: ApartmentService) {
    this.route.params.subscribe((param: Params) => {
      this.id = param['id'];
      this.getApartment();
    });
  }

  getApartment(){
    this.apartmentService.get(this.id,  'apartments').subscribe(data => {
      this.apartment = data;
    },(error: AppError) => {
        if(error instanceof NotFoundError)
          this.toasterService.pop('error', 'Error', 'Apartment not found!');
        else if(error instanceof ForbiddenError)
          this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
        else if(error instanceof BadRequestError)
          this.toasterService.pop('error', 'Error', 'Apartment does not exist!');
        else {
          this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
          throw error;
        }
      });
  }

  changeOwner(){
    this.modalRef = this.modalService.show(
          ChooseOwnerModalComponent,
          Object.assign({},{ class: 'modal-lg' })
        );
    this.modalRef.content.apartment = this.apartment;
    this.modalRef.content.addingNewTenant = false;
    this.modalRef.content.modalRef = this.modalRef;
    this.modalRef.content.ownerSet.subscribe( data => {
      this.apartment.owner = data;
    })
  }

  removeTenant(tenant: Tenant){
    this.modalRef = this.modalService.show(
      ConfirmDeleteModalComponent,
      Object.assign({},{ class: 'modal-sm' })
    );

    this.modalRef.content.confirmed.subscribe((value) => {
      if(value) {
        this.apartmentService.updatePart({}, 'buildings',this.apartment.buildingId ,'apartments',
          this.apartment.id,'tenants', tenant.id , 'remove_tenant_from_apartment').subscribe(() => {
            this.toasterService.pop('success', 'Congratulation', 'Successfully removed tenant!');
            this.getApartment();
          },
          (error: AppError) => {
            if(error instanceof NotFoundError)
              this.toasterService.pop('error', 'Error', 'Tenant not found!');
            else if(error instanceof ForbiddenError)
              this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
            else if(error instanceof BadRequestError)
              this.toasterService.pop('error', 'Error', 'Selected tenant doesn\'t exist!');
            else {
              this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
              throw error;
            }
          });
      }
    });
  }

  displayAddTenant(){
    this.modalRef = this.modalService.show(
      ChooseOwnerModalComponent,
      Object.assign({},{ class: 'modal-lg' })
    );
    this.modalRef.content.apartment = this.apartment;
    this.modalRef.content.addingNewTenant = true;
    this.modalRef.content.modalRef = this.modalRef;
    this.modalRef.content.tenantAdded.subscribe( data => {
      this.getApartment();
    })
  }

}
