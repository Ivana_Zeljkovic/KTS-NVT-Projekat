

import {by, element} from "protractor";

export class OfferModal{

  getOfferModalComponent(){
      return element(by.tagName('app-offer-modal'));
  }

  getModalClass(){
    return element(by.css('.modal-content'));
  }

  getInput(){
    return element(by.tagName('input'));
  }

  getButon(){
    return element(by.css('.btn.btn-success'));
  }

  getAlertDanger(){
    return element(by.id('priceError'));
  }

  getModalHeader(){
    return element(by.css('.modal-header'));
  }

  getCloseButton(){
    return element(by.css('.close.pull-right'));
  }

  getContainsSpace(){
    return element(by.id('priceContainsSpace'));
  }

  getPricePattern(){
    return element(by.id('pricePattern'));
  }

}
