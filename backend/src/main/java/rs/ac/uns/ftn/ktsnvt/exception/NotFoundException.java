package rs.ac.uns.ftn.ktsnvt.exception;

/**
 * Created by Ivana Zeljkovic on 25/11/2017.
 */
public class NotFoundException extends RuntimeException {

    public NotFoundException() { }

    public NotFoundException(String message) {
        super(message);
    }
}
