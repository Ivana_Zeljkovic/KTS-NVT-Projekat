import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BsModalRef } from "ngx-bootstrap";
import { ToasterService } from "angular5-toaster/dist";
// service
import { TenantService } from "../../../core/services/tenant.service";
import { JwtService } from "../../../core/services/jwt.service";
import { DamageService } from "../../../core/services/damage.service";
import { DamageTypeResponsibilityService } from "../../../core/services/damage-type-responsibility.service";
// model
import { PageAndSort } from "../../models/page-and-sort";
import { Tenant } from "../../models/tenant";
import { Damage } from "../../models/damage";
import { ResourceId } from "../../models/id";
import { DamageTypeResponsibility } from "../../models/damage-type-responsibility";
// error
import { AppError } from "../../errors/app-error";
import { BadRequestError } from "../../errors/bad-request-error";
import { ForbiddenError } from "../../errors/forbidden-error";
import { NotFoundError } from "../../errors/not-found-error";


@Component({
  selector: 'app-delegate-responsibility-to-person',
  templateUrl: './delegate-responsibility-to-person.component.html',
  styleUrls: ['./delegate-responsibility-to-person.component.css']
})
export class DelegateResponsibilityToPersonComponent implements OnInit {
  // input model in case if we set/delegate responsibility for damage
  @Input() damage: Damage;
  // input model in case if we set/delegate responsibility for damage type
  @Input() damageTypeResponsibility: DamageTypeResponsibility;

  // input field that indicates are we set (true) or delegate (false) responsibility
  @Input() setResponsibility: boolean;
  // input field that indicates are we delegate damage responsibility (true)
  // or damage type responsibility (false)
  @Input() isDelegatingDamage: boolean;

  // output (event) in case if we delegated some responsibility (for damage or damage type)
  @Output() responsibilitySubmitted = new EventEmitter<boolean>();

  tenantsList: Array<Tenant>;

  // Elements for bootstrap paging
  currentPage: number;
  totalItems: number;
  maxSize: number;
  itemsPerPage: number;

  // Elements for getting data from server
  pageSize: string;
  sortDirection: string;
  sortProperties: string[];


  constructor(public modalRef: BsModalRef, private tenantService: TenantService,
              private jwtService: JwtService, private toasterService: ToasterService,
              private damageService: DamageService,
              private damageTypeResponsibilityService: DamageTypeResponsibilityService) {
    // Sett up for bootstrap paging
    this.currentPage = 1;
    this.maxSize = 3;
    this.itemsPerPage = 4;

    // Sett up for getting damages request
    this.pageSize = '4';
    this.sortDirection = 'ASC';
    this.sortProperties = ['firstName'];
  }

  ngOnInit() {
    this.loadList();
  }

  pageChanged(event: any) {
    this.currentPage = event.page;
    this.loadList();
  }

  delegateResponsibilityPerson(tenant: Tenant) {
    if(this.setResponsibility) {
      // in case if council president set responsible tenant for some damage type in some building
      this.damageTypeResponsibilityService.updatePart(new ResourceId(tenant.id), 'buildings', this.jwtService.getBuildingIdFromToken(),
        'damage_types', this.damageTypeResponsibility.damageTypeId, 'responsible_person')
        .subscribe((updatedDamageTypeResponsibility) => {
        this.damageTypeResponsibility.tenantId = updatedDamageTypeResponsibility.tenantId;
          this.damageTypeResponsibility.tenantName = updatedDamageTypeResponsibility.tenantName;
          this.responsibilitySubmitted.emit(true);
        }, (error: AppError) => {
          if(error instanceof BadRequestError)
            this.toasterService.pop('error', 'Error', 'Tenant doesn\'t live in this building!');
          else if(error instanceof ForbiddenError)
            this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
          else if(error instanceof NotFoundError)
            this.toasterService.pop('error', 'Error', 'Building/Damage type/Tenant not found!');
          else {
            this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
            throw error;
          }
        });
    }
    else {
      if(this.isDelegatingDamage) {
        // in case if current responsible tenant for some damage delegate that responsibility to another tenant
        this.damageService.updatePart(new ResourceId(tenant.id), 'tenants', this.jwtService.getIdFromToken(),
          'responsible_for_damages', this.damage.id, 'delegate_responsibility')
          .subscribe(() => {
            this.responsibilitySubmitted.emit(true);
          }, (error: AppError) => {
            if (error instanceof BadRequestError)
              this.toasterService.pop('error', 'Error', 'Tenant doesn\'t live in this building!');
            else if (error instanceof ForbiddenError)
              this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
            else if (error instanceof NotFoundError)
              this.toasterService.pop('error', 'Error', 'Tenant/Damage not found!');
            else {
              this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
              throw error;
            }
          });
      }
      else {
        // in case if current responsible tenant for some damage type delegate that responsibility to another tenant
        this.damageTypeResponsibilityService.updatePart(new ResourceId(tenant.id), 'tenants', this.damageTypeResponsibility.tenantId,
          'damage_types', this.damageTypeResponsibility.damageTypeId, 'delegate_responsibility')
          .subscribe(() => {
            this.responsibilitySubmitted.emit(true);
          }, (error: AppError) => {
            if (error instanceof BadRequestError)
              this.toasterService.pop('error', 'Error', 'Tenant doesn\'t live in this building!');
            else if (error instanceof ForbiddenError)
              this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
            else if (error instanceof NotFoundError)
              this.toasterService.pop('error', 'Error', 'Tenant/Damage responsibility not found!');
            else {
              this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
              throw error;
            }
          });
      }
    }
    this.modalRef.hide();
  }

  private loadList() {
    let page = new PageAndSort((this.currentPage - 1).toString(), this.pageSize, this.sortDirection, this.sortProperties);
    this.tenantService.getPage(page)
      .subscribe(tenants => {
        if (tenants.length > 0)
          this.totalItems = tenants[0].numberOfElements;
        else this.totalItems = 0;
        this.tenantsList = tenants;
      }, (error: AppError) => {
        this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
        throw error;
      });
  }
}
