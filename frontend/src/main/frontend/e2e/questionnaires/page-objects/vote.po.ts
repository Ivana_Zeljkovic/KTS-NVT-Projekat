import {by, element, ElementArrayFinder, ElementFinder} from "protractor";

export class VotePage {
  // get methods
  getVoteElement(): ElementFinder {
    return element(by.tagName('app-vote'));
  }

  getTitle(): ElementFinder {
    return element(by.css('app-vote h4.modal-title'));
  }

  getQuestionList(): ElementFinder {
    return element(by.css('.question-list'));
  }

  getQuestions(): ElementArrayFinder {
    return element.all(by.css('.question-list > li'));
  }

  getQuestionContent(index: number): ElementFinder {
    return element(by.css(`.question-list > li:nth-child(${index+1}) > span`));
  }

  getQuestionAnswers(index: number): ElementArrayFinder {
    return element.all(by.css(`.question-list > li:nth-child(${index+1}) table > tbody > tr`));
  }

  getQuestionAnswerText(index: number, indexAnswer: number): ElementFinder {
    return element(by.css(`.question-list > li:nth-child(${index+1}) table > tbody > tr:nth-child(${indexAnswer+1}) > td:nth-child(1)`));
  }

  getQuestionAnswerRadioButton(index: number, indexAnswer: number): ElementFinder {
    return element(by.css(`.question-list > li:nth-child(${index+1}) table > tbody > tr:nth-child(${indexAnswer+1}) > td:nth-child(2) > input`));
  }

  getSaveButton(): ElementFinder {
    return element(by.css('.btn-save'));
  }

  getResetButton(): ElementFinder {
    return element(by.css('.btn-reset'));
  }

  getCancelButton(): ElementFinder {
    return element(by.css('.buttons > button:nth-child(3)'));
  }
}
