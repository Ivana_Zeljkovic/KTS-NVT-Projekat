package rs.ac.uns.ftn.ktsnvt.repository.unit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.model.entity.DamageType;
import rs.ac.uns.ftn.ktsnvt.repository.DamageTypeRepository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Katarina Cukurov on 28/11/2017.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class DamageTypeRepositoryUnitTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private DamageTypeRepository damageTypeRepository;

    private Long damage_type_id;


    @Before
    public void setUp(){
        // DAMAGE TYPE
        DamageType damageType = new DamageType("ELECTRICAL_DAMAGE");
        this.damage_type_id = (Long)entityManager.persistAndGetId(damageType);
    }

    @Test
    public void testFindByName(){
        /*
         * Test proverava ispravnost rada metode findByName repozitorijuma DamageTypeRepository
         * (pronalazenje svih tipova kvara koji su u bazi na osnovu naziva kvara)
         * */

        /*
        postoji tip kvara sa sledecim podacima:
            id = damage_type_id,
            name = ELECTRICAL_DAMAGE
        */
        DamageType damageType = this.damageTypeRepository.findByName("ELECTRICAL_DAMAGE");

        assertNotNull(damageType);
        assertEquals("ELECTRICAL_DAMAGE", damageType.getName());
        assertEquals(this.damage_type_id, damageType.getId());
    }
}
