import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { ToasterConfig, ToasterService } from "angular5-toaster/dist";
// validator
import { SpaceValidator } from "../../shared/validators/space.validator";
import { PasswordValidator } from "../../shared/validators/password.validator";
import { UsernameUniqueValidator } from "../../shared/validators/username-unique.validator";
// model
import { Login } from "../../shared/models/login";
import { AdministratorCreate } from "../../shared/models/administrator-create";
// service
import { AdminService } from "../../core/services/admin.service";
// error
import { AppError } from "../../shared/errors/app-error";
import { BadRequestError } from "../../shared/errors/bad-request-error";
import { NotFoundError } from "../../shared/errors/not-found-error";


@Component({
  selector: 'app-admin-registration',
  templateUrl: './admin-registration.component.html',
  styleUrls: ['./admin-registration.component.css']
})
export class AdminRegistrationComponent implements OnInit {
  form: FormGroup;
  returnURL: string;
  toasterConfig: ToasterConfig;

  constructor(private fb: FormBuilder, private router: Router, private route: ActivatedRoute, private toasterService: ToasterService,
              private usernameUniqueValidator: UsernameUniqueValidator, private passwordValidator: PasswordValidator,
              private adminService: AdminService) {
    this.form = this.fb.group({
      firstName: [ '', [
        Validators.required,
        Validators.minLength(2)
      ]],
      lastName: [ '', [
        Validators.required,
        Validators.minLength(2)
      ]],
      username: [ '', [
        Validators.required,
        SpaceValidator.cannotContainSpace
      ],
        this.usernameUniqueValidator.isUsernameTaken.bind(this.usernameUniqueValidator)
      ],
      passwords: fb.group({
        password: ['', [
          Validators.required,
          Validators.minLength(5)
        ]],
        confirmPassword: ['',
          Validators.required
        ]
      }, { validator: this.passwordValidator.matchPasswords.bind(this.passwordValidator)})
    });
  }

  ngOnInit() {
    this.returnURL = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  get firstName() {
    return this.form.get('firstName');
  }

  get lastName() {
    return this.form.get('lastName');
  }

  get username() {
    return this.form.get('username');
  }

  get passwords() {
    return this.form.get('passwords');
  }

  get password() {
    return this.form.get('passwords').get('password');
  }

  get confirmPassword() {
    return this.form.get('passwords').get('confirmPassword');
  }

  register() {
    let login = new Login(this.username.value, this.password.value);
    let admin = new AdministratorCreate(login, this.firstName.value, this.lastName.value);
    this.adminService.save(admin).subscribe(data => {
        this.toasterService.pop('success', 'Congratulation', 'Successfully registered new admin!');
        setTimeout(() => {
          this.router.navigateByUrl(this.returnURL);
        }, 2000);
      }, (error: AppError) => {
        if (error instanceof BadRequestError)
          this.toasterService.pop('error', 'Error', 'Given data have bad format!');
        else if (error instanceof NotFoundError)
          this.toasterService.pop('error', 'Error', 'Registration can\'t be done, because admin role is missing!');
        else {
          this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
          throw error;
        }
      });
  }
}
