package rs.ac.uns.ftn.ktsnvt.service;


import rs.ac.uns.ftn.ktsnvt.model.entity.Question;

import java.util.Map;

/**
 * Created by Ivana Zeljkovic on 19/11/2017.
 */
public interface QuestionService {

    void checkQuestions(Map<Long, String> questionsIdAnswer, Long questionnaireId);

    void removeQuestions(Long questionnaireId);

    void remove(Long questionId);

    Question findOne(Long questionId);

    void removeAnswers(Long questionId);
}
