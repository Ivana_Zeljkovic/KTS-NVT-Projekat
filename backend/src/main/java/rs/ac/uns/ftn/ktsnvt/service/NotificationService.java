package rs.ac.uns.ftn.ktsnvt.service;

import org.springframework.data.domain.Page;
import rs.ac.uns.ftn.ktsnvt.model.entity.Notification;

import java.util.Date;

/**
 * Created by Ivana Zeljkovic on 06/11/2017.
 */
public interface NotificationService {

    Notification save(Notification notification);

    Page<Notification> findAll(Long billboardId, Integer pageNumber, Integer pageSize,
                               String sortDirection, String sortProperty);

    Page<Notification> findAllFromTo(Long billboardId, Date from, Date to, Integer pageNumber,
                                     Integer pageSize, String sortDirection, String sortProperty);

    Notification findOne(Long id);

    void findOneByIdAndBillboard(Long id, Long billboardId);

    void remove(Long notificationId);
}
