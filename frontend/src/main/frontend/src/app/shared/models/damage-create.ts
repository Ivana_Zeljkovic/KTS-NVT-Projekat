export class DamageCreate {
  constructor(public description: string,
              public typeName: string,
              public urgent: boolean,
              public apartmentId?: number,
              public image?: string) { }
}
