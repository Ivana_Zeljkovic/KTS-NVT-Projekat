import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
// service
import { DataService } from "./data.service";
// models
import { Tenant } from "../../shared/models/tenant";
import { PageAndSort } from "../../shared/models/page-and-sort";


@Injectable()
export class TenantService extends DataService<Tenant> {

  constructor(http: HttpClient) {
    super('tenants', http);
  }

  confirmRegistration(id: number): Observable<Tenant> {
    return this.http.patch<Tenant>(`${this.urlBase}/tenants/${id}/confirm`, {})
      .catch(this.handleErrors);
  }

  getAllByNameAndUsernameInMyBuilding(apartmentId: string, name: string, lastName: string, username: string): Observable<Array<Tenant>> {
    let params: HttpParams = new HttpParams()
      .append('apartmentId', apartmentId)
      .append('name', name)
      .append('lastName', lastName)
      .append('username', username);

    return this.http.get<Array<Tenant>>(`${this.urlBase}/tenants/tenants_in_building`, {params})
      .catch(this.handleErrors);
  }

  getAllByNameOrUsername(name: string, lastName: string, username: string): Observable<Array<Tenant>> {
    let params: HttpParams = new HttpParams()
      .append('name', name)
      .append('lastName', lastName)
      .append('username', username);

    return this.http.get<Array<Tenant>>(`${this.urlBase}/tenants/tenants_by_username`, {params})
      .catch(this.handleErrors);
  }

  getTenantsByBuildingId(pageAndSort: PageAndSort, buildingId: string): Observable<Array<Tenant>> {
    let params: HttpParams = new HttpParams()
      .append('buildingId', buildingId)
      .append('pageNumber', pageAndSort.pageNumber)
      .append('pageSize', pageAndSort.pageSize)
      .append('sortDirection', pageAndSort.sortDirection)
      .append('sortProperty', pageAndSort.sortProperties[0]);

    return this.http.get<Array<Tenant>>(`${this.urlBase}/tenants/tenants_by_building_id`, {params})
      .catch(this.handleErrors);
  }

  getCouncilMembers(pageAndSort: PageAndSort, buildingId: number): Observable<Array<Tenant>> {
    let params: HttpParams = new HttpParams()
      .append('pageNumber', pageAndSort.pageNumber)
      .append('pageSize', pageAndSort.pageSize)
      .append('sortDirection', pageAndSort.sortDirection)
      .append('sortProperty', pageAndSort.sortProperties[0]);
    return this.http.get<Array<Tenant>>(`${this.urlBase}/buildings/${buildingId}/council/members`, {params})
      .catch(this.handleErrors);
  }
}
