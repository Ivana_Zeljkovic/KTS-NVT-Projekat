import { Component } from '@angular/core';
import { ToasterConfig, ToasterService } from "angular5-toaster/dist";
import { ActivatedRoute } from "@angular/router";
import { BsModalService } from "ngx-bootstrap";
import * as _ from "lodash";
// model
import { Meeting } from "../../shared/models/meeting";
import { MeetingItem } from "../../shared/models/meeting-item";
import { PageAndSort } from "../../shared/models/page-and-sort";
import { QuestionnaireUpdate } from "../../shared/models/questionnaire-update";
// service
import { MeetingService } from "../../core/services/meeting.service";
import { MeetingItemService } from "../../core/services/meeting-item.service";
import { JwtService } from "../../core/services/jwt.service";
// error
import { AppError} from "../../shared/errors/app-error";
import { BadRequestError } from "../../shared/errors/bad-request-error";
import { ForbiddenError } from "../../shared/errors/forbidden-error";
import { NotFoundError } from "../../shared/errors/not-found-error";
// component
import { QuestionnaireComponent } from "../../shared/components/questionnaire/questionnaire.component";


@Component({
  selector: 'app-meeting-details',
  templateUrl: './meeting-details.component.html',
  styleUrls: ['./meeting-details.component.css']
})
export class MeetingDetailsComponent {
  meeting: Meeting;
  meetingId: number;
  meetingItemList: Array<MeetingItem>;
  toasterConfig: ToasterConfig;

  // Elements for bootstrap paging
  currentPage: number;
  totalItems: number;
  maxSize: number;
  itemsPerPage: number;

  // Elements for getting data from server
  pageSize: string;
  sortDirection: string;
  sortProperties: string[];


  constructor(private route: ActivatedRoute, private meetingService: MeetingService,
              private meetingItemService: MeetingItemService, private toasterService: ToasterService,
              private jwtService: JwtService, private modalService: BsModalService) {
    this.toasterConfig = new ToasterConfig({
      timeout: {success: 2000, error: 4000}
    });

    // Sett up for bootstrap paging
    this.currentPage = 1;
    this.maxSize = 3;
    this.itemsPerPage = 4;

    // Sett up for getting damages request
    this.pageSize = '4';
    this.sortDirection = 'ASC';
    this.sortProperties = ['date'];

    this.route.params.subscribe(params => {
      if (params['id']) {
        this.meetingId = params['id'];
        this.loadMeeting();
      }
    });
  }

  viewQuestionnaire(meetingItem) {
    const modalRef = this.modalService.show(QuestionnaireComponent);

    modalRef.content.isReviewMode = true;
    modalRef.content.model = new QuestionnaireUpdate(meetingItem.questionnaire.id, meetingItem.questionnaire.questions);
    modalRef.content.initialValues = _.cloneDeep(modalRef.content.model);
  }

  pageChanged(event: any) {
    this.currentPage = event.page;
    this.loadMeetingItems();
  }

  private loadMeeting() {
    this.meetingService.get(this.meetingId, 'buildings', this.jwtService.getBuildingIdFromToken(), 'council/meetings')
      .subscribe(meeting => {
        this.meeting = meeting;
        this.loadMeetingItems();
      }, (error: AppError) => {
        if(error instanceof BadRequestError)
          this.toasterService.pop('error', 'Error', 'Selected meeting is already finished! ' +
            'You can see details from this meeting in his record.');
        else if(error instanceof ForbiddenError)
          this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
        else if(error instanceof NotFoundError)
          this.toasterService.pop('error', 'Error', 'Meeting not found');
        else {
          this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
          throw error;
        }
      })
  }

  private loadMeetingItems() {
    let page = new PageAndSort((this.currentPage - 1).toString(), this.pageSize, this.sortDirection, this.sortProperties);

    this.meetingItemService.getPage(page, 'buildings', this.jwtService.getBuildingIdFromToken(), 'meetings',
      this.meetingId, 'meeting_items')
      .subscribe(meetingItems => {
        if (meetingItems.length > 0)
          this.totalItems = meetingItems[0].numberOfElements;
        else this.totalItems = 0;
        this.meetingItemList = meetingItems;
      }, (error: AppError) => {
        if(error instanceof BadRequestError)
          this.toasterService.pop('error', 'Error', 'Selected meeting-details doesn\'t belong to this building or ' +
            'selected meeting-details is in progress/already finished!');
        else if(error instanceof ForbiddenError)
          this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
        else if(error instanceof NotFoundError)
          this.toasterService.pop('error', 'Error', 'Meeting not found');
        else {
          this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
          throw error;
        }
      });
  }
}
