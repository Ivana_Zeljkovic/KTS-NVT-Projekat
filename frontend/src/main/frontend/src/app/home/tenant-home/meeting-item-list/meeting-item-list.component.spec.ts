import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeetingItemListComponent } from './meeting-item-list.component';

describe('MeetingItemListComponent', () => {
  let component: MeetingItemListComponent;
  let fixture: ComponentFixture<MeetingItemListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeetingItemListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeetingItemListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
