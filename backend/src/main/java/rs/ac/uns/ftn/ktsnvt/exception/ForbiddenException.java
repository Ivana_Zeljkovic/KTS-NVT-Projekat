package rs.ac.uns.ftn.ktsnvt.exception;

/**
 * Created by Ivana Zeljkovic on 25/11/2017.
 */
public class ForbiddenException extends RuntimeException {

    public ForbiddenException() { }

    public ForbiddenException(String message) {
        super(message);
    }
}
