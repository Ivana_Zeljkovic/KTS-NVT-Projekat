import {by, element, ElementFinder} from "protractor";

export class ChooseManagerModalPage {
  // get methods
  getPageTitle(): ElementFinder {
    return element(by.tagName('h4'));
  }

  getBusinessTable(): ElementFinder {
    return element(by.id('businessTable'));
  }

  getPagination(): ElementFinder {
    return element(by.className("col-md-12 div-page"));
  }

  getNoElements(): ElementFinder {
    return element(by.tagName('h3'));
  }

  getCloseButton(): ElementFinder {
    return element(by.css('.btn.btn-danger'));
  }
}
