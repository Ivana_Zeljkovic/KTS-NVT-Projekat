package rs.ac.uns.ftn.ktsnvt.web.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ivana Zeljkovic on 13/11/2017.
 */
public class QuestionnaireCreateDTO implements Serializable {

    @NotNull
    @Valid
    @Size(min = 1)
    private List<QuestionCreateDTO> questions;

    public QuestionnaireCreateDTO() {
        this.questions = new ArrayList<>();
    }

    public QuestionnaireCreateDTO(List<QuestionCreateDTO> questions) {
        this.questions = questions;
    }

    public List<QuestionCreateDTO> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionCreateDTO> questions) {
        this.questions = questions;
    }
}
