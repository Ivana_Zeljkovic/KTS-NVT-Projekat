import { browser } from "protractor";
import {BusinessRegistrationPage} from "../../page-objects/business-registration.po";
import {BusinessAddAcountModalPage} from "../../shared/business-add-account/page-objects/businesss-add-account.po";

describe( 'Business registration', () => {
  let businessRegistrationPage: BusinessRegistrationPage;
  let businessAddAccount : BusinessAddAcountModalPage;

  beforeEach(() => {
    businessRegistrationPage = new BusinessRegistrationPage();
    businessAddAccount = new BusinessAddAcountModalPage();

    browser.get('http://localhost:49152/#/registration/institution')

  });


  it('should successfully populate data and add one account, so business can be created', () =>{

    expect(businessRegistrationPage.getNameInput().isDisplayed()).toBe(true);
    expect(businessRegistrationPage.getEmailInput().isDisplayed()).toBe(true);
    expect(businessRegistrationPage.getPhoneNumberInput().isDisplayed()).toBe(true);
    expect(businessRegistrationPage.getStreetInput().isDisplayed()).toBe(true);
    expect(businessRegistrationPage.getNumberInput().isDisplayed()).toBe(true);
    expect(businessRegistrationPage.getCityInput().isDisplayed()).toBe(true);
    expect(businessRegistrationPage.getPostalCodeInput().isDisplayed()).toBe(true);
    expect(businessRegistrationPage.getDescriptionInput().isDisplayed()).toBe(true);
    expect(businessRegistrationPage.getRegisterButton().isEnabled()).toBe(false);

    businessRegistrationPage.setName('MUP Novi Sad');
    businessRegistrationPage.setEmail('informatika@gmail.com');
    businessRegistrationPage.setPhoneNumber('315890');
    businessRegistrationPage.setStreet('Bulevar Kralja Petra');
    businessRegistrationPage.setNumber('79');
    businessRegistrationPage.setCity('Novi Sad');
    businessRegistrationPage.setPostalCode('21000');
    businessRegistrationPage.setDescription('MUP Novi Sad je novosadsko odeljenje ministartstva unutrasnjih poslova');

    businessRegistrationPage.getAddNewAccountButton().click().then(function () {
          businessAddAccount.setUsername('jovan12');
          businessAddAccount.setPassword('jovan12');
          businessAddAccount.setConfirmPassword('jovan12');
          businessAddAccount.getAddAccountButton().click();
    });

    expect(businessRegistrationPage.getRegisterButton().isEnabled()).toBe(true);

  });

  it('should show error for required business name when user put focus and leaves it blank', () =>{

    businessRegistrationPage.getNameInput().click();
    businessRegistrationPage.getPanelHeading().click();
    expect(businessRegistrationPage.getNameRequiredError().isDisplayed()).toBe(true);

  });

  it('should show error for minimum length on name field', () =>{

    businessRegistrationPage.setName('a');
    businessRegistrationPage.getPanelHeading().click();
    expect(businessRegistrationPage.getNameMinimumLengthError().isDisplayed()).toBe(true);

  });

  it('should show error for required field when email looses focus', () =>{

    businessRegistrationPage.getEmailInput().click();
    businessRegistrationPage.getPanelHeading().click();
    expect(businessRegistrationPage.getEmailRequiredError().isDisplayed()).toBe(true);

  });


  it('should show error for invalid email format', () =>{

    businessRegistrationPage.setEmail('fasgasgas');
    businessRegistrationPage.getPanelHeading().click();
    expect(businessRegistrationPage.getValidEmailError().isDisplayed()).toBe(true);

  });

  it('should show error for required field when phone number field looses focus', () =>{

    businessRegistrationPage.getPhoneNumberInput().click();
    businessRegistrationPage.getPanelHeading().click();
    expect(businessRegistrationPage.getPhoneNumberRequiredError().isDisplayed()).toBe(true);

  });

  it('should show error when user enters numbers with space on phone number field', () =>{

    businessRegistrationPage.setPhoneNumber('513 5132');
    businessRegistrationPage.getPanelHeading().click();
    expect(businessRegistrationPage.getPhoneNumberSpaceError().isDisplayed()).toBe(true);

  });

  it('should show error when user enters invalid characters', () =>{

    businessRegistrationPage.setPhoneNumber('513af5132');
    businessRegistrationPage.getPanelHeading().click();
    expect(businessRegistrationPage.getPhoneNumberLengthError().isDisplayed()).toBe(true);

  });

  it('should show error for required field when street looses focus', () =>{

    businessRegistrationPage.getStreetInput().click();
    businessRegistrationPage.getPanelHeading().click();
    expect(businessRegistrationPage.getStreetRequiredError().isDisplayed()).toBe(true);

  });

  it('should show error for minimum length for street name when street name loses focus', () =>{

    businessRegistrationPage.setStreet('a');
    businessRegistrationPage.getPanelHeading().click();
    expect(businessRegistrationPage.getStreetMinimumLegthError().isDisplayed()).toBe(true);

  });

  it('should show error for number field when it loses focus', () =>{

    businessRegistrationPage.getNumberInput().click();
    businessRegistrationPage.getPanelHeading().click();
    expect(businessRegistrationPage.getNumberRequiredError().isDisplayed()).toBe(true);

  });

  it('should show error for number field when it has spaces inside', () =>{

    businessRegistrationPage.setNumber('21 214');
    businessRegistrationPage.getPanelHeading().click();
    expect(businessRegistrationPage.getNumberSpaceError().isDisplayed()).toBe(true);

  });

  it('should show error for city field when it loses focus', () =>{

    businessRegistrationPage.getCityInput().click();
    businessRegistrationPage.getPanelHeading().click();
    expect(businessRegistrationPage.getCityRequiredError().isDisplayed()).toBe(true);

  });

  it('should show error for postal code field when it loses focus', () =>{

    businessRegistrationPage.getPostalCodeInput().click();
    businessRegistrationPage.getPanelHeading().click();
    expect(businessRegistrationPage.getPostalCodeRequiredError().isDisplayed()).toBe(true);

  });

  it('should show error for postal code field when it doesn\'t include numbers', () =>{

    businessRegistrationPage.setPostalCode('41fsaf');
    businessRegistrationPage.getPanelHeading().click();
    expect(businessRegistrationPage.getPostalCodeLengthError().isDisplayed()).toBe(true);

  });

  it('should show error for description field when it loses focus', () =>{

    businessRegistrationPage.getDescriptionInput().click();
    businessRegistrationPage.getPanelHeading().click();
    expect(businessRegistrationPage.getDescriptionRequiredError().isDisplayed()).toBe(true);

  });

  it('should show error for description field when it has fewer than 10 characters', () =>{

    businessRegistrationPage.setDescription('fsaf');
    businessRegistrationPage.getPanelHeading().click();
    expect(businessRegistrationPage.getDescriptionMinimumLengthError().isDisplayed()).toBe(true);

  });


});
