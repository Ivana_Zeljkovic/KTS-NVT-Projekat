import { VoteQuestionCreate } from "./vote-question-create";

export class VoteCreate {
  constructor(public questionVotes: Array<VoteQuestionCreate>) { }
}
