package rs.ac.uns.ftn.ktsnvt.web.controller;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Address;
import rs.ac.uns.ftn.ktsnvt.model.entity.Building;
import rs.ac.uns.ftn.ktsnvt.model.entity.City;
import rs.ac.uns.ftn.ktsnvt.service.*;
import rs.ac.uns.ftn.ktsnvt.web.controller.util.MessageConstants;
import rs.ac.uns.ftn.ktsnvt.web.dto.BuildingDTO;
import rs.ac.uns.ftn.ktsnvt.model.entity.*;
import rs.ac.uns.ftn.ktsnvt.web.dto.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

import static rs.ac.uns.ftn.ktsnvt.web.util.ConverterDTOToModel.convertAdressDTOToAddress;
import static rs.ac.uns.ftn.ktsnvt.web.util.ConverterDTOToModel.convertBuildingCreateDTOtoBuilding;
import static rs.ac.uns.ftn.ktsnvt.web.util.ConverterDTOToModel.convertCityDTOToCity;


/**
 * Created by Ivana Zeljkovic on 06/11/2017.
 */
@RestController
@RequestMapping(value = "/api/buildings")
@Api(value="buildings", description="Operations pertaining to buildings in application.")
public class BuildingController {

    @Autowired
    private BuildingService buildingService;

    @Autowired
    private BillboardService billboardService;

    @Autowired
    private CouncilService councilService;

    @Autowired
    private BusinessService businessService;

    @Autowired
    private CityService cityService;

    @Autowired
    private ApartmentService apartmentService;

    @Autowired
    private AddressService addressService;

    @Autowired
    private TenantService tenantService;



    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a list of all buildings in page form.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of buildings", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to view these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    })
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity getAllBuildings(
            @ApiParam(value = "The page number for getting buildings.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting buildings.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting buildings.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "The sort property for getting buildings.", required = true) @RequestParam String sortProperty) {

        Page<Building> buildings = this.buildingService.getAllBuildings(Integer.valueOf(pageNumber),
                Integer.valueOf(pageSize), sortDirection, sortProperty);

        List<BuildingDTO> buildingDTOS = new ArrayList<>();

        buildings.getContent().forEach(building -> buildingDTOS.add(new BuildingDTO(building,
                buildings.getTotalElements())));

        return new ResponseEntity<>(buildingDTOS, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/firms/{id}/buildings",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a list of all buildings managed by firm with id, in page form.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of buildings managed by the firm", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to view these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    })
    @PreAuthorize("hasAuthority('COMPANY')")
    public ResponseEntity getAllBuildingsByFirm(
            @ApiParam(value = "The ID of the firm.", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "The page number for getting building.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting building.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting building.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "The sort property for getting building.", required = true) @RequestParam String sortProperty) {

        Page<Building> buildings = this.buildingService.findAllByFirm(id, Integer.valueOf(pageNumber),
                Integer.valueOf(pageSize), sortDirection, sortProperty);

        List<BuildingDTO> buildingDTOS = new ArrayList<>();
        buildings.getContent().forEach(building ->
                buildingDTOS.add(new BuildingDTO(building, buildings.getTotalElements())));

        return new ResponseEntity<>(buildingDTOS, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/building_address",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a single building with given address.",
            notes = "You have to provide a valid address parameters.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved building", response = BuildingDTO.class),
            @ApiResponse(code = 400, message = "Some of required parameters in URL is missing"),
            @ApiResponse(code = 401, message = "You are not authorized to view these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity getBuildingFromAddress(
            @ApiParam(value = "The city name.", required = true) @RequestParam("city_name") String cityName) {

        City city = this.cityService.findByName(cityName);
        if(city == null)
            throw new BadRequestException("City does not exist.");

        List<Building> buildings = this.buildingService.findByCityId(city.getId());
        List<BuildingDTO> buildingDTOS = new ArrayList<>();
        for(Building b : buildings)
            buildingDTOS.add(new BuildingDTO(b));
        return new ResponseEntity<>(buildingDTOS, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a single building.",
            notes = "You have to provide a valid building ID.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved building", response = BuildingDTO.class),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity getBuilding(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("id") Long id){

        Building building = this.buildingService.findOne(id);

        for(GrantedAuthority authority : SecurityContextHolder.getContext().getAuthentication().getAuthorities()) {
            if(authority.getAuthority().equals(MessageConstants.PRESIDENT_ROLE))
                this.tenantService.checkPermissionForCurrentPresidentInBuilding(building);
        }

        return new ResponseEntity<>(new BuildingDTO(building), HttpStatus.OK);
    }



    @RequestMapping(
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Create a building resource.",
            notes = "Returns the building being saved.",
            httpMethod = "POST",
            produces = "application/json",
            consumes = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully created building", response = BuildingDTO.class),
            @ApiResponse(code = 400, message = "Inappropriate building object sent in request body"),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 403, message = "You don't have permission to create resource"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity registerBuilding(
            @ApiParam(value = "The building object", required = true) @Valid @RequestBody BuildingCreateDTO buildingCreateDTO,
            @ApiParam(value = "The object that contains all errors from validation of DTO object") BindingResult errors){

        Building building = convertBuildingCreateDTOtoBuilding(buildingCreateDTO);

        for(ApartmentCreateDTO apartmentCreateDTO : buildingCreateDTO.getApartments()){
            if(apartmentCreateDTO.getFloor() > building.getNumberOfFloors())
                throw new BadRequestException("Floor of the some apartment is not good.");
            int numberOfSame = 0;
            for(ApartmentCreateDTO apar : buildingCreateDTO.getApartments()){
                if(apar.getNumber() == apartmentCreateDTO.getNumber())
                    numberOfSame++;
            }
            if(numberOfSame > 1){
                throw new BadRequestException("More than one apartment has same number.");
            }
        }

        City city = this.cityService.findByName(buildingCreateDTO.getAddress().getCity().getName());
        if(city == null) {
            city = convertCityDTOToCity(buildingCreateDTO.getAddress().getCity());
            this.cityService.save(city);
        }

        Address address = this.addressService.findByCityStreetNumber(city.getId(), buildingCreateDTO.getAddress().getStreet(),
                buildingCreateDTO.getAddress().getNumber());
        if(address == null) {
            address = convertAdressDTOToAddress(buildingCreateDTO.getAddress());
            address.setCity(city);
            this.addressService.save(address);
        }
        else throw new BadRequestException("There is already building at this address.");

        building.setAddress(address);
        Billboard billboard = this.billboardService.save(new Billboard());
        building.setBillboard(billboard);

        Council council = new Council();
        if(buildingCreateDTO.getFirmManagerId() != null) {
            Business business = this.businessService.findOne(buildingCreateDTO.getFirmManagerId());
            if(!business.isIsCompany())
                throw new BadRequestException("Firm that manages with building must be some firm, not institution!");
            council.setResponsibleCompany(business);
        }

        council = this.councilService.save(council);
        building.setCouncil(council);
        building.setApartments(new ArrayList<>());
        building = this.buildingService.save(building);

        for(ApartmentCreateDTO apartmentCreateDTO : buildingCreateDTO.getApartments()){
            Apartment apartment = new Apartment(apartmentCreateDTO.getNumber(),
                    apartmentCreateDTO.getFloor(), apartmentCreateDTO.getSurface());
            apartment.setBuilding(building);
            this.apartmentService.save(apartment);
            building.getApartments().add(apartment);
        }

        return new ResponseEntity<>(new BuildingDTO(building), HttpStatus.CREATED);
    }



    @RequestMapping(
            value = "/{id}",
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Update a building resource.",
            notes = "You have to provide a valid building ID in the URL. " +
                    "Method returns the building being updated.",
            httpMethod = "PUT",
            produces = "application/json",
            consumes = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated building", response = BuildingDTO.class),
            @ApiResponse(code = 400, message = "Inappropriate building object sent in request body"),
            @ApiResponse(code = 401, message = "You are not authorized to update the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity updateBuilding(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "The building object.", required = true) @RequestBody @Valid BuildingUpdateDTO buildingUpdateDTO,
            @ApiParam(value = "The object that contains all errors from validation of DTO object") BindingResult errors){

        Building building = this.buildingService.findOne(id);

        City city = this.cityService.findByName(buildingUpdateDTO.getAddress().getCity().getName());
        if(city == null) {
            city = convertCityDTOToCity(buildingUpdateDTO.getAddress().getCity());
            this.cityService.save(city);
        }

        Address address = this.addressService.findByCityStreetNumber(city.getId(),
                buildingUpdateDTO.getAddress().getStreet(), buildingUpdateDTO.getAddress().getNumber());
        if(address == null) {
            address = convertAdressDTOToAddress(buildingUpdateDTO.getAddress());
            address.setCity(city);
            this.addressService.save(address);
            building.getAddress().getCity().getAddresses().remove(building.getAddress());
            this.cityService.save(building.getAddress().getCity());
            Long oldAddressId = building.getAddress().getId();
            building.setAddress(null);
            this.addressService.remove(oldAddressId);
        }
        else if(!(building.getAddress().getStreet().equals(buildingUpdateDTO.getAddress().getStreet()) &&
                building.getAddress().getNumber() ==  buildingUpdateDTO.getAddress().getNumber() &&
                building.getAddress().getCity().getPostalNumber() == buildingUpdateDTO.getAddress().getCity().getPostalNumber() &&
                building.getAddress().getCity().getName().equals(buildingUpdateDTO.getAddress().getCity().getName())))
            throw new BadRequestException("There is already building at this address.");

        building.setAddress(address);
        building.setNumberOfFloors(buildingUpdateDTO.getNumberOfFloors());
        building.setHasParking(buildingUpdateDTO.isHasParking());
        this.buildingService.save(building);

        return new ResponseEntity<>(new BuildingDTO(building), HttpStatus.OK);
    }



    @RequestMapping(
            value = "/{id}",
            method = RequestMethod.DELETE
    )
    @ApiOperation(
            value = "Delete a building resource.",
            notes = "You have to provide a valid building ID in the URL. " +
                    "Once deleted the resource can not be recovered.",
            httpMethod = "DELETE"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully deleted building"),
            @ApiResponse(code = 401, message = "You are not authorized to delete the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity deleteBuilding(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("id") Long id){

        Building building = this.buildingService.findOne(id);

        if(building.getApartments() != null){
            for(Apartment apartment : building.getApartments()){
                if(apartment.getOwner() != null)
                    throw new BadRequestException("The selected building has apartment with owner!");
                if(apartment.getTenants() != null && !apartment.getTenants().isEmpty())
                    throw new BadRequestException("The selected building has apartment with tenants!");
            }
        }

        if(building.getApartments() != null){
            for(Apartment apartment : building.getApartments()){
                this.apartmentService.remove(apartment.getId());
            }
        }

        this.buildingService.remove(building.getId());

        return new ResponseEntity(HttpStatus.OK);
    }
}
