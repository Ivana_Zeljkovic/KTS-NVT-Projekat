import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
// service
import { DataService } from "./data.service";
// models
import { Damage } from "../../shared/models/damage";
import { PageAndSort } from "../../shared/models/page-and-sort";


@Injectable()
export class DamageService extends DataService<Damage>{
  constructor(http: HttpClient) {
    super('', http);
  }

  searchFromTo(urlFirstSuffix: string, id: number, urlSecondSuffix: string, from: string, to: string, pageAndSort: PageAndSort): Observable<Array<Damage>> {
    let params: HttpParams = new HttpParams()
      .append('from', from)
      .append('to', to)
      .append('pageNumber', pageAndSort.pageNumber)
      .append('pageSize', pageAndSort.pageSize)
      .append('sortDirection', pageAndSort.sortDirection)
      .append('sortProperty', pageAndSort.sortProperties[0])
      .append('sortPropertySecond', pageAndSort.sortProperties[1]);

    return this.http.get<Array<Damage>>(`${this.urlBase}/${urlFirstSuffix}/${id}/${urlSecondSuffix}_from_to`, {params})
      .catch(this.handleErrors);
  }
}
