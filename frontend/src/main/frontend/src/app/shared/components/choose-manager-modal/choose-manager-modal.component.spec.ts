import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooseManagerModalComponent } from './choose-manager-modal.component';

describe('ChooseManagerModalComponent', () => {
  let component: ChooseManagerModalComponent;
  let fixture: ComponentFixture<ChooseManagerModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChooseManagerModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseManagerModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
