import { browser } from "protractor";
// page
import { DamageDetailsPage } from "../page-objects/damage-details.po";
import { DamageListPage } from "../../page-objects/damage-list.po";
import { DamageAddPage } from "../page-objects/damage-add.po";
import { LoginPage } from "../../page-objects/login.po";
import { NavbarPage } from "../../page-objects/navbar.po";
import { ConfirmDeletePage } from "../../page-objects/confirm-delete.po";


describe('All details about some damage', () => {
  let damageListPage: DamageListPage;
  let damageDetailsPage: DamageDetailsPage;
  let damageAddPage: DamageAddPage;
  let loginPage: LoginPage;
  let navbarPage: NavbarPage;
  let confirmDeletePage: ConfirmDeletePage;
  let existingDamage: any;

  beforeAll(() => {
    loginPage = new LoginPage();
    navbarPage = new NavbarPage();
    damageListPage = new DamageListPage();
    damageDetailsPage = new DamageDetailsPage();
    damageAddPage = new DamageAddPage();
    confirmDeletePage = new ConfirmDeletePage();

    existingDamage = {'description':'Some description', 'urgency':'true', 'creator':'', 'date':''};

    browser.get('/');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');

    loginPage.setUsername('kaca');
    loginPage.setPassword('kaca');
    loginPage.getSignInButton().click();

    // before all of these tests we should add damage in list
    browser.wait(function() {
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/home/notifications';
      });
    }).then(function() {
      expect(navbarPage.getNavbarElement().isDisplayed()).toBe(true);
      expect(navbarPage.getDamageDropdownLink().isDisplayed()).toBe(true);
      navbarPage.getDamageDropdownLink().click();

      browser.wait(function() {
        return navbarPage.getContentOfDamageDropdownLink().isDisplayed();
      }).then(function() {
        expect(navbarPage.getDamagesIReportedLink().isDisplayed()).toBe(true);
        navbarPage.getDamagesIReportedLink().click();

        browser.wait(function () {
          return damageListPage.getDamageListElement().isDisplayed();
        }).then(function () {
          //expect to see enable button Add on this page
          expect(damageListPage.getAddButton().isDisplayed()).toBe(true);
          expect(damageListPage.getAddButton().isEnabled()).toBe(true);

          damageListPage.getAddButton().click();

          browser.wait(function () {
            return damageAddPage.getDamageAddElement().isDisplayed();
          }).then(function () {
            damageAddPage.setDamageDescription(existingDamage.description);
            damageAddPage.getDamageUrgent(1).click();

            expect(damageAddPage.getSaveButton().isEnabled()).toBe(true);
            damageAddPage.getSaveButton().click();

            // expect to see this damage as first in damage list (with urgent label)
            expect(damageListPage.getDamageDescriptionFromDamagePanel(0).getText()).toEqual(existingDamage.description);
            expect(damageListPage.getLabelUrgentFromDamagePanel(0).isDisplayed()).toBe(true);
            damageListPage.getCreatorNameFromDamagePanel(0).getText().then(value => {
              existingDamage.creator = value;
            });
            damageListPage.getDateFromDamagePanel(0).getText().then(value => {
              existingDamage.date = value;
            });
          });
        });
      });
    });
  });

  afterAll(() => {
    // after all these tests, we should remove added damage
    navbarPage.getDamageDropdownLink().click();

    browser.wait(function() {
      return navbarPage.getContentOfDamageDropdownLink().isDisplayed();
    }).then(function() {
      expect(navbarPage.getDamagesIReportedLink().isDisplayed()).toBe(true);
      navbarPage.getDamagesIReportedLink().click();

      browser.wait(function () {
        return damageListPage.getDamageListElement().isDisplayed();
      }).then(function () {
        // delete option is third in option list (in menu)
        expect(damageListPage.getMenuFromDamagePanel(0).isDisplayed()).toBe(true);
        damageListPage.getMenuFromDamagePanel(0).click();

        browser.wait(function() {
          // delete is third option in damage menu
          return damageListPage.getLinkFromDamagePanel(0, 3).isDisplayed();
        }).then(function() {
          damageListPage.getLinkFromDamagePanel(0, 3).click();

          browser.wait(function() {
            // expect to show modal dialog for delete confirmation
            return confirmDeletePage.getConfirmDeleteElement().isDisplayed();
          }).then(function() {
            // expect to modal has all elements
            expect(confirmDeletePage.getConfirmButton().isDisplayed()).toBe(true);
            expect(confirmDeletePage.getConfirmButton().isEnabled()).toBe(true);
            expect(confirmDeletePage.getDeclineButton().isDisplayed()).toBe(true);
            expect(confirmDeletePage.getDeclineButton().isEnabled()).toBe(true);

            confirmDeletePage.getConfirmButton().click();
            browser.executeScript('window.localStorage.clear();');
          });
        });
      });
    });
  });



  it('should show all details about damage when user clik on Damage details option ' +
    'in damage menu', () => {
    expect(damageListPage.getMenuFromDamagePanel(0).isDisplayed()).toBe(true);
    damageListPage.getMenuFromDamagePanel(0).click();

    browser.wait(function() {
      return damageListPage.getContentOfMenuFromDamagePanel(0).isDisplayed();
    }).then(function() {
      expect(damageListPage.getDamageDetailsLinkFromDamagePanel(0).isDisplayed()).toBe(true);
      damageListPage.getDamageDetailsLinkFromDamagePanel(0).click();

      browser.wait(function() {
        return damageDetailsPage.getDamageDetailsElement().isDisplayed();
      }).then(function() {
        // expect to see all elements of damage details page
        expect(damageDetailsPage.getTitle().isDisplayed()).toBe(true);
        expect(damageDetailsPage.getTitle().getText()).toEqual('Damage details');
        expect(damageDetailsPage.getDamageCreator().getText()).toEqual(existingDamage.creator);
        expect(damageDetailsPage.getDamageDate().getText()).toEqual(existingDamage.date);
        expect(damageDetailsPage.getDamageDescription().getText()).toEqual(existingDamage.description);
        expect(damageDetailsPage.getDamageType().getText()).toEqual('Type: Other');
        expect(damageDetailsPage.getDamageAddress().getText()).toContain('Address: ');
        // because current logged tenant (president) is in same time creator and responsible person, we can
        // check full value of field Responsible person
        expect(damageDetailsPage.getDamageResponsiblePerson().getText()).toContain(
          `Responsible person: ${existingDamage.creator} (me)`);
        expect(damageDetailsPage.getLabelUrgent().isDisplayed()).toBe(true);

        // because current logged tenant (president) is in same time creator and responsible person, there is
        // comments section with Add button for sure
        expect(damageDetailsPage.getCommentsSectionTitle().isDisplayed()).toBe(true);
        expect(damageDetailsPage.getAddButton().isDisplayed()).toBe(true);
        expect(damageDetailsPage.getAddButton().isEnabled()).toBe(true);
      });
    });
  })
});
