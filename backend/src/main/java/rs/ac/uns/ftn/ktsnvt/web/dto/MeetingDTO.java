package rs.ac.uns.ftn.ktsnvt.web.dto;
import rs.ac.uns.ftn.ktsnvt.model.entity.Meeting;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Nikola Garabandic on 22/11/2017.
 */
public class MeetingDTO implements Serializable{

    private Long id;
    private Date date;
    private int numberOfMeetingItems;
    private RecordDTO record;
    private int duration;
    private long numberOfElements;

    public MeetingDTO(){ }

    public MeetingDTO(Meeting meeting) {
        this.id = meeting.getId();
        this.date = meeting.getDate();
        this.numberOfElements = meeting.getMeetingItems().size();
        this.record = meeting.getRecord() == null ? null : new RecordDTO(meeting.getRecord());
        this.duration = meeting.getDuration();
    }

    public MeetingDTO(Meeting meeting, long numberOfElements) {
        this.id = meeting.getId();
        this.date = meeting.getDate();
        this.numberOfMeetingItems = meeting.getMeetingItems().size();
        this.record = meeting.getRecord() == null ? null : new RecordDTO(meeting.getRecord());
        this.duration = meeting.getDuration();
        this.numberOfElements = numberOfElements;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getNumberOfMeetingItems() { return numberOfMeetingItems; }

    public void setNumberOfMeetingItems(int numberOfMeetingItems) { this.numberOfMeetingItems = numberOfMeetingItems; }

    public RecordDTO getRecord() { return record; }

    public void setRecord(RecordDTO record) { this.record = record; }

    public int getDuration() { return duration; }

    public void setDuration(int duration) { this.duration = duration; }

    public long getNumberOfElements() { return numberOfElements; }

    public void setNumberOfElements(long numberOfElements) { this.numberOfElements = numberOfElements; }
}
