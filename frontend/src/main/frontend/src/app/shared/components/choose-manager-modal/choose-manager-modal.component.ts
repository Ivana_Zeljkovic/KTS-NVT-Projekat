import {Component, Input, OnInit} from '@angular/core';
import {Business} from "../../models/business";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {ToasterConfig, ToasterService} from "angular5-toaster/dist";
import {Router} from "@angular/router";
import {BusinessService} from "../../../core/services/business.service";
import {PageAndSort} from "../../models/page-and-sort";
import {AppError} from "../../errors/app-error";
import {BadRequestError} from "../../errors/bad-request-error";
import {NotFoundError} from "../../errors/not-found-error";

@Component({
  selector: 'app-choose-manager-modal',
  templateUrl: './choose-manager-modal.component.html',
  styleUrls: ['./choose-manager-modal.component.css']
})
export class ChooseManagerModalComponent implements OnInit {

  businesses: Array<Business>;
  modalRef: BsModalRef;
  toasterConfig: ToasterConfig;
  @Input() manager: Business[];

  //Elemnts for paging
  currentPage: number;
  maxSize: number;
  itemsPerPage: number;
  pageSize: string;
  sortProperties: string[];
  sortDirection: string;
  totalItems: number;

  constructor(private businessService: BusinessService, private modalService: BsModalService,
              private toasterService: ToasterService, private router: Router) {
    this.businesses = new Array<Business>();
    // Set up for bootstrap paging
    this.currentPage = 1;
    this.maxSize = 3;
    this.itemsPerPage = 4;

    // Set up for getting notification request
    this.pageSize = '4';
    this.sortDirection = 'ASC';
    this.sortProperties = ['name'];
  }

  ngOnInit() {
    this.getBusinesses();
    this.manager = new Array<Business>();
  }

  getBusinesses(){
    let page = new PageAndSort((this.currentPage - 1).toString(), this.pageSize, this.sortDirection, this.sortProperties);
    this.businessService.getPage(page,'businesses')
      .subscribe(data => {
        this.businesses = data;
        if (data.length > 0)
          this.totalItems = data[0].numberOfElements;
        else this.totalItems = 0;
      }, (error: AppError) => {
        if (error instanceof BadRequestError)
          this.toasterService.pop('error', 'Error', 'Invalid format of given data!');
        else if (error instanceof NotFoundError)
          this.toasterService.pop('error', 'Error', 'Bad credentials!');
        else {
          this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
          throw error;
        }
      });
  }

  pageChanged(event: any) {
    this.currentPage = event.page;
    this.getBusinesses();
  }

  choose(manager: Business){
    this.manager.push(manager);
  }
}
