import {by, element, ElementFinder} from "protractor";

export class ChooseOwnerModalPage {
  // get methods
  getPageTitle(): ElementFinder {
    return element(by.tagName('h4'));
  }

  getTenantsTable(): ElementFinder {
    return element(by.id('tenantsTable'));
  }

  getPagination(): ElementFinder {
    return element(by.id('pagination'));
  }

  getNoElements(): ElementFinder {
    return element(by.tagName('h3'));
  }

  getCloseButton(): ElementFinder {
    return element(by.buttonText('Close'));
  }
}
