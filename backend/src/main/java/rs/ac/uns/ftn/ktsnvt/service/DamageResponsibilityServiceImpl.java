package rs.ac.uns.ftn.ktsnvt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.exception.ForbiddenException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Building;
import rs.ac.uns.ftn.ktsnvt.model.entity.DamageResponsibility;
import rs.ac.uns.ftn.ktsnvt.model.entity.DamageType;
import rs.ac.uns.ftn.ktsnvt.repository.DamageResponsibilityRepository;
import rs.ac.uns.ftn.ktsnvt.repository.DamageTypeRepository;

import java.util.List;

/**
 * Created by Ivana Zeljkovic on 23/11/2017.
 */
@Service
public class DamageResponsibilityServiceImpl implements DamageResponsibilityService {

    @Autowired
    private DamageResponsibilityRepository damageResponsibilityRepository;

    @Autowired
    private DamageTypeRepository damageTypeRepository;


    @Override
    @Transactional(readOnly = true)
    public DamageResponsibility findOneByDamageTypeAndTenantAndBuilding(Long damageTypeId, Long tenantId, Long buildingId) {
        DamageResponsibility damageResponsibility =
                this.damageResponsibilityRepository.findOneByDamageTypeAndTenantAndBuilding(damageTypeId, tenantId, buildingId);
        // ukoliko stanar koji je trenutno logovan nije odgovorna osoba za zadati tip kvara na nivou zgrade u kojoj zivi,
        // nije mu dozvoljeno delegiranje pomenute odgovornosti
        if(damageResponsibility == null)
            throw new ForbiddenException("You don't have permission for this action! " +
                    "(You are not responsible person for selected damage type in selected building)");
        return damageResponsibility;
    }

    @Override
    @Transactional(readOnly = true)
    public DamageResponsibility findOneByDamageTypeAndInstitutionAndBuilding(Long damageTypeId, Long institutionId, Long buildingId) {
        DamageResponsibility damageResponsibility =
                this.damageResponsibilityRepository.findOneByDamageTypeAndInstitutionAndBuilding(damageTypeId, institutionId, buildingId);
        // ukoliko institucija koja je trenutno logovana nije odgovorna institucija za zadati tip kvara na nivou zadate zgrade,
        // nije joj dozvoljeno delegiranje pomenute odgovornosti
        if (damageResponsibility == null)
            throw new ForbiddenException("You don't have permission for this action! " +
                    "(You are not responsible institution for selected damage type in selected building)");
        return damageResponsibility;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DamageResponsibility> findAllByInstitutionId(Long institutionId, Integer pageNumber,
                                                             Integer pageSize, String sortDirection, String sortProperty) {
        return this.damageResponsibilityRepository.findAllByInstitutionId(institutionId, new PageRequest(
                pageNumber, pageSize, (sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortProperty
        ));
    }

    @Override
    @Transactional(readOnly = true)
    public DamageResponsibility findOneByDamageTypeAndBuilding(Long damageTypeId, Long buildingId) {
        return this.damageResponsibilityRepository.findOneByDamageTypeAndBuilding(damageTypeId, buildingId);
    }

    @Override
    @Transactional(readOnly = false)
    public void remove(Long id) {
        this.damageResponsibilityRepository.delete(id);
    }

    @Override
    @Transactional(readOnly = false)
    public DamageResponsibility save(DamageResponsibility damageResponsibility) {
        return this.damageResponsibilityRepository.save(damageResponsibility);
    }

    @Override
    @Transactional(readOnly = true)
    public List<DamageResponsibility> findAllByBuildingId(Building building) {
        List<DamageResponsibility> damageResponsibilities = this.damageResponsibilityRepository.findAllByBuildingId(building.getId());
        List<DamageType> damageTypes = this.damageTypeRepository.findAll();

        if(damageResponsibilities.size() < damageTypes.size()) {
            boolean damageTypeFound = false;
            for(DamageType damageType : damageTypes) {
                for(DamageResponsibility damageResponsibility : damageResponsibilities) {
                    if(damageResponsibility.getDamageType().getId().equals(damageType.getId())) {
                        damageTypeFound = true;
                        break;
                    }
                }
                if(!damageTypeFound)
                    damageResponsibilities.add(new DamageResponsibility(null, null, damageType, building));
            }
        }

        return damageResponsibilities;
    }
}
