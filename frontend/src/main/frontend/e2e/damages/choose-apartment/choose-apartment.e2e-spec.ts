import { browser } from "protractor";
// page
import { DamageListPage } from "../../page-objects/damage-list.po";
import { DamageDetailsPage } from "../page-objects/damage-details.po";
import { DamageAddPage } from "../page-objects/damage-add.po";
import { ConfirmDeletePage } from "../../page-objects/confirm-delete.po";
import { NavbarPage } from "../../page-objects/navbar.po";
import { LoginPage } from "../../page-objects/login.po";
import { ChooseApartmentPage } from "../page-objects/choose-apartment.po";
// helper
import { FloorConverter } from "../../util/floor-converter";


describe('Choosing apartment as location of damage in process of reporting new damage', () => {
  let damageListPage: DamageListPage;
  let damageDetailsPage: DamageDetailsPage;
  let damageAddPage: DamageAddPage;
  let loginPage: LoginPage;
  let navbarPage: NavbarPage;
  let confirmDeletePage: ConfirmDeletePage;
  let chooseApartmentPage: ChooseApartmentPage;
  let existingDamage: any;

  beforeAll(() => {
    loginPage = new LoginPage();
    navbarPage = new NavbarPage();
    damageListPage = new DamageListPage();
    damageDetailsPage = new DamageDetailsPage();
    damageAddPage = new DamageAddPage();
    confirmDeletePage = new ConfirmDeletePage();
    chooseApartmentPage = new ChooseApartmentPage();

    existingDamage = {'description':'Some description', 'urgency':'true', 'apartmentFloor':'', 'apartmentNumber':''};

    browser.get('/');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');

    loginPage.setUsername('kaca');
    loginPage.setPassword('kaca');
    loginPage.getSignInButton().click();

    // before all of these tests we should add damage in list
    browser.wait(function () {
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/home/notifications';
      });
    }).then(function () {
      expect(navbarPage.getNavbarElement().isDisplayed()).toBe(true);
      expect(navbarPage.getDamageDropdownLink().isDisplayed()).toBe(true);
      navbarPage.getDamageDropdownLink().click();

      browser.wait(function () {
        return navbarPage.getContentOfDamageDropdownLink().isDisplayed();
      }).then(function () {
        expect(navbarPage.getDamagesIReportedLink().isDisplayed()).toBe(true);
        navbarPage.getDamagesIReportedLink().click();

        browser.wait(function () {
          return damageListPage.getDamageListElement().isDisplayed();
        }).then(function () {
          //expect to see enable button Add on this page
          expect(damageListPage.getAddButton().isDisplayed()).toBe(true);
          expect(damageListPage.getAddButton().isEnabled()).toBe(true);

          damageListPage.getAddButton().click();

          browser.wait(function () {
            return damageAddPage.getDamageAddElement().isDisplayed();
          }).then(function () {
            damageAddPage.setDamageDescription(existingDamage.description);
            damageAddPage.getDamageUrgent(1).click();
          });
        });
      });
    });
  });

  afterAll(() => {
    // after all these tests, we should remove added damage
    navbarPage.getDamageDropdownLink().click();

    browser.wait(function () {
      return navbarPage.getContentOfDamageDropdownLink().isDisplayed();
    }).then(function () {
      expect(navbarPage.getDamagesIReportedLink().isDisplayed()).toBe(true);
      navbarPage.getDamagesIReportedLink().click();

      browser.wait(function () {
        return damageListPage.getDamageListElement().isDisplayed();
      }).then(function () {
        // delete option is third in option list (in menu)
        expect(damageListPage.getMenuFromDamagePanel(0).isDisplayed()).toBe(true);
        damageListPage.getMenuFromDamagePanel(0).click();

        browser.wait(function () {
          // delete is third option in damage menu
          return damageListPage.getLinkFromDamagePanel(0, 3).isDisplayed();
        }).then(function () {
          damageListPage.getLinkFromDamagePanel(0, 3).click();

          browser.wait(function () {
            // expect to show modal dialog for delete confirmation
            return confirmDeletePage.getConfirmDeleteElement().isDisplayed();
          }).then(function () {
            // expect to modal has all elements
            expect(confirmDeletePage.getConfirmButton().isDisplayed()).toBe(true);
            expect(confirmDeletePage.getConfirmButton().isEnabled()).toBe(true);
            expect(confirmDeletePage.getDeclineButton().isDisplayed()).toBe(true);
            expect(confirmDeletePage.getDeclineButton().isEnabled()).toBe(true);

            confirmDeletePage.getConfirmButton().click();
            browser.executeScript('window.localStorage.clear();');
          });
        });
      });
    });
  });



  it('should open modal for choosing apartment as location for some damage and all its elements, when ' +
    'user click on Choose button in page for create damage', () => {
    damageAddPage.getDamageLocation(1).click();

    expect(damageAddPage.getButtonChooseApartment().isDisplayed()).toBe(true);
    expect(damageAddPage.getButtonChooseApartment().isEnabled()).toBe(true);
    damageAddPage.getButtonChooseApartment().click();

    browser.wait(function() {
      return chooseApartmentPage.getChooseApartmentElement().isDisplayed();
    }).then(function() {
      expect(chooseApartmentPage.getTitle().isDisplayed()).toBe(true);
      expect(chooseApartmentPage.getCloseButton().isDisplayed()).toBe(true);
      expect(chooseApartmentPage.getCloseButton().isEnabled()).toBe(true);
      chooseApartmentPage.getApartmentsRowInTable().count().then(value => {
        // every apartment row in table should have floor, number and button
        for(let i:number = 0; i < value; i++) {
          expect(chooseApartmentPage.getApartmentFloor(i).isDisplayed()).toBe(true);
          expect(chooseApartmentPage.getApartmentNumber(i).isDisplayed()).toBe(true);
          expect(chooseApartmentPage.getButtonChooseApartment(i).isDisplayed()).toBe(true);
          expect(chooseApartmentPage.getButtonChooseApartment(i).isEnabled()).toBe(true);
        }
      });
    });
  });



  it('should enable Save button in modal for creating new damage when user click on some apartment' +
    ' from table', () => {
    // choose first one apartment, but first store data about floor and number of tahta apartment
    chooseApartmentPage.getApartmentFloor(0).getText().then(value => {
      existingDamage.apartmentFloor = value;
    });
    chooseApartmentPage.getApartmentNumber(0).getText().then(value => {
      existingDamage.apartmentNumber = value;
    });

    chooseApartmentPage.getButtonChooseApartment(0).click();

    expect(damageAddPage.getSaveButton().isEnabled()).toBe(true);
    damageAddPage.getSaveButton().click();

    // go to Damage details page of this (new added damage to check apartment information)
    expect(damageListPage.getMenuFromDamagePanel(0).isDisplayed()).toBe(true);
    damageListPage.getMenuFromDamagePanel(0).click();

    browser.wait(function() {
      return damageListPage.getContentOfMenuFromDamagePanel(0).isDisplayed();
    }).then(function() {
      expect(damageListPage.getDamageDetailsLinkFromDamagePanel(0).isDisplayed()).toBe(true);
      damageListPage.getDamageDetailsLinkFromDamagePanel(0).click();

      browser.wait(function() {
        return damageDetailsPage.getDamageDetailsElement().isDisplayed();
      }).then(function() {
        damageDetailsPage.getDamageAddress().getText().then(value => {
          let floor = FloorConverter.getFloor(existingDamage.apartmentFloor);
          let apartmentInformation = `${floor} floor, ${existingDamage.apartmentNumber}`;
          expect(value).toContain(apartmentInformation);
        });
      });
    });
  });
});
