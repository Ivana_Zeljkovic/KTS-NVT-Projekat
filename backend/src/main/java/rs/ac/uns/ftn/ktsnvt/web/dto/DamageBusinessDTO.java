package rs.ac.uns.ftn.ktsnvt.web.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Katarina Cukurov on 23/11/2017.
 */
public class DamageBusinessDTO implements Serializable {

    @NotNull
    @Size(min = 1)
    private List<Long> businessId;

    public DamageBusinessDTO() { }

    public List<Long> getBusinessId() {
        return businessId;
    }

    public void setBusinessId(List<Long> businessId) {
        this.businessId = businessId;
    }
}
