

import {by, element, protractor} from "protractor";

export class BusinessProfilePage{


    //Getters for input fields and validator elements
    getMap(){
      return element(by.id('map'));
    }

    getUsernameInput(){
      return element(by.id('username'));
    }

    getBusinessNameInput(){
      return element(by.id('name'));
    }

    getEmailInput(){
      return element(by.id('email'));
    }

    getPhoneNumberInput(){
      return element(by.id('phoneNumber'));
    }

    getBuisessTypeInput(){
      return element(by.id('businessType'));
    }

    getDescriptionInput(){
      return element(by.id('description'));
    }

    getStreetInput(){
      return element(by.id('street'));
    }

    getNumberInput(){
      return element(by.id('number'));
    }

    getCityInput(){
      return element(by.id('city'));
    }

    getPostalCodeInput(){
      return element(by.id('postalCode'));
    }

    getCurrentPasswordInput(){
      return element(by.id('currentPassword'));
    }

    getNewPasswordInput(){
      return element(by.id('password'));
    }

    getConfirmPasswordInput(){
      return element(by.id('confirmPassword'));
    }

    getDeactivateButton(){
      return element(by.css('.btn.btn-danger.account'));
    }

    getActivateButton(){
      return element(by.css('.btn.btn-success.account'));
    }

    getManageAccounts(){
      return element(by.css('.btn.btn-warning.account'));
    }

    getSaveButton(){
      return element(by.css('.btn.save'));
    }

    getResetButton(){
      return element(by.css('.btn.reset'));
    }


    //getters for validation errors
    getBusinessNameMinLengthError(){
      return element(by.id('nameMinimumLength'));
    }

    getBusinessNameRequired(){
      return element(by.id('nameRequired'));
    }

    getEmailRequired(){
      return element(by.id('emailRequired'));
    }

    getEmailPattern(){
      return element(by.id('emailPattern'));
    }

    getPhoneNumberRequired(){
      return element(by.id('phoneNumberRequired'))
    }

    getPhoneNumberContainsSpace(){
      return element(by.id('phoneNumberContainSpace'));
    }

    getPhoneNumberPattern(){
      return element(by.id('phoneNumberPattern'));
    }

    getDescriptionRequired(){
      return element(by.id('descriptionRequired'));
    }

    getDescriptionMinimumLength(){
      return element(by.id('descriptionLength'));
    }

    getStreetRequired(){
      return element(by.id('streetRequired'));
    }

    getStreetMinimumLength(){
      return element(by.id('streetLength'));
    }

    getNumberRequired(){
      return element(by.id('numberRequired'));
    }

    getCityRequired(){
      return element(by.id('cityRequired'));
    }

    getPostalCodeRequired(){
      return element(by.id('postalCodeRequired'));
    }

    getPostalCodePattern(){
      return element(by.id('postalCodePattern'));
    }

    getNewPasswordLength(){
      return element(by.id('newPasswordMinLength'))
    }

    getNewPasswordRequired(){
      return element(by.id('newPasswordRequired'))
    }

    getConfirmPasswordRequired(){
      return element(by.id('confirmPasswordRequired'))
    }

    getConfirmPasswordMatch(){
      return element(by.id('confirmPasswordNotMatch'));
    }

    getCurrentPasswordRequired(){
      return element(by.id('currentPasswordRequired'));
    }


    //Setters for input fields.
    setName(value : string){
      this.clearField(this.getBusinessNameInput());
      this.getBusinessNameInput().sendKeys(value);
    }

    setEmail(value : string){
      this.clearField(this.getEmailInput());
      this.getEmailInput().sendKeys(value);
    }

    setPhoneNumber(value : string){
      this.clearField(this.getPhoneNumberInput());
      this.getPhoneNumberInput().sendKeys(value);
    }

    setDescription(value : string){
      this.clearField(this.getDescriptionInput());
      this.getDescriptionInput().sendKeys(value);
    }

    setStreet(value : string){
      this.clearField(this.getStreetInput());
      this.getStreetInput().sendKeys(value);
    }

    setNumber(value : string){
      this.clearField(this.getNumberInput());
      this.getNumberInput().sendKeys(value);
    }

    setCity(value : string){
      this.clearField(this.getCityInput());
      this.getCityInput().sendKeys(value);
    }

    setPostalCode(value : string){
      this.clearField(this.getPostalCodeInput());
      this.getPostalCodeInput().sendKeys(value);
    }

    setCurrentPassword(value : string){
      this.getCurrentPasswordInput().sendKeys(value);
    }

    setNewPassword(value : string){
      this.getNewPasswordInput().sendKeys(value);
    }

    setConfirmPassword(value : string){
      this.getConfirmPasswordInput().sendKeys(value);
    }



    clearField(field) {
      field.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, "a"));
      field.sendKeys(protractor.Key.BACK_SPACE);
      field.clear();
    }
}
