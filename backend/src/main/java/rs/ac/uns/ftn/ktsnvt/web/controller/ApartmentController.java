package rs.ac.uns.ftn.ktsnvt.web.controller;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.*;
import rs.ac.uns.ftn.ktsnvt.service.*;
import rs.ac.uns.ftn.ktsnvt.web.controller.util.MessageConstants;
import rs.ac.uns.ftn.ktsnvt.web.dto.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

import static rs.ac.uns.ftn.ktsnvt.web.util.ConverterDTOToModel.convertApartmentCreateDTOToApartment;


/**
 * Created by Nikola Garabandic on 17/11/2017.
 */
@RestController
@RequestMapping(value = "/api")
@Api(value = "apartments", description = "Operations pertaining to apartments in application.")
public class ApartmentController {

    @Autowired
    private ApartmentService apartmentService;

    @Autowired
    private BuildingService buildingService;

    @Autowired
    private CityService cityService;

    @Autowired
    private AddressService addressService;

    @Autowired
    private TenantService tenantService;



    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a list of all apartments in page form.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of apartments", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to view these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    })
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity getAllApartments(
            @ApiParam(value = "The page number for getting apartments.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting apartments.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting apartments.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "The sort property for getting apartments.", required = true) @RequestParam String sortProperty) {

        Page<Apartment> apartments = this.apartmentService.findAllPageable(Integer.valueOf(pageNumber),
                Integer.valueOf(pageSize), sortDirection, sortProperty);
        List<ApartmentDTO> apartmentDTOS = new ArrayList<>();

        apartments.getContent().forEach(apartment -> apartmentDTOS.add(new ApartmentDTO(apartment, apartments.getTotalElements())));

        return new ResponseEntity<>(apartmentDTOS, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/apartments",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a list of all apartments in some building in page form.",
            notes = "You have to provide valid building ID in the URL.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of apartments", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to view these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('TENANT')")
    public ResponseEntity getApartments(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The page number for getting apartments.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting apartments.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting apartments.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "First sort property for getting apartments.", required = true) @RequestParam String sortProperty,
            @ApiParam(value = "Second sort property for getting apartments.", required = true) @RequestParam String sortPropertySecond) {

        Building building = this.buildingService.findOne(buildingId);

        for (GrantedAuthority authority : SecurityContextHolder.getContext().getAuthentication().getAuthorities()) {
            if (authority.getAuthority().equals(MessageConstants.TENANT_ROLE))
                this.tenantService.checkPermissionForCurrentTenantInBuilding(building.getId());
        }

        List<ApartmentDTO> apartmentDTOS = new ArrayList<>();

        Page<Apartment> apartments = this.apartmentService.getAllFromBuilding(building.getId(), Integer.valueOf(pageNumber),
                Integer.valueOf(pageSize), sortDirection, sortProperty, sortPropertySecond);

        apartments.getContent().forEach(apartment -> apartmentDTOS.add(new ApartmentDTO(apartment,
                apartments.getTotalElements())));

        return new ResponseEntity<>(apartmentDTOS, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/apartments/{apartmentId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a single apartment in some building.",
            notes = "You have to provide valid ID apartment in the URL.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved apartment", response = ApartmentDTO.class),
            @ApiResponse(code = 400, message = "Selected apartment doesn't belong to selected building"),
            @ApiResponse(code = 401, message = "You are not authorized to view these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity getApartment(
            @ApiParam(value = "The ID of the existing apartment resource.", required = true) @PathVariable("apartmentId") Long apartmentId) {

        Apartment apartment = this.apartmentService.findOne(apartmentId);

        if (apartment == null)
            throw new BadRequestException("Apartment with this id does not exist!");
        return new ResponseEntity<>(new ApartmentDTO(apartment), HttpStatus.OK);
    }



    @RequestMapping(
            value = "/apartments/free_apartments",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a list of all free apartments in page form.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of free apartments", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to view these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")})
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity getFreeApartments(
            @ApiParam(value = "The page number for getting apartments.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting apartments.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting apartments.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "The sort property for getting apartments.", required = true) @RequestParam String sortProperty) {

        Page<Apartment> apartments = this.apartmentService.findFreeApartments(Integer.valueOf(pageNumber),
                Integer.valueOf(pageSize), sortDirection, sortProperty);
        List<ApartmentDTO> apartmentDTOS = new ArrayList<>();

        apartments.getContent().forEach(apartment -> apartmentDTOS.add(new ApartmentDTO(apartment, apartments.getTotalElements())));

        return new ResponseEntity<>(apartmentDTOS, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/apartments/free_apartments_building",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a list of free apartments in building with given address in page form.",
            notes = "You have to provide a valid address parameter.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of free apartments", response = List.class),
            @ApiResponse(code = 400, message = "Some of required parameters in URL is missing"),
            @ApiResponse(code = 401, message = "You are not authorized to view these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity getFreeApartmentsByBuilding(
            @ApiParam(value = "The street name of the building.", required = true) @RequestParam("street") String street,
            @ApiParam(value = "The street number of the building.", required = true) @RequestParam("number") Integer number,
            @ApiParam(value = "The city name.", required = true) @RequestParam("city_name") String cityName,
            @ApiParam(value = "The city postal number.", required = true) @RequestParam("postal_number") Integer postalNumber,
            @ApiParam(value = "The page number for getting apartments.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting apartments.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting apartments.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "The sort property for getting apartments.", required = true) @RequestParam String sortProperty) {

        if (street.isEmpty() || number == null || cityName.isEmpty() || postalNumber == null)
            throw new BadRequestException("All parameters are required!");

        street = street.replace("%20", " ");
        cityName = cityName.replace("%20", " ");

        Building building = findBuildingFromDTO(new AddressDTO(street, number, new CityDTO(cityName, postalNumber)));

        Page<Apartment> apartments = this.apartmentService.findFreeApartmentsInBuilding(building.getId(), Integer.valueOf(pageNumber),
                Integer.valueOf(pageSize), sortDirection, sortProperty);
        List<ApartmentDTO> apartmentDTOS = new ArrayList<>();

        apartments.getContent().forEach(apartment -> apartmentDTOS.add(new ApartmentDTO(apartment, apartments.getTotalElements())));

        return new ResponseEntity<>(apartmentDTOS, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/apartments/free_apartments_size",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a list of all free apartments in building with surface in the given range in page form.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of free apartments", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to view these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    })
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity getFreeApartmentsBySize(
            @ApiParam(value = "Minimum apartment size", required = true) @RequestParam Integer min,
            @ApiParam(value = "Maximum apartment size", required = true) @RequestParam Integer max,
            @ApiParam(value = "The ID of the existing building resource.", required = true) @RequestParam Long buildingId,
            @ApiParam(value = "The page number for getting apartments.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting apartments.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting apartments.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "The sort property for getting apartments.", required = true) @RequestParam String sortProperty) {

        if (min == null || max == null)
            throw new BadRequestException("All parameters are required!");

        Page<Apartment> apartments = this.apartmentService.findFreeApartmentsBySize(min, max, buildingId, Integer.valueOf(pageNumber),
                Integer.valueOf(pageSize), sortDirection, sortProperty);
        List<ApartmentDTO> apartmentDTOS = new ArrayList<>();

        apartments.getContent().forEach(apartment -> apartmentDTOS.add(new ApartmentDTO(apartment, apartments.getTotalElements())));

        return new ResponseEntity<>(apartmentDTOS, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/apartments/{apartmentId}/make_tenant_owner",
            method = RequestMethod.PATCH,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Make tenant to be an owner of apartment.",
            notes = "You have to provide object with valid tenant ID and valid IDs for building and apartment in the URL. " +
                    "Method returns updated tenant-apartment object.",
            httpMethod = "PATCH",
            produces = "application/json",
            consumes = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully made tenant to be an owner of apartment", response = ApartmentDTO.class),
            @ApiResponse(code = 400, message = "Inappropriate object sent in request body or invalid IDs sent in URL"),
            @ApiResponse(code = 401, message = "You are not authorized to make this changes"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity makeTenantOwner(
            @ApiParam(value = "The ID of the existing apartment resource.", required = true) @PathVariable("apartmentId") Long apartmentId,
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The object with ID of tenant (owner) resource.", required = true) @Valid @RequestBody TenantInApartmentDTO tenantInApartmentDTO,
            @ApiParam(value = "The object that contains all errors from validation of DTO object") BindingResult errors) {

        Building building = this.buildingService.findOne(buildingId);
        Tenant tenant = this.tenantService.findOne(tenantInApartmentDTO.getId());
        Apartment apartment = this.apartmentService.findOne(apartmentId);

        this.apartmentService.checkBuilding(apartment, building.getId());

        tenant.getPersonalApartments().add(apartment);
        apartment.setOwner(tenant);

        this.apartmentService.save(apartment);

        return new ResponseEntity<>(new ApartmentDTO(apartment), HttpStatus.OK);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/apartments/{apartmentId}/connect_tenant_and_apartment",
            method = RequestMethod.PATCH,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Set apartment in which tenant lives.",
            notes = "You have to provide object with valid tenant ID and valid IDs for building and apartment in the URL. " +
                    "Method returns updated tenant-apartment object.",
            httpMethod = "PATCH",
            produces = "application/json",
            consumes = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully connected tenant and apartment", response = ApartmentDTO.class),
            @ApiResponse(code = 400, message = "Inappropriate object sent in request body or invalid IDs sent in URL"),
            @ApiResponse(code = 401, message = "You are not authorized to make this changes"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity connectTenantAndApartment(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The ID of the existing apartment resource.", required = true) @PathVariable("apartmentId") Long apartmentId,
            @ApiParam(value = "The object with ID of tenant (owner) resource.", required = true) @RequestBody @Valid TenantInApartmentDTO tenantInApartmentDTO,
            @ApiParam(value = "The object that contains all errors from validation of DTO object") BindingResult errors) {

        Building building = this.buildingService.findOne(buildingId);
        Tenant tenant = this.tenantService.findOne(tenantInApartmentDTO.getId());
        Apartment apartment = this.apartmentService.findOne(apartmentId);

        this.apartmentService.checkBuilding(apartment, building.getId());

        tenant.setApartmentLiveIn(apartment);
        if (apartment.getTenants().isEmpty()) {
            // ukoliko je stanar kog trenutno povezujemo sa nekim stanom ujedno i jedini stanar tog stana,
            // njega postavljamo kao clana skupstine stanara u posmatranoj zgradi za taj konkretan stan
            tenant.setCouncil(building.getCouncil());
            building.getCouncil().getMembers().add(tenant);
        }
        apartment.getTenants().add(tenant);
        this.tenantService.save(tenant);

        return new ResponseEntity<>(new ApartmentDTO(apartment), HttpStatus.OK);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/apartments/{apartmentId}/tenants/{tenantId}/remove_tenant_from_apartment",
            method = RequestMethod.PATCH,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Remove tenant from apartment.",
            notes = "You have to provide valid IDs for building, apartment and tenant in the URL. " +
                    "Method returns updated tenant-apartment object.",
            httpMethod = "PATCH",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully removed tenant from the apartment", response = ApartmentDTO.class),
            @ApiResponse(code = 400, message = "Invalid IDs sent in URL"),
            @ApiResponse(code = 401, message = "You are not authorized to make this changes"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity removeTenantFromApartment(
            @ApiParam(value = "The ID of the existing tenant resource.", required = true) @PathVariable("tenantId") Long tenantId,
            @ApiParam(value = "The ID of the existing apartment resource.", required = true) @PathVariable("apartmentId") Long apartmentId,
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId) {

        Building building = this.buildingService.findOne(buildingId);
        Tenant tenant = this.tenantService.findOne(tenantId);
        Apartment apartment = this.apartmentService.findOne(apartmentId);

        this.apartmentService.checkBuilding(apartment, building.getId());

        if (tenant.getApartmentLiveIn() == null)
            throw new BadRequestException("Selected tenant is not connected with any apartment!");

        if (!tenant.getApartmentLiveIn().getId().equals(apartment.getId()))
            throw new BadRequestException("Selected tenant doesn't live in selected apartment!");

        tenant.setApartmentLiveIn(null);
        apartment.getTenants().remove(tenant);

        this.tenantService.save(tenant);

        return new ResponseEntity<>(new ApartmentDTO(apartment), HttpStatus.OK);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/apartments/{apartmentId}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Delete a single apartment in some building.",
            notes = "You have to provide valid IDs for building and apartment in the URL. " +
                    "Once deleted the resource can not be recovered.",
            httpMethod = "DELETE",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully deleted apartment", response = ApartmentDTO.class),
            @ApiResponse(code = 400, message = "Selected apartment doesn't belong to selected building"),
            @ApiResponse(code = 401, message = "You are not authorized to delete these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity deleteApartment(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The ID of the existing apartment resource.", required = true) @PathVariable("apartmentId") Long apartmentId) {

        Building building = this.buildingService.findOne(buildingId);
        Apartment apartment = this.apartmentService.findOne(apartmentId);

        if (building == null)
            throw new BadRequestException("Building with this id does not exist!");

        if (apartment == null)
            throw new BadRequestException("Apartment with this id does not exist!");

        if (!apartment.getBuilding().getId().equals(building.getId()))
            throw new BadRequestException("Selected apartment doesn't belong to selected building!");

        if (!apartment.getTenants().isEmpty())
            throw new BadRequestException("Apartment with ID: " + apartment.getId() + " cannot be deleted because it has tenants.");

        this.apartmentService.remove(apartmentId);

        return new ResponseEntity<>(HttpStatus.OK);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/apartments",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Create new apartment resource.",
            notes = "You have to provide valid building ID in the URL",
            httpMethod = "POST",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully created new apartment", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to create these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity createApartment(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The apartment object", required = true) @Valid @RequestBody ApartmentCreateDTO apartmentCreateDTO) {

        Building building = this.buildingService.findOne(buildingId);

        if (building == null)
            throw new BadRequestException("Building with this id does not exist!");

        Apartment apartment = convertApartmentCreateDTOToApartment(apartmentCreateDTO);

        if (apartment.getFloor() > building.getNumberOfFloors())
            throw new BadRequestException("Apartment can't have greater floor than building!");

        for (Apartment ap : building.getApartments()) {
            if (ap.getNumber() == apartment.getNumber())
                throw new BadRequestException("There is already apartment with that number!");
        }

        apartment.setBuilding(building);
        this.apartmentService.save(apartment);

        return new ResponseEntity<>(new ApartmentDTO(apartment), HttpStatus.OK);
    }



    public Address findAddressFromDTO(AddressDTO addressDTO) {
        City city = this.cityService.findByNameAndPostalNumber(addressDTO.getCity().getName(), addressDTO.getCity().getPostalNumber());
        return this.addressService.findByCityStreetNumber(city.getId(), addressDTO.getStreet(), addressDTO.getNumber());
    }

    public Building findBuildingFromDTO(AddressDTO addressDTO) {
        Address address = findAddressFromDTO(addressDTO);
        if (address == null) throw new NotFoundException("Address not found!");
        return this.buildingService.findByAddressId(address.getId());
    }

}
