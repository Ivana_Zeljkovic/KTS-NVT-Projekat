package rs.ac.uns.ftn.ktsnvt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import rs.ac.uns.ftn.ktsnvt.model.entity.Address;

/**
 * Created by Katarina Cukurov on 07/11/2017.
 */
public interface AddressRepository extends JpaRepository<Address, Long> {

    @Query("SELECT a FROM Address a WHERE city_id = :city_id AND street = :street AND number = :number")
    Address findByCityStreetNumber(@Param("city_id") Long cityId, @Param("street") String street, @Param("number") int number);

}
