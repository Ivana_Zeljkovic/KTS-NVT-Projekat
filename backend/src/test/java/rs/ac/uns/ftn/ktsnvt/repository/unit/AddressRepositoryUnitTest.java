package rs.ac.uns.ftn.ktsnvt.repository.unit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.model.entity.Address;
import rs.ac.uns.ftn.ktsnvt.model.entity.City;
import rs.ac.uns.ftn.ktsnvt.repository.AddressRepository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Katarina Cukurov on 28/11/2017.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class AddressRepositoryUnitTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private AddressRepository addressRepository;

    private Long city_id;


    @Before
    public void setUp(){
        // CITY
        City city = new City("Novi Sad", 21000);
        entityManager.persist(city);
        this.city_id = (Long)entityManager.persistAndGetId(city);

        // ADDRESS
        Address address = new Address("Janka Veselinovica", 10, city);
        entityManager.persist(address);
    }

    @Test
    public void testFindByCityStreetNumber() {
        /*
         * Test proverava ispravnost rada metode findByCityStreetNumber repozitorijuma AddressRepository
         * (pronalazenje adrese na osnovu zadatog naziva grada, naziva ulice i broja)
         * */

        Address address = this.addressRepository.findByCityStreetNumber(this.city_id, "Janka Veselinovica", 10);

        assertNotNull(address);
        assertEquals("Janka Veselinovica", address.getStreet());
        assertEquals("Novi Sad", address.getCity().getName());
        assertEquals(10, address.getNumber());
    }
}
