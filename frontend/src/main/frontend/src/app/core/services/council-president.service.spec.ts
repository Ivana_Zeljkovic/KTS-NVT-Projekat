import { TestBed, inject } from '@angular/core/testing';

import { CouncilPresidentService } from './council-president.service';

describe('CouncilPresidentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CouncilPresidentService]
    });
  });

  it('should be created', inject([CouncilPresidentService], (service: CouncilPresidentService) => {
    expect(service).toBeTruthy();
  }));
});
