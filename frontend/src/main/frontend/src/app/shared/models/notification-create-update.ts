export class NotificationCreateUpdate {
  constructor(public title: string,
              public content: string) { }
}
