import {by, element, ElementArrayFinder, ElementFinder} from "protractor";

export class DamageTypesModalPage {
  // get methods
  getDamageTypesModalElement(): ElementFinder {
    return element(by.tagName('app-damage-types-modal'));
  }

  getTitle(): ElementFinder {
    return element(by.css('.modal-title'));
  }

  getCloseButton(): ElementFinder {
    return element(by.css('div.modal-header > button'));
  }

  getResponsibilityTable(): ElementFinder {
    return element(by.css('div.modal-body table'));
  }

  getResponsibilityTableRows(): ElementArrayFinder {
    return element.all(by.css('div.modal-body table > tbody > tr'));
  }

  getDamageTypeName(index: number): ElementFinder {
    return element(by.css(`div.modal-body table > tbody > tr:nth-child(${index+1}) > td:nth-child(1)`));
  }

  getDamageResponsibleTenant(index: number): ElementFinder {
    return element(by.css(`div.modal-body table > tbody > tr:nth-child(${index+1}) > td:nth-child(2)`));
  }

  getDamageResponsibleInstitution(index: number): ElementFinder {
    return element(by.css(`div.modal-body table > tbody > tr:nth-child(${index+1}) > td:nth-child(3)`));
  }

  getLinkForTenant(index: number): ElementFinder {
    return element(by.css(`div.modal-body table > tbody > tr:nth-child(${index+1}) > td:nth-child(2) > a`));
  }

  getLinkForInstitution(index: number): ElementFinder {
    return element(by.css(`div.modal-body table > tbody > tr:nth-child(${index+1}) > td:nth-child(3) > a`));
  }
}
