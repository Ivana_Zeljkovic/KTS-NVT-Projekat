package rs.ac.uns.ftn.ktsnvt.web.controller;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.ForbiddenException;
import rs.ac.uns.ftn.ktsnvt.model.entity.*;
import rs.ac.uns.ftn.ktsnvt.service.*;
import rs.ac.uns.ftn.ktsnvt.web.controller.util.MessageConstants;
import rs.ac.uns.ftn.ktsnvt.web.dto.QuestionnaireResultsDTO;
import rs.ac.uns.ftn.ktsnvt.web.dto.VoteCreateDTO;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Ivana Zeljkovic on 18/11/2017.
 */
@RestController
@RequestMapping(value = "/api")
@Api(value="questionnaires", description="Operations pertaining to questionnaires in application.")
public class QuestionnaireController {

    @Autowired
    private BuildingService buildingService;

    @Autowired
    private QuestionnaireService questionnaireService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private TenantService tenantService;

    @Autowired
    private VoteService voteService;

    @Autowired
    private ApartmentService apartmentService;

    @Autowired
    private QuestionService questionService;



    @RequestMapping(
            value = "/buildings/{buildingId}/questionnaires/{questionnaireId}/results",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a single questionnaire in some building.",
            notes = "You have to provide valid IDs for building and questionnaire in the URL.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved questionnaire",
                    response = QuestionnaireResultsDTO.class),
            @ApiResponse(code = 400, message = "Selected questionnaire doesn't belong to selected building"),
            @ApiResponse(code = 401, message = "You are not authorized to view these resources"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity getQuestionnaireResult(
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The ID of the existing questionnaire resource.", required = true) @PathVariable("questionnaireId") Long questionnaireId){

        Building building = this.buildingService.findOne(buildingId);
        Questionnaire questionnaire = this.questionnaireService.findOne(questionnaireId);

        // provera da li izabrana anketa pripada izabranoj zgradi
        this.questionnaireService.checkBuildingForQuestionnaire(questionnaire.getId(), building.getBillboard().getId());

        // provera da li je tacka dnevnog reda koja sadrzi anketu povezana sa nekim sastankom
        // - ukoliko nije, nije dovoljeno dobavljanje rezultata ankete
        if(questionnaire.getMeetingItem().getMeeting() == null)
            throw new BadRequestException("The selected questionnaire has not been processed yet!");

        // provera da li je isteklo vreme glasanja na anketu ciji se rezultati zahtevaju
        this.questionnaireService.checkQuestionnaireExpired(questionnaire);

        Tenant currentTenant = this.accountService.findByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()).getTenant();

        /*
        dobavljanje rezultata ankete treba omoguciti predsedniku skupstine stanara i osobama koje su clanovi skupstine u
        posmatranoj zgradi
        */
        if(currentTenant.getCouncil() == null)
            throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);

        return new ResponseEntity<>(new QuestionnaireResultsDTO(questionnaire), HttpStatus.OK);
    }



    @RequestMapping(
            value = "/buildings/{buildingId}/questionnaires/{questionnaireId}/vote",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Create a vote resource for selected questionnaire in selected building.",
            notes = "You have to provide valid IDs for building and questionnaire in the URL.",
            httpMethod = "POST",
            consumes = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully created vote"),
            @ApiResponse(code = 400, message = "Inappropriate vote object sent in request body " +
                    "or building's/questionnaire's ID in URL."),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 403, message = "You don't have permission to create resource"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('TENANT')")
    public ResponseEntity vote(
            @ApiParam(value = "The ID of the existing questionnaire resource.", required = true) @PathVariable("questionnaireId") Long questionnaireId,
            @ApiParam(value = "The ID of the existing building resource.", required = true) @PathVariable("buildingId") Long buildingId,
            @ApiParam(value = "The vote object.", required = true) @Valid @RequestBody VoteCreateDTO voteCreateDTO,
            @ApiParam(value = "The object that contains all errors from validation of DTO object") BindingResult errors) {

        Building building = this.buildingService.findOne(buildingId);
        Questionnaire questionnaire = this.questionnaireService.findOne(questionnaireId);

        // provera da li izabrana anketa pripada izabranoj zgradi
        this.questionnaireService.checkBuildingForQuestionnaire(questionnaire.getId(), building.getBillboard().getId());

        // provera da li je tacka dnevnog reda koja sadrzi anketu povezana sa nekim sastankom
        // - ukoliko nije, nije dovoljeno glasanje jos uvek
        // - ukoliko jeste, proverava se da li je isteklo vreme za glasanje
        this.questionnaireService.isVoteEnabled(questionnaire);

        Tenant currentTenant = this.accountService.findByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()).getTenant();
        boolean buildingFound = this.tenantService.checkTenantsPersonalApartments(currentTenant, buildingId);

        if(!buildingFound) throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);

        int numberOfPersonalVotes = this.voteService.countPersonalVotes(currentTenant.getId(), questionnaireId);
        int numberOfApartmentsInBuilding = this.apartmentService.countApartmentsInBuilding(
                currentTenant.getId(), building.getId());

        if(numberOfPersonalVotes == numberOfApartmentsInBuilding)
            throw new BadRequestException("You can't vote on selected questionnaire again!");

        Map<Long, String> questionsIdAnswer = new HashMap<>();
        voteCreateDTO.getQuestionVotes().forEach(voteQuestionCreateDTO ->
                questionsIdAnswer.put(voteQuestionCreateDTO.getQuestionId(), voteQuestionCreateDTO.getAnswer()));
        this.questionService.checkQuestions(questionsIdAnswer, questionnaireId);

        Vote vote = new Vote();
        vote.setTenant(currentTenant);
        vote.setQuestionnaire(questionnaire);

        List<QuestionVote> questionVotes = new ArrayList<>();
        voteCreateDTO.getQuestionVotes().forEach(voteQuestionCreateDTO -> {
            Question question = questionnaire.findQuestionById(voteQuestionCreateDTO.getQuestionId());
            String answer = question.getAnswers().get(question.getAnswers().indexOf(voteQuestionCreateDTO.getAnswer()));
            QuestionVote questionVote = new QuestionVote(question, answer);
            questionVote.setVote(vote);
            questionVotes.add(questionVote);
        });
        vote.setQuestionsVotes(questionVotes);
        vote.getQuestionnaire().getVotes().add(vote);

        this.voteService.save(vote);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
