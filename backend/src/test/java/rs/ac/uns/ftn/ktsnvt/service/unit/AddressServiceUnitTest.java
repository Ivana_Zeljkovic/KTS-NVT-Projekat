package rs.ac.uns.ftn.ktsnvt.service.unit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Address;
import rs.ac.uns.ftn.ktsnvt.model.entity.City;
import rs.ac.uns.ftn.ktsnvt.repository.AddressRepository;
import rs.ac.uns.ftn.ktsnvt.service.AddressService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.BDDMockito.given;

/**
 * Created by Nikola Garabandic on 30/11/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AddressServiceUnitTest {

    @Autowired
    AddressService addressService;

    @MockBean
    AddressRepository addressRepository;


    @Before
    public void setUp(){
        City city = new City("Novi Sad", 21000);
        Address address = new Address("Sentandrejski put", 75, city);

        given(
                this.addressRepository.findOne(100L)
        ).willReturn(
                address
        );

        given(
                this.addressRepository.findOne(200L)
        ).willReturn(
                null
        );
    }

    @Test
    public void testFindOne(){
        /*
         * Test proverava ispravnost rada metode findOne servisa AddressService
         * (dobavljanje address objekta na osnovu ID-a)
         * u slucaju kada postoji address sa zadatim Id-em
         * */

        /*
        adresa sa Id-em 100 ima podatke:
            street = Novi Sad,
            number = 21000,
            City: Name = Novi Sad postalNumber = 21000
         */
        Address address = this.addressService.findOne(100L);

        assertNotNull(address);
        assertEquals(75, address.getNumber());
        assertEquals("Sentandrejski put", address.getStreet());
        assertEquals("Novi Sad", address.getCity().getName());
        assertEquals(21000, address.getCity().getPostalNumber());
    }

    @Test(expected = NotFoundException.class)
    public void testFindOneNoneExist() {
        /*
         * Test proverava ispravnost rada metode findOne servisa AddressService
         * (dobavljanje address objekta na osnovu ID-a)
         * u slucaju kada ne postoji adresa sa zadatim ID-em - ocekivano NotFoundException
         * */

        // Adresa sa id-em 200 ne postoji
        Address address = this.addressService.findOne(200L);
    }
}
