package rs.ac.uns.ftn.ktsnvt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.ktsnvt.model.entity.Council;

/**
 * Created by Ivana Zeljkovic on 16/11/2017.
 */
public interface CouncilRepository extends JpaRepository<Council, Long>{

}
