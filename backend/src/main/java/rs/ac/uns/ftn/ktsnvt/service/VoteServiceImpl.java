package rs.ac.uns.ftn.ktsnvt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.model.entity.Vote;
import rs.ac.uns.ftn.ktsnvt.repository.VoteRepository;

/**
 * Created by Ivana Zeljkovic on 19/11/2017.
 */
@Service
public class VoteServiceImpl implements VoteService {

    @Autowired
    private VoteRepository voteRepository;


    @Override
    @Transactional(readOnly = true)
    public int countPersonalVotes(Long tenantId, Long questionnaireId) {
        return this.voteRepository.countPersonalVotes(tenantId, questionnaireId);
    }

    @Override
    @Transactional(readOnly = false)
    public Vote save(Vote vote) {
        return this.voteRepository.save(vote);
    }
}
