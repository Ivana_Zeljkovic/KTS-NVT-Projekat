import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
import { Injectable } from "@angular/core";
import { JwtService } from "../services/jwt.service";

@Injectable()
export class CouncilMemberGuard implements CanActivate {

  constructor(private router: Router, private jwtService: JwtService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if(this.jwtService.hasRole('TENANT') && this.jwtService.getApartmentIdFromToken() !== -1
      && this.jwtService.getIsCouncilMemberFromToken()) return true;

    // logged user hasn't role TENANT or he isn't connected to any apartment already or isn't member of council in his building
    this.router.navigate(['home']);
    return false;
  }
}
