package rs.ac.uns.ftn.ktsnvt.web.controller;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.ktsnvt.exception.ConflictException;
import rs.ac.uns.ftn.ktsnvt.exception.ForbiddenException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Business;
import rs.ac.uns.ftn.ktsnvt.model.entity.Damage;
import rs.ac.uns.ftn.ktsnvt.model.entity.Offer;
import rs.ac.uns.ftn.ktsnvt.service.BusinessService;
import rs.ac.uns.ftn.ktsnvt.service.DamageService;
import rs.ac.uns.ftn.ktsnvt.service.EmailService;
import rs.ac.uns.ftn.ktsnvt.service.OfferService;
import rs.ac.uns.ftn.ktsnvt.web.controller.util.MessageConstants;
import rs.ac.uns.ftn.ktsnvt.web.dto.OfferCreateDTO;
import rs.ac.uns.ftn.ktsnvt.web.dto.OfferDTO;

import javax.validation.Valid;


/**
 * Created by Ivana Zeljkovic on 06/11/2017.
 */
@RestController
@RequestMapping(value = "/api")
@Api(value = "offers", description = "Operations pertaining to offers in application.")
public class OfferController {

    @Autowired
    private DamageService damageService;

    @Autowired
    private BusinessService businessService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private OfferService offerService;



    @RequestMapping(
            value = "/businesses/{businessId}/damages/{damageId}/send_offer",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Create an offer resource.",
            notes = "You have to provide valid IDs for firm/institution and damage in the URL. " +
                    "Method returns the offer being saved.",
            httpMethod = "POST",
            produces = "application/json",
            consumes = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully created offer", response = OfferDTO.class),
            @ApiResponse(code = 400, message = "Inappropriate offer object sent in request body"),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 403, message = "You don't have permission to create resource"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('COMPANY') or hasAuthority('INSTITUTION')")
    public ResponseEntity sendOfferForDamage(
            @ApiParam(value = "The ID of the existing institution/firm resource.", required = true) @PathVariable("businessId") Long businessId,
            @ApiParam(value = "The ID of the existing damage resource.", required = true) @PathVariable("damageId") Long damageId,
            @ApiParam(value = "The offer object.", required = true) @RequestBody @Valid OfferCreateDTO offerCreateDTO,
            @ApiParam(value = "The object that contains all errors from validation of DTO object") BindingResult errors) {

        Damage damage = this.damageService.findOne(damageId);
        Business business = this.businessService.findOne(businessId);

        this.businessService.checkPermissionForCurrentBusiness(businessId);

        if(!damage.getBusinessesDamageSentToIds().contains(businessId))
            throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);

        Offer offer = new Offer();
        offer.setPrice(offerCreateDTO.getPrice());
        offer.setBusiness(business);
        damage.getOffers().add(offer);
        offer.setDamage(damage);

        try {
            this.emailService.sendEmailOfferForDamage(offer, damage, damage.getResponsiblePerson());
        } catch (InterruptedException e) {
            throw new ConflictException("Problem in sending e-mail: " + e.getMessage());
        }

        this.offerService.save(offer);
        this.damageService.save(damage);

        return new ResponseEntity<>(new OfferDTO(offer), HttpStatus.CREATED);
    }



    @RequestMapping(
            value = "/businesses/{businessId}/damages/{damageId}/accepted_offer",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a single offer.",
            notes = "You have to provide valid IDs for firm/institution and damage in the URL. " +
                    "Method returns the offer which satisfies both IDs.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully created offer", response = OfferDTO.class),
            @ApiResponse(code = 400, message = "Inappropriate offer object sent in request body"),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 403, message = "You don't have permission to create resource"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('COMPANY') or hasAuthority('INSTITUTION')")
    public ResponseEntity getOfferForDamage(
            @ApiParam(value = "The ID of the existing institution/firm resource.", required = true) @PathVariable("businessId") Long businessId,
            @ApiParam(value = "The ID of the existing damage resource.", required = true) @PathVariable("damageId") Long damageId) {

        this.businessService.checkPermissionForCurrentBusiness(businessId);

        Offer offer = this.offerService.getOfferByDamageAndBusiness(damageId, businessId);

        return new ResponseEntity<>(new OfferDTO(offer), HttpStatus.OK);
    }
}
