package rs.ac.uns.ftn.ktsnvt.model.entity;

import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@Table(name = "damage_responsibility")
@Where(clause="deleted=0")
public class DamageResponsibility {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @ManyToOne
    private Tenant responsibleTenant;

    @ManyToOne
    private Business responsibleInstitution;

    @ManyToOne
    private DamageType damageType;

    @OneToOne
    private Building building;

    @Column(nullable = false, columnDefinition = "INTEGER DEFAULT 0")
    @Version
    private int version;

    @Column(nullable = false, columnDefinition = "BOOL DEFAULT FALSE")
    private boolean deleted;

    public DamageResponsibility() { }

    public DamageResponsibility(Tenant responsibleTenant, Business responsibleInstitution, DamageType damageType, Building building) {
        this.responsibleTenant = responsibleTenant;
        this.responsibleInstitution = responsibleInstitution;
        this.damageType = damageType;
        this.building = building;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Tenant getResponsibleTenant() {
        return responsibleTenant;
    }

    public void setResponsibleTenant(Tenant responsibleTenant) {
        this.responsibleTenant = responsibleTenant;
    }

    public Business getResponsibleInstitution() { return responsibleInstitution; }

    public void setResponsibleInstitution(Business responsibleInstitution) { this.responsibleInstitution = responsibleInstitution; }

    public DamageType getDamageType() {
        return damageType;
    }

    public void setDamageType(DamageType damageType) {
        this.damageType = damageType;
    }

    public Building getBuilding() { return building; }

    public void setBuilding(Building building) { this.building = building; }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
