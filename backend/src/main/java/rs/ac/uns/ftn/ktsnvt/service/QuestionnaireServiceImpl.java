package rs.ac.uns.ftn.ktsnvt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.ForbiddenException;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Meeting;
import rs.ac.uns.ftn.ktsnvt.model.entity.Questionnaire;
import rs.ac.uns.ftn.ktsnvt.repository.QuestionnaireRepository;

import java.util.Calendar;

/**
 * Created by Ivana Zeljkovic on 13/11/2017.
 */
@Service
public class QuestionnaireServiceImpl implements QuestionnaireService {

    @Autowired
    private QuestionnaireRepository questionnaireRepository;


    @Override
    @Transactional(readOnly = false)
    public Questionnaire save(Questionnaire questionnaire) {
        return this.questionnaireRepository.save(questionnaire);
    }

    @Override
    @Transactional(readOnly = true)
    public Questionnaire findOne(Long id) {
        Questionnaire questionnaire = this.questionnaireRepository.findOne(id);
        if(questionnaire == null) throw new NotFoundException("Questionnaire not found!");
        return questionnaire;
    }

    @Override
    public void checkQuestionnaireExpired(Questionnaire questionnaire) {
        if(questionnaire.getDateExpire() == null)
            throw new BadRequestException("The questionnaire hasn't expired yet!");

        Calendar cal = Calendar.getInstance();
        cal.setTime(questionnaire.getDateExpire());

        //Ako upitnik jos nije istekao
        if(Calendar.getInstance().getTime().before(cal.getTime()))
            throw new BadRequestException("The questionnaire hasn't expired yet!");
    }

    @Override
    @Transactional(readOnly = false)
    public void remove(Long questionnaireId) {
        this.questionnaireRepository.delete(questionnaireId);
    }

    @Override
    @Transactional(readOnly = true)
    public void isVoteEnabled(Questionnaire questionnaire) {
        if(questionnaire.getMeetingItem() != null && questionnaire.getMeetingItem().getMeeting() == null) {
            // glasanje se zabranjuje tacka dnevnog reda koja sadrzi anketu jos uvek nije povezana
            // ni sa jednim sastankom
            throw new ForbiddenException("You don't have permission for this action!" +
                    " Questionnaire isn't connected with any meeting!");
        }
        else if(questionnaire.getMeetingItem().getMeeting() != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(questionnaire.getDateExpire());

            //Ako upitnik jos nije istekao
            if(Calendar.getInstance().getTime().after(cal.getTime()))
                throw new BadRequestException("The questionnaire has already expired!");
        }
    }

    @Override
    @Transactional(readOnly = true)
    public void checkBuildingForQuestionnaire(Long questionnaireId, Long billboardId) {
        Questionnaire questionnaire = this.questionnaireRepository.findOneByIdAndBillboardId(questionnaireId, billboardId);
        if(questionnaire == null)
            throw new BadRequestException("Selected questionnaire doesn't belong to selected building!");
    }

    @Override
    public Questionnaire setDateExpired(Questionnaire questionnaire, Meeting meeting) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(meeting.getDate());
        cal.add(Calendar.MINUTE, meeting.getDuration());

        questionnaire.setDateExpire(cal.getTime());
        return questionnaire;
    }
}
