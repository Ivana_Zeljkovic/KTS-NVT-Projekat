package rs.ac.uns.ftn.ktsnvt.web.dto;

import rs.ac.uns.ftn.ktsnvt.model.entity.Business;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ivana Zeljkovic on 11/01/2018.
 */
public class BusinessForDamageDTO implements Serializable {

    private String name;
    private Long id;
    private AddressDTO address;
    private List<String> usernames;

    public BusinessForDamageDTO() {
        this.usernames = new ArrayList<>();
    }

    public BusinessForDamageDTO(Business business) {
        this.name = business.getName();
        this.id = business.getId();
        this.address = new AddressDTO(business.getAddress());
        this.usernames = new ArrayList<>();
        business.getAccounts().forEach(account -> this.usernames.add(account.getUsername()));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AddressDTO getAddress() { return address; }

    public void setAddress(AddressDTO address) { this.address = address; }

    public List<String> getUsernames() {
        return usernames;
    }

    public void setUsernames(List<String> usernames) {
        this.usernames = usernames;
    }
}
