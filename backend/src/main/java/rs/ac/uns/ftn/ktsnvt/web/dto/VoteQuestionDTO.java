package rs.ac.uns.ftn.ktsnvt.web.dto;

import rs.ac.uns.ftn.ktsnvt.model.entity.QuestionVote;

import java.io.Serializable;

/**
 * Created by Ivana Zeljkovic on 20/11/2017.
 */
public class VoteQuestionDTO implements Serializable{

    private Long id;
    private Long questionId;
    private String answer;

    public VoteQuestionDTO() { }

    public VoteQuestionDTO(QuestionVote questionVote) {
        this.id = questionVote.getId();
        this.questionId = questionVote.getQuestion().getId();
        this.answer = questionVote.getAnswer();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
