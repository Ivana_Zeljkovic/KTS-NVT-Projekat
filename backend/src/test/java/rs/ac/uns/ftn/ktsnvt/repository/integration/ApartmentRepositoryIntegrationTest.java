package rs.ac.uns.ftn.ktsnvt.repository.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.model.entity.Apartment;
import rs.ac.uns.ftn.ktsnvt.repository.ApartmentRepository;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Nikola Garabandic on 30/11/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class ApartmentRepositoryIntegrationTest {

    @Autowired
    private ApartmentRepository apartmentRepository;

    private Pageable page = new PageRequest(0, 10, Sort.Direction.ASC, "id");

    @Test
    @Transactional
    public void testGetAllFromBuilding() {
        /*
        * Test proverava ispravnost rada metode getAllFromBuilding repozitorijuma ApartmentRepository
        * (pronalazenje svih stanova)
        */
        Page<Apartment> apartmentList = apartmentRepository.getAllFromBuilding(100L , this.page);

        assertEquals(4, apartmentList.getContent().size());
    }

    @Test
    @Transactional
    public void testFindFreeApartment() {
        /*
        * Test proverava ispravnost rada metode findFreeApartment repozitorijuma ApartmentRepository
        * (pronalazenje praznih stanova)
        */

        /*
        u bazi postoji samo jedan prazan stan koji obuhvata sledece podatke:
            id = 104,
            building_id = 100,
            owner_id = 102
        */
        Page<Apartment> apartmentList = apartmentRepository.findFreeApartments(this.page);

        assertEquals(1, apartmentList.getContent().size());
        assertEquals(Long.valueOf(104), apartmentList.getContent().get(0).getId());
        assertEquals(Long.valueOf(100), apartmentList.getContent().get(0).getBuilding().getId());
        assertEquals(Long.valueOf(102), apartmentList.getContent().get(0).getOwner().getId());
    }

    @Test
    @Transactional
    public void testFindFreeApartmentInBuilding(){
        /*
        * Test proverava ispravnost rada metode findFreeApartmentsInBuilding repozitorijuma ApartmentRepository
        * (pronalazenje slobodnih stanova na osnovu ID-a zgrade)
        */

        /*
        u zgradi sa ID-em = 100 se nalazi jedan prazan stan sa sledecim podacima:
            id = 104,
            building_id = 100,
            owner_id = 102
        */
        Page<Apartment> apartmentList = this.apartmentRepository.findFreeApartmentsInBuilding(100L, this.page);

        assertEquals(1, apartmentList.getContent().size());
        assertEquals(Long.valueOf(104), apartmentList.getContent().get(0).getId());
        assertEquals(Long.valueOf(100), apartmentList.getContent().get(0).getBuilding().getId());
        assertEquals(Long.valueOf(102), apartmentList.getContent().get(0).getOwner().getId());
    }

    @Test
    @Transactional
    public void testFindFreeApartmentsInBuildingNoFreeApartments() {
       /*
        * Test proverava ispravnost rada metode findFreeApartmentsInBuilding repozitorijuma ApartmentRepository
        * (pronalazenje slobodnih stanova na osnovu ID-a zgrade)
        */

        // u zgradi sa ID-em 101 nema praznih stanova
        Page<Apartment> apartmentList = this.apartmentRepository.findFreeApartmentsInBuilding(101L, this.page);

        assertEquals(0, apartmentList.getContent().size());
    }

    @Test
    @Transactional
    public void testFindFreeApartmentsBySize(){
        /*
        * Test proverava ispravnost rada metode findFreeApartmentsBySize repozitorijuma ApartmentRepository
        * (pronalazenje slobodnih stanova na osnovu minimalne i maksimalne kvadrature)
        */

        /*
        samo jedan slobodan stan odgovara datom opsegu kvadrature, a to je stan sa ID-em 104 i sledecim podacima:
            id = 104,
            building_id = 100,
            owner_id = 102
        */
        Page<Apartment> apartmentList = this.apartmentRepository.findFreeApartmentsBySize(20, 200, Long.valueOf(100), this.page);

        assertEquals(1, apartmentList.getContent().size());
        assertEquals(Long.valueOf(104), apartmentList.getContent().get(0).getId());
        assertEquals(Long.valueOf(100), apartmentList.getContent().get(0).getBuilding().getId());
        assertEquals(Long.valueOf(102), apartmentList.getContent().get(0).getOwner().getId());
    }

    @Test
    @Transactional
    public void testFindNoneFreeApartmentsBySize(){
        /*
        * Test proverava ispravnost rada metode findFreeApartmentsBySize repozitorijuma ApartmentRepository
        * (pronalazenje slobodnih stanova na osnovu minimalne i maksimalne kvadrature)
        */

        // ne postoji nijedan slobodan stan koji ima kvadraturu u datom opsegu
        Page<Apartment> apartment = this.apartmentRepository.findFreeApartmentsBySize(100, 200,  Long.valueOf(104), this.page);

        assertEquals(0, apartment.getContent().size());
    }

    @Test
    @Transactional
    public void testCountApartmentsInBuilding(){
        /*
        * Test proverava ispravnost rada metode countApartmentsInBuilding repozitorijuma ApartmentRepository
        * (dobavljanje broja stanova u zgradi sa zadatim ID-em ciji je vlasnik stanar sa zadatim ID-em)
        */

        // u zgradi sa ID-em = 100 postoje 3 stana ciji je vlasnik stanar sa ID-em = 101
        int count = this.apartmentRepository.countApartmentsInBuilding(101L, 100L);

        assertEquals(3, count);
    }
}
