package rs.ac.uns.ftn.ktsnvt.web.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by Ivana Zeljkovic on 10/12/2017.
 */
public class TenantMemberCouncilDTO implements Serializable {

    @NotNull
    private Long tenantId;

    public TenantMemberCouncilDTO() { }

    public TenantMemberCouncilDTO(Long tenantId) {
        this.tenantId = tenantId;
    }

    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }
}
