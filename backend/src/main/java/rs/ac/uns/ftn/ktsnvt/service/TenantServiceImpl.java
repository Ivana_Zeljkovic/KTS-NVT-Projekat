package rs.ac.uns.ftn.ktsnvt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.ForbiddenException;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Apartment;
import rs.ac.uns.ftn.ktsnvt.model.entity.Building;
import rs.ac.uns.ftn.ktsnvt.model.entity.Tenant;
import rs.ac.uns.ftn.ktsnvt.repository.AccountAuthorityRepository;
import rs.ac.uns.ftn.ktsnvt.repository.TenantRepository;
import rs.ac.uns.ftn.ktsnvt.web.controller.util.MessageConstants;

import java.util.List;

/**
 * Created by Ivana Zeljkovic on 05/11/2017.
 */
@Service
public class TenantServiceImpl implements TenantService {

    @Autowired
    private TenantRepository tenantRepository;

    @Autowired
    private AccountAuthorityRepository accountAuthorityRepository;

    @Autowired
    private AccountService accountService;


    @Override
    @Transactional(readOnly = true)
    public Page<Tenant> findAll(Integer pageNumber, Integer pageSize, String sortDirection, String sortProperty) {
        return this.tenantRepository.findAll(new PageRequest(pageNumber, pageSize,
                (sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortProperty));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Tenant> findAllByBuildingId(Long buildingId, Integer pageNumber, Integer pageSize,
                                            String sortDirection, String sortProperty){
        return this.tenantRepository.findAllByBuildingId(buildingId,
                new PageRequest(pageNumber, pageSize,
                        (sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortProperty));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Tenant> findAllByBuildingIdExceptCurrent(Long buildingId, Long currentTenantId, Integer pageNumber,
                                                         Integer pageSize, String sortDirection, String sortProperty){
        return this.tenantRepository.findAllByBuildingIdExceptCurrent(buildingId, currentTenantId,
                new PageRequest(pageNumber, pageSize,
                        (sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortProperty));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Tenant> findAllCouncilMembers(Long buildingId, Long currentTenantId, Integer pageNumber,
                                              Integer pageSize, String sortDirection, String sortProperty) {
        return this.tenantRepository.findAllCouncilMembers(buildingId, currentTenantId,
                new PageRequest(pageNumber, pageSize,
                        (sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortProperty));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Tenant> findAllInApartment(Long apartmentId, Long currentTenantId, Integer pageNumber,
                                           Integer pageSize, String sortDirection, String sortProperty) {
        return this.tenantRepository.findAllInApartment(apartmentId, currentTenantId,
                new PageRequest(pageNumber, pageSize,
                        (sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortProperty));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Tenant> findAllInApartmentExceptPresident(Long apartmentId, Long currentTenantId,
                                                          Long currentPresidentId, Integer pageNumber,
                                                          Integer pageSize, String sortDirection, String sortProperty) {
        return this.tenantRepository.findAllInApartmentExceptPresident(apartmentId, currentTenantId, currentPresidentId,
                new PageRequest(pageNumber, pageSize,
                        (sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortProperty));
    }

    @Override
    @Transactional(readOnly = true)
    public Tenant findOne(Long id) {
        Tenant tenant = this.tenantRepository.findOne(id);
        if(tenant == null) throw new NotFoundException("Tenant not found!");
        return tenant;
    }

    @Override
    @Transactional(readOnly = false)
    public Tenant save(Tenant tenant) {
        return this.tenantRepository.save(tenant);
    }

    @Override
    @Transactional(readOnly = false)
    public void remove(Long id) {
        this.tenantRepository.delete(id);
    }

    @Override
    public boolean checkPasswordUpdate(Tenant tenant, String currentPassword, String newPassword) {
        if(currentPassword == null && newPassword == null) return true;

        BCryptPasswordEncoder crypt = new BCryptPasswordEncoder();

        if(currentPassword == null) throw new BadRequestException("Value of current password is missing!");
        if(newPassword == null) throw new BadRequestException("Value of new password is missing!");

        if(crypt.matches(currentPassword, tenant.getAccount().getPassword())) {
            tenant.getAccount().setPassword(crypt.encode(newPassword));
            return true;
        }

        throw new BadRequestException("Invalid value of current password!");
    }

    @Override
    @Transactional(readOnly = true)
    public List<Tenant> findByNameOrUsername(String name, String lastName, String username) {
        return this.tenantRepository.findByNameOrUsername(name, lastName, username);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Tenant> findTenantsByBuildingAndNameAndUsername(Long id, String name, String lastName, String username) {
        return this.tenantRepository.findTenantsByBuildingAndNameAndUsername(id, name, lastName, username);
    }

    @Override
    @Transactional(readOnly = false)
    public void removePresidentRole(Tenant tenant) {
        this.accountAuthorityRepository.removePresidentRole(tenant.getAccount().getId());
    }

    @Override
    public boolean checkTenantsPersonalApartments(Tenant tenant, Long buildingId) {
        for (Apartment apartment : tenant.getPersonalApartments()) {
            if (apartment.getBuilding().getId().toString().equals(buildingId.toString())) return true;
        }
        return false;
    }

    @Override
    @Transactional(readOnly = true)
    public Tenant findOneByFirstNameAndLastNameAndUsername(String firstName, String lastName, String username) {
        Tenant tenant = this.tenantRepository.findByNameAndUsername(firstName, lastName, username);
        if(tenant == null) throw new NotFoundException("Tenant with name: " + firstName + " " + lastName +
                " and username: " + username + " not found!");
        return tenant;
    }

    @Override
    @Transactional(readOnly = true)
    public void checkPermissionForCurrentTenantInBuilding(Long buildingId) {
        Tenant currentTenant = this.accountService.findByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()).getTenant();

        if(currentTenant != null) {
            // trenutno logovani stanar nije povezan ni sa jednim stanom
            if (currentTenant.getApartmentLiveIn() == null)
                throw new ForbiddenException("You don't have permission for this action! (You are not connected with any apartment)");

            // trenutno logovani stanar ne zivi u zgradi koja ima ID building_id
            if (currentTenant.getApartmentLiveIn().getBuilding().getId() != buildingId)
                throw new ForbiddenException(MessageConstants.FORBIDDEN_ERROR_MESSAGE);
        }
        else throw new NullPointerException("There is no user with role TENANT on session");
    }

    @Override
    @Transactional(readOnly = true)
    public void checkPermissionForCurrentPresidentInBuilding(Building building) {
        Tenant currentPresident = this.accountService.findByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()).getTenant();

        // trenutno logovani predsednik skupstine stanara nije predsednik skupstine u zgradi building
        if(currentPresident != null && building.getCouncil().getPresident().getId() != currentPresident.getId())
            throw new ForbiddenException("You don't have permission for this action!");
        else if(currentPresident == null)
            throw new NullPointerException("There is no tenant on session");
    }

    @Override
    @Transactional(readOnly = true)
    public void checkPermissionForCurrentTenant(Long id) {
        Tenant currentTenant = this.accountService.findByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()).getTenant();
        // trenutno logovani stanar nema ID id
        if(currentTenant != null && currentTenant.getId() != id)
            throw new ForbiddenException("You don't have permission for this action!");
        else if(currentTenant == null)
            throw new NullPointerException("There is no tenant on session");
    }
}