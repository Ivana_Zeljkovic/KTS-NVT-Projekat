import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import { Observable } from "rxjs/Observable";
// service
import { DataService } from "./data.service";
// models
import { MeetingItem } from "../../shared/models/meeting-item";
import { PageAndSort } from "../../shared/models/page-and-sort";


@Injectable()
export class MeetingItemService extends DataService<MeetingItem>{

  constructor(http: HttpClient) {
    super('', http);
  }

  searchFromTo(buildingId: number, from: string, to: string, pageAndSort: PageAndSort): Observable<Array<MeetingItem>> {
    let params: HttpParams = new HttpParams()
      .append('from', from)
      .append('to', to)
      .append('pageNumber', pageAndSort.pageNumber)
      .append('pageSize', pageAndSort.pageSize)
      .append('sortDirection', pageAndSort.sortDirection)
      .append('sortProperty', pageAndSort.sortProperties[0]);

    return this.http.get<Array<MeetingItem>>(`${this.urlBase}/buildings/${buildingId}/billboard/meeting_items_from_to`, {params})
      .catch(this.handleErrors);
  }
}
