import { Component, OnInit } from '@angular/core';
import { BsDatepickerConfig, BsLocaleService, BsModalService, defineLocale, enGb } from "ngx-bootstrap";
import { ToasterService } from "angular5-toaster/dist";
import * as _ from "lodash";
// service
import { MeetingItemService } from "../../../core/services/meeting-item.service";
import { JwtService } from "../../../core/services/jwt.service";
// model
import { MeetingItem} from "../../../shared/models/meeting-item";
import { MeetingItemCreate } from "../../../shared/models/meeting-item-create";
import { PageAndSort } from "../../../shared/models/page-and-sort";
import { QuestionCreate } from "../../../shared/models/question-create";
import { QuestionnaireCreate } from "../../../shared/models/questionnaire-create";
import { MeetingItemUpdate } from "../../../shared/models/meeting-item-update";
import { QuestionnaireUpdate } from "../../../shared/models/questionnaire-update";
import { DamageForMeetingItem } from "../shared/models/damage-for-meeting-item";
import { Question } from "../../../shared/models/question";
// component
import { ConfirmDeleteModalComponent } from "../../../shared/components/confirm-delete-modal/confirm-delete-modal.component";
import { MeetingItemComponent} from "../meeting-item/meeting-item.component";
import { QuestionnaireComponent } from "../../../shared/components/questionnaire/questionnaire.component";
// error
import { AppError } from "../../../shared/errors/app-error";
import { BadRequestError } from "../../../shared/errors/bad-request-error";
import { ForbiddenError } from "../../../shared/errors/forbidden-error";
import { NotFoundError } from "../../../shared/errors/not-found-error";
// pipe
import { DateTimeFormatServerPipe } from "../../../shared/pipes/date-time-format-server.pipe";


@Component({
  selector: 'app-meeting-item-list',
  templateUrl: './meeting-item-list.component.html',
  styleUrls: ['./meeting-item-list.component.css']
})
export class MeetingItemListComponent implements OnInit {
  meetingItemList: Array<MeetingItem>;
  buildingID: number;
  daterangepickerModel: Date[];
  isSearchMode: boolean;
  dateTimeFormatPipe: DateTimeFormatServerPipe;

  // Daterangepicker configuration
  bsConfig: Partial<BsDatepickerConfig>;
  maxDate: Date;

  // Elements for bootstrap paging
  currentPage: number;
  totalItems: number;
  maxSize: number;
  itemsPerPage: number;

  // Elements for getting data from server
  pageSize: string;
  sortDirection: string;
  sortProperties: string[];


  constructor(private meetingItemService: MeetingItemService, private jwtService: JwtService,
              private toasterService: ToasterService, private modalService: BsModalService,
              private localeService: BsLocaleService) {
    // Daterangepicker config
    this.bsConfig = Object.assign({}, {containerClass: 'theme-dark-blue'});
    this.maxDate = new Date();
    defineLocale('enGb', enGb);
    this.localeService.use('enGb');
    this.daterangepickerModel = [];
    this.dateTimeFormatPipe = new DateTimeFormatServerPipe();

    // Sett up for bootstrap paging
    this.currentPage = 1;
    this.maxSize = 3;
    this.itemsPerPage = 4;

    // Sett up for getting meeting-details items request
    this.pageSize = '4';
    this.sortDirection = 'DESC';
    this.sortProperties = ['date'];
  }

  ngOnInit() {
    this.buildingID = this.jwtService.getBuildingIdFromToken();
    // load list of meeting items only if tenant is connected to some apartment (and building)
    if(this.buildingID !== -1)
      this.loadMeetingItemList();
  }

  add() {
    const modalRef = this.modalService.show(MeetingItemComponent);

    modalRef.content.model = new MeetingItemCreate('', '', new QuestionnaireCreate(new Array<QuestionCreate>()));
    modalRef.content.update = false;
    modalRef.content.initialValues = _.cloneDeep(modalRef.content.model);
    modalRef.content.damageForMeetingItem = new DamageForMeetingItem(-1, '');
    modalRef.content.damageForMeetingItemInitial = _.cloneDeep(modalRef.content.damageForMeetingItem);

    modalRef.content.submitted.subscribe(() => {
      this.currentPage = 1;
      this.loadMeetingItemList();
    });
  }

  questionnaireReview(meetingItem) {
    const modalRef = this.modalService.show(QuestionnaireComponent);

    modalRef.content.isReviewMode = true;
    modalRef.content.model = new QuestionnaireUpdate(meetingItem.questionnaire.id, meetingItem.questionnaire.questions);
    modalRef.content.initialValues = _.cloneDeep(modalRef.content.model);
  }

  edit(meetingItem: MeetingItem) {
    let index = this.meetingItemList.indexOf(meetingItem);
    let questionnaire = meetingItem.questionnaire ?
      new QuestionnaireUpdate(meetingItem.questionnaire.id, meetingItem.questionnaire.questions) :
      new QuestionnaireUpdate(-1, new Array<Question>());

    const modalRef = this.modalService.show(MeetingItemComponent);

    modalRef.content.model = new MeetingItemUpdate(meetingItem.content, meetingItem.title, questionnaire,
      (meetingItem.damageId ? meetingItem.damageId : null));
    modalRef.content.update = true;
    modalRef.content.meetingItemID = meetingItem.id;
    modalRef.content.initialValues = _.cloneDeep(modalRef.content.model);
    modalRef.content.damageForMeetingItem = new DamageForMeetingItem((meetingItem.damageId) ? meetingItem.damageId : -1,
      (meetingItem.damageDescription) ? meetingItem.damageDescription : '');
    modalRef.content.damageForMeetingItemInitial = _.cloneDeep(modalRef.content.damageForMeetingItem);

    modalRef.content.submitted.subscribe(updatedMeetingItem => {
      this.meetingItemList[index] = updatedMeetingItem;
    });
  }

  remove(meetingItem: MeetingItem) {
    const modalRef = this.modalService.show(
      ConfirmDeleteModalComponent,
      Object.assign({},{ class: 'modal-sm' })
    );

    let numberOfPages = Math.ceil(meetingItem.numberOfElements / +(this.pageSize));
    let pageShouldDecrement = false;
    if(numberOfPages === this.currentPage && (meetingItem.numberOfElements - 1) % +(this.pageSize) === 0) {
      pageShouldDecrement = true;
    }

    modalRef.content.confirmed.subscribe((value) => {
      if(value) {
        this.meetingItemService.remove(meetingItem.id, 'buildings', this.buildingID, 'billboard/meeting_items')
          .subscribe(() => {
            if(pageShouldDecrement && this.currentPage !== 1)
              this.currentPage--;
            this.loadMeetingItemList();
          }, (error: AppError) => {
            if(error instanceof BadRequestError)
              this.toasterService.pop('error', 'Error', 'Selected meeting-details item doesn\'t belong to this building!');
            else if(error instanceof ForbiddenError)
              this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
            else if(error instanceof NotFoundError)
              this.toasterService.pop('error', 'Error', 'Meeting item not found!');
            else {
              this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
              throw error;
            }
          });
      }
    });
  }

  pageChanged(event: any) {
    this.currentPage = event.page;
    this.loadMeetingItemList();
  }

  search() {
    this.isSearchMode = true;
    if(this.daterangepickerModel.length > 0) {
      this.currentPage = 1;
      this.loadMeetingItemList();
    }
  }

  reset() {
    this.isSearchMode = false;
    this.daterangepickerModel = [];
    this.currentPage = 1;
    this.loadMeetingItemList();
  }

  setTime() {
    if(this.daterangepickerModel.length > 0) {
      this.daterangepickerModel[0].setHours(0, 0, 0);
      this.daterangepickerModel[1].setHours(23, 59, 59);
    }
  }

  private loadMeetingItemList() {
    let page = new PageAndSort((this.currentPage - 1).toString(), this.pageSize, this.sortDirection, this.sortProperties);
    if(!this.isSearchMode) {
      // getting page, without search
      this.meetingItemService.getPage(page, 'buildings', this.buildingID, 'billboard/meeting_items')
        .subscribe(meetingItems => {
          if(meetingItems.length !== 0)
            this.totalItems = meetingItems[0].numberOfElements;
          else this.totalItems = 0;
          this.meetingItemList = meetingItems;
        }, (error: AppError) => {
          if(error instanceof ForbiddenError)
            this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
          else if(error instanceof NotFoundError)
            this.toasterService.pop('error', 'Error', 'Building not found!');
          else {
            this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
            throw error;
          }
        });
    }
    else {
      // getting page, with search by date range
      let from = this.dateTimeFormatPipe.transform(this.daterangepickerModel[0]);
      let to = this.dateTimeFormatPipe.transform(this.daterangepickerModel[1]);
      this.meetingItemService.searchFromTo( this.buildingID, from, to, page)
        .subscribe(meetingItems => {
          if(meetingItems.length !== 0)
            this.totalItems = meetingItems[0].numberOfElements;
          else this.totalItems = 0;
          this.meetingItemList = meetingItems;
        }, (error: AppError) => {
          if(error instanceof ForbiddenError)
            this.toasterService.pop('error', 'Error', 'You don\'t have permissiong for this action!');
          else if(error instanceof NotFoundError)
            this.toasterService.pop('error', 'Error', 'Building not found!');
          else {
            this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
            throw error;
          }
        });
    }
  }
}
