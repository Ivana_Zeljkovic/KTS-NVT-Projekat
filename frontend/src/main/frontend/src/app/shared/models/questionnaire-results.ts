import { QuestionResults } from "./question-results";

export interface QuestionnaireResults {
  questionnaireId: number;
  numberOfVotes: number;
  results: Array<QuestionResults>;
}
