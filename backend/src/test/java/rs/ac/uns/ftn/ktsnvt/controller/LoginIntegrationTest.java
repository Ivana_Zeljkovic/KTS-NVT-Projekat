package rs.ac.uns.ftn.ktsnvt.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import rs.ac.uns.ftn.ktsnvt.controller.util.TestUtil;
import rs.ac.uns.ftn.ktsnvt.web.dto.LoginDTO;

import javax.annotation.PostConstruct;
import java.nio.charset.Charset;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Created by Ivana Zeljkovic on 04/12/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class LoginIntegrationTest {

    private static final String URL_PREFIX = "/api";

    private MediaType contentType = new MediaType(
            MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    private MockMvc mockMvc;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja stanara
    private String accessTokenTenant;


    @Autowired
    private WebApplicationContext webApplicationContext;


    @PostConstruct
    public void setup() {
        this.mockMvc = MockMvcBuilders.
                webAppContextSetup(webApplicationContext).apply(springSecurity()).build();
    }

    // TESTOVI za login
    @Test
    public void testLogin() throws Exception {
       /*
        * Test proverava ispravnost rada metode login kontrolera AppUserController
        * (prijavljivanje korisnika u aplikaciju)
        * u slucaju kada je dolazni DTO objekat validan: poseduje polja neophodna za logovanje korisnika: korisnicko ime i lozinku
        * pri cemu su korisnicko ime i lozinka odgovarajuci i pripadaju postojecem korisniku u bazi
        * */

        LoginDTO loginDTO = new LoginDTO("ivana", "ivana");

        String loginDTOJson = TestUtil.json(loginDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/login")
                .contentType(contentType)
                .content(loginDTOJson))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.value").exists());
    }

    @Test
    public void testLoginMissingUsername() throws Exception {
       /*
        * Test proverava ispravnost rada metode login kontrolera AppUserController
        * (prijavljivanje korisnika u aplikaciju)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne poseduje sva polja neophodna za logovanje - nedostaje polje korisnicko ime
        * */

        LoginDTO loginDTO = new LoginDTO();
        loginDTO.setPassword("ivana");

        String loginDTOJson = TestUtil.json(loginDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/login")
                .contentType(contentType)
                .content(loginDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testLoginMissingPassword() throws Exception {
       /*
        * Test proverava ispravnost rada metode login kontrolera AppUserController
        * (prijavljivanje korisnika u aplikaciju)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne poseduje sva polja neophodna za logovanje - nedostaje polje lozinka
        * */

        LoginDTO loginDTO = new LoginDTO();
        loginDTO.setUsername("ivana");

        String loginDTOJson = TestUtil.json(loginDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/login")
                .contentType(contentType)
                .content(loginDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testLoginInappropriateTypeOfUsername() throws Exception {
       /*
        * Test proverava ispravnost rada metode login kontrolera AppUserController
        * (prijavljivanje korisnika u aplikaciju)
        * u slucaju kada je dolazni DTO objekat nevalidan: poseduje sva polja neohodna za registraciju,
        * ali je tip podatka u polju korisnicko ime nevalidan (nije String nego int)
        * */

        String loginDTOJson = "{" +
                    "username:1" +
                    "password:'ivana'" +
                "}";

        this.mockMvc.perform(post(URL_PREFIX + "/login")
                .contentType(contentType)
                .content(loginDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testLoginInappropriateTypeOfPassword() throws Exception {
       /*
        * Test proverava ispravnost rada metode login kontrolera AppUserController
        * (prijavljivanje korisnika u aplikaciju)
        * u slucaju kada je dolazni DTO objekat nevalidan: poseduje sva polja neohodna za registraciju,
        * ali je tip podatka u polju lozinka nevalidan (nije String nego int)
        * */

        String loginDTOJson = "{" +
                "username:'ivana'" +
                "password:1" +
                "}";

        this.mockMvc.perform(post(URL_PREFIX + "/login")
                .contentType(contentType)
                .content(loginDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testLoginNonexistentUsername() throws Exception {
       /*
        * Test proverava ispravnost rada metode login kontrolera AppUserController
        * (prijavljivanje korisnika u aplikaciju)
        * u slucaju kada je dolazni DTO objekat validan: poseduje sva polja neohodna za registraciju - korisnicko ime i lozinku
        * ali ne postoji korisnik sa zadatim korisnickim imenom u bazi
        * */

        LoginDTO loginDTO = new LoginDTO("nevalidno", "ivana");

        String loginDTOJson = TestUtil.json(loginDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/login")
                .contentType(contentType)
                .content(loginDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testLoginInvalidPassword() throws Exception {
       /*
        * Test proverava ispravnost rada metode login kontrolera AppUserController
        * (prijavljivanje korisnika u aplikaciju)
        * u slucaju kada je dolazni DTO objekat validan: poseduje sva polja neohodna za registraciju - korisnicko ime i lozinku
        * ali je pogresna lozinka za zadato korisnicko ime
        * */

        LoginDTO loginDTO = new LoginDTO("ivana", "nevalidno");

        String loginDTOJson = TestUtil.json(loginDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/login")
                .contentType(contentType)
                .content(loginDTOJson))
                .andExpect(status().isNotFound());
    }
}
