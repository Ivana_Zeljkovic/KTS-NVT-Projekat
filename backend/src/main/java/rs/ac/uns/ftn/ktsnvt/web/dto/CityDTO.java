package rs.ac.uns.ftn.ktsnvt.web.dto;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by Katarina Cukurov on 07/11/2017.
 */
public class CityDTO implements Serializable{

    @NotNull
    @NotEmpty
    private String name;
    @NotNull
    private int postalNumber;

    public CityDTO() {}

    public CityDTO(String name, int postalNumber) {
        this.name = name;
        this.postalNumber = postalNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPostalNumber() {
        return postalNumber;
    }

    public void setPostalNumber(int postalNumber) {
        this.postalNumber = postalNumber;
    }
}
