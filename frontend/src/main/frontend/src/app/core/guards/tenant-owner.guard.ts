import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
import { Injectable } from "@angular/core";
import { JwtService } from "../services/jwt.service";

@Injectable()
export class TenantOwnerGuard implements CanActivate {

  constructor(private router: Router, private jwtService: JwtService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if(this.jwtService.hasRole('TENANT') && this.jwtService.getIsOwnerFromToken()) return true;

    // logged user hasn't role TENANT or he isn't owner of any apartment
    this.router.navigate(['home']);
    return false;
  }
}
