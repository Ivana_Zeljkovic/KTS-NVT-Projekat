package rs.ac.uns.ftn.ktsnvt.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;
import rs.ac.uns.ftn.ktsnvt.controller.util.TestUtil;
import rs.ac.uns.ftn.ktsnvt.web.dto.*;

import javax.annotation.PostConstruct;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.hamcrest.Matchers.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Created by Ivana Zeljkovic on 03/12/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class TenantControllerIntegrationTest {

    private static final String URL_PREFIX = "/api/tenants";

    private MediaType contentType = new MediaType(
            MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    private MockMvc mockMvc;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja administratora
    private String accessTokenAdmin;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja stanara
    private String accessTokenTenant, accessTokenTenant2, accessTokenTenant3, accessTokentTenantWithoutApartment;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja stanara koji je ujedno i predsednik skupstine
    private String accessTokenPresident;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    private MultiValueMap<String, String> params;


    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private TestRestTemplate testRestTemplate;



    @PostConstruct
    public void setup() {
        this.mockMvc = MockMvcBuilders.
                webAppContextSetup(webApplicationContext).apply(springSecurity()).build();
    }

    // pre izvrsavanja testa, neophodno je izvrsiti logovanje u aplikaciju da bismo dobili token
    @Before
    public void login() throws Exception {
        // logovanje predsednika skupstine stanara
        LoginDTO loginDTOPresident = new LoginDTO("kaca", "kaca");

        ResponseEntity<TokenDTO> responseEntityPresident =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOPresident,
                        TokenDTO.class);
        // preuzmemo token za logovanog predsednika iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenPresident = responseEntityPresident.getBody().getValue();


        // logovanje stanara
        LoginDTO loginDTOTenant = new LoginDTO("jasmina", "jasmina");

        ResponseEntity<TokenDTO> responseEntityTenant =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOTenant,
                        TokenDTO.class);
        // preuzmemo token za logovanog stanara iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenTenant = responseEntityTenant.getBody().getValue();


        // logovanje stanara 2
        LoginDTO loginDTOTenant2 = new LoginDTO("jovanka", "jovanka");

        ResponseEntity<TokenDTO> responseEntityTenant2 =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOTenant2,
                        TokenDTO.class);
        // preuzmemo token za logovanog stanara iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenTenant2 = responseEntityTenant2.getBody().getValue();


        // logovanje stanara 3
        LoginDTO loginDTOTenant3 = new LoginDTO("sanja", "sanja");

        ResponseEntity<TokenDTO> responseEntityTenant3 =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOTenant3,
                        TokenDTO.class);
        // preuzmemo token za logovanog stanara iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenTenant3 = responseEntityTenant3.getBody().getValue();


        // logovanje stanara koji nije povezan ni sa jednim stanom
        LoginDTO loginDTOTenantWithoutapartment = new LoginDTO("jovanka", "jovanka");

        ResponseEntity<TokenDTO> responseEntityTenantWithoutApartment =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOTenantWithoutapartment,
                        TokenDTO.class);
        // preuzmemo token za logovanog stanara iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokentTenantWithoutApartment = responseEntityTenantWithoutApartment.getBody().getValue();


        // logovanje administratora
        LoginDTO loginDTO = new LoginDTO("ivana", "ivana");

        ResponseEntity<TokenDTO> responseEntity =
                testRestTemplate.postForEntity("/api/login",
                        loginDTO,
                        TokenDTO.class);
        // preuzmemo token za logovanog administratora iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenAdmin = responseEntity.getBody().getValue();

        // za pageable atribut
        params = TestUtil.getParams(
                "0","10", "ASC", "id", "");
    }

    // TESTOVI ZA getAllTenants
    @Test
    public void testGetAllTenantsByAdmin() throws Exception {
       /*
        * Test proverava ispravnost rada metode getAllTenants kontrolera TenantController
        * (dobavljanje liste svih stanara koji su registrovani u aplikaciji)
        * u slucaju kada je trenutno logovan administrator
        * */

        this.mockMvc.perform(get(URL_PREFIX)
                .header("Authentication-Token", accessTokenAdmin)
                .params(params))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(10)));
    }

    @Test
    public void testGetAllTenantsByPresident() throws Exception {
       /*
        * Test proverava ispravnost rada metode getAllTenants kontrolera TenantController
        * (dobavljanje liste svih stanara koji zive u zgradi u kojoj je trenutno logovani
        * korisnik predsednik skupstine stanara)
        * u slucaju kada je trenutno logovan predsednik skupstine stanara
        * */

        this.mockMvc.perform(get(URL_PREFIX)
                .header("Authentication-Token", accessTokenPresident)
                .params(params))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(6)))
                .andExpect(jsonPath("$.[*].id", containsInAnyOrder(
                        101, 102, 103, 104, 105, 106)))
                .andExpect(jsonPath("$.[*].firstName", containsInAnyOrder(
                        "Katarina", "Nikola", "Jasmina", "Sanja", "Isidora", "Jasmina")))
                .andExpect(jsonPath("$.[*].lastName", containsInAnyOrder(
                        "Cukurov", "Garabandic", "Markovic", "Nikolic", "Krajinovic", "Markovic")));
    }

    @Test
    public void testGetAllTenantsByNotAuthenticatedUser() throws Exception {
       /*
        * Test proverava ispravnost rada metode getAllTenants kontrolera TenantController
        * (dobavljanje liste svih stanara)
        * ali dobavljanje resursa pokusava da izvrsi korisnik koji nije ulogovan
        * */

        this.mockMvc.perform(get(URL_PREFIX)
                .params(params))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetAllTenantsByNotAuthenticatedAdminOrPresident() throws Exception {
       /*
        * Test proverava ispravnost rada metode getAllTenants kontrolera TenantController
        * (dobavljanje liste svih stanara)
        * ali dobavljanje resursa pokusava da izvrsi neulogovani korisnik
        * */

        this.mockMvc.perform(get(URL_PREFIX)
                .params(params))
                .andExpect(status().isForbidden());
    }

    // TESTOVI ZA getTenant
    @Test
    public void testGetTenantByAdmin() throws Exception {
       /*
        * Test proverava ispravnost rada metode getTenant kontrolera TenantController
        * (dobavljanje konkretnog stanara na osnovu zadatog ID-a)
        * pri cemu u bazi postoji stanar sa ID-em koji je zadat u URL-u
        * */

        Long id = 107L;

        this.mockMvc.perform(get(URL_PREFIX + "/" + id)
                .header("Authentication-Token", accessTokenAdmin))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value("107"))
                .andExpect(jsonPath("$.firstName").value("Leontina"))
                .andExpect(jsonPath("$.lastName").value("Simonovic"))
                .andExpect(jsonPath("$.email").value("leontina@gmail.com"))
                .andExpect(jsonPath("$.phoneNumber").value("807123"));
    }

    @Test
    public void testGetTenantByPresident() throws Exception {
       /*
        * Test proverava ispravnost rada metode getTenant kontrolera TenantController
        * (dobavljanje konkretnog stanara iz zgrade u kojoj je trenutno logovani korisnik predsednik skupstine stanara,
         * na osnovu zadatog ID-a)
        * pri cemu u bazi postoji stanar sa ID-em koji je zadat u URL-u
        * */

        Long id = 104L;

        this.mockMvc.perform(get(URL_PREFIX + "/" + id)
                .header("Authentication-Token", accessTokenPresident))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value("104"))
                .andExpect(jsonPath("$.firstName").value("Sanja"))
                .andExpect(jsonPath("$.lastName").value("Nikolic"))
                .andExpect(jsonPath("$.email").value("sanja@gmail.com"))
                .andExpect(jsonPath("$.phoneNumber").value("654321"));
    }

    @Test
    public void testGetTenantByNotAuthenticatedUser() throws Exception {
       /*
        * Test proverava ispravnost rada metode getTenant kontrolera TenantController
        * (dobavljanje konkretnog stanara na osnovu zadatog ID-a)
        * ali dobavljanje resursa pokusava da izvrsi korisnik koji nije ulogovan
        * */

        Long id = 104L;

        this.mockMvc.perform(get(URL_PREFIX + "/" + id))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetTenantByNotAuthenticatedAdminOrPresident() throws Exception {
       /*
        * Test proverava ispravnost rada metode getTenant kontrolera TenantController
        * (dobavljanje konkretnog stanara na osnovu zadatog ID-a)
        * ali dobavljanje resursa pokusava da izvrsi ulogovani korisnik koji nema ulogu administratora niti
        * predsednika skupstine stanara
        * */

        Long id = 104L;

        this.mockMvc.perform(get(URL_PREFIX + "/" + id)
                .header("Authentication-Token", accessTokenTenant))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetTenantNonexistentTenant() throws Exception {
       /*
        * Test proverava ispravnost rada metode getTenant kontrolera TenantController
        * (dobavljanje konkretnog stanara na osnovu zadatog ID-a)
        * pri cemu u bazi ne postoji stanar sa ID-em koji je zadat u URL-u
        * */

        Long id = 200L;

        this.mockMvc.perform(get(URL_PREFIX + "/" + id)
                .header("Authentication-Token", accessTokenAdmin))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetTenantByPresidentNonexistentTenant() throws Exception {
       /*
        * Test proverava ispravnost rada metode getTenant kontrolera TenantController
        * (dobavljanje konkretnog stanara iz zgrade u kojoj je trenutno logovani korisnik predsednik skupstine stanara,
        * na osnovu zadatog ID-a)
        * pri cemu u bazi postoji stanar sa ID-em koji je zadat u URL-u, ali taj stanar ne zivi u zgradi u kojoj
        * je trenutno logovani korisnik predsednik skupstine stanara
        * */

        Long id = 107L;

        this.mockMvc.perform(get(URL_PREFIX + "/" + id)
                .header("Authentication-Token", accessTokenPresident))
                .andExpect(status().isForbidden());
    }

    // TESTOVI za deleteTenant
    @Test
    @Transactional
    @Rollback(true)
    public void testDeleteTenantByAdmin() throws Exception {
       /*
        * Test proverava ispravnost rada metode deleteTenant kontrolera TenantController
        * (uklanjanje konkretnog stanara na osnovu zadatog ID-a)
        * pri cemu u bazi postoji stanar sa ID-em koji je zadat u URL-u
        * */

        Long id = 106L;

        this.mockMvc.perform(delete(URL_PREFIX + "/" + id)
                .header("Authentication-Token", accessTokenAdmin))
                .andExpect(status().isOk());
        // nakon brisanja, ne postoji trazeni resurs u bazi
        this.mockMvc.perform(get(URL_PREFIX + "/" + id)
                .header("Authentication-Token", accessTokenAdmin))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeleteTenantByNotAuthenticatedUser() throws Exception {
       /*
        * Test proverava ispravnost rada metode deleteTenant kontrolera TenantController
        * (uklanjanje konkretnog stanara na osnovu zadatog ID-a)
        * ali uklanjanje resursa pokusava da izvrsi korisnik koji nije ulogovan
        * */

        Long id = 104L;

        this.mockMvc.perform(delete(URL_PREFIX + "/" + id))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDeleteTenantByNotAuthenticatedAdminOrPresident() throws Exception {
        /*
        * Test proverava ispravnost rada metode deleteTenant kontrolera TenantController
        * (uklanjanje konkretnog stanara na osnovu zadatog ID-a)
        * ali uklanjanje resursa pokusava da izvrsi ulogovani korisnik koji nema ulogu administratora niti predsednika
        * skupstine stanara
        * */

        Long id = 104L;

        this.mockMvc.perform(delete(URL_PREFIX + "/" + id)
                .header("Authentication-Token", accessTokenTenant))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDeleteTenantNonexistentTenant() throws Exception {
       /*
        * Test proverava ispravnost rada metode deleteTenant kontrolera TenantController
        * (uklanjanje konkretnog stanara na osnovu zadatog ID-a)
        * pri cemu u bazi ne postoji stanar sa ID-em koji je zadat u URL-u
        * */

        Long id = 200L;

        this.mockMvc.perform(delete(URL_PREFIX + "/" + id)
                .header("Authentication-Token", accessTokenAdmin))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeleteTenantWhoIsPresident() throws Exception {
       /*
        * Test proverava ispravnost rada metode deleteTenant kontrolera TenantController
        * (uklanjanje konkretnog stanara iz zgrade u kojoj je trenutno logovani korisnik admin,
        * na osnovu zadatog ID-a)
        * pri cemu se brise stanar koji je ujedno i predsednik skupstine stanara
        * */

        Long id = 101L;

        // stanar sa ID-em = 101 je predsednik skupstine stanara
        this.mockMvc.perform(delete(URL_PREFIX + "/" + id)
                .header("Authentication-Token", accessTokenAdmin))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDeleteTenantWhoIsResponsibleForDamage() throws Exception {
       /*
        * Test proverava ispravnost rada metode deleteTenant kontrolera TenantController
        * (uklanjanje konkretnog stanara iz zgrade u kojoj je trenutno logovani korisnik admin,
        * na osnovu zadatog ID-a)
        * pri cemu se brise stanar koji je odgovoran za neki kvar koji jos uvek nije resen
        * */

        Long id = 104L;

        // stanar sa ID-em = 104 je odgovoran za kvar sa ID-em = 100 koji jos uvek nije resen
        this.mockMvc.perform(delete(URL_PREFIX + "/" + id)
                .header("Authentication-Token", accessTokenAdmin))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testDeleteTenantWhoIsResponsibleForDamageTypes() throws Exception {
       /*
        * Test proverava ispravnost rada metode deleteTenant kontrolera TenantController
        * (uklanjanje konkretnog stanara iz zgrade u kojoj je trenutno logovani korisnik admin,
        * na osnovu zadatog ID-a)
        * pri cemu se brise stanar koji je odgovoran za neki tip kvara
        * */

        Long id = 102L;

        // stanar sa ID-em = 102 je odgovoran za tip kvara sa ID-em = 3
        this.mockMvc.perform(delete(URL_PREFIX + "/" + id)
                .header("Authentication-Token", accessTokenAdmin))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za registerTenant
    @Test
    @Transactional
    @Rollback(true)
    public void testRegisterTenantAsAnonymous() throws Exception{
        /*
        * Test proverava ispravnost rada metode registerTenant kontrolera TenantController
        * (registracija novog stanara kao korisnik koji nije ulogovan)
        * */
        TenantCreateDTO tenantCreateDTO = new TenantCreateDTO(new LoginDTO("pera", "pera"),
                "Pera", "Peric", new Date(), "peraperic@gmail.com", "0634198131");

        String tenantJson = TestUtil.json(tenantCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX)
                .contentType(contentType)
                .content(tenantJson))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.firstName").value("Pera"))
                .andExpect(jsonPath("$.lastName").value("Peric"))
                .andExpect(jsonPath("$.phoneNumber").value("0634198131"));
    }

    @Test
    @Transactional
    @Rollback(true)
    public void testRegisterTenantByAdministrator() throws Exception{
        /*
        * Test proverava ispravnost rada metode registerTenant kontrolera TenantController
        * (registracija novog stanara kao administrator)
        * u slucaju kada akciju izvrsava ulogovani administrator
        * */
        TenantCreateDTO tenantCreateDTO = new TenantCreateDTO(new LoginDTO("pera", "pera"),
                "Pera", "Peric", new Date(), "peraperic@gmail.com", "0634198131");

        String tenantJson = TestUtil.json(tenantCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX)
                .header("Authentication-Token", this.accessTokenAdmin)
                .contentType(contentType)
                .content(tenantJson))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.firstName").value("Pera"))
                .andExpect(jsonPath("$.lastName").value("Peric"))
                .andExpect(jsonPath("$.phoneNumber").value("0634198131"));
    }

    @Test
    public void testRegisterTenantByTenant() throws Exception{
        /*
        * Test proverava ispravnost rada metode registerTenant kontrolera TenantController
        * (registracija novog stanara)
        * u slucaju kada akciju izvrsava ulogovani stanar
        * */
        TenantCreateDTO tenantCreateDTO = new TenantCreateDTO(new LoginDTO("pera", "pera"),
                "Pera", "Peric", new Date(), "peraperic@gmail.com", "0634198131");

        String tenantJson = TestUtil.json(tenantCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX)
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(tenantJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testRegisterTenantUsernameAlreadyExists() throws Exception{
        /*
        * Test proverava ispravnost rada metode registerTenant kontrolera TenantController
        * (registracija novog stanara)
        * u slucaju kada je dolazni DTO objekat validan: poseduje sva polja neohodna za registraciju - ime, prezime i kredencijale
        * pri cemu je izabrano korisnicko ime vec zauzeto od strane nekog drugog korisnika u aplikaciji
        * */
        TenantCreateDTO tenantCreateDTO = new TenantCreateDTO(new LoginDTO("nikola", "pera"),
                "Pera", "Peric", new Date(), "peraperic@gmail.com", "0634198131");

        String tenantJson = TestUtil.json(tenantCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX)
                .contentType(contentType)
                .content(tenantJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testRegisterTenantMissingUsername() throws Exception{
         /*
        * Test proverava ispravnost rada metode registerAdmin kontrolera TenantController
        * (registracija novog stanara)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne poseduje sva polja neohodna za registraciju - nedostaje polje username
        * */
        TenantCreateDTO tenantCreateDTO = new TenantCreateDTO(new LoginDTO(null, "pera"),
                "Pera", "Peric", new Date(), "peraperic@gmail.com", "0634198131");

        String tenantJson = TestUtil.json(tenantCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX)
                .contentType(contentType)
                .content(tenantJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testRegisterTenantMissingPassword() throws Exception{
        /*
        * Test proverava ispravnost rada metode registerAdmin kontrolera TenantController
        * (registracija novog stanara)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne poseduje sva polja neohodna za registraciju - nedostaje polje password
        * */
        TenantCreateDTO tenantCreateDTO = new TenantCreateDTO(new LoginDTO("pera", null),
                "Pera", "Peric", new Date(), "peraperic@gmail.com", "0634198131");

        String tenantJson = TestUtil.json(tenantCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX)
                .contentType(contentType)
                .content(tenantJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testRegisterTenantMissingFirstName() throws Exception{
        /*
        * Test proverava ispravnost rada metode registerAdmin kontrolera TenantController
        * (registracija novog stanara)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne poseduje sva polja neohodna za registraciju - nedostaje polje firstName
        * */

        TenantCreateDTO tenantCreateDTO = new TenantCreateDTO(new LoginDTO("pera", "pera"),
                null, "Peric", new Date(), "peraperic@gmail.com", "0634198131");

        String tenantJson = TestUtil.json(tenantCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX)
                .contentType(contentType)
                .content(tenantJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testRegisterTenantMissingLastName() throws Exception{
        /*
        * Test proverava ispravnost rada metode registerAdmin kontrolera TenantController
        * (registracija novog stanara od strane anonimnog korisnika)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne poseduje sva polja neohodna za registraciju - nedostaje polje lastName
        * */

        TenantCreateDTO tenantCreateDTO = new TenantCreateDTO(new LoginDTO("pera", "pera"),
                "Pera", null , new Date(), "peraperic@gmail.com", "0634198131");

        String tenantJson = TestUtil.json(tenantCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX)
                .contentType(contentType)
                .content(tenantJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testRegisterTenantMissingBirthDate() throws Exception{
        /*
        * Test proverava ispravnost rada metode registerAdmin kontrolera TenantController
        * (registracija novog stanara od strane anonimnog korisnika)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne poseduje sva polja neohodna za registraciju - nedostaje polje birthDate
        * */

        TenantCreateDTO tenantCreateDTO = new TenantCreateDTO(new LoginDTO("pera", "pera"),
                "Pera", "Peric" , null, "peraperic@gmail.com", "0634198131");

        String tenantJson = TestUtil.json(tenantCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX)
                .contentType(contentType)
                .content(tenantJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testRegisterTenantMissingEmail() throws Exception{
        /*
        * Test proverava ispravnost rada metode registerAdmin kontrolera TenantController
        * (registracija novog stanara od strane anonimnog korisnika)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne poseduje sva polja neohodna za registraciju - nedostaje polje email
        * */

        TenantCreateDTO tenantCreateDTO = new TenantCreateDTO(new LoginDTO("pera", "pera"),
                "Pera", "Peric" , new Date(), null, "0634198131");

        String tenantJson = TestUtil.json(tenantCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX)
                .contentType(contentType)
                .content(tenantJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testRegisterTenantMissingPhoneNumber() throws Exception{
        /*
        * Test proverava ispravnost rada metode registerAdmin kontrolera TenantController
        * (registracija novog stanara od strane anonimnog korisnika)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne poseduje sva polja neohodna za registraciju - nedostaje polje phoneNumber
        * */

        TenantCreateDTO tenantCreateDTO = new TenantCreateDTO(new LoginDTO("pera", "pera"),
                "Pera", "Peric" , new Date(), "kantagara@gmail.com", null);

        String tenantJson = TestUtil.json(tenantCreateDTO);
        this.mockMvc.perform(post(URL_PREFIX)
                .contentType(contentType)
                .content(tenantJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testRegisterTenantInappropriateTypeOfBirthDate() throws Exception{
        /*
        * Test proverava ispravnost rada metode registerAdmin kontrolera TenantController
        * (registracija novog stanara od strane anonimnog korisnika)
        * u slucaju kada je dolazni DTO objekat nevalidan: poseduje sva polja neohodna za registraciju, ali je polje birthDate
        * nevalidnog tipa
        * */

        String tenantJson = "{\"loginAccount\":{\"username\":\"pera\",\"password\":\"pera\"}," +
                "\"firstName\":\"Pera\",\"lastName\":\"Peric\",\"birthDate\":\"dgadagag\"," +
                "\"email\":\"peraperic@gmail.com\",\"phoneNumber\":\"0634198131\"}\n";

        this.mockMvc.perform(post(URL_PREFIX)
                .contentType(contentType)
                .content(tenantJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testRegisterTenantInappropriateTypeOfUsername() throws Exception{
        /*
        * Test proverava ispravnost rada metode registerAdmin kontrolera TenantController
        * (registracija novog stanara od strane anonimnog korisnika)
        * u slucaju kada je dolazni DTO objekat nevalidan: poseduje sva polja neohodna za registraciju, ali je polje
         * username nevalidnog tipa
        * */

        String tenantJson = "{'loginAccount':{'username':1,'password':'pera'}," +
                "'firstName':'Pera','lastName':'Peric','birthDate':465684489849561456," +
                "'email':'peraperic@gmail.com','phoneNumber':'0634198131'}\n";

        this.mockMvc.perform(post(URL_PREFIX)
                .contentType(contentType)
                .content(tenantJson))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za updateTenant
    @Test
    @Transactional
    @Rollback(true)
    public void testUpdateTenant() throws Exception {
       /*
        * Test proverava ispravnost rada metode updateTenant kontrolera TenantController
        * (azuriranje konkretnog stanara na osnovu zadatog ID-a)
        * pri cemu u bazi postoji stanar sa ID-em koji je zadat u URL-u
        * */

        TenantUpdateDTO tenantUpdateDTO = new TenantUpdateDTO("Jasmina", "Markovic", "izmenjeno@gmail.com",
                "654111", this.sdf.parse("1995-09-21"), "jasmina", "novi");

        String tenantUpdateDTOJson = TestUtil.json(tenantUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/103")
                .header("Authentication-Token", accessTokenTenant)
                .contentType(contentType)
                .content(tenantUpdateDTOJson))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value("103"))
                .andExpect(jsonPath("$.firstName").value("Jasmina"))
                .andExpect(jsonPath("$.lastName").value("Markovic"))
                .andExpect(jsonPath("$.email").value("izmenjeno@gmail.com"))
                .andExpect(jsonPath("$.phoneNumber").value("654111"));
    }

    @Test
    @Transactional
    @Rollback(true)
    public void testUpdateTenantMissingNewPassAndCurrentPass() throws Exception {
       /*
        * Test proverava ispravnost rada metode updateTenant kontrolera TenantController
        * (azuriranje konkretnog stanara na osnovu zadatog ID-a)
        * pri cemu dolazni DTO objekat validan: ne poseduje polja currentPassword i newPassword - azuriraju se samo ime, prezime, datum rodjenja,
        *broj telefona i email
        * */

        TenantUpdateDTO tenantUpdateDTO = new TenantUpdateDTO();
        tenantUpdateDTO.setFirstName("Jasmina");
        tenantUpdateDTO.setLastName("Doncic");
        tenantUpdateDTO.setBirthDate(this.sdf.parse("1995-09-21"));
        tenantUpdateDTO.setEmail("izmenjeno@gmail.com");
        tenantUpdateDTO.setPhoneNumber("654111");

        String tenantUpdateDTOJson = TestUtil.json(tenantUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/103")
                .header("Authentication-Token", accessTokenTenant)
                .contentType(contentType)
                .content(tenantUpdateDTOJson))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value("103"))
                .andExpect(jsonPath("$.firstName").value("Jasmina"))
                .andExpect(jsonPath("$.lastName").value("Doncic"))
                .andExpect(jsonPath("$.email").value("izmenjeno@gmail.com"))
                .andExpect(jsonPath("$.phoneNumber").value("654111"));
    }

    @Test
    public void testUpdateTenantByNotAuthenticatedUser() throws Exception {
       /*
        * Test proverava ispravnost rada metode updateTenant kontrolera TenantController
        * (azuriranje konkretnog stanara na osnovu zadatog ID-a)
        * ali azuriranje resursa pokusava da izvrsi korisnik koji nije ulogovan
        * */

        TenantUpdateDTO tenantUpdateDTO = new TenantUpdateDTO("Jasmina", "Markovic", "izmenjeno@gmail.com",
                "654111", this.sdf.parse("1995-09-21"), "jasmina", "novi");

        String tenantUpdateDTOJson = TestUtil.json(tenantUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/103")
                .contentType(contentType)
                .content(tenantUpdateDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testUpdateTenantByNotAuthenticatedAdmin() throws Exception {
       /*
        * Test proverava ispravnost rada metode updateTenant kontrolera TenantController
        * (azuriranje konkretnog stanara na osnovu zadatog ID-a)
        * ali azuriranje resursa pokusava da izvrsi ulogovani korisnik koji nema ulogu stanara, vec administratora
        * */

        TenantUpdateDTO tenantUpdateDTO = new TenantUpdateDTO("Jasmina", "Markovic", "izmenjeno@gmail.com",
                "654111", this.sdf.parse("1995-09-21"), "jasmina", "novi");

        String tenantUpdateDTOJson = TestUtil.json(tenantUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/103")
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(tenantUpdateDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testUpdateTenantNonexistentTenant() throws Exception {
       /*
        * Test proverava ispravnost rada metode updateTenant kontrolera TenantController
        * (azuriranje konkretnog stanara na osnovu zadatog ID-a)
        * pri cemu u bazi ne postoji stanar sa ID-em koji je zadat u URL-u
        * */

        TenantUpdateDTO tenantUpdateDTO = new TenantUpdateDTO("Jasmina", "Markovic", "izmenjeno@gmail.com",
                "654111", this.sdf.parse("1995-09-21"), "jasmina", "novi");

        String tenantUpdateDTOJson = TestUtil.json(tenantUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/200")
                .header("Authentication-Token", accessTokenTenant)
                .contentType(contentType)
                .content(tenantUpdateDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testUpdateTenantNotCurrentTenant() throws Exception {
       /*
        * Test proverava ispravnost rada metode updateTenant kontrolera TenantController
        * (azuriranje konkretnog stanara na osnovu zadatog ID-a)
        * pri cemu u bazi postoji stanar sa ID-em koji je zadat u URL-u, ali to nije ujedno i ID
        * trenutno ulogovanog stanara
        * */

        TenantUpdateDTO tenantUpdateDTO = new TenantUpdateDTO("Jasmina", "Markovic", "izmenjeno@gmail.com",
                "654111", this.sdf.parse("1995-09-21"), "jasmina", "novi");

        String tenantUpdateDTOJson = TestUtil.json(tenantUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/104")
                .header("Authentication-Token", accessTokenTenant)
                .contentType(contentType)
                .content(tenantUpdateDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testUpdateTenantMissingFirstName() throws Exception {
       /*
        * Test proverava ispravnost rada metode updateTenant kontrolera TenantController
        * (azuriranje konkretnog stanara na osnovu zadatog ID-a)
        * pri cemu dolazni DTO objekat nevalidan: ne poseduje sva polja - nedostaje polje firstName
        * */

        TenantUpdateDTO tenantUpdateDTO = new TenantUpdateDTO();
        tenantUpdateDTO.setLastName("Doncic");
        tenantUpdateDTO.setBirthDate(this.sdf.parse("1995-09-21"));
        tenantUpdateDTO.setEmail("izmenjeno@gmail.com");
        tenantUpdateDTO.setPhoneNumber("654111");
        tenantUpdateDTO.setCurrentPassword("jasmina");
        tenantUpdateDTO.setNewPassword("novi");

        String tenantUpdateDTOJson = TestUtil.json(tenantUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/103")
                .header("Authentication-Token", accessTokenTenant)
                .contentType(contentType)
                .content(tenantUpdateDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateTenantMissingLastName() throws Exception {
       /*
        * Test proverava ispravnost rada metode updateTenant kontrolera TenantController
        * (azuriranje konkretnog stanara na osnovu zadatog ID-a)
        * pri cemu dolazni DTO objekat nevalidan: ne poseduje sva polja - nedostaje polje lastName
        * */

        TenantUpdateDTO tenantUpdateDTO = new TenantUpdateDTO();
        tenantUpdateDTO.setFirstName("Jasmina");
        tenantUpdateDTO.setBirthDate(this.sdf.parse("1995-09-21"));
        tenantUpdateDTO.setEmail("izmenjeno@gmail.com");
        tenantUpdateDTO.setPhoneNumber("654111");
        tenantUpdateDTO.setCurrentPassword("jasmina");
        tenantUpdateDTO.setNewPassword("novi");

        String tenantUpdateDTOJson = TestUtil.json(tenantUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/103")
                .header("Authentication-Token", accessTokenTenant)
                .contentType(contentType)
                .content(tenantUpdateDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateTenantMissingBirthDate() throws Exception {
       /*
        * Test proverava ispravnost rada metode updateTenant kontrolera TenantController
        * (azuriranje konkretnog stanara na osnovu zadatog ID-a)
        * pri cemu dolazni DTO objekat nevalidan: ne poseduje sva polja - nedostaje polje birthDate
        * */

        TenantUpdateDTO tenantUpdateDTO = new TenantUpdateDTO();
        tenantUpdateDTO.setFirstName("Jasmina");
        tenantUpdateDTO.setLastName("Doncic");
        tenantUpdateDTO.setEmail("izmenjeno@gmail.com");
        tenantUpdateDTO.setPhoneNumber("654111");
        tenantUpdateDTO.setCurrentPassword("jasmina");
        tenantUpdateDTO.setNewPassword("novi");

        String tenantUpdateDTOJson = TestUtil.json(tenantUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/103")
                .header("Authentication-Token", accessTokenTenant)
                .contentType(contentType)
                .content(tenantUpdateDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateTenantMissingEmail() throws Exception {
       /*
        * Test proverava ispravnost rada metode updateTenant kontrolera TenantController
        * (azuriranje konkretnog stanara na osnovu zadatog ID-a)
        * pri cemu dolazni DTO objekat nevalidan: ne poseduje sva polja - nedostaje polje email
        * */

        TenantUpdateDTO tenantUpdateDTO = new TenantUpdateDTO();
        tenantUpdateDTO.setFirstName("Jasmina");
        tenantUpdateDTO.setLastName("Doncic");
        tenantUpdateDTO.setBirthDate(this.sdf.parse("1995-09-21"));
        tenantUpdateDTO.setPhoneNumber("654111");
        tenantUpdateDTO.setCurrentPassword("jasmina");
        tenantUpdateDTO.setNewPassword("novi");

        String tenantUpdateDTOJson = TestUtil.json(tenantUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/103")
                .header("Authentication-Token", accessTokenTenant)
                .contentType(contentType)
                .content(tenantUpdateDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateTenantMissingPhoneNumber() throws Exception {
       /*
        * Test proverava ispravnost rada metode updateTenant kontrolera TenantController
        * (azuriranje konkretnog stanara na osnovu zadatog ID-a)
        * pri cemu dolazni DTO objekat nevalidan: ne poseduje sva polja - nedostaje polje phoneNumber
        * */

        TenantUpdateDTO tenantUpdateDTO = new TenantUpdateDTO();
        tenantUpdateDTO.setFirstName("Jasmina");
        tenantUpdateDTO.setLastName("Doncic");
        tenantUpdateDTO.setBirthDate(this.sdf.parse("1995-09-21"));
        tenantUpdateDTO.setEmail("izmenjeno@gmail.com");
        tenantUpdateDTO.setCurrentPassword("jasmina");
        tenantUpdateDTO.setNewPassword("novi");

        String tenantUpdateDTOJson = TestUtil.json(tenantUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/103")
                .header("Authentication-Token", accessTokenTenant)
                .contentType(contentType)
                .content(tenantUpdateDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateTenantMissingCurrentPassButNotNewPass() throws Exception {
       /*
        * Test proverava ispravnost rada metode updateTenant kontrolera TenantController
        * (azuriranje konkretnog stanara na osnovu zadatog ID-a)
        * pri cemu dolazni DTO objekat nevalidan: ne poseduje sva polja - nedostaje polje currentPassword
        * */

        TenantUpdateDTO tenantUpdateDTO = new TenantUpdateDTO();
        tenantUpdateDTO.setFirstName("Jasmina");
        tenantUpdateDTO.setLastName("Doncic");
        tenantUpdateDTO.setBirthDate(this.sdf.parse("1995-09-21"));
        tenantUpdateDTO.setEmail("izmenjeno@gmail.com");
        tenantUpdateDTO.setPhoneNumber("654111");
        tenantUpdateDTO.setNewPassword("novi");

        String tenantUpdateDTOJson = TestUtil.json(tenantUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/103")
                .header("Authentication-Token", accessTokenTenant)
                .contentType(contentType)
                .content(tenantUpdateDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateTenantMissingNewPassButNotCurrentPass() throws Exception {
       /*
        * Test proverava ispravnost rada metode updateTenant kontrolera TenantController
        * (azuriranje konkretnog stanara na osnovu zadatog ID-a)
        * pri cemu dolazni DTO objekat nevalidan: ne poseduje sva polja - nedostaje polje newPassword
        * */

        TenantUpdateDTO tenantUpdateDTO = new TenantUpdateDTO();
        tenantUpdateDTO.setFirstName("Jasmina");
        tenantUpdateDTO.setLastName("Doncic");
        tenantUpdateDTO.setBirthDate(this.sdf.parse("1995-09-21"));
        tenantUpdateDTO.setEmail("izmenjeno@gmail.com");
        tenantUpdateDTO.setPhoneNumber("654111");
        tenantUpdateDTO.setCurrentPassword("jasmina");

        String tenantUpdateDTOJson = TestUtil.json(tenantUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/103")
                .header("Authentication-Token", accessTokenTenant)
                .contentType(contentType)
                .content(tenantUpdateDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateTenantInappropriateTypeOfFirstName() throws Exception {
       /*
        * Test proverava ispravnost rada metode updateTenant kontrolera TenantController
        * (azuriranje konkretnog stanara na osnovu zadatog ID-a)
        * pri cemu dolazni DTO objekat nevalidan: poseduje sva polja ali je tip podatka
        * u polju ime nevalidan (nije String nego int)
        * */

        String tenantUpdateDTOJson = "{" +
                    "firstName:1," +
                    "lastName:'Doncic'," +
                    "email:'izmenjeno@gmail.com'" +
                    "phoneNumber:'654111'" +
                    "birthDate:'1995-09-21'" +
                    "currentPassword:'jasmina'," +
                    "newPassword:'novi'" +
                "}";

        this.mockMvc.perform(put(URL_PREFIX + "/103")
                .header("Authentication-Token", accessTokenTenant)
                .contentType(contentType)
                .content(tenantUpdateDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateTenantInvalidCurrentPass() throws Exception {
       /*
        * Test proverava ispravnost rada metode updateTenant kontrolera TenantController
        * (azuriranje konkretnog stanara na osnovu zadatog ID-a)
        * pri cemu dolazni DTO objekat nevalidan: poseduje sva polja ali je vrednost
        * currentPassword nevalidna
        * */

        TenantUpdateDTO tenantUpdateDTO = new TenantUpdateDTO("Jasmina", "Markovic", "izmenjeno@gmail.com",
                "654", this.sdf.parse("1995-09-21"), "nevalidno", "novi");

        String tenantUpdateDTOJson = TestUtil.json(tenantUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/103")
                .header("Authentication-Token", accessTokenTenant)
                .contentType(contentType)
                .content(tenantUpdateDTOJson))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za setTenantAsCouncilMember
    @Test
    public void testSetTenantAsCouncilMemberByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode setTenantAsCouncilMember kontrolera TenantController
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        TenantMemberCouncilDTO tenantMemberCouncilDTO = new TenantMemberCouncilDTO(105L);

        String councilMemberDTOJson = TestUtil.json(tenantMemberCouncilDTO);

        this.mockMvc.perform(patch("/api/tenants/buildings/100/apartments/102/council_member")
                .contentType(contentType)
                .content(councilMemberDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSetTenantAsCouncilMemberNonexistentBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode setTenantAsCouncilMember kontrolera TenantController
        * u slucaju kada ne postoji zgrada sa zadatim ID-em
        * */

        TenantMemberCouncilDTO tenantMemberCouncilDTO = new TenantMemberCouncilDTO(105L);

        String councilMemberDTOJson = TestUtil.json(tenantMemberCouncilDTO);

        // ne postoji zgrada sa ID-em = 200
        this.mockMvc.perform(patch("/api/tenants/buildings/200/apartments/102/council_member")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(councilMemberDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testSetTenantAsCouncilMemberNonexistentApartment() throws Exception {
        /*
        * Test proverava ispravnost rada metode setTenantAsCouncilMember kontrolera TenantController
        * u slucaju kada ne postoji stan sa zadatim ID-em
        * */

        TenantMemberCouncilDTO tenantMemberCouncilDTO = new TenantMemberCouncilDTO(105L);

        String councilMemberDTOJson = TestUtil.json(tenantMemberCouncilDTO);

        // ne postoji stan sa ID-em = 200
        this.mockMvc.perform(patch("/api/tenants/buildings/100/apartments/200/council_member")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(councilMemberDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testSetTenantAsCouncilMemberCurrentTenantFromInvalidApartment() throws Exception {
        /*
        * Test proverava ispravnost rada metode setTenantAsCouncilMember kontrolera TenantController
        * u slucaju kada trenutno logovani stanar ne zivi u stanu sa zadatim ID-em
        * */

        TenantMemberCouncilDTO tenantMemberCouncilDTO = new TenantMemberCouncilDTO(105L);

        String councilMemberDTOJson = TestUtil.json(tenantMemberCouncilDTO);

        // stanar sa ID-em = 101 (korisnicko ime = kaca, lozinka = kaca) ne zivi u stanu sa ID-em = 102
        this.mockMvc.perform(patch("/api/tenants/buildings/100/apartments/102/council_member")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(councilMemberDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testSetTenantAsCouncilMemberMissingTenantId() throws Exception {
        /*
        * Test proverava ispravnost rada metode setTenantAsCouncilMember kontrolera TenantController
        * u slucaju kada je dolazni DTO objekat nevalidan: ne sadrzi sva neohodna polja - nedostaje polje tenantId
        * */

        // nedostaje vrednost polja tenantId
        TenantMemberCouncilDTO tenantMemberCouncilDTO = new TenantMemberCouncilDTO(null);

        String councilMemberDTOJson = TestUtil.json(tenantMemberCouncilDTO);

        this.mockMvc.perform(patch("/api/tenants/buildings/100/apartments/102/council_member")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(councilMemberDTOJson))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za confirmTenant
    @Test
    @Transactional
    @Rollback(true)
    public void testConfirmTenant() throws Exception{
        /*
        * Test proverava ispravnost rada metode confirmTenant kontrolera TenantController
        * (potvrdjivanje registracije stanara)
        * */

        this.mockMvc.perform(patch(URL_PREFIX + "/109/confirm")
                .header("Authentication-Token", this.accessTokenAdmin))
                .andExpect(status().isOk());
    }

    @Test
    public void testConfirmByNotAuthenticatedUser() throws Exception{
        /*
        * Test proverava ispravnost rada metode confirmTenant kontrolera TenantController
        * (potvrdjivanje registracije stanara)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        this.mockMvc.perform(patch(URL_PREFIX + "/109/confirm"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testConfirmTenantNonexistentTenant() throws Exception{
        /*
        * Test proverava ispravnost rada metode confirmTenant kontrolera TenantController
        * (potvrdjivanje registracije stanara)
        * u slucaju kada ne postoji stanar sa zadatim ID-em
        * */

        // ne postoji stanar sa ID-em = 200
        this.mockMvc.perform(patch(URL_PREFIX + "/200/confirm")
                .header("Authentication-Token", this.accessTokenAdmin))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testConfirmTenantAlreadyConfirmed() throws Exception{
        /*
        * Test proverava ispravnost rada metode confirmTenant kontrolera TenantController
        * (potvrdjivanje registracije stanara)
        * u slucaju kada je registracija stanara sa zadatim ID-em vec potvrdjena
        * */

        // registracija stanara sa ID-em = 106 je vec potvrdjena
        this.mockMvc.perform(patch(URL_PREFIX + "/106/confirm")
                .header("Authentication-Token", this.accessTokenAdmin))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za getTenantsInMyBuilding
    @Test
    public void testGetTenantsInMyBuilding() throws Exception{
        /*
        * Test proverava ispravnost rada metode getTenantsInMyBuilding kontrolera TenantController
        * (dobavljanje liste svih stanara u zgradi trenutno ulogovanog stanara koji se pretrazuju
        * na osnovu imena ili prezimena ili korisnickog imena)
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/tenants_in_building")
                .header("Authentication-Token", accessTokenTenant)
                .param("apartmentId", "102")
                .param("username", "nikola")
                .param("name", "")
                .param("lastName", ""))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetTenantsInMyBuildingByNotAuthenticatedUser() throws Exception{
        /*
        * Test proverava ispravnost rada metode getTenantsInMyBuilding kontrolera TenantController
        * (dobavljanje liste svih stanara u zgradi trenutno ulogovanog stanara koji se pretrazuju
        * na osnovu imena ili prezimena ili korisnickog imena)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/tenants_in_building")
                .param("apartmentId", "102")
                .param("username", "nikola")
                .param("name", "")
                .param("lastName", ""))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetTenantsInMyBuildingByNotAuthenticatedTenant() throws Exception{
        /*
        * Test proverava ispravnost rada metode getTenantsInMyBuilding kontrolera TenantController
        * (dobavljanje liste svih stanara u zgradi trenutno ulogovanog stanara koji se pretrazuju
        * na osnovu imena ili prezimena ili korisnickog imena)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu stanara
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/tenants_in_building")
                .header("Authentication-Token", accessTokenAdmin)
                .param("apartmentId", "102")
                .param("username", "nikola")
                .param("name", "")
                .param("lastName", ""))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetTenantsInMyBuildingNonexistentApartment() throws Exception{
        /*
        * Test proverava ispravnost rada metode getTenantsInMyBuilding kontrolera TenantController
        * (dobavljanje liste svih stanara u zgradi trenutno ulogovanog stanara koji se pretrazuju
        * na osnovu imena ili prezimena ili korisnickog imena)
        * u slucaju kada ne postoji stan sa zadatim ID-em
        * */

        // ne postoji stan sa ID-em = 200
        this.mockMvc.perform(get(URL_PREFIX + "/tenants_in_building")
                .header("Authentication-Token", accessTokenTenant)
                .param("apartmentId", "200")
                .param("username", "nikola")
                .param("name", "")
                .param("lastName", ""))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetTenantsInMyBuildingTenantWithoutApartment() throws Exception{
        /*
        * Test proverava ispravnost rada metode getTenantsInMyBuilding kontrolera TenantController
        * (dobavljanje liste svih stanara u zgradi trenutno ulogovanog stanara koji se pretrazuju
        * na osnovu imena ili prezimena ili korisnickog imena)
        * u slucaju kada trenutno logovani stanar nije povezan ni sa jednim stanom
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/tenants_in_building")
                .header("Authentication-Token", accessTokenTenant2)
                .param("apartmentId", "102")
                .param("username", "nikola")
                .param("name", "")
                .param("lastName", ""))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetTenantsNotInMyBuildingTenantFromInvalidApartment() throws Exception{
        /*
        * Test proverava ispravnost rada metode getTenantsInMyBuilding kontrolera TenantController
        * (dobavljanje liste svih stanara u zgradi trenutno ulogovanog stanara koji se pretrazuju
        * na osnovu imena ili prezimena ili korisnickog imena)
        * u slucaju kada trenutno logovani stanar ne zivi u stanu ciji je ID poslat kao parametar
        * */

        // stanar sa ID-em = 103 (trenutno logovani stanar) zivi u stanu sa ID-em = 101 (a ne sa ID-em = 103)
        this.mockMvc.perform(get(URL_PREFIX + "/tenants_in_building")
                .header("Authentication-Token", accessTokenTenant)
                .param("apartmentId", "103")
                .param("username", "nikola")
                .param("name", "")
                .param("lastName", ""))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetTenantsInMyBuildingMissingApartmentId() throws Exception{
        /*
        * Test proverava ispravnost rada metode getTenantsInMyBuilding kontrolera TenantController
        * (dobavljanje liste svih stanara u zgradi trenutno ulogovanog stanara koji se pretrazuju
        * na osnovu imena ili prezimena ili korisnickog imena)
        * u slucaju kada nedostaje parametar apartmentId
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/tenants_in_building")
                .header("Authentication-Token", accessTokenTenant)
                .param("apartmentId", "")
                .param("username", "nikola")
                .param("name", "")
                .param("lastName", ""))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testGetTenantsInMyBuildingNoParameters() throws Exception{
        /*
        * Test proverava ispravnost rada metode getTenantsInMyBuilding kontrolera TenantController
        * (dobavljanje liste svih stanara u zgradi trenutno ulogovanog stanara koji se pretrazuju
        * na osnovu imena ili prezimena ili korisnickog imena)
        * u slucaju kada nedostaju svi parametri osim apartmentId-a
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/tenants_in_building")
                .header("Authentication-Token", accessTokenTenant2)
                .param("apartmentId", "102")
                .param("username", "")
                .param("name", "")
                .param("lastName", ""))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testTenantsInMyBuildingInappropriateTypeOfApartmentId() throws Exception{
        /*
        * Test proverava ispravnost rada metode tenantsInMyBuilding kontrolera TenantController
        * (dobavljanje liste svih stanara u zgradi trenutno ulogovanog stanara koji se pretrazuju
        * na osnovu imena ili prezimena ili korisnickog imena)
        * u slucaju kada je prosledjen string umesto long-a kao parametar apartmentId-a
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/tenants_in_building")
                .header("Authentication-Token", accessTokenTenant)
                .param("apartmentId", "fsaz")
                .param("username", "")
                .param("name", "")
                .param("lastName", ""))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za getTenantsByNameOrUsername
    @Test
    public void testGetTenantsByNameOrUsername() throws Exception{
        /*
        * Test proverava ispravnost rada metode getTenantsByNameOrUsername kontrolera TenantController
        * (dobavljanje liste svih stanara na osnovu imena ili prezimena ili korisnickog imena)
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/tenants_by_username")
                .header("Authentication-Token", accessTokenAdmin)
                .param("username", "nikola")
                .param("name", "")
                .param("lastName", ""))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetTenantsByNameOrUsernameByNotAuthenticatedUser() throws Exception{
        /*
        * Test proverava ispravnost rada metode getTenantsByNameOrUsername kontrolera TenantController
        * (dobavljanje liste svih stanara na osnovu imena ili prezimena ili korisnickog imena)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/tenants_by_username")
                .param("username", "nikola")
                .param("name", "")
                .param("lastName", ""))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetTenantsByNameOrUsernameNotAdmin() throws Exception{
        /*
        * Test proverava ispravnost rada metode getTenantsByNameOrUsername kontrolera TenantController
        * (dobavljanje liste svih stanara na osnovu imena ili prezimena ili korisnickog imena)
        * u slucaju kada akciju vrsi logovani korisnik koji nema ulogu administratora
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/tenants_by_username")
                .header("Authentication-Token", accessTokenTenant)
                .param("username", "nikola")
                .param("name", "")
                .param("lastName", ""))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetTenantsByNameOrUsernameNoParameters() throws Exception{
        /*
        * Test proverava ispravnost rada metode getTenantsByNameOrUsername kontrolera TenantController
        * (dobavljanje liste svih stanara na osnovu imena ili prezimena ili korisnickog imena)
        * u slucaju kada nedostaju parametri
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/tenants_by_username")
                .header("Authentication-Token", accessTokenAdmin)
                .param("username", "")
                .param("name", "")
                .param("lastName", ""))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za getTenantsByBuildingId
    @Test
    public void testGetTenantsByBuildingId() throws Exception{
        /*
        * Test proverava ispravnost metode getTenantsByBuildingId kontolera TenantController
        * (dobavlja sve stanara jedne zgrade)
        * kada je prosledjen dobar id zgrade i kada je ulogovan administrator
        * */

        mockMvc.perform(get(URL_PREFIX + "/tenants_by_building_id")
                .param("buildingId", "102")
                .params(params)
                .header("Authentication-Token", accessTokenAdmin))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetTenantsByBuildingIdByUnauthorizedUser() throws Exception{
        /*
        * Test proverava ispravnost metode getTenantsByBuildingId kontolera TenantController
        * (dobavlja sve stanara jedne zgrade)
        * kada je prosledjen dobar id zgrade i kada korisnik nije ovlascen
        * */

        mockMvc.perform(get(URL_PREFIX + "/tenants_by_building_id")
                .param("buildingId", "102")
                .params(params)
                .header("Authentication-Token", accessTokenTenant))
                .andExpect(status().isForbidden());
    }

    // TESTOVI za getCouncilMembers
    @Test
    public void testGetCouncilMembersByTenant() throws Exception{
        /*
        * Test proverava ispravnost rada metode getCouncilMembers kontrolera TenantController
        * (dobavljanje liste svih stanara koji zive u istom stanu kao i trenutno logovani stanar
        * kako bi se izabrao novi stanar koji je predstavnik skupstine stanara za taj stan u datoj zgradi)
        * */

        /*
        sa stanarom ciji je ID=104 (koji je trenutni predstavnik stana sa ID-em 102 u skupstini stanara)
        zive i stanari:
            1 - id = 105
                firstName = Isidora
                lastName = Krajinovic
            2 - id = 106
                firstName = Jasmina
                lastName = Markovic
        */
        this.mockMvc.perform(get("/api/buildings/100/council/members")
                .params(params)
                .header("Authentication-Token", this.accessTokenTenant3))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[*]", hasSize(2)))
                .andExpect(jsonPath("$.[0].id").value(105))
                .andExpect(jsonPath("$.[0].firstName").value("Isidora"))
                .andExpect(jsonPath("$.[0].lastName").value("Krajinovic"))
                .andExpect(jsonPath("$.[1].id").value(106))
                .andExpect(jsonPath("$.[1].firstName").value("Jasmina"))
                .andExpect(jsonPath("$.[1].lastName").value("Markovic"));
    }
}