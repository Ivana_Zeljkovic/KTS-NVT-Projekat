package rs.ac.uns.ftn.ktsnvt.service;

import org.springframework.data.domain.Page;
import rs.ac.uns.ftn.ktsnvt.model.entity.Building;
import rs.ac.uns.ftn.ktsnvt.model.entity.DamageResponsibility;

import java.util.List;

/**
 * Created by Ivana Zeljkovic on 23/11/2017.
 */
public interface DamageResponsibilityService {

    DamageResponsibility findOneByDamageTypeAndTenantAndBuilding(Long damageTypeId, Long tenantId, Long buildingId);

    DamageResponsibility findOneByDamageTypeAndInstitutionAndBuilding(Long damageTypeId, Long institutionId, Long buildingId);

    Page<DamageResponsibility>  findAllByInstitutionId(Long institutionId, Integer pageNumber,
                                                       Integer pageSize, String sortDirection, String sortProperty);

    DamageResponsibility findOneByDamageTypeAndBuilding(Long damageTypeId, Long buildingId);

    void remove(Long id);

    DamageResponsibility save(DamageResponsibility damageResponsibility);

    List<DamageResponsibility> findAllByBuildingId(Building building);
}
