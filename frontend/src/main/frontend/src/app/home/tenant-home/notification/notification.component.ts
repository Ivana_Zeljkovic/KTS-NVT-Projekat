import { Component, EventEmitter, Input, Output } from '@angular/core';
import { BsModalRef } from "ngx-bootstrap";
import * as _ from "lodash";
// model
import { NotificationCreateUpdate } from "../../../shared/models/notification-create-update";
import { Notification} from "../../../shared/models/notification";
// service
import { NotificationService } from "../../../core/services/notification.service";
import { JwtService } from "../../../core/services/jwt.service";
import { ToasterService } from "angular5-toaster/dist";
// error
import { AppError } from "../../../shared/errors/app-error";
import { BadRequestError } from "../../../shared/errors/bad-request-error";
import { ForbiddenError } from "../../../shared/errors/forbidden-error";
import { NotFoundError } from "../../../shared/errors/not-found-error";


@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent {
  @Input() model: NotificationCreateUpdate;
  @Input() initialValues: NotificationCreateUpdate;
  @Input() notificationID: number;
  @Input() update: boolean;
  @Output() submited = new EventEmitter<Notification>();
  disableSaveAndReset: boolean;


  constructor(public modalRef: BsModalRef, private notificationService: NotificationService,
              private jwtService: JwtService, private toasterService: ToasterService) {
    this.disableSaveAndReset = true;
  }

  onSubmit() {
    let buildingID = this.jwtService.getBuildingIdFromToken();

    if(!this.update) {
      this.notificationService.save(this.model, 'buildings', buildingID, 'billboard/notifications')
        .subscribe(newNotification => {
          this.submited.emit(newNotification);
          this.modalRef.hide();
        }, (error: AppError) => {
          if(error instanceof BadRequestError)
            this.toasterService.pop('error', 'Error', 'Given data have bad format');
          else if(error instanceof ForbiddenError)
            this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action');
          else if(error instanceof NotFoundError)
            this.toasterService.pop('error', 'Error', 'Building not found');
          else {
            this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
            throw error;
          }
        });
    }
    else {
      this.notificationService.update(this.notificationID, this.model, 'buildings', buildingID, 'billboard/notifications')
        .subscribe(updatedNotification => {
          this.submited.emit(updatedNotification);
          this.modalRef.hide();
        }, (error: AppError) => {
          if(error instanceof BadRequestError)
            this.toasterService.pop('error', 'Error', 'Given data have bad format/Selected notification doesn\'t belong to selected building');
          else if(error instanceof ForbiddenError)
            this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action');
          else if(error instanceof NotFoundError)
            this.toasterService.pop('error', 'Error', 'Building not found/Selected notification doesn\'t belong to this building');
          else {
            this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
            throw error;
          }
        });
    }
  }

  reset() {
    this.model = _.cloneDeep(this.initialValues);
    this.disableSaveAndReset = true;
  }

  changeExist(newValues) {
    this.disableSaveAndReset = (JSON.stringify(newValues) === JSON.stringify(this.initialValues));
  }
}
