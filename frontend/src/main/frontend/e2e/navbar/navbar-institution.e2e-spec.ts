
import {NavbarPage} from "../page-objects/navbar.po";
import {LoginPage} from "../page-objects/login.po";
import {browser} from "protractor";

describe('Navbar for institution', () => {

  let navbarPage: NavbarPage;
  let loginPage: LoginPage;

  beforeEach(() => {
    loginPage = new LoginPage();
    navbarPage = new NavbarPage();

    browser.get('/');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');

    loginPage.setUsername('inst1');
    loginPage.setPassword('inst1');
    loginPage.getSignInButton().click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/home');
  });

  afterEach(() => {
    browser.executeScript('window.localStorage.clear();');
  });

  it('should display navbar when institution is logged in on home page', () => {
    expect(navbarPage.getHomeLink().isDisplayed()).toBe(true);
    expect(navbarPage.getResponsibilityDropdownLink().isDisplayed()).toBe(true);
    expect(navbarPage.getDamageRequests().isDisplayed()).toBe(true);
  });

  it('should go to damage requests list when clicked on buildings link in navbar', () => {
    navbarPage.getDamageRequests().click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/damage-requests');
  });


  it('should go to  damages im responsible for when clicked on a link', () => {
    navbarPage.getResponsibilityDropdownLink().click().then(function () {
      navbarPage.getDamagesResponsibleForInResponsibilityLink().click();
      expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/damages');
    });
  });
});
