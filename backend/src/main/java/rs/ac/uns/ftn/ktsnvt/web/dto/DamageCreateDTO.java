package rs.ac.uns.ftn.ktsnvt.web.dto;


import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by Ivana Zeljkovic on 24/11/2017.
 */
public class DamageCreateDTO implements Serializable {

    @NotNull
    @NotEmpty
    private String description;
    @NotNull
    @NotEmpty
    private String typeName;
    private Long apartmentId;
    @NotNull
    private boolean urgent;
    private String image;

    public DamageCreateDTO() { }

    public DamageCreateDTO(String description, String typeName, Long apartmentId,
                           boolean urgent) {
        this.description = description;
        this.typeName = typeName;
        this.apartmentId = apartmentId;
        this.urgent = urgent;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public Long getApartmentId() {
        return apartmentId;
    }

    public void setApartmentId(Long apartmentId) {
        this.apartmentId = apartmentId;
    }

    public boolean isUrgent() { return urgent; }

    public void setUrgent(boolean urgent) { this.urgent = urgent; }

    public String getImage() { return image; }

    public void setImage(String image) { this.image = image; }
}
