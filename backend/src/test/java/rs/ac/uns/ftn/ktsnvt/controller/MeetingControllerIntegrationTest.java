package rs.ac.uns.ftn.ktsnvt.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;
import rs.ac.uns.ftn.ktsnvt.controller.util.TestUtil;
import rs.ac.uns.ftn.ktsnvt.web.dto.LoginDTO;
import rs.ac.uns.ftn.ktsnvt.web.dto.MeetingCreateDTO;
import rs.ac.uns.ftn.ktsnvt.web.dto.TokenDTO;

import javax.annotation.PostConstruct;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;

import static org.hamcrest.Matchers.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Created by Nikola Garabandic on 06/12/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class MeetingControllerIntegrationTest {

    private static final String URL_PREFIX = "/api/buildings";

    private MediaType contentType = new MediaType(
            MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    private MockMvc mockMvc;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja administratora
    private String accessTokenPresident;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja stanara
    private String accessTokenTenant;
    private MultiValueMap<String, String> params;


    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private TestRestTemplate testRestTemplate;


    @PostConstruct
    public void setup() {
        this.mockMvc = MockMvcBuilders.
                webAppContextSetup(webApplicationContext).apply(springSecurity()).build();
    }

    // pre izvrsavanja testa, neophodno je izvrsiti logovanje u aplikaciju da bismo dobili token
    @Before
    public void login() throws Exception {
        // logovanje predsednika skupstine stanara
        LoginDTO loginDTOPresident = new LoginDTO("kaca", "kaca");

        ResponseEntity<TokenDTO> responseEntityPresident =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOPresident,
                        TokenDTO.class);
        // preuzmemo token za logovanog predsednika iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenPresident = responseEntityPresident.getBody().getValue();


        // logovanje stanara
        LoginDTO loginDTOTenant = new LoginDTO("jasmina", "jasmina");

        ResponseEntity<TokenDTO> responseEntityTenant =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOTenant,
                        TokenDTO.class);
        // preuzmemo token za logovanog stanara iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenTenant = responseEntityTenant.getBody().getValue();

        // za pageable atribut
        params = TestUtil.getParams(
                "0", "10", "ASC", "id", "");
    }

    // TESTOVI za createMeeting
    @Test
    @Transactional
    @Rollback(true)
    public void testCreateMeeting() throws Exception{
        /*
        * Test proverava ispravnost rada metode createMeeting kontrolera MeetingController
        * (kreiranje sastanka u zgradi sa zadatim ID-em)
        * */

        MeetingCreateDTO meetingCreateDTO = new MeetingCreateDTO(new Date(),
                45, new ArrayList<Long>(){{add(102L);}});

        String meetingJson = TestUtil.json(meetingCreateDTO);

        /*
        tacka dnevnog reda sa ID-em = 102 ima sledece podatke:
            id = 102,
            creator_id = 102,
            content = Third meeting item
         */
        this.mockMvc.perform(post( URL_PREFIX + "/100/council/meetings")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(meetingJson))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.duration").value(45));
    }

    @Test
    public void testCreateMeetingByNotAuthenticatedUser() throws Exception{
        /*
        * Test proverava ispravnost rada metode createMeeting kontrolera MeetingController
        * (kreiranje sastanka u zgradi sa zadatim ID-em)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */
        MeetingCreateDTO meetingCreateDTO = new MeetingCreateDTO(new Date(),
                45, new ArrayList<Long>(){{add(102L);}});

        String meetingJson = TestUtil.json(meetingCreateDTO);

        this.mockMvc.perform(post( URL_PREFIX + "/100/council/meetings")
                .contentType(contentType)
                .content(meetingJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testCreateMeetingByNotAuthenticatedPresident() throws Exception{
        /*
        * Test proverava ispravnost rada metode createMeeting kontrolera MeetingController
        * (kreiranje sastanka u zgradi sa zadatim ID-em)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu predsednika skupstine stanara
        * */
        MeetingCreateDTO meetingCreateDTO = new MeetingCreateDTO(new Date(),
                45, new ArrayList<Long>(){{add(102L);}});

        String meetingJson = TestUtil.json(meetingCreateDTO);

        this.mockMvc.perform(post( URL_PREFIX + "/100/council/meetings")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(meetingJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testCreateMeetingNonexistentBuilding() throws Exception{
        /*
        * Test proverava ispravnost rada metode createMeeting kontrolera MeetingController
        * (kreiranje sastanka u zgradi sa zadatim ID-em)
        * u slucaju kad ane postoji zgrada sa zadatim ID-em
        * */
        MeetingCreateDTO meetingCreateDTO = new MeetingCreateDTO(new Date(),
                45, new ArrayList<Long>(){{add(102L);}});

        String meetingJson = TestUtil.json(meetingCreateDTO);

        // ne postiji zgrada sa ID-em = 200
        this.mockMvc.perform(post( URL_PREFIX + "/200/council/meetings")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(meetingJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testCreateMeetingPresidentFromInvalidBuilding() throws Exception{
        /*
        * Test proverava ispravnost rada metode createMeeting kontrolera MeetingController
        * (kreiranje sastanka u zgradi sa zadatim ID-em)
        * u slucaju kada trenutno logovani predsednik skupstine stanara nije ujedno i predsednik skupstine u zgradi sa zadatim ID-em
        * */

        MeetingCreateDTO meetingCreateDTO = new MeetingCreateDTO(new Date(),
                45, new ArrayList<Long>(){{add(102L);}});

        String meetingJson = TestUtil.json(meetingCreateDTO);

        // stanar sa ID-em = 101 je predsednik skupstine stanara u zgradi sa ID-em = 100 (a ne sa ID-em = 101)
        this.mockMvc.perform(post( URL_PREFIX + "/101/council/meetings")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(meetingJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testCreateMeetingMissingMeetingDate() throws Exception{
        /*
        * Test proverava ispravnost rada metode createMeeting kontrolera MeetingController
        * (kreiranje sastanka u zgradi sa zadatim ID-em)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne sadrzi sva neophodna polja - nedostaje polje meetingDate
        * */

        // nedostaje vrednost polja meetingDate
        MeetingCreateDTO meetingCreateDTO = new MeetingCreateDTO(null,
                45, new ArrayList<Long>(){{add(102L);}});

        String meetingJson = TestUtil.json(meetingCreateDTO);

        this.mockMvc.perform(post( URL_PREFIX + "/100/council/meetings")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(meetingJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateMeetingMissingDuration() throws Exception{
        /*
        * Test proverava ispravnost rada metode createMeeting kontrolera MeetingController
        * (kreiranje sastanka u zgradi sa zadatim ID-em)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne sadrzi sva neophodna polja - nedostaje polje duration
        * */

        // nedostaje vrednost polja duration
        MeetingCreateDTO meetingCreateDTO = new MeetingCreateDTO(new Date(),
                null, new ArrayList<Long>(){{add(102L);}});

        String meetingJson = TestUtil.json(meetingCreateDTO);

        this.mockMvc.perform(post( URL_PREFIX + "/100/council/meetings")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(meetingJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateMeetingMissingListOfMeetingItemsId() throws Exception{
        /*
        * Test proverava ispravnost rada metode createMeeting kontrolera MeetingController
        * (kreiranje sastanka u zgradi sa zadatim ID-em)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne sadrzi sva neophodna polja - nedostaje lista id-eva predloga
        * tacaka dnevnog reda meetingItemsId
        * */

        // nedostaje lista meetingItemsId
        MeetingCreateDTO meetingCreateDTO = new MeetingCreateDTO(new Date(),
                45, null);

        String meetingJson = TestUtil.json(meetingCreateDTO);

        this.mockMvc.perform(post( URL_PREFIX + "/100/council/meetings")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(meetingJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateMeetingListOfMeetingItemsIdIsEmpty() throws Exception{
        /*
        * Test proverava ispravnost rada metode createMeeting kontrolera MeetingController
        * (kreiranje sastanka u zgradi sa zadatim ID-em)
        * u slucaju kada je dolazni DTO objekat nevalidan: sadrzi sva neophodna polja, ali je lista id-eva
        * predloga tacaka dnevnog reda meetingItemsId prazna
        * */

        // lista meetingItemsId je prazna
        MeetingCreateDTO meetingCreateDTO = new MeetingCreateDTO(new Date(),
                45, new ArrayList<>());

        String meetingJson = TestUtil.json(meetingCreateDTO);

        this.mockMvc.perform(post( URL_PREFIX + "/100/council/meetings")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(meetingJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateMeetingNoMeetingItem() throws Exception{
        /*
        * Test proverava ispravnost rada metode createMeeting kontrolera MeetingController
        * (kreiranje sastanka u zgradi sa zadatim ID-em)
        * u slucaju kada je dolazni DTO objekat nevalidan: sadrzi sva neophodna polja, ali za bar jedan element u listi
        * id-eva predloga tacaka dnevnog reda vazi da ne postoji predlog tacke dnevnog reda sa tim ID-em
        * */

        // ne postoji predlog tacke dnevnog reda sa ID-em = 200
        MeetingCreateDTO meetingCreateDTO = new MeetingCreateDTO(new Date(),
                45, new ArrayList<Long>(){{add(200L);}});

        String meetingJson = TestUtil.json(meetingCreateDTO);

        this.mockMvc.perform(post( URL_PREFIX + "/100/council/meetings")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(meetingJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testCreateMeetingMeetingItemAlreadyHasMeeting() throws Exception{
        /*
        * Test proverava ispravnost rada metode createMeeting kontrolera MeetingController
        * (kreiranje sastanka u zgradi sa zadatim ID-em)
        * u slucaju kada je dolazni DTO objekat nevalidan: sadrzi sva neophodna polja, ali za bar jedan element u listi
        * id-eva predloga tacaka dnevnog reda vazi da izabrani predlog tacke dnevnog reda vec pripada nekom drugom sastanku
        * */

        // predlog tacke dnevnog reda sa ID-em = 101 pripada sastanku sa ID-em = 100
        MeetingCreateDTO meetingCreateDTO = new MeetingCreateDTO(new Date(),
                45, new ArrayList<Long>(){{add(101L);}});

        String meetingJson = TestUtil.json(meetingCreateDTO);

        this.mockMvc.perform(post( URL_PREFIX + "/100/council/meetings")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(meetingJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateMeetingNoMeetingItemFromInvalidBuilding() throws Exception{
        /*
        * Test proverava ispravnost rada metode createMeeting kontrolera MeetingController
        * (kreiranje sastanka u zgradi sa zadatim ID-em)
        * u slucaju kada je dolazni DTO objekat nevalidan: sadrzi sva neophodna polja, ali za bar jedan element u listi
        * id-eva predloga tacaka dnevnog reda vazi da izabrani predlog tacke dnevnog reda ne pripada zgradi sa zadatim ID-em
        * */

        // predlog tacke dnevnog reda sa ID-em = 103 pripada zgradi sa ID-em = 101 (a ne sa ID-em = 100)
        MeetingCreateDTO meetingCreateDTO = new MeetingCreateDTO(new Date(),
                45, new ArrayList<Long>(){{add(103L);}});

        String meetingJson = TestUtil.json(meetingCreateDTO);

        this.mockMvc.perform(post( URL_PREFIX + "/100/council/meetings")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(meetingJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateMeetingInvalidMeetingDateFormat() throws Exception{
        /*
        * Test proverava ispravnost rada metode createMeeting kontrolera MeetingController
        * (kreiranje sastanka u zgradi sa zadatim ID-em)
        * */


        MeetingCreateDTO meetingCreateDTO = new MeetingCreateDTO(new Date(),
                45, new ArrayList<Long>(){{add(102L);}});

        String meetingJson = "{'meetingDate':'sfa','duration':45,'meetingItemsId':[102]}";



        this.mockMvc.perform(post( "/api/buildings/100/council/meetings").header
                ("Authentication-Token", this.accessTokenPresident).
                contentType(contentType).
                content(meetingJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateMeetingInvalidDurationFormat() throws Exception{
        /*
        * Test proverava ispravnost rada metode createMeeting kontrolera MeetingController
        * (kreiranje sastanka u zgradi sa zadatim ID-em)
        * */


        MeetingCreateDTO meetingCreateDTO = new MeetingCreateDTO(new Date(),
                45, new ArrayList<Long>(){{add(102L);}});

        String meetingJson = "{'meetingDate':1512501756994,'duration':'45','meetingItemsId':[102]}";



        this.mockMvc.perform(post( "/api/buildings/100/council/meetings").header
                ("Authentication-Token", this.accessTokenPresident).
                contentType(contentType).
                content(meetingJson))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za getMeetings
    @Test
    public void testGetMeetings() throws Exception{
        /*
        * Test proverava ispravnost rada metode getMeetings kontrolera MeetingController
        * (dobavljanje liste svih sastanka u zgradi sa zadatim ID-em koji se jos uvek nisu odrzali)
        * */

        /*
        na nivou zgrade sa ID-em = 100 postoje 2 sastanka koja se jos uvek nisu odrzala:
            1 - id = 101,
                date = 2018-12-01 10:34:09,
                duration = 45,
                council_id = 100
            2 - id = 102,
                date = 2018-11-29 10:00:00,
                duration = 45,
                council_id = 100
         */
        this.mockMvc.perform(get( URL_PREFIX + "/100/council/meetings")
                .header("Authentication-Token", this.accessTokenPresident)
                .params(params))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[*].id", containsInAnyOrder(101, 102)));
    }

    @Test
    public void testGetMeetingsByNotAuthenticatedUser() throws Exception{
        /*
        * Test proverava ispravnost rada metode getMeetings kontrolera MeetingController
        * (dobavljanje liste svih sastanka u zgradi sa zadatim ID-em koji se jos uvek nisu odrzali)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        this.mockMvc.perform(get( URL_PREFIX + "/100/council/meetings")
                .params(params))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetMeetingsNonexistentBuilding() throws Exception{
        /*
        * Test proverava ispravnost rada metode getMeetings kontrolera MeetingController
        * (dobavljanje liste svih sastanka u zgradi sa zadatim ID-em koji se jos uvek nisu odrzali)
        * u slucaju kada ne postoji zgrada sa zadatim ID-em
        * */

        // ne postoji zgrada sa ID-em = 200
        this.mockMvc.perform(get( URL_PREFIX + "/200/council/meetings")
                .header("Authentication-Token", this.accessTokenPresident)
                .params(params))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetMeetingsPresidentFromInvalidBuilding() throws Exception{
        /*
        * Test proverava ispravnost rada metode getMeetings kontrolera MeetingController
        * (dobavljanje liste svih sastanka u zgradi sa zadatim ID-em koji se jos uvek nisu odrzali)
        * u slucaju kada trenutno logovani predsednik skupstine stanara nije ujedno i predsednik skupstine u zgradi sa zadatim ID-em
        * */

        // stanar sa ID-em = 101 je predsednik skupstine u zgradi sa ID-em = 100 (a ne sa ID-em = 101)
        this.mockMvc.perform(get( URL_PREFIX + "/101/council/meetings")
                .header("Authentication-Token", this.accessTokenPresident)
                .params(params))
                .andExpect(status().isForbidden());
    }

    // TESTOVI za getMeeting
    @Test
    public void testGetMeeting() throws Exception{
        /*
        * Test proverava ispravnost rada metode getMeeting kontrolera MeetingController
        * (dobavljanje konkretnog sastanaka sa zadatim ID-em (koji jos uvek nije poceo) na nivou zgrade sa zadatim ID-em)
        * */

        /*
        zgradi sa ID-em = 100 pripada sastanak koji se jos nije odrzao a ima sledece podatke:
            id = 101,
            date = 2018-12-01 10:34:09,
            duration = 45,
            council_id = 100
         */
        this.mockMvc.perform(get( URL_PREFIX + "/100/council/meetings/101")
                .header("Authentication-Token", this.accessTokenPresident))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(101));
    }

    @Test
    public void testGetMeetingByNotAuthenticatedUser() throws Exception{
        /*
        * Test proverava ispravnost rada metode getMeeting kontrolera MeetingController
        * (dobavljanje konkretnog sastanaka sa zadatim ID-em (koji jos uvek nije poceo) na nivou zgrade sa zadatim ID-em)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        this.mockMvc.perform(get( URL_PREFIX + "/100/council/meetings/101"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetMeetingNonexistentBuilding() throws Exception{
        /*
        * Test proverava ispravnost rada metode getMeeting kontrolera MeetingController
        * (dobavljanje konkretnog sastanaka sa zadatim ID-em (koji jos uvek nije poceo) na nivou zgrade sa zadatim ID-em)
        * u slucaju kada ne postoji zgrada sa zadatim ID-em
        * */

        // ne postoji zgrada sa ID-em = 200
        this.mockMvc.perform(get( URL_PREFIX + "/200/council/meetings/101").
                header("Authentication-Token", this.accessTokenPresident))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetMeetingPresidentFromInvalidBuilding() throws Exception{
        /*
        * Test proverava ispravnost rada metode getMeeting kontrolera MeetingController
        * (dobavljanje konkretnog sastanaka sa zadatim ID-em (koji jos uvek nije poceo) na nivou zgrade sa zadatim ID-em)
        * u slucaju kada trenutno logovani predsednik skupstine stanara nije ujedno i predsednik skupstine u zgradi sa zadatim ID-em
        * */

        // stanar sa ID-em = 101 je predsednik skupstine stanara u zgradi sa ID-em = 100 (a ne sa ID-em = 101)
        this.mockMvc.perform(get( URL_PREFIX + "/101/council/meetings/101").
                header("Authentication-Token", this.accessTokenPresident))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetMeetingNonexistentMeeting() throws Exception{
        /*
        * Test proverava ispravnost rada metode getMeeting kontrolera MeetingController
        * (dobavljanje konkretnog sastanaka sa zadatim ID-em (koji jos uvek nije poceo) na nivou zgrade sa zadatim ID-em)
        * u slucaju kada ne postoji sastanak sa zadatim ID-em
        * */

        // ne postoji sastanak sa ID-em = 200
        this.mockMvc.perform(get( URL_PREFIX + "/100/council/meetings/794")
                .header("Authentication-Token", this.accessTokenPresident))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetMeetingInvalidBuilding() throws Exception{
        /*
        * Test proverava ispravnost rada metode getMeeting kontrolera MeetingController
        * (dobavljanje konkretnog sastanaka sa zadatim ID-em (koji jos uvek nije poceo) na nivou zgrade sa zadatim ID-em)
        * u slucaju kada sastanak sa zadatim ID-em ne pripada zgradi sa zadatim ID-em
        * */

        // sastanak sa ID-em = 104 pripada zgradi sa ID-em = 101 (a ne sa ID-em = 100)
        this.mockMvc.perform(get( URL_PREFIX + "/101/council/meetings/104")
                .header("Authentication-Token", this.accessTokenPresident))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetMeetingAlreadyFinished() throws Exception{
        /*
        * Test proverava ispravnost rada metode getMeeting kontrolera MeetingController
        * (dobavljanje konkretnog sastanaka sa zadatim ID-em (koji jos uvek nije poceo) na nivou zgrade sa zadatim ID-em)
        * u slucaju kada se sastanak sa zadatim ID-em vec zavrsio
        * */

        // sastanak sa ID-em = 100 se vec zavrsio
        this.mockMvc.perform(get( URL_PREFIX + "/100/council/meetings/100")
                .header("Authentication-Token", this.accessTokenPresident))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za deleteMeeting
    @Test
    @Transactional
    @Rollback(true)
    public void testDeleteMeeting() throws Exception{
        /*
        * Test proverava ispravnost metode deleteMeeting kontrolera MeetingController
        * (brisanje sastanka ciji je ID zadat)
        * */

        mockMvc.perform(delete(URL_PREFIX + "/100/council/meetings/101")
                .header("Authentication-Token", this.accessTokenPresident))
                .andExpect(status().isOk());
        mockMvc.perform(get(URL_PREFIX + "/100/council/meetings/101")
                .header("Authentication-Token", this.accessTokenPresident))
                .andExpect(status().isNotFound());
    }
}
