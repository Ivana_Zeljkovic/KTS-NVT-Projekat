package rs.ac.uns.ftn.ktsnvt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Offer;
import rs.ac.uns.ftn.ktsnvt.repository.OfferRepository;

import java.util.List;

/**
 * Created by Katarina Cukurov on 24/11/2017.
 */
@Service
public class OfferServiceImpl implements OfferService {

    @Autowired
    OfferRepository offerRepository;


    @Override
    @Transactional(readOnly = false)
    public Offer save(Offer offer) {
        return offerRepository.save(offer);
    }

    @Override
    @Transactional(readOnly = true)
    public Offer findOne(Long id) {
        Offer offer = offerRepository.findOne(id);
        if(offer == null) throw new NotFoundException("Offer not found!");
        return offer;
    }

    @Override
    public List<Offer> findAll(){
        return this.offerRepository.findAll();
    }

    @Override
    public Page<Offer> findAllForCurrentDamage(Long damageId, Integer pageNumber, Integer pageSize, String sortDirection, String sortProperty) {
        return this.offerRepository.findAllForCurrentDamage
                (damageId, new PageRequest(pageNumber, pageSize,(sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortProperty));
    }

    @Override
    public Offer getOfferByDamageAndBusiness(Long damageId, Long businessId) {
        return this.offerRepository.getOfferByDamageAndBusiness(damageId, businessId);
    }
}
