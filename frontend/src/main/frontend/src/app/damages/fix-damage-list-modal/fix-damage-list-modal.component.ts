import {Component, EventEmitter, OnInit} from '@angular/core';
import {BsModalRef} from 'ngx-bootstrap';
import {ToasterConfig, ToasterService} from 'angular5-toaster/dist';
// service
import {BusinessService} from '../../core/services/business.service';
import {DamageService} from '../../core/services/damage.service';
import {JwtService} from '../../core/services/jwt.service';
// model
import {Business} from '../../shared/models/business';
import {PageAndSort} from '../../shared/models/page-and-sort';
import {Damage} from '../../shared/models/damage';
// error
import {ForbiddenError} from '../../shared/errors/forbidden-error';
import {AppError} from "../../shared/errors/app-error";


@Component({
  selector: 'app-fix-damage-list-modal',
  templateUrl: './fix-damage-list-modal.component.html',
  styleUrls: ['./fix-damage-list-modal.component.css']
})
export class FixDamageListModalComponent implements OnInit {
  sortDirection: string;
  pageSize: string;
  itemsPerPage: number;
  maxSize: number;
  currentPage: number;
  sortProperties: [string];
  totalItems: number;

  tabs: any[];
  businesses: Business[];
  damage: Damage;
  activeTab: any;
  toasterConfig: ToasterConfig;

  businessChosen: EventEmitter<boolean> = new EventEmitter();

  constructor(private businessService: BusinessService, public modalRef: BsModalRef, private damageService: DamageService,
              private toasterService: ToasterService, private jwtService: JwtService) {
    this.tabs = [
      {
        title: 'Select businesses from offers',
        active: true
      },
      {
        title: 'Select from all businesses',
        active: false
      }
    ];

    this.currentPage = 1;
    this.maxSize = 3;
    this.itemsPerPage = 4;

    // Sett up for getting damages request
    this.pageSize = '4';
    this.sortDirection = 'ASC';
    this.sortProperties = ['id'];
    this.activeTab = this.tabs[0];

    this.toasterConfig = new ToasterConfig({
      timeout: {success: 2000, error: 4000, warning: 4000}
    });
  }

  ngOnInit() {
    setTimeout(() => this.loadBusinesses(this.activeTab), 50); //Moram staviti ovo na 50 ms timeout-a jer nece raditi
    // drugacije (damage ce biti null)
  }

  pageChanged(event: any) {
    this.currentPage = event.page;
    this.loadBusinesses(this.activeTab);
  }

  loadBusinesses(tab) {
    let page = new PageAndSort((this.currentPage - 1).toString(), this.pageSize, this.sortDirection, this.sortProperties);

    tab.active = true;
    this.activeTab = tab;

    if (tab.title === 'Select businesses from offers') {
      this.businessService.getPage(page, 'businesses/damages', this.damage.id, 'offer_for_damage_sent').subscribe(data => {
        this.businesses = data;
        this.totalItems = data.length == 0 ? 0 : data[0].numberOfElements;
      }, error => {
        this.toasterService.pop('error', 'Error', 'Something unexpected happened! \n See information about error in console.');
        throw error;
      });
    }
    else {
      this.businessService.getPage(page, 'businesses').subscribe(data => {
        this.businesses = data;
        this.totalItems = data.length == 0 ? 0 : data[0].numberOfElements;
      },error => {
        this.toasterService.pop('error', 'Error', 'Something unexpected happened! \n See information about error in console.');
        throw error;
      });
    }
  }

  isFirstTab() {
    return this.activeTab.title === 'Select businesses from offers';
  }

  chooseBusiness(business: Business) {
    let resource = business.offer ? {id: business.id, offerId: business.offer.id} : {id: business.id};

    this.damageService.updatePart(resource, 'buildings', this.damage.buildingId,
      'damages', this.damage.id, 'firm_or_institution_for_fixing')
      .subscribe(data => {
        this.toasterService.pop('success', 'Success', 'The business has been successfully chosen!');
        this.modalRef.hide();
        this.businessChosen.emit();
      }, (error: AppError) => {
        if (error instanceof ForbiddenError) {
          this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!\n' +
            '(You are not responsible institution for selected damage)');
        }
        else {
          this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
          throw error;
        }
      });
  }
}
