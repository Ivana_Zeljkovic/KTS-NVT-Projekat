import { Component, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToasterConfig } from 'angular5-toaster/dist';
import { BsModalRef } from 'ngx-bootstrap';
import { ToasterService } from 'angular5-toaster/dist/src/toaster.service';
// validator
import { SpaceValidator } from '../../shared/validators/space.validator';
// model
import { Damage } from '../../shared/models/damage';
// service
import { OfferService } from '../../core/services/offer.service';
import { JwtService } from '../../core/services/jwt.service';
import {ForbiddenError} from "../../shared/errors/forbidden-error";


@Component({
  selector: 'app-offer-modal',
  templateUrl: './offer-modal.component.html',
  styleUrls: ['./offer-modal.component.css']
})
export class OfferModalComponent {

  formGroup : FormGroup;
  damage : Damage;
  offerSent : EventEmitter<Damage> = new EventEmitter();
  toasterConfig : ToasterConfig;

  constructor(private fb : FormBuilder, public modalRef : BsModalRef,
              private offerService : OfferService, private jwtService: JwtService,
              private toasterService: ToasterService) {
    this.formGroup = this.fb.group({
      price: ['', [
        Validators.required,
        SpaceValidator.cannotContainSpace,
        Validators.pattern('[0-9]{1,10}')
      ]],
    });

    this.toasterConfig = new ToasterConfig({
      timeout: {success: 2000, error: 4000, warning: 4000}
    });
  }

  get price(){
    return this.formGroup.get('price');
  }

  sendOffer(){
      this.offerService.save({price: this.price.value}, 'businesses',
        this.jwtService.getIdFromToken(), 'damages', this.damage.id, 'send_offer')
          .subscribe(data =>{
            this.offerSent.emit(this.damage);
            this.toasterService.pop('success', 'Success', 'Offer has been sent successfully!!');
            this.modalRef.hide();
          }, error => {
            if(error instanceof ForbiddenError)
              this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action');
            else {
              this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
              throw error;
            }
          });
  }
}
