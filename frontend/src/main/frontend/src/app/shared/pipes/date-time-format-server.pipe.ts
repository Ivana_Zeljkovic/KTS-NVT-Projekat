import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: 'dateTimeServer'
})
export class DateTimeFormatServerPipe implements PipeTransform {
  transform(value: Date, args?: any): string {
    let day = value.getDate();
    let month = value.getMonth() + 1;
    let year = value.getFullYear();
    let hours = value.getHours();
    let minutes = value.getMinutes();
    let seconds = value.getSeconds();

    let dayString: string = '';
    let monthString: string = '';
    let hoursString: string = '';
    let minutesString: string = '';
    let secondsString: string = '';

    dayString = (day <= 9) ? `0${day}` : `${day}`;
    monthString = (month <= 9) ? `0${month}` : `${month}`;
    hoursString = (hours <= 9) ? `0${hours}` : `${hours}`;
    minutesString = (minutes <= 9) ? `0${minutes}` : `${minutes}`;
    secondsString = (seconds <= 9) ? `0${seconds}` : `${seconds}`;

    return `${year}-${monthString}-${dayString} ${hoursString}:${minutesString}:${secondsString}`;
  }
}
