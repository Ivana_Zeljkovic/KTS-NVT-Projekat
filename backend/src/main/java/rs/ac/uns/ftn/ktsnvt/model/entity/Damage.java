package rs.ac.uns.ftn.ktsnvt.model.entity;

import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "damage")
@Where(clause="deleted=0")
public class Damage {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Lob
    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private Date date;

    @ManyToOne
    private DamageType type;

    @Column(nullable = false)
    private boolean urgent;

    @Lob
    private String image;

    @Column(nullable = false, columnDefinition = "INTEGER DEFAULT 0")
    @Version
    private int version;

    @Column(nullable = false, columnDefinition = "BOOL DEFAULT FALSE")
    private boolean deleted;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private Apartment apartment;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private Building building;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private Tenant responsiblePerson;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private Business responsibleInstitution;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private Business selectedBusiness;

    @ManyToOne
    private Tenant creator;

    @OneToMany(mappedBy = "damage", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private List<Comment> comments;

    @OneToMany(mappedBy = "damage", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private List<MeetingItem> meetingItems;

    @Column(nullable = false, columnDefinition = "BOOL DEFAULT FALSE")
    private boolean fixed;

    @OneToMany(mappedBy = "damage", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private List<Offer> offers;

    @Column(nullable = false)
    @ElementCollection(targetClass=Long.class)
    private List<Long> businessesDamageSentToIds;

    public Damage() {
        this.offers = new ArrayList<>();
        this.comments = new ArrayList<>();
        this.meetingItems = new ArrayList<>();
    }

    public Damage(String description, Date date, DamageType type, boolean urgent, Tenant creator) {
        this.description = description;
        this.date = date;
        this.type = type;
        this.urgent = urgent;
        this.creator = creator;
        this.comments = new ArrayList<>();
        this.offers = new ArrayList<>();
        this.meetingItems = new ArrayList<>();
        this.fixed = false;
    }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }

    public Date getDate() { return date; }

    public void setDate(Date date) { this.date = date; }

    public DamageType getType() { return type; }

    public void setType(DamageType type) { this.type = type; }

    public boolean isUrgent() { return urgent; }

    public void setUrgent(boolean urgent) { this.urgent = urgent; }

    public String getImage() { return image; }

    public void setImage(String image) { this.image = image; }

    public int getVersion() { return version; }

    public void setVersion(int version) { this.version = version; }

    public boolean isDeleted() { return deleted; }

    public void setDeleted(boolean deleted) { this.deleted = deleted; }

    public Apartment getApartment() { return apartment; }

    public void setApartment(Apartment apartment) { this.apartment = apartment; }

    public Building getBuilding() { return building; }

    public void setBuilding(Building building) { this.building = building; }

    public Tenant getResponsiblePerson() { return responsiblePerson; }

    public void setResponsiblePerson(Tenant responsiblePerson) { this.responsiblePerson = responsiblePerson; }

    public Business getResponsibleInstitution() { return responsibleInstitution; }

    public void setResponsibleInstitution(Business responsibleInstitution) { this.responsibleInstitution = responsibleInstitution; }

    public Business getSelectedBusiness() { return selectedBusiness; }

    public void setSelectedBusiness(Business selectedBusiness) { this.selectedBusiness = selectedBusiness; }

    public Tenant getCreator() { return creator; }

    public void setCreator(Tenant creator) { this.creator = creator; }

    public List<Comment> getComments() { return comments; }

    public void setComments(List<Comment> comments) { this.comments = comments; }

    public List<MeetingItem> getMeetingItems() { return meetingItems; }

    public void setMeetingItems(List<MeetingItem> meetingItems) { this.meetingItems = meetingItems; }

    public boolean isFixed() { return fixed; }

    public void setFixed(boolean fixed) { this.fixed = fixed; }

    public List<Offer> getOffers() {return offers;}

    public void setOffers(List<Offer> offers) {this.offers = offers;}

    public List<Long> getBusinessesDamageSentToIds() {return businessesDamageSentToIds;}

    public void setBusinessesDamageSentToIds(List<Long> businessesDamageSentToIds) {
        this.businessesDamageSentToIds = businessesDamageSentToIds;
    }

    public boolean doesContainComment(Long commentId) {
        for(Comment comment : this.comments) {
            if(comment.getId().equals(commentId))
                return true;
        }
        return false;
    }
}
