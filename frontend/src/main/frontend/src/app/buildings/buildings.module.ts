import { NgModule } from '@angular/core';
// module
import { SharedModule } from "../shared/shared.module";
import { BuildingsRouterModule } from "./buildings-router.module";
// component
import { BuildingListComponent } from './building-list/building-list.component';
import { BuildingRegistrationComponent } from './building-registration/building-registration.component';
import { BuildingComponent } from './building/building.component';

@NgModule({
  imports: [
    SharedModule,
    BuildingsRouterModule
  ],
  declarations: [
    BuildingListComponent,
    BuildingRegistrationComponent,
    BuildingComponent,
  ]
})
export class BuildingsModule { }
