import { Tenant } from "./tenant";

export interface Notification {
  id: number;
  title: string;
  content: string;
  date: Date;
  creator: Tenant;
  numberOfElements?: number;
}
