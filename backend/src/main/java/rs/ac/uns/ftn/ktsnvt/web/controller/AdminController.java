package rs.ac.uns.ftn.ktsnvt.web.controller;

import io.swagger.annotations.*;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rs.ac.uns.ftn.ktsnvt.model.entity.*;

import rs.ac.uns.ftn.ktsnvt.service.*;
import rs.ac.uns.ftn.ktsnvt.web.dto.AdminCreateDTO;
import rs.ac.uns.ftn.ktsnvt.web.dto.AdminDTO;
import rs.ac.uns.ftn.ktsnvt.web.dto.AdminUpdateDTO;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

import static rs.ac.uns.ftn.ktsnvt.web.util.ConverterDTOToModel.convertAdminCreateDTOToAdministrator;


/**
 * Created by Ivana Zeljkovic on 04/11/2017.
 */
@RestController
@RequestMapping(value = "/api/admins")
@Api(value = "administrators", description = "Operations pertaining to administrators in application.")
public class AdminController {

    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private AccountAuthorityService accountAuthorityService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private AuthorityService authorityService;



    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a list of all administrators in page form.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of administrators", response = List.class),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "Request caused some error on server side")
    })
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity getAllAdmins(
            @ApiParam(value = "The page number for getting administrators.", required = true) @RequestParam String pageNumber,
            @ApiParam(value = "The page size for getting administrators.", required = true) @RequestParam String pageSize,
            @ApiParam(value = "The sort direction for getting administrators.", required = true) @RequestParam String sortDirection,
            @ApiParam(value = "The sort property for getting administrators.", required = true) @RequestParam String sortProperty) {

        Page<Administrator> administrators = this.administratorService.getAllAdmins(Integer.valueOf(pageNumber),
                Integer.valueOf(pageSize), sortDirection, sortProperty);

        List<AdminDTO> administratorsDTO = new ArrayList<>();

        administrators.getContent().forEach(administrator -> administratorsDTO.add(new AdminDTO(administrator,
                administrators.getTotalElements())));

        return new ResponseEntity<>(administratorsDTO, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get a single administrator.",
            notes = "You have to provide a valid administrator ID in the URL.",
            httpMethod = "GET",
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved administrator", response = AdminDTO.class),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity getAdmin(
            @ApiParam(value = "The ID of the existing administrator resource.", required = true) @PathVariable Long id) {

        Administrator administrator = this.administratorService.findOne(id);

        return new ResponseEntity<>(new AdminDTO(administrator), HttpStatus.OK);
    }



    @RequestMapping(
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Create a administrator resource.",
            notes = "Returns the administrator being saved.",
            httpMethod = "POST",
            produces = "application/json",
            consumes = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully created administrator", response = AdminDTO.class),
            @ApiResponse(code = 400, message = "Inappropriate administrator object sent in request body"),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 403, message = "You don't have permission to create resource"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity registerAdmin(
            @ApiParam(value = "The administrator object", required = true) @Valid @RequestBody AdminCreateDTO adminCreateDTO,
            @ApiParam(value = "The object that contains all errors from validation of DTO object") BindingResult errors) {

        this.accountService.checkUsername(adminCreateDTO.getLoginAccount().getUsername());

        Account account = new Account(adminCreateDTO.getLoginAccount().getUsername(), adminCreateDTO.getLoginAccount().getPassword());

        Administrator administrator = convertAdminCreateDTOToAdministrator(adminCreateDTO);

        Authority authority = this.authorityService.findByName("ADMIN");

        AccountAuthority accountAuthority = new AccountAuthority(account, authority);
        account.getAccountAuthorities().add(accountAuthority);
        account = this.accountService.save(account);

        administrator.setAccount(account);
        administrator = this.administratorService.save(administrator);

        account.setAdministrator(administrator);
        account = this.accountService.save(account);

        accountAuthority.setAccount(account);
        accountAuthority.setAuthority(authority);
        this.accountAuthorityService.save(accountAuthority);

        return new ResponseEntity<>(new AdminDTO(administrator), HttpStatus.CREATED);
    }



    @RequestMapping(
            value = "/{id}",
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Update a administrator resource.",
            notes = "You have to provide a valid administrator ID in the URL. " +
                    "Method returns the administrator being updated.",
            httpMethod = "PUT",
            produces = "application/json",
            consumes = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated administrator", response = AdminDTO.class),
            @ApiResponse(code = 400, message = "Inappropriate administrator object sent in request body"),
            @ApiResponse(code = 401, message = "You are not authorized to update the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity updateAdmin(
            @ApiParam(value = "The ID of the existing administrator resource.", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "The administrator object.", required = true) @Valid @RequestBody AdminUpdateDTO adminUpdateDTO,
            @ApiParam(value = "The object that contains all errors from validation of DTO object") BindingResult errors) {

        Administrator administrator = this.administratorService.findOne(id);

        this.administratorService.checkPermissionForCurrentAdmin(administrator.getId());

        administrator.setFirstName(adminUpdateDTO.getFirstName());
        administrator.setLastName(adminUpdateDTO.getLastName());
        this.administratorService.checkPasswordUpdate(administrator, adminUpdateDTO.getCurrentPassword(), adminUpdateDTO.getNewPassword());

        administrator = this.administratorService.save(administrator);
        return new ResponseEntity<>(new AdminDTO(administrator), HttpStatus.OK);
    }



    @RequestMapping(
            value = "/{id}",
            method = RequestMethod.DELETE
    )
    @ApiOperation(
            value = "Delete administrator resource.",
            notes = "You have to provide a valid administrator ID in the URL. " +
                    "Once deleted the resource can not be recovered.",
            httpMethod = "DELETE"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully deleted administrator"),
            @ApiResponse(code = 401, message = "You are not authorized to delete the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity deleteAdmin(
            @ApiParam(value = "The ID of the existing administrator resource.", required = true) @PathVariable("id") Long id) {

        Administrator administrator = this.administratorService.findOne(id);
        // treba ukloniti nalog za prijavljivanje i odgovarajucu rolu admina kojeg brisemo
        this.accountAuthorityService.remove(administrator.getAccount().getAccountAuthorities().get(0).getId());
        this.accountService.remove(administrator.getAccount().getId());
        this.administratorService.remove(administrator.getId());

        return new ResponseEntity(HttpStatus.OK);
    }
}
