export interface City {
  name: string;
  postalNumber: string;
}
