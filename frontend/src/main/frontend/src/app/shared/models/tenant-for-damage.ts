export interface TenantForDamage {
  username: string;
  firstName: string;
  lastName: string;
  id: number;
}
