import { NgModule } from '@angular/core';
// module
import { SharedModule } from "../shared/shared.module";
import { MeetingsRouterModule } from "./meetings-router.module";
// component
import { MeetingListComponent } from './meeting-list/meeting-list.component';
import { MeetingDetailsComponent } from './meeting-details/meeting-details.component';
import { QuestionnaireComponent } from "../shared/components/questionnaire/questionnaire.component";
import { ConfirmDeleteModalComponent } from "../shared/components/confirm-delete-modal/confirm-delete-modal.component";
import { MeetingAddComponent } from './meeting-add/meeting-add.component';
import { MeetingItemListModalComponent } from './meeting-item-list-modal/meeting-item-list-modal.component';
import { RecordCreateComponent } from './record-create/record-create.component';
// directive
import { CustomMinDirective } from "./meeting-add/directives/custom-min.directive";
import { CustomMaxDirective } from "./meeting-add/directives/custom-max.directive";
import { CustomMinTimeDirective } from "./meeting-add/directives/custom-min-time.directive";
import { CustomMaxTimeDirective } from "./meeting-add/directives/custom-max-time.directive";


@NgModule({
  imports: [
    SharedModule,
    MeetingsRouterModule
  ],
  declarations: [
    MeetingListComponent,
    MeetingDetailsComponent,
    MeetingAddComponent,
    CustomMinDirective,
    CustomMaxDirective,
    CustomMinTimeDirective,
    CustomMaxTimeDirective,
    MeetingItemListModalComponent,
    RecordCreateComponent
  ],
  entryComponents: [
    QuestionnaireComponent,
    ConfirmDeleteModalComponent,
    MeetingAddComponent,
    MeetingItemListModalComponent,
    RecordCreateComponent
  ]
})
export class MeetingsModule { }
