import { AddressCreate } from "./address-create";

export class BusinessUpdate {
  constructor(private name: string,
              private description: string,
              private phoneNumber: string,
              private email: string,
              private address: AddressCreate,
              private oldPassword?: string,
              private newPassword?: string,
              private confirmPassword?: string) { }
}
