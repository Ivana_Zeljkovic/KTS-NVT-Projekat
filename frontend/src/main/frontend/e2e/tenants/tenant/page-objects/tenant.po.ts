import {by, element, ElementArrayFinder, ElementFinder} from "protractor";

export class TenantPage {
  // get methods
  getPanelHeading(): ElementFinder {
    return element(by.className('panel-heading-location'));
  }

  getPanelBody(): ElementFinder {
    return element(by.className('panel-body'));
  }

  getMap(): ElementFinder {
    return element(by.tagName('agm-map'));
  }

  getTenantTable(): ElementFinder {
    return element(by.id('tenantTable'));
  }

  getFirstName(): ElementFinder {
    return element(by.xpath("//tr[1]/td[2]"));
  }

  getLastName(): ElementFinder {
    return element(by.xpath("//tr[2]/td[2]"));
  }

  getUsername(): ElementFinder {
    return element(by.xpath("//tr[3]/td[2]"));
  }

  getBirthday(): ElementFinder {
    return element(by.xpath("//tr[4]/td[2]"));
  }

  getEmail(): ElementFinder {
    return element(by.xpath("//tr[5]/td[2]"));
  }

  getPhoneNumber(): ElementFinder {
    return element(by.xpath("//tr[6]/td[2]"));
  }

  getCity(): ElementFinder {
    return element(by.xpath("//tr[7]/td[2]"));
  }

  getPostalNumber(): ElementFinder {
    return element(by.xpath("//tr[8]/td[2]"));
  }

  getStreet(): ElementFinder {
    return element(by.xpath("//tr[9]/td[2]"));
  }

  getNumber(): ElementFinder {
    return element(by.xpath("//tr[10]/td[2]"));
  }

}
