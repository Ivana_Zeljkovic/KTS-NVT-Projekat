import { browser } from "protractor";
// page
import { DamageListPage } from "../../page-objects/damage-list.po";
import { DamageDetailsPage } from "../page-objects/damage-details.po";
import { LoginPage } from "../../page-objects/login.po";
import { NavbarPage } from "../../page-objects/navbar.po";
import { DelegateResponsibilityToInstitutionPage } from "../../page-objects/delegate-responsibility-to-institution.po";


describe('Damage list for institution', () => {
  let damageListPage: DamageListPage;
  let damageDetailsPage: DamageDetailsPage;
  let loginPage: LoginPage;
  let navbarPage: NavbarPage;
  let delegateResponsibilityToInstitutionPage: DelegateResponsibilityToInstitutionPage;

  beforeAll(() => {
    loginPage = new LoginPage();
    navbarPage = new NavbarPage();
    damageListPage = new DamageListPage();
    damageDetailsPage = new DamageDetailsPage();
    delegateResponsibilityToInstitutionPage = new DelegateResponsibilityToInstitutionPage();

    browser.get('/');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');

    loginPage.setUsername('inst1');
    loginPage.setPassword('inst1');
    loginPage.getSignInButton().click();

    browser.wait(function () {
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/home';
      });
    }).then(function() {
      expect(navbarPage.getNavbarElement().isDisplayed()).toBe(true);
    });
  });

  afterAll(() => {
    browser.executeScript('window.localStorage.clear();');
  });



  it('should show list of damages that current institution should repair, when institution is ' +
    'successfully logged in', () => {
    browser.wait(function () {
      return damageListPage.getDamageListElement().isDisplayed();
    }).then(function () {
      // expected view of page
      expect(damageListPage.getTitle().isDisplayed()).toBe(true);
      expect(damageListPage.getTitle().getText()).toEqual('Damages that I should repair');
      expect(damageListPage.getSearchDateRangeInput().isDisplayed()).toBe(true);
      expect(damageListPage.getSearchDateRangeClearButton().isDisplayed()).toBe(true);
      expect(damageListPage.getSearchDateRangeClearButton().isEnabled()).toBe(true);
      expect(damageListPage.getSearchDateRangeSearchButton().isDisplayed()).toBe(true);
      expect(damageListPage.getSearchDateRangeSearchButton().isEnabled()).toBe(true);

      // if list of damages is not empty, list is displayed
      if (damageListPage.getDamageList().isPresent() && damageListPage.getDamageList().isDisplayed()) {
        expect(damageListPage.getMessageNoDamageInList().isPresent()).toBe(false);
        expect(damageListPage.getPanelsFromDamageList().isPresent()).toBe(true);
        expect(damageListPage.getPaginatorInDamageList().isDisplayed()).toBe(true);

        let numberOfDamages = {'value': 0};
        damageListPage.getPanelsFromDamageList().count().then(value => {
          numberOfDamages.value = value;

          // for every damage in list, wh should check is there option for damage details and declare
          // damage as fixed
          for (let i: number = 0; i < numberOfDamages.value; i++) {
            expect(damageListPage.getCreatorNameFromDamagePanel(i).isDisplayed()).toBe(true);
            expect(damageListPage.getDateFromDamagePanel(i).isDisplayed()).toBe(true);
            expect(damageListPage.getDamageTypeFromDamagePanel(i).isDisplayed()).toBe(true);
            expect(damageListPage.getDamageDescriptionFromDamagePanel(i).isDisplayed()).toBe(true);

            if (damageListPage.getMenuFromDamagePanel(i).isPresent() &&
              damageListPage.getMenuFromDamagePanel(i).isDisplayed()) {
              damageListPage.getMenuFromDamagePanel(i).click();

              browser.wait(function () {
                return damageListPage.getContentOfMenuFromDamagePanel(i).isDisplayed();
              }).then(function () {
                expect(damageListPage.getDamageDetailsLinkFromDamagePanel(i).isDisplayed()).toBe(true);
                expect(damageListPage.getDamageDetailsLinkFromDamagePanel(i).getText()).toEqual('Damage details');
                // second option in damage menu in this category of damages is declare damage fixed
                expect(damageListPage.getLinkFromDamagePanel(i, 2).isDisplayed()).toBe(true);
                expect(damageListPage.getLinkFromDamagePanel(i, 2).getText()).toEqual('Declare fixed');
              });
            }
          }
        });
      }
      // opposite, list is not displayed
      else {
        expect(damageListPage.getMessageNoDamageInList().isDisplayed()).toBe(true);
        expect(damageListPage.getMessageNoDamageInList().getText()).toContain('There is still no damages');
      }
    });
  });



  it('should show list of damages that current institution is responsible for when user click on Damages ' +
    'option in Responsibility dropdown in navbar', () => {
    expect(navbarPage.getResponsibilityDropdownLink().isDisplayed()).toBe(true);
    navbarPage.getResponsibilityDropdownLink().click();

    browser.wait(function() {
      return navbarPage.getContentOfResponsibilityDropdownLink().isDisplayed();
    }).then(function() {
      expect(navbarPage.getDamagesResponsibleForInResponsibilityLink().isDisplayed()).toBe(true);

      navbarPage.getDamagesResponsibleForInResponsibilityLink().click();

      browser.wait(function() {
        return damageListPage.getDamageListElement().isDisplayed();
      }).then(function() {
        // expected view of page
        expect(damageListPage.getTitle().isDisplayed()).toBe(true);
        expect(damageListPage.getTitle().getText()).toEqual('Damages I am responsible institution for');
        expect(damageListPage.getSearchDateRangeInput().isDisplayed()).toBe(true);
        expect(damageListPage.getSearchDateRangeClearButton().isDisplayed()).toBe(true);
        expect(damageListPage.getSearchDateRangeClearButton().isEnabled()).toBe(true);
        expect(damageListPage.getSearchDateRangeSearchButton().isDisplayed()).toBe(true);
        expect(damageListPage.getSearchDateRangeSearchButton().isEnabled()).toBe(true);

        // if list of damages is not empty, list is displayed
        if(damageListPage.getDamageList().isPresent() && damageListPage.getDamageList().isDisplayed()) {
          expect(damageListPage.getMessageNoDamageInList().isPresent()).toBe(false);
          expect(damageListPage.getPanelsFromDamageList().isPresent()).toBe(true);
          expect(damageListPage.getPaginatorInDamageList().isDisplayed()).toBe(true);

          let numberOfDamages = {'value':0};
          damageListPage.getPanelsFromDamageList().count().then(value => {
            numberOfDamages.value = value;

            // for every damage in list, wh should check is there option for damage details and delegate
            // responsibility in damage menu
            for(let i: number = 0; i<numberOfDamages.value; i++) {
              expect(damageListPage.getCreatorNameFromDamagePanel(i).isDisplayed()).toBe(true);
              expect(damageListPage.getDateFromDamagePanel(i).isDisplayed()).toBe(true);
              expect(damageListPage.getDamageTypeFromDamagePanel(i).isDisplayed()).toBe(true);
              expect(damageListPage.getDamageDescriptionFromDamagePanel(i).isDisplayed()).toBe(true);

              if(damageListPage.getMenuFromDamagePanel(i).isPresent() &&
                damageListPage.getMenuFromDamagePanel(i).isDisplayed()) {
                damageListPage.getMenuFromDamagePanel(i).click();

                browser.wait(function() {
                  return damageListPage.getContentOfMenuFromDamagePanel(i).isDisplayed();
                }).then(function() {
                  expect(damageListPage.getDamageDetailsLinkFromDamagePanel(i).isDisplayed()).toBe(true);
                  expect(damageListPage.getDamageDetailsLinkFromDamagePanel(i).getText()).toEqual('Damage details');
                  // second option in damage menu in this category of damages is delegating responsibility option
                  expect(damageListPage.getLinkFromDamagePanel(i, 2).isDisplayed()).toBe(true);
                  expect(damageListPage.getLinkFromDamagePanel(i,2).getText()).toEqual('Delegate responsibility')
                });
              }
            }
          });
        }
        // opposite, list is not displayed
        else {
          expect(damageListPage.getMessageNoDamageInList().isDisplayed()).toBe(true);
          expect(damageListPage.getMessageNoDamageInList().getText()).toContain('There is still no damages');
        }
      });
    });
  });



  it('should open modal dialog for delegating responsibility for damage when user click on Delegate ' +
    'responsibility option in menu and close that modal when user click on Close button', () => {
    expect(damageListPage.getMenuFromDamagePanel(0).isDisplayed()).toBe(true);
    damageListPage.getMenuFromDamagePanel(0).click();

    browser.wait(function() {
      return damageListPage.getContentOfMenuFromDamagePanel(0).isDisplayed();
    }).then(function() {
      // delegate responsibility option is second in option list (in menu)
      damageListPage.getLinkFromDamagePanel(0, 2).click();

      browser.wait(function() {
        // expect to show modal dialog for delegating responsibility
        return delegateResponsibilityToInstitutionPage.getDelegateResponsibilityToInstitutionElement().isDisplayed();
      }).then(function() {
        // expect to modal has all elements
        expect(delegateResponsibilityToInstitutionPage.getTitle().isDisplayed()).toBe(true);
        expect(delegateResponsibilityToInstitutionPage.getCloseButton().isDisplayed()).toBe(true);
        expect(delegateResponsibilityToInstitutionPage.getCloseButton().isEnabled()).toBe(true);

        delegateResponsibilityToInstitutionPage.getCloseButton().click();
      });
    });
  });
});
