import { browser } from "protractor";
// page
import { TenantProfilePage } from "../../page-objects/tenant-profile.po";
import { LoginPage } from "../../page-objects/login.po";


describe('Tenant profile', () => {
  let loginPage: LoginPage;
  let tenantProfilePage: TenantProfilePage;
  let updatedValues: any;

  beforeAll(() => {
    loginPage = new LoginPage();
    tenantProfilePage = new TenantProfilePage();
    updatedValues = {'firstName':'Kaca', 'email':'katarina@gmail.com'};

    browser.get('/');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');

    loginPage.setUsername('kaca');
    loginPage.setPassword('kaca');
    loginPage.getSignInButton().click();

    browser.wait(function() {
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/home/notifications';
      });
    }).then(function() {
      browser.get('http://localhost:49152/#/profile');

      browser.wait(function() {
        return tenantProfilePage.getTenantProfileElement().isDisplayed();
      });
    });
  });

  afterAll(() => {
    tenantProfilePage.getFirstName().clear();
    tenantProfilePage.setFirstName('Katarina');
    tenantProfilePage.getEmail().clear();
    tenantProfilePage.setEmail('kaca@gmail.com');
    tenantProfilePage.getSaveButton().click();
    browser.executeScript('window.localStorage.clear();');
  });



  it('should show all elements that belongs to tenant profile', () => {
    expect(tenantProfilePage.getFirstName().isDisplayed()).toBe(true);
    expect(tenantProfilePage.getLastName().isDisplayed()).toBe(true);
    expect(tenantProfilePage.getEmail().isDisplayed()).toBe(true);
    expect(tenantProfilePage.getPhoneNumber().isDisplayed()).toBe(true);
    expect(tenantProfilePage.getBirthDate().isDisplayed()).toBe(true);
    expect(tenantProfilePage.getUsername().isDisplayed()).toBe(true);
    expect(tenantProfilePage.getCurrentPassword().isDisplayed()).toBe(true);
    expect(tenantProfilePage.getPassword().isDisplayed()).toBe(true);
    expect(tenantProfilePage.getConfirmPassword().isDisplayed()).toBe(true);
    expect(tenantProfilePage.getFirstName().getAttribute('value')).toEqual('Katarina');
    expect(tenantProfilePage.getLastName().getAttribute('value')).toEqual('Cukurov');
    expect(tenantProfilePage.getEmail().getAttribute('value')).toEqual('kaca@gmail.com');
    expect(tenantProfilePage.getPhoneNumber().getAttribute('value')).toEqual('123456');
    expect(tenantProfilePage.getBirthDate().getAttribute('value')).toEqual('11/01/1995');
    expect(tenantProfilePage.getSaveButton().isDisplayed()).toBe(true);
    expect(tenantProfilePage.getSaveButton().isEnabled()).toBe(false);
    expect(tenantProfilePage.getResetButton().isDisplayed()).toBe(true);
    expect(tenantProfilePage.getResetButton().isEnabled()).toBe(false);
  });



  it('should enable Save and Reset button if user change some values in form, reset form to initial values ' +
    'when user click on Reset, and again disable Save and Reset buttons', () => {
    tenantProfilePage.getFirstName().clear();
    tenantProfilePage.getEmail().clear();
    tenantProfilePage.setFirstName(updatedValues.firstName);
    tenantProfilePage.setEmail(updatedValues.email);

    expect(tenantProfilePage.getSaveButton().isEnabled()).toBe(true);
    expect(tenantProfilePage.getResetButton().isEnabled()).toBe(true);

    tenantProfilePage.getResetButton().click();

    expect(tenantProfilePage.getSaveButton().isEnabled()).toBe(false);
    expect(tenantProfilePage.getResetButton().isEnabled()).toBe(false);
    expect(tenantProfilePage.getFirstName().getAttribute('value')).toEqual('Katarina');
    expect(tenantProfilePage.getEmail().getAttribute('value')).toEqual('kaca@gmail.com');
  });



  it('should show error message that current and confirm password fields must be filled if user put some ' +
    'value in password field', () => {
    tenantProfilePage.setPassword('password');
    tenantProfilePage.getConfirmPassword().click();
    expect(tenantProfilePage.getCurrentPasswordRequiredError().isDisplayed()).toBe(true);
    expect(tenantProfilePage.getConfirmPasswordRequiredError().isDisplayed()).toBe(true);
  });



  it('should show error message about minimum length of password value, when user put values with less then ' +
    '5 characters in that field', () => {
    tenantProfilePage.getPassword().clear();
    tenantProfilePage.setPassword('1234');

    expect(tenantProfilePage.getPasswordMinLengthError().isDisplayed()).toBe(true);
  });



  it('should show error message that email and phone number pattern are invalid, if user put some invalid ' +
    'values in that fields', () => {
    tenantProfilePage.getEmail().clear();
    tenantProfilePage.getPhoneNumber().clear();

    tenantProfilePage.setEmail('something');
    tenantProfilePage.setPhoneNumber('23jhffv');

    expect(tenantProfilePage.getEmailPatternError().isDisplayed()).toBe(true);
    expect(tenantProfilePage.getPhoneNumberPatternError().isDisplayed()).toBe(true);
  });



  it('should show error that password don\'t match, if user put different values in password and confirm ' +
    'password fields', () => {
    tenantProfilePage.getPassword().clear();
    tenantProfilePage.getConfirmPassword().clear();

    tenantProfilePage.setPassword('someValue');
    tenantProfilePage.setConfirmPassword('Somevalue');

    expect(tenantProfilePage.getPasswordDontMatchError().isDisplayed()).toBe(true);
  });



  it('should disable Save and Reset button, and show updated values when user click on Save button', () => {
    tenantProfilePage.getResetButton().click();

    tenantProfilePage.getFirstName().clear();
    tenantProfilePage.setFirstName(updatedValues.firstName);
    tenantProfilePage.getEmail().clear();
    tenantProfilePage.setEmail(updatedValues.email);

    expect(tenantProfilePage.getSaveButton().isEnabled()).toBe(true);

    tenantProfilePage.getSaveButton().click();

    expect(tenantProfilePage.getSaveButton().isEnabled()).toBe(false);
    expect(tenantProfilePage.getResetButton().isEnabled()).toBe(false);
    expect(tenantProfilePage.getFirstName().getAttribute('value')).toEqual(updatedValues.firstName);
    expect(tenantProfilePage.getEmail().getAttribute('value')).toEqual(updatedValues.email);
  });
});
