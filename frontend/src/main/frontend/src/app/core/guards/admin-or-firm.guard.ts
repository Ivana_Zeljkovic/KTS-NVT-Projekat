import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
import { Injectable } from "@angular/core";
import { JwtService } from "../services/jwt.service";

@Injectable()
export class AdminOrFirmGuard implements CanActivate {

  constructor(private router: Router, private jwtService: JwtService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let roles: Array<string> = this.jwtService.getRolesFromToken();
    if(roles.indexOf('ADMIN') != -1 || roles.indexOf('COMPANY') != -1) return true;

    // logged user hasn't role ADMIN or COMPANY
    this.router.navigate(['home']);
    return false;
  }
}
