package rs.ac.uns.ftn.ktsnvt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.ForbiddenException;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Damage;
import rs.ac.uns.ftn.ktsnvt.repository.DamageRepository;

import java.util.Date;

/**
 * Created by Ivana Zeljkovic on 23/11/2017.
 */
@Service
public class DamageServiceImpl implements DamageService {

    @Autowired
    private DamageRepository damageRepository;


    @Override
    @Transactional(readOnly = false)
    public Damage save(Damage damage) {
        return this.damageRepository.save(damage);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Damage> findAllByBuilding(Long buildingId, Integer pageNumber, Integer pageSize, String sortDirection,
                                          String sortProperty) {
        return this.damageRepository.findAllByBuilding(buildingId, new PageRequest(pageNumber, pageSize,
                (sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortProperty));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Damage> getDamagesForInstitution(Long institutionId, Integer pageNumber, Integer pageSize,
                                                 String sortDirection, String sortProperty, String sortPropertySecond) {
        Sort sort = new Sort(
                new Sort.Order(Sort.Direction.DESC, sortProperty),
                new Sort.Order((sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortPropertySecond)
        );
        return this.damageRepository.getAllForInstitution(institutionId,
                new PageRequest(pageNumber, pageSize, sort));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Damage> getDamagesForInstitutionFromTo(Long institutionId, Date fromDate, Date toDate, Integer pageNumber,
                                                       Integer pageSize, String sortDirection, String sortProperty,
                                                       String sortPropertySecond) {
        Sort sort = new Sort(
                new Sort.Order(Sort.Direction.DESC, sortProperty),
                new Sort.Order((sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortPropertySecond)
        );
        return this.damageRepository.getAllForInstitutionFromTo(institutionId, fromDate, toDate,
                new PageRequest(pageNumber, pageSize, sort));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Damage> getDamagesForRepair(Long businessId, Integer pageNumber, Integer pageSize,
                                                 String sortDirection, String sortProperty, String sortPropertySecond) {
        Sort sort = new Sort(
                new Sort.Order(Sort.Direction.DESC, sortProperty),
                new Sort.Order((sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortPropertySecond)
        );
        return this.damageRepository.getAllForRepair(businessId,
                new PageRequest(pageNumber, pageSize, sort));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Damage> getDamagesForRepairFromTo(Long businessId, Date fromDate, Date toDate, Integer pageNumber,
                                                  Integer pageSize, String sortDirection, String sortProperty,
                                                  String sortPropertySecond) {
        Sort sort = new Sort(
                new Sort.Order(Sort.Direction.DESC, sortProperty),
                new Sort.Order((sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortPropertySecond)
        );
        return this.damageRepository.getAllForRepairFromTo(businessId, fromDate, toDate,
                new PageRequest(pageNumber, pageSize, sort));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Damage> getDamagesInBuilding(Long buildingId, Integer pageNumber, Integer pageSize,
                                            String sortDirection, String sortProperty, String sortPropertySecond) {
        Sort sort = new Sort(
                new Sort.Order(Sort.Direction.ASC, "fixed"),
                new Sort.Order(Sort.Direction.DESC, sortProperty),
                new Sort.Order((sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortPropertySecond)
        );
        return this.damageRepository.getAllInBuilding(buildingId,
                new PageRequest(pageNumber, pageSize, sort));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Damage> getDamagesInBuildingFromTo(Long buildingId, Date fromDate, Date toDate, Integer pageNumber,
                                                   Integer pageSize, String sortDirection, String sortProperty,
                                                   String sortPropertySecond) {
        Sort sort = new Sort(
                new Sort.Order(Sort.Direction.ASC, "fixed"),
                new Sort.Order(Sort.Direction.DESC, sortProperty),
                new Sort.Order((sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortPropertySecond)
        );
        return this.damageRepository.getAllInBuildingFromTo(buildingId, fromDate, toDate,
                new PageRequest(pageNumber, pageSize, sort));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Damage> getDamagesForTenant(Long tenantId, Integer pageNumber, Integer pageSize,
                                             String sortDirection, String sortProperty, String sortPropertySecond) {
        Sort sort = new Sort(
                new Sort.Order(Sort.Direction.DESC, sortProperty),
                new Sort.Order((sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortPropertySecond)
        );
        return this.damageRepository.getAllForTenant(tenantId,
                new PageRequest(pageNumber, pageSize, sort));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Damage> getDamagesForTenantFromTo(Long tenantId, Date fromDate, Date toDate, Integer pageNumber,
                                            Integer pageSize, String sortDirection, String sortProperty,
                                            String sortPropertySecond) {
        Sort sort = new Sort(
                new Sort.Order(Sort.Direction.DESC, sortProperty),
                new Sort.Order((sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortPropertySecond)
        );
        return this.damageRepository.getAllForTenantFromTo(tenantId, fromDate, toDate,
                new PageRequest(pageNumber, pageSize, sort));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Damage> getDamagesInPersonalApartments(Long tenantId, Integer pageNumber, Integer pageSize,
                                            String sortDirection, String sortProperty, String sortPropertySecond) {
        Sort sort = new Sort(
                new Sort.Order(Sort.Direction.DESC, sortProperty),
                new Sort.Order((sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortPropertySecond)
        );
        return this.damageRepository.getAllInPersonalApartments(tenantId,
                new PageRequest(pageNumber, pageSize, sort));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Damage> getDamagesInPersonalApartmentsFromTo(Long tenantId, Date fromDate, Date toDate, Integer pageNumber,
                                                             Integer pageSize, String sortDirection, String sortProperty,
                                                             String sortPropertySecond) {
        Sort sort = new Sort(
                new Sort.Order(Sort.Direction.DESC, sortProperty),
                new Sort.Order((sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortPropertySecond)
        );
        return this.damageRepository.getAllInPersonalApartmentsFromTo(tenantId, fromDate, toDate,
                new PageRequest(pageNumber, pageSize, sort));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Damage> getDamagesInApartment(Long apartmentId, Integer pageNumber, Integer pageSize,
                                                       String sortDirection, String sortProperty, String sortPropertySecond) {
        Sort sort = new Sort(
                new Sort.Order(Sort.Direction.DESC, sortProperty),
                new Sort.Order((sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortPropertySecond)
        );
        return this.damageRepository.getAllInApartment(apartmentId,
                new PageRequest(pageNumber, pageSize, sort));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Damage> getDamagesInApartmentFromTo(Long apartmentId, Date fromDate, Date toDate, Integer pageNumber, Integer pageSize,
                                              String sortDirection, String sortProperty, String sortPropertySecond) {
        Sort sort = new Sort(
                new Sort.Order(Sort.Direction.DESC, sortProperty),
                new Sort.Order((sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortPropertySecond)
        );
        return this.damageRepository.getAllInApartmentFromTo(apartmentId, fromDate, toDate,
                new PageRequest(pageNumber, pageSize, sort));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Damage> getMyDamages(Long tenantId, Integer pageNumber, Integer pageSize,
                                              String sortDirection, String sortProperty, String sortPropertySecond) {
        Sort sort = new Sort(
                new Sort.Order(Sort.Direction.DESC, sortProperty),
                new Sort.Order((sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortPropertySecond)
        );
        return this.damageRepository.getAllMyDamages(tenantId,
                new PageRequest(pageNumber, pageSize, sort));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Damage> getMyDamagesFromTo(Long tenantId, Date fromDate, Date toDate, Integer pageNumber, Integer pageSize,
                                     String sortDirection, String sortProperty, String sortPropertySecond) {
        Sort sort = new Sort(
                new Sort.Order(Sort.Direction.DESC, sortProperty),
                new Sort.Order((sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortPropertySecond)
        );
        return this.damageRepository.getAllMyDamagesFromTo(tenantId, fromDate, toDate,
                new PageRequest(pageNumber, pageSize, sort));
    }

    @Override
    @Transactional(readOnly = true)
    public Damage findOne(Long id) {
        Damage damage = this.damageRepository.findOne(id);
        if(damage == null) throw new NotFoundException("Damage not found!");
        return damage;
    }

    @Override
    public void checkDamageInBuilding(Damage damage, Long buildingId) {
        if(!damage.getBuilding().getId().toString().equals(buildingId.toString()))
            throw new BadRequestException("Selected damage doesn't belong to the selected building!");
    }

    @Override
    public void checkResponsiblePerson(Damage damage, Long tenantId) {
        if(damage.getResponsiblePerson() == null ||
                (damage.getResponsiblePerson() != null && !damage.getResponsiblePerson().getId().equals(tenantId)))
            throw new ForbiddenException("You don't have permission for this action! " +
                    "You are not responsible person for selected damage!");
    }

    @Override
    @Transactional(readOnly = false)
    public void remove(Long damageId) {
        this.damageRepository.delete(damageId);
    }

    @Override
    @Transactional(readOnly = true)
    public void isDamageFixed(Damage damage) {
        if(damage.isFixed())
            throw new BadRequestException("Damage is already fixed!");
    }

    @Override
    public boolean findDamageIdInOfferRequest(Long damageId, Long businessId) {
        Damage damage = this.damageRepository.findDamageIdInOfferRequest(damageId, businessId);
        return  damage != null;
    }

    @Override
    public Page<Damage> findAllDamageFixRequests(Long businessId, Integer pageNumber, Integer pageSize, String sortDirection, String sortProperty) {
        return this.damageRepository.findAllDamageFixRequests(businessId, new PageRequest(
                pageNumber, pageSize, (sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortProperty
        ));
    }
}
