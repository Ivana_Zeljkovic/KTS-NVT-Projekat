import { Component, Input, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap';
import { ToasterConfig, ToasterService } from 'angular5-toaster/dist';
// service
import { DamageTypeResponsibilityService } from '../../core/services/damage-type-responsibility.service';
import { JwtService } from '../../core/services/jwt.service';
// model
import { DamageTypeResponsibility } from '../../shared/models/damage-type-responsibility';
import { PageAndSort } from '../../shared/models/page-and-sort';
// error
import { AppError } from '../../shared/errors/app-error';
import { ForbiddenError } from '../../shared/errors/forbidden-error';
import { NotFoundError } from '../../shared/errors/not-found-error';
// component
import { DelegateResponsibilityToInstitutionComponent } from "../../shared/components/delegate-responsibility-to-institution/delegate-responsibility-to-institution.component";


@Component({
  selector: 'app-damage-types',
  templateUrl: './damage-types.component.html',
  styleUrls: ['./damage-types.component.css']
})
export class DamageTypesComponent implements OnInit {
  toasterConfig: ToasterConfig;
  damageTypeResponsibilities : Array<DamageTypeResponsibility>;

  // Elements for bootstrap paging
  currentPage: number;
  totalItems: number;
  maxSize: number;
  itemsPerPage: number;

  // Elements for getting data from server
  pageSize: string;
  sortDirection: string;
  sortProperties: string[];

  constructor(private damageTypeResponsibilityService: DamageTypeResponsibilityService, private modalService: BsModalService,
              private jwtService: JwtService, private toasterService: ToasterService) {
    // Sett up for bootstrap paging
    this.currentPage = 1;
    this.maxSize = 3;
    this.itemsPerPage = 4;

    // Sett up for getting notification request
    this.pageSize = '4';
    this.sortDirection = 'DESC';
    this.sortProperties = ['id'];

    this.toasterConfig = new ToasterConfig({
      timeout: {success: 2000, error: 4000, warning: 4000}
    });
  }

  ngOnInit() {
    this.getDamageResponsibilities();
  }

  delegateResponsibility(damageResponsibility: DamageTypeResponsibility){
    const modalRef = this.modalService.show(DelegateResponsibilityToInstitutionComponent);
    modalRef.content.setResponsibility = false;
    modalRef.content.isDelegatingDamage = false;
    modalRef.content.damageTypeResponsibility = damageResponsibility;

    modalRef.content.responsibilitySubmitted.subscribe(responsibilityDelegated => {
      if(responsibilityDelegated)
        this.getDamageResponsibilities();
    });

  }

  private getDamageResponsibilities(){
    let page = new PageAndSort((this.currentPage - 1).toString(), this.pageSize, this.sortDirection, this.sortProperties);

    this.damageTypeResponsibilityService.getPage(page, 'institutions', this.jwtService.getIdFromToken(), 'responsible_for_damage_types')
      .subscribe(damageTypeResponsibilityList => {
        this.totalItems = damageTypeResponsibilityList.length == 0 ? 0 : damageTypeResponsibilityList[0].numberOfElements;
        this.damageTypeResponsibilities = damageTypeResponsibilityList;
      }, (error: AppError) => {
        if(error instanceof ForbiddenError)
          this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
        else if(error instanceof NotFoundError)
          this.toasterService.pop('error', 'Error', 'Institution not found!');
        else {
          this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
          throw error;
        }
      });
  }
}
