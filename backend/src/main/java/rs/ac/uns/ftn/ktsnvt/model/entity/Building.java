package rs.ac.uns.ftn.ktsnvt.model.entity;

import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "building")
@Where(clause="deleted=0")
public class Building {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Column(nullable = false)
    private int numberOfFloors;

    @Column(nullable = false)
    private boolean hasParking;

    @Column(nullable = false, columnDefinition = "INTEGER DEFAULT 0")
    @Version
    private int version;

    @Column(nullable = false, columnDefinition = "BOOL DEFAULT FALSE")
    private boolean deleted;

    @OneToMany(mappedBy = "building", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private List<Apartment> apartments;

    @OneToOne
    private Council council;

    @OneToMany(mappedBy = "building", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private List<Damage> damages;

    @OneToOne
    private Address address;

    @OneToOne
    private Billboard billboard;

    public Building() {
        this.apartments = new ArrayList<>();
        this.damages = new ArrayList<>();
    }

    public Building(int numberOfFloors, boolean hasParking) {
        this.numberOfFloors = numberOfFloors;
        this.hasParking = hasParking;
        this.apartments = new ArrayList<>();
        this.damages = new ArrayList<>();
    }

    public Building(int numberOfFloors, boolean hasParking, Address address) {
        this.numberOfFloors = numberOfFloors;
        this.hasParking = hasParking;
        this.address = address;
        this.apartments = new ArrayList<>();
        this.damages = new ArrayList<>();
    }

    public Long getId() { return id; }

    public void setId(Long id) {
        this.id = id;
    }

    public int getNumberOfFloors() {
        return numberOfFloors;
    }

    public void setNumberOfFloors(int numberOfFloors) {
        this.numberOfFloors = numberOfFloors;
    }

    public boolean isHasParking() { return hasParking; }

    public void setHasParking(boolean hasParking) { this.hasParking = hasParking; }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public List<Apartment> getApartments() { return apartments; }

    public void setApartments(List<Apartment> apartments) { this.apartments = apartments; }

    public Council getCouncil() { return council; }

    public void setCouncil(Council council) { this.council = council; }

    public List<Damage> getDamages() { return damages; }

    public void setDamages(List<Damage> damages) { this.damages = damages; }

    public Address getAddress() { return address; }

    public void setAddress(Address address) { this.address = address; }

    public Billboard getBillboard() { return billboard; }

    public void setBillboard(Billboard billboard) { this.billboard = billboard; }
}
