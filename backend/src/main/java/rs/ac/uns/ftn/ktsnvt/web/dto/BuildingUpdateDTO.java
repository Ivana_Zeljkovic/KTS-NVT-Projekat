package rs.ac.uns.ftn.ktsnvt.web.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Katarina Cukurov on 16/11/2017.
 */
public class BuildingUpdateDTO implements Serializable {

    @NotNull
    @Valid
    private boolean hasParking;
    @NotNull
    @Valid
    private AddressDTO address;

    private List<ApartmentUpdateDTO> apartments;
    @NotNull
    private int numberOfFloors;

    public BuildingUpdateDTO() {}

    public BuildingUpdateDTO(boolean hasParking, AddressDTO address, List<ApartmentUpdateDTO> apartments, int numberOfFloors) {
        this.hasParking = hasParking;
        this.address = address;
        this.apartments = apartments;
        this.numberOfFloors = numberOfFloors;
    }

    public boolean isHasParking() {
        return hasParking;
    }

    public void setHasParking(boolean hasParking) {
        this.hasParking = hasParking;
    }

    public AddressDTO getAddress() {
        return address;
    }

    public void setAddress(AddressDTO address) {
        this.address = address;
    }

    public List<ApartmentUpdateDTO> getApartments() {
        return apartments;
    }

    public void setApartments(List<ApartmentUpdateDTO> apartments) {
        this.apartments = apartments;
    }

    public int getNumberOfFloors() {
        return numberOfFloors;
    }

    public void setNumberOfFloors(int numberOfFloors) {
        this.numberOfFloors = numberOfFloors;
    }
}
