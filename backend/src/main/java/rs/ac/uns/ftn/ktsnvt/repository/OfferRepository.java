package rs.ac.uns.ftn.ktsnvt.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import rs.ac.uns.ftn.ktsnvt.model.entity.Offer;

/**
 * Created by Katarina Cukurov on 24/11/2017.
 */
public interface OfferRepository extends JpaRepository<Offer, Long> {

    @Query(value = "SELECT O FROM Offer O WHERE O.damage.id = :damageId")
    Page<Offer> findAllForCurrentDamage(@Param("damageId") Long damageId, Pageable pageable);


    @Query(value = "SELECT * FROM OFFER O WHERE O.damage_id = :damageId AND O.business_id = :businessId", nativeQuery = true)
    Offer getOfferByDamageAndBusiness(@Param("damageId") Long damageId,@Param("businessId") Long businessId);
}
