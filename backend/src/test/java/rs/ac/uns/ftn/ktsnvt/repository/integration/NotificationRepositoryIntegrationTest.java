package rs.ac.uns.ftn.ktsnvt.repository.integration;

import org.aspectj.weaver.ast.Not;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.model.entity.Notification;
import rs.ac.uns.ftn.ktsnvt.repository.NotificationRepository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Ivana Zeljkovic on 28/11/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class NotificationRepositoryIntegrationTest {

    @Autowired
    private NotificationRepository notificationRepository;


    @Test
    @Transactional
    public void testFindAllByBillboard() {
        /*
         * Test proverava ispravnost rada metode findAllByBillboard repozitorijuma NotificationRepository
         * (pronalazenje svih javnih obavestenja sa bilborda zgrade)
         * */

        /*
        oglasnoj tabli zgrade sa ID-em = 100 (oglasna tabla sa ID-em = 100) pripadaju 2 obavestenja:
            1 - ID = 100,
                ID kreatora = 101
                content = First notification
            2 - ID = 101
                ID kreatora = 101
                content = Second notification
        */
        Page<Notification> notifications = this.notificationRepository.findAllByBillboard(new Long(100),
                new PageRequest(0, 2));

        assertEquals(2, notifications.getTotalElements());
        assertEquals(Long.valueOf(100), notifications.getContent().get(0).getId());
        assertEquals("First notification content", notifications.getContent().get(0).getContent());
        assertEquals(Long.valueOf(101), notifications.getContent().get(0).getCreator().getId());
        assertEquals(Long.valueOf(101), notifications.getContent().get(1).getId());
        assertEquals("Second notification content", notifications.getContent().get(1).getContent());
        assertEquals(Long.valueOf(101), notifications.getContent().get(1).getCreator().getId());
    }

    @Test
    @Transactional
    public void testFindOneByIdAndBillboard() {
        /*
         * Test proverava ispravnost rada metode findOneByIdAndBillboard repozitorijuma NotificationRepository
         * (pronalazenje konkretnog javnog obavestenja na osnovu zadatog ID-a i ID-a bilborda zgrade kom obavestenje pripada)
         * u slucaju kada postoji obavestenje sa zadatim ID-em na bilbordu sa zadatim ID-em
         * */

        /*
        oglasnoj tabli zgrade sa ID-em = 100 (oglasna tabla sa ID-em = 100) pripada obavestenje:
            ID = 100,
            ID kreatora = 101
            content = First notification
        */
        Notification notification = this.notificationRepository.findOneByIdAndBillboard(new Long(100), new Long(100));

        assertNotNull(notification);
        assertEquals(Long.valueOf(100), notification.getId());
        assertEquals(Long.valueOf(101), notification.getCreator().getId());
        assertEquals("First notification", notification.getTitle());
        assertEquals("First notification content", notification.getContent());
    }

    @Test
    @Transactional
    public void testFindOneByIdAndBillboardNonexistent() {
        /*
         * Test proverava ispravnost rada metode findOneByIdAndBillboard repozitorijuma NotificationRepository
         * (pronalazenje konkretnog javnog obavestenja na osnovu zadatog ID-a i ID-a bilborda zgrade kom obavestenje pripada)
         * u slucaju kada ne postoji obavestenje sa zadatim ID-em na bilbordu sa zadatim ID-em
         * */

        // oglasnoj tabli zgrade sa ID-em = 100 (oglasna tabla sa ID-em = 100) ne pripada obavestenje sa ID-em 102:
        Notification notification = this.notificationRepository.findOneByIdAndBillboard(new Long(102), new Long(100));

        assertNull(notification);
    }

    @Test
    @Transactional
    public void testFindAllFromTo() {
        /*
         * Test proverava ispravnost rada metode findAllFromTo repozitorijuma NotificationRepository
         * (pronalazenje svih javnih obavestenja koja su kreiranja u periodu od-do na osnovu ID-a bilborda zgrade kom obavestenje pripada,
         * datuma pocetka i datuma kraja)
         * */

        /*
        na odglasnoj tabli zgrade sa ID-em = 100 (oglasna tabla sa ID-em = 100) nalazi se samo jedno
        obavestenje koje je kreirano u periodu od 2017-11-26 do 2017-11-27 i ono ima podatke:
            id = 100,
            content = First notification,
            creator_id = 101 (firstName = Katarina, lastName = Cukurov)
        */
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date from = null;
        Date to = null;
        try {
            from = sdf.parse("2017-11-25");
            to = sdf.parse("2017-11-27");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Page<Notification> notifications = this.notificationRepository.findAllFromDateToDate(100L, from, to,
                new PageRequest(0,1));

        assertEquals(1, notifications.getTotalElements());
        assertEquals(Long.valueOf(100), notifications.getContent().get(0).getId());
        assertEquals("First notification content", notifications.getContent().get(0).getContent());
        assertEquals(Long.valueOf(101), notifications.getContent().get(0).getCreator().getId());
        assertEquals("Katarina", notifications.getContent().get(0).getCreator().getFirstName());
        assertEquals("Cukurov", notifications.getContent().get(0).getCreator().getLastName());
    }
}
