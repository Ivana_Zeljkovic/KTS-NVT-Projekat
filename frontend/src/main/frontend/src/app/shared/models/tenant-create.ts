import { Login } from "./login";

export class TenantCreate {
  constructor(private loginAccount: Login, private firstName: string,
              private lastName: string, private birthDate: Date,
              private email: string, private phoneNumber: string) { }
}
