package rs.ac.uns.ftn.ktsnvt.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import rs.ac.uns.ftn.ktsnvt.model.entity.Apartment;

import java.util.List;

/**
 * Created by Ivana Zeljkovic on 05/11/2017.
 */
public interface ApartmentService {

    List<Apartment> findAll();

    Page<Apartment> findAll(Pageable page);

    Page<Apartment> getAllFromBuilding(Long buildingId, Integer pageNumber, Integer pageSize,
                              String sortDirection, String sortProperty, String sortPropertySecond);

    Apartment findOne(Long id);

    Apartment findOneIfExist(Long id);

    Apartment save(Apartment apartment);

    void remove(Long id);

    Page<Apartment> findFreeApartments(Integer pageNumber, Integer pageSize,
                                       String sortDirection, String sortProperty);

    Page<Apartment> findFreeApartmentsInBuilding(Long buildingId, Integer pageNumber, Integer pageSize,
                                                 String sortDirection, String sortProperty);

    Page<Apartment> findFreeApartmentsBySize(int min, int max, Long buildingId, Integer pageNumber, Integer pageSize,
                                             String sortDirection, String sortProperty);

    int countApartmentsInBuilding(Long ownerId, Long buildingId);

    void checkBuilding(Apartment apartment, Long buildingId);

    Page<Apartment> findAllPageable(Integer pageNumber, Integer pageSize,
                                    String sortDirection, String sortProperty);
}
