import {by, element, ElementFinder} from "protractor";

export class NavbarPage {

  getNavbarElement(): ElementFinder {
    return element(by.tagName("app-navbar"));
  }

  getHomeLink(): ElementFinder {
    return element(by.css('.navbar-home > a'));
  }


  // Dropdowns
  getDamageDropdownLink(): ElementFinder {
    return element(by.css('.navbar-damage-dropdown > a'));
  }

  getContentOfDamageDropdownLink(): ElementFinder {
    return element(by.id('dropdownMenuDamages'));
  }

  getCouncilDropdownLink(): ElementFinder {
    return element(by.css('.navbar-council-dropdown > a'));
  }

  getContentOfCouncilDropdownLink(): ElementFinder {
    return element(by.id('dropdownMenuCouncil'));
  }

  getQuestionnairesDropdownLink(): ElementFinder {
    return element(by.css('.navbar-questionnaires-dropdown > a'));
  }

  getContentOfQuestionnaireDropdownLink(): ElementFinder {
    return element(by.id('dropdownMenuQuestionnaires'));
  }

  getResponsibilityDropdownLink(): ElementFinder {
    return element(by.css('.navbar-responsibility-dropdown > a'));
  }

  getContentOfResponsibilityDropdownLink(): ElementFinder {
    return element(by.id('dropdownMenuResponsibility'));
  }

  getPersonalDropdown(): ElementFinder {
    return element(by.css('.navbar-personal-dropdown > a'));
  }


  // Options in Damages dropdown
  getDamagesResponsibleForLink(): ElementFinder {
    return element(by.css('#dropdownMenuDamages li:nth-child(1) > a'));
  }

  getDamagesIReportedLink(): ElementFinder {
    return element(by.css('#dropdownMenuDamages li:nth-child(2) > a'));
  }

  getDamagesInMyApartmentLink(): ElementFinder {
    return element(by.css('#dropdownMenuDamages li:nth-child(3) > a'));
  }

  getDamagesInMyBuildingLink(index: number): ElementFinder {
    return element(by.css(`#dropdownMenuDamages li:nth-child(${index}) > a`));
  }

  getDamagesInMyPersonalApartmentsLink(index: number): ElementFinder {
    return element(by.css(`#dropdownMenuDamages li:nth-child(${index}) > a`));
  }


  // Options in Council dropdown
  getMeetingsLink(): ElementFinder {
    return element(by.css('#dropdownMenuCouncil li:nth-child(1) > a'));
  }

  getRecordsLink(): ElementFinder {
    return element(by.css('#dropdownMenuCouncil li:nth-child(2) > a'));
  }

  getResignateLink(): ElementFinder {
    return element(by.css('#dropdownMenuCouncil li:nth-child(3) > a'));
  }

  getDelegateCouncilResponsibilityLink(index: number): ElementFinder {
    return element(by.css(`#dropdownMenuCouncil li:nth-child(${index}) > a`));
  }


  // Options in Questionnaires dropdown
  getActiveQuestionnairesLink(): ElementFinder {
    return element(by.css('#dropdownMenuQuestionnaires li:nth-child(1) > a'));
  }

  getResultsLink(index: number): ElementFinder {
    return element(by.css(`#dropdownMenuQuestionnaires li:nth-child(${index}) > a`));
  }

  getDamageRequests() : ElementFinder{
    return element(by.css('.navbar-damage-request-option'));
  }


  // Options in Responsibility dropdown
  getDamagesResponsibleForInResponsibilityLink(): ElementFinder {
    return element(by.css('#dropdownMenuResponsibility li:nth-child(1) > a'));
  }

  getDamageTypesResponsibleForLink(): ElementFinder {
    return element(by.css('#dropdownMenuResponsibility li:nth-child(2) > a'));
  }


  // Options in Personal dropdown
  getProfileLink(): ElementFinder {
    return element(by.css('#dropdownMenuPersonal li:nth-child(1) > a'));
  }

  getLogoutLink(): ElementFinder {
    return element(by.css('#dropdownMenuPersonal li:nth-child(2) > a'));
  }


  // Admin navbar
  getAdminNavbar(): ElementFinder {
    return element(by.css('#adminsNavbar'));
  }


  // get admins links in navbar
  getAdmins(): ElementFinder {
    return element(by.css('.navbar-admins-option > a'));
  }

  getTenants(): ElementFinder {
    return element(by.css('.navbar-tenants-option > a'));
  }

  getBusinesses(): ElementFinder {
    return element(by.css('.navbar-businesses-option > a'));
  }

  getBuildings(): ElementFinder {
    return element(by.css('.navbar-buildings-option > a'));
  }

  getApartments(): ElementFinder {
    return element(by.css('.navbar-apartments-option > a'));
  }
}
