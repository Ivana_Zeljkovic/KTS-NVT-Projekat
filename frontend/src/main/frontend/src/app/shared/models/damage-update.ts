export class DamageUpdate {
  constructor(public description: string,
              public urgent: boolean) { }
}
