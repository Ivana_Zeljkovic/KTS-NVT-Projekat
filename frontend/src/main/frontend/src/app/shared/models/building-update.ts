import { AddressCreate } from "./address-create";
import { ApartmentUpdate } from "./apartment-update";

export class BuildingUpdate {
  constructor(private hasParking: boolean,
              private address: AddressCreate,
              private apartments: Array<ApartmentUpdate>,
              private numberOfFloors: number) { }
}
