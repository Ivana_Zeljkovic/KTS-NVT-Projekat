import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { HttpClient, HttpParams } from "@angular/common/http";


@Injectable()
export class LocationService {
  protected readonly urlBase = "/api";

  constructor(private http: HttpClient) { }

  getCoordinates(address: string): Observable<any> {
    let params = new HttpParams()
      .append('address', address);
    return this.http.get<any>(`${this.urlBase}/location`, {params});
  }
}
