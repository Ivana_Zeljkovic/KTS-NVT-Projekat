package rs.ac.uns.ftn.ktsnvt.service.unit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.ForbiddenException;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Meeting;
import rs.ac.uns.ftn.ktsnvt.model.entity.MeetingItem;
import rs.ac.uns.ftn.ktsnvt.model.entity.Question;
import rs.ac.uns.ftn.ktsnvt.model.entity.Questionnaire;
import rs.ac.uns.ftn.ktsnvt.repository.QuestionnaireRepository;
import rs.ac.uns.ftn.ktsnvt.service.QuestionnaireService;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ivana Zeljkovic on 01/12/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class QuestionnaireServiceUnitTest {

    @Autowired
    private QuestionnaireService questionnaireService;

    @MockBean
    private QuestionnaireRepository questionnaireRepository;


    @Before
    public void setUp() {
        // QUESTIONS FOR QUESTIONNAIRE 1
        Question question_1 = new Question();
        question_1.setContent("First question");
        List<String> answers_1 = new ArrayList<>();
        answers_1.add("Yes");
        answers_1.add("No");
        question_1.setAnswers(answers_1);

        Question question_2 = new Question();
        question_2.setContent("Second question");
        List<String> answers_2 = new ArrayList<>();
        answers_2.add("True");
        answers_2.add("False");
        question_2.setAnswers(answers_2);

        // QUESTIONNAIRE 1
        Questionnaire questionnaire_1 = new Questionnaire();
        questionnaire_1.setId(new Long(100));
        questionnaire_1.getQuestions().add(question_1);
        questionnaire_1.getQuestions().add(question_2);
        try {
            questionnaire_1.setDateExpire(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse("2018-01-01 10:00:00"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        // MEETING ITEM THAT CONTAINS QUESTIONNAIRE 1
        MeetingItem meetingItem = new MeetingItem();
        meetingItem.setId(100L);
        meetingItem.setQuestionnaire(questionnaire_1);
        questionnaire_1.setMeetingItem(meetingItem);

        Meeting meeting = new Meeting();
        meeting.setId(100L);
        meetingItem.setMeeting(meeting);
        meeting.getMeetingItems().add(meetingItem);


        // QUESTIONS FOR QUESTIONNAIRE 2
        Question question_3 = new Question();
        question_3.setContent("Some question");
        List<String> answers_3 = new ArrayList<>();
        answers_3.add("Confirm");
        answers_3.add("Decline");
        question_3.setAnswers(answers_3);

        // QUESTIONNAIRE 2
        Questionnaire questionnaire_2 = new Questionnaire();
        questionnaire_2.setId(new Long(101));
        questionnaire_2.getQuestions().add(question_3);
        try {
            questionnaire_2.setDateExpire(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse("2018-03-05 10:00:00"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        // MEETING ITEM THAT CONTAINS QUESTIONNAIRE 2
        MeetingItem meetingItem2 = new MeetingItem();
        meetingItem2.setId(200L);
        meetingItem2.setQuestionnaire(questionnaire_2);
        questionnaire_2.setMeetingItem(meetingItem2);
        
        
        //Questionaire 3 
        Questionnaire questionnaire_3 = new Questionnaire();
        questionnaire_3.setId(new Long(111));
        questionnaire_3.getQuestions().add(question_1);
        questionnaire_3.getQuestions().add(question_2);
        try {
            questionnaire_3.setDateExpire(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse("2018-07-06 10:00:00"));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //Meeting item that contains questionnaire 3
        MeetingItem meetingItem3 = new MeetingItem();
        meetingItem3.setId(220L);
        meetingItem3.setQuestionnaire(questionnaire_3);
        questionnaire_3.setMeetingItem(meetingItem3);

        Meeting meeting2 = new Meeting();
        meeting2.setId(120L);
        meetingItem3.setMeeting(meeting2);
        meeting2.getMeetingItems().add(meetingItem3);
        

        given(
                this.questionnaireRepository.findOne(new Long(100))
        ).willReturn(
                questionnaire_1
        );

        given(
                this.questionnaireRepository.findOne(new Long(101))
        ).willReturn(
                questionnaire_2
        );

        given(
                this.questionnaireRepository.findOne(new Long(102))
        ).willReturn(
                null
        );

        given(
                this.questionnaireRepository.findOne(new Long(111))
        ).willReturn(
                questionnaire_3
        );

        given(
                this.questionnaireRepository.findOneByIdAndBillboardId(new Long(100), new Long(100))
        ).willReturn(
                questionnaire_1
        );

        given(
                this.questionnaireRepository.findOneByIdAndBillboardId(new Long(101), new Long(100))
        ).willReturn(
                null
        );
    }

    @Test
    public void testFindOne() {
        /*
         * Test proverava ispravnost metode findOne servisa QuestionnaireService
         * (dobavljanje objekta ankete na osnovu zadatog ID-a)
         * u slucaju kada postoji anketa sa zadatim ID-em
         * */

        /*
        postoji anketa sa ID-em = 100 i podacima:
            glasovi = prazna lista,
            pitanja = lista sa 2 pitanja
         */
        Questionnaire questionnaire = this.questionnaireService.findOne(new Long(100));

        assertNotNull(questionnaire);
        assertEquals(Long.valueOf(100), questionnaire.getId());
        assertEquals(0, questionnaire.getVotes().size());
        assertEquals(2, questionnaire.getQuestions().size());
    }

    @Test(expected = NotFoundException.class)
    public void testFindOneNonExistent() {
        /*
         * Test proverava ispravnost metode findOne servisa QuestionnaireService
         * (dobavljanje objekta ankete na osnovu zadatog ID-a)
         * u slucaju kada postoji anketa sa zadatim ID-em - ocekivano NotFoundException
         * */

        // ne postoji anketa sa ID-em = 102
        Questionnaire questionnaire = this.questionnaireService.findOne(new Long(102));
    }

    @Test
    public void testCheckQuestionnaireExpiredAlready() {
        /*
         * Test proverava ispravnost metode checkQuestionnaireExpired servisa QuestionnaireService
         * (provera da li je vreme glasanja na anketa sa zadatim ID-em isteklo)
         * u slucaju kada je vreme glasanja na anketu sa zadatim ID-em isteklo
         * */

        Questionnaire questionnaire = this.questionnaireService.findOne(new Long(100));

        // vreme glasanja za anketu sa ID-em = 100 je isteklo, pa metoda servisa nece vratiti nikakvu gresku
        this.questionnaireService.checkQuestionnaireExpired(questionnaire);
    }

    @Test(expected = BadRequestException.class)
    public void testCheckQuestionnaireExpired() {
        /*
         * Test proverava ispravnost metode checkQuestionnaireExpired servisa QuestionnaireService
         * (provera da li je vreme glasanja na anketa sa zadatim ID-em isteklo)
         * u slucaju kada vreme glasanja na anketu sa zadatim ID-em nije isteklo - ocekivano BadRequestException
         * */

        Questionnaire questionnaire = this.questionnaireService.findOne(new Long(101));

        // vreme glasanja za anketu sa ID-em = 101 nije isteklo, pa metoda servisa baca izuzetak
        this.questionnaireService.checkQuestionnaireExpired(questionnaire);
    }

    @Test
    public void testIsVoteEnabled() {
        /*
         * Test proverava ispravnost metode isVoteEnabled servisa QuestionnaireService
         * (provera da li je dozvoljeno glasanje na prosledjenu anketu)
         * u slucaju kada je glasanje dozvoljeno
         * */

        /*
        postoji anketa sa ID-em = 100 koja pripada tacki dnevnog reda sa ID-em = 100 koja je povezana sa sastankom sa ID-em = 100,
        te je glasanje na ovu anketu moguce
        */

        Questionnaire questionnaire = this.questionnaireService.findOne(111L);
        this.questionnaireService.isVoteEnabled(questionnaire);
    }

    @Test(expected = ForbiddenException.class)
    public void testIsVoteEnabledInvalid() {
        /*
         * Test proverava ispravnost metode isVoteEnabled servisa QuestionnaireService
         * (provera da li je dozvoljeno glasanje na prosledjenu anketu)
         * u slucaju kada glasanje nije dozvoljeno - ocekivano ForbiddenException
         * */

        /*
        u bazi postoji anketa sa ID-em = 101 koja pripada tacki dnevnog reda sa ID-em = 200 koja nije povezana (preuzeta) ni sa jednim
        sastankom, te glasanje na ovu anketu jos uvek nije moguce
        */

        Questionnaire questionnaire = this.questionnaireService.findOne(101L);
        this.questionnaireService.isVoteEnabled(questionnaire);
    }

    @Test
    public void testCheckBuildingForQuestionnaire() {
        /*
         * Test proverava ispravnost metode checkBuildingForQuestionnaire servisa QuestionnaireService
         * (provera da li anketa sa zadatim Id-em pripada zgradi sa zadatim ID-em njenog bilborda)
         * u slucaju kada anketa pripada
         * */

        this.questionnaireService.checkBuildingForQuestionnaire(100L, 100L);
    }

    @Test(expected = BadRequestException.class)
    public void testCheckBuildingForQuestionnaireInvalid() {
        /*
         * Test proverava ispravnost metode checkBuildingForQuestionnaire servisa QuestionnaireService
         * (provera da li anketa sa zadatim Id-em pripada zgradi sa zadatim ID-em njenog bilborda)
         * u slucaju kada anketa ne pripada zadatoj zgradi
         * */

        this.questionnaireService.checkBuildingForQuestionnaire(101L, 100L);
    }
}
