package rs.ac.uns.ftn.ktsnvt.service.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Account;
import rs.ac.uns.ftn.ktsnvt.service.AccountService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Katarina Cukurov on 30/11/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class AccountServiceIntegrationTest {

    @Autowired
    private AccountService accountService;


    @Test
    public void testFindOne() {
        /*
         * Test proverava ispravnost rada metode findOne servisa AccountService
         * (dobavljanje naloga na osnovu ID-a)
         * u slucaju kada postoji nalog sa zadatim ID-em
         * */

        /*
        nalog sa ID-em = 101 ima podatke:
            id = 101,
            username = kaca,
            password = kaca ($2a$10$FXALP0p8VlmzCbbCTDTV8uIS46lpCbCpeIq74qulCyxzhCsOazAZS),
            tenant_id = 101
         */
        Account account = this.accountService.findOne(new Long(101));

        assertNotNull(account);
        assertEquals(Long.valueOf(101), account.getId());
        assertEquals("kaca", account.getUsername());
        assertEquals("$2a$10$FXALP0p8VlmzCbbCTDTV8uIS46lpCbCpeIq74qulCyxzhCsOazAZS", account.getPassword());
        assertEquals(Long.valueOf(101), account.getTenant().getId());
    }

    @Test(expected = NotFoundException.class)
    public void testFindOneNonexistent() {
        /*
         * Test proverava ispravnost rada metode findOne servisa AccountService
         * (dobavljanje naloga na osnovu ID-a)
         * u slucaju kada ne postoji nalog sa zadatim ID-em - ocekivano NotFoundException
         * */

        // account sa ID-em = 301 ne postoji
        Account account = this.accountService.findOne(new Long(301));
    }

    @Test
    public void testFindByUsername() {
        /*
         * Test proverava ispravnost rada metode findByUsername servisa AccountService
         * (dobavljanje naloga na osnovu korisnickog imena)
         * u slucaju kada postoji nalog sa zadatim korisnickim imenom
         * */

        /*
        nalog sa korisnickim imenom "kaca" ima od podataka:
            id = 101,
            username = kaca,
            password = kaca ($2a$10$FXALP0p8VlmzCbbCTDTV8uIS46lpCbCpeIq74qulCyxzhCsOazAZS),
            tenant_id = 101
        */
        Account account = this.accountService.findByUsername("kaca");

        assertNotNull(account);
        assertEquals(Long.valueOf(101), account.getId());
        assertEquals("kaca", account.getUsername());
        assertEquals("$2a$10$FXALP0p8VlmzCbbCTDTV8uIS46lpCbCpeIq74qulCyxzhCsOazAZS", account.getPassword());
        assertEquals(Long.valueOf(101), account.getTenant().getId());
    }

    @Test(expected = NotFoundException.class)
    public void testFindByUsernameNonexistent() {
        /*
         * Test proverava ispravnost rada metode findByUsername servisa AccountService
         * (dobavljanje naloga na osnovu korisnickog imena)
         * u slucaju kada ne postoji nalog sa zadatim korisnickim imenom - ocekivano NotFoundException
         * */

        // ne postoji account sa korisnickim imenom acak
        Account account = this.accountService.findByUsername("acak");
    }

    @Test(expected = BadRequestException.class)
    public void testCheckUsername(){
        /*
         * Test proverava ispravnost rada metode checkUsername servisa AccountService
         * (provera da li je neko korisnicko ime vec zauzeto)
         * u slucaju kada postoji nalog sa zadatim korisnickim imenom - ocekivano BadRequestException
         * */

        this.accountService.checkUsername("kaca");
    }
}
