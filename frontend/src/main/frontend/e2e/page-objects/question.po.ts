import {by, element, ElementArrayFinder, ElementFinder} from "protractor";

export class QuestionPage {
  // get methods
  getQuestionElement(): ElementFinder {
    return element(by.tagName('app-question'));
  }

  getContentInput(): ElementFinder {
    return element(by.css('app-question #content'));
  }

  getAnswerInput(): ElementFinder {
    return element(by.id('newAnswer'));
  }

  getAddAnswerButton(): ElementFinder {
    return element(by.css('.btn-add-answer'));
  }

  getAnswersElement(): ElementFinder {
    return element(by.css('ul.answer-list'));
  }

  getAllAnswers(): ElementArrayFinder {
    return element.all(by.css('ul.answer-list > li'));
  }

  getAnswer(index: number): ElementFinder {
    return element(by.css(`ul.answer-list > li:nth-child(${index+1})`));
  }

  getSaveButton(): ElementFinder {
    return element(by.id('buttonSave'));
  }

  getResetButton(): ElementFinder {
    return element(by.id('buttonReset'));
  }

  getCancelButton(): ElementFinder {
    return element(by.id('buttonCancel'));
  }

  // set methods
  setContent(text: string) {
    this.getContentInput().sendKeys(text);
  }

  setNewAnswer(text: string) {
    this.getAnswerInput().sendKeys(text);
  }

  //get error methods
  getContentRequiredError(): ElementFinder {
    return element(by.id('contentRequiredError'));
  }

  getOnlyOneAnswerError(): ElementFinder {
    return element(by.id('onlyOneAnswerError'));
  }
}
