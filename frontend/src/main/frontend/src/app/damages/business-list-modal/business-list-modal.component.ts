import { Component, OnInit } from '@angular/core';
import { ToasterConfig, ToasterService } from 'angular5-toaster/dist';
import { BsModalRef } from 'ngx-bootstrap';
// model
import { Business } from '../../shared/models/business';
import { PageAndSort } from '../../shared/models/page-and-sort';
import { Damage } from '../../shared/models/damage';
 //service
import { BusinessService } from '../../core/services/business.service';
import { DamageService } from '../../core/services/damage.service';
import { JwtService } from '../../core/services/jwt.service';
// error
import { BadRequestError } from '../../shared/errors/bad-request-error';
import { AppError } from "../../shared/errors/app-error";
import {NotFoundError} from "../../shared/errors/not-found-error";


@Component({
  selector: 'app-business-list-modal',
  templateUrl: './business-list-modal.component.html',
  styleUrls: ['./business-list-modal.component.css']
})
export class BusinessListModalComponent implements OnInit {
  pageSize: string;
  currentPage: number;
  maxSize: number;
  itemsPerPage: number;
  sortDirection: string;
  sortProperties: [string];
  totalItems: number;

  toasterConfig: ToasterConfig;

  damage: Damage;
  businesses: Array<Business>;

  requests: number[] = [];


  constructor(private businessService: BusinessService, public modalRef: BsModalRef,
              private damageService: DamageService, private jwtService: JwtService,
              private toasterService: ToasterService) {
    // Sett up for bootstrap paging
    this.currentPage = 1;
    this.maxSize = 3;
    this.itemsPerPage = 4;

    // Sett up for getting damages request
    this.pageSize = '4';
    this.sortDirection = 'ASC';
    this.sortProperties = ['id'];

    this.toasterConfig = new ToasterConfig({
      timeout: {success: 2000, error: 4000, warning: 4000}
    });
  }

  ngOnInit() {
    setTimeout(() => this.loadBusinesses(), 50);
  }

  loadBusinesses() {
    let page = new PageAndSort((this.currentPage - 1).toString(), this.pageSize, this.sortDirection, this.sortProperties);
    this.businessService.getPage(page, 'businesses/damages', this.damage.id, 'request_not_sent')
      .subscribe(data => {
        this.totalItems = data.length == 0 ? 0 : data[0].numberOfElements;
        this.businesses = data;
      }, error => {
        this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
        throw error;
      });
  }

  pageChanged(event: any) {
    this.currentPage = event.page;
    this.loadBusinesses();
  }

  addToRequestList(business: Business) {
    let b = this.businessInList(business);

    if (b != -1)
      this.requests.splice(b, 1);
    else
      this.requests.push(business.id);
  }

  businessInList(business) {
    return this.requests.indexOf(business.id);
  }

  sendAllOffers() {
    this.damageService.updatePart({businessId: this.requests}, 'buildings', this.damage.buildingId,
      'damages', this.damage.id, 'send_email_to_firm')
      .subscribe(() => {
        this.toasterService.pop('success', 'Success', "Emails have been successfully sent!");
        this.modalRef.hide();
      }, (error: AppError) => {
         if(error instanceof BadRequestError)
           this.toasterService.pop('error', 'Error', 'There was an error while sending the emails to businesses');
         else {
           this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
           throw error;
         }
      });
  }
}
