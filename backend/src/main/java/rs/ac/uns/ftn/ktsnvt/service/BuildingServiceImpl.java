package rs.ac.uns.ftn.ktsnvt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Building;
import rs.ac.uns.ftn.ktsnvt.repository.BuildingRepository;

import java.util.List;

/**
 * Created by Ivana Zeljkovic on 05/11/2017.
 */
@Service
public class BuildingServiceImpl implements BuildingService {

    @Autowired
    private BuildingRepository buildingRepository;


    @Override
    @Transactional(readOnly = true)
    public List<Building> findAll() {
        return this.buildingRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Building> findAll(Pageable page) {
        return this.buildingRepository.findAll(page);
    }

    @Override
    @Transactional(readOnly = true)
    public Building findOne(Long id) {
        Building building = this.buildingRepository.findOne(id);
        if(building == null) throw new NotFoundException("Building not found!");
        return building;
    }

    @Override
    @Transactional(readOnly = false)
    public Building save(Building building) {
        return this.buildingRepository.save(building);
    }

    @Override
    @Transactional(readOnly = true)
    public Building findByAddressId(Long addressId) {
        Building building = this.buildingRepository.findByAddressId(addressId);
        if(building == null) throw new NotFoundException("Building not found!");
        return building;
    }

    @Override
    @Transactional(readOnly = false)
    public void remove(Long id) {
        this.buildingRepository.delete(id);
    }

    @Override
    public Page<Building> findAllByFirm(Long id, Integer pageNumber, Integer pageSize, String sortDirection, String sortProperty) {
        return this.buildingRepository.findAllByFirm(id, new PageRequest
                (pageNumber, pageSize, (sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortProperty));
    }

    @Override
    @Transactional(readOnly = true)
    public List<Building> findByCityId(Long cityId){
        return this.buildingRepository.findByCity(cityId);
    }

    @Override
    @Transactional(readOnly = true)
    public Building findOneByBillboardId(Long billboardId) {
        return this.buildingRepository.findByBillboardId(billboardId);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Building> getAllBuildings(Integer pageNumber, Integer pageSize,
                                            String sortDirection, String sortProperty) {
        return this.buildingRepository.findAll(new PageRequest(pageNumber, pageSize,
                (sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortProperty));
    }
}
