

import {by, element} from "protractor";

export class BusinessProfileAccountsModalPage{

  getComponent(){
    return element(by.tagName('app-business-profile-users-modal'));
  }

  getCloseButton(){
    return element(by.css('.modal-header > button'));
  }

  getAllAccountsList(){
    return element.all(by.css('.modal-body  li'));
  }

  getOpenAddAccountModal(){
    return element(by.css('.btn.btn-new-account'));
  }

  getModalFooter() {
    return element(by.css('.modal-footer'));
  }
}
