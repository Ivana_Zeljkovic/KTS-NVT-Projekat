package rs.ac.uns.ftn.ktsnvt.web.util;

import org.modelmapper.ModelMapper;
import rs.ac.uns.ftn.ktsnvt.model.entity.*;
import rs.ac.uns.ftn.ktsnvt.web.dto.*;

/**
 * Created by Ivana Zeljkovic on 25/11/2017.
 */
public class ConverterDTOToModel {

    private ConverterDTOToModel() { }

    public static Address convertAdressDTOToAddress(AddressDTO addressDTO)
    {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(addressDTO, Address.class);
    }

    public static City convertCityDTOToCity(CityDTO cityDTO)
    {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(cityDTO, City.class);
    }

    public static Administrator convertAdminCreateDTOToAdministrator(AdminCreateDTO adminCreateDTO)
    {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(adminCreateDTO, Administrator.class);
    }

    public static Tenant convertTenantCreateDTOToTenant(TenantCreateDTO tenantCreateDTO)
    {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(tenantCreateDTO, Tenant.class);
    }

    public static Building convertBuildingCreateDTOtoBuilding(BuildingCreateDTO buildingCreateDTO)
    {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(buildingCreateDTO, Building.class);
    }

    public static Business convertBusinessCreateDTOToBusiness(BusinessCreateDTO businessCreateDTO){
        ModelMapper mapper = new ModelMapper();
        return mapper.map(businessCreateDTO, Business.class);
    }

    public static Comment convertCommentCreateDTOToComment(CommentCreateOrUpdateDTO commentCreateOrUpdateDTO) {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(commentCreateOrUpdateDTO, Comment.class);
    }

    public static Notification convertNotificationCreateUpdateDTOToNotification(NotificationCreateUpdateDTO notificationCreateUpdateDTO) {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(notificationCreateUpdateDTO, Notification.class);
    }

    public static Question convertQuestionCreateDTOToQuestion(QuestionCreateDTO questionCreateDTO) {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(questionCreateDTO, Question.class);
    }

    public static Apartment convertApartmentCreateDTOToApartment(ApartmentCreateDTO apartmentCreateDTO){
        ModelMapper mapper = new ModelMapper();
        return mapper.map(apartmentCreateDTO, Apartment.class);
    }
}
