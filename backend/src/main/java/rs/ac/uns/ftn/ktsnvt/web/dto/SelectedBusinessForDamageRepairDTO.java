package rs.ac.uns.ftn.ktsnvt.web.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by Ivana Zeljkovic on 24/11/2017.
 */
public class SelectedBusinessForDamageRepairDTO implements Serializable {

    @NotNull
    private Long id;
    private Long offerId;

    public SelectedBusinessForDamageRepairDTO() { }

    public SelectedBusinessForDamageRepairDTO(Long id, Long offerId) {
        this.id = id;
        this.offerId = offerId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOfferId() {
        return offerId;
    }

    public void setOfferId(Long offerId) {
        this.offerId = offerId;
    }
}
