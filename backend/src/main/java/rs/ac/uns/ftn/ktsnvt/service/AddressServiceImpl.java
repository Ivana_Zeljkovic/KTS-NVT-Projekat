package rs.ac.uns.ftn.ktsnvt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Address;
import rs.ac.uns.ftn.ktsnvt.repository.AddressRepository;

/**
 * Created by Katarina Cukurov on 07/11/2017.
 */
@Service
public class AddressServiceImpl implements AddressService {

    @Autowired
    private AddressRepository addressRepository;


    @Override
    @Transactional(readOnly = true)
    public Address findOne(Long id) {
        Address address = this.addressRepository.findOne(id);
        if(address == null) throw new NotFoundException("Address not found!");
        return address;
    }

    @Override
    @Transactional(readOnly = false)
    public Address save(Address building) {
        return this.addressRepository.save(building);
    }

    @Override
    @Transactional(readOnly = true)
    public Address findByCityStreetNumber(Long cityId, String street, int number) {
        return this.addressRepository.findByCityStreetNumber(cityId, street, number);
    }

    @Override
    @Transactional(readOnly = false)
    public void remove(Long id) {
        this.addressRepository.delete(id);
    }
}
