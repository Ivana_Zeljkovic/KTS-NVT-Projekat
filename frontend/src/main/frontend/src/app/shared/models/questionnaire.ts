import { Question } from "./question";
import { Vote } from "./vote";

export interface Questionnaire {
  id: number;
  questions: Array<Question>;
  votes: Array<Vote>;
  dateExpire: Date;
  buildingId: number;
}
