export class DamageForMeetingItem {
  constructor(public id: number,
              public description: string) { }
}
