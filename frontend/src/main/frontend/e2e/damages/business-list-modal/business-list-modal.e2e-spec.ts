import {LoginPage} from "../../page-objects/login.po";
import {browser, by, element} from "protractor";
import {DamageListPage} from "../../page-objects/damage-list.po";
import {BusinessListModalPage} from "./page-objects/buisness-list-modal.po";

describe('Modal for sending requests for fixing the damage to businesses', () => {

  let loginPage: LoginPage;
  let damageList: DamageListPage;
  let businessListModal: BusinessListModalPage;

  beforeAll(() => {
    loginPage = new LoginPage();
    damageList = new DamageListPage();
    businessListModal = new BusinessListModalPage();

    browser.get('/');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');

    loginPage.setUsername('kaca');
    loginPage.setPassword('kaca');
    loginPage.getSignInButton().click();

    browser.wait(function () {
      return browser.getCurrentUrl().then(value => {
        return value == 'http://localhost:49152/#/home/notifications'
      });
    }).then(function () {
      browser.get('http://localhost:49152/#/damages');
      browser.wait(function () {
        return browser.getCurrentUrl().then(value => {
          return value == 'http://localhost:49152/#/damages'
        });
      });
    });
  });

  it('Should click on an offer and send it and then the number of firms in table will change', () => {
    let businessName = null;
    damageList.getMenuFromDamagePanel(0).click().then(() => {
      damageList.getSendRequestForOffer().click().then(function () {
        browser.wait(function () {
          return businessListModal.getComponent().isDisplayed();
        }).then(function () {

          expect(businessListModal.getComponent().isDisplayed()).toBe(true);
          browser.wait(function () {
            return businessListModal.getBlueButtons().count().then(function (value) {
              return value == 4;
            });
          }).then(function () {
              businessName = businessListModal.getFirstBusinessName();
              businessListModal.getBlueButtons().get(0).click().then(function () {
                expect(businessListModal.getRedButtons().count()).toBe(1);
                businessListModal.getSendOffersButton().click().then(function () {
                  browser.wait(function () {
                    return businessListModal.getComponent().isPresent().then(function (visible) {
                      return !visible;
                    });
                  }).then(function () {
                    expect(businessListModal.getComponent().isPresent()).toBe(false);
                    damageList.getMenuFromDamagePanel(0).click().then(() => {
                      damageList.getSendRequestForOffer().click().then(function () {
                        browser.wait(function () {
                          return businessListModal.getComponent().isDisplayed();
                        }).then(function () {

                          expect(businessListModal.getComponent().isDisplayed()).toBe(true);
                          expect(businessListModal.getFirstBusinessName()).not.toBe(businessName);
                        });
                      });
                    });
                  });
                })
              });
            }
          );
        })
      })
    });
  });

  afterAll(() => {
    browser.executeScript('window.localStorage.clear();');
  })


});


/*

 */
