import { browser } from "protractor";
// page
import { NavbarPage } from "../page-objects/navbar.po";
import { LoginPage } from "../page-objects/login.po";

describe('Navbar for tenants review', () => {
  let navbarPage: NavbarPage;
  let loginPage: LoginPage;

  beforeEach(() => {
    loginPage = new LoginPage();
    navbarPage = new NavbarPage();

    browser.get('/');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');

    loginPage.setUsername('ivana');
    loginPage.setPassword('ivana');
    loginPage.getSignInButton().click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/home');
  });

  afterEach(() => {
    browser.executeScript('window.localStorage.clear();');
  });

  it('should display navbar when administrator is logged in on home page', () => {
    expect(navbarPage.getHomeLink().isDisplayed()).toBe(true);
    expect(navbarPage.getPersonalDropdown().getText()).toEqual('ivana');

  });

  it('should display navbar when administrator is logged in and not on home page', () => {

    browser.get("http://localhost:49152/#/admins");
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/admins');
    expect(navbarPage.getHomeLink().isDisplayed()).toBe(true);
    expect(navbarPage.getAdmins().isDisplayed()).toBe(true);
    expect(navbarPage.getTenants().isDisplayed()).toBe(true);
    expect(navbarPage.getBusinesses().isDisplayed()).toBe(true);
    expect(navbarPage.getBuildings().isDisplayed()).toBe(true);
    expect(navbarPage.getApartments().isDisplayed()).toBe(true);
    expect(navbarPage.getPersonalDropdown().getText()).toEqual('ivana');

  });

  it('should go to admins list page when clicked on admins link', () => {
    browser.get("http://localhost:49152/#/businesses");
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/businesses');
    expect(navbarPage.getAdmins().isDisplayed()).toBe(true);
    navbarPage.getAdmins().click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/admins');
  });

  it('should go to businesses list page when clicked on businesses link', () => {
    browser.get("http://localhost:49152/#/admins");
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/admins');
    expect(navbarPage.getBusinesses().isDisplayed()).toBe(true);
    navbarPage.getBusinesses().click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/businesses');
  });

  it('should go to tenant list page when clicked on tenant link', () => {
    browser.get("http://localhost:49152/#/admins");
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/admins');
    expect(navbarPage.getTenants().isDisplayed()).toBe(true);
    navbarPage.getTenants().click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/tenants');
  });

  it('should go to building list page when clicked on building link', () => {
    browser.get("http://localhost:49152/#/admins");
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/admins');
    expect(navbarPage.getBuildings().isDisplayed()).toBe(true);
    navbarPage.getBuildings().click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/buildings');
  });

  it('should go to apartments list page when clicked on apartment link', () => {
    browser.get("http://localhost:49152/#/admins");
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/admins');
    expect(navbarPage.getApartments().isDisplayed()).toBe(true);
    navbarPage.getApartments().click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/apartments');
  });

});
