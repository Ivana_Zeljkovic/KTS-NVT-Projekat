package rs.ac.uns.ftn.ktsnvt.service.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Building;
import rs.ac.uns.ftn.ktsnvt.model.entity.Tenant;
import rs.ac.uns.ftn.ktsnvt.service.BuildingService;
import rs.ac.uns.ftn.ktsnvt.service.TenantService;

import static org.junit.Assert.*;

/**
 * Created by Ivana Zeljkovic on 01/12/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class TenantServiceIntegrationTest {

    @Autowired
    private TenantService tenantService;

    @Autowired
    private BuildingService buildingService;


    @Test
    @Transactional
    public void testFindOne() {
        /*
         * Test proverava ispravnost metode findOne servisa TenantService
         * (dobavljanje objekta stanara sa zadatim ID-em)
         * u slucaju kada postoji stanar sa zadatim ID-em
         * */

        /*
        u bazi postoji stanar sa ID-em = 101 i podacima:
            ime = Katarina,
            prezime = Cukurov,
            stan u kom zivi ima ID = 101
         */
        Tenant tenant = this.tenantService.findOne(new Long(101));

        assertNotNull(tenant);
        assertEquals(Long.valueOf(101), tenant.getId());
        assertEquals("Katarina", tenant.getFirstName());
        assertEquals("Cukurov", tenant.getLastName());
        assertEquals(Long.valueOf(101), tenant.getApartmentLiveIn().getId());
    }

    @Test(expected = NotFoundException.class)
    public void testFindOneNonexistent() {
        /*
         * Test proverava ispravnost metode findOne servisa TenantService
         * (dobavljanje objekta stanara sa zadatim ID-em)
         * u slucaju kada postoji ne stanar sa zadatim ID-em - ocekivano NotFoundException
         * */

        // u bazi ne postoji stanar sa ID-em = 200
        Tenant tenant = this.tenantService.findOne(new Long(200));
    }

    @Test
    public void testCheckPasswordUpdate() {
        /*
         * Test proverava ispravnost rada metode checkPasswordUpdate servisa TenantService
         * (provera prava izmene trenutne vrednosti lozinke)
         * u slucaju kada su vrednosti trenutne lozinke i nove lozinke ispravne (nisu null, trenutna vrednost lozinke
         * odgovara vrednosti lozinke koja je prezistenta)
         * */

        /*
        stanar sa ID-em = 101 ima sledece podatke:
            ime = Katarina,
            prezime = Cukurov,
            korisnicko ime = kaca,
            lozinka = kaca
         */
        Tenant tenant = this.tenantService.findOne(new Long(101));
        String current_password = "kaca";
        String new_password = "novi";

        boolean password_changed = this.tenantService.checkPasswordUpdate(tenant, current_password, new_password);

        assertTrue(password_changed);

        BCryptPasswordEncoder crypt = new BCryptPasswordEncoder();
        boolean password_matched = crypt.matches(new_password, tenant.getAccount().getPassword());

        assertTrue(password_matched);
    }

    @Test(expected = BadRequestException.class)
    public void testCheckPasswordUpdateMissingNewPass() {
        /*
         * Test proverava ispravnost rada metode checkPasswordUpdate servisa TenantService
         * (provera prava izmene trenutne vrednosti lozinke)
         * u slucaju kada je vrednost trenutne lozinke ispravna (nije null i odgovara vrednosti lozinke koja je prezistenta)
         * i nedostaje vrednost nove lozinke - ocekivano BadRequestException
         * */

        /*
        stanar sa ID-em = 101 ima sledece podatke:
            ime = Katarina,
            prezime = Cukurov,
            korisnicko ime = kaca,
            lozinka = kaca
         */
        Tenant tenant = this.tenantService.findOne(new Long(101));
        String current_password = "kaca";

        boolean password_changed = this.tenantService.checkPasswordUpdate(tenant, current_password, null);
    }

    @Test(expected = BadRequestException.class)
    public void testCheckPasswordUpdateMissingCurrentPass() {
        /*
         * Test proverava ispravnost rada metode checkPasswordUpdate servisa TenantService
         * (provera prava izmene trenutne vrednosti lozinke)
         * u slucaju kada je vrednost nove lozinke ispravna (nije null)
         * i nedostaje vrednost trenutne lozinke - ocekivano BadRequestException
         * */

        /*
        stanar sa ID-em = 101 ima sledece podatke:
            ime = Katarina,
            prezime = Cukurov,
            korisnicko ime = kaca,
            lozinka = kaca
         */
        Tenant tenant = this.tenantService.findOne(new Long(101));
        String new_password = "novi";

        boolean password_changed = this.tenantService.checkPasswordUpdate(tenant, null, new_password);
    }

    @Test(expected = BadRequestException.class)
    public void testCheckPasswordUpdateInvalidCurrentPass() {
        /*
         * Test proverava ispravnost rada metode checkPasswordUpdate servisa TenantService
         * (provera prava izmene trenutne vrednosti lozinke)
         * u slucaju kada je vrednost trenutne lozinke neispravna (nije null ali ne odgovara vrednosti lozinke koja je prezistenta)
         * i vrednost nove lozinke ispravna (nije null) - ocekivano BadRequestException
         * */

        /*
        stanar sa ID-em = 101 ima sledece podatke:
            ime = Katarina,
            prezime = Cukurov,
            username = kaca,
            password = kaca
         */
        Tenant tenant = this.tenantService.findOne(new Long(101));
        String current_password = "nevalidno";
        String new_password = "novi";

        boolean password_changed = this.tenantService.checkPasswordUpdate(tenant, current_password, new_password);
    }

    @Test
    @Transactional
    public void testCheckTenantsPersonalApartments() {
        /*
         * Test proverava ispravnost rada metode checkTenantsPersonalApartments servisa TenantService
         * (dobavljanje indikatora da li zadati stanar poseduje bar jedan stan u zgradi sa zadatim ID-em)
         * u slucaju kada zadati stanar poseduje bar jedan stan u zgradi sa zadatim ID-em
         * */

        Tenant tenant = this.tenantService.findOne(101L);

        // stanar sa ID-em = 101 poseduje 2 stana u zgradi sa ID-em = 100
        boolean has_apartments = this.tenantService.checkTenantsPersonalApartments(tenant, 100L);

        assertTrue(has_apartments);
    }

    @Test
    @Transactional
    public void testCheckTenantsPersonalApartmentsNonexistent() {
        /*
         * Test proverava ispravnost rada metode checkTenantsPersonalApartments servisa TenantService
         * (dobavljanje indikatora da li zadati stanar poseduje bar jedan stan u zgradi sa zadatim ID-em)
         * u slucaju kada zadati stanar ne poseduje nijedan stan u zgradi sa zadatim ID-em
         * */

        Tenant tenant = this.tenantService.findOne(101L);

        // stanar sa ID-em = 101 ne poseduje stanove u zgradi sa ID-em = 101
        boolean has_apartments = this.tenantService.checkTenantsPersonalApartments(tenant, 101L);

        assertFalse(has_apartments);
    }

    @Test
    @Transactional
    public void testFindOneByFirstNameAndLastNameAndUsername() {
        /*
         * Test proverava ispravnost metode findOneByFirstNameAndLastNameAndUsername servisa TenantService
         * (dobavljanje objekta stanara na osnovu zadatog imena, prezimena i korisnickog imena)
         * u slucaju kada postoji stanar kom odgovara zadato ime, prezime i korisnicko ime
         * */

        /*
        u bazi postoji stanar sa ID-em = 101 ima sledece podatke:
            ime = Katarina,
            prezime = Cukurov,
            korisnicko ime = kaca
         */
        Tenant tenant = this.tenantService.findOneByFirstNameAndLastNameAndUsername("Katarina", "Cukurov", "kaca");

        assertNotNull(tenant);
        assertEquals(Long.valueOf(101), tenant.getId());
        assertEquals("Katarina", tenant.getFirstName());
        assertEquals("Cukurov", tenant.getLastName());
        assertEquals("kaca", tenant.getAccount().getUsername());
    }

    @Test(expected = NotFoundException.class)
    @Transactional
    public void testFindOneByFirstNameAndLastNameAndUsernameNonexistent() {
        /*
         * Test proverava ispravnost metode findOneByFirstNameAndLastNameAndUsername servisa TenantService
         * (dobavljanje objekta stanara na osnovu zadatog imena, prezimena i korisnickog imena)
         * u slucaju kada ne postoji stanar kom odgovara zadato ime, prezime i korisnicko ime - ocekivano NotFoundException
         * */

        /*
        u bazi ne postoji stanar koji ima sledece podatke:
            ime = Katarina,
            prezime = Cukurov,
            korisnicko ime = ivana
         */
        Tenant tenant = this.tenantService.findOneByFirstNameAndLastNameAndUsername("Katarina", "Cukurov", "ivana");
    }

    @Test(expected = NullPointerException.class)
    public void testCheckPermissionForCurrentTenant() {
        /*
         * Test proverava ispravnost rada metode checkPermissionForCurrentTenant servisa TenantService
         * (provera poklapanja ID-a trenutno logovanog stanara i prosledjenog ID-a)
         * u slucaju kada nema trenutno ulogovanog stanara na nivou aplikacije - ocekivano NullPointerException
         * */

        // trenutno nema stanara na sesiji
        this.tenantService.checkPermissionForCurrentTenant(new Long(101));
    }

    @Test(expected = NullPointerException.class)
    public void testCheckPermissionForCurrentTenantInBuilding() {
        /*
         * Test proverava ispravnost rada metode checkPermissionForCurrentTenantInBuilding servisa TenantService
         * (provera da li trenutno logovani stanar zivi u zgradi koja je prosledjena)
         * u slucaju kada nema trenutno ulogovanog stanara na nivou aplikacije - ocekivano NullPointerException
         * */

        Building building = this.buildingService.findOne(new Long(100));

        // trenutno nema stanara na sesiji
        this.tenantService.checkPermissionForCurrentPresidentInBuilding(building);
    }

    @Test(expected = NullPointerException.class)
    public void testCheckPermissionForCurrentPresidentInBuilding() {
        /*
         * Test proverava ispravnost rada metode checkPermissionForCurrentPresidentInBuilding servisa TenantService
         * (provera da li je trenutno logovani stanar predsednik skupstine stanara u zgradi koja je prosledjena)
         * u slucaju kada nema trenutno ulogovanog stanara na nivou aplikacije - ocekivano NullPointerException
         * */

        Building building = this.buildingService.findOne(new Long(100));

        // trenutno nema stanara na sesiji
        this.tenantService.checkPermissionForCurrentPresidentInBuilding(building);
    }
}
