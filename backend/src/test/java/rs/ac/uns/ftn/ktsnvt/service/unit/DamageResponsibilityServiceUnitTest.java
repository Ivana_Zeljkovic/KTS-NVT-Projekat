package rs.ac.uns.ftn.ktsnvt.service.unit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.exception.ForbiddenException;
import rs.ac.uns.ftn.ktsnvt.model.entity.*;
import rs.ac.uns.ftn.ktsnvt.repository.BuildingRepository;
import rs.ac.uns.ftn.ktsnvt.repository.BusinessRepository;
import rs.ac.uns.ftn.ktsnvt.repository.DamageResponsibilityRepository;
import rs.ac.uns.ftn.ktsnvt.repository.TenantRepository;
import rs.ac.uns.ftn.ktsnvt.service.BuildingService;
import rs.ac.uns.ftn.ktsnvt.service.BusinessService;
import rs.ac.uns.ftn.ktsnvt.service.DamageResponsibilityService;
import rs.ac.uns.ftn.ktsnvt.service.TenantService;

import static org.mockito.BDDMockito.given;
import static org.junit.Assert.*;
import static org.mockito.BDDMockito.willReturn;

/**
 * Created by Ivana Zeljkovic on 30/11/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DamageResponsibilityServiceUnitTest {

    @Autowired
    private DamageResponsibilityService damageResponsibilityService;

    @Autowired
    private TenantService tenantService;

    @Autowired
    private BusinessService businessService;

    @Autowired
    private BuildingService buildingService;

    @MockBean
    private DamageResponsibilityRepository damageResponsibilityRepository;

    @MockBean
    private TenantRepository tenantRepository;

    @MockBean
    private BusinessRepository businessRepository;

    @MockBean
    private BuildingRepository buildingRepository;


    @Before
    public void setUp() {
        // TENANTS
        Tenant responsible_tenant = new Tenant();
        responsible_tenant.setFirstName("Ivana");
        responsible_tenant.setLastName("Zeljkovic");
        responsible_tenant.setId(new Long(100));

        // INSTITUTION
        Business responsible_institution = new Business();
        responsible_institution.setName("First institution");
        responsible_institution.setIsCompany(false);
        responsible_institution.setId(new Long(101));

        //BUILDING, ADDRESS
        City city = new City("Novi Sad", 21000);
        Address address_2 = new Address("Bulevar oslobodjenja", 5, city);

        Building building = new Building(5, true);
        building.setAddress(address_2);
        building.setId(new Long(102));

        // DAMAGE TYPES
        DamageType damage_type_1 = new DamageType("Damage type 1");
        damage_type_1.setId(new Long(1));
        DamageType damage_type_2 = new DamageType("Damage type 2");
        damage_type_2.setId(new Long(2));

        // DAMAGE RESPONSIBILITY
        DamageResponsibility damage_responsibility_1 = new DamageResponsibility(
                responsible_tenant, null, damage_type_1, building);
        DamageResponsibility damage_responsibility_2 = new DamageResponsibility(
                null, responsible_institution, damage_type_2, building);

        given(
               this.damageResponsibilityRepository.findOneByDamageTypeAndTenantAndBuilding(
                       new Long(1), new Long(100), new Long(102))
        ).willReturn(
                damage_responsibility_1
        );

        given(
                this.damageResponsibilityRepository.findOneByDamageTypeAndInstitutionAndBuilding(
                        new Long(2), new Long(101), new Long(102))
        ).willReturn(
                damage_responsibility_2
        );

        given(
                this.tenantRepository.findOne(new Long(100))
        ).willReturn(
                responsible_tenant
        );

        given(
                this.businessRepository.findOne(new Long(101))
        ).willReturn(
                responsible_institution
        );

        given(
                this.buildingRepository.findOne(new Long(102))
        ).willReturn(
                building
        );
    }


    @Test
    public void testFindOneByDamageTypeAndTenantAndBuilding() {
        /*
         * Test proverava ispravnost metode findOneByDamageTypeAndTenantAndBuilding servisa DamageResponsibilityService
         * (dobavljanje odgovornosti za kvar na osnovu zadatog tipa kvara, stanara i zgrade)
         * u slucaju kada postoji odgovornost zadatog stanara za zadati tip kvara u zadatoj zgradi
         * */

        Tenant responsible_tenant = this.tenantService.findOne(new Long(100));
        Building building = this.buildingService.findOne(new Long(102));

        // stanar sa ID-em = 100 je odgovorna osoba za tip kvara sa ID-em = 1 u zgradi sa ID-em = 102
        DamageResponsibility damage_responsibility = this.damageResponsibilityService.findOneByDamageTypeAndTenantAndBuilding(
                new Long(1), new Long(100), new Long(102));

        assertNotNull(damage_responsibility);
        assertEquals(responsible_tenant.getId(), damage_responsibility.getResponsibleTenant().getId());
        assertEquals(responsible_tenant.getFirstName(), damage_responsibility.getResponsibleTenant().getFirstName());
        assertEquals(responsible_tenant.getLastName(), damage_responsibility.getResponsibleTenant().getLastName());
        assertEquals(building.getId(), damage_responsibility.getBuilding().getId());
        assertEquals(building.getAddress(), damage_responsibility.getBuilding().getAddress());
    }

    @Test(expected = ForbiddenException.class)
    public void testFindOneByDamageTypeAndTenantAndBuildingNonexistent() {
        /*
         * Test proverava ispravnost metode findOneByDamageTypeAndTenantAndBuilding servisa DamageResponsibilityService
         * (dobavljanje odgovornosti za kvar na osnovu zadatog tipa kvara, stanara i zgrade)
         * u slucaju kada ne postoji odgovornost zadatog stanara za zadati tip kvara u zadatoj zgradi - ocekivano ForbiddenException
         * */

        // za tip kvara sa ID-em = 2 u zgradi sa ID-em = 102 postoji odgovorna institucija, ali ne i odgovorno lice
        DamageResponsibility damage_responsibility = this.damageResponsibilityService.findOneByDamageTypeAndTenantAndBuilding(
                new Long(2), new Long(100), new Long(102));
    }

    @Test
    public void testFindOneByDamageTypeAndInstitutionAndBuilding() {
        /*
         * Test proverava ispravnost metode findOneByDamageTypeAndInstitutionAndBuilding servisa DamageResponsibilityService
         * (dobavljanje odgovornosti za kvar na osnovu zadatog tipa kvara, institucije i zgrade)
         * u slucaju kada postoji odgovornost zadate institucije za zadati tip kvara u zadatoj zgradi
         * */

        Business responsible_institution = this.businessService.findOne(new Long(101));
        Building building = this.buildingService.findOne(new Long(102));

        // institucija sa ID-em = 101 je odgovorna osoba za tip kvara sa ID-em = 2 u zgradi sa ID-em = 102
        DamageResponsibility damage_responsibility = this.damageResponsibilityService.findOneByDamageTypeAndInstitutionAndBuilding(
                new Long(2), new Long(101), new Long(102));

        assertNotNull(damage_responsibility);
        assertEquals(responsible_institution.getId(), damage_responsibility.getResponsibleInstitution().getId());
        assertFalse(damage_responsibility.getResponsibleInstitution().isIsCompany());
        assertEquals(responsible_institution.getName(), damage_responsibility.getResponsibleInstitution().getName());
        assertEquals(building.getId(), damage_responsibility.getBuilding().getId());
        assertEquals(building.getAddress().getStreet(), damage_responsibility.getBuilding().getAddress().getStreet());
        assertEquals(building.getAddress().getNumber(), damage_responsibility.getBuilding().getAddress().getNumber());
        assertEquals(building.getAddress().getCity().getName(), damage_responsibility.getBuilding().getAddress().getCity().getName());
    }

    @Test(expected = ForbiddenException.class)
    public void testFindOneByDamageTypeAndInstitutionAndBuildingNonexistent() {
        /* Test proverava ispravnost metode findOneByDamageTypeAndInstitutionAndBuilding servisa DamageResponsibilityService
         * (dobavljanje odgovornosti za kvar na osnovu zadatog tipa kvara, institucije i zgrade)
         * u slucaju kada ne postoji odgovornost zadate institucije za zadati tip kvara u zadatoj zgradi - ocekivano ForbiddenException
         * */

        // za tip kvara sa ID-em = 1 u zgradi sa ID-em = 102 postoji odgovorna osoba, ali ne i odgovorno institucija
        DamageResponsibility damage_responsibility = this.damageResponsibilityService.findOneByDamageTypeAndInstitutionAndBuilding(
                new Long(1), new Long(101), new Long(102));
    }

}
