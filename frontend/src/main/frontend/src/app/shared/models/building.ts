import { Tenant } from "./tenant";
import { Apartment } from "./apartment";
import { Business } from "./business";
import { Address } from "./address";

export interface Building {
  id: number;
  address: Address;
  apartments: Array<Apartment>;
  presidentOfCouncil?: Tenant;
  responsibleCompany?: Business;
  numberOfElements? : number;
  numberOfFloors: number;
  hasParking: boolean;
}
