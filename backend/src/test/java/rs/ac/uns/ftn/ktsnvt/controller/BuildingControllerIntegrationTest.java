package rs.ac.uns.ftn.ktsnvt.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;
import rs.ac.uns.ftn.ktsnvt.controller.util.TestUtil;
import rs.ac.uns.ftn.ktsnvt.web.dto.*;

import javax.annotation.PostConstruct;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Created by Katarina Cukurov on 06/12/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class BuildingControllerIntegrationTest {

    private static final String URL_PREFIX = "/api/buildings";

    private MediaType contentType = new MediaType(
        MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    private MockMvc mockMvc;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja administratora
    private String accessTokenAdmin;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja stanara
    private String accessTokenTenant;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja predsednika skupstine
    private String accessTokenCouncilPresident;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja firme
    private String accessTokenFirm;
    private MultiValueMap<String, String> params;


    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private TestRestTemplate testRestTemplate;


    @PostConstruct
    public void setup() {
         this.mockMvc = MockMvcBuilders.
                                webAppContextSetup(webApplicationContext).apply(springSecurity()).build();
    }

    // pre izvrsavanja testa, neophodno je izvrsiti logovanje u aplikaciju da bismo dobili token
    @Before
    public void login() throws Exception {
        // logovanje administratora
        LoginDTO loginDTO = new LoginDTO("ivana", "ivana");

        ResponseEntity<TokenDTO> responseEntity =
            testRestTemplate.postForEntity("/api/login",
                loginDTO,
                TokenDTO.class);
        // preuzmemo token za logovanog administratora iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenAdmin = responseEntity.getBody().getValue();

        // logovanje stanara
        LoginDTO loginDTOTenant = new LoginDTO("jasmina", "jasmina");

        ResponseEntity<TokenDTO> responseEntityTenant =
            testRestTemplate.postForEntity("/api/login",
                loginDTOTenant,
                TokenDTO.class);
        // preuzmemo token za logovanog stanara iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenTenant = responseEntityTenant.getBody().getValue();

        // logovanje predsednika skupstine stanara
        LoginDTO loginDTOPresident = new LoginDTO("kaca", "kaca");

        ResponseEntity<TokenDTO> responseEntityPresident =
            testRestTemplate.postForEntity("/api/login",
                loginDTOPresident,
                TokenDTO.class);
        // preuzmemo token za logovanog predsednika iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenCouncilPresident = responseEntityPresident.getBody().getValue();

        // logovanje predsednika firme
        LoginDTO loginDTOFirm = new LoginDTO("firm1", "firm1");

        ResponseEntity<TokenDTO> responseEntityFirm =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOFirm,
                        TokenDTO.class);
        // preuzmemo token za logovanu firmu iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenFirm = responseEntityFirm.getBody().getValue();

        // za pageable atribut
        params = TestUtil.getParams(
                "0", "10", "ASC", "id", "");
    }

    // TESTOVI ZA getAllBuildings
    @Test
    public void testGetAllBuildings() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllBuildings kontrolera BuildingController
        * (dobavljanje liste svih zgrada koje su registrovane u aplikaciji)
        * */

        this.mockMvc.perform(get(URL_PREFIX)
                .header("Authentication-Token", accessTokenAdmin)
                .params(params))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(5)));
    }

    @Test
    public void testGetAllBuildingsByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode getAllBuildings kontrolera BuildingController
        * (dobavljanje liste svih zgrada koje su registrovane u aplikaciji)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        this.mockMvc.perform(get(URL_PREFIX)
                .contentType(contentType)
                .params(params))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetAllBuildingsByNotAuthenticatedAdministrator() throws Exception {
       /*
       * Test proverava ispravnost rada metode getAllBuildings kontrolera BuildingController
       * (dobavljanje liste svih zgrada koje su registrovane u aplikaciji)
       * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu administratora
       * */

        this.mockMvc.perform(get(URL_PREFIX)
                .header("Authentication-Token", accessTokenTenant)
                .contentType(contentType)
                .params(params))
                .andExpect(status().isForbidden());
    }

    // TESTOVI za getAllBuildingsByFirm
    @Test
    public void testGetAllBuildingsByFirm() throws Exception {
        /*
        * Test proverava ispravnost metode getAllBuildingsByFirm kontrolera BuildingController
        * u slucaju kada su prosledjeni podaci ispravni i kada je ulogovana kompanija
        * */
        this.mockMvc.perform(get(URL_PREFIX + "/firms/100/buildings")
                .params(params)
                .header("Authentication-Token", accessTokenFirm))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetAllBuildingsByUnauthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost metode getAllBuildingsByFirm kontrolera BuildingController
        * u slucaju kada su prosledjeni podaci ispravni i kada niko nije ulogovan
        * */
        this.mockMvc.perform(get(URL_PREFIX + "/firms/100/buildings")
                .params(params))
                .andExpect(status().isForbidden());
    }

    // TESTOVI za getBuildingFromAddress
    @Test
    public void testGetBuildingFromAddress() throws Exception{
        /*
        * Test proverava ispravnost metode getBuildingFromAddress kontrolera BuildingController
        * u slucaju kada su podaci ispravni i kada je ulogovan administrator
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/building_address")
                .param("city_name", "Novi Sad")
                .header("Authentication-Token", accessTokenAdmin))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetBuildingFromAddressByUnauthenticatedUser() throws Exception{
        /*
        * Test proverava ispravnost metode getBuildingFromAddress kontrolera BuildingController
        * u slucaju kada su podaci ispravni i kada niko nije ulogovan
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/building_address")
                .param("city_name", "Novi Sad"))
                .andExpect(status().isForbidden());
    }

    // TESTOVI ZA getBuilding
    @Test
    public void testGetBuildingByAdministrator() throws Exception {
        /*
        * Test proverava ispravnost rada metode getBuilding kontrolera BuildingController
        * (dobavljanje jedne zgrade na osnovu zadatog ID-a)
        * u slucaju kada akciju izvrsava trenutno logovani administrator
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/100")
                .header("Authentication-Token", accessTokenAdmin))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType));
    }

    @Test
    public void testGetBuildingByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode getBuilding kontrolera BuildingController
        * (dobavljanje jedne zgrade na osnovu zadatog ID-a)
        * u slucaju kada akciju izvrsava trenutno nelogovani korisnik
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/100")
                .contentType(contentType))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetBuildingByNotAuthenticatedPresidentOrAdministrator() throws Exception {
        /*
        * Test proverava ispravnost rada metode getBuilding kontrolera BuildingController
        * (dobavljanje jedne zgrade na osnovu zadatog ID-a)
        * u slucaju kada akciju izvrsava trenutno logovani korisnik koji nema ulogu predsednika skupstine stanara niti administratora
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/100")
                .header("Authentication-Token", accessTokenTenant)
                .contentType(contentType))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetBuildingNonexistentBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode getBuilding kontrolera BuildingController
        * (dobavljanje jedne zgrade na osnovu zadatog ID-a)
        * u slucaju kada ne postoji zgrada sa zadatim ID-em
        * */

        // ne postoji zgrada sa ID-em = 200
        this.mockMvc.perform(get(URL_PREFIX + "/200")
                .header("Authentication-Token", accessTokenAdmin))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(contentType));
    }

    // TESTOVI za registerBuilding
    @Test
    @Transactional
    @Rollback(true)
    public void testRegisterBuilding() throws Exception {
       /*
        * Test proverava ispravnost rada metode registerBuilding kontrolera BuildingController
        * (registracija nove zgrade)
        * */

        CityDTO cityDTO = new CityDTO("Novi Knezevac", 23330);
        AddressDTO addressDTO = new AddressDTO("Branka Radicevica", 75, cityDTO);
        ApartmentCreateDTO apartmentCreateDTO = new ApartmentCreateDTO(1, 1, 60);
        List<ApartmentCreateDTO> apartmentCreateDTOList = new ArrayList<>();
        apartmentCreateDTOList.add(apartmentCreateDTO);
        BuildingCreateDTO buildingCreateDTO = new BuildingCreateDTO(4, true, addressDTO,
                apartmentCreateDTOList, new Long(101));


        String buildingDTOJson = TestUtil.json(buildingCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX)
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(buildingDTOJson))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").exists());
    }

    @Test
    public void testRegisterBuildingByNotAuthenticatedUser() throws Exception {
       /*
        * Test proverava ispravnost rada metode registerBuilding kontrolera BuildingController
        * (registracija nove zgrade)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        CityDTO cityDTO = new CityDTO("Novi Knezevac", 23330);
        AddressDTO addressDTO = new AddressDTO("Branka Radicevica", 74, cityDTO);
        ApartmentCreateDTO apartmentCreateDTO = new ApartmentCreateDTO(1, 1, 60);
        List<ApartmentCreateDTO> apartmentCreateDTOList = new ArrayList<>();
        apartmentCreateDTOList.add(apartmentCreateDTO);
        BuildingCreateDTO buildingCreateDTO = new BuildingCreateDTO(4, true, addressDTO,
                apartmentCreateDTOList, new Long(101));


        String buildingDTOJson = TestUtil.json(buildingCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX)
                .contentType(contentType)
                .content(buildingDTOJson))
                .andExpect(status().isForbidden());
    }


    @Test
    public void testRegisterBuildingByNotAuthenticatedAdmin() throws Exception {
        /*
        * Test proverava ispravnost rada metode registerBuilding kontrolera BuildingController
        * (registracija nove zgrade)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu administratora
        * */

        CityDTO cityDTO = new CityDTO("Novi Knezevac", 23330);
        AddressDTO addressDTO = new AddressDTO("Branka Radicevica", 74, cityDTO);
        ApartmentCreateDTO apartmentCreateDTO = new ApartmentCreateDTO(1, 1, 60);
        List<ApartmentCreateDTO> apartmentCreateDTOList = new ArrayList<>();
        apartmentCreateDTOList.add(apartmentCreateDTO);
        BuildingCreateDTO buildingCreateDTO = new BuildingCreateDTO(4, true, addressDTO,
                apartmentCreateDTOList, new Long(101));


        String buildingDTOJson = TestUtil.json(buildingCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX)
                .header("Authentication-Token", accessTokenTenant)
                .contentType(contentType)
                .content(buildingDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testRegisterBuildingMissingNumberOfFloors() throws Exception {
        /*
        * Test proverava ispravnost rada metode registerBuilding kontrolera BuildingController
        * (registracija nove zgrade)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne poseduje sva neophodna polja - nedostaje polje numberOfFloors
        * */

        CityDTO cityDTO = new CityDTO("Novi Knezevac", 23330);
        AddressDTO addressDTO = new AddressDTO("Branka Radicevica", 74, cityDTO);
        ApartmentCreateDTO apartmentCreateDTO = new ApartmentCreateDTO(1, 1, 60);
        List<ApartmentCreateDTO> apartmentCreateDTOList = new ArrayList<>();
        apartmentCreateDTOList.add(apartmentCreateDTO);
        BuildingCreateDTO buildingCreateDTO = new BuildingCreateDTO(0, true, addressDTO,
                apartmentCreateDTOList, new Long(101));

        String buildingDTOJson = TestUtil.json(buildingCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX)
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(buildingDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testRegisterBuildingMissingAddress() throws Exception {
        /*
        * Test proverava ispravnost rada metode registerBuilding kontrolera BuildingController
        * (registracija nove zgrade)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne poseduje sva neophodna polja - nedostaje polje AddressDTO
        * */
        ApartmentCreateDTO apartmentCreateDTO = new ApartmentCreateDTO(1, 1, 60);
        List<ApartmentCreateDTO> apartmentCreateDTOList = new ArrayList<>();
        apartmentCreateDTOList.add(apartmentCreateDTO);
        BuildingCreateDTO buildingCreateDTO = new BuildingCreateDTO(4, true, null,
                apartmentCreateDTOList, new Long(101));

        String buildingDTOJson = TestUtil.json(buildingCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX)
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(buildingDTOJson))
                .andExpect(status().isBadRequest());
    }


    @Test
    public void testRegisterBuildingMissingCityDTO() throws Exception {
        /*
        * Test proverava ispravnost rada metode registerBuilding kontrolera BuildingController
        * (registracija nove zgrade)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne poseduje sva neophodna polja - nedostaje polje cityDTO
        * */

        AddressDTO addressDTO = new AddressDTO("Branka Radicevica", 74, null);
        ApartmentCreateDTO apartmentCreateDTO = new ApartmentCreateDTO(1, 1, 60);
        List<ApartmentCreateDTO> apartmentCreateDTOList = new ArrayList<>();
        apartmentCreateDTOList.add(apartmentCreateDTO);
        BuildingCreateDTO buildingCreateDTO = new BuildingCreateDTO(4, true, addressDTO,
                apartmentCreateDTOList, new Long(101));

        String buildingDTOJson = TestUtil.json(buildingCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX)
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(buildingDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testRegisterBuildingInappropriateTypeOfNumberOfFloors() throws Exception {
        /*
        * Test proverava ispravnost rada metode registerBuilding kontrolera BuildingController
        * (registracija nove zgrade)
        * u slucaju kada je dolazni DTO objekat nevalidan: sadrzi sva neophodna polja, ali je polje numberOfFloors je string
        * */

        String buildingDTOJson = "{\"numberOfFloors\":\"4.3\",\"hasParking\":true,"
                + "\"address\":{\"street\":\"Branka Radicevica\",\"number\":74,"
                + "\"city\":{\"name\":\"Novi Knezevac\",\"postalNumber\":23330}},"
                + "\"apartments\":[{\"number\":1,\"floor\":1,\"surface\":60}],"
                + "\"firmManagerId\":101}";

        this.mockMvc.perform(post(URL_PREFIX)
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(buildingDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testRegisterBuildingEmptyApartmentList() throws Exception {
        /*
        * Test proverava ispravnost rada metode registerBuilding kontrolera BuildingController
        * (registracija nove zgrade)
        * u slucaju kada je dolazni DTO objekat nevalidan: sadrzi sva neophodna polja, ali je polje apartmentDTOS je prazna lista
        * */

        String buildingDTOJson = "{\"numberOfFloors\":4,\"hasParking\":true,"
                + "\"address\":{\"street\":\"Branka Radicevica\",\"number\":74,"
                + "\"city\":{\"name\":\"Novi Knezevac\",\"postalNumber\":23330}},"
                + "\"apartments\":[],"
                + "\"firmManagerId\":101}";

        this.mockMvc.perform(post(URL_PREFIX)
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(buildingDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testRegisterBuildingApartmentFloorBiggerThanNumberOfFloors() throws Exception {
        /*
        * Test proverava ispravnost rada metode registerBuilding kontrolera BuildingController
        * (registracija nove zgrade)
        * u slucaju kada je dolazni DTO objekat nevalidan: sadrzi sva neophodna polja, ali polje floor u apartment je vece od numberOfFloors
        * */

        String buildingDTOJson = "{\"numberOfFloors\":3,\"hasParking\":true,"
                + "\"address\":{\"street\":\"Branka Radicevica\",\"number\":74,"
                + "\"city\":{\"name\":\"Novi Knezevac\",\"postalNumber\":23330}},"
                + "\"apartments\":[{\"number\":1,\"floor\":5,\"surface\":60}],"
                + "\"firmManagerId\":101}";

        this.mockMvc.perform(post(URL_PREFIX)
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(buildingDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testRegisterBuildingApartmentsHaveSameNumber() throws Exception {
        /*
        * Test proverava ispravnost rada metode registerBuilding kontrolera BuildingController
        * (registracija nove zgrade)
        * u slucaju kada je dolazni DTO objekat nevalidan: sadrzi sva neophodna polja, ali dva stana imaju isti broj
        * */

        String buildingDTOJson = "{\"numberOfFloors\":4,\"hasParking\":true,"
                + "\"address\":{\"street\":\"Branka Radicevica\",\"number\":74,"
                + "\"city\":{\"name\":\"Novi Knezevac\",\"postalNumber\":23330}},"
                + "\"apartments\":[{\"number\":1,\"floor\":1,\"surface\":60}, {\"number\":1,\"floor\":1,\"surface\":60}],"
                + "\"firmManagerId\":101}";

        this.mockMvc.perform(post(URL_PREFIX)
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(buildingDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testRegisterBuildingHasSameAddressAsAnotherBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode registerBuilding kontrolera BuildingController
        * (registracija nove zgrade)
        * u slucaju kada je dolazni DTO objekat nevalidan: sadrzi sva neophodna polja, ali ima istu adresu kao neka druga zgrada
        * */

        String buildingDTOJson = "{\"numberOfFloors\":3,\"hasParking\":true,"
                + "\"address\":{\"street\":\"Janka Veselinovica\",\"number\":8,"
                + "\"city\":{\"name\":\"Novi Sad\",\"postalNumber\":21000}},"
                + "\"apartments\":[{\"number\":1,\"floor\":1,\"surface\":60}],"
                + "\"firmManagerId\":101}";

        this.mockMvc.perform(post(URL_PREFIX)
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(buildingDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testRegisterBuildingFirmManagerIsInstitution() throws Exception {
        /*
        * Test proverava ispravnost rada metode registerBuilding kontrolera BuildingController
        * (registracija nove zgrade)
        * u slucaju kada je dolazni DTO objekat nevalidan: sadrzi sva neophodna polja, ali polje firmManagerId je id institucije
        * a ne id postojece firme
        * */

        String buildingDTOJson = "{\"numberOfFloors\":4,\"hasParking\":true,"
                + "\"address\":{\"street\":\"Branka Radicevica\",\"number\":74,"
                + "\"city\":{\"name\":\"Novi Knezevac\",\"postalNumber\":23330}},"
                + "\"apartments\":[{\"number\":1,\"floor\":1,\"surface\":60}],"
                + "\"firmManagerId\":102}";

        this.mockMvc.perform(post(URL_PREFIX)
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(buildingDTOJson))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za updateBuilding
    @Test
    @Transactional
    @Rollback(true)
    public void testUpdateBuilding() throws Exception{
        /*
        * Test proverava ispravnost rada metode updateBuilding kontrolera BuildingController
        * (azuriranje podatka zgrade sa zadatim ID-em)
        * */

        CityDTO cityDTO = new CityDTO("Novi Sad", 21000);
        AddressDTO addressDTO = new AddressDTO("Janka Veselinovica", 8, cityDTO);
        List<ApartmentUpdateDTO> list = new ArrayList<>();
        BuildingUpdateDTO buildingUpdateDTO = new BuildingUpdateDTO(true, addressDTO,
            list, 4);

        String token = TestUtil.json(buildingUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/100")
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType));
    }

    @Test
    public void testUpdateBuildingByNotAuthenticatedUser() throws Exception{
        /*
        * Test proverava ispravnost rada metode updateBuilding kontrolera BuildingController
        * (azuriranje podatka zgrade sa zadatim ID-em)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        CityDTO cityDTO = new CityDTO("Novi Sad", 21000);
        AddressDTO addressDTO = new AddressDTO("Janka Veselinovica", 8, cityDTO);
        ApartmentUpdateDTO apartmentUpdateDTO = new ApartmentUpdateDTO(new Long(100), 4, 1, 60);
        ApartmentUpdateDTO apartmentUpdateDTO1 = new ApartmentUpdateDTO(new Long(101), 3, 1, 60);
        ApartmentUpdateDTO apartmentUpdateDTO2 = new ApartmentUpdateDTO(new Long(102), 2, 1, 60);
        List<ApartmentUpdateDTO> list = new ArrayList<>();
        list.add(apartmentUpdateDTO);
        list.add(apartmentUpdateDTO1);
        list.add(apartmentUpdateDTO2);
        BuildingUpdateDTO buildingUpdateDTO = new BuildingUpdateDTO(true, addressDTO,
            list, 4);

        String token = TestUtil.json(buildingUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/100")
                .contentType(contentType)
                .content(token))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testUpdateBuildingByNotAuthenticatedAdministrator() throws Exception{
        /*
        * Test proverava ispravnost rada metode updateBuilding kontrolera BuildingController
        * (azuriranje podatka zgrade sa zadatim ID-em)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu administratora
        * */

        CityDTO cityDTO = new CityDTO("Novi Sad", 21000);
        AddressDTO addressDTO = new AddressDTO("Janka Veselinovica", 8, cityDTO);
        ApartmentUpdateDTO apartmentUpdateDTO = new ApartmentUpdateDTO(new Long(100), 4, 1, 60);
        ApartmentUpdateDTO apartmentUpdateDTO1 = new ApartmentUpdateDTO(new Long(101), 3, 1, 60);
        ApartmentUpdateDTO apartmentUpdateDTO2 = new ApartmentUpdateDTO(new Long(102), 2, 1, 60);
        List<ApartmentUpdateDTO> list = new ArrayList<>();
        list.add(apartmentUpdateDTO);
        list.add(apartmentUpdateDTO1);
        list.add(apartmentUpdateDTO2);
        BuildingUpdateDTO buildingUpdateDTO = new BuildingUpdateDTO(true, addressDTO,
            list, 4);

        String token = TestUtil.json(buildingUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/100")
                .header("Authentication-Token", accessTokenTenant)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testUpdateBuildingNonexistentBuilding() throws Exception{
        /*
        * Test proverava ispravnost rada metode updateBuilding kontrolera BuildingController
        * (azuriranje podatka zgrade sa zadatim ID-em)
        * u slucaju kada ne postoji zgrada sa zadatim ID-em
        * */

        String token = "{\"hasParking\":true,"
            + "\"address\":{\"street\":\"Janka Veselinovica\",\"number\":8,"
            + "\"city\":{\"name\":\"Novi Sad\",\"postalNumber\":21000}},"
            + "\"apartments\":[{\"id\":100,\"number\":4,\"floor\":1,\"surface\":60},"
            + "{\"id\":101,\"number\":3,\"floor\":1,\"surface\":60},"
            + "{\"id\":102,\"number\":2,\"floor\":1,\"surface\":60}],"
            + "\"numberOfFloors\":4}";

        // ne postoji zgrada sa ID-em = 200
        this.mockMvc.perform(put(URL_PREFIX + "/200")
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testUpdateBuildingMissingNumberOfFloors() throws Exception{
        /*
        * Test proverava ispravnost rada metode updateBuilding kontrolera BuildingController
        * (azuriranje podatka zgrade sa zadatim ID-em)
        * u slucaju kada je prosledjeni DTO objekat nevalidan: ne sadrzi sva neophodna polja - nedostaje polje numberOfFloors
        * */

        String token = "{\"hasParking\":true,"
            + "\"address\":{\"street\":\"Janka Veselinovica\",\"number\":8,"
            + "\"city\":{\"name\":\"Novi Sad\",\"postalNumber\":21000}},"
            + "\"apartments\":[{\"id\":100,\"number\":4,\"floor\":1,\"surface\":60},"
            + "{\"id\":101,\"number\":3,\"floor\":1,\"surface\":60},"
            + "{\"id\":102,\"number\":2,\"floor\":1,\"surface\":60}}";

        this.mockMvc.perform(put(URL_PREFIX + "/100")
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateBuildingMissingAddressDTO() throws Exception{
        /*
        * Test proverava ispravnost rada metode updateBuilding kontrolera BuildingController
        * (azuriranje podatka zgrade sa zadatim ID-em)
        * u slucaju kada je prosledjeni DTO objekat nevalidan: ne sadrzi sva neophodna polja - nedostaje polje addressDTO
        * */

        String token = "{\"hasParking\":true,"
            + "\"apartments\":[{\"id\":100,\"number\":4,\"floor\":1,\"surface\":60},"
            + "{\"id\":101,\"number\":3,\"floor\":1,\"surface\":60},"
            + "{\"id\":102,\"number\":2,\"floor\":1,\"surface\":60}],"
            + "\"numberOfFloors\":4}";

        this.mockMvc.perform(put(URL_PREFIX + "/100")
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateBuildingMissingCityDTO() throws Exception{
        /*
        * Test proverava ispravnost rada metode updateBuilding kontrolera BuildingController
        * (azuriranje podatka zgrade sa zadatim ID-em)
        * u slucaju kada je prosledjeni DTO objekat nevalidan: ne sadrzi sva neophodna polja - nedostaje polje cityDTO
        * */

        String token = "{\"hasParking\":true,"
            + "\"address\":{\"street\":\"Janka Veselinovica\",\"number\":8},"
            + "\"apartments\":[{\"id\":100,\"number\":4,\"floor\":1,\"surface\":60},"
            + "{\"id\":101,\"number\":3,\"floor\":1,\"surface\":60},"
            + "{\"id\":102,\"number\":2,\"floor\":1,\"surface\":60}],"
            + "\"numberOfFloors\":4}";

        this.mockMvc.perform(put(URL_PREFIX + "/100")
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateBuildingSameAddress() throws Exception{
        /*
        * Test proverava ispravnost rada metode updateBuilding kontrolera BuildingController
        * (azuriranje podatka zgrade sa zadatim ID-em)
        * u slucaju kada je prosledjeni DTO objekat nevalidan: sadrzi sva neophodna polja, ali je zadata zadresa vec zauzeta
        * nekim drugim objektom
        * */

        String token = "{\"hasParking\":true,"
            + "\"address\":{\"street\":\"Janka Veselinovica\",\"number\":10,"
            + "\"city\":{\"name\":\"Novi Sad\",\"postalNumber\":21000}},"
            + "\"apartments\":[{\"id\":100,\"number\":4,\"floor\":1,\"surface\":60},"
            + "{\"id\":101,\"number\":3,\"floor\":1,\"surface\":60},"
            + "{\"id\":102,\"number\":2,\"floor\":1,\"surface\":60}],"
            + "\"numberOfFloors\":4}";

        this.mockMvc.perform(put(URL_PREFIX + "/100")
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateBuildingDeletingApartmentWithTenants() throws Exception{
        /*
        * Test proverava ispravnost rada metode updateBuilding kontrolera BuildingController
        * (azuriranje podatka zgrade sa zadatim ID-em)
        * u slucaju kada je prosledjeni DTO objekat nevalidan: sadrzi sva neophodna polja, ali se pokusava obrisati stan u kom
        * postoje stanari
        * */

        String token = "{\"hasParking\":true,"
            + "\"address\":{\"street\":\"Janka Veselinovica\",\"number\":10,"
            + "\"city\":{\"name\":\"Novi Sad\",\"postalNumber\":21000}},"
            + "\"apartments\":[{\"id\":100,\"number\":4,\"floor\":1,\"surface\":60},"
            + "{\"id\":101,\"number\":3,\"floor\":1,\"surface\":60}],"
            + "\"numberOfFloors\":4}";

        this.mockMvc.perform(put(URL_PREFIX + "/100")
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(token))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za deleteBuidling
    @Test
    @Transactional
    @Rollback(true)
    public void testDeleteBuilding() throws Exception{
        /*
        * Test proverava ispravnost rada metode deleteBuilding kontrolera BuildingController
        * (brisanje zgrade sa zadatim ID-em)
        * */

        this.mockMvc.perform(delete(URL_PREFIX + "/104")
                .header("Authentication-Token", accessTokenAdmin))
                .andExpect(status().isOk());
        // nakon brisanja, ne postoji trazeni resurs u bazi
        this.mockMvc.perform(get(URL_PREFIX + "/104")
                .header("Authentication-Token", accessTokenAdmin))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeleteBuildingByNotAuthenticatedUser() throws Exception{
        /*
        * Test proverava ispravnost rada metode deleteBuilding kontrolera BuildingController
        * (brisanje zgrade sa zadatim ID-em)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        this.mockMvc.perform(delete(URL_PREFIX + "/102"))
                    .andExpect(status().isForbidden());
    }

    @Test
    public void testDeleteBuildingByNotAuthenticatedAdministrator() throws Exception{
        /*
        * Test proverava ispravnost rada metode deleteBuilding kontrolera BuildingController
        * (brisanje zgrade sa zadatim ID-em)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu administratora
        * */

        this.mockMvc.perform(delete(URL_PREFIX + "/102")
                .header("Authentication-Token", accessTokenTenant))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDeleteBuildingNonexistentBuilding() throws Exception{
        /*
        * Test proverava ispravnost rada metode deleteBuilding kontrolera BuildingController
        * (brisanje zgrade sa zadatim ID-em)
        * u slucaju kada ne postoji zgrada sa zadatim ID-em
        * */

        // ne postoji zgrada sa ID-em = 200
        this.mockMvc.perform(delete(URL_PREFIX + "/200")
                .header("Authentication-Token", accessTokenTenant))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDeleteBuildingHasTenants() throws Exception{
        /*
        * Test proverava ispravnost rada metode deleteBuilding kontrolera BuildingController
        * (brisanje zgrade sa zadatim ID-em)
        * u slucaju kada u zgradi sa zadatim ID-em postoji bar jedan stan koji ima stanare
        * */

        // u zgradi sa ID-em zive stanari
        this.mockMvc.perform(delete(URL_PREFIX + "/100")
                .header("Authentication-Token", accessTokenTenant))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDeleteBuildingHasAparmtnentWithOwner() throws Exception{
        /*
        * Test proverava ispravnost rada metode deleteBuilding kontrolera BuildingController
        * (brisanje zgrade sa zadatim ID-em)
        * u slucaju kada u zgradi sa ID-em postoji bar jedan stan koji ima vlasnika
        * */

        // u zgradi sa ID-em postoje stanovi koji imaju vlasnika
        this.mockMvc.perform(delete(URL_PREFIX + "/100")
                .header("Authentication-Token", accessTokenTenant))
                .andExpect(status().isForbidden());
    }
}
