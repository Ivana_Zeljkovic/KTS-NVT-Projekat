export enum BusinessType{
  POLICE,
  FIRE_SERVICE,
  HOSPITAL,
  COMMUNAL_POLICE,
  FIRM_FOR_PLUMBING,
  FIRM_FOR_ELECTRICAL_INSTALLATIONS,
  FIRM_FOR_EXTERIOR,
  FIRM_FOR_GAS_INSTALLATIONS,
  DERATISATION_DESINSECTION_FIRM,
  HYGIENE_MAINTENANCE_FIRM,
  OTHER
}
