package rs.ac.uns.ftn.ktsnvt.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import rs.ac.uns.ftn.ktsnvt.model.entity.MeetingItem;

import java.util.Date;
import java.util.List;

/**
 * Created by Ivana Zeljkovic on 13/11/2017.
 */
public interface MeetingItemRepository extends JpaRepository<MeetingItem, Long> {

    @Query("SELECT m FROM MeetingItem m WHERE m.billboard.id = :billboardId AND m.meeting.id is NULL")
    List<MeetingItem> findAllOnBillboard(@Param("billboardId") Long billboardId);

    @Query(value = "SELECT m FROM MeetingItem m WHERE m.billboard.id = :id AND m.meeting.id is NULL")
    Page<MeetingItem> findAllOnBillboard(@Param("id") Long billboardId, Pageable pageable);

    @Query(value = "SELECT m FROM MeetingItem m WHERE m.billboard.id = :id " +
            "AND m.meeting.id is NULL AND m.date BETWEEN :fromDate AND :toDate")
    Page<MeetingItem> findAllFromDateToDate(@Param("id") Long billboardId, @Param("fromDate") Date fromDate,
                                            @Param("toDate") Date toDate, Pageable pageable);

    @Query(value = "SELECT m FROM MeetingItem m WHERE m.meeting.id = :meetingId")
    Page<MeetingItem> findAllForMeeting(@Param("meetingId") Long meetingId, Pageable pageable);

    @Query(value = "SELECT m FROM MeetingItem m WHERE m.meeting.id = :meetingId AND m.meeting.record IS NOT NULL")
    Page<MeetingItem> findAllForRecord(@Param("meetingId") Long meetingId, Pageable pageable);

    @Query(value = "SELECT m FROM MeetingItem m WHERE m.billboard.id IN " +
            "(SELECT a.building.billboard.id FROM Apartment a WHERE a.owner.id = :tenantId) AND " +
            "m.questionnaire.dateExpire > :currentDate AND " +
            "((SELECT COUNT(v) FROM Vote v WHERE v.questionnaire.id = m.questionnaire.id AND v.tenant.id = :tenantId) " +
            "< (SELECT COUNT(a) FROM Apartment a WHERE a.building.billboard.id = m.billboard.id AND a.owner.id = :tenantId))")
    Page<MeetingItem> findAllWithActiveQuestionnairesForTenant(@Param("tenantId") Long tenantId,
                                                               @Param("currentDate") Date currentDate, Pageable pageable);

    @Query(value = "SELECT m FROM MeetingItem m WHERE m.questionnaire.dateExpire <= :currentDate AND m.billboard.id = " +
            "(SELECT b.billboard.id FROM Building b WHERE b.id = :buildingId)")
    Page<MeetingItem> findAllFinishedQuestionnairesInBuilding(@Param("buildingId") Long buildingId,
                                                              @Param("currentDate") Date currentDate, Pageable pageable);

    @Query("SELECT m FROM MeetingItem m WHERE m.id = :id AND m.billboard.id = :billboardId AND m.meeting.id is NULL")
    MeetingItem findOneByIdAndBillboard(@Param("id") Long id, @Param("billboardId") Long billboardId);

    @Query("SELECT m FROM MeetingItem m WHERE m.damage.id = :damageId")
    List<MeetingItem> findAllByDamageId(@Param("damageId") Long damageId);
}
