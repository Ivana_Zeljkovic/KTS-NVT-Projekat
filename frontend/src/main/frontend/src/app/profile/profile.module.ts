import { NgModule } from '@angular/core';
// module
import { SharedModule } from "../shared/shared.module";
import { ProfileRouterModule } from "./profile-router.module";
// component
import { ProfileComponent } from './profile/profile.component';
import { TenantProfileComponent } from "./tenant-profile/tenant-profile.component";
import { BusinessProfileComponent } from './business-profile/business-profile.component';
import { AdminProfileComponent } from './admin-profile/admin-profile.component';

@NgModule({
  imports: [
    SharedModule,
    ProfileRouterModule
  ],
  declarations: [
    ProfileComponent,
    TenantProfileComponent,
    BusinessProfileComponent,
    AdminProfileComponent
  ]
})
export class ProfileModule { }
