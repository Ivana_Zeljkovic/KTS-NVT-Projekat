

import {by, element} from "protractor";

export class FixDamageModalPage{

  getComponent(){
    return element(by.tagName('app-fix-damage-list-modal'));
  }

  getButton(){
    return element(by.xpath("//tr[1]/td[last()]"))
  }

}
