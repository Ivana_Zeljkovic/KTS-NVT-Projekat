package rs.ac.uns.ftn.ktsnvt.service.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.DamageType;
import rs.ac.uns.ftn.ktsnvt.service.DamageTypeService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Nikola Garabandic on 01/12/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class DamageTypeIntegrationService {

    @Autowired
    private DamageTypeService damageTypeService;


    @Test
    public void testFindOne(){
        //damage type sa id-em 1: ID=  1, name = PLUMBING_DAMAGE
        DamageType damageType = damageTypeService.findOne(1L);

        assertNotNull(damageType);
        assertEquals("PLUMBING_DAMAGE", damageType.getName());
    }

    @Test(expected = NotFoundException.class)
    public void testFindNone(){
        //damage_type sa id-em 100 ne postoji
        DamageType damageType = damageTypeService.findOne(100L);
    }

    @Test
    public void testFindByName(){
        //DamagetType sa nazivom ELECTRICAL_DAMAGE ima ID 2
        DamageType damageType = damageTypeService.findByName("ELECTRICAL_DAMAGE");

        assertNotNull(damageType);
        assertEquals(new Long(2), damageType.getId());
        assertEquals("ELECTRICAL_DAMAGE", damageType.getName());
    }

    @Test(expected = NotFoundException.class)
    public void testFindNOneByName() {
        //DamageType sa nazivom ELECTRICAL_DAMAG ne posotji u bazi
        DamageType damageType = damageTypeService.findByName("ELECTRICAL_DAMAG");
    }
}
