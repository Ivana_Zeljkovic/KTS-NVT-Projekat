import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";
// component
import { DamageRequestsComponent } from "./damage-requests/damage-requests.component";


const routes: Routes = [
  { path: '', component: DamageRequestsComponent },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class DamageRequestsRouterModule { }
