package rs.ac.uns.ftn.ktsnvt.service;

import org.springframework.data.domain.Page;
import rs.ac.uns.ftn.ktsnvt.model.entity.Account;
import rs.ac.uns.ftn.ktsnvt.model.entity.Building;
import rs.ac.uns.ftn.ktsnvt.model.entity.Business;

/**
 * Created by Ivana Zeljkovic on 05/11/2017.
 */
public interface BusinessService {

    Page<Business> findAll(Integer pageNumber, Integer pageSize, String sortDirection, String sortProperty);

    Page<Business> findAllExceptCurrent(Long currentId, Integer pageNumber, Integer pageSize,
                                        String sortDirection, String sortProperty);

    Page<Business> findAllInstitutionsOrFirms(boolean isFirm, Integer integer, Integer valueOf,
                                              String sortDirection, String sortProperty);

    Page<Business> findAllInstitutionsOrFirmsByAdmin(boolean isFirm, Integer integer, Integer valueOf,
                                              String sortDirection, String sortProperty);

    Page<Business> findAllInstitutionsOrFirmsExceptCurrent(boolean isFirm, Long currentId, Integer integer,
                                                           Integer valueOf, String sortDirection, String sortProperty);

    Business findOne(Long id);

    Business save(Business business);

    void remove(Long id);

    Business findByAddressId(Long id, boolean isCompany);

    void checkPermissionForCurrentBusiness(Long businessId);

    boolean checkPasswordUpdate(Account account, String oldPassword, String newPassword, String confirmPassword);

    void checkPermissionForCurrentBusinessInBuilding(Building building);

    Business findOneInactive(Long id);

    Page<Business> findAllWhoDidntGetOffer(Long damageId, Integer integer, Integer integer1, String sortDirection, String sortProperty);
}
