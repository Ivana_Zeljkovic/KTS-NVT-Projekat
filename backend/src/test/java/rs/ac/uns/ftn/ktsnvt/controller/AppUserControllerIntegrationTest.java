package rs.ac.uns.ftn.ktsnvt.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import rs.ac.uns.ftn.ktsnvt.web.dto.LoginDTO;
import rs.ac.uns.ftn.ktsnvt.web.dto.TokenDTO;

import javax.annotation.PostConstruct;

import java.nio.charset.Charset;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Created by Ivana Zeljkovic on 04/12/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class AppUserControllerIntegrationTest {

    private static final String URL_PREFIX = "/api";

    private MediaType contentType = new MediaType(
            MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    private MockMvc mockMvc;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja stanara
    private String accessTokenTenant;


    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private TestRestTemplate testRestTemplate;


    @PostConstruct
    public void setup() {
        this.mockMvc = MockMvcBuilders.
                webAppContextSetup(webApplicationContext).apply(springSecurity()).build();
    }

    // pre izvrsavanja testa, neophodno je izvrsiti logovanje u aplikaciju da bismo dobili token
    @Before
    public void login() throws Exception {
        // logovanje stanara
        LoginDTO loginDTOTenant = new LoginDTO("kaca", "kaca");

        ResponseEntity<TokenDTO> responseEntityTenant =
                testRestTemplate.postForEntity(URL_PREFIX + "/login",
                        loginDTOTenant,
                        TokenDTO.class);
        // preuzmemo token za logovanog stanara iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenTenant = responseEntityTenant.getBody().getValue();
    }

    // TESTOVI za checkUsername
    @Test
    public void testCheckUsername() throws Exception {
        /*
        * Test proverava ispravnost rada metode checkUsername kontrolera AppUserController
        * (provera da li je zadato korisnicko ime zauzeto)
        * */

        // korisnicko ime Sabina nije zauzeto
        this.mockMvc.perform(get(URL_PREFIX + "/check_username?username=Sabina"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$").value(false));
    }

    @Test
    public void testCheckUsernameAlreadyTaken() throws Exception {
        /*
        * Test proverava ispravnost rada metode checkUsername kontrolera AppUserController
        * (provera da li je zadato korisnicko ime zauzeto)
        * */

        // korisnicko ime Sabina nije zauzeto
        this.mockMvc.perform(get(URL_PREFIX + "/check_username?username=ivana"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$").value(true));
    }

    @Test
    public void testCheckUsernameMissingParameter() throws Exception {
        /*
        * Test proverava ispravnost rada metode checkUsername kontrolera AppUserController
        * (provera da li je zadato korisnicko ime zauzeto)
        * u slucaju kada nije zadato korisniko ime kao parametar
        * */

        // nije zadato korisnicko ime
        this.mockMvc.perform(get(URL_PREFIX + "/check_username?username="))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za getCurrentUser
    @Test
    public void testGetCurrentUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode getCurrentUser kontrolera AppUserController
        * (dobavljanje podataka o korisniku koji je trenutno ulogovan)
        * */

        /*
        ulogovan je stanar sa ID-em = 101 i sledecim podacima:
            firstName: Katarina,
            lastName: Cukurov,
            email: kaca@gmail.com,
            apartmentNumber: 101,
            address:
                street: Janka Veselinovica,
                number: 8,
                city:
                    name: Novi Sad,
                    postalNumber: 21000
        */
        this.mockMvc.perform(get(URL_PREFIX + "/current_user")
                .header("Authentication-Token", this.accessTokenTenant))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(101))
                .andExpect(jsonPath("$.firstName").value("Katarina"))
                .andExpect(jsonPath("$.lastName").value("Cukurov"))
                .andExpect(jsonPath("$.email").value("kaca@gmail.com"))
                .andExpect(jsonPath("$.apartmentNumber").value(2))
                .andExpect(jsonPath("$.address.street").value("Janka Veselinovica"))
                .andExpect(jsonPath("$.address.number").value(8))
                .andExpect(jsonPath("$.address.city.name").value("Novi Sad"))
                .andExpect(jsonPath("$.address.city.postalNumber").value(21000));
    }
}
