package rs.ac.uns.ftn.ktsnvt.web.dto;

import rs.ac.uns.ftn.ktsnvt.model.entity.Business;

import java.io.Serializable;

/**
 * Created by Nikola Garabandic on 06/12/2017.
 */
public class BusinessDTO implements Serializable {

    private Long id;
    private String name;
    private String description;
    private String phoneNumber;
    private String email;
    private String businessType;
    private AddressDTO address;
    private boolean isCompany;
    private boolean confirmed;
    private long numberOfElements;
    private OfferDTO offer;


    public BusinessDTO(){ }

    public BusinessDTO(Business b, long numberOfElements)
    {
        this.id = b.getId();
        this.name = b.getName();
        this.description = b.getDescription();
        this.phoneNumber = b.getPhoneNumber();
        this.email = b.getEmail();
        this.businessType = b.getBusinessType().toString();
        this.address = new AddressDTO(b.getAddress());
        this.isCompany = b.isIsCompany();
        this.confirmed = b.getConfirmed();
        this.numberOfElements = numberOfElements;
        this.offer = null;
    }

    public BusinessDTO(Business b)
    {
        this.id = b.getId();
        this.name = b.getName();
        this.description = b.getDescription();
        this.phoneNumber = b.getPhoneNumber();
        this.email = b.getEmail();
        this.businessType = b.getBusinessType().toString();
        this.address = new AddressDTO(b.getAddress());
        this.isCompany = b.isIsCompany();
        this.confirmed = b.getConfirmed();
        this.offer = null;
    }


    public BusinessDTO(Business b, long numberOfElements, OfferDTO offer)
    {
        this.id = b.getId();
        this.name = b.getName();
        this.description = b.getDescription();
        this.phoneNumber = b.getPhoneNumber();
        this.email = b.getEmail();
        this.businessType = b.getBusinessType().toString();
        this.address = new AddressDTO(b.getAddress());
        this.isCompany = b.isIsCompany();
        this.confirmed = b.getConfirmed();
        this.numberOfElements = numberOfElements;
        this.offer = offer;
    }

    public BusinessDTO(Business b, OfferDTO offer)
    {
        this.id = b.getId();
        this.name = b.getName();
        this.description = b.getDescription();
        this.phoneNumber = b.getPhoneNumber();
        this.email = b.getEmail();
        this.businessType = b.getBusinessType().toString();
        this.address = new AddressDTO(b.getAddress());
        this.isCompany = b.isIsCompany();
        this.confirmed = b.getConfirmed();
        this.offer = offer;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public AddressDTO getAddress() {
        return address;
    }

    public void setAddress(AddressDTO address) {
        this.address = address;
    }

    public boolean isCompany() {
        return isCompany;
    }

    public void setCompany(boolean company) {
        isCompany = company;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    public long getNumberOfElements() {
        return numberOfElements;
    }

    public void setNumberOfElements(long numberOfElements) {
        this.numberOfElements = numberOfElements;
    }

    public OfferDTO getOffer() {
        return offer;
    }

    public void setOffer(OfferDTO offer) {
        this.offer = offer;
    }
}
