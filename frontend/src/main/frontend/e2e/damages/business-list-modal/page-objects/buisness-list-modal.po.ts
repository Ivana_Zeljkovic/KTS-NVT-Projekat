import {by, element} from "protractor";

export class BusinessListModalPage {

  getComponent() {
    return element(by.tagName('app-business-list-modal'));
  }

  getBlueButtons() {
    return element.all(by.css('.btn-send-request'))
  }

  getRedButtons(){
    return element.all(by.css('.btn-remove-request'))
  }

  getCloseButton(){
    return element(by.css('.modal-footer .btn-warning'))
  }

  getSendOffersButton(){
    return element(by.css('.modal-footer .btn-send-offers'))
  }

  getFirstBusinessName(){
    return element(by.xpath('//tr[1]/td[1]')).getText();
  }
}
