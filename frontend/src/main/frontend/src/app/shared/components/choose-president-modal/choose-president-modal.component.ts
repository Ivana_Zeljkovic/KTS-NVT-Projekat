import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
// model
import { Tenant } from '../../models/tenant';
// service
import { BuildingService } from '../../../core/services/building.service';
import { ToasterService } from 'angular5-toaster/dist';
import { CouncilPresidentService } from '../../../core/services/council-president.service';
import { TenantService } from "../../../core/services/tenant.service";
// model
import { PresidentUpdate } from '../../models/president-update';
import { Building } from '../../models/building';
import { PageAndSort } from "../../models/page-and-sort";
// error
import { AppError } from '../../errors/app-error';
import { BadRequestError } from '../../errors/bad-request-error';
import { NotFoundError } from '../../errors/not-found-error';
import { ForbiddenError } from '../../errors/forbidden-error';


@Component({
  selector: 'app-choose-president-modal',
  templateUrl: './choose-president-modal.component.html',
  styleUrls: ['./choose-president-modal.component.css']
})
export class ChoosePresidentModalComponent implements OnInit {

  modalRef: BsModalRef;
  building: Building;
  buildingId: number;
  tenants: Array<Tenant>;
  presidentUpdate: PresidentUpdate;


  currentPageTenant: number;
  totalItemsTenant: number;
  maxSizeTenant: number;
  itemsPerPageTenant: number;
  pageSizeTenant: string;
  sortDirectionTenant: string;
  sortPropertiesTenant: string[];

  constructor(private modalService : BsModalService, private councilPresidentService: CouncilPresidentService,
              private buildingService: BuildingService, private toasterService: ToasterService, private tenantService: TenantService) {

    this.currentPageTenant = 1;
    this.maxSizeTenant = 3;
    this.itemsPerPageTenant = 10;

    this.pageSizeTenant = '10';
    this.sortDirectionTenant = 'ASC';
    this.sortPropertiesTenant = ['first_name'];
  }

  ngOnInit() {}

  chooseTenant(tenant: Tenant){
    this.presidentUpdate = new PresidentUpdate(tenant.firstName, tenant.lastName, tenant.username);
    this.councilPresidentService.updatePart(this.presidentUpdate,'buildings',
      this.buildingId, 'council_president').subscribe(data => {
        this.building.presidentOfCouncil = tenant;
        this.modalRef.hide();
    }, (error: AppError) => {
      if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Error', '"Selected tenant is not member of building\'s ' +
          'council or the user doesn\'t live in this building!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
      else {
        this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
        throw error;
      }
    });
  }

  pageChanged(event: any) {
    this.currentPageTenant = event.page;
    this.getTenants();
  }

  getTenants(){
    let page = new PageAndSort((this.currentPageTenant - 1).toString(), this.pageSizeTenant, this.sortDirectionTenant,
      this.sortPropertiesTenant);
    this.tenantService.getTenantsByBuildingId(page, this.buildingId.toString())
      .subscribe(data => {
        this.tenants = data;
        if (data.length > 0)
          this.totalItemsTenant = data[0].numberOfElements;
        else this.totalItemsTenant = 0;
      },(error: AppError) => {
        if(error instanceof NotFoundError)
          this.toasterService.pop('error', 'Error', 'Building not found!');
        else if(error instanceof ForbiddenError)
          this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
        else if(error instanceof BadRequestError)
          this.toasterService.pop('error', 'Error', 'Bad request!');
        else {
          this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
          throw error;
        }
      });
  }

}
