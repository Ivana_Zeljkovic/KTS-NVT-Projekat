import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BsModalRef } from "ngx-bootstrap";
import * as _ from "lodash";
// model
import { QuestionCreate } from "../../../shared/models/question-create";


@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {
  @Input() model: QuestionCreate;
  @Input() initialValues: QuestionCreate;
  @Output() submitted = new EventEmitter<QuestionCreate>();
  disableSaveAndReset: boolean;
  newAnswer: string;

  constructor(private modalRef: BsModalRef) { }

  ngOnInit() {
    this.disableSaveAndReset = true;
    this.newAnswer = '';
  }

  addAnswer() {
    this.model.answers.push(this.newAnswer);
    this.newAnswer = '';
  }

  onSubmit() {
    this.submitted.emit(this.model);
    this.modalRef.hide();
  }

  reset() {
    this.model = _.cloneDeep(this.initialValues);
    this.disableSaveAndReset = true;
  }

  changeExist(newValues) {
    this.disableSaveAndReset = (JSON.stringify(newValues) === JSON.stringify(this.initialValues));
  }
}
