package rs.ac.uns.ftn.ktsnvt.web.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by Ivana Zeljkovic on 23/11/2017.
 */
public class ResponsibleForDamageTypeDTO implements Serializable {

    @NotNull
    private Long id;

    public ResponsibleForDamageTypeDTO() { }

    public ResponsibleForDamageTypeDTO(Long id) {
        this.id = id;
    }

    public Long getId() { return id; }

    public void setId(Long id) {
        this.id = id;
    }
}
