import { Component, EventEmitter, Output } from '@angular/core';
import { BsModalRef } from "ngx-bootstrap";


@Component({
  selector: 'app-confirm-delete-modal',
  templateUrl: './confirm-delete-modal.component.html',
  styleUrls: ['./confirm-delete-modal.component.css']
})
export class ConfirmDeleteModalComponent {
  @Output() confirmed = new EventEmitter<boolean>();

  constructor(private modalRef: BsModalRef) { }

  confirm() {
    this.confirmed.emit(true);
    this.modalRef.hide();
  }

  decline() {
    this.confirmed.emit(false);
    this.modalRef.hide();
  }
}
