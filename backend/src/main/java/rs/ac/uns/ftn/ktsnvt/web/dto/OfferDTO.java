package rs.ac.uns.ftn.ktsnvt.web.dto;

import rs.ac.uns.ftn.ktsnvt.model.entity.Offer;

import java.io.Serializable;

/**
 * Created by Katarina Cukurov on 24/11/2017.
 */
public class OfferDTO implements Serializable {
    private Long id;
    private Long businessId;
    private float price;

    public OfferDTO() { }

    public OfferDTO(Offer offer) {
        if(offer != null) {
            this.id = offer.getId();
            this.businessId = offer.getBusiness().getId();
            this.price = offer.getPrice();
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBusinessId() {
        return businessId;
    }

    public void setBusinessId(Long businessId) {
        this.businessId = businessId;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
