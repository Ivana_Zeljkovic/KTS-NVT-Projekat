package rs.ac.uns.ftn.ktsnvt.service;

import rs.ac.uns.ftn.ktsnvt.model.entity.Billboard;

/**
 * Created by Ivana Zeljkovic on 13/11/2017.
 */
public interface BillboardService {

    Billboard save(Billboard billboard);

}
