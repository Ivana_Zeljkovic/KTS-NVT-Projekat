-- ROLES
insert into authority (id, name) values (1, 'ADMIN');
insert into authority (id, name) values (2, 'TENANT');
insert into authority (id, name) values (3, 'COUNCIL_PRESIDENT');
insert into authority (id, name) values (4, 'INSTITUTION');
insert into authority (id, name) values (5, 'COMPANY');

-- DAMAGE TYPES
insert into damage_type (id, name) values (1, 'PLUMBING_DAMAGE');
insert into damage_type (id, name) values (2, 'ELECTRICAL_DAMAGE');
insert into damage_type (id, name) values (3, 'GAS_DAMAGE');
insert into damage_type (id, name) values (4, 'EXTERIOR_DAMAGE');
insert into damage_type (id, name) values (5, 'OTHER');




-- ADMINISTRATORS
  -- administrator 1
insert into administrator (id, first_name, last_name, version, deleted)
values (99, 'Ivana', 'Zeljkovic', 0, 0);
insert into account (id, username, password, business_owner_id, tenant_id, administrator_id, version, deleted)
values (99, 'ivana', '$2a$10$7k8Hzy7V5NsJahY0SUfTKef9i61DaPRxlixHFr14LL9KJHcNYWfoC', null, null, 99, 0, 0);
insert into account_authority(id, account_id, authority_id) values (99, 99, 1);
  -- administrator 2
insert into administrator (id, first_name, last_name, version, deleted)
values (100, 'Marko', 'Markovic', 0, 0);
insert into account (id, username, password, business_owner_id, tenant_id, administrator_id, version, deleted)
values (100, 'marko', '$2a$10$CS//RnaZMdVfXJEEcLW7QOYY3g/YK2y4RWtbHYpwwn10gwf.jxgsW', null, null, 100, 0, 0);
insert into account_authority(id, account_id, authority_id) values (100, 100, 1);




-- TENANTS
  -- tenant 1
insert into tenant (id, first_name, last_name, birth_date, email, phone_number, version, deleted, confirmed, apartment_live_in_id, council_id)
values (101, 'Katarina', 'Cukurov', '1995-01-11', 'kaca@gmail.com', '123456', 0, 0, 1, null, null);
  -- tenant 2
insert into tenant (id, first_name, last_name, birth_date, email, phone_number, version, deleted, confirmed, apartment_live_in_id, council_id)
values (102, 'Nikola', 'Garabandic', '1995-03-23', 'nikola@gmail.com', '321456', 0, 0, 1, null, null);
  -- tenant 3
insert into tenant (id, first_name, last_name, birth_date, email, phone_number, version, deleted, confirmed, apartment_live_in_id, council_id)
values (103, 'Jasmina', 'Markovic', '1995-09-21', 'jasmina@gmail.com', '456321', 0, 0, 1, null, null);
  -- tenant 4
insert into tenant (id, first_name, last_name, birth_date, email, phone_number, version, deleted, confirmed, apartment_live_in_id, council_id)
values (104, 'Sanja', 'Nikolic', '1995-03-09', 'sanja@gmail.com', '654321', 0, 0, 1, null, null);
  -- tenant 5
insert into tenant (id, first_name, last_name, birth_date, email, phone_number, version, deleted, confirmed, apartment_live_in_id, council_id)
values (105, 'Isidora', 'Krajinovic', '1995-11-02', 'isidora@gmail.com', '789654', 0, 0, 1, null, null);
  -- tenant 6
insert into tenant (id, first_name, last_name, birth_date, email, phone_number, version, deleted, confirmed, apartment_live_in_id, council_id)
values (106, 'Jasmina', 'Markovic', '1995-07-14', 'markovic@gmail.com', '987456', 0, 0, 1, null, null);
  -- tenant 7
insert into tenant (id, first_name, last_name, birth_date, email, phone_number, version, deleted, confirmed, apartment_live_in_id, council_id)
values (107, 'Leontina', 'Simonovic', '1995-05-09', 'leontina@gmail.com', '807123', 0, 0, 1, null, null);
-- tenant 8
insert into tenant (id, first_name, last_name, birth_date, email, phone_number, version, deleted, confirmed, apartment_live_in_id, council_id)
values (108, 'Jovanka', 'Stavric', '1995-05-09', 'jocas@gmail.com', '817123', 0, 0, 1, null, null);
-- tenant 9
insert into tenant (id, first_name, last_name, birth_date, email, phone_number, version, deleted, confirmed, apartment_live_in_id, council_id)
values (109, 'Milunka', 'Milosevic', '1995-05-09', 'milam@gmail.com', '897009', 0, 0, 0, null, null);
-- tenant 10
insert into tenant (id, first_name, last_name, birth_date, email, phone_number, version, deleted, confirmed, apartment_live_in_id, council_id)
values (110, 'Nestor', 'Jovanovic', '1995-05-09', 'nestor@gmail.com', '123098', 0, 0, 1, null, null);
-- tenant 11
insert into tenant (id, first_name, last_name, birth_date, email, phone_number, version, deleted, confirmed, apartment_live_in_id, council_id)
values (111, 'Sabina', 'Simovic', '1995-05-09', 'sabina@gmail.com', '345678', 0, 0, 1, null, null);
-- tenant 12
insert into tenant (id, first_name, last_name, birth_date, email, phone_number, version, deleted, confirmed, apartment_live_in_id, council_id)
values (112, 'Mirko', 'Mirkovic', '1995-05-09', 'mirko@gmail.com', '123546', 0, 0, 1, null, null);
-- tenant 13
insert into tenant (id, first_name, last_name, birth_date, email, phone_number, version, deleted, confirmed, apartment_live_in_id, council_id)
values (113, 'Radojka', 'Rajkovic', '1995-05-09', 'radojka@gmail.com', '345786', 0, 0, 1, null, null);





-- ACCOUNTS FOR TENANTS
  -- account tenant 1
insert into account (id, username, password, business_owner_id, tenant_id, administrator_id, version, deleted)
values (101, 'kaca', '$2a$10$FXALP0p8VlmzCbbCTDTV8uIS46lpCbCpeIq74qulCyxzhCsOazAZS', null, 101, null, 0, 0);
insert into account_authority(id, account_id, authority_id) values (101, 101, 2);
insert into account_authority(id, account_id, authority_id) values (102, 101, 3);
  -- account tenant 2
insert into account (id, username, password, business_owner_id, tenant_id, administrator_id, version, deleted)
values (102, 'nikola', '$2a$10$QIitOJ6WdmjiGSm.nucUWe562xT91MZKeHP5JJRe9mZSmQVDVN82S', null, 102, null, 0, 0);
insert into account_authority(id, account_id, authority_id) values (103, 102, 2);
  -- account tenant 3
insert into account (id, username, password, business_owner_id, tenant_id, administrator_id, version, deleted)
values (103, 'jasmina', '$2a$10$9ucG8I7g0eA49gj4e6SdOOywzAqab/KbHq61kOMQI5fidR/wvLpZy', null, 103, null, 0, 0);
insert into account_authority(id, account_id, authority_id) values (104, 103, 2);
  -- account tenant 4
insert into account (id, username, password, business_owner_id, tenant_id, administrator_id, version, deleted)
values (104, 'sanja', '$2a$10$FsLhuHoJXCzBlvR20nXSc.H9zXlS.Ha13Ap731akbO0WjgYZkrxoe', null, 104, null, 0, 0);
insert into account_authority(id, account_id, authority_id) values (105, 104, 2);
  -- account tenant 5
insert into account (id, username, password, business_owner_id, tenant_id, administrator_id, version, deleted)
values (105, 'isidora', '$2a$10$xTBkoxuims9wp3FIBTxxrOH4PSiNgefI124G35JdMeA5EPz0Jgjgq', null, 105, null, 0, 0);
insert into account_authority(id, account_id, authority_id) values (106, 105, 2);
  -- account tenant 6
insert into account (id, username, password, business_owner_id, tenant_id, administrator_id, version, deleted)
values (106, 'markovic', '$2a$10$cnKiWjwVEpTWrZuZnYoFD.uq1xJXE9/lzEBhitr4a8p1P6E60ymPi', null, 106, null, 0, 0);
insert into account_authority(id, account_id, authority_id) values (107, 106, 2);
  -- account tenant 7
insert into account (id, username, password, business_owner_id, tenant_id, administrator_id, version, deleted)
values (107, 'leontina', '$2a$10$QErmHWxVWEgm0b9Og250UumV1brq4/dQYb47XhKnzB0k/aOXq6Na.', null, 107, null, 0, 0);
insert into account_authority(id, account_id, authority_id) values (108, 107, 2);
insert into account_authority(id, account_id, authority_id) values (251, 107, 3);
-- account tenant 8
insert into account(id, username, password, business_owner_id, tenant_id, administrator_id, version, deleted)
values (246, 'jovanka', '$2a$10$aKtOJlHjHbWBZ5cfkzqXdO1jr1jPt8vZYb17iHXVfekBo9ZIrMumu', null, 108, null, 0, 0);
insert into account_authority(id, account_id, authority_id) values (245, 246, 2);
-- account tenant 9
insert into account(id, username, password, business_owner_id, tenant_id, administrator_id, version, deleted)
values (247, 'milunka', '$2a$10$aKtOJlHjHbWBZ5cfkzqXdO1jr1jPt8vZYb17iHXVfekBo9ZIrMumu', null, 109, null, 0, 0);
insert into account_authority(id, account_id, authority_id) values (246, 247, 2);
-- account tenant 10
insert into account(id, username, password, business_owner_id, tenant_id, administrator_id, version, deleted)
values (248, 'nestor', '$2a$10$HeIbLJke/n8qv9TqFk8w3.aSFGg/a5tmCMBT.bSRf10sLFcjaIhl6', null, 110, null, 0, 0);
insert into account_authority(id, account_id, authority_id) values (247, 248, 2);
-- account tenant 11
insert into account(id, username, password, business_owner_id, tenant_id, administrator_id, version, deleted)
values (249, 'sabina', '$2a$10$f.uoVVvxJpZ1Y/48UbA2K.dUdUmz1BjS88Qgl9JVXfo2txn6jZZv6', null, 111, null, 0, 0);
insert into account_authority(id, account_id, authority_id) values (248, 249, 2);
-- account tenant 12
insert into account(id, username, password, business_owner_id, tenant_id, administrator_id, version, deleted)
values (250, 'mirko', '$2a$10$agkqIDR9DvaLFayAYtI1ouXi.sN9Aan2mTbMqQNfF7TxzP340JpZe', null, 112, null, 0, 0);
insert into account_authority(id, account_id, authority_id) values (249, 250, 2);
-- account tenant 13
insert into account(id, username, password, business_owner_id, tenant_id, administrator_id, version, deleted)
values (251, 'radojka', '$2a$10$iEJ7jW541vSezdE.iWzu9.GSIWKlBp35bmg.tRlxJzwVyDkLwK7zq', null, 113, null, 0, 0);
insert into account_authority(id, account_id, authority_id) values (250, 251, 2);




-- CITIES
  -- city 1
insert into city (id, name, postal_number, version, deleted) values (100, 'Novi Sad', 21000, 0, 0);
  -- city 2
insert into city (id, name, postal_number, version, deleted) values (101, 'Beograd', 11000, 0, 0);

-- ADDRESSES
  -- address 1
insert into address (id, street, number, city_id, version, deleted) values (100, 'Janka Veselinovica', 8, 100, 0, 0);
  -- address 2
insert into address (id, street, number, city_id, version, deleted) values (101, 'Janka Veselinovica', 10, 100, 0, 0);
  -- address 3
insert into address (id, street, number, city_id, version, deleted) values (102, 'Janka Veselinovica', 12, 100, 0, 0);
  -- address 4
insert into address (id, street, number, city_id, version, deleted) values (103, 'Janka Veselinovica', 14, 100, 0, 0);
  -- address 5
insert into address (id, street, number, city_id, version, deleted) values (104, 'Janka Veselinovica', 16, 100, 0, 0);
  -- address 6
insert into address (id, street, number, city_id, version, deleted) values (105, 'Janka Veselinovica', 18, 100, 0, 0);
  -- address 7
insert into address (id, street, number, city_id, version, deleted) values (106, 'Janka Veselinovica', 20, 100, 0, 0);
  -- address 8
insert into address (id, street, number, city_id, version, deleted) values (107, 'Janka Veselinovica', 22, 100, 0, 0);
  -- address 9
insert into address (id, street, number, city_id, version, deleted) values (108, 'Janka Veselinovica', 24, 100, 0, 0);
  -- address 10
insert into address (id, street, number, city_id, version, deleted) values (109, 'Janka Veselinovica', 26, 100, 0, 0);
  -- address 11
insert into address (id, street, number, city_id, version, deleted) values (110, 'Janka Veselinovica', 28, 100, 0, 0);
  -- address 12
insert into address (id, street, number, city_id, version, deleted) values (111, 'Janka Veselinovica', 30, 100, 0, 0);





-- BUSINESS (COMPANIES)
  -- firm 1
insert into business (id, name, description, phone_number, email, business_type, is_company, version, deleted, confirmed, address_id)
values (100, 'First firm', 'Description for first firm', '123456789', 'ivana.zeljkovic995@gmail.com', 4, true, 0, 0, true, 102);
  -- firm 2
insert into business (id, name, description, phone_number, email, business_type, is_company, version, deleted, confirmed, address_id)
values (101, 'Second firm', 'Description for second firm', '987654321', 'ivana.zeljkovic995@gmail.com', 5, true, 0, 0, true, 103);
  -- firm 3
insert into business (id, name, description, phone_number, email, business_type, is_company, version, deleted, confirmed, address_id)
values (105, 'Third firm', 'Description for third firm', '987654321', 'ivana.zeljkovic995@gmail.com', 5, true, 0, 1, true, 104);
  -- firm 4
insert into business (id, name, description, phone_number, email, business_type, is_company, version, deleted, confirmed, address_id)
values (106, 'Fourth firm', 'Description for fourth firm', '987654321', 'ivana.zeljkovic995@gmail.com', 5, true, 0, 0, false, 105);


-- BUSINESS (INSTITUTIONS)
  -- institution 1
insert into business (id, name, description, phone_number, email, business_type, is_company, version, deleted, confirmed, address_id)
values (102, 'First institution', 'Description for first institution', '123456789', 'ivana.zeljkovic995@gmail.com', 0, false, 0, 0, true, 106);
  -- institution 2
insert into business (id, name, description, phone_number, email, business_type, is_company, version, deleted, confirmed, address_id)
values (103, 'Second institution', 'Description for second institution', '987654321', 'ivana.zeljkovic995@gmail.com', 1, false, 0, 0, true, 107);
  -- institution 3
insert into business (id, name, description, phone_number, email, business_type, is_company, version, deleted, confirmed, address_id)
values (104, 'Third institution', 'Description for third institution', '192837465', 'ivana.zeljkovic995@gmail.com', 1, false, 0, 0, true, 108);


-- ACCOUNTS FOR BUSINESS
  -- account for firm 1
insert into account (id, username, password, business_owner_id, tenant_id, administrator_id, version, deleted)
values (108, 'firm1', '$2a$10$IyhYaJ0UBfWmjisRsyugoOaOYP7pkKwSziAlA0HDmPpjRtzcASo1m', 100, null, null, 0, 0);
insert into account_authority(id, account_id, authority_id) values (109, 108, 5);
  -- account for firm 2
insert into account (id, username, password, business_owner_id, tenant_id, administrator_id, version, deleted)
values (109, 'firm2', '$2a$10$35iyKX1Br7G82uNSQ4FU8OkKs2AWVT.gBuNKYMO11oYg67neb.XZG', 101, null, null, 0, 0);
insert into account_authority(id, account_id, authority_id) values (110, 109, 5);
  -- account for institution 1
insert into account (id, username, password, business_owner_id, tenant_id, administrator_id, version, deleted)
values (110, 'inst1', '$2a$10$qUzbnYL4U0R7r.YEHngKcO90nJIHf7JWZ4jA.CCGLXIo/88fVEc6.', 102, null, null, 0, 0);
insert into account_authority(id, account_id, authority_id) values (111, 110, 4);
  -- account for institution 2
insert into account (id, username, password, business_owner_id, tenant_id, administrator_id, version, deleted)
values (111, 'inst2', '$2a$10$8nnNzaM1Es5qvJR2mVlT7Obqe2uxUTPlG9133..KlUxfxULRSLGZu', 103, null, null, 0, 0);
insert into account_authority(id, account_id, authority_id) values (112, 111, 4);
  -- account for institution 3
insert into account (id, username, password, business_owner_id, tenant_id, administrator_id, version, deleted)
values (112, 'inst3', '$2a$10$8nnNzaM1Es5qvJR2mVlT7Obqe2uxUTPlG9133..KlUxfxULRSLGZu', 104, null, null, 0, 0);
insert into account_authority(id, account_id, authority_id) values (113, 112, 4);
  -- account for firm 3
insert into account (id, username, password, business_owner_id, tenant_id, administrator_id, version, deleted)
values (113, 'firm3', '$2a$10$35iyKX1Br7G82uNSQ4FU8OkKs2AWVT.gBuNKYMO11oYg67neb.XZG', 105, null, null, 0, 0);
insert into account_authority(id, account_id, authority_id) values (114, 113, 5);





-- BUILDINGS
  -- building 1
insert into council (id, version, deleted, president_id, responsible_company_id) values (100, 0, 0, 101, 100);
insert into billboard (id, version, deleted) values (100, 0, 0);
insert into building (id, number_of_floors, has_parking, version, deleted, council_id, address_id, billboard_id)
values (100, 5, 1, 0, 0, 100, 100, 100);
  -- building 2
insert into council (id, version, deleted, president_id, responsible_company_id) values (101, 0, 0, 107, null);
insert into billboard (id, version, deleted) values (101, 0, 0);
insert into building (id, number_of_floors, has_parking, version, deleted, council_id, address_id, billboard_id)
values (101, 5, 1, 0, 0, 101, 101, 101);
  -- building 3
insert into council (id, version, deleted, president_id, responsible_company_id) values (102, 0, 0, null, null);
insert into billboard (id, version, deleted) values (102, 0, 0);
insert into building (id, number_of_floors, has_parking, version, deleted, council_id, address_id, billboard_id)
values (102, 5, 1, 0, 0, 102, 109, 102);
  -- building 4
insert into council (id, version, deleted, president_id, responsible_company_id) values (103, 0, 0, null, 100);
insert into billboard (id, version, deleted) values (103, 0, 0);
insert into building (id, number_of_floors, has_parking, version, deleted, council_id, address_id, billboard_id)
values (103, 5, 1, 0, 0, 103, 110, 103);
  -- building 5
insert into council (id, version, deleted, president_id, responsible_company_id) values (104, 0, 0, null, null);
insert into billboard (id, version, deleted) values (104, 0, 0);
insert into building (id, number_of_floors, has_parking, version, deleted, council_id, address_id, billboard_id)
values (104, 5, 1, 0, 0, 104, 111, 104);



-- APARTMENTS
  -- building 1
  -- apartment 1
insert into apartment (id, number, floor, surface, version, deleted, owner_id, building_id) values (100, 1, 1, 60, 0, 0, 101, 100);
update tenant set apartment_live_in_id = 100 where id = 102;
update tenant set council_id = 100 where id = 102;
  -- apartment 2
insert into apartment (id, number, floor, surface, version, deleted, owner_id, building_id) values (101, 2, 1, 60, 0, 0, 101, 100);
update tenant set apartment_live_in_id = 101 where id = 101;
update tenant set apartment_live_in_id = 101 where id = 103;
update tenant set council_id = 100 where id = 101;
  -- apartment 3
insert into apartment (id, number, floor, surface, version, deleted, owner_id, building_id) values (102, 3, 1, 60, 0, 0, 101, 100);
update tenant set apartment_live_in_id = 102 where id = 104;
update tenant set apartment_live_in_id = 102 where id = 105;
update tenant set apartment_live_in_id = 102 where id = 106;
update tenant set council_id = 100 where id = 104;
  -- apartment 4
insert into apartment (id, number, floor, surface, version, deleted, owner_id, building_id) values (104, 4, 1, 60, 0, 0, 102, 100);
  -- building 2
  -- apartment 1
insert into apartment (id, number, floor, surface, version, deleted, owner_id, building_id) values (103, 1, 1, 60, 0, 0, 102, 101);
update tenant set apartment_live_in_id = 103 where id = 107;
update tenant set council_id = 101 where id = 107;
  -- building 3
  -- apartment 1
insert into apartment (id, number, floor, surface, version, deleted, owner_id, building_id) values (105, 1, 1, 60, 0, 0, null, 102);
update tenant set apartment_live_in_id = 105 where id = 110;
update tenant set apartment_live_in_id = 105 where id = 111;
update tenant set council_id = 102 where id = 110;
  -- building 4
  -- apartment 1
insert into apartment (id, number, floor, surface, version, deleted, owner_id, building_id) values (106, 1, 1, 60, 0, 0, null, 103);
update tenant set apartment_live_in_id = 106 where id = 112;
update tenant set apartment_live_in_id = 106 where id = 113;
update tenant set council_id = 103 where id = 112;



-- RESPONSIBILITY FOR DAMAGE TYPE
  -- damage responsibility 1
insert into damage_responsibility (id, responsible_tenant_id, responsible_institution_id, damage_type_id, building_id)
values (100, 101, 102, 1, 100);
  -- damage responsibility 2
insert into damage_responsibility (id, responsible_tenant_id, responsible_institution_id, damage_type_id, building_id)
values (103, 103, null, 4, 100);
  -- damage responsibility 3
insert into damage_responsibility (id, responsible_tenant_id, responsible_institution_id, damage_type_id, building_id)
values (101, 102, null, 3, 100);
  -- damage responsibility 4
insert into damage_responsibility (id, responsible_tenant_id, responsible_institution_id, damage_type_id, building_id)
values (102, null, 103, 2, 100);





-- NOTIFICATIONS
  -- building 1 (billboard 1)
insert into notification (id, title, content, date, version, deleted, creator_id, billboard_id)
values (100, 'First notification', 'First notification content', '2017-11-27', 0, 0, 101, 100);
insert into notification (id, title, content, date, version, deleted, creator_id, billboard_id)
values (101, 'Second notification', 'Second notification content', '2017-11-28', 0, 0, 101, 100);
  -- building 2 (billboard 2)
insert into notification (id, title, content, date, version, deleted, creator_id, billboard_id)
values (102, 'Third notification', 'Third notification content', '2017-11-27', 0, 0, 107, 101);





-- MEETINGS
  -- building 1
    -- meeting 1
insert into meeting (id, date, version, duration, deleted, council_id, record_id)
values (100, '2017-11-28 10:34:09', 0, 60, 0, 100, null);
    -- meeting 2
insert into meeting (id, date, version, duration, deleted, council_id, record_id)
values (101, '2018-12-01 10:34:09', 0, 45, 0, 100, null);
    -- meeting 3
insert into meeting (id, date, version, duration, deleted, council_id, record_id)
values (102, '2018-11-29 10:00:00', 0, 45, 0, 100, null);
    -- meeting 4
insert into meeting (id, date, version, duration, deleted, council_id, record_id)
values (103, '2017-12-5 21:40:00', 0, 45, 0, 100, null);
  -- building 2
    -- meeting 1
insert into meeting (id, date, version, duration, deleted, council_id, record_id)
values (104, '2018-12-30 10:00:00', 0, 60, 0, 101, null);
    -- meeting 2
insert into meeting (id, date, version, duration, deleted, council_id, record_id)
values (105, '2018-12-30 07:00:00', 0, 45, 0, 101, null);



-- MEETING ITEMS
  -- building 1 (billboard 1)
    -- meeting item 1
insert into meetingitem (id, title, content, date, version, meeting_id, deleted, creator_id, questionnaire_id, billboard_id, damage_id)
values (100, 'First meeting item', 'First meeting item content', '2017-11-28 10:34:09', 0, 100, 0, 101, null, 100, null);
    -- meeting item 2
insert into meetingitem (id, title, content, date, version, meeting_id, deleted, creator_id, questionnaire_id, billboard_id, damage_id)
values (101, 'Second meeting item', 'Second meeting item content', '2017-11-29 07:54:09', 0, 100, 0, 102, null, 100, null);
    -- meeting item 3
insert into meetingitem (id, title, content, date, version, meeting_id, deleted, creator_id, questionnaire_id, billboard_id, damage_id)
values (102, 'Third meeting item', 'Third meeting item content', '2017-11-30 23:28:10', 0, null, 0, 102, null, 100, null);
    -- meeting item 4
insert into meetingitem (id, title, content, date, version, meeting_id, deleted, creator_id, questionnaire_id, billboard_id, damage_id)
values (104, 'Fourth meeting item', 'Fourth meeting item content', '2017-11-30 12:00:09', 0, 102, 0, 102, null, 100, null);
  -- building 2 (billboard 2)
    -- meeting item 1
insert into meetingitem (id, title, content, date, version, meeting_id, deleted, creator_id, questionnaire_id, billboard_id, damage_id)
values (103, 'Fifth meeting item', 'Fifth meeting item content', '2017-11-28 16:14:35', 0, null, 0, 107, null, 101, null);



-- DAMAGE
  -- damage 1
insert into damage (id, date, deleted, description, fixed, version, apartment_id, building_id, responsible_institution_id, responsible_person_id, selected_business_id, creator_id, type_id, urgent)
values (100, '2017-11-27', false, 'first damage description', false, 0, 102, 100, null, 104, null, 104, 1, false);
  -- damage 2
insert into damage (id, date, deleted, description, fixed, version, apartment_id, building_id, responsible_institution_id, responsible_person_id, selected_business_id, creator_id, type_id, urgent)
values (101, '2017-11-28', false, 'second damage description', false, 0, 101, 100, null, 101, null, 101, 2, false);
  -- damage 3
insert into damage (id, date, deleted, description, fixed, version, apartment_id, building_id, responsible_institution_id, responsible_person_id, selected_business_id, creator_id, type_id, urgent)
values (102, '2017-11-29', false, 'third damage description', false, 0, 101, 100, null, 103, null, 101, 3, false);
  -- damage 4
insert into damage (id, date, deleted, description, fixed, version, apartment_id, building_id, responsible_institution_id, responsible_person_id, selected_business_id, creator_id, type_id, urgent)
values (103, '2017-11-29', false, 'forth damage description', false, 0, 101, 100, 103, 103, 101, 103, 3, false);
  -- damage 5
insert into damage (id, date, deleted, description, fixed, version, apartment_id, building_id, responsible_institution_id, responsible_person_id, selected_business_id, creator_id, type_id, urgent)
values (104, '2017-11-29', false, 'fifth damage description', false, 0, 101, 100, 102, 103, 102, 101, 3, false);
  -- damage 6
insert into damage (id, date, deleted, description, fixed, version, apartment_id, building_id, responsible_institution_id, responsible_person_id, selected_business_id, creator_id, type_id, urgent)
values (105, '2017-11-29', false, 'sixth damage description', false, 0, 103, 101, 102, null, null, 107, 3, false);

-- BUSINESSES DAMAGES SENT TO
  -- damage 3
insert into damage_businesses_damage_sent_to_ids (damage_id, businesses_damage_sent_to_ids) values (102, 101);
  -- damage 2
insert into damage_businesses_damage_sent_to_ids (damage_id, businesses_damage_sent_to_ids) values (101, 100);

-- OFFER
insert into offer (id, accepted, price, version, business_id, damage_id) values (100, false, 500, 0, 100, 100);
insert into offer (id, accepted, price, version, business_id, damage_id) values (101, false, 750, 0, 100, 101);
insert into offer (id, accepted, price, version, business_id, damage_id) values (102, false, 750, 0, 102, 104);
insert into offer (id, accepted, price, version, business_id, damage_id) values (103, false, 750, 0, 101, 103);
insert into offer (id, accepted, price, version, business_id, damage_id) values (104, false, 750, 0, 102, 105);




-- QUESTIONNAIRE
  -- questionnaire 1
insert into questionnaire (id, date_expire, deleted, version, meeting_item_id) values (100, '2017-11-28 11:04:09', 0, 0, 100);
update meetingitem set questionnaire_id = 100 where id = 100;
  -- questionnaire 2
insert into questionnaire (id, date_expire, deleted, version, meeting_item_id) values (101, '2018-11-29 10:45:00', 0, 0, 104);
update meetingitem set questionnaire_id = 101 where id = 104;
  -- questionnaire 3
insert into questionnaire (id, date_expire, deleted, version, meeting_item_id) values (102, null, 0, 0, 102);
update meetingitem set questionnaire_id = 102 where id = 102;
  -- questionnaire 4
insert into questionnaire (id, date_expire, deleted, version, meeting_item_id) values (103, null, 0, 0, 103);
update meetingitem set questionnaire_id = 103 where id = 103;

-- QUESTION
  -- question 1 (questionnaire 1)
insert into question (id, content, version, questionnaire_id) values (100, 'Changing the council president', 0, 100);
    -- answer 1 (question 1)
insert into question_answers (question_id, answers) values (100, 'First tenant');
insert into question_answers (question_id, answers) values (100, 'Second tenant');

  -- question 1 (questionnaire 2)
insert into question (id, content, version, questionnaire_id) values (101, 'First question', 0, 101);
  -- question 2 (questionnaire 2)
insert into question (id, content, version, questionnaire_id) values (102, 'Second question', 0, 101);
    -- answer 1 (question 1)
insert into question_answers (question_id, answers) values (101, 'Yes');
    -- answer 2 (question 1)
insert into question_answers (question_id, answers) values (101, 'No');
    -- answer 1 (question 2)
insert into question_answers (question_id, answers) values (102, 'Confirm');
    -- answer 2 (question 2)
insert into question_answers (question_id, answers) values (102, 'Decline');

  -- question 1 (questionnaire 3)
insert into question (id, content, version, questionnaire_id) values (103, 'Some question', 0, 102);
    -- answer 1 (question 1)
insert into question_answers (question_id, answers) values (103, 'Maybe');
    -- answer 2 (question 1)
insert into question_answers (question_id, answers) values (103, 'Sure');




-- RECORD
insert into record(id, content, version, deleted, meeting_id) values (100, 'First record', 0, 0, 100);
update meeting set record_id = 100 where id = 100;


-- COMMENTS
insert into comment(id, content, date, deleted, version, creator_id, damage_id)
values (100, 'This is comment', '2017-11-29 10:45:00', 0, 0, 101, 101);

--VOTES
insert into vote(id, deleted, version, questionnaire_id, tenant_id) values (100, 0, 0, 100, 101);
insert into question_vote(id, answer, deleted, version, question_id, vote_id) values (100, 'First tenant', 0, 0, 100, 100);
