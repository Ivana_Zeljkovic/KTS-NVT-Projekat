package rs.ac.uns.ftn.ktsnvt.web.dto;

import org.hibernate.validator.constraints.NotEmpty;
import rs.ac.uns.ftn.ktsnvt.model.entity.Record;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by Nikola Garabandic on 22/11/2017.
 */
public class RecordCreateDTO implements Serializable{

    @NotNull
    @NotEmpty
    private String content;

    public RecordCreateDTO() { }

    public RecordCreateDTO(String content) {
        this.content = content;
    }

    public RecordCreateDTO(Record record) {
        if(record != null && record.getContent() != null)
            this.content = record.getContent();
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
