package rs.ac.uns.ftn.ktsnvt.repository.unit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.model.entity.*;
import rs.ac.uns.ftn.ktsnvt.repository.NotificationRepository;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Ivana Zeljkovic on 28/11/2017.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class NotificationRepositoryUnitTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private NotificationRepository notificationRepository;

    private Long billboard_id;
    private Long tenant_id;
    private Long notification_id_1;
    private Long notification_id_2;
    private Long notification_id_3;


    @Before
    public void setUp() {
        // BILLBOARDS
        Billboard billboard = new Billboard();
        Billboard billboard_2 = new Billboard();
        this.billboard_id = (Long)entityManager.persistAndGetId(billboard);
        entityManager.persist(billboard_2);

        // ACCOUNT, TENANT
        Account account = new Account("ivana", "ivana");
        Tenant tenant = new Tenant("Ivana", "Zeljkovic", new Date(),
                "ivana@gmail.com", "123456789", true, account);
        entityManager.persist(account);
        this.tenant_id = (Long)entityManager.persistAndGetId(tenant);


        // NOTIFICATIONS
        Notification notification_1 = new Notification(
                "First notification", "First notification content", new Date(), tenant, billboard);
        Notification notification_2 = new Notification(
                "Second notification", "Second notification content", new Date(), tenant, billboard);
        Notification notification_3 = new Notification(
                "Third notification", "Third notification content", new Date(), tenant, billboard_2);
        this.notification_id_1 = (Long)entityManager.persistAndGetId(notification_1);
        this.notification_id_2 = (Long)entityManager.persistAndGetId(notification_2);
        this.notification_id_3 = (Long)entityManager.persistAndGetId(notification_3);
        billboard.getNotifications().add(notification_1);
        billboard.getNotifications().add(notification_2);
        billboard_2.getNotifications().add(notification_3);
        entityManager.merge(billboard);
        entityManager.merge(billboard_2);
    }

    @Test
    public void testFindAllByBillboard() {
        /*
         * Test proverava ispravnost rada metode findAllByBillboard repozitorijuma NotificationRepository
         * (pronalazenje svih javnih obavestenja sa bilborda zgrade)
         * */

        /*
        oglasnoj tabli sa ID-em = billboard_id pripadaju 2 obavestenja:
            1 - ID = notification_id_1,
                ID kreatora = tenant_id
                content = First notification
            2 - ID = notification_id_2
                ID kreatora = tenant_id
                content = Second notification
         */
        Page<Notification> notifications = this.notificationRepository.findAllByBillboard(this.billboard_id,
                new PageRequest(0, 2));

        assertEquals(2L, notifications.getTotalElements());
    }

    @Test
    public void testFindOneByIdAndBillboard() {
        /*
         * Test proverava ispravnost rada metode findOneByIdAndBillboard repozitorijuma NotificationRepository
         * (pronalazenje konkretnog javnog obavestenja na osnovu zadatog ID-a i ID-a bilborda zgrade kom obavestenje pripada)
         * u slucaju kada postoji obavestenje sa zadatim ID-em na bilbordu sa zadatim ID-em
         * */

        /*
        oglasnoj tabli sa ID-em = billboard_id pripada obavestenje:
            ID = notification_id_1,
            ID kreatora = tenant_id,
            content = First notification
        */
        Notification notification = this.notificationRepository.findOneByIdAndBillboard(this.notification_id_1, this.billboard_id);

        assertNotNull(notification);
        assertEquals(this.notification_id_1, notification.getId());
        assertEquals(this.tenant_id, notification.getCreator().getId());
        assertEquals("First notification content", notification.getContent());
        assertEquals("First notification", notification.getTitle());
    }

    @Test
    public void testFindOneByIdAndBillboardNonexistent() {
        /*
         * Test proverava ispravnost rada metode findOneByIdAndBillboard repozitorijuma NotificationRepository
         * (pronalazenje konkretnog javnog obavestenja na osnovu zadatog ID-a i ID-a bilborda zgrade kom obavestenje pripada)
         * u slucaju kada ne postoji obavestenje sa zadatim ID-em na bilbordu sa zadatim ID-em
         * */

        // oglasnoj tabli ID-em = billboard_id ne pripada obavestenje sa ID-em notification_id_3:
        Notification notification = this.notificationRepository.findOneByIdAndBillboard(this.notification_id_3, this.billboard_id);

        assertNull(notification);
    }
}
