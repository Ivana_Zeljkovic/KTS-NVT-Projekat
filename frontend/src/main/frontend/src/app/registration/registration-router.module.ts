import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
// component
import { TenantRegistrationComponent } from "./tenant-registration/tenant-registration.component";
import { BusinessRegistrationComponent } from "./business-registration/business-registration.component";

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'tenant', component: TenantRegistrationComponent },
  { path: 'institution', component: BusinessRegistrationComponent },
  { path: 'firm', component : BusinessRegistrationComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule]
})
export class RegistrationRouterModule { }
