package rs.ac.uns.ftn.ktsnvt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.MeetingItem;
import rs.ac.uns.ftn.ktsnvt.repository.MeetingItemRepository;
import rs.ac.uns.ftn.ktsnvt.repository.QuestionRepository;
import rs.ac.uns.ftn.ktsnvt.repository.QuestionnaireRepository;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Ivana Zeljkovic on 13/11/2017.
 */
@Service
public class MeetingItemServiceImpl implements MeetingItemService {

    @Autowired
    private MeetingItemRepository meetingItemRepository;

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private QuestionnaireRepository questionnaireRepository;


    @Override
    @Transactional(readOnly = false)
    public MeetingItem save(MeetingItem meetingItem) {
        return this.meetingItemRepository.save(meetingItem);
    }

    @Override
    @Transactional(readOnly = true)
    public List<MeetingItem> findAllOnBillboard(Long billboardId) {
        return this.meetingItemRepository.findAllOnBillboard(billboardId);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<MeetingItem> findAllOnBillboard(Long billboardId, Integer pageNumber, Integer pageSize,
                                                String sortDirection, String sortProperty) {
        return this.meetingItemRepository.findAllOnBillboard(billboardId,
                new PageRequest(pageNumber, pageSize,
                        (sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortProperty));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<MeetingItem> findAllOnBillboardFromTo(Long billboardId, Date from, Date to, Integer pageNumber,
                                            Integer pageSize, String sortDirection, String sortProperty) {
        return this.meetingItemRepository.findAllFromDateToDate(billboardId, from, to,
                new PageRequest(pageNumber, pageSize,
                        (sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortProperty));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<MeetingItem> findAllForMeeting(Long meetingId, Integer pageNumber, Integer pageSize,
                                               String sortDirection, String sortProperty) {
        return this.meetingItemRepository.findAllForMeeting(meetingId,
                new PageRequest(pageNumber, pageSize,
                        (sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortProperty));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<MeetingItem> findAllForRecord(Long meetingId, Integer pageNumber, Integer pageSize,
                                              String sortDirection, String sortProperty) {
        return this.meetingItemRepository.findAllForRecord(meetingId,
                new PageRequest(pageNumber, pageSize,
                        (sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortProperty));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<MeetingItem> findAllWithActiveQuestionnairesForTenant(Long tenantId, Integer pageNumber,
                                                                      Integer pageSize, String sortDirection, String sortProperty) {
        return this.meetingItemRepository.findAllWithActiveQuestionnairesForTenant(tenantId, new Date(),
                new PageRequest(pageNumber, pageSize,
                        (sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortProperty));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<MeetingItem> findAllFinishedQuestionnairesInBuilding(Long buildingId, Integer pageNumber,
                                                                     Integer pageSize, String sortDirection, String sortProperty) {
        return this.meetingItemRepository.findAllFinishedQuestionnairesInBuilding(buildingId, new Date(),
                new PageRequest(pageNumber, pageSize,
                        (sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortProperty));
    }

    @Override
    @Transactional(readOnly = true)
    public MeetingItem findOne(Long id) {
        MeetingItem meetingItem = this.meetingItemRepository.findOne(id);
        if(meetingItem == null) throw new NotFoundException("Meeting item not found!");
        return meetingItem;
    }

    @Override
    @Transactional(readOnly = true)
    public void findOneByIdAndBillboard(Long id, Long billboardId) {
        MeetingItem meetingItem = this.meetingItemRepository.findOneByIdAndBillboard(id, billboardId);
        if(meetingItem == null)
            throw new BadRequestException("Selected meeting item doesn't belong to selected building!");
        else if(meetingItem.getMeeting() != null)
            throw new BadRequestException("Selected meeting item is already processed!");
    }

    @Override
    public void checkMeetingItemInBuilding(MeetingItem meetingItem, List<MeetingItem> items) {
        HashMap<Long, Long> meetingItems = new HashMap<>();
        items.forEach(item -> meetingItems.put(item.getId(), item.getId()));

        if(!meetingItems.containsKey(meetingItem.getId()))
            throw new BadRequestException("Selected meeting item doesn't belong to selected building!");
    }

    @Override
    public void checkMeetingIsNull(MeetingItem meetingItem) {
        if(meetingItem.getMeeting() != null)
            throw new BadRequestException("Meeting item already belongs to some meeting");
    }

    @Override
    @Transactional(readOnly = false)
    public Iterable<MeetingItem> save(Iterable<MeetingItem> meetingItems) {
        return this.meetingItemRepository.save(meetingItems);
    }

    @Override
    public void checkMeetingItemQuestionnaire(MeetingItem meetingItem, Long questionnaireId) {
        if(!meetingItem.getQuestionnaire().getId().equals(questionnaireId))
            throw new BadRequestException("Selected questionnaire doesn't belong to selected meeting item!");
    }

    @Override
    @Transactional(readOnly = false)
    public void remove(Long meetingItemId) {
        this.meetingItemRepository.delete(meetingItemId);
    }

    @Override
    @Transactional(readOnly = true)
    public void containsDamage(Long damageId) {
        List<MeetingItem> meetingItems = this.meetingItemRepository.findAllByDamageId(damageId);
        if(!meetingItems.isEmpty())
            throw new BadRequestException("Damage with given ID is already topic of some meeting items");
    }
}
