package rs.ac.uns.ftn.ktsnvt.web.controller.aspects;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;

@Component
@Aspect
public class InvalidDTOAspect {

    @Before("execution(* rs.ac.uns.ftn.ktsnvt.web.controller.*.*(..,org.springframework.validation.BindingResult)) && args(..,errors)")
    public void logBefore(BindingResult errors) {
        if(errors.hasErrors()) throw new BadRequestException("The request object is not valid!");
    }
}
