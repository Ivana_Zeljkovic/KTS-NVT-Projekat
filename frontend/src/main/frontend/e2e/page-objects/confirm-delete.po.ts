import {by, element, ElementFinder} from "protractor";

export class ConfirmDeletePage {
  // get methods
  getConfirmDeleteElement(): ElementFinder {
    return element(by.tagName('app-confirm-delete-modal'));
  }

  getConfirmButton(): ElementFinder {
    return element(by.buttonText('Yes'));
  }

  getDeclineButton(): ElementFinder {
    return element(by.buttonText('No'));
  }
}
