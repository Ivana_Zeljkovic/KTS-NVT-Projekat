import {by, element, ElementArrayFinder, ElementFinder} from "protractor";

export class DamageForQuestionnairePage {
  // get methods
  getDamageForQuestionnaireElement(): ElementFinder {
    return element(by.tagName('app-damage-for-questionnaire'));
  }

  getTitleFromHeader(): ElementFinder {
    return element(by.css('.modal-title'));
  }

  getCloseButtonFromHeader(): ElementFinder {
    return element(by.css('.modal-header > button'));
  }

  getDamageList(): ElementFinder {
    return element(by.id('damageList'));
  }

  getMessageNoDamageInList(): ElementFinder {
    return element(by.id('messageEmptyList'));
  }

  getPanelsFromDamageList(): ElementArrayFinder {
    return element.all(by.css('#damageList > div.panel'));
  }

  getPaginatorInDamageList(): ElementFinder {
    return element(by.css('#damageList pagination'));
  }

  getCreatorNameFromDamagePanel(index: number): ElementFinder {
    return element(by.css(`#damageList > .panel:nth-child(${index+1}) .damage-metadata span:nth-child(2)`));
  }

  getDateFromDamagePanel(index: number): ElementFinder {
    return element(by.css(`#damageList > .panel:nth-child(${index+1}) .damage-metadata span:nth-child(4)`));
  }

  getDamageTypeFromDamagePanel(index: number): ElementFinder {
    return element(by.css(`#damageList > .panel:nth-child(${index+1}) .panel-body > p:first-of-type`));
  }

  getDamageDescriptionFromDamagePanel(index: number): ElementFinder {
    return element(by.css(`#damageList > .panel:nth-child(${index+1}) .panel-body > p:last-of-type`));
  }

  getMenuFromDamagePanel(index: number): ElementFinder {
    return element(by.css(`#damageList > .panel:nth-child(${index+1}) .damage-menu`));
  }

  getChooseOptionFromDamagePanel(index: number): ElementFinder {
    return element(by.css(`#damageList > .panel:nth-child(${index+1}) .damage-menu a`));
  }
}
