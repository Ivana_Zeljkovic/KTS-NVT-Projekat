import {browser} from "protractor";
import {BusinessProfilePage} from "./page-objects/business-profile.po";
import {LoginPage} from "../../page-objects/login.po";
import {BusinessProfileAccountsModalPage} from "../../shared/business-profile-accounts-modal/page-objects/business-profile-accounts-modal.po";

describe('Business profile', () => {

  let businessProfilePage: BusinessProfilePage;
  let loginPage: LoginPage;
  let businessProfileAccountsModal: BusinessProfileAccountsModalPage;


  beforeAll(() => {
    loginPage = new LoginPage();
    browser.get("http://localhost:49152/#/login?returnUrl=%2Fprofile");
    loginPage.setUsername('firm2');
    loginPage.setPassword('firm2');
    loginPage.getSignInButton().submit();

    businessProfilePage = new BusinessProfilePage();
    browser.wait(function () {
      return browser.getCurrentUrl().then(value => {
        return value == 'http://localhost:49152/#/profile'
      });
    });

  });

  beforeEach(() => {
    businessProfilePage = new BusinessProfilePage();
    businessProfileAccountsModal = new BusinessProfileAccountsModalPage();
  });

  afterEach(() => {

    if (businessProfilePage.getResetButton().isEnabled())//only in first test this button will be disabled.
      businessProfilePage.getResetButton().click();

  });

  afterAll(() => {
    browser.executeScript('window.localStorage.clear();');
  });

  it('Should display all inputs of business profile correctly', () => {

    browser.wait(function () {
      return businessProfilePage.getUsernameInput().isDisplayed();
    });


    expect(businessProfilePage.getUsernameInput().isDisplayed()).toBe(true);
    expect(businessProfilePage.getBusinessNameInput().isDisplayed()).toBe(true);
    expect(businessProfilePage.getPhoneNumberInput().isDisplayed()).toBe(true);
    expect(businessProfilePage.getBuisessTypeInput().isDisplayed()).toBe(true);
    expect(businessProfilePage.getDescriptionInput().isDisplayed()).toBe(true);

    expect(businessProfilePage.getCityInput().isDisplayed()).toBe(true);
    expect(businessProfilePage.getStreetInput().isDisplayed()).toBe(true);
    expect(businessProfilePage.getNumberInput().isDisplayed()).toBe(true);
    expect(businessProfilePage.getPostalCodeInput().isDisplayed()).toBe(true);

    expect(businessProfilePage.getCurrentPasswordInput().isDisplayed()).toBe(true);
    expect(businessProfilePage.getNewPasswordInput().isDisplayed()).toBe(true);
    expect(businessProfilePage.getConfirmPasswordInput().isDisplayed()).toBe(true);


  });


  it('Should revert values back to original when reset button is clicked', () => {

    browser.wait(function () {
      return businessProfilePage.getResetButton().isPresent();
    });

    let name = businessProfilePage.getBusinessNameInput().getAttribute('value');
    businessProfilePage.setName('fasafs');
    let newName = businessProfilePage.getBusinessNameInput().getAttribute('value');

    expect(businessProfilePage.getBusinessNameInput().getAttribute('value')).toEqual(newName);

    businessProfilePage.getResetButton().click().then(function () {
      expect(businessProfilePage.getBusinessNameInput().getAttribute('value')).toEqual(name);
    })

  });



  it('Should display error about business name required when field is empty and lost focus', () => {
    businessProfilePage.setName('');
    businessProfilePage.getDescriptionInput().click();
    expect(businessProfilePage.getBusinessNameRequired().isDisplayed()).toBe(true);
  });

  it('Should display error about business name minimum length', () => {
    businessProfilePage.setName('j');
    businessProfilePage.getDescriptionInput().click();
    expect(businessProfilePage.getBusinessNameMinLengthError().isDisplayed()).toBe(true);

  });

  it('Should display error when email input is an empty value and lost focus', () => {
    businessProfilePage.setEmail('');
    businessProfilePage.getDescriptionInput().click();
    expect(businessProfilePage.getEmailRequired().isDisplayed()).toBe(true);
  });

  it('Should display error when email has an invalid format', () => {
    businessProfilePage.setEmail('afsfasfa');
    businessProfilePage.getDescriptionInput().click();
    expect(businessProfilePage.getEmailPattern().isDisplayed()).toBe(true);
  });

  it('Should display error when phone number is set to empty and lost focus', () => {
    businessProfilePage.setPhoneNumber('');
    businessProfilePage.getDescriptionInput().click();
    expect(businessProfilePage.getPhoneNumberRequired().isDisplayed()).toBe(true);
  });

  it('Should display error when phone number contains space', () => {
    businessProfilePage.setPhoneNumber('51 13551');
    businessProfilePage.getDescriptionInput().click();
    expect(businessProfilePage.getPhoneNumberContainsSpace().isDisplayed()).toBe(true);
  });

  it('Should display error when phone number doesn\'t match pattern', () => {
    businessProfilePage.setPhoneNumber('8190a');
    businessProfilePage.getDescriptionInput().click();
    expect(businessProfilePage.getPhoneNumberPattern().isDisplayed()).toBe(true);
  });

  it('Should display error when description is set to empty string and lost focus', () => {
    businessProfilePage.setDescription('');
    businessProfilePage.getPhoneNumberInput().click();
    expect(businessProfilePage.getDescriptionRequired().isDisplayed()).toBe(true);
  });


  it('Should display description error about descriptions minimum length', () => {
    businessProfilePage.setDescription('a');
    businessProfilePage.getPhoneNumberInput().click();
    expect(businessProfilePage.getDescriptionMinimumLength().isDisplayed()).toBe(true);
  });


  it('Should display street is required when user enters empty string and loses focus', () => {
    businessProfilePage.setStreet('');
    businessProfilePage.getDescriptionInput().click();
    expect(businessProfilePage.getStreetRequired().isDisplayed()).toBe(true);
  });

  it('Should display street\'s minimum length when user doesn\'t input enough characters', () => {
    businessProfilePage.setStreet('a');
    businessProfilePage.getDescriptionInput().click();
    expect(businessProfilePage.getStreetMinimumLength().isDisplayed()).toBe(true);
  });

  it('Should display number is required when user enters empty string and loses focus', () => {
    businessProfilePage.setNumber('');
    businessProfilePage.getDescriptionInput().click();
    expect(businessProfilePage.getNumberRequired().isDisplayed()).toBe(true);
  });


  it('Should display city is required when user enters empty string and loses focus', () => {
    businessProfilePage.setCity('');
    businessProfilePage.getDescriptionInput().click();
    expect(businessProfilePage.getCityRequired().isDisplayed()).toBe(true);
  });

  it('Should display postal code is required when user enters empty string and loses focus', () => {
    businessProfilePage.setPostalCode('');
    businessProfilePage.getDescriptionInput().click();
    expect(businessProfilePage.getPostalCodeRequired().isDisplayed()).toBe(true);
  });


  it('Should display postal code needs to have exactly 5 digits', () => {
    businessProfilePage.setPostalCode('1543');
    businessProfilePage.getDescriptionInput().click();
    expect(businessProfilePage.getPostalCodePattern().isDisplayed()).toBe(true);
  });

  it('Should display new password\'s minimum length is 5 characters', () => {
    businessProfilePage.setNewPassword('a');
    businessProfilePage.getDescriptionInput().click();
    expect(businessProfilePage.getNewPasswordLength().isDisplayed()).toBe(true);
  });

  it('Should display current password is required when new password with valid length is entered', () => {
    businessProfilePage.setNewPassword('123341a');
    businessProfilePage.getDescriptionInput().click();
    expect(businessProfilePage.getCurrentPasswordRequired().isDisplayed()).toBe(true);
  });


  it('Should display confirm password is required when new password with valid length is entered', () => {
    businessProfilePage.setNewPassword('123341a');
    businessProfilePage.getDescriptionInput().click();
    expect(businessProfilePage.getConfirmPasswordRequired().isDisplayed()).toBe(true);
  });

  it('Should display passwords don\'t match when new password and confirm password input fields have different content', () => {
    businessProfilePage.setNewPassword('123341a');
    businessProfilePage.setConfirmPassword('123341');
    businessProfilePage.getDescriptionInput().click();
    expect(businessProfilePage.getConfirmPasswordMatch().isDisplayed()).toBe(true);
  });


  it('Should enable reset and save button when new data is entered in any of the fields', () => {
    businessProfilePage.setCurrentPassword('firm1');
    expect(businessProfilePage.getResetButton().isEnabled()).toBe(true);
    expect(businessProfilePage.getSaveButton().isEnabled()).toBe(true);
  });

  it('Should open modal where all accounts have been listed.', () => {
    businessProfilePage.getManageAccounts().click().then(function () {
      expect(businessProfileAccountsModal.getComponent().isDisplayed()).toBe(true).then(function () {
          businessProfileAccountsModal.getCloseButton().click();
        }
      );
    })
  });


  //This test can only be performed on a business which doesn't have anything in their damage list to fix nor they're responsible for any type of damage in any building.
  /*it('Should change deactivate to activate if firm doesn\'t  have any damages for repair or is not responsible for any type of damage in any building', ()=>{
      businessProfilePage.getDeactivateButton().click().then(function () {
          if(businessProfilePage.getActivateButton().isDisplayed()){

          }
      })
  });
*/
});


