package rs.ac.uns.ftn.ktsnvt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.ktsnvt.model.entity.Administrator;

/**
 * Created by Ivana Zeljkovic on 05/11/2017.
 */
public interface AdministratorRepository extends JpaRepository<Administrator, Long>{}
