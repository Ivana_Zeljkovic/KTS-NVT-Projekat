package rs.ac.uns.ftn.ktsnvt.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import rs.ac.uns.ftn.ktsnvt.model.entity.Tenant;

import java.util.List;

/**
 * Created by Ivana Zeljkovic on 05/11/2017.
 */
public interface TenantRepository extends JpaRepository<Tenant, Long> {

    @Override
    @Query(value = "SELECT t FROM Tenant t")
    Page<Tenant> findAll(Pageable pageable);

    @Query(value = "SELECT t FROM Tenant t WHERE t.apartmentLiveIn.id IN (SELECT DISTINCT(a.id) FROM Apartment a WHERE a.building.id = :id)" +
            "AND lower(t.firstName) like lower(:firstName) and lower(t.lastName) like lower(:lastName) AND t.id IN" +
            "\t(SELECT a.tenant.id FROM Account a WHERE lower(a.username) like :username)")
        List<Tenant> findTenantsByBuildingAndNameAndUsername(@Param("id") Long id, @Param("firstName") String name,
                                                         @Param("lastName") String lastName, @Param("username") String username);

    @Query(value = "SELECT * FROM Tenant t WHERE lower(t.first_name) like lower(:first_name) AND lower(t.last_name) like lower(:last_name) " +
            "UNION " +
            "SELECT * FROM TENANT t WHERE t.id IN (SELECT a.tenant_id FROM Account a WHERE lower(a.username) like (:username))", nativeQuery = true)
    List<Tenant> findByNameOrUsername(@Param("first_name") String name, @Param("last_name") String lastName,
                                      @Param("username") String username);

    @Query(value = "SELECT t FROM Tenant t WHERE lower(t.firstName) = lower(:firstName) AND lower(t.lastName) = lower(:lastName) AND " +
            "t.id IN (SELECT a.tenant.id FROM Account a WHERE a.username = :username)")
    Tenant findByNameAndUsername(@Param("firstName") String firstName, @Param("lastName") String lastName,
                                 @Param("username") String username);

    @Query(value = "SELECT t FROM Tenant t WHERE t.confirmed = TRUE AND t.apartmentLiveIn.id IN " +
            "(SELECT DISTINCT(a.id) FROM Apartment a WHERE a.building.id = :id)")
    Page<Tenant> findAllByBuildingId(@Param("id") Long buildingId, Pageable pageable);

    @Query(value = "SELECT t FROM Tenant t WHERE t.id != :id AND t.confirmed = TRUE AND t.apartmentLiveIn.id IN " +
            "(SELECT DISTINCT(a.id) FROM Apartment a WHERE a.building.id = :idBuilding)")
    Page<Tenant> findAllByBuildingIdExceptCurrent(@Param("idBuilding") Long buildingId,
                                                  @Param("id") Long currentTenantId, Pageable pageable);

    @Query(value = "SELECT t FROM Tenant t WHERE t.id != :id AND t.council.id = " +
            "(SELECT b.council.id FROM Building b WHERE b.id = :buildingId)")
    Page<Tenant> findAllCouncilMembers(@Param("buildingId") Long buildingId, @Param("id") Long currentTenantId,
                                       Pageable pageable);

    @Query(value = "SELECT t FROM Tenant t WHERE t.id != :id AND t.apartmentLiveIn.id = :apartmentId")
    Page<Tenant> findAllInApartment(@Param("apartmentId") Long apartmentId, @Param("id") Long currentTenantId,
                                    Pageable pageable);

    @Query(value = "SELECT t FROM Tenant t WHERE t.id != :id AND t.id != :idPresident AND t.apartmentLiveIn.id = :apartmentId")
    Page<Tenant> findAllInApartmentExceptPresident(@Param("apartmentId") Long apartmentId, @Param("id") Long currentTenantId,
                                                   @Param("idPresident") Long currentPresidentId, Pageable pageable);
}
