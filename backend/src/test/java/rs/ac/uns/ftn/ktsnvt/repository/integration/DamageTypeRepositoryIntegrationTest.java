package rs.ac.uns.ftn.ktsnvt.repository.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.model.entity.DamageType;
import rs.ac.uns.ftn.ktsnvt.repository.DamageTypeRepository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Katarina Cukurov on 28/11/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class DamageTypeRepositoryIntegrationTest {

    @Autowired
    private DamageTypeRepository damageTypeRepository;

    @Test
    @Transactional
    public void testFindByName(){
        /*
         * Test proverava ispravnost rada metode findByName repozitorijuma DamageTypeRepository
         * (pronalazenje svih tipova kvara koji su u bazi na osnovu naziva kvara)
         * */

        /*
        u bazi postoji tip kvara sa sledecim podacima:
            id = 2,
            name = ELECTRICAL_DAMAGE
        */
        DamageType damageType = this.damageTypeRepository.findByName("ELECTRICAL_DAMAGE");

        assertNotNull(damageType);
        assertEquals("ELECTRICAL_DAMAGE", damageType.getName());
        assertEquals(Long.valueOf(2), damageType.getId());
    }
}
