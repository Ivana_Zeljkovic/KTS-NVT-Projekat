export class QuestionCreate {
  constructor(public content: string,
              public answers: Array<string>) { }
}
