package rs.ac.uns.ftn.ktsnvt.service.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Apartment;
import rs.ac.uns.ftn.ktsnvt.service.ApartmentService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Katarina Cukurov on 30/11/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class ApartmentServiceIntegrationTest {

    @Autowired
    private ApartmentService apartmentService;


    @Test
    public void testFindOne() {
        /*
         * Test proverava ispravnost rada metode findOne servisa ApartmentService
         * (dobavljanje stana na osnovu ID-a)
         * u slucaju kada postoji stan sa zadatim ID-em
         * */

        /*
        stan sa ID-em = 100 ima podatke:
            id = 100,
            number = 1,
            floor = 1,
            surface = 60
        */
        Apartment apartment = this.apartmentService.findOne(new Long(100));

        assertNotNull(apartment);
        assertEquals(Long.valueOf(100), apartment.getId());
        assertEquals(1, apartment.getNumber());
        assertEquals(1, apartment.getFloor());
        assertEquals(60, apartment.getSurface());
    }

    @Test(expected = NotFoundException.class)
    public void testFindOneNonexistent() {
         /*
         * Test proverava ispravnost rada metode findOne servisa ApartmentService
         * (dobavljanje stana na osnovu ID-a)
         * u slucaju kada ne postoji stan sa zadatim ID-em - ocekivano NotFoundException
         * */

        // apartment sa ID-em = 500 ne postoji
        Apartment apartment = this.apartmentService.findOne(new Long(500));
    }

    @Test
    @Transactional
    public void checkBuilding() {
        /*
         * Test proverava ispravnost rada metode checkBuilding servisa ApartmentService
         * (provera da li prosledjeni stan pripada zgradi ciji je ID prosledjen)
         * u slucaju kada prosledjeni stan pripada zgradi ciji je ID prosledjen u metodu
         * */

        /*
        stan sa ID-em = 100 pripada zgradi sa ID-em = 100 i ima podatke:
            id = 100,
            number = 1,
            floor = 1,
            surface = 60
            building_id = 100
        */
        Apartment apartment = this.apartmentService.findOne(new Long(100));
        this.apartmentService.checkBuilding(apartment, new Long(100));
    }

    @Test(expected = BadRequestException.class)
    @Transactional
    public void checkBuildingInvalid() {
        /*
         * Test proverava ispravnost rada metode checkBuilding servisa ApartmentService
         * (provera da li prosledjeni stan pripada zgradi ciji je ID prosledjen)
         * u slucaju kada prosledjeni stan ne pripada zgradi ciji je ID prosledjen u metodu - ocekivano BadRequestException
         * */

        // stan sa ID-em = 103 ne pripada zgradi sa ID-em = 100
        Apartment apartment = this.apartmentService.findOne(new Long(103));
        this.apartmentService.checkBuilding(apartment, new Long(100));
    }
}
