import { browser } from "protractor";
// page
import { MeetingItemPage } from "../../../page-objects/meeting-item.po";
import { LoginPage } from "../../../page-objects/login.po";
import { MeetingItemListPage } from "../page-objects/meeting-item-list.po";
import { DamageForQuestionnairePage } from "../page-objects/damage-for-questionnaire.po";
import { QuestionnairePage } from "../../../page-objects/questionnaire.po";


describe('Meeting item page', () => {
  let meetingItemPage: MeetingItemPage;
  let meetingItemListPage: MeetingItemListPage;
  let loginPage: LoginPage;
  let damageForQuestionnairePage: DamageForQuestionnairePage;
  let questionnairePage: QuestionnairePage;

  beforeAll(() => {
    loginPage = new LoginPage();
    meetingItemPage = new MeetingItemPage();
    meetingItemListPage = new MeetingItemListPage();
    damageForQuestionnairePage = new DamageForQuestionnairePage();
    questionnairePage = new QuestionnairePage();

    browser.get('/');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');

    loginPage.setUsername('kaca');
    loginPage.setPassword('kaca');
    loginPage.getSignInButton().click();

    browser.wait(function() {
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/home/notifications';
      });
    }).then(function() {
      browser.get('http://localhost:49152/#/home/meeting-details-items');

      // expect to redirect on page for review list of meeting items
      expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/home/meeting-details-items');
      expect(meetingItemListPage.getMeetingItemListElement().isDisplayed()).toBe(true);
    });
  });

  afterAll(() => {
    browser.executeScript('window.localStorage.clear();');
  });



  it('should show empty form when user click on Add button on page for review ' +
    'meeting item list', () => {
    expect(meetingItemListPage.getAddButton().isDisplayed()).toBe(true);
    meetingItemListPage.getAddButton().click();

    browser.wait(function() {
      // expect to show meeting item modal page when user click on button
      return meetingItemPage.getMeetingItemElement().isDisplayed();
    }).then(function() {
      // expected view of modal dialog - page for creating new meeting item
      // input fields should be empty
      expect(meetingItemPage.getTitleInput().isDisplayed()).toBe(true);
      expect(meetingItemPage.getTitleInput().getAttribute('value')).toEqual('');
      expect(meetingItemPage.getContentInput().isDisplayed()).toBe(true);
      expect(meetingItemPage.getContentInput().getAttribute('value')).toEqual('');
      expect(meetingItemPage.getChooseDamageLink().isDisplayed()).toBe(true);
      expect(meetingItemPage.getQuestionnaireLink().isDisplayed()).toBe(true);
      expect(meetingItemPage.getSaveButton().isDisplayed()).toBe(true);
      expect(meetingItemPage.getSaveButton().isEnabled()).toBe(false);
      expect(meetingItemPage.getResetButton().isDisplayed()).toBe(true);
      expect(meetingItemPage.getResetButton().isEnabled()).toBe(false);
      expect(meetingItemPage.getCancelButton().isDisplayed()).toBe(true);
      expect(meetingItemPage.getCancelButton().isEnabled()).toBe(true);
    });
  });



  it('should enable Reset button when user change something in form and reset all values to initial ' +
    'when user click on that button', () => {
    meetingItemPage.setTitle('Some change');
    meetingItemPage.setContent('Some other change');

    // expect to Reset button is enabled
    expect(meetingItemPage.getResetButton().isEnabled()).toBe(true);

    meetingItemPage.getResetButton().click();

    expect(meetingItemPage.getTitleInput().getAttribute('value')).toEqual('');
    expect(meetingItemPage.getContentInput().getAttribute('value')).toEqual('');
    expect(meetingItemPage.getResetButton().isEnabled()).toBe(false);
  });



  it('should show errors message that both input fields must be filled (for title and content) ' +
    'when user put focus on field and then leave it', () => {
    meetingItemPage.setTitle('');
    meetingItemPage.setContent('');

    // expect to display error message about required values for title and content
    expect(meetingItemPage.getTitleRequiredError().isDisplayed()).toBe(true);
    expect(meetingItemPage.getContentRequiredError().isDisplayed()).toBe(true);
  });



  it('should enable Save button when required part of form is filled (title and content input ' +
    'have non-empty value) and close modal when click on Save button', () => {
    meetingItemPage.setTitle('Some title value');
    meetingItemPage.setContent('Some content value');

    expect(meetingItemPage.getSaveButton().isEnabled()).toBe(true);
    meetingItemPage.getSaveButton().click();
  });



  it('should show filled form when user click on Edit link in menu option on page for ' +
    'review meeting item list', () => {
    //store data from first meeting item in list
    let existingMeetingItem = {'title':'', 'content':''};
    meetingItemListPage.getTitleFromMeetingItemPanel(0).getText().then(value => {
      existingMeetingItem.title = value;
    });
    meetingItemListPage.getContentFromMeetingItemPanel(0).getText().then(value => {
      existingMeetingItem.content = value;
    });

    expect(meetingItemListPage.getMenuFromMeetingItemPanel(0).isDisplayed()).toBe(true);
    meetingItemListPage.getMenuFromMeetingItemPanel(0).click();

    browser.wait(function() {
      return meetingItemListPage.getContentOfMenuFromMeetingItemPanel(0).isDisplayed();
    }).then(function() {
      meetingItemListPage.getEditMenuOptionFromMeetingItemPanel(0,1).click();

      browser.wait(function() {
        // expect to show modal dialog - page for editing meeting item
        return meetingItemPage.getMeetingItemElement().isDisplayed();
      }).then(function() {
        // expected view of modal dialog - input fields should have values same as property values of selected meeting item
        meetingItemPage.getTitleInput().getAttribute('value').then(value => {
          expect(value).toEqual(existingMeetingItem.title);
        });
        meetingItemPage.getContentInput().getAttribute('value').then(value => {
          expect(value).toEqual(existingMeetingItem.content);
        });
        expect(meetingItemPage.getSaveButton().isDisplayed()).toBe(true);
        expect(meetingItemPage.getSaveButton().isEnabled()).toBe(false);
        expect(meetingItemPage.getResetButton().isDisplayed()).toBe(true);
        expect(meetingItemPage.getResetButton().isEnabled()).toBe(false);
        expect(meetingItemPage.getCancelButton().isDisplayed()).toBe(true);
        expect(meetingItemPage.getCancelButton().isEnabled()).toBe(true);
      });
    });
   });



  it('should display description of selected damage that current meeting item is connected with ' +
    'when user click on Select damage link and choose some damage from given list', () => {
    expect(meetingItemPage.getChooseDamageLink().isDisplayed()).toBe(true);
    meetingItemPage.getChooseDamageLink().click();

    browser.wait(function() {
      // expect to show modal dialog - page for choosing damage that new meeting item is connected with
      return damageForQuestionnairePage.getDamageForQuestionnaireElement().isDisplayed();
    }).then(function() {
      damageForQuestionnairePage.getPanelsFromDamageList().count().then(value => {
        expect(value).toBeGreaterThan(0);

        // store description from selected damage so that we can compare description after
        // rendering that data in section about selected damage in meeting item page
        let selectedDamage = {'description':''};
        damageForQuestionnairePage.getDamageDescriptionFromDamagePanel(0).getText().then(value => {
          selectedDamage.description = value;
        });
        expect(damageForQuestionnairePage.getMenuFromDamagePanel(0).isDisplayed()).toBe(true);
        damageForQuestionnairePage.getMenuFromDamagePanel(0).click();

        browser.wait(function() {
          return damageForQuestionnairePage.getChooseOptionFromDamagePanel(0).isDisplayed();
        }).then(function() {
          damageForQuestionnairePage.getChooseOptionFromDamagePanel(0).click();

          meetingItemPage = new MeetingItemPage();

          browser.wait(function() {
            return meetingItemPage.getMeetingItemElement().isDisplayed();
          }).then(function() {
            // expect to see damage description and Remove connection to damage link in damage section on meeting item page
            // and not to see Choose damage link
            expect(meetingItemPage.getSelectedDamagePanel().isDisplayed()).toBe(true);
            expect(meetingItemPage.getDescriptionFromSelectedDamagePanel().isDisplayed()).toBe(true);
            expect(meetingItemPage.getRemoveDamageLinkFromSelectedDamagePanel().isDisplayed()).toBe(true);
            meetingItemPage.getDescriptionFromSelectedDamagePanel().getText().then(value => {
              expect(value).toEqual(selectedDamage.description);
            });
          });
        });
      });
    });
  });



  it('should remove section with description of selected damage when user click on Remove ' +
    'connection link in that section', () => {
    meetingItemPage.getRemoveDamageLinkFromSelectedDamagePanel().click();

    // expect to see only link for choosing damage, and not damage section from previous test
    expect(meetingItemPage.getChooseDamageLink().isDisplayed()).toBe(true);
  });



  it('should show modal dialog for create/update questionnaire when user click on ' +
    'Questionnaire link', () => {
    expect(meetingItemPage.getQuestionnaireLink().isDisplayed()).toBe(true);

    meetingItemPage.getQuestionnaireLink().click();

    browser.wait(function () {
      // expect to show modal dialog for create new or update existing questionnaire - questionnaire page
      return questionnairePage.getQuestionnaireElement().isDisplayed();
    });
  });
});
