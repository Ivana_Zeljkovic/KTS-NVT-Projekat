import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
// guard
import { AdminGuard } from "./guards/admin.guard";
import { AnonymusGuard } from "./guards/anonymus.guard";
import { AuthGuard } from "./guards/auth.guard";
import { BusinessGuard } from "./guards/business.guard";
import { InstitutionGuard } from "./guards/institution.guard";
import { TenantOrBusinessGuard } from "./guards/tenant-or-business.guard";
import { AdminOrFirmGuard } from './guards/admin-or-firm.guard';
import { CouncilMemberGuard } from "./guards/council-member.guard";
import { TenantOwnerGuard } from "./guards/tenant-owner.guard";
import { TenantOwnerOrCouncilMemberGuard } from "./guards/tenant-owner-or-council-member.guard";
// service
import { AuthService } from "./services/auth.service";
import { AdminService } from "./services/admin.service";
import { ApartmentService } from "./services/apartment.service";
import { BuildingService } from "./services/building.service";
import { BusinessService } from "./services/business.service";
import { CouncilPresidentService } from "./services/council-president.service";
import { DamageTypeResponsibilityService } from "./services/damage-type-responsibility.service";
import { FirmService } from "./services/firm.service";
import { InstitutionService } from "./services/institution.service";
import { JwtService } from "./services/jwt.service";
import { MeetingService } from "./services/meeting.service";
import { MeetingItemService } from "./services/meeting-item.service";
import { NotificationService } from "./services/notification.service";
import { OfferService } from "./services/offer.service";
import { QuestionnaireService } from "./services/questionnaire.service";
import { RecordService } from "./services/record.service";
import { TenantService } from "./services/tenant.service";
import { LocationService } from "./services/location.service";
import { DamagesCategoryService } from "./services/damages-category.service";
import { DamageService } from "./services/damage.service";
import { CommentService } from "./services/comment.service";


@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    AdminGuard,
    AnonymusGuard,
    AuthGuard,
    BusinessGuard,
    AdminOrFirmGuard,
    InstitutionGuard,
    TenantOrBusinessGuard,
    CouncilMemberGuard,
    TenantOwnerGuard,
    TenantOwnerOrCouncilMemberGuard,
    AdminService,
    ApartmentService,
    AuthService,
    BuildingService,
    BusinessService,
    CouncilPresidentService,
    DamageTypeResponsibilityService,
    FirmService,
    InstitutionService,
    JwtService,
    MeetingService,
    MeetingItemService,
    NotificationService,
    OfferService,
    QuestionnaireService,
    RecordService,
    TenantService,
    LocationService,
    DamagesCategoryService,
    DamageService,
    CommentService
  ]
})
export class CoreModule {
  constructor (@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only');
    }
  }
}
