package rs.ac.uns.ftn.ktsnvt.service.integration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Building;
import rs.ac.uns.ftn.ktsnvt.model.entity.MeetingItem;
import rs.ac.uns.ftn.ktsnvt.service.BuildingService;
import rs.ac.uns.ftn.ktsnvt.service.MeetingItemService;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;


/**
 * Created by Nikola Garabandic on 02/12/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class MeetingItemServiceIntegrationTest {

    @Autowired
    private MeetingItemService meetingItemService;

    @Autowired
    private BuildingService buildingService;


    @Test
    @Transactional
    public void testCheckMeetingItemInBuilding(){
        /*
         * Test proverava ispravnost rada metode checkMeetingItemInBuilding servisa MeetingItemService
         * (provera da li tacka dnevnog reda pripada zgradi)
         * u slucaju kada tacka dnevnog reda sa zadatim ID-em pripada zgradi sa zadatim ID-em
         * */

        // tacka dnevnog reda sa ID-em 102 pripada zgradi ciji je ID 100
        MeetingItem meetingItem = this.meetingItemService.findOne(102L);
        Building building = this.buildingService.findOne(100L);

        List<MeetingItem> list = this.meetingItemService.findAllOnBillboard(building.getBillboard().getId());
        this.meetingItemService.checkMeetingItemInBuilding(meetingItem, list);
    }

    @Test(expected = BadRequestException.class)
    @Transactional
    public void testCheckMeetingItemInBuildingNonexistent(){
        /*
         * Test proverava ispravnost rada metode CheckMeetingItemInBuilding servisa MeetingItemService
         * (provera da li tacka dnevnog reda pripada zgradi)
         * u slucaju kada tacka dnevnog reda sa zadatim ID-em ne pripada zgradi sa zadatim ID-em - ocekivano BadRequestException
         * */

        // tacka dnevnog reda sa ID-em 103 ne pripada zgradi ciji je ID 100
        MeetingItem meetingItem = this.meetingItemService.findOne(103L);
        Building building = this.buildingService.findOne(100L);

        List<MeetingItem> list = this.meetingItemService.findAllOnBillboard(building.getBillboard().getId());
        this.meetingItemService.checkMeetingItemInBuilding(meetingItem, list);
    }

    @Test
    public void testCheckMeetingIsNull(){
        /*
         * Test proverava ispravnost rada metode CheckMeetingItemIsNull servisa MeetingItemService
         * (proverava da li je tacka dnevnog reda dodeljena vec nekom sastanku)
         * u slucaju kada zadata tacka dnevnog reda jos uvek nije dodeljena nijednom sastanku
         * */

        // tacka dnevnog reda sa ID-em 102 nije dodeljena nijednom sastanku
        MeetingItem meetingItem = meetingItemService.findOne(102L);

        this.meetingItemService.checkMeetingIsNull(meetingItem);
    }

    @Test(expected = BadRequestException.class)
    public void testCheckMeetingItemIsNotNull(){
        /*
         * Test proverava ispravnost rada metode CheckMeetingItemIsNull servisa MeetingItemService
         * (proverava da li je tacka dnevnog reda dodeljena vec nekom sastanku)
         * u slucaju kada je zadata tacka dnevnog reda vec dodeljena nekom sastanku - ocekivano BadRequestException
         * */

        // tacka dnevnog reda sa ID-em 100 je dodeljena sastanku sa ID-em 100
        MeetingItem meetingItem = meetingItemService.findOne(100L);

        this.meetingItemService.checkMeetingIsNull(meetingItem);
    }

    @Test
    @Transactional
    public void testFindOneByIdAndBillboard() {
        /*
         * Test proverava ispravnost rada metode findOneByIdAndBillboard servisa MeetingItemService
         * (pronalazenje konkretne tacke dnevnog reda na osnovu zadatog ID-a i ID-a bilborda zgrade kom tacka dnevnog reda pripada,
         * pri cemu ta tacka dnevnog reda nije obradjena jos ni na jednom sastanku skupstine)
         * u slucaju kada postoji tacka dnevnog reda sa zadatim ID-em na bilbordu sa zadatim ID-em i pri tome jos uvek nije
         * obradjena ni na jednom sastanku
         * */

        /*
        oglasnoj tabli zgrade sa ID-em = 100 (oglasna tabla sa ID-em = 100) pripada tacka dnevnog reda:
            ID = 102,
            ID kreatora = 102
            content = First meeting item
            meeting_id = null
        */
        this.meetingItemService.findOneByIdAndBillboard(new Long(102), new Long(100));
    }

    @Test(expected = BadRequestException.class)
    @Transactional
    public void testFindOneByIdAndBillboardNonexistent() {
        /*
         * Test proverava ispravnost rada metode findOneByIdAndBillboard servisa MeetingItemService
         * (pronalazenje konkretne tacke dnevnog reda na osnovu zadatog ID-a i ID-a bilborda zgrade kom tacka dnevnog reda pripada,
         * pri cemu ta tacka dnevnog reda nije obradjena jos ni na jednom sastanku skupstine)
         * u slucaju kada ne postoji tacka dnevnog reda sa zadatim ID-em na bilbordu sa zadatim ID-em - ocekivano BadRequestException
         * */

        // oglasnoj tabli zgrade sa ID-em = 100 (oglasna tabla sa ID-em = 100) ne pripada tacka dnevnog reda sa ID-em 103:
        this.meetingItemService.findOneByIdAndBillboard(new Long(103), new Long(100));
    }

    @Test(expected = BadRequestException.class)
    @Transactional
    public void testFindOneByIdAndBillboardAlreadyProcessed() {
        /*
         * Test proverava ispravnost rada metode findOneByIdAndBillboard servisa MeetingItemService
         * (pronalazenje konkretne tacke dnevnog reda na osnovu zadatog ID-a i ID-a bilborda zgrade kom tacka dnevnog reda pripada,
         * pri cemu ta tacka dnevnog reda nije obradjena jos ni na jednom sastanku skupstine)
         * u slucaju kada postoji tacka dnevnog reda sa zadatim ID-em na bilbordu sa zadatim ID-em ali je vec obradjena na nekom sastanku
         * - ocekivano BadRequestException
         * */

        /*
        oglasnoj tabli zgrade sa ID-em = 100 (oglasna tabla sa ID-em = 100) pripada tacka dnevnog reda:
            ID = 100,
            ID kreatora = 101
            content = First meeting item,
            meeting_id = 100
        */
        this.meetingItemService.findOneByIdAndBillboard(new Long(100), new Long(100));
    }

    @Test
    @Transactional
    public void testFindOne() {
        /*
         * Test proverava ispravnost metode findOne servisa MeetingItemService
         * (dobavljanje objekta tacke dnevnog reda na osnovu zadatog ID-a)
         * u slucaju kada postoji tacka dnevnog reda sa zadatim ID-em
         * */

        /*
        u bazi postoji tacka dnevnog reda sa ID-em = 100 i podacima:
            ID = 100,
            ID kreatora = 101,
            content = First meeting item
         */
        MeetingItem meetingItem = this.meetingItemService.findOne(new Long(100));

        assertNotNull(meetingItem);
        assertEquals(Long.valueOf(100), meetingItem.getId());
        assertEquals(Long.valueOf(101), meetingItem.getCreator().getId());
        assertEquals("First meeting item content", meetingItem.getContent());
    }

    @Test(expected = NotFoundException.class)
    @Transactional
    public void testFindOneNonexistent() {
        /*
         * Test proverava ispravnost metode findOne servisa MeetingItemService
         * (dobavljanje objekta tacke dnevnog reda na osnovu zadatog ID-a)
         * u slucaju kada ne postoji tacka dnevnog reda sa zadatim ID-em - ocekivano NotFoundException
         * */

        // u bazi ne postoji tacka dnevnog reda sa ID-em = 200
        MeetingItem meetingItem = this.meetingItemService.findOne(new Long(200));

        assertNull(meetingItem);
    }

    @Test
    public void testCheckMeetingItemQuestionnaire() {
        /*
         * Test proverava ispravnost metode checkMeetingItemQuestionnaire servisa MeetingItemService
         * (provera da li prosledjena tacka dnevnog reda sadrzi anketu ciji je id jednak prosledjenom ID-u)
         * u slucaju kada su ID ankete prosledjene tacke dnevnog reda i prosledjeni ID jednaki
         * */

        // u bazi postoji anketa sa ID-em = 101 koja pripada tacki dnevnog reda sa ID-em = 104
        MeetingItem meetingItem = this.meetingItemService.findOne(Long.valueOf(104));

        this.meetingItemService.checkMeetingItemQuestionnaire(meetingItem, 101L);
    }

    @Test(expected = BadRequestException.class)
    public void testCheckMeetingItemQuestionnaireInvalid() {
        /*
         * Test proverava ispravnost metode checkMeetingItemQuestionnaire servisa MeetingItemService
         * (provera da li prosledjena tacka dnevnog reda sadrzi anketu ciji je id jednak prosledjenom ID-u)
         * u slucaju kada ID ankete prosledjene tacke dnevnog reda i prosledjeni ID nisu jednaki - ocekivano BadRequestException
         * */

        // u bazi postoji anketa sa ID-em = 100 koja pripada tacki dnevnog reda sa ID-em = 100 (ne sa ID-em = 104)
        MeetingItem meetingItem = this.meetingItemService.findOne(Long.valueOf(104));

        this.meetingItemService.checkMeetingItemQuestionnaire(meetingItem, 100L);
    }
}
