import { ChangeDetectorRef, Component, Input, OnInit, ViewChild } from '@angular/core';
import { ToasterConfig, ToasterService } from "angular5-toaster/dist";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { AgmMap } from "@agm/core";
//service
import { AuthService } from "../../core/services/auth.service";
import { JwtService } from "../../core/services/jwt.service";
import { BusinessService } from "../../core/services/business.service";
import { LocationService } from "../../core/services/location.service";
//validator
import { PasswordValidator } from "../../shared/validators/password.validator";
import { SpaceValidator } from "../../shared/validators/space.validator";
// model
import { BusinessProfile } from "../../shared/models/business-profile";
import { BusinessType } from "../../registration/business-registration/models/business-type";
import { BusinessUpdate } from "../../shared/models/business-update";
import { AddressCreate } from "../../shared/models/address-create";
import { CityCreate } from "../../shared/models/city-create";
import { Login } from "../../shared/models/login";
// error
import { AppError } from "../../shared/errors/app-error";
import { BadRequestError } from "../../shared/errors/bad-request-error";
import { NotFoundError } from "../../shared/errors/not-found-error";
import { ForbiddenError } from "../../shared/errors/forbidden-error";
// component
import { BusinessProfileAccountsModalComponent } from "../../shared/components/business-profile-accounts-modal/business-profile-accounts-modal.component";
import {BusinessTypePipe} from "../../shared/pipes/business-type.pipe";


@Component({
  selector: 'app-business-profile',
  templateUrl: './business-profile.component.html',
  styleUrls: ['./business-profile.component.css']
})
export class BusinessProfileComponent implements OnInit {
  businessData: BusinessProfile;
  form: FormGroup;
  initialValues;
  disableSaveOrReset: boolean;
  longitude: number;
  latitude: number;
  zoom: number;
  businessTypes: string[];
  @ViewChild(AgmMap) map: AgmMap;
  indexType: number;
  bsModalRef: BsModalRef;
  buttonClassName: string;
  initialAccountValues: Login[];
  @Input() toasterConfig: ToasterConfig;
  businessTypePipe: BusinessTypePipe;


  constructor(private fb: FormBuilder, private passwordValidator: PasswordValidator,
              private jwtService: JwtService, private businessService: BusinessService,
              private authService: AuthService, private locationService: LocationService,
              private toasterService: ToasterService, private modalService: BsModalService,
              private changeDetectorRef: ChangeDetectorRef) {
    this.zoom = 14;
    // default coordinates if there are no coordinates for given address
    this.latitude = 45.2461;
    this.longitude = 19.8517;
    this.indexType = 0;
    this.businessTypePipe = new BusinessTypePipe();
  }

  ngOnInit() {
    this.authService.currentUser()
      .subscribe(data => {
        this.businessData = (data as BusinessProfile);
        this.initialAccountValues = JSON.parse(JSON.stringify(this.businessData.usernamesList));

        this.locationService.getCoordinates(`${this.businessData.address.street} ${this.businessData.address.city.name}`)
          .subscribe(response => {
            if (response.status == 'OK') {
              this.latitude = response.results[0].geometry.location.lat;
              this.longitude = response.results[0].geometry.location.lng;
              this.map.triggerResize();
              this.buttonClassName = this.businessData.deactivated ? "btn btn-success account" : "btn btn-danger account";

              this.changeDetectorRef.detectChanges();

            } else if (response.status == 'ZERO_RESULTS') {
              this.toasterService.pop('error', 'Error', 'Address couldn\'t be found!');
            } else {
              this.toasterService.pop('error', 'Error', 'Error in detecting coordinates of given address!');
            }
          }, (error: AppError) => {
            this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
            throw error;
          });

        let everything = Object.keys(BusinessType);
        this.businessTypes = everything.splice(everything.length / 2);

        this.form = this.fb.group({
          name: [this.businessData.name, [
            Validators.required,
            Validators.minLength(2)
          ]],
          description: [this.businessData.description, [
            Validators.required,
            Validators.minLength(2)
          ]],
          email: [this.businessData.email, [
            Validators.required,
            Validators.email
          ]],
          phoneNumber: [this.businessData.phoneNumber, [
            Validators.required,
            SpaceValidator.cannotContainSpace,
            Validators.pattern('[0-9]{6,10}')
          ]],
          username: this.jwtService.getUsernameFromToken(),
          currentPassword: '',
          passwords: this.fb.group({
            password: '',
            confirmPassword: '',
          }),
          businessType : [this.businessTypePipe.transform(this.businessData.businessType)],
          street: [this.businessData.address.street, [
            Validators.required,
            Validators.minLength(2),
          ]],
          number: [this.businessData.address.number, [
            Validators.required,
          ]],
          city: [this.businessData.address.city.name, [
            Validators.required,
          ]],
          postalCode: [this.businessData.address.city.postalNumber, [
            Validators.required,
            Validators.pattern('[0-9]{5}')
          ]]
        });

        this.initialValues = this.form.value;
        this.disableSaveOrReset = true;

        this.form.valueChanges.subscribe(newValues => {
          // disable the save and reset button if there are no changes
          this.disableSaveOrReset = !this.changeExist(newValues);
        });

        this.password.valueChanges.subscribe((newValue) => {
          // if user write some value in new password field, fields current password and confirm password
          // become required too
          // else, all validators from all password fields are removed
          (newValue.trim() === '') ? this.removeValidators() : this.addValidators();
        });
      }, (error: AppError) => {
        this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
        throw error;
      });
  }

  get name() {
    return this.form.get('name');
  }

  get description() {
    return this.form.get('description');
  }

  get email() {
    return this.form.get('email');
  }

  get phoneNumber() {
    return this.form.get('phoneNumber');
  }

  get username() {
    return this.form.get('username');
  }

  get currentPassword() {
    return this.form.get('currentPassword');
  }

  get passwords() {
    return this.form.get('passwords');
  }

  get password() {
    return this.form.get('passwords').get('password');
  }

  get confirmPassword() {
    return this.form.get('passwords').get('confirmPassword');
  }

  get street() {
    return this.form.get('street');
  }

  get number() {
    return this.form.get('number');
  }

  get city() {
    return this.form.get('city');
  }

  get postalCode() {
    return this.form.get('postalCode');
  }

  displayAddAccount() {
    this.bsModalRef = this.modalService.show(BusinessProfileAccountsModalComponent);
    this.bsModalRef.content.accountsList = this.businessData.usernamesList;
    this.bsModalRef.content.thisAccountModalRef = this.bsModalRef;
  }

  accountListsAreSame(): boolean {
    return (JSON.stringify(this.initialAccountValues) === JSON.stringify(this.businessData.usernamesList));
  }

  reset() {
    this.form.setValue(this.initialValues);
  }

  update() {
    if ((this.currentPassword.value.trim() !== '' || this.confirmPassword.value.trim() !== '') && this.password.value === '')
      this.toasterService.pop('warning', 'Warning', 'New password value is missing!');
    else {
      let currentPasswordVal = null;
      let newPasswordVal = null;
      if (this.password.value !== '') {
        currentPasswordVal = this.currentPassword.value;
        newPasswordVal = this.password.value;
      }

      let businessUpdate = null;
      if (this.currentPassword.value == '' && this.password.value == '' && this.confirmPassword.value == '') {
        businessUpdate = new BusinessUpdate(this.name.value, this.description.value, this.phoneNumber.value, this.email.value,
          new AddressCreate(this.street.value, this.number.value, new CityCreate(this.city.value, this.postalCode.value)));
      }
      else {
        businessUpdate = new BusinessUpdate(this.name.value, this.description.value, this.phoneNumber.value, this.email.value,
          new AddressCreate(this.street.value, this.number.value, new CityCreate(this.city.value, this.postalCode.value)),
          this.currentPassword.value, this.password.value, this.confirmPassword.value);
      }

      this.businessService.update(this.businessData.id, businessUpdate, 'businesses').subscribe(() => {
        this.form.get('currentPassword').setValue('');
        this.form.get('passwords').get('password').setValue('');
        this.form.get('passwords').get('confirmPassword').setValue('');
        this.initialValues = this.form.value;
        this.disableSaveOrReset = true;

        let accounts = {loginAccounts: this.businessData.usernamesList};

        this.businessService.updatePart(accounts, 'businesses', this.businessData.id, 'edit_accounts')
          .subscribe(() => {
          this.toasterService.pop('success', '', 'Update of business data successfully done!');
        }, (error: AppError) => {
            if(error instanceof BadRequestError)
              this.toasterService.pop('error', 'Error', 'Given data have bad format!');
            else if(error instanceof ForbiddenError)
              this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
            else if(error instanceof NotFoundError)
              this.toasterService.pop('error', 'Error', 'Wrong ID of business!');
            else {
              this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
              throw error;
            }
          });
      }, (error: AppError) => {
        if (error instanceof BadRequestError)
          this.toasterService.pop('error', 'Error', 'Given data have bad format/Current password is invalid!');
        else if (error instanceof NotFoundError)
          this.toasterService.pop('error', 'Error', 'Wrong ID of business!');
        else {
          this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
          throw error;
        }
      });
    }
  }

  private activateOrDeactivate() {
    if (!this.businessData.deactivated)
      this.businessService.updatePart({}, 'businesses', this.businessData.id, 'deactivate')
        .subscribe(data => {
          this.buttonClassName = "btn btn-success account";
          this.businessData.deactivated = true;
        }, (error: AppError) => {
          if(error instanceof BadRequestError)
            this.toasterService.pop('error', 'Error',
              'You are responsible for some damages/damage types or you should to repair some damages!');
          else if(error instanceof ForbiddenError)
            this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
          else if(error instanceof NotFoundError)
            this.toasterService.pop('error', 'Error', 'Wrong ID of business!');
          else {
            this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
            throw error;
          }
        });
    else
      this.businessService.updatePart({}, 'businesses', this.businessData.id, 'activate')
        .subscribe(data => {
          this.buttonClassName = "btn btn-danger account";
          this.businessData.deactivated = false;
        }, (error: AppError) => {
          if(error instanceof ForbiddenError)
            this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
          else if(error instanceof NotFoundError)
            this.toasterService.pop('error', 'Error', 'Wrong ID of business!');
          else {
            this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
            throw error;
          }
        });
  }

  private changeExist(newValues): boolean {
    return !(JSON.stringify(newValues) === JSON.stringify(this.initialValues) &&
      JSON.stringify(this.initialAccountValues) === JSON.stringify(this.businessData.usernamesList));
  }

  private addValidators() {
    if (this.password.value.length === 1) {
      this.password.setValidators([Validators.required, Validators.minLength(5)]);
      this.currentPassword.setValidators([Validators.required]);
      this.confirmPassword.setValidators([Validators.required]);
      this.passwords.setValidators([this.passwordValidator.matchPasswords.bind(this.passwordValidator)]);
      this.updateAndValidity();
    }
  }

  private removeValidators() {
    this.password.clearValidators();
    this.currentPassword.clearValidators();
    this.confirmPassword.clearValidators();
    this.passwords.clearValidators();
    this.updateAndValidity();
  }

  private updateAndValidity() {
    this.password.updateValueAndValidity({emitEvent: false});
    this.currentPassword.updateValueAndValidity();
    this.confirmPassword.updateValueAndValidity();
  }
}
