import {by, element, ElementArrayFinder, ElementFinder} from "protractor";

export class RecordPage {
  // get methods
  getRecordElement(): ElementFinder {
    return element(by.tagName('app-record'));
  }

  getTitle(): ElementFinder {
    return element(by.css('div.page-title'));
  }

  getDateOfMeeting(): ElementFinder {
    return element(by.css('div.row .panel-heading'));
  }

  getRecordContent(): ElementFinder {
    return element(by.css('div.row .panel-body'));
  }

  getMeetingItems(): ElementArrayFinder {
    return element.all(by.css('#meetingItems .panel'));
  }

  getMeetingItemTitle(index: number): ElementFinder {
    return element(by.css(`#meetingItems .panel:nth-child(${index+1}) .meeting-item-in-meeting-title`));
  }

  getMenuFromMeetingItem(index: number): ElementFinder {
    return element(by.css(`#meetingItems .panel:nth-child(${index+1}) .meeting-item-in-meeting-menu > span`));
  }

  getViewQuestionnaireOptionFromMenu(index: number): ElementFinder {
    return element(by.css(`#meetingItems .panel:nth-child(${index+1}) .meeting-item-in-meeting-menu a`));
  }

  getMeetingItemCreator(index: number): ElementFinder {
    return element(by.css(`#meetingItems .panel:nth-child(${index+1}) .meeting-item-in-meeting-metadata > span:nth-child(2)`));
  }

  getMeetingItemDate(index: number): ElementFinder {
    return element(by.css(`#meetingItems .panel:nth-child(${index+1}) .meeting-item-in-meeting-metadata > span:nth-child(4)`));
  }

  getMeetingItemContent(index: number): ElementFinder {
    return element(by.css(`#meetingItems .panel:nth-child(${index+1}) .panel-body`));
  }

  getNoMeetingItemsMessage(): ElementFinder {
    return element(by.css('.no-meeting-items'));
  }
}
