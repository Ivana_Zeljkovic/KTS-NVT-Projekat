import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
// service
import { DataService } from "./data.service";
// models
import { Apartment } from "../../shared/models/apartment";
import { PageAndSort } from "../../shared/models/page-and-sort";


@Injectable()
export class ApartmentService extends DataService<Apartment>{
  constructor(http: HttpClient) {
    super('', http);
  }

  getFreeInBuilding(pageAndSort: PageAndSort, street: string, number: string, cityName: string, postalNumber: string): Observable<Array<Apartment>> {
    let params: HttpParams = new HttpParams()
      .append('street', street)
      .append('number', number)
      .append('city_name', cityName)
      .append('postal_number', postalNumber)
      .append('pageNumber', pageAndSort.pageNumber)
      .append('pageSize', pageAndSort.pageSize)
      .append('sortDirection', pageAndSort.sortDirection)
      .append('sortProperty', pageAndSort.sortProperties[0])
      .append((pageAndSort.sortProperties.length > 1) ? 'sortPropertySecond' : '',
        (pageAndSort.sortProperties.length > 1) ? pageAndSort.sortProperties[1] : '');

    return this.http.get<Array<Apartment>>(`${this.urlBase}/apartments/free_apartments_building`, {params})
      .catch(this.handleErrors);
  }

  getFreeInRange(pageAndSort: PageAndSort, min: string, max: string, buildingId: string): Observable<Array<Apartment>> {
    let params: HttpParams = new HttpParams()
      .append('min', min)
      .append('max', max)
      .append("buildingId", buildingId)
      .append('pageNumber', pageAndSort.pageNumber)
      .append('pageSize', pageAndSort.pageSize)
      .append('sortDirection', pageAndSort.sortDirection)
      .append('sortProperty', pageAndSort.sortProperties[0])
      .append((pageAndSort.sortProperties.length > 1) ? 'sortPropertySecond' : '',
        (pageAndSort.sortProperties.length > 1) ? pageAndSort.sortProperties[1] : '');

    return this.http.get<Array<Apartment>>(`${this.urlBase}/apartments/free_apartments_size`, {params})
      .catch(this.handleErrors);
  }
}
