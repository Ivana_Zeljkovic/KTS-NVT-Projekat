import { TestBed, inject } from '@angular/core/testing';

import { DamageTypeResponsibilityService } from './damage-type-responsibility.service';

describe('DamageTypeResponsibilityService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DamageTypeResponsibilityService]
    });
  });

  it('should be created', inject([DamageTypeResponsibilityService], (service: DamageTypeResponsibilityService) => {
    expect(service).toBeTruthy();
  }));
});
