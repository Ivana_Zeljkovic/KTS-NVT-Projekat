import { TestBed, inject } from '@angular/core/testing';

import { OfferService } from './offer.service';
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {Offer} from "../../shared/models/offer";

describe('OfferService', () => {

  let offerService: OfferService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule],
      providers: [OfferService]
    });
    offerService = TestBed.get(OfferService);
    httpMock = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', inject([OfferService], (service: OfferService) => {
    expect(service).toBeTruthy();
  }));

  it('should get offer by building and damage id', () => {
    const response = {
      'id' : 1,
      'businessId' : 1,
      'price' : 1000
    }
    offerService.getOfferByBuildingAndDamageId(1, 1)
      .subscribe((offer: Offer) => {
        expect(offer).toEqual(response);
      });
    const httpRequest = httpMock.expectOne(
      '/api/businesses/1/damages/1/accepted_offer');
    httpRequest.flush(response);
  });
});
