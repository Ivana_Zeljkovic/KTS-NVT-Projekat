package rs.ac.uns.ftn.ktsnvt.service.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.ForbiddenException;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Questionnaire;
import rs.ac.uns.ftn.ktsnvt.service.QuestionnaireService;

import static org.junit.Assert.*;

/**
 * Created by Ivana Zeljkovic on 01/12/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class QuestionnaireServiceIntegrationTest {

    @Autowired
    private QuestionnaireService questionnaireService;


    @Test
    @Transactional
    public void testFindOne() {
        /*
         * Test proverava ispravnost metode findOne servisa QuestionnaireService
         * (dobavljanje objekta ankete na osnovu zadatog ID-a)
         * u slucaju kada postoji anketa sa zadatim ID-em
         * */

        /*
        u bazi postoji anketa sa ID-em = 101 i podacima:
            glasovi = prazna lista,
            pitanja = lista sa 2 pitanja
         */
        Questionnaire questionnaire = this.questionnaireService.findOne(new Long(101));

        assertNotNull(questionnaire);
        assertEquals(Long.valueOf(101), questionnaire.getId());
        assertEquals(0, questionnaire.getVotes().size());
        assertEquals(2, questionnaire.getQuestions().size());
    }

    @Test(expected = NotFoundException.class)
    public void testFindOneNonExistent() {
        /*
         * Test proverava ispravnost metode findOne servisa QuestionnaireService
         * (dobavljanje objekta ankete na osnovu zadatog ID-a)
         * u slucaju kada postoji anketa sa zadatim ID-em - ocekivano NotFoundException
         * */

        // u bazi ne postoji anketa sa ID-em = 104
        Questionnaire questionnaire = this.questionnaireService.findOne(new Long(104));
    }

    @Test
    public void testCheckQuestionnaireExpiredAlready() {
        /*
         * Test proverava ispravnost metode checkQuestionnaireExpired servisa QuestionnaireService
         * (provera da li je vreme glasanja na anketa sa zadatim ID-em isteklo)
         * u slucaju kada je vreme glasanja na anketu sa zadatim ID-em isteklo
         * */

        Questionnaire questionnaire = this.questionnaireService.findOne(new Long(100));

        // vreme glasanja za anketu sa ID-em = 100 je isteklo, pa metoda servisa nece vratiti nikakvu gresku
        this.questionnaireService.checkQuestionnaireExpired(questionnaire);
    }

    @Test(expected = BadRequestException.class)
    public void testCheckQuestionnaireExpired() {
        /*
         * Test proverava ispravnost metode checkQuestionnaireExpired servisa QuestionnaireService
         * (provera da li je vreme glasanja na anketa sa zadatim ID-em isteklo)
         * u slucaju kada vreme glasanja na anketu sa zadatim ID-em nije isteklo - ocekivano BadRequestException
         * */

        Questionnaire questionnaire = this.questionnaireService.findOne(new Long(101));

        // vreme glasanja za anketu sa ID-em = 101 nije isteklo, pa metoda servisa baca izuzetak
        this.questionnaireService.checkQuestionnaireExpired(questionnaire);
    }

    @Test
    @Transactional
    public void testIsVoteEnabled() {
        /*
         * Test proverava ispravnost metode isVoteEnabled servisa QuestionnaireService
         * (provera da li je dozvoljeno glasanje na prosledjenu anketu)
         * u slucaju kada je glasanje dozvoljeno
         * */

        /*
        u bazi postoji anketa sa ID-em = 100 koja pripada tacki dnevnog reda sa ID-em = 100 koja je povezana sa sastankom sa ID-em = 100,
        te je glasanje na ovu anketu moguce
        */

        Questionnaire questionnaire = this.questionnaireService.findOne(101L);
        this.questionnaireService.isVoteEnabled(questionnaire);
    }

    @Test(expected = ForbiddenException.class)
    @Transactional
    public void testIsVoteEnabledInvalid() {
        /*
         * Test proverava ispravnost metode isVoteEnabled servisa QuestionnaireService
         * (provera da li je dozvoljeno glasanje na prosledjenu anketu)
         * u slucaju kada glasanje nije dozvoljeno - ocekivano ForbiddenException
         * */

        /*
        u bazi postoji anketa sa ID-em = 102 koja pripada tacki dnevnog reda sa ID-em = 102 koja nije povezana (preuzeta) ni sa jednim
        sastankom, te glasanje na ovu anketu jos uvek nije moguce
        */

        Questionnaire questionnaire = this.questionnaireService.findOne(102L);
        this.questionnaireService.isVoteEnabled(questionnaire);
    }

    @Test
    public void testCheckBuildingForQuestionnaire() {
        /*
         * Test proverava ispravnost metode checkBuildingForQuestionnaire servisa QuestionnaireService
         * (provera da li anketa sa zadatim ID-em pripada zgradi sa zadatim ID-em njenog bilborda)
         * u slucaju kada anketa pripada
         * */

        this.questionnaireService.checkBuildingForQuestionnaire(100L, 100L);
    }

    @Test(expected = BadRequestException.class)
    public void testCheckBuildingForQuestionnaireInvalid() {
        /*
         * Test proverava ispravnost metode checkBuildingForQuestionnaire servisa QuestionnaireService
         * (provera da li anketa sa zadatim ID-em pripada zgradi sa zadatim ID-em njenog bilborda)
         * u slucaju kada anketa ne pripada zadatoj zgradi
         * */

        this.questionnaireService.checkBuildingForQuestionnaire(200L, 100L);
    }
}
