import { browser } from "protractor";
// page
import { DamageListPage } from "../../page-objects/damage-list.po";
import { DamageAddPage } from "../page-objects/damage-add.po";
import { DamageEditPage } from "../page-objects/damage-edit.po";
import { DamageDetailsPage } from "../page-objects/damage-details.po";
import { DamageTypesModalPage } from "../page-objects/damage-types-modal.po";
import { LoginPage } from "../../page-objects/login.po";
import { NavbarPage } from "../../page-objects/navbar.po";
import { ConfirmDeletePage } from "../../page-objects/confirm-delete.po";
import { DelegateResponsibilityToPersonPage } from "../../page-objects/delegate-responsibility-to-person.po";
// helper
import { DateConverter } from "../../util/date-converter";


describe('Damage list for tenant', () => {
  let damageListPage: DamageListPage;
  let damageAddPage: DamageAddPage;
  let damageEditPage: DamageEditPage;
  let damageDetailsPage: DamageDetailsPage;
  let damageTypesModalPage: DamageTypesModalPage;
  let loginPage: LoginPage;
  let navbarPage: NavbarPage;
  let confirmDeletePage: ConfirmDeletePage;
  let delegateResponsibilityToPersonPage: DelegateResponsibilityToPersonPage;
  let newDamage: any;
  let updatedDamage: any;


  beforeAll(() => {
    loginPage = new LoginPage();
    navbarPage = new NavbarPage();
    damageListPage = new DamageListPage();
    damageAddPage = new DamageAddPage();
    damageEditPage = new DamageEditPage();
    damageDetailsPage = new DamageDetailsPage();
    damageTypesModalPage = new DamageTypesModalPage();
    confirmDeletePage = new ConfirmDeletePage();
    delegateResponsibilityToPersonPage = new DelegateResponsibilityToPersonPage();
    newDamage = {'description':'Description of new damage', 'urgent':'', 'creator':'', 'date':'', 'type':''};
    updatedDamage = {'description': 'Updated description'};

    browser.get('/');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');

    loginPage.setUsername('kaca');
    loginPage.setPassword('kaca');
    loginPage.getSignInButton().click();

    browser.wait(function() {
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/home/notifications';
      });
    }).then(function() {
      expect(navbarPage.getNavbarElement().isDisplayed()).toBe(true);
    });
  });

  beforeEach(() => {
    expect(navbarPage.getDamageDropdownLink().isDisplayed()).toBe(true);
    navbarPage.getDamageDropdownLink().click();

    browser.wait(function() {
      return navbarPage.getContentOfDamageDropdownLink().isDisplayed();
    });
  });

  afterAll(() => {
    browser.executeScript('window.localStorage.clear();');
  });



  it('should show list of damages that current tenant is responsible for when user click on Damages I am ' +
    'responsible for option in Damage dropdown in navbar', () => {
    expect(navbarPage.getDamagesResponsibleForLink().isDisplayed()).toBe(true);

    navbarPage.getDamagesResponsibleForLink().click();

    browser.wait(function() {
      return damageListPage.getDamageListElement().isDisplayed();
    }).then(function() {
      // expected view of page
      expect(damageListPage.getTitle().isDisplayed()).toBe(true);
      expect(damageListPage.getTitle().getText()).toEqual('Damages I am responsible person for');
      expect(damageListPage.getSearchDateRangeInput().isDisplayed()).toBe(true);
      expect(damageListPage.getSearchDateRangeClearButton().isDisplayed()).toBe(true);
      expect(damageListPage.getSearchDateRangeClearButton().isEnabled()).toBe(true);
      expect(damageListPage.getSearchDateRangeSearchButton().isDisplayed()).toBe(true);
      expect(damageListPage.getSearchDateRangeSearchButton().isEnabled()).toBe(true);
      // link for delegating personal responsibility for some damage types in building
      expect(damageListPage.getDamageTypesResponsibilityOption().isDisplayed()).toBe(true);

      // if list of damages is not empty, list is displayed
      if(damageListPage.getDamageList().isPresent() && damageListPage.getDamageList().isDisplayed()) {
        expect(damageListPage.getMessageNoDamageInList().isPresent()).toBe(false);
        expect(damageListPage.getPanelsFromDamageList().isPresent()).toBe(true);
        expect(damageListPage.getPaginatorInDamageList().isDisplayed()).toBe(true);

        let numberOfDamages = {'value':0};
        damageListPage.getPanelsFromDamageList().count().then(value => {
          numberOfDamages.value = value;

          // for every damage in list, wh should check is there option for damage details and delegate
          // responsibility in damage menu
          for(let i: number = 0; i<numberOfDamages.value; i++) {
            expect(damageListPage.getCreatorNameFromDamagePanel(i).isDisplayed()).toBe(true);
            expect(damageListPage.getDateFromDamagePanel(i).isDisplayed()).toBe(true);
            expect(damageListPage.getDamageTypeFromDamagePanel(i).isDisplayed()).toBe(true);
            expect(damageListPage.getDamageDescriptionFromDamagePanel(i).isDisplayed()).toBe(true);

            if(damageListPage.getMenuFromDamagePanel(i).isPresent() &&
              damageListPage.getMenuFromDamagePanel(i).isDisplayed()) {
              damageListPage.getMenuFromDamagePanel(i).click();

              browser.wait(function() {
                return damageListPage.getContentOfMenuFromDamagePanel(i).isDisplayed();
              }).then(function() {
                expect(damageListPage.getDamageDetailsLinkFromDamagePanel(i).isDisplayed()).toBe(true);
                expect(damageListPage.getDamageDetailsLinkFromDamagePanel(i).getText()).toEqual('Damage details');
                // second option in damage menu in this category of damages is delegating responsibility option
                expect(damageListPage.getLinkFromDamagePanel(i, 2).isDisplayed()).toBe(true);
                expect(damageListPage.getLinkFromDamagePanel(i,2).getText()).toEqual('Delegate responsibility')
              });
            }
          }
        });
      }
      // opposite, list is not displayed
      else {
        expect(damageListPage.getMessageNoDamageInList().isDisplayed()).toBe(true);
        expect(damageListPage.getMessageNoDamageInList().getText()).toContain('There is still no damages');
      }
    });
  });



  it('should show list of damages that are created by current tenant when user click on Damages I reported ' +
    'option in Damage dropdown in navbar', () => {
    expect(navbarPage.getDamagesIReportedLink().isDisplayed()).toBe(true);

    navbarPage.getDamagesIReportedLink().click();

    browser.wait(function() {
      return damageListPage.getDamageListElement().isDisplayed();
    }).then(function() {
      // expected view of page
      expect(damageListPage.getTitle().isDisplayed()).toBe(true);
      expect(damageListPage.getTitle().getText()).toEqual('Damages that I reported');
      expect(damageListPage.getSearchDateRangeInput().isDisplayed()).toBe(true);
      expect(damageListPage.getSearchDateRangeClearButton().isDisplayed()).toBe(true);
      expect(damageListPage.getSearchDateRangeClearButton().isEnabled()).toBe(true);
      expect(damageListPage.getSearchDateRangeSearchButton().isDisplayed()).toBe(true);
      expect(damageListPage.getSearchDateRangeSearchButton().isEnabled()).toBe(true);
      expect(damageListPage.getAddButton().isDisplayed()).toBe(true);
      expect(damageListPage.getAddButton().isEnabled()).toBe(true);

      // if list of damages is not empty, list is displayed
      if(damageListPage.getDamageList().isPresent() && damageListPage.getDamageList().isDisplayed()) {
        expect(damageListPage.getMessageNoDamageInList().isPresent()).toBe(false);
        expect(damageListPage.getPanelsFromDamageList().isPresent()).toBe(true);
        expect(damageListPage.getPaginatorInDamageList().isDisplayed()).toBe(true);

        let numberOfDamages = {'value':0};
        damageListPage.getPanelsFromDamageList().count().then(value => {
          numberOfDamages.value = value;

          // for every damage in list, wh should check is there option for damage details in damage menu
          // and options for edit and delete damage (if damage is not already in fixing process)
          for(let i: number = 0; i<numberOfDamages.value; i++) {
            expect(damageListPage.getCreatorNameFromDamagePanel(i).isDisplayed()).toBe(true);
            expect(damageListPage.getDateFromDamagePanel(i).isDisplayed()).toBe(true);
            expect(damageListPage.getDamageTypeFromDamagePanel(i).isDisplayed()).toBe(true);
            expect(damageListPage.getDamageDescriptionFromDamagePanel(i).isDisplayed()).toBe(true);

            if(damageListPage.getMenuFromDamagePanel(i).isPresent() &&
              damageListPage.getMenuFromDamagePanel(i).isDisplayed()) {
              damageListPage.getMenuFromDamagePanel(i).click();

              browser.wait(function() {
                return damageListPage.getContentOfMenuFromDamagePanel(i).isDisplayed();
              }).then(function() {

                // we should check number of menu options so we can check visibility of that menu options
                damageListPage.getMenuOptionsFromDamagePanel().count().then(value => {
                  // there is damage details option for sure
                  expect(damageListPage.getDamageDetailsLinkFromDamagePanel(i).isDisplayed()).toBe(true);
                  expect(damageListPage.getDamageDetailsLinkFromDamagePanel(i).getText()).toEqual('Damage details');

                  // but if number of options are greater then 1, there are also options Edit and Delete
                  if(value > 1) {
                    // edit option is second option in this menu
                    expect(damageListPage.getLinkFromDamagePanel(i, 2).isDisplayed()).toBe(true);
                    expect(damageListPage.getLinkFromDamagePanel(i,2).getText()).toEqual('Edit damage');
                    // delete option is third option in this menu
                    expect(damageListPage.getLinkFromDamagePanel(i, 3).isDisplayed()).toBe(true);
                    expect(damageListPage.getLinkFromDamagePanel(i,3).getText()).toBe('Remove damage');
                  }
                });
              });
            }
          }
        });
      }
      // opposite, list is not displayed
      else {
        expect(damageListPage.getMessageNoDamageInList().isDisplayed()).toBe(true);
        expect(damageListPage.getMessageNoDamageInList().getText()).toContain('There is still no damages');
      }
    });
  });



  it('should show list of damages that are in building where current tenant is council\'s president, ' +
    'when user click on Damages in my building option in Damage dropdown in navbar', () => {
    expect(navbarPage.getDamagesInMyBuildingLink(3).isDisplayed()).toBe(true);

    navbarPage.getDamagesInMyBuildingLink(3).click();

    browser.wait(function() {
      return damageListPage.getDamageListElement().isDisplayed();
    }).then(function() {
      // expected view of page
      expect(damageListPage.getTitle().isDisplayed()).toBe(true);
      expect(damageListPage.getTitle().getText()).toEqual('Damages in my building');
      expect(damageListPage.getSearchDateRangeInput().isDisplayed()).toBe(true);
      expect(damageListPage.getSearchDateRangeClearButton().isDisplayed()).toBe(true);
      expect(damageListPage.getSearchDateRangeClearButton().isEnabled()).toBe(true);
      expect(damageListPage.getSearchDateRangeSearchButton().isDisplayed()).toBe(true);
      expect(damageListPage.getSearchDateRangeSearchButton().isEnabled()).toBe(true);
      // link for setting responsibility for damage types in building (by council president)
      expect(damageListPage.getDamageTypesResponsibilityOption().isDisplayed()).toBe(true);

      // if list of damages is not empty, list is displayed
      if(damageListPage.getDamageList().isPresent() && damageListPage.getDamageList().isDisplayed()) {
        expect(damageListPage.getMessageNoDamageInList().isPresent()).toBe(false);
        expect(damageListPage.getPanelsFromDamageList().isPresent()).toBe(true);
        expect(damageListPage.getPaginatorInDamageList().isDisplayed()).toBe(true);

        let numberOfDamages = {'value':0};
        damageListPage.getPanelsFromDamageList().count().then(value => {
          numberOfDamages.value = value;

          // for every damage in list, wh should check is there option for damage details in damage menu
          for(let i: number = 0; i<numberOfDamages.value; i++) {
            expect(damageListPage.getCreatorNameFromDamagePanel(i).isDisplayed()).toBe(true);
            expect(damageListPage.getDateFromDamagePanel(i).isDisplayed()).toBe(true);
            expect(damageListPage.getDamageTypeFromDamagePanel(i).isDisplayed()).toBe(true);
            expect(damageListPage.getDamageDescriptionFromDamagePanel(i).isDisplayed()).toBe(true);

            if(damageListPage.getMenuFromDamagePanel(i).isPresent() &&
              damageListPage.getMenuFromDamagePanel(i).isDisplayed()) {
              damageListPage.getMenuFromDamagePanel(i).click();

              browser.wait(function() {
                return damageListPage.getContentOfMenuFromDamagePanel(i).isDisplayed();
              }).then(function() {
                expect(damageListPage.getDamageDetailsLinkFromDamagePanel(i).isDisplayed()).toBe(true);
                expect(damageListPage.getDamageDetailsLinkFromDamagePanel(i).getText()).toEqual('Damage details');
              });
            }
          }
        });
      }
      // opposite, list is not displayed
      else {
        expect(damageListPage.getMessageNoDamageInList().isDisplayed()).toBe(true);
        expect(damageListPage.getMessageNoDamageInList().getText()).toContain('There is still no damages');
      }
    });
  });



  it('should show list of damages that are in apartments which current tenant owner is, ' +
    'when user click on Damages in my personal apartments option in Damage dropdown in navbar', () => {
    expect(navbarPage.getDamagesInMyPersonalApartmentsLink(4).isDisplayed()).toBe(true);

    navbarPage.getDamagesInMyPersonalApartmentsLink(4).click();

    browser.wait(function() {
      return damageListPage.getDamageListElement().isDisplayed();
    }).then(function() {
      // expected view of page
      expect(damageListPage.getTitle().isDisplayed()).toBe(true);
      expect(damageListPage.getTitle().getText()).toEqual('Damages in my personal apartments');
      expect(damageListPage.getSearchDateRangeInput().isDisplayed()).toBe(true);
      expect(damageListPage.getSearchDateRangeClearButton().isDisplayed()).toBe(true);
      expect(damageListPage.getSearchDateRangeClearButton().isEnabled()).toBe(true);
      expect(damageListPage.getSearchDateRangeSearchButton().isDisplayed()).toBe(true);
      expect(damageListPage.getSearchDateRangeSearchButton().isEnabled()).toBe(true);

      // if list of damages is not empty, list is displayed
      if(damageListPage.getDamageList().isPresent() && damageListPage.getDamageList().isDisplayed()) {
        expect(damageListPage.getMessageNoDamageInList().isPresent()).toBe(false);
        expect(damageListPage.getPanelsFromDamageList().isPresent()).toBe(true);
        expect(damageListPage.getPaginatorInDamageList().isDisplayed()).toBe(true);

        let numberOfDamages = {'value':0};
        damageListPage.getPanelsFromDamageList().count().then(value => {
          numberOfDamages.value = value;

          // for every damage in list, wh should check is there option for damage details in damage menu
          for(let i: number = 0; i<numberOfDamages.value; i++) {
            expect(damageListPage.getCreatorNameFromDamagePanel(i).isDisplayed()).toBe(true);
            expect(damageListPage.getDateFromDamagePanel(i).isDisplayed()).toBe(true);
            expect(damageListPage.getDamageTypeFromDamagePanel(i).isDisplayed()).toBe(true);
            expect(damageListPage.getDamageDescriptionFromDamagePanel(i).isDisplayed()).toBe(true);

            if(damageListPage.getMenuFromDamagePanel(i).isPresent() &&
              damageListPage.getMenuFromDamagePanel(i).isDisplayed()) {
              damageListPage.getMenuFromDamagePanel(i).click();

              browser.wait(function() {
                return damageListPage.getContentOfMenuFromDamagePanel(i).isDisplayed();
              }).then(function() {
                expect(damageListPage.getDamageDetailsLinkFromDamagePanel(i).isDisplayed()).toBe(true);
                expect(damageListPage.getDamageDetailsLinkFromDamagePanel(i).getText()).toEqual('Damage details');
              });
            }
          }
        });
      }
      // opposite, list is not displayed
      else {
        expect(damageListPage.getMessageNoDamageInList().isDisplayed()).toBe(true);
        expect(damageListPage.getMessageNoDamageInList().getText()).toContain('There is still no damages');
      }
    });
  });



  it('should add new damage in list of damages when user in on page with damages that are created by him/her,' +
    'and click on button Add', () => {
    expect(navbarPage.getDamagesIReportedLink().isDisplayed()).toBe(true);

    navbarPage.getDamagesIReportedLink().click();

    browser.wait(function() {
      return damageListPage.getDamageListElement().isDisplayed();
    }).then(function() {
      //expect to see enable button Add on this page
      expect(damageListPage.getAddButton().isDisplayed()).toBe(true);
      expect(damageListPage.getAddButton().isEnabled()).toBe(true);

      damageListPage.getAddButton().click();

      browser.wait(function() {
        return damageAddPage.getDamageAddElement().isDisplayed();
      }).then(function() {
        // expect to see necessary inputs and button for creating new damage
        expect(damageAddPage.getDescriptionInput().isDisplayed()).toBe(true);
        expect(damageAddPage.getDamageUrgencySelector().isDisplayed()).toBe(true);
        expect(damageAddPage.getSaveButton().isDisplayed()).toBe(true);
        expect(damageAddPage.getSaveButton().isEnabled()).toBe(false);

        damageAddPage.setDamageDescription(newDamage.description);

        damageAddPage.getDamageUrgencySelector().click();
        damageAddPage.getDamageUrgencyOptions().count().then(value => {
          // expect to have only 2 option in urgency selector - urgent and not urgent
          expect(value).toEqual(2);

          expect(damageAddPage.getDamageUrgent(1).isDisplayed()).toBe(true);
          expect(damageAddPage.getDamageUrgent(1).getAttribute('value')).toBe('true');

          // click on urgent option in urgency selector for damage, to be sure that after
          // click on Save button this new damage will be first in list
          damageAddPage.getDamageUrgent(1).click();
          // default values for damage type is OTHER
          newDamage.type = 'OTHER';

          expect(damageAddPage.getSaveButton().isEnabled()).toBe(true);
          damageAddPage.getSaveButton().click();

          // expect to see this damage as first in damage list (with urgent label)
          expect(damageListPage.getDamageDescriptionFromDamagePanel(0).getText()).toEqual(newDamage.description);
          damageListPage.getDamageTypeFromDamagePanel(0).getText().then(textValue => {
            expect(textValue.toLowerCase() === newDamage.type.toLowerCase());
          });
          expect(damageListPage.getLabelUrgentFromDamagePanel(0).isDisplayed()).toBe(true);
          damageListPage.getDateFromDamagePanel(0).getText().then(dateValue => {
            // difference between current time and time of new damage is less then 5sec
            let currentDate = new Date().getTime();
            newDamage.date = dateValue;
            let damageDate = DateConverter.convertStringToDate(dateValue).getTime();
              expect(currentDate - damageDate).toBeLessThan(5000);
          });
          damageListPage.getCreatorNameFromDamagePanel(0).getText().then(creatorValue => {
            newDamage.creator = creatorValue;
          });
        });
      });
    });
  });



  it('should edit previous added damage when user in on page with damages that are created by him/her,' +
    'and click on option Edit damage in menu of first damage in list', () => {
    expect(damageListPage.getMenuFromDamagePanel(0).isDisplayed()).toBe(true);
    damageListPage.getMenuFromDamagePanel(0).click();

    browser.wait(function() {
      return damageListPage.getContentOfMenuFromDamagePanel(0).isDisplayed();
    }).then(function() {
      // link for Edit damage is second in option list (in menu)
      expect(damageListPage.getLinkFromDamagePanel(0, 2).isDisplayed()).toBe(true);

      damageListPage.getLinkFromDamagePanel(0, 2).click();

      browser.wait(function() {
        // expect to show modal - page for editing selected damage
        return damageEditPage.getDamageEditElement().isDisplayed();
      }).then(function() {
        // expect to modal has all elements
        expect(damageEditPage.getDescriptionInput().isDisplayed()).toBe(true);
        expect(damageEditPage.getSaveButton().isDisplayed()).toBe(true);
        expect(damageEditPage.getSaveButton().isEnabled()).toBe(false);

        damageEditPage.getDescriptionInput().clear();
        damageEditPage.setDescription(updatedDamage.description);

        damageEditPage.getSaveButton().click().then(function() {
          // expect that modal is closed and updated damage has updated values for description field,
          // but date and creator fields must be same as in newDamage object
          expect(damageListPage.getDamageDescriptionFromDamagePanel(0).getText()).toEqual(updatedDamage.description);
          damageListPage.getDateFromDamagePanel(0).getText().then(value => {
            expect(value).toEqual(newDamage.date);
          });
          damageListPage.getCreatorNameFromDamagePanel(0).getText().then(value => {
            expect(value).toEqual(newDamage.creator);
          });
        });
      });
    });
  });



  it('should open damage details page when user in on page with damages that are created by him/her,' +
    'and click on option Damage details in menu of first damage in list', () => {
    expect(damageListPage.getMenuFromDamagePanel(0).isDisplayed()).toBe(true);
    damageListPage.getMenuFromDamagePanel(0).click();

    browser.wait(function() {
      return damageListPage.getContentOfMenuFromDamagePanel(0).isDisplayed();
    }).then(function() {
      expect(damageListPage.getDamageDetailsLinkFromDamagePanel(0).isDisplayed()).toBe(true);

      damageListPage.getDamageDetailsLinkFromDamagePanel(0).click();

      browser.wait(function() {
        // expect to show page with details of selected damage
        return damageDetailsPage.getDamageDetailsElement().isDisplayed();
      }).then(function() {
        // expect to page has all elements
        expect(damageDetailsPage.getDamageDescription().isDisplayed()).toBe(true);
        expect(damageDetailsPage.getDamageDescription().getText()).toEqual(updatedDamage.description);
        expect(damageDetailsPage.getDamageCreator().isDisplayed()).toBe(true);
        expect(damageDetailsPage.getDamageCreator().getText()).toBe(newDamage.creator);
        expect(damageDetailsPage.getDamageDate().isDisplayed()).toBe(true);
        expect(damageDetailsPage.getDamageDate().getText()).toBe(newDamage.date);
        expect(damageDetailsPage.getLabelUrgent().isDisplayed()).toBe(true);
      });
    });
  });



  it('should remove previous added and updated damage from list when user click on Delete option ' +
    'from menu in damage panel', () => {
    expect(navbarPage.getDamagesIReportedLink().isDisplayed()).toBe(true);

    navbarPage.getDamagesIReportedLink().click();

    browser.wait(function() {
      return damageListPage.getDamageListElement().isDisplayed();
    }).then(function() {
      expect(damageListPage.getMenuFromDamagePanel(0).isDisplayed()).toBe(true);
      damageListPage.getMenuFromDamagePanel(0).click();

      browser.wait(function() {
        return damageListPage.getContentOfMenuFromDamagePanel(0).isDisplayed();
      }).then(function() {
        // delete option is third in option list (in menu)
        damageListPage.getLinkFromDamagePanel(0, 3).click();

        browser.wait(function() {
          // expect to show modal dialog for delete confirmation
          return confirmDeletePage.getConfirmDeleteElement().isDisplayed();
        }).then(function() {
          // expect to modal has all elements
          expect(confirmDeletePage.getConfirmButton().isDisplayed()).toBe(true);
          expect(confirmDeletePage.getConfirmButton().isEnabled()).toBe(true);
          expect(confirmDeletePage.getDeclineButton().isDisplayed()).toBe(true);
          expect(confirmDeletePage.getDeclineButton().isEnabled()).toBe(true);

          confirmDeletePage.getConfirmButton().click();

          // expect to first damage in list has date less then date of new damage (that we removed in previous line)
          // description, type, creator possibly can be equals
          damageListPage.getDateFromDamagePanel(0).getText().then(value => {
            let date = DateConverter.convertStringToDate(value).getTime();
            let removedDamageDate = DateConverter.convertStringToDate(newDamage.date).getTime();
            expect(removedDamageDate - date).toBeGreaterThan(0);
          });
        });
      });
    });
  });



  it('should open modal dialog for delegating responsibility for damage when user click on Delegate ' +
    'responsibility option in menu and close that modal when user click on Close button', () => {
    expect(navbarPage.getDamagesResponsibleForLink().isDisplayed()).toBe(true);

    navbarPage.getDamagesResponsibleForLink().click();

    browser.wait(function() {
      return damageListPage.getDamageListElement().isDisplayed();
    }).then(function() {
      expect(damageListPage.getMenuFromDamagePanel(0).isDisplayed()).toBe(true);
      damageListPage.getMenuFromDamagePanel(0).click();

      browser.wait(function() {
        return damageListPage.getContentOfMenuFromDamagePanel(0).isDisplayed();
      }).then(function() {
        // delegate responsibility option is second in option list (in menu)
        damageListPage.getLinkFromDamagePanel(0, 2).click();

        browser.wait(function() {
          // expect to show modal dialog for delegating responsibility
          return delegateResponsibilityToPersonPage.getDelegateResponsibilityToPersonElement().isDisplayed();
        }).then(function() {
          // expect to modal has all elements
          expect(delegateResponsibilityToPersonPage.getTitle().isDisplayed()).toBe(true);
          expect(delegateResponsibilityToPersonPage.getCloseButton().isDisplayed()).toBe(true);
          expect(delegateResponsibilityToPersonPage.getCloseButton().isEnabled()).toBe(true);

          delegateResponsibilityToPersonPage.getCloseButton().click();
        });
      });
    });
  });



  it('should open modal dialog for delegating responsibility for damage types when user click on ' +
    'My responsibilities for damage types option on the top of damage list and close that modal when user ' +
    'click on Close button', () => {
    expect(damageListPage.getDamageTypesResponsibilityOption().isDisplayed()).toBe(true);
    damageListPage.getDamageTypesResponsibilityOption().click();

    browser.wait(function() {
      return damageTypesModalPage.getDamageTypesModalElement().isDisplayed();
    }).then(function() {
      expect(damageTypesModalPage.getTitle().isDisplayed()).toBe(true);
      expect(damageTypesModalPage.getCloseButton().isDisplayed()).toBe(true);
      expect(damageTypesModalPage.getCloseButton().isEnabled()).toBe(true);

      damageTypesModalPage.getCloseButton().click();
    });
  });



  it('should open modal dialog for delegating responsibility for damage types when president click on ' +
    'Responsibilities for damage types in building option on the top of damage list (in section Damages in my building) ' +
    'and close that modal when president click on Close button', () => {
    expect(navbarPage.getDamagesInMyBuildingLink(3).isDisplayed()).toBe(true);

    navbarPage.getDamagesInMyBuildingLink(3).click();

    browser.wait(function() {
      return damageListPage.getDamageListElement().isDisplayed();
    }).then(function() {
      expect(damageListPage.getDamageTypesResponsibilityOption().isDisplayed()).toBe(true);
      damageListPage.getDamageTypesResponsibilityOption().click();

      browser.wait(function() {
        return damageTypesModalPage.getDamageTypesModalElement().isDisplayed();
      }).then(function() {
        expect(damageTypesModalPage.getTitle().isDisplayed()).toBe(true);
        expect(damageTypesModalPage.getCloseButton().isDisplayed()).toBe(true);
        expect(damageTypesModalPage.getCloseButton().isEnabled()).toBe(true);

        damageTypesModalPage.getCloseButton().click();
      });
    });
  });
});
