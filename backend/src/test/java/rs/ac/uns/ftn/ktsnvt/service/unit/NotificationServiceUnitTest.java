package rs.ac.uns.ftn.ktsnvt.service.unit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Billboard;
import rs.ac.uns.ftn.ktsnvt.model.entity.Notification;
import rs.ac.uns.ftn.ktsnvt.repository.NotificationRepository;
import rs.ac.uns.ftn.ktsnvt.service.NotificationService;

import static org.mockito.BDDMockito.given;
import static org.junit.Assert.*;
import java.util.Date;

/**
 * Created by Ivana Zeljkovic on 05/12/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class NotificationServiceUnitTest {

    @Autowired
    private NotificationService notificationService;

    @MockBean
    private NotificationRepository notificationRepository;



    @Before
    public void setUp() {
        // NOTIFICATIONS
        Notification notification_1 = new Notification(
                "First notification", "First notification content", new Date());
        notification_1.setId(100L);
        Notification notification_2 = new Notification(
                "Second notification", "Second notification content", new Date());
        notification_2.setId(101L);

        Billboard billboard = new Billboard();
        Billboard billboard_2 = new Billboard();
        billboard.setId(100L);
        billboard_2.setId(101L);
        billboard.getNotifications().add(notification_1);
        billboard_2.getNotifications().add(notification_2);

        given(
                this.notificationRepository.findOneByIdAndBillboard(new Long(100), new Long(100))
        ).willReturn(
                notification_1
        );

        given(
                this.notificationRepository.findOneByIdAndBillboard(new Long(101), new Long(100))
        ).willReturn(
                null
        );

        given(
                this.notificationRepository.findOne(new Long(100))
        ).willReturn(
                notification_1
        );

        given(
                this.notificationRepository.findOne(new Long(200))
        ).willReturn(
                null
        );
    }

    @Test
    public void testFindOneByIdAndBillboard() {
        /*
         * Test proverava ispravnost rada metode findOneByIdAndBillboard servisa NotificationService
         * (provera da li postoji konkretno javno obavestenje na osnovu zadatog ID-a i ID-a bilborda zgrade kom obavestenje pripada)
         * u slucaju kada postoji obavestenje sa zadatim ID-em na bilbordu sa zadatim ID-em
         * */

        /*
        oglasnoj tabli sa ID-em = 100 pripada obavestenje:
            ID = 100,
            content = First notification
        */
        this.notificationService.findOneByIdAndBillboard(new Long(100), new Long(100));
    }

    @Test(expected = BadRequestException.class)
    public void testFindOneByIdAndBillboardNonexistent() {
        /*
         * Test proverava ispravnost rada metode findOneByIdAndBillboard servisa NotificationService
         * (provera da li postoji konkretno javnog obavestenje na osnovu zadatog ID-a i ID-a bilborda zgrade kom obavestenje pripada)
         * u slucaju kada ne postoji obavestenje sa zadatim ID-em na bilbordu sa zadatim ID-em - ocekivano NotFoundException
         * */

        // oglasnoj tabli sa ID-em = 100 ne pripada obavestenje sa ID-em 101:
        this.notificationService.findOneByIdAndBillboard(new Long(101), new Long(100));
    }

    @Test
    public void testFindOne() {
        /*
         * Test proverava ispravnost metode findOne servisa NotificationService
         * (dobavljanje objekta obavestenja na osnovu zadatog ID-a)
         * u slucaju kada postoji obavestenje sa zadatim ID-em
         * */

        /*
        postoji obavestenje sa ID-em = 100 i podacima:
            ID = 100,
            content = First notification
         */
        Notification notification = this.notificationService.findOne(new Long(100));

        assertNotNull(notification);
        assertEquals(Long.valueOf(100), notification.getId());
        assertEquals("First notification content", notification.getContent());
    }

    @Test(expected = NotFoundException.class)
    public void testFindOneNonexistent() {
        /*
         * Test proverava ispravnost metode findOne servisa NotificationService
         * (dobavljanje objekta obavestenja na osnovu zadatog ID-a)
         * u slucaju kada ne postoji obavestenje sa zadatim ID-em - ocekivano NotFoundException
         * */

        // ne postoji obavestenje sa ID-em = 200
        Notification notification = this.notificationService.findOne(new Long(200));

        assertNull(notification);
    }

}
