import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoosePresidentModalComponent } from './choose-president-modal.component';

describe('ChoosePresidentModalComponent', () => {
  let component: ChoosePresidentModalComponent;
  let fixture: ComponentFixture<ChoosePresidentModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChoosePresidentModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChoosePresidentModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
