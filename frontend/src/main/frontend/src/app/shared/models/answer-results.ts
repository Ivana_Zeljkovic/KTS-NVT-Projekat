export interface AnswerResults {
  content: string;
  numberOfVotes: number;
}
