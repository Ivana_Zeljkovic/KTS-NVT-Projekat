import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class DamagesCategoryService {
  private category = new BehaviorSubject<string>('');
  currentCategory = this.category.asObservable();

  constructor() { }

  changeCategory(message: string) {
    this.category.next(message);
  }
}
