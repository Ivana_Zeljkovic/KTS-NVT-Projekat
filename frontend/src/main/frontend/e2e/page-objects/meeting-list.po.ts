import {by, element, ElementArrayFinder, ElementFinder} from "protractor";

export class MeetingListPage {
  // get methods
  getMeetingListElement(): ElementFinder {
    return element(by.tagName('app-meeting-list'));
  }

  getTitle(): ElementFinder {
    return element(by.css('app-meeting-list div.title > h3'));
  }

  getAddLink(): ElementFinder {
    return element(by.css('.div-add > a'));
  }

  getMeetingsTable(): ElementFinder {
    return element(by.id('meetingsTable'));
  }

  getMeetingsTableRows(): ElementArrayFinder {
    return element.all(by.css('#meetingsTable > tbody > tr'));
  }

  getMeetingDate(index: number): ElementFinder {
    return element(by.css(`#meetingsTable > tbody > tr:nth-child(${index+1}) > td:nth-child(1)`));
  }

  getMeetingDuration(index: number): ElementFinder {
    return element(by.css(`#meetingsTable > tbody > tr:nth-child(${index+1}) > td:nth-child(2)`));
  }

  getMeetingNumberOfMeetingItems(index: number): ElementFinder {
    return element(by.css(`#meetingsTable > tbody > tr:nth-child(${index+1}) > td:nth-child(3)`));
  }

  getDetailsOptionFromMeetingRow(index: number): ElementFinder {
    return element(by.css(`#meetingsTable > tbody > tr:nth-child(${index+1}) > td:nth-child(4)`));
  }

  getMeetingMenu(index: number): ElementFinder {
    return element(by.css(`#meetingsTable > tbody > tr:nth-child(${index+1}) > td:nth-child(4) > span`));
  }

  getContentOfMeetingMenu(index: number): ElementFinder {
    return element(by.css(`#meetingsTable > tbody > tr:nth-child(${index+1}) > td:nth-child(4) #meetingPopover`));
  }

  getDetailsOptionFromMeetingMenu(index: number): ElementFinder {
    return element(by.css(`#meetingsTable > tbody > tr:nth-child(${index+1}) > td:nth-child(4) #meetingPopover > div:nth-child(1)`));
  }

  getRemoveOptionFromMeetingMenu(index: number): ElementFinder {
    return element(by.css(`#meetingsTable > tbody > tr:nth-child(${index+1}) > td:nth-child(4) #meetingPopover > div:nth-child(2)`));
  }

  getTitleForFinishedMeetingList(): ElementFinder {
    return element(by.css('.title-finished-meetings'));
  }

  getFinishedMeetingsList(): ElementFinder {
    return element(by.id('finishedMeetingsTable'));
  }

  getFinishedMeetingsTableRows(): ElementArrayFinder {
    return element.all(by.css('#finishedMeetingsTable > tbody > tr'));
  }

  getFinishedMeetingDate(index: number): ElementFinder {
    return element(by.css(`#finishedMeetingsTable > tbody > tr:nth-child(${index+1}) > td:nth-child(1)`));
  }

  getFinishedMeetingDuration(index: number): ElementFinder {
    return element(by.css(`#finishedMeetingsTable > tbody > tr:nth-child(${index+1}) > td:nth-child(2)`));
  }

  getFinishedMeetingNumberOfMeetingItems(index: number): ElementFinder {
    return element(by.css(`#finishedMeetingsTable > tbody > tr:nth-child(${index+1}) > td:nth-child(3)`));
  }

  getFinishedMeetingMenu(index: number): ElementFinder {
    return element(by.css(`#finishedMeetingsTable > tbody > tr:nth-child(${index+1}) > td.meeting-menu > span`));
  }

  getContentOfFinishedMeetingMenu(index: number): ElementFinder {
    return element(by.css(`#finishedMeetingsTable > tbody > tr:nth-child(${index+1}) > td.meeting-menu #finishedMeetingPopover`));
  }

  getDetailsOptionFromFinishedMeetingMenu(index: number): ElementFinder {
    return element(by.css(`#finishedMeetingsTable > tbody > tr:nth-child(${index+1}) > td.meeting-menu #finishedMeetingPopover > div:nth-child(1)`));
  }

  getCreateRecordOptionFromFinishedMeetingMenu(index: number): ElementFinder {
    return element(by.css(`#finishedMeetingsTable > tbody > tr:nth-child(${index+1}) > td.meeting-menu #finishedMeetingPopover > div:nth-child(2)`));
  }
}
