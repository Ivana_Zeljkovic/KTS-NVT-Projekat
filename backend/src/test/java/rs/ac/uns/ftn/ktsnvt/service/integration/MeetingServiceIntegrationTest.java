package rs.ac.uns.ftn.ktsnvt.service.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Meeting;
import rs.ac.uns.ftn.ktsnvt.service.MeetingService;

import static org.junit.Assert.*;

/**
 * Created by Ivana Zeljkovic on 01/12/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class MeetingServiceIntegrationTest {

    @Autowired
    private MeetingService meetingService;


    @Test
    @Transactional
    public void testFindOne() {
        /*
         * Test proverava ispravnost metode findOne servisa MeetingService
         * (dobavljanje objekta sastanka na osnovu zadatog ID-a)
         * u slucaju kada postoji sastanak sa zadatim ID-em
         * */

        /*
        u bazi postoji sastanak ciji je ID = 100 sa podacima:
            trajanje sastanka = 60min,
            ID skupstine stanara kojoj pripada = 100,
            sadrzi 2 tacke dnevnog reda
         */
        Meeting meeting = this.meetingService.findOne(new Long(100));

        assertNotNull(meeting);
        assertEquals(Long.valueOf(100), meeting.getId());
        assertEquals(60, meeting.getDuration());
        assertEquals(Long.valueOf(100), meeting.getCouncil().getId());
        assertEquals(2, meeting.getMeetingItems().size());
    }

    @Test(expected = NotFoundException.class)
    public void testFindOneNonexistent() {
        /*
         * Test proverava ispravnost metode findOne servisa MeetingService
         * (dobavljanje objekta sastanka na osnovu zadatog ID-a)
         * u slucaju kada ne postoji sastanak sa zadatim ID-em - ocekivano NotFoundException
         * */

        // u bazi ne postoji sastanak sa ID-em = 1099
        Meeting meeting = this.meetingService.findOne(new Long(1099));
    }

    @Test
    public void testCheckMeetingFinished() {
        /*
         * Test proverava ispravnost metode checkMeetingFinished servisa MeetingService
         * (provera da li je sastanak sa zadatim ID-em zavrsen)
         * u slucaju kada se sastanak sa zadatim ID-em zavrsio
         * */
        Meeting meeting = this.meetingService.findOne(new Long(100));

        // sastanak sa ID-em = 100 se zavrsio, pa metoda servisa nece vratiti nikakvu gresku
        this.meetingService.checkMeetingFinished(meeting);
    }

    @Test(expected = BadRequestException.class)
    public void testCheckMeetingFinishedNotYet() {
        /*
         * Test proverava ispravnost metode checkMeetingFinished servisa MeetingService
         * (provera da li je sastanak sa zadatim ID-em zavrsen)
         * u slucaju kada se sastanak sa zadatim ID-em jos uvek nije zavrsio - ocekivano BadRequestException
         * */
        Meeting meeting = this.meetingService.findOne(new Long(101));

        // sastanak sa ID-em = 101 se jos uvek nije zavrsio, pa metoda servisa baca izuzetak
        this.meetingService.checkMeetingFinished(meeting);
    }

    @Test
    @Transactional
    @Rollback(true)
    public void testClearMeetingItemsList() {
        /*
         * Test proverava ispravnost metode clearMeetingItemsList servisa MeetingService
         * (ciscenje liste tacaka dnevnog reda u prosledjenom objektu sastanka)
         * u slucaju kada postoji sastanak sa zadatim ID-em
         * */

        /*
        postoji sastanak ciji je ID = 100 sa podacima:
            trajanje sastanka = 60min,
            ID skupstine stanara kojoj pripada = 100,
            sadrzi 2 tacke dnevnog reda
         */
        Meeting meeting = this.meetingService.findOne(new Long(100));

        this.meetingService.clearMeetingItemsList(meeting);

        assertEquals(0, meeting.getMeetingItems().size());
    }

    @Test
    public void testCheckMeetingStart() {
        /*
         * Test proverava ispravnost metode checkMeetingStart servisa MeetingService
         * (provera da li je sastanak sa zadatim ID-em u toku svog trajanja ili se zavrsio vec)
         * u slucaju kada sastanak sa zadatim ID-em jos nije poceo
         * */

        Meeting meeting = this.meetingService.findOne(new Long(101));

        // sastanak sa ID-em = 101 jos nije poceo, pa metoda servisa ne baca izuzetak
        this.meetingService.checkMeetingStart(meeting);
    }

    @Test(expected = BadRequestException.class)
    public void testCheckMeetingStartMeetingFinished() {
        /*
         * Test proverava ispravnost metode checkMeetingStart servisa MeetingService
         * (provera da li je sastanak sa zadatim ID-em u toku svog trajanja ili se zavrsio vec)
         * u slucaju kada se sastanak sa zadatim ID-em zavrsio - ocekivano BadRequestException
         * */

        Meeting meeting = this.meetingService.findOne(new Long(100));

        // sastanak sa ID-em = 100 je zavrsen, pa metoda servisa baca izuzetak
        this.meetingService.checkMeetingStart(meeting);
    }

    @Test
    public void testMeetingIsInTheFutureFalse(){
        /*
         * Test proverava ispravnost metode meetingIsInTheFuture servisa MeetingService
         * (provera da li je sastanak u buducnosti)
         * u slucaju da je sastanak u buducnosti, vraca true, u suportnom vraca false
         * */


        Meeting meeting = this.meetingService.findOne(new Long(100L));
        assertEquals(false, this.meetingService.meetingIsInTheFuture(meeting));

    }

    @Test
    public void testMeetingIsInTheFutureTrue(){
        /*
         * Test proverava ispravnost metode meetingIsInTheFuture servisa MeetingService
         * (provera da li je sastanak u buducnosti)
         * u slucaju da je sastanak u buducnosti, vraca true, u suportnom vraca false
         * */

        Meeting meeting = this.meetingService.findOne(new Long(101L));
        assertEquals(true, this.meetingService.meetingIsInTheFuture(meeting));

    }
}
