import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";
// component
import {AparatmentListComponent} from "./aparatment-list/aparatment-list.component";
import {ApartmentComponent} from "./apartment/apartment.component";

const routes: Routes = [
  { path: '', component: AparatmentListComponent },
  { path: ':id', component: ApartmentComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ApartmentsRouterModule { }
