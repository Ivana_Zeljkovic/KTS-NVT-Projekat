import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";
// component
import { MeetingListComponent } from "./meeting-list/meeting-list.component";
import { MeetingDetailsComponent } from "./meeting-details/meeting-details.component";

const routes: Routes = [
  { path: '', component: MeetingListComponent },
  { path: ':id', component: MeetingDetailsComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class MeetingsRouterModule { }
