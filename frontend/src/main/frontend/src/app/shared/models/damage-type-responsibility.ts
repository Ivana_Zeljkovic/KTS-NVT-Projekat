import { Address } from "./address";

export interface DamageTypeResponsibility {
  id: number;
  damageTypeId: number;
  buildingId: number;
  address: Address;
  damageTypeName: string;
  numberOfElements?: number;
  tenantId?: number;
  institutionId?: number;
  tenantName?: string;
  institutionName?: string;
}
