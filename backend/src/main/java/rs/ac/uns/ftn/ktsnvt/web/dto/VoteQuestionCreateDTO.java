package rs.ac.uns.ftn.ktsnvt.web.dto;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by Ivana Zeljkovic on 19/11/2017.
 */
public class VoteQuestionCreateDTO implements Serializable {

    @NotNull
    private Long questionId;
    @NotNull
    @NotEmpty
    private String answer;

    public VoteQuestionCreateDTO() { }

    public VoteQuestionCreateDTO(Long questionId, String answer) {
        this.questionId = questionId;
        this.answer = answer;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
