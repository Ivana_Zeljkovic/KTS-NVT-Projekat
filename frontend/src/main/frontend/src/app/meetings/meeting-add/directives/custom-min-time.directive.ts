import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS, Validator, FormControl } from '@angular/forms';

@Directive({
  selector: '[customMinTime][ngModel]',
  providers: [{provide: NG_VALIDATORS, useExisting: CustomMinTimeDirective, multi: true}]
})
export class CustomMinTimeDirective implements Validator {
  @Input()
  customMinTime: Date;

  validate(c: FormControl): {[key: string]: any} {
    let v = c.value;
    if(!v) return null;
    return ( v.getHours() <= this.customMinTime.getHours() ) ?
      {"customMinTime": true} : null;
  }
}
