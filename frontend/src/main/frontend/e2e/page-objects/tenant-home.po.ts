import {by, element, ElementFinder} from "protractor";

export class TenantHomePage {
  getTenantHomeElement(): ElementFinder {
    return element(by.tagName('app-tenant-home'));
  }

  getTabset(): ElementFinder {
    return element(by.tagName('tabset'));
  }

  getNotificationsTabLink(): ElementFinder {
    return element(by.css('li.customTabClass:first-of-type > a'));
  }

  getMeetingItemsTabLink(): ElementFinder {
    return element(by.css('li.customTabClass:last-of-type > a'));
  }

  getActiveNotificationsTabLink(): ElementFinder {
    return element(by.css('li.customTabClass:first-of-type.active > a'))
  }

  getActiveMeetingItemsTabLink(): ElementFinder {
    return element(by.css('li.customTabClass:last-of-type.active > a'))
  }
}
