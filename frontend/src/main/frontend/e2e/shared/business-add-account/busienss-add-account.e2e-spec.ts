import {LoginPage} from "../../page-objects/login.po";
import {BusinessProfilePage} from "../../profile/business-profile/page-objects/business-profile.po";
import {browser} from "protractor";
import {BusinessAddAcountModalPage} from "../business-add-account/page-objects/businesss-add-account.po";
import {count} from "rxjs/operator/count";
import {BusinessProfileAccountsModalPage} from "../business-profile-accounts-modal/page-objects/business-profile-accounts-modal.po";
import {after} from "selenium-webdriver/testing";

describe('Adding new account to business via modal either trough registration or editing the current profile', () => {


  let loginPage: LoginPage;
  let businessProfilePage: BusinessProfilePage;
  let businessProfileAccountsModal: BusinessProfileAccountsModalPage;
  let addAccountModalPage: BusinessAddAcountModalPage;

  beforeAll(() => {

    loginPage = new LoginPage();
    browser.get("http://localhost:49152/#/login?returnUrl=%2Fprofile");
    loginPage.setUsername('firm2');
    loginPage.setPassword('firm2');
    loginPage.getSignInButton().submit();

    businessProfilePage = new BusinessProfilePage();
    browser.wait(function () {
      return browser.getCurrentUrl().then(value => {
        return value == 'http://localhost:49152/#/profile'
      });
    });

    businessProfileAccountsModal = new BusinessProfileAccountsModalPage();
    addAccountModalPage = new BusinessAddAcountModalPage();

    businessProfilePage.getManageAccounts().click().then(function () {
      expect(businessProfileAccountsModal.getComponent().isDisplayed()).toBe(true).then(function () {
        businessProfileAccountsModal.getOpenAddAccountModal().click();
      });
    });

  });


  afterEach(() =>{
      addAccountModalPage.setUsername('');
      addAccountModalPage.setPassword('');
      addAccountModalPage.setConfirmPassword('');
  });


  afterAll(() => {
    addAccountModalPage.getCloseButton().click();
    browser.executeScript('window.localStorage.clear();');
  });

  it('Should populate all three fields with valid data and the button for adding account will be displayed', () => {
    addAccountModalPage.setUsername('jova12');
    addAccountModalPage.setPassword('jova12');
    addAccountModalPage.setConfirmPassword('jova12');
    expect(addAccountModalPage.getAddAccountButton().isEnabled()).toBe(true);
  });

  it('Should display username required when username field was left blank and lost focus', () =>{
    addAccountModalPage.getUsernameField().click();
    addAccountModalPage.getPasswordField().click();
    expect(addAccountModalPage.getUsernameRequired().isDisplayed()).toBe(true);

  });

  it('Should display username taken when username field contains username which is taken', () =>{
    addAccountModalPage.setUsername('ivana');
    addAccountModalPage.getPasswordField().click();
    expect(addAccountModalPage.getUsernameIsTaken().isDisplayed()).toBe(true);
  });


  it('Should display password required when password field was left blank and lost focus', () =>{
    addAccountModalPage.getPasswordField().click();
    addAccountModalPage.getConfirmPasswordField().click();
    expect(addAccountModalPage.getPasswordRequired().isDisplayed()).toBe(true);
  });


  it('Should display password field has less than 5 characters', () =>{
    addAccountModalPage.setPassword('asf');
    addAccountModalPage.getConfirmPasswordField().click();
    expect(addAccountModalPage.getPasswordMinimumLength().isDisplayed()).toBe(true);
  });


  it('Should display confirm password required when confirm password field is left blank and lost focus', () =>{
    addAccountModalPage.getConfirmPasswordField().click();
    addAccountModalPage.getPasswordField().click();
    expect(addAccountModalPage.getConfirmPasswordRequired().isDisplayed()).toBe(true);
  });

  it('Should display passwords don\'t match when confirm password is different than password', () =>{
    addAccountModalPage.setPassword('jovica12');
    addAccountModalPage.setConfirmPassword('jovica');
    expect(addAccountModalPage.getConfirmPasswordNotMatch().isDisplayed()).toBe(true);
  });



});
