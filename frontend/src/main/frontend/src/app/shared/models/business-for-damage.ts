import { Address } from "./address";

export interface BusinessForDamage {
  name: string;
  id: number;
  address: Address;
  usernames:Array<string>;
}
