package rs.ac.uns.ftn.ktsnvt.service.unit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.*;
import rs.ac.uns.ftn.ktsnvt.repository.BuildingRepository;
import rs.ac.uns.ftn.ktsnvt.repository.MeetingItemRepository;
import rs.ac.uns.ftn.ktsnvt.service.BuildingService;
import rs.ac.uns.ftn.ktsnvt.service.MeetingItemService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;


/**
 * Created by Nikola Garabandic on 02/12/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MeetingItemServiceUnitTest {

    @Autowired
    private MeetingItemService meetingItemService;

    @Autowired
    private BuildingService buildingService;

    @MockBean
    private MeetingItemRepository meetingItemRepository;

    @MockBean
    private BuildingRepository buildingRepository;


    @Before
    public void setUp(){
        // QUESTIONNAIRE FOR MEETING ITEM 1
        Questionnaire questionnaire = new Questionnaire();
        questionnaire.setId(100L);

        // QUESTIONNAIRE FOR MEETING ITEM 2
        Questionnaire questionnaire_2 = new Questionnaire();
        questionnaire_2.setId(200L);


        // MEETING ITEMS FOR BILLBOARD IN BUILDING 1
        MeetingItem meeting_item_1 = new MeetingItem();
        meeting_item_1.setContent("First meeting item");
        meeting_item_1.setId(100L);
        meeting_item_1.setQuestionnaire(questionnaire);
        questionnaire.setMeetingItem(meeting_item_1);

        MeetingItem meeting_item_2 = new MeetingItem();
        meeting_item_2.setContent("Second meeting item");
        meeting_item_2.setId(200L);
        meeting_item_2.setQuestionnaire(questionnaire_2);
        questionnaire_2.setMeetingItem(meeting_item_2);

        MeetingItem meeting_item_3 = new MeetingItem();
        meeting_item_3.setContent("Third meeting item");
        meeting_item_3.setId(300L);

        // BUILDING 1
        Building building1 = new Building();
        building1.setId(100L);
        Billboard billboard1 = new Billboard();
        billboard1.setId(100L);
        building1.setBillboard(billboard1);
        building1.getBillboard().setMeetingItems(new ArrayList<MeetingItem>(){{
            add(meeting_item_1);
            add(meeting_item_2);
            add(meeting_item_3);
        }});

        // MEETING ITEM FOR BILLBOARD IN BUILDING 2
        MeetingItem meeting_item_4 = new MeetingItem();
        meeting_item_4.setContent("Fourth meeting item");
        meeting_item_4.setId(400L);
        Meeting meeting = new Meeting();
        meeting.setId(100L);
        meeting_item_4.setMeeting(meeting); //odradio sam ovo zbog testCheckMeetingItemIsNull testa

        // BUILDING 2
        Building building2 = new Building();
        building2.setId(200L);
        Billboard billboard2 = new Billboard();
        billboard2.setId(200L);
        building2.setBillboard(billboard2);
        building2.getBillboard().setMeetingItems(new ArrayList<MeetingItem>(){{
            add(meeting_item_4);
        }});


        given(
                this.meetingItemRepository.findOne(new Long(100))
        ).willReturn(
                meeting_item_1
        );

        given(
                this.meetingItemRepository.findOne(new Long(200))
        ).willReturn(
                meeting_item_2
        );

        given(
                this.meetingItemRepository.findOne(new Long(300))
        ).willReturn(
                meeting_item_3
        );

        given(
                this.meetingItemRepository.findOne(new Long(400))
        ).willReturn(
                meeting_item_4
        );

        given(
                this.meetingItemRepository.findOne(new Long(800))
        ).willReturn(
                null
        );

        given(
                this.buildingRepository.findOne(new Long(100))
        ).willReturn(
                building1
        );

        given(
                this.buildingRepository.findOne(new Long(200))
        ).willReturn(
                building2
        );

        given(
                this.meetingItemRepository.findOneByIdAndBillboard(new Long(100), new Long(100))
        ).willReturn(
                meeting_item_1
        );

        given(
                this.meetingItemRepository.findOneByIdAndBillboard(new Long(400), new Long(100))
        ).willReturn(
                null
        );

        given(
                this.meetingItemRepository.findOneByIdAndBillboard(new Long(400), new Long(200))
        ).willReturn(
                null
        );

        given(
                this.meetingItemRepository.findAllOnBillboard(new Long(100))
        ).willReturn(
                building1.getBillboard().getMeetingItems()
        );
    }

    @Test
    public void testCheckMeetingItemInBuilding(){
        /*
         * Test proverava ispravnost rada metode checkMeetingItemInBuilding servisa MeetingItemService
         * (provera da li tacka dnevnog reda pripada zgradi)
         * u slucaju kada tacka dnevnog reda sa zadatim ID-em pripada zgradi sa zadatim ID-em
         * */

        // tacka dnevnog reda sa ID-em 100 pripada zgradi ciji je ID 100
        Building building = this.buildingService.findOne(100L);
        MeetingItem meetingItem = this.meetingItemService.findOne(100L);

        List<MeetingItem> list = this.meetingItemService.findAllOnBillboard(building.getBillboard().getId());
        this.meetingItemService.checkMeetingItemInBuilding(meetingItem, list);
    }

    @Test(expected = BadRequestException.class)
    public void testMeetingItemNotInBuilding(){
        /*
         * Test proverava ispravnost rada metode CheckMeetingItemInBuilding servisa MeetingItemService
         * (provera da li tacka dnevnog reda pripada zgradi)
         * u slucaju kada tacka dnevnog reda sa zadatim ID-em ne pripada zgradi sa zadatim ID-em - ocekivano BadRequestException
         * */

        // tacka dnevnog reda sa ID-em 400 ne pripada zgradi ciji je ID 100
        Building building = this.buildingService.findOne(100L);
        MeetingItem meetingItem = this.meetingItemService.findOne(400L);

        List<MeetingItem> list = this.meetingItemService.findAllOnBillboard(building.getBillboard().getId());
        this.meetingItemService.checkMeetingItemInBuilding(meetingItem, list);
    }

    @Test
    public void testCheckMeetingItemIsNull(){
        /*
         * Test proverava ispravnost rada metode CheckMeetingItemIsNull servisa MeetingItemService
         * (proverava da li je tacka dnevnog reda dodeljena vec nekom sastanku)
         * u slucaju kada zadata tacka dnevnog reda jos uvek nije dodeljena nijednom sastanku
         * */

        // tacka dnevnog reda sa ID-em 100 nije dodeljena nijednom sastanku
        MeetingItem meetingItem = meetingItemService.findOne(100L);

        this.meetingItemService.checkMeetingIsNull(meetingItem);
    }

    @Test(expected = BadRequestException.class)
    public void testCheckMeetingItemIsNotNull(){
        /*
         * Test proverava ispravnost rada metode CheckMeetingItemIsNull servisa MeetingItemService
         * (proverava da li je tacka dnevnog reda dodeljena vec nekom sastanku)
         * u slucaju kada je zadata tacka dnevnog reda vec dodeljena nekom sastanku - ocekivano BadRequestException
         * */

        // tacka dnevnog reda sa ID-em 400 je dodeljena sastanku sa ID-em 100
        MeetingItem meetingItem = meetingItemService.findOne(400L);

        this.meetingItemService.checkMeetingIsNull(meetingItem);
    }

    @Test
    public void testFindOneByIdAndBillboard() {
        /*
         * Test proverava ispravnost rada metode findOneByIdAndBillboard servisa MeetingItemService
         * (pronalazenje konkretne tacke dnevnog reda na osnovu zadatog ID-a i ID-a bilborda zgrade kom tacka dnevnog reda pripada,
         * pri cemu ta tacka dnevnog reda nije obradjena jos ni na jednom sastanku skupstine)
         * u slucaju kada postoji tacka dnevnog reda sa zadatim ID-em na bilbordu sa zadatim ID-em i pri tome jos uvek nije
         * obradjena ni na jednom sastanku
         * */

        /*
        oglasnoj tabli sa ID-em = 100 pripada tacka dnevnog reda:
            ID = 100,
            content = First meeting item
            meeting_id = null
        */
        this.meetingItemService.findOneByIdAndBillboard(new Long(100), new Long(100));
    }

    @Test(expected = BadRequestException.class)
    public void testFindOneByIdAndBillboardNonexistent() {
        /*
         * Test proverava ispravnost rada metode findOneByIdAndBillboard servisa MeetingItemService
         * (pronalazenje konkretne tacke dnevnog reda na osnovu zadatog ID-a i ID-a bilborda zgrade kom tacka dnevnog reda pripada,
         * pri cemu ta tacka dnevnog reda nije obradjena jos ni na jednom sastanku skupstine)
         * u slucaju kada ne postoji tacka dnevnog reda sa zadatim ID-em na bilbordu sa zadatim ID-em - ocekivano BadRequestException
         * */

        // oglasnoj tabli sa ID-em = 100 ne pripada tacka dnevnog reda sa ID-em 400:
        this.meetingItemService.findOneByIdAndBillboard(new Long(400), new Long(100));
    }

    @Test(expected = BadRequestException.class)
    public void testFindOneByIdAndBillboardAlreadyProcessed() {
        /*
         * Test proverava ispravnost rada metode findOneByIdAndBillboard servisa MeetingItemService
         * (pronalazenje konkretne tacke dnevnog reda na osnovu zadatog ID-a i ID-a bilborda zgrade kom tacka dnevnog reda pripada,
         * pri cemu ta tacka dnevnog reda nije obradjena jos ni na jednom sastanku skupstine)
         * u slucaju kada postoji tacka dnevnog reda sa zadatim ID-em na bilbordu sa zadatim ID-em ali je vec obradjena na nekom sastanku
         * - ocekivano BadRequestException
         * */

        /*
        oglasnoj tabli sa ID-em = 200 pripada tacka dnevnog reda:
            ID = 400,
            content = Fourth meeting item,
            meeting_id = 100
        */
        this.meetingItemService.findOneByIdAndBillboard(new Long(400), new Long(200));
    }

    @Test
    public void testFindOne() {
        /*
         * Test proverava ispravnost metode findOne servisa MeetingItemService
         * (dobavljanje objekta tacke dnevnog reda na osnovu zadatog ID-a)
         * u slucaju kada postoji tacka dnevnog reda sa zadatim ID-em
         * */

        /*
        postoji tacka dnevnog reda sa ID-em = 100 i podacima:
            ID = 100,
            content = First meeting item
         */
        MeetingItem meetingItem = this.meetingItemService.findOne(new Long(100));

        assertNotNull(meetingItem);
        assertEquals(Long.valueOf(100), meetingItem.getId());
        assertEquals("First meeting item", meetingItem.getContent());
    }

    @Test(expected = NotFoundException.class)
    public void testFindOneNonexistent() {
        /*
         * Test proverava ispravnost metode findOne servisa MeetingItemService
         * (dobavljanje objekta tacke dnevnog reda na osnovu zadatog ID-a)
         * u slucaju kada ne postoji tacka dnevnog reda sa zadatim ID-em - ocekivano NotFoundException
         * */

        // ne postoji tacka dnevnog reda sa ID-em = 800
        MeetingItem meetingItem = this.meetingItemService.findOne(new Long(800));

        assertNull(meetingItem);
    }

    @Test
    public void testCheckMeetingItemQuestionnaire() {
        /*
         * Test proverava ispravnost metode checkMeetingItemQuestionnaire servisa MeetingItemService
         * (provera da li prosledjena tacka dnevnog reda sadrzi anketu ciji je id jednak prosledjenom ID-u)
         * u slucaju kada su ID ankete prosledjene tacke dnevnog reda i prosledjeni ID jednaki
         * */

        // postoji anketa sa ID-em = 100 koja pripada tacki dnevnog reda sa ID-em = 100
        MeetingItem meetingItem = this.meetingItemService.findOne(Long.valueOf(100));

        this.meetingItemService.checkMeetingItemQuestionnaire(meetingItem, 100L);
    }

    @Test(expected = BadRequestException.class)
    public void testCheckMeetingItemQuestionnaireInvalid() {
        /*
         * Test proverava ispravnost metode checkMeetingItemQuestionnaire servisa MeetingItemService
         * (provera da li prosledjena tacka dnevnog reda sadrzi anketu ciji je id jednak prosledjenom ID-u)
         * u slucaju kada ID ankete prosledjene tacke dnevnog reda i prosledjeni ID nisu jednaki - ocekivano BadRequestException
         * */

        // postoji anketa sa ID-em = 200 koja pripada tacki dnevnog reda sa ID-em = 200 (ne sa ID-em = 100)
        MeetingItem meetingItem = this.meetingItemService.findOne(Long.valueOf(200));

        this.meetingItemService.checkMeetingItemQuestionnaire(meetingItem, 100L);
    }
}
