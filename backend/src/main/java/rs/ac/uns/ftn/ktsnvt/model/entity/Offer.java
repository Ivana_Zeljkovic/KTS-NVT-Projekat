package rs.ac.uns.ftn.ktsnvt.model.entity;

import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * Created by Katarina Cukurov on 23/11/2017.
 */
@Entity
@Table(name = "offer")
@Where(clause="deleted=0")
public class Offer {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Column(nullable = false, columnDefinition = "INTEGER DEFAULT 0")
    @Version
    private int version;

    @Column(nullable = false, columnDefinition = "BOOL DEFAULT FALSE")
    private boolean deleted;

    @Column(nullable = false, columnDefinition = "BOOL DEFAULT FALSE")
    private boolean accepted;

    @Column(nullable = false)
    private float price;

    @OneToOne
    private Business business;

    @ManyToOne
    private Damage damage;

    public Offer() {}

    public Offer(boolean accepted, float price) {
        this.accepted = accepted;
        this.price = price;
    }

    public Long getId() {return id;}

    public void setId(Long id) {this.id = id;}

    public int getVersion() {return version;}

    public void setVersion(int version) {this.version = version;}

    public boolean isDeleted() { return deleted; }

    public void setDeleted(boolean deleted) { this.deleted = deleted; }

    public boolean isAccepted() {return accepted;}

    public void setAccepted(boolean accepted) {this.accepted = accepted;}

    public float getPrice() {return price;}

    public void setPrice(float price) {this.price = price;}

    public Business getBusiness() {return business;}

    public void setBusiness(Business business) {this.business = business;}

    public Damage getDamage() {
        return damage;
    }

    public void setDamage(Damage damage) {
        this.damage = damage;
    }
}
