import { Component, EventEmitter, Input, Output } from '@angular/core';
import { BsModalRef } from "ngx-bootstrap";
import { ToasterService } from "angular5-toaster/dist";
// model
import { RecordCreate } from "../../shared/models/record-create";
// service
import { RecordService } from "../../core/services/record.service";
import { JwtService } from "../../core/services/jwt.service";
// error
import { AppError } from "../../shared/errors/app-error";
import { NotFoundError } from "../../shared/errors/not-found-error";
import { ForbiddenError } from "../../shared/errors/forbidden-error";
import { BadRequestError } from "../../shared/errors/bad-request-error";


@Component({
  selector: 'app-record-create',
  templateUrl: './record-create.component.html',
  styleUrls: ['./record-create.component.css']
})
export class RecordCreateComponent {
  @Input() model: RecordCreate;
  @Input() meetingID: number;
  @Output() submitted = new EventEmitter<boolean>();
  disableSaveAndReset: boolean;

  constructor(public modalRef: BsModalRef, private recordService: RecordService,
              private jwtService: JwtService, private toasterService: ToasterService) {
    this.disableSaveAndReset = true;
  }

  onSubmit() {
    this.recordService.save(this.model, 'buildings', this.jwtService.getBuildingIdFromToken(),
      'council/meetings', this.meetingID, 'record')
      .subscribe(() => {
        this.submitted.emit(true);
        this.modalRef.hide();
      }, (error: AppError) => {
        if(error instanceof BadRequestError)
          this.toasterService.pop('error', 'Error', 'Selected meeting hasn\'t finished yet or ' +
            'the record for selected meeting has already been created!');
        else if(error instanceof ForbiddenError)
          this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action');
        else if(error instanceof NotFoundError)
          this.toasterService.pop('error', 'Error', 'Meeting not found!');
        else {
          this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
          throw error;
        }
      });
  }

  reset() {
    this.model.content = '';
    this.disableSaveAndReset = true;
  }

  changeExist() {
    this.disableSaveAndReset = (this.model.content === '');
  }
}
