import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { BsDatepickerConfig, BsLocaleService, defineLocale, enGb } from "ngx-bootstrap";
import { ToasterConfig, ToasterService } from "angular5-toaster/dist";
import { AgmMap } from "@agm/core";
// service
import { AuthService } from "../../core/services/auth.service";
import { TenantService } from "../../core/services/tenant.service";
import { JwtService } from "../../core/services/jwt.service";
import { LocationService } from "../../core/services/location.service";
// model
import { TenantProfile } from "./models/tenant-profile";
import { TenantUpdate } from "../../shared/models/tenant-update";
// validator
import { PasswordValidator } from "../../shared/validators/password.validator";
import { SpaceValidator } from "../../shared/validators/space.validator";
// error
import { BadRequestError } from "../../shared/errors/bad-request-error";
import { NotFoundError } from "../../shared/errors/not-found-error";
import { AppError } from "../../shared/errors/app-error";


@Component({
  selector: 'app-tenant-profile',
  templateUrl: './tenant-profile.component.html',
  styleUrls: ['./tenant-profile.component.css']
})
export class TenantProfileComponent implements OnInit {
  tenantData: TenantProfile;
  form: FormGroup;
  initialValues;
  disableSaveOrReset: boolean;
  bsConfig: Partial<BsDatepickerConfig>;
  maxDate: Date;
  longitude: number;
  latitude: number;
  zoom: number;
  @ViewChild(AgmMap) map: AgmMap;
  @Input() toasterConfig: ToasterConfig;

  constructor(private fb: FormBuilder, private passwordValidator: PasswordValidator,
              private jwtService: JwtService, private tenantService: TenantService,
              private localeService: BsLocaleService, private authService: AuthService,
              private locationService: LocationService, private toasterService: ToasterService) {
    this.bsConfig = Object.assign({}, {containerClass: 'theme-dark-blue'});
    this.maxDate = new Date();
    defineLocale('enGb', enGb);
    this.localeService.use('enGb');
    this.zoom = 14;
    // default coordinates if there are no coordinates for given address
    this.latitude = 45.2461;
    this.longitude = 19.8517;
  }

  ngOnInit() {
    this.authService.currentUser()
      .subscribe(data => {
        let tenant = (data as TenantProfile);
        this.tenantData = tenant;
        this.tenantData.birthDate = new Date(tenant.birthDate);

        // load coordinates for tenant address only if he/she is already connected to some apartment
        if(tenant.address) {
          this.locationService.getCoordinates(`${tenant.address.street} ${tenant.address.city.name}`)
            .subscribe(response => {
              if (response.status == 'OK') {
                this.latitude = response.results[0].geometry.location.lat;
                this.longitude = response.results[0].geometry.location.lng;
                this.map.triggerResize();
              }
              else if (response.status == 'ZERO_RESULTS')
                this.toasterService.pop('error', 'Error', 'Address couldn\'t be found!');
              else
                this.toasterService.pop('error', 'Error', 'Error in detecting coordinates of given address!');
            }, (error: AppError) => {
              this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
              throw error;
            });
        }

        this.form = this.fb.group({
          firstName: [this.tenantData.firstName, [
            Validators.required,
            Validators.minLength(2)
          ]],
          lastName: [this.tenantData.lastName, [
            Validators.required,
            Validators.minLength(2)
          ]],
          email: [this.tenantData.email, [
            Validators.required,
            Validators.email
          ]],
          phoneNumber: [this.tenantData.phoneNumber, [
            Validators.required,
            SpaceValidator.cannotContainSpace,
            Validators.pattern('[0-9]{6,10}')
          ]],
          birthDate: [this.tenantData.birthDate, [
            Validators.required
          ]],
          username: this.jwtService.getUsernameFromToken(),
          currentPassword: '',
          passwords: this.fb.group({
            password: '',
            confirmPassword: '',
          })
        });

        this.initialValues = this.form.value;
        this.disableSaveOrReset = true;

        this.form.valueChanges.subscribe(newValues => {
          // disable the save and reset button if there are no changes
          this.disableSaveOrReset = !this.changeExist(newValues);
        });

        this.password.valueChanges.subscribe((newValue) => {
          // if user write some value in new password field, fields current password and confirm password
          // become required too
          // else, all validators from all password fields are removed
          (newValue.trim() === '') ? this.removeValidators() : this.addValidators();
        });
      }, (error: AppError) => {
        throw error
      });
  }

  get firstName() {
    return this.form.get('firstName');
  }

  get lastName() {
    return this.form.get('lastName');
  }

  get email() {
    return this.form.get('email');
  }

  get phoneNumber() {
    return this.form.get('phoneNumber');
  }

  get birthDate() {
    return this.form.get('birthDate');
  }

  get username() {
    return this.form.get('username');
  }

  get currentPassword() {
    return this.form.get('currentPassword');
  }

  get passwords() {
    return this.form.get('passwords');
  }

  get password() {
    return this.form.get('passwords').get('password');
  }

  get confirmPassword() {
    return this.form.get('passwords').get('confirmPassword');
  }

  save() {
    if((this.currentPassword.value.trim() !== '' || this.confirmPassword.value.trim() !== '') && this.password.value === '')
      this.toasterService.pop('warning', 'Warning', 'New password value is missing!');
    else {
      let currentPasswordVal = null;
      let newPasswordVal = null;
      if (this.password.value !== '') {
        currentPasswordVal = this.currentPassword.value;
        newPasswordVal = this.password.value;
      }

      let tenantUpdate = new TenantUpdate(this.firstName.value,
        this.lastName.value, this.email.value, this.phoneNumber.value,
        new Date(this.birthDate.value), currentPasswordVal, newPasswordVal);

      this.tenantService.update(this.tenantData.id, tenantUpdate)
        .subscribe(() => {
          this.form.get('currentPassword').setValue('');
          this.form.get('passwords').get('password').setValue('');
          this.form.get('passwords').get('confirmPassword').setValue('');
          this.initialValues = this.form.value;
          this.disableSaveOrReset = true;
          this.toasterService.pop('success', '', 'Update of personal data successfully done!');
        }, (error: AppError) => {
          if (error instanceof BadRequestError)
            this.toasterService.pop('error', 'Error', 'Given data have bad format/Current password is invalid!');
          else if (error instanceof NotFoundError)
            this.toasterService.pop('error', 'Error', 'Wrong ID of tenant!');
          else {
            this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
            throw error;
          }
        });
    }
  }

  reset() {
    this.form.setValue(this.initialValues);
  }

  private changeExist(newValues): boolean {
    return !(JSON.stringify(newValues) === JSON.stringify(this.initialValues));
  }

  private addValidators() {
    if(this.password.value.length === 1) {
      this.password.setValidators([Validators.required, Validators.minLength(5)]);
      this.currentPassword.setValidators([Validators.required]);
      this.confirmPassword.setValidators([Validators.required]);
      this.passwords.setValidators([this.passwordValidator.matchPasswords.bind(this.passwordValidator)]);
      this.updateAndValidity();
    }
  }

  private removeValidators() {
    this.password.clearValidators();
    this.currentPassword.clearValidators();
    this.confirmPassword.clearValidators();
    this.passwords.clearValidators();
    this.updateAndValidity();
  }

  private updateAndValidity() {
    this.password.updateValueAndValidity({emitEvent : false});
    this.currentPassword.updateValueAndValidity();
    this.confirmPassword.updateValueAndValidity();
  }
}
