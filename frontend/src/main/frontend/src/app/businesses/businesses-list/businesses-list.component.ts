import {Component, OnInit} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {ToasterConfig, ToasterService} from 'angular5-toaster/dist';
import {Router} from '@angular/router';
// model
import {Business} from '../../shared/models/business';
import {BusinessService} from '../../core/services/business.service';
// component
import {ConfirmDeleteModalComponent} from '../../shared/components/confirm-delete-modal/confirm-delete-modal.component';
// error
import {AppError} from '../../shared/errors/app-error';
import {NotFoundError} from '../../shared/errors/not-found-error';
import {ForbiddenError} from '../../shared/errors/forbidden-error';
import {BadRequestError} from '../../shared/errors/bad-request-error';
import {PageAndSort} from '../../shared/models/page-and-sort';


@Component({
  selector: 'app-businesses-list',
  templateUrl: './businesses-list.component.html',
  styleUrls: ['./businesses-list.component.css']
})
export class BusinessesListComponent implements OnInit {
  institutionList: Array<Business>;
  firmsList: Array<Business>;
  modalRef: BsModalRef;
  toasterConfig: ToasterConfig;
  //Elements for sort
  sortListFirms: string[];
  selectedSortFirm: string;
  sortListInstitutions: string[];
  selectedSortInstitutions: string;
  //Elemnts for paging
  currentPage: number;
  currentPageInst: number;
  maxSize: number;
  maxSizeInst: number;
  itemsPerPage: number;
  itemsPerPageInst: number;
  pageSize: string;
  pageSizeInst: string;
  sortProperties: string[];
  sortPropertiesInst: string[];
  sortDirection: string;
  sortDirectionInst: string;
  totalItems: number;
  totalItemsInst: number;

  constructor(private businessService: BusinessService, private modalService: BsModalService,
              private toasterService: ToasterService, private router: Router) {
    this.toasterConfig = new ToasterConfig({
      timeout: {success: 2000, error: 4000, warning: 4000}
    });
    this.institutionList = new Array<Business>();
    this.firmsList = new Array<Business>();
    this.sortListFirms = ['Name', 'Email', 'Phone number', 'Business type'];
    this.selectedSortFirm = 'Name';
    this.sortListInstitutions = ['Name', 'Email', 'Phone number', 'Business type'];
    this.selectedSortInstitutions = 'Name';

    // Set up for bootstrap paging
    this.currentPage = 1;
    this.maxSize = 3;
    this.itemsPerPage = 4;
    this.currentPageInst = 1;
    this.maxSizeInst = 3;
    this.itemsPerPageInst = 4;

    // Set up for getting notification request
    this.pageSize = '4';
    this.sortDirection = 'ASC';
    this.sortProperties = ['name'];
    this.pageSizeInst = '4';
    this.sortDirectionInst = 'ASC';
    this.sortPropertiesInst = ['name'];

  }

  ngOnInit() {
    this.getFirms();
    this.getInstitutions();
  }


  getFirms(){
    let page = new PageAndSort((this.currentPage - 1).toString(), this.pageSize, this.sortDirection, this.sortProperties);
    this.businessService.getPage(page,'businesses/firms')
      .subscribe(data => {
        this.firmsList = data;
        if (data.length > 0)
          this.totalItems = data[0].numberOfElements;
        else this.totalItems = 0;
      }, (error: AppError) => {
        if (error instanceof BadRequestError)
          this.toasterService.pop('error', 'Error', 'Invalid format of given data!');
        else if (error instanceof NotFoundError)
          this.toasterService.pop('error', 'Error', 'Bad credentials!');
        else {
          this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
          throw error;
        }
      });
  }

  getInstitutions(){
    let page = new PageAndSort((this.currentPageInst - 1).toString(), this.pageSizeInst, this.sortDirectionInst, this.sortPropertiesInst);
    this.businessService.getPage(page,'businesses/institutions')
      .subscribe(data => {
        this.institutionList = data;
        if (data.length > 0)
          this.totalItemsInst = data[0].numberOfElements;
        else this.totalItemsInst = 0;
      }, (error: AppError) => {
        if (error instanceof BadRequestError)
          this.toasterService.pop('error', 'Error', 'Invalid format of given data!');
        else if (error instanceof NotFoundError)
          this.toasterService.pop('error', 'Error', 'Bad credentials!');
        else {
          this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
          throw error;
        }
      });
  }

  deleteInstitution(institution: Business) {
    this.modalRef = this.modalService.show(
      ConfirmDeleteModalComponent,
      Object.assign({}, {class: 'modal-sm'})
    );

    this.modalRef.content.confirmed
      .subscribe((value) => {
        if (value) {
          let index = this.institutionList.indexOf(institution, 0);
          this.institutionList.splice(index, 1);
          this.businessService.remove(institution.id, 'businesses')
            .subscribe(() => {
              this.toasterService.pop('success', 'Congratulation', 'Successfully deleted institution!');
              this.getInstitutions();
            }, (error: AppError) => {
              this.institutionList.splice(index, 0, institution);

              if (error instanceof NotFoundError)
                this.toasterService.pop('error', 'Error', 'Institution not found!');
              else if (error instanceof ForbiddenError)
                this.toasterService.pop('error', 'Error', 'You are not allowed to delete this institution!');
              else if (error instanceof BadRequestError)
                this.toasterService.pop('error', 'Error', 'This institution can\'t be deleted!');
              else {
                this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
                throw error;
              }
            });
        }
      });
  }

  deleteFirm(firm: Business) {
    this.modalRef = this.modalService.show(
      ConfirmDeleteModalComponent,
      Object.assign({}, {class: 'modal-sm'})
    );

    this.modalRef.content.confirmed
      .subscribe((value) => {
        if (value) {
          let index = this.firmsList.indexOf(firm, 0);
          this.firmsList.splice(index, 1);
          this.businessService.remove(firm.id, 'businesses')
            .subscribe(() => {
              this.toasterService.pop('success', 'Congratulation', 'Successfully deleted firm!');
              this.getFirms();
            }, (error: AppError) => {
              this.firmsList.splice(index, 0, firm);

              if (error instanceof NotFoundError)
                this.toasterService.pop('error', 'Error', 'Firm not found!');
              else if (error instanceof ForbiddenError)
                this.toasterService.pop('error', 'Error', 'You are not allowed to delete this firm!');
              else if (error instanceof BadRequestError)
                this.toasterService.pop('error', 'Error', 'This firm can\'t be deleted!');
              else {
                this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
                throw error;
              }
            });
        }
      });
  }

  confirmInstitution(institution: Business) {
    let index = this.institutionList.indexOf(institution, 0);
    institution.confirmed = true;
    this.institutionList.splice(index, 1, institution);
    this.businessService.updatePart({}, 'businesses', institution.id, 'confirm')
      .subscribe(data => {
        this.toasterService.pop('success', '', 'Institution confirmed!');
      }, (error: AppError) => {
        institution.confirmed = false;
        this.institutionList.splice(index, 1, institution);
        if (error instanceof NotFoundError)
          this.toasterService.pop('error', 'Error', 'Wrong ID of institution!');
        else {
          this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
          throw error;
        }
      });
  }

  confirmFirm(firm: Business) {
    let index = this.firmsList.indexOf(firm, 0);
    firm.confirmed = true;
    this.firmsList.splice(index, 1, firm);
    this.businessService.updatePart({}, 'businesses', firm.id, 'confirm')
      .subscribe(data => {
        this.toasterService.pop('success', '', 'Firm confirmed!');
      }, (error: AppError) => {
        firm.confirmed = false;
        this.firmsList.splice(index, 1, firm);
        if (error instanceof NotFoundError)
          this.toasterService.pop('error', 'Error', 'Wrong ID of firm!');
        else {
          this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
          throw error;
        }
      });
  }

  registerInstitution() {
    this.router.navigate(['/registration/institution']);
  }

  registerFirm() {
    this.router.navigate(['/registration/firm']);
  }

  moreDetails(business: Business) {
    this.router.navigate([`/businesses/${business.id}`]);
  }

  pageChangedInst(event: any) {
    this.currentPageInst = event.page;
    this.getInstitutions();
  }

  pageChanged(event: any) {
    this.currentPage = event.page;
    this.getFirms();
  }

  setSortFirm(sort: string) {
    this.selectedSortFirm = sort;
  }

  callSortFirm(){
    if(this.selectedSortFirm == 'Name')
      this.sortProperties = ['name'];
    else if(this.selectedSortFirm == 'Email')
      this.sortProperties = ['email'];
    else if(this.selectedSortFirm == 'Phone number')
      this.sortProperties = ['phoneNumber'];
    else if(this.selectedSortFirm == 'Business type')
      this.sortProperties = ['businessType'];

    this.getFirms();
  }

  setSortInst(sort: string) {
    this.selectedSortInstitutions = sort;
  }

  callSortInst(){
    if(this.selectedSortInstitutions == 'Name')
      this.sortPropertiesInst = ['name'];
    else if(this.selectedSortInstitutions == 'Email')
      this.sortPropertiesInst = ['email'];
    else if(this.selectedSortInstitutions == 'Phone number')
      this.sortPropertiesInst = ['phoneNumber'];
    else if(this.selectedSortInstitutions == 'Business type')
      this.sortPropertiesInst = ['businessType'];
    this.getInstitutions();
  }
}
