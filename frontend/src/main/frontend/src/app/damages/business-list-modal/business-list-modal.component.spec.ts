import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessListModalComponent } from './business-list-modal.component';

describe('BusinessListModalComponent', () => {
  let component: BusinessListModalComponent;
  let fixture: ComponentFixture<BusinessListModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessListModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessListModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
