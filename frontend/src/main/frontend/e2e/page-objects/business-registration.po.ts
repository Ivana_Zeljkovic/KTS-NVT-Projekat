import {by, element, ElementFinder} from "protractor";

export class BusinessRegistrationPage {
  // get methods
  getBusinessRegistrationElement(): ElementFinder {
    return element(by.tagName('app-business-registration'));
  }

  getPageTitle(): ElementFinder {
    return element(by.id('pageTitle'));
  }

  getNameInput(): ElementFinder {
    return element(by.id('name'));
  }

  getEmailInput(): ElementFinder {
    return element(by.id('email'));
  }

  getPhoneNumberInput(): ElementFinder {
    return element(by.id('phoneNumber'));
  }

  getBusinessTypeInput(): ElementFinder {
    return element(by.id('businessType'));
  }

  getStreetInput(): ElementFinder {
    return element(by.id('street'));
  }

  getNumberInput(): ElementFinder {
    return element(by.id('number'));
  }

  getCityInput(): ElementFinder {
    return element(by.id('city'));
  }

  getPostalCodeInput(): ElementFinder {
    return element(by.id('postalCode'));
  }

  getDescriptionInput(): ElementFinder {
    return element(by.id('description'));
  }

  getAddNewAccountButton(): ElementFinder {
    return element(by.buttonText('Add new account'));
  }

  getDeleteAccountButton(): ElementFinder {
    return element(by.buttonText('<i class="glyphicon glyphicon-trash"></i>'));
  }

  getRegisterButton(): ElementFinder {
    return element(by.buttonText('Register'));
  }

  getAccountsContainer(): ElementFinder {
    return element(by.id('accountsContainer'));
  }

  // set methods
  setName(text: string) {
    this.getNameInput().sendKeys(text);
  }

  setEmail(text: string) {
    this.getEmailInput().sendKeys(text);
  }

  setPhoneNumber(text: string) {
    this.getPhoneNumberInput().sendKeys(text);
  }

  setBusiessType(text: string) {
    this.getBusinessTypeInput().sendKeys(text);
  }

  setStreet(text: string) {
    this.getStreetInput().sendKeys(text);
  }

  setNumber(text: string) {
    this.getNumberInput().sendKeys(text);
  }

  setCity(text: string) {
    this.getCityInput().sendKeys(text);
  }

  setPostalCode(text: string) {
    this.getPostalCodeInput().sendKeys(text);
  }

  setDescription(text: string) {
    this.getDescriptionInput().sendKeys(text);
  }

  // get error methods
  getNameRequiredError(): ElementFinder {
    return element(by.id('nameRequiredError'));
  }

  getPanelHeading(): ElementFinder {
    return element(by.css('.panel-heading'));
  }

  getNameMinimumLengthError(): ElementFinder {
    return element(by.id('nameMinimumLengthError'));
  }

  getEmailRequiredError(): ElementFinder {
    return element(by.id('emailRequiredError'));
  }

  getValidEmailError(): ElementFinder {
    return element(by.id('validEmailError'));
  }

  getPhoneNumberRequiredError(): ElementFinder {
    return element(by.id('phoneNumberRequiredError'));
  }

  getPhoneNumberSpaceError(): ElementFinder {
    return element(by.id('phoneNumberSpaceError'));
  }

  getPhoneNumberLengthError(): ElementFinder {
    return element(by.id('phoneNumberLengthError'));
  }

  getStreetRequiredError(): ElementFinder {
    return element(by.id('streetRequiredError'));
  }

  getNumberSpaceError() : ElementFinder{
    return element(by.id('numberSpaceError'));
  }

  getStreetMinimumLegthError(): ElementFinder {
    return element(by.id('streetMinimumLegthError'));
  }

  getNumberRequiredError(): ElementFinder {
    return element(by.id('numberRequiredError'));
  }

  getCityRequiredError(): ElementFinder {
    return element(by.id('cityRequiredError'));
  }

  getPostalCodeRequiredError(): ElementFinder {
    return element(by.id('postalCodeRequiredError'));
  }

  getPostalCodeLengthError(): ElementFinder {
    return element(by.id('postalCodeLengthError'));
  }

  getDescriptionRequiredError(): ElementFinder {
    return element(by.id('descriptionRequiredError'));
  }

  getDescriptionMinimumLengthError(): ElementFinder {
    return element(by.id('descriptionMinimumLengthError'));
  }
}
