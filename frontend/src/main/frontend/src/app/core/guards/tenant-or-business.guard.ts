import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
import { Injectable } from "@angular/core";
import { JwtService } from "../services/jwt.service";

@Injectable()
export class TenantOrBusinessGuard implements CanActivate {

  constructor(private router: Router, private jwtService: JwtService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let roles: Array<string> = this.jwtService.getRolesFromToken();
    if((roles.indexOf('TENANT') != -1 && this.jwtService.getApartmentIdFromToken() !== -1) ||
      roles.indexOf('COMPANY') != -1 || roles.indexOf('INSTITUTION') != -1)
      return true;

    // logged user hasn't role TENANT, COMPANY or INSTITUTION; or logged user has role TENANT but he/she isn't
    // connected to any apartment already
    this.router.navigate(['home']);
    return false;
  }
}
