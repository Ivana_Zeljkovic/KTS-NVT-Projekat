package rs.ac.uns.ftn.ktsnvt.repository.unit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.model.entity.Address;
import rs.ac.uns.ftn.ktsnvt.model.entity.Business;
import rs.ac.uns.ftn.ktsnvt.model.entity.City;
import rs.ac.uns.ftn.ktsnvt.model.enumeration.BusinessType;
import rs.ac.uns.ftn.ktsnvt.repository.BusinessRepository;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Katarina Cukurov on 28/11/2017.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class BusinessRepositoryUnitTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private BusinessRepository businessRepository;

    private Long address_company_id;
    private Long address_institution_id;
    private Long company_id;
    private Long deleted_company_id;

    @Before
    public void setUp(){
        // CITY
        City city = new City("Novi Sad", 21000);
        entityManager.persist(city);

        // ADDRESS
        Address address_institution = new Address("Janka Veselinovica", 10, city);
        entityManager.persist(address_institution);
        this.address_institution_id = (Long)entityManager.persistAndGetId(address_institution);

        Address address_company = new Address("Janka Veselinovica", 8, city);
        entityManager.persist(address_company);
        this.address_company_id = (Long)entityManager.persistAndGetId(address_company);

        // BUSINESS
        Business company = new Business("First company", "First company description", "026548320",
          "ivana.zeljkovic995@gmail.com", BusinessType.FIRM_FOR_PLUMBING, true);
        company.setAddress(address_company);
        this.company_id = (Long)entityManager.persistAndGetId(company);
        Business institution = new Business("First institution", "First institution description", "02135458",
          "ivana.zeljkovic995@gmail.com", BusinessType.POLICE, false);
        institution.setAddress(address_institution);
        entityManager.persist(institution);
        Business deleted_company = new Business("Second company", "Second company description", "026548320",
            "ivana.zeljkovic995@gmail.com", BusinessType.FIRM_FOR_PLUMBING, true);
        deleted_company.setDeleted(true);
        deleted_company.setAddress(address_company);
        this.deleted_company_id = (Long)entityManager.persistAndGetId(deleted_company);
    }

    @Test
    public void testFindByAddressId(){
        /*
         * Test proverava ispravnost rada metode findByAddressId repozitorijuma BusinessRepository
         * (pronalazenje svih kompanija koji su u bazi na osnovu id-a adrese)
         * */

        /*
        postoji firma cija adresa ima ID = 103
            name = 'First company',
            description = 'First company description',
            phone_number = 026548320,
            email = ivana.zeljkovic995@gmail.com,
            business_type = 4 (FIRM_FOR_PLUMBING),
            is_company = true,
            address_id = address_company_id
        */
        Business business = this.businessRepository.findByAddressId(address_company_id, true);

        assertNotNull(business);
        assertEquals("First company description", business.getDescription());
        assertEquals("First company", business.getName());
        assertEquals("ivana.zeljkovic995@gmail.com", business.getEmail());
        assertEquals("026548320", business.getPhoneNumber());
    }

    @Test
    public void testFindAll(){
        /*
         * Test proverava ispravnost rada metode findAll repozitorijuma BusinessRepository
         * (pronalazenje svih firmi/institucija koje se nalaze u bazi)
         * */

        // postoji ukupno 2 firme/institucije
        List<Business> business = this.businessRepository.findAll();

        assertNotNull(business);
        assertEquals(3, business.size());
    }

    @Test
    public void testFindOne(){
        /*
         * Test proverava ispravnost rada metode findOne repozitorijuma BusinessRepository
         * (pronalazenje firme/institucije koje su u bazi na osnovu id-a)
         * */

        /*
        postoji firma:
            id = company_id,
            name = 'First company',
            description = 'First company description',
            phone_number = 026548320,
            email = ivana.zeljkovic995@gmail.com,
            business_type = 4 (FIRM_FOR_PLUMBING),
            is_company = true,
            address_id = address_id
        */
        Business business = this.businessRepository.findOne(company_id);

        assertNotNull(business);
        assertEquals("First company description", business.getDescription());
        assertEquals("First company", business.getName());
        assertEquals("ivana.zeljkovic995@gmail.com", business.getEmail());
    }

    @Test
    public void testFindOneInactive(){
        /*
         * Test proverava ispravnost rada metode findOneInactive repozitorijuma BusinessRepository
         * (pronalazenje firme/institucije koje su u bazi na osnovu id-a koja je logcki obrisana)
         * */

        /*
        postoji firma:
            id = deleted_company_id,
            name = 'Second company',
            description = 'Second company description',
            phone_number = 026548320,
            email = ivana.zeljkovic995@gmail.com,
            business_type = 4 (FIRM_FOR_PLUMBING),
            is_company = true,
            address_id = address_id
        */
        Business business = this.businessRepository.findOneInactive(deleted_company_id);

        assertNotNull(business);
        assertEquals("Second company description", business.getDescription());
        assertEquals("Second company", business.getName());
        assertEquals("ivana.zeljkovic995@gmail.com", business.getEmail());
    }
}
