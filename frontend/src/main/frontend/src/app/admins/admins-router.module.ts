import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";
// component
import { AdminsListComponent } from "./admins-list/admins-list.component";
import { AdminRegistrationComponent } from "./admin-registration/admin-registration.component";

const routes: Routes = [
  { path: '', component: AdminsListComponent },
  { path: 'register', component: AdminRegistrationComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class AdminsRouterModule { }
