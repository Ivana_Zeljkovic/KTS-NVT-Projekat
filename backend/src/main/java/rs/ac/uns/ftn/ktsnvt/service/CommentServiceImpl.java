package rs.ac.uns.ftn.ktsnvt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Comment;
import rs.ac.uns.ftn.ktsnvt.model.entity.Damage;
import rs.ac.uns.ftn.ktsnvt.repository.CommentRepository;

/**
 * Created by Nikola Garabandic on 24/11/2017.
 */
@Service
public class CommentServiceImpl implements CommentService{

    @Autowired
    private CommentRepository commentRepository;


    @Override
    @Transactional(readOnly = false)
    public Comment save(Comment comment) {
        return this.commentRepository.save(comment);
    }

    @Override
    @Transactional(readOnly = true)
    public Comment findOne(Long commentId) {
        Comment comment = this.commentRepository.findOne(commentId);
        if(comment == null) throw new NotFoundException("Comment not found!");
        return comment;
    }

    @Override
    public void belongToDamage(Long commentId, Damage damage) {
        boolean commentExists = damage.doesContainComment(commentId);
        if(!commentExists) throw new BadRequestException("Selected comment doesn't belong to selected damage!");
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Comment> getAllForDamage(Long damageId, Integer pageNumber, Integer pageSize,
                                     String sortDirection, String sortProperty) {
        return this.commentRepository.getAllByDamage(damageId,
                new PageRequest(pageNumber, pageSize, (sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortProperty));
    }

    @Override
    public void delete(Long id) {
        this.commentRepository.delete(id);
    }
}
