import {by, element, ElementFinder} from "protractor";

export class DamageRequestsPage {

  getDamageRequestsElement(): ElementFinder {
    return element(by.tagName('app-damage-requests'));
  }

  getDamageRequestsTable() : ElementFinder {
    return element(by.css('.panel.panel-default'));
  }

  getNoDamageRequests() : ElementFinder{
    return element(by.css('.no-damages'));
  }

  getFirstSendOfferAnchor() : ElementFinder{
    return element.all(by.css('.damage-menu a')).get(0);
  }

  getDamageDetailsAnchor() : ElementFinder{
    return element.all(by.css('.damage-menu a')).get(1);
  }

  getPopElement(){
    return element(by.css('.fa.fa-ellipsis-v'));
  }

}
