import { browser } from "protractor";
// page
import { NotificationListPage } from "../page-objects/notification-list.po";
import { MeetingItemListPage } from "../page-objects/meeting-item-list.po";
import { LoginPage } from "../../../page-objects/login.po";
import { TenantHomePage } from "../../../page-objects/tenant-home.po";


describe('Tenant home page', () => {
  let loginPage: LoginPage;
  let tenantHomePage: TenantHomePage;
  let notificationListPage: NotificationListPage;
  let meetingItemListPage: MeetingItemListPage;

  beforeAll(() => {
    loginPage = new LoginPage();
    tenantHomePage = new TenantHomePage();
    notificationListPage = new NotificationListPage();
    meetingItemListPage = new MeetingItemListPage();

    browser.get('/');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');

    loginPage.setUsername('kaca');
    loginPage.setPassword('kaca');
    loginPage.getSignInButton().click();

    browser.wait(function() {
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/home/notifications';
      });
    }).then(function() {
      // expect tenant home page is loaded
      expect(tenantHomePage.getTenantHomeElement().isDisplayed()).toBe(true);
    });
  });

  afterAll(() => {
    browser.executeScript('window.localStorage.clear();');
  });



  it('should show two tabs, first one with notification list and second one with meeting item list, where' +
    ' first one is initially active', () => {
    expect(tenantHomePage.getTabset().isDisplayed()).toBe(true);
    expect(tenantHomePage.getActiveNotificationsTabLink().isDisplayed()).toBe(true);
    expect(tenantHomePage.getActiveNotificationsTabLink().getText()).toEqual('Public notifications');
    expect(tenantHomePage.getMeetingItemsTabLink().isDisplayed()).toBe(true);
    expect(tenantHomePage.getMeetingItemsTabLink().getText()).toEqual('Suggestions of meeting-details item');
    expect(notificationListPage.getNotificationListElement().isDisplayed()).toBe(true);
  });


  it('should show two tabs, first one with notification list and second one with meeting item list, where' +
    ' second one is active when user click on second tab', () => {
      tenantHomePage.getMeetingItemsTabLink().click();

      browser.wait(function() {
        return browser.getCurrentUrl().then(value => {
          return value === 'http://localhost:49152/#/home/meeting-details-items';
        });
      }).then(function () {
        expect(tenantHomePage.getNotificationsTabLink().isDisplayed()).toBe(true);
        expect(tenantHomePage.getNotificationsTabLink().getText()).toEqual('Public notifications');
        expect(tenantHomePage.getActiveMeetingItemsTabLink().isDisplayed()).toBe(true);
        expect(tenantHomePage.getActiveMeetingItemsTabLink().getText()).toEqual('Suggestions of meeting-details item');
        expect(meetingItemListPage.getMeetingItemListElement().isDisplayed()).toBe(true);
      });
  });
});
