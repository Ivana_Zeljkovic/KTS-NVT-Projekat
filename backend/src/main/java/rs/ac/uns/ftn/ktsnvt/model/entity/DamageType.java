package rs.ac.uns.ftn.ktsnvt.model.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "damage_type")
public class DamageType {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Column(nullable = false)
    private String name;

    @OneToMany(mappedBy = "damageType", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<DamageResponsibility> damageResponsibilities;

    @OneToMany(mappedBy = "type", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private List<Damage> damages;

    public DamageType() {
        this.damageResponsibilities = new ArrayList<>();
        this.damages = new ArrayList<>();
    }

    public DamageType(String name) {
        this.name = name;
        this.damageResponsibilities = new ArrayList<>();
        this.damages = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<DamageResponsibility> getDamageResponsibilities() {
        return damageResponsibilities;
    }

    public void setDamageResponsibilities(List<DamageResponsibility> damageResponsibilities) {
        this.damageResponsibilities = damageResponsibilities;
    }

    public List<Damage> getDamages() {
        return damages;
    }

    public void setDamages(List<Damage> damages) {
        this.damages = damages;
    }
}
