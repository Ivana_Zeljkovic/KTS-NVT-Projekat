export class DateConverter {
  static convertStringToDate(date: string): Date {
    let tokensDateTime = date.split(' ');
    let dateTokens = tokensDateTime[0].split('/');

    // date string with format yyyy-mm-dd HH:mm:ss
    let dateWithNewFormat = `${dateTokens[2]}-${dateTokens[1]}-${dateTokens[0]} ${tokensDateTime[1]}`;
    return new Date(Date.parse(dateWithNewFormat));
  }

  static convertDateToString(value: Date): string {
    let day = value.getDate();
    let month = value.getMonth() + 1;
    let year = value.getFullYear();
    let hours = value.getHours();
    let minutes = value.getMinutes();
    let seconds = value.getSeconds();

    let dayString = (day <= 9) ? `0${day}` : `${day}`;
    let monthString = (month <= 9) ? `0${month}` : `${month}`;
    let hoursString = (hours <= 9) ? `0${hours}` : `${hours}`;
    let minutesString = (minutes <= 9) ? `0${minutes}` : `${minutes}`;
    let secondsString = (seconds <= 9) ? `0${seconds}` : `${seconds}`;

    return `${dayString}/${monthString}/${year} ${hoursString}:${minutesString}:${secondsString}`;
  }
}
