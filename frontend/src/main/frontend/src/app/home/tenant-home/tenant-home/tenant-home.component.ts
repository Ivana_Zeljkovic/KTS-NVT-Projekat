import { Component } from '@angular/core';
import { Router, NavigationEnd } from "@angular/router";
import { ToasterConfig } from "angular5-toaster/dist";


@Component({
  selector: 'app-tenant-home',
  templateUrl: './tenant-home.component.html',
  styleUrls: ['./tenant-home.component.css']
})
export class TenantHomeComponent {
  tabs: any[];
  activeFirstTab: boolean = true;
  toasterConfig: ToasterConfig;

  constructor(private router: Router) {
    this.tabs = [
      {
        title: 'Public notifications',
        active: true
      },
      {
        title: 'Suggestions of meeting-details item',
        active: false
      }
    ];
    this.setRoute(this.tabs[0]);

    this.router.events
      .filter(event => event instanceof NavigationEnd)
      .subscribe(event => {
        if((event as NavigationEnd).url === '/home/notifications' && !this.activeFirstTab) {
          this.tabs[0].active = true;
          this.tabs[1].active = false;
        }
        if((event as NavigationEnd).url === '/home/meeting-details-items' && this.activeFirstTab) {
          this.tabs[0].active = false;
          this.tabs[1].active = true;
        }
      });

    this.toasterConfig = new ToasterConfig({
      timeout: {error: 4000}
    });
  }

  setRoute(tab) {
    tab.active = true;
    if(tab.title === 'Public notifications') {
      this.activeFirstTab = true;
      this.router.navigate(['/home/notifications']);
    }
    else {
      this.activeFirstTab = false;
      this.router.navigate(['/home/meeting-details-items']);
    }
  }
}
