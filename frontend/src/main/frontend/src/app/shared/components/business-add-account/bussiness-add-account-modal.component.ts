import { Component, Input } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
// model
import { Login } from "../../models/login";
// validator
import { SpaceValidator } from "../../validators/space.validator";
import { UsernameUniqueValidator } from "../../validators/username-unique.validator";
import { PasswordValidator } from "../../validators/password.validator";


@Component({
  selector: 'modal-content',
  templateUrl: './business-add-account-modal.component.html',
  styleUrls: ['./business-add-account-modal.component.css']
})
export class BusinessAddAccountModalComponent {
  title: string;
  @Input() usersList: Login[] = [];
  form: FormGroup;
  bsModalRef: BsModalRef;

  constructor( private fb: FormBuilder, private usernameUniqueValidator: UsernameUniqueValidator,
              private passwordValidator: PasswordValidator) {
    this.form = this.fb.group({
      usernameModal: ['', [
        Validators.required,
        SpaceValidator.cannotContainSpace
        ],
        this.usernameUniqueValidator.isUsernameTaken.bind(this.usernameUniqueValidator)
      ],
      passwords: fb.group({
        password: ['', [
          Validators.required,
          Validators.minLength(5)
        ]],
        confirmPassword: ['',
          Validators.required
        ]
      }, {validator: this.passwordValidator.matchPasswords.bind(this.passwordValidator)}),
    });
  }

  get usernameModal() {
    return this.form.get('usernameModal');
  }

  get passwords() {
    return this.form.get('passwords');
  }

  get password() {
    return this.form.get('passwords').get('password');
  }

  get confirmPassword() {
    return this.form.get('passwords').get('confirmPassword');
  }

  usernameInList(): boolean {
    for (let i = 0; i < this.usersList.length; i++)
      if (this.usersList[i].username === this.usernameModal.value)
        return true;

    return false;
  }

  addAccount() {
    this.usersList.push(new Login(this.usernameModal.value, this.password.value));

    this.usernameModal.setValue('');
    this.password.setValue('');
    this.confirmPassword.setValue('');
    this.usernameModal.markAsUntouched();
    this.password.markAsUntouched();
    this.confirmPassword.markAsUntouched();

    this.form.updateValueAndValidity();
  }
}
