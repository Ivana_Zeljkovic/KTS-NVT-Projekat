package rs.ac.uns.ftn.ktsnvt.repository.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.model.entity.MeetingItem;
import rs.ac.uns.ftn.ktsnvt.repository.MeetingItemRepository;

import static org.junit.Assert.*;

import java.util.List;

/**
 * Created by Ivana Zeljkovic on 28/11/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class MeetingItemRepositoryIntegrationTest {

    @Autowired
    private MeetingItemRepository meetingItemRepository;


    @Test
    @Transactional
    public void testFindAllOnBillboard() {
        /*
         * Test proverava ispravnost rada metode findAllOnBillboard repozitorijuma MeetingItemRepository
         * (pronalazenje svih predloga tacaka dnevnog reda sa bilborda zgrade)
         * */

        // na bilbordu zgrade sa ID-em = 100 (bilboard sa ID-em = 100) nalazi se 1 jos uvek 'nepokupljen' predlog tacke dnevnog reda
        List<MeetingItem> meeting_items = this.meetingItemRepository.findAllOnBillboard(new Long(100));

        assertEquals(1, meeting_items.size());
        assertEquals(Long.valueOf(102), meeting_items.get(0).getId());
        assertEquals("Third meeting item content", meeting_items.get(0).getContent());
        assertEquals(Long.valueOf(102), meeting_items.get(0).getCreator().getId());
    }

    @Test
    @Transactional
    public void testFindOneByIdAndBillboard() {
        /*
         * Test proverava ispravnost rada metode findOneByIdAndBillboard repozitorijuma MeetingItemRepository
         * (pronalazenje konkretne tacke dnevnog reda na osnovu zadatog ID-a i ID-a bilborda zgrade kom tacka dnevnog reda pripada,
         * pri cemu ta tacka dnevnog reda nije obradjena jos ni na jednom sastanku skupstine)
         * u slucaju kada postoji tacka dnevnog reda sa zadatim ID-em na bilbordu sa zadatim ID-em i pri tome jos uvek nije
         * obradjena ni na jednom sastanku
         * */

        /*
        oglasnoj tabli zgrade sa ID-em = 100 (oglasna tabla sa ID-em = 100) pripada tacka dnevnog reda:
            ID = 102,
            ID kreatora = 102
            content = First meeting item
            meeting_id = null
        */
        MeetingItem meetingItem = this.meetingItemRepository.findOneByIdAndBillboard(new Long(102), new Long(100));

        assertNotNull(meetingItem);
        assertEquals(Long.valueOf(102), meetingItem.getId());
        assertEquals(Long.valueOf(102), meetingItem.getCreator().getId());
        assertEquals("Third meeting item content", meetingItem.getContent());
    }

    @Test
    @Transactional
    public void testFindOneByIdAndBillboardNonexistent() {
        /*
         * Test proverava ispravnost rada metode findOneByIdAndBillboard repozitorijuma MeetingItemRepository
         * (pronalazenje konkretne tacke dnevnog reda na osnovu zadatog ID-a i ID-a bilborda zgrade kom tacka dnevnog reda pripada,
         * pri cemu ta tacka dnevnog reda nije obradjena jos ni na jednom sastanku skupstine)
         * u slucaju kada ne postoji tacka dnevnog reda sa zadatim ID-em na bilbordu sa zadatim ID-em
         * */

        // oglasnoj tabli zgrade sa ID-em = 100 (oglasna tabla sa ID-em = 100) ne pripada tacka dnevnog reda sa ID-em 103:
        MeetingItem meetingItem = this.meetingItemRepository.findOneByIdAndBillboard(new Long(103), new Long(100));

        assertNull(meetingItem);
    }

    @Test
    @Transactional
    public void testFindOneByIdAndBillboardAlreadyProcessed() {
        /*
         * Test proverava ispravnost rada metode findOneByIdAndBillboard repozitorijuma MeetingItemRepository
         * (pronalazenje konkretne tacke dnevnog reda na osnovu zadatog ID-a i ID-a bilborda zgrade kom tacka dnevnog reda pripada,
         * pri cemu ta tacka dnevnog reda nije obradjena jos ni na jednom sastanku skupstine)
         * u slucaju kada postoji tacka dnevnog reda sa zadatim ID-em na bilbordu sa zadatim ID-em ali je vec obradjena na nekom sastanku
         * */

        /*
        oglasnoj tabli zgrade sa ID-em = 100 (oglasna tabla sa ID-em = 100) pripada tacka dnevnog reda:
            ID = 100,
            ID kreatora = 101
            content = First meeting item,
            meeting_id = 100
        */
        MeetingItem meetingItem = this.meetingItemRepository.findOneByIdAndBillboard(new Long(100), new Long(100));

        assertNull(meetingItem);
    }
}
