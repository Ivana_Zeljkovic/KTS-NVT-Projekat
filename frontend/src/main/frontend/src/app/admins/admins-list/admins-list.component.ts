import { Component, OnInit } from '@angular/core';
import { Router} from "@angular/router";
import { ToasterService } from "angular5-toaster/dist";
import { BsModalRef, BsModalService } from "ngx-bootstrap";
// model
import { Administrator } from "../../shared/models/administrator";
// service
import { AdminService } from "../../core/services/admin.service";
import { JwtService } from "../../core/services/jwt.service";
// component
import { ConfirmDeleteModalComponent } from "../../shared/components/confirm-delete-modal/confirm-delete-modal.component";
// error
import { AppError } from "../../shared/errors/app-error";
import { NotFoundError } from "../../shared/errors/not-found-error";
import { ForbiddenError } from "../../shared/errors/forbidden-error";
import { BadRequestError } from "../../shared/errors/bad-request-error";
import {PageAndSort} from "../../shared/models/page-and-sort";


@Component({
  selector: 'app-admins-list',
  templateUrl: './admins-list.component.html',
  styleUrls: ['./admins-list.component.css']
})
export class AdminsListComponent implements OnInit {
  adminList: Array<Administrator>;
  modalRef: BsModalRef;
  //Elements for sort
  sortList: string[];
  selectedSort: string;

  // Elements for bootstrap paging
  currentPage: number;
  totalItems: number;
  maxSize: number;
  itemsPerPage: number;

  // Elements for getting data from server
  pageSize: string;
  sortDirection: string;
  sortProperties: string[];

  constructor(private router: Router, private adminService: AdminService, private jwtService: JwtService,
              private toasterService: ToasterService, private modalService: BsModalService) {
    this.sortList = ['First name', 'Last name'];
    this.selectedSort = 'First name';

    // Sett up for bootstrap paging
    this.currentPage = 1;
    this.maxSize = 3;
    this.itemsPerPage = 4;

    // Sett up for getting damages request
    this.pageSize = '4';
    this.sortDirection = 'ASC';
    this.sortProperties = ['firstName'];
  }

  ngOnInit() {
    this.loadAdminsList();
  }

  private loadAdminsList(){
    let page = new PageAndSort((this.currentPage - 1).toString(), this.pageSize, this.sortDirection, this.sortProperties);
    this.adminService.getPage(page).subscribe(data => {
      this.adminList = data;
      if(data.length > 0)
        this.totalItems = data[0].numberOfElements;
      else
        this.totalItems = 0;
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Admins not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Error', 'Bad request!');
      else {
        this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
        throw error;
      }
    });
  }

  registerAdmin() {
    this.router.navigate(['/admins/register']);
  }

  deleteAdmin(admin: Administrator) {
    this.modalRef = this.modalService.show(
      ConfirmDeleteModalComponent,
      Object.assign({},{ class: 'modal-sm' })
    );

    this.modalRef.content.confirmed.subscribe((value) => {
      if(value) {
        let index = this.adminList.indexOf(admin, 0);
        this.adminList.splice(index, 1);
        this.adminService.remove(admin.id).subscribe(() => {
            this.toasterService.pop('success', 'Congratulation', 'Successfully deleted admin!');
            this.loadAdminsList();
          },
          (error: AppError) => {
            this.adminList.splice(index, 0, admin);

            if(error instanceof NotFoundError)
              this.toasterService.pop('error', 'Error', 'Admin not found!');
            else if(error instanceof ForbiddenError)
              this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
            else if(error instanceof BadRequestError)
              this.toasterService.pop('error', 'Error', 'Selected admin doesn\'t exist!');
            else {
              this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
              throw error;
            }
        });
      }
    });
  }

  setSort(sort: string) {
    this.selectedSort = sort;
  }

  callSort(){
    if(this.selectedSort == 'First name')
      this.sortProperties = ['firstName'];
    else if(this.selectedSort == 'Last name')
      this.sortProperties = ['lastName'];
    this.loadAdminsList();
  }

  pageChanged(event: any) {
    this.currentPage = event.page;
    this.loadAdminsList();
  }
}
