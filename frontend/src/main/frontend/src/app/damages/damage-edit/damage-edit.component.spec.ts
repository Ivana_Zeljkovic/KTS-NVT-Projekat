import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DamageEditComponent } from './damage-edit.component';

describe('DamageEditComponent', () => {
  let component: DamageEditComponent;
  let fixture: ComponentFixture<DamageEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DamageEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DamageEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
