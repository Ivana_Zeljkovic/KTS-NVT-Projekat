package rs.ac.uns.ftn.ktsnvt.repository.unit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.model.entity.Address;
import rs.ac.uns.ftn.ktsnvt.model.entity.Building;
import rs.ac.uns.ftn.ktsnvt.model.entity.City;
import rs.ac.uns.ftn.ktsnvt.repository.BuildingRepository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Katarina Cukurov on 28/11/2017.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class BuildingRepositoryUnitTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private BuildingRepository buildingRepository;

    private Long address_id, building_id;


    @Before
    public void setUp(){
        // CITY
        City city = new City("Novi Sad", 21000);
        entityManager.persist(city);

        // ADDRESS
        Address address = new Address("Janka Veselinovica", 10, city);
        entityManager.persist(address);
        this.address_id = (Long)entityManager.persistAndGetId(address);

        //BUILDING
        Building building  = new Building(4, true, address);
        this.building_id = (Long)entityManager.persistAndGetId(building);
    }

    @Test
    public void testFindByAddress_Id(){
       /*
        * Test proverava ispravnost rada metode findBAddressId repozitorijuma BuildingRepository
        * (pronalazenje zgrade na osnovu id-a)
        * */

        /*
        podaci koji postoje u bazi za zgradu sa id-em building_id:
            number_of_floors = 5,
            has_parking = true,
            address_id = address_id
        */
        Building building = this.buildingRepository.findByAddressId(address_id);

        assertNotNull(building);
        assertEquals(this.address_id, building.getAddress().getId());
        assertEquals(this.building_id, building.getId());
        assertEquals(4, building.getNumberOfFloors());
        assertTrue(building.isHasParking());
    }
}
