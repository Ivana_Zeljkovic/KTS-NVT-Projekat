package rs.ac.uns.ftn.ktsnvt.service;

import org.springframework.data.domain.Page;
import rs.ac.uns.ftn.ktsnvt.model.entity.Building;
import rs.ac.uns.ftn.ktsnvt.model.entity.Tenant;

import java.util.List;

/**
 * Created by Ivana Zeljkovic on 05/11/2017.
 */
public interface TenantService {
    Page<Tenant> findAll(Integer pageNumber, Integer pageSize, String sortDirection, String sortProperty);

    Page<Tenant> findAllByBuildingId(Long buildingId, Integer pageNumber, Integer pageSize,
                                     String sortDirection, String sortProperty);

    Page<Tenant> findAllByBuildingIdExceptCurrent(Long buildingId, Long currentTenantId, Integer pageNumber,
                                                  Integer pageSize, String sortDirection, String sortProperty);

    Page<Tenant> findAllCouncilMembers(Long buildingId, Long currentTenantId, Integer pageNumber, Integer pageSize,
                                       String sortDirection, String sortProperty);
    Page<Tenant> findAllInApartment(Long apartmentId, Long currentTenantId, Integer pageNumber, Integer pageSize,
                                    String sortDirection, String sortProperty);

    Page<Tenant> findAllInApartmentExceptPresident(Long apartmentId, Long currentTenantId, Long currentPresidentId,
                                                   Integer pageNumber, Integer pageSize,
                                                   String sortDirection, String sortProperty);

    Tenant findOne(Long id);

    Tenant save(Tenant tenant);

    void remove(Long id);

    boolean checkPasswordUpdate(Tenant tenant, String currentPassword, String newPassword);

    List<Tenant> findByNameOrUsername(String name, String lastName, String  username);

    List<Tenant> findTenantsByBuildingAndNameAndUsername(Long id, String name, String lastName, String username);

    void removePresidentRole(Tenant tenant);

    boolean checkTenantsPersonalApartments(Tenant tenant, Long buildingId);

    Tenant findOneByFirstNameAndLastNameAndUsername(String firstName, String lastName, String username);

    void checkPermissionForCurrentTenantInBuilding(Long buildingId);

    void checkPermissionForCurrentPresidentInBuilding(Building building);

    void checkPermissionForCurrentTenant(Long id);
}
