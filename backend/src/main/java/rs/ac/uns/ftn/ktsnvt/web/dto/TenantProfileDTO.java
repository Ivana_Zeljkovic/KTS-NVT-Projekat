package rs.ac.uns.ftn.ktsnvt.web.dto;

import rs.ac.uns.ftn.ktsnvt.model.entity.Apartment;
import rs.ac.uns.ftn.ktsnvt.model.entity.Tenant;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Ivana Zeljkovic on 02/01/2018.
 */
public class TenantProfileDTO implements Serializable {

    private Long id;
    private String firstName;
    private String lastName;
    private Date birthDate;
    private String email;
    private String phoneNumber;
    private AddressDTO address;
    private Integer apartmentNumber;

    public TenantProfileDTO() { }

    public TenantProfileDTO(Tenant tenant) {
        this.id = tenant.getId();
        this.firstName = tenant.getFirstName();
        this.lastName = tenant.getLastName();
        this.birthDate = tenant.getBirthDate();
        this.email = tenant.getEmail();
        this.phoneNumber = tenant.getPhoneNumber();
        Apartment apartment = tenant.getApartmentLiveIn();
        if(apartment != null) {
            this.address = new AddressDTO(apartment.getBuilding().getAddress());
            this.apartmentNumber = apartment.getNumber();
        }
        else {
            this.address = null;
            this.apartmentNumber = null;
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public AddressDTO getAddress() {
        return address;
    }

    public void setAddress(AddressDTO address) {
        this.address = address;
    }

    public Integer getApartmentNumber() {
        return apartmentNumber;
    }

    public void setApartmentNumber(Integer apartmentNumber) {
        this.apartmentNumber = apartmentNumber;
    }
}
