import { TestBed, inject } from '@angular/core/testing';

import { TenantService } from './tenant.service';
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {Tenant} from "../../shared/models/tenant";
import {PageAndSort} from "../../shared/models/page-and-sort";

describe('TenantService', () => {

  let tenantService: TenantService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule],
      providers: [TenantService]
    });
    tenantService = TestBed.get(TenantService);
    httpMock = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', inject([TenantService], (service: TenantService) => {
    expect(service).toBeTruthy();
  }));

  it('should successfully confirm tenant registration', () => {
    const response = {
      'id': 1,
      'firstName': 'Katarina',
      'lastName': 'Cukurov',
      'birthDate': new Date(),
      'email': 'kaca@gmail.com',
      'phoneNumber': '123456',
      'username': 'kaca',
      'confirmed': true,
      'address': {
        'street': 'Janka Veselinovica',
        'number': 18,
        'city': {
          'name': 'Novi Sad',
          'postalNumber': '21000'
        }
      }
    };
    tenantService.confirmRegistration(1)
      .subscribe((tenant: Tenant) => {
        expect(tenant).toEqual(response);
      });
    const httpRequest = httpMock.expectOne(
      '/api/tenants/1/confirm');
    expect(httpRequest.request.method).toEqual('PATCH');
    httpRequest.flush(response);
  });

  it('should get all tenants by username and name in building', () => {
    const responseList = [{
      'id': 1,
      'firstName': 'Katarina',
      'lastName': 'Cukurov',
      'birthDate': new Date(),
      'email': 'kaca@gmail.com',
      'phoneNumber': '123456',
      'username': 'kaca',
      'confirmed': true,
      'address': {
      'street': 'Janka Veselinovica',
        'number': 18,
        'city': {
        'name': 'Novi Sad',
          'postalNumber': '21000'
        }
      }
    }, {
      'id': 2,
      'firstName': 'Djulinka',
      'lastName': 'Cukurov',
      'birthDate': new Date(),
      'email': 'djudja@gmail.com',
      'phoneNumber': '123456',
      'username': 'djudja',
      'confirmed': true,
      'address': {
        'street': 'Janka Veselinovica',
        'number': 18,
        'city': {
          'name': 'Novi Sad',
          'postalNumber': '21000'
        }
      }
    }];
    tenantService.getAllByNameAndUsernameInMyBuilding('1', 'Katarina', 'Cukurov', 'kaca')
      .subscribe((tenants: Array<Tenant>) => {
        expect(tenants).toEqual(responseList);
        expect(tenants.length).toEqual(2);
      });
    const httpRequest = httpMock.expectOne(
      '/api/tenants/tenants_in_building?' +
      'apartmentId=1&' +
      'name=Katarina&' +
      'lastName=Cukurov&' +
      'username=kaca'
    );
    expect(httpRequest.request.method).toEqual('GET');
    expect(httpRequest.request.params.get('apartmentId')).toEqual('1');
    expect(httpRequest.request.params.get('name')).toEqual('Katarina');
    expect(httpRequest.request.params.get('lastName')).toEqual('Cukurov');
    expect(httpRequest.request.params.get('username')).toEqual('kaca');
    httpRequest.flush(responseList);
  });

  it('should get all tenants by username and name ', () => {
    const responseList = [{
      'id': 1,
      'firstName': 'Katarina',
      'lastName': 'Cukurov',
      'birthDate': new Date(),
      'email': 'kaca@gmail.com',
      'phoneNumber': '123456',
      'username': 'kaca',
      'confirmed': true,
      'address': {
        'street': 'Janka Veselinovica',
        'number': 18,
        'city': {
          'name': 'Novi Sad',
          'postalNumber': '21000'
        }
      }
    }, {
      'id': 2,
      'firstName': 'Djulinka',
      'lastName': 'Cukurov',
      'birthDate': new Date(),
      'email': 'djudja@gmail.com',
      'phoneNumber': '123456',
      'username': 'djudja',
      'confirmed': true,
      'address': {
        'street': 'Janka Veselinovica',
        'number': 18,
        'city': {
          'name': 'Novi Sad',
          'postalNumber': '21000'
        }
      }
    }];
    tenantService.getAllByNameOrUsername('Katarina', 'Cukurov', 'kaca',)
      .subscribe((tenants: Array<Tenant>) => {
        expect(tenants).toEqual(responseList);
        expect(tenants.length).toEqual(2);
      });
    const httpRequest = httpMock.expectOne(
      '/api/tenants/tenants_by_username?' +
      'name=Katarina&' +
      'lastName=Cukurov&' +
      'username=kaca'
    );
    expect(httpRequest.request.method).toEqual('GET');
    expect(httpRequest.request.params.get('name')).toEqual('Katarina');
    expect(httpRequest.request.params.get('lastName')).toEqual('Cukurov');
    expect(httpRequest.request.params.get('username')).toEqual('kaca');
    httpRequest.flush(responseList);
  });

  it('should get all tenants by building id ', () => {
    const responseList = [{
      'id': 1,
      'firstName': 'Katarina',
      'lastName': 'Cukurov',
      'birthDate': new Date(),
      'email': 'kaca@gmail.com',
      'phoneNumber': '123456',
      'username': 'kaca',
      'confirmed': true,
      'address': {
        'street': 'Janka Veselinovica',
        'number': 18,
        'city': {
          'name': 'Novi Sad',
          'postalNumber': '21000'
        }
      }
    }, {
      'id': 2,
      'firstName': 'Djulinka',
      'lastName': 'Cukurov',
      'birthDate': new Date(),
      'email': 'djudja@gmail.com',
      'phoneNumber': '123456',
      'username': 'djudja',
      'confirmed': true,
      'address': {
        'street': 'Janka Veselinovica',
        'number': 18,
        'city': {
          'name': 'Novi Sad',
          'postalNumber': '21000'
        }
      }
    }];
    let page = new PageAndSort('0', '2', 'ASC', ['id']);
    tenantService.getTenantsByBuildingId(page, '1')
      .subscribe((tenants: Array<Tenant>) => {
        expect(tenants).toEqual(responseList);
        expect(tenants.length).toEqual(2);
      });
    const httpRequest = httpMock.expectOne(
      '/api/tenants/tenants_by_building_id?' +
      'buildingId=1&' +
      'pageNumber=0&' +
      'pageSize=2&' +
      'sortDirection=ASC&' +
      'sortProperty=id');
    expect(httpRequest.request.method).toEqual('GET');
    expect(httpRequest.request.params.get('pageNumber')).toEqual('0');
    expect(httpRequest.request.params.get('pageSize')).toEqual('2');
    expect(httpRequest.request.params.get('sortDirection')).toEqual('ASC');
    expect(httpRequest.request.params.get('sortProperty')).toEqual('id');
    expect(httpRequest.request.params.get('buildingId')).toEqual('1');
    httpRequest.flush(responseList);
  });

  it('should get all council members ', () => {
    const responseList = [{
      'id': 1,
      'firstName': 'Katarina',
      'lastName': 'Cukurov',
      'birthDate': new Date(),
      'email': 'kaca@gmail.com',
      'phoneNumber': '123456',
      'username': 'kaca',
      'confirmed': true,
      'address': {
        'street': 'Janka Veselinovica',
        'number': 18,
        'city': {
          'name': 'Novi Sad',
          'postalNumber': '21000'
        }
      }
    }, {
      'id': 2,
      'firstName': 'Djulinka',
      'lastName': 'Cukurov',
      'birthDate': new Date(),
      'email': 'djudja@gmail.com',
      'phoneNumber': '123456',
      'username': 'djudja',
      'confirmed': true,
      'address': {
        'street': 'Janka Veselinovica',
        'number': 18,
        'city': {
          'name': 'Novi Sad',
          'postalNumber': '21000'
        }
      }
    }];
    let page = new PageAndSort('0', '2', 'ASC', ['id']);
    tenantService.getCouncilMembers(page, 1)
      .subscribe((tenants: Array<Tenant>) => {
        expect(tenants).toEqual(responseList);
        expect(tenants.length).toEqual(2);
      });
    const httpRequest = httpMock.expectOne(
      '/api/buildings/1/council/members?' +
      'pageNumber=0&' +
      'pageSize=2&' +
      'sortDirection=ASC&' +
      'sortProperty=id');
    expect(httpRequest.request.method).toEqual('GET');
    expect(httpRequest.request.params.get('pageNumber')).toEqual('0');
    expect(httpRequest.request.params.get('pageSize')).toEqual('2');
    expect(httpRequest.request.params.get('sortDirection')).toEqual('ASC');
    expect(httpRequest.request.params.get('sortProperty')).toEqual('id');
    httpRequest.flush(responseList);
  });
});
