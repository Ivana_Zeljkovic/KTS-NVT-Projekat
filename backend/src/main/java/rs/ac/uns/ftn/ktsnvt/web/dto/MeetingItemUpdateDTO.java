package rs.ac.uns.ftn.ktsnvt.web.dto;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by Ivana Zeljkovic on 06/12/2017.
 */
public class MeetingItemUpdateDTO implements Serializable {

    @NotNull
    @NotEmpty
    private String title;
    @NotNull
    @NotEmpty
    private String content;
    @Valid
    private QuestionnaireUpdateDTO questionnaireUpdate;
    private Long damageId;

    public MeetingItemUpdateDTO() { }

    public MeetingItemUpdateDTO(String title, String content, QuestionnaireUpdateDTO questionnaireUpdate, Long damageId) {
        this.title = title;
        this.content = content;
        this.questionnaireUpdate = questionnaireUpdate;
        this.damageId = damageId;
    }

    public String getTitle() { return title; }

    public void setTitle(String title) { this.title = title; }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public QuestionnaireUpdateDTO getQuestionnaireUpdate() {
        return questionnaireUpdate;
    }

    public void setQuestionnaireUpdate(QuestionnaireUpdateDTO questionnaireUpdate) {
        this.questionnaireUpdate = questionnaireUpdate;
    }

    public Long getDamageId() { return damageId; }

    public void setDamageId(Long damageId) { this.damageId = damageId; }
}
