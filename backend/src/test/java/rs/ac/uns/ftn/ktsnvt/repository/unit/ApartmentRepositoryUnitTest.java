package rs.ac.uns.ftn.ktsnvt.repository.unit;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.model.entity.Account;
import rs.ac.uns.ftn.ktsnvt.model.entity.Apartment;
import rs.ac.uns.ftn.ktsnvt.model.entity.Building;
import rs.ac.uns.ftn.ktsnvt.model.entity.Tenant;
import rs.ac.uns.ftn.ktsnvt.repository.ApartmentRepository;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Nikola Garabandic on 30/11/2017.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class ApartmentRepositoryUnitTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ApartmentRepository apartmentRepository;

    private Long apartment_id, building_id_1, building_id_2, account_id;

    private Pageable page;


    @Before
    public void setUp(){
        //BUILDINGS
        Building building = new Building();
        Building building2 = new Building();
        //APARTMENTS
        Apartment apartment1 = new Apartment(1, 1, 46);
        Apartment apartment2 = new Apartment(2, 1, 49);
        Apartment apartment3 = new Apartment(1, 1, 100);
        //SETTING APARTMENTS IN BUILDING AND VICE VERSA
        building.getApartments().add(apartment1);
        building.getApartments().add(apartment2);
        apartment1.setBuilding(building);
        apartment2.setBuilding(building);
        building2.getApartments().add(apartment3);
        apartment3.setBuilding(building2);

        //ACCOUNTS
        Account account1 = new Account("nikola", "nikola");
        Account account2 = new Account("ivana", "ivana");
        Account account3 = new Account("kaca", "kaca");
        //TENANTS
        Tenant tenant1 = new Tenant("Nikola", "Garabandic", new Date(), "kantagara@gmail.com", "06314153131", true, account1);
        Tenant tenant2 = new Tenant("Katarina", "Cukurov", new Date(), "kacac@gmail.com", "06314153131", true, account3);
        Tenant tenant3 = new Tenant("Ivana", "Zeljkovic", new Date(), "ivanaz@gmail.com", "06314153131", true, account2);

        //SETTING TENANTS APARTMENT WHERE THEY LIVE
        tenant1.setApartmentLiveIn(apartment1);
        tenant2.setApartmentLiveIn(apartment2);
        tenant3.setApartmentLiveIn(apartment1);

        //MAKING TENANTS OWNERS OF THE APARTMENTS (THEY DON'T NEED TO LIVE IN THE APARTMENT TO BE THE OWNER OF IT)
        apartment1.setOwner(tenant1);
        apartment2.setOwner(tenant1);

        this.building_id_1 =   entityManager.persistAndGetId(building, Long.class);
        this.building_id_2 =  entityManager.persistAndGetId(building2, Long.class);

        entityManager.persist(apartment1);
        entityManager.persist(apartment2);
        this.apartment_id = entityManager.persistAndGetId(apartment3, Long.class);

        entityManager.persist(account1);
        entityManager.persist(account2);
        entityManager.persist(account3);

        this.account_id = entityManager.persistAndGetId(tenant1, Long.class);
        entityManager.persist(tenant2);
        entityManager.persist(tenant3);

        this.page = new PageRequest(0, 10, Sort.Direction.ASC, "id");
    }

    @Test
    public void testFindFreeApartment(){
        /*
        * Test proverava ispravnost rada metode findFreeApartment repozitorijuma ApartmentRepository
        * (pronalazenje slobodnih stanova)
        */

        // postoji samo jedan stan koji je slobodan a to je stan apartment_3
        Page<Apartment> apartments = this.apartmentRepository.findFreeApartments(this.page);

        assertEquals(1, apartments.getContent().size());
    }

    @Test
    public void testFindFreeApartmentInBuilding(){
        /*
        * Test proverava ispravnost rada metode findFreeApartmentInBuilding repozitorijuma ApartmentRepository
        * (pronalazenje slobodnih stanova koji pripadaju zgradi sa zadatim ID-em)
        */

        // postoji samo jedan stan koji je slobodan u zgradi building2 a to je stan apartment_3
        Page<Apartment> apartments = this.apartmentRepository.findFreeApartmentsInBuilding(this.building_id_2, this.page);

        assertEquals(1, apartments.getContent().size());
    }

    @Test
    public void testFindNoneFreeApartmenstsInBuilding(){
        /*
        * Test proverava ispravnost rada metode FreeApartmentInBuilding repozitorijuma ApartmentRepository
        * (pronalazenje slobodnih stanova koji pripadaju zgradi sa zadatim ID-em)
        */

        // u zgradi building ne postoji nijedan slobodan stan
        Page<Apartment> apartments = this.apartmentRepository.findFreeApartmentsInBuilding(this.building_id_1, this.page);

        assertEquals(0, apartments.getContent().size());
    }


    @Test
    public void testFindFreeApartmentsBySize(){
        /*
        * Test proverava ispravnost rada metode findFreeApartmentsBySize repozitorijuma ApartmentRepository
        * (pronalazenje slobodnih stanova na osnovu kvadrature)
        */

        // postoji samo jedan stan koji je slobodan a koji pripada datom opsegu kvadrature, a to je stan apartment_3
        Page<Apartment> apartments = this.apartmentRepository.findFreeApartmentsBySize(20, 200, this.building_id_1, page);

        assertEquals(0, apartments.getContent().size());
    }

    @Test
    public void testFindNoneApartmentsBySize(){
        /*
        * Test proverava ispravnost rada metode findFreeApartmentsBySize repozitorijuma ApartmentRepository
        * (pronalazenje slobodnih stanova na osnovu kvadrature)
        */

        // ne postoji nijedan slobodan stan koji pripada datom opsegu kvadrature
        Page<Apartment> apartments = this.apartmentRepository.findFreeApartmentsBySize(30, 45, this.building_id_1, this.page);

        assertEquals(0, apartments.getContent().size());
    }

    @Test
    public void testCountApartmentsInBuilding(){
        /*
        * Test proverava ispravnost rada metode countApartmentsInBuilding repozitorijuma ApartmentRepository
        * (dobavljanje broja stanova u zgradi sa zadatim ID-em ciji je vlasnik stanar sa zadatim ID-em)
        */

        // u zgradi building postoje 2 stana ciji je vlasnik tenant1
        int count = this.apartmentRepository.countApartmentsInBuilding(account_id, this.building_id_1);

        assertEquals(2, count);
    }
}
