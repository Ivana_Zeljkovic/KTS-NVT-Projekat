package rs.ac.uns.ftn.ktsnvt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.ForbiddenException;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Administrator;
import rs.ac.uns.ftn.ktsnvt.repository.AdministratorRepository;

import java.util.List;

/**
 * Created by Ivana Zeljkovic on 05/11/2017.
 */
@Service
public class AdministratorServiceImpl implements AdministratorService {

    @Autowired
    private AdministratorRepository administratorRepository;

    @Autowired
    private AccountService accountService;


    @Override
    @Transactional(readOnly = false)
    public Administrator save(Administrator administrator) {
        return this.administratorRepository.save(administrator);
    }

    @Override
    @Transactional(readOnly = true)
    public Administrator findOne(Long id) {
        Administrator administrator = this.administratorRepository.findOne(id);
        if(administrator == null) throw new NotFoundException("Administrator not found!");
        return administrator;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Administrator> findAll() {
        return this.administratorRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Administrator> findAll(Pageable pageable){
        return this.administratorRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = false)
    public void remove(Long id) {
        this.administratorRepository.delete(id);
    }

    @Override
    public boolean checkPasswordUpdate(Administrator administrator, String currentPassword, String newPassword) {
        if(currentPassword == null && newPassword == null) return true;

        BCryptPasswordEncoder crypt = new BCryptPasswordEncoder();

        if(currentPassword == null) throw new BadRequestException("Current password value is missing!");
        if(newPassword == null) throw new BadRequestException("New password value is missing!");

        if(crypt.matches(currentPassword, administrator.getAccount().getPassword())) {
            administrator.getAccount().setPassword(crypt.encode(newPassword));
            return true;
        }

        throw new BadRequestException("Invalid value of current password!");
    }

    @Override
    @Transactional(readOnly = true)
    public void checkPermissionForCurrentAdmin(Long id) {
        Administrator currentAdmin = this.accountService.findByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()).getAdministrator();
        // trenutno logovani administrator nema ID id
        if(currentAdmin != null && currentAdmin.getId() != id)
            throw new ForbiddenException("You don't have permission for this action!");
        else if(currentAdmin == null)
            throw new NullPointerException("There is no administrator on session!");
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Administrator> getAllAdmins(Integer pageNumber, Integer pageSize,
                                              String sortDirection, String sortProperty) {
        return this.administratorRepository.findAll(new PageRequest(pageNumber, pageSize,
                (sortDirection.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC), sortProperty));
    }
}
