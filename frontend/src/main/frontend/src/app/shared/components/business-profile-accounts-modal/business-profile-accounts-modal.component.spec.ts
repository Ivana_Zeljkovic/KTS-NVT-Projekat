import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BusinessProfileAccountsModalComponent } from "./business-profile-accounts-modal.component";


describe('BusinessProfileAccountsModalComponent', () => {
  let component: BusinessProfileAccountsModalComponent;
  let fixture: ComponentFixture<BusinessProfileAccountsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessProfileAccountsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessProfileAccountsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
