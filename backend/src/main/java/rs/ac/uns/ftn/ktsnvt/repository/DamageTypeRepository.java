package rs.ac.uns.ftn.ktsnvt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import rs.ac.uns.ftn.ktsnvt.model.entity.DamageType;

/**
 * Created by Ivana Zeljkovic on 23/11/2017.
 */
public interface DamageTypeRepository extends JpaRepository<DamageType, Long> {

    @Query("SELECT d FROM DamageType d WHERE d.name = :name")
    DamageType findByName(@Param("name") String name);
}
