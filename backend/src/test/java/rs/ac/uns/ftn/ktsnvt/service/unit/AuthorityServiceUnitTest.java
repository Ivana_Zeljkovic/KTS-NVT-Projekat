package rs.ac.uns.ftn.ktsnvt.service.unit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Authority;
import rs.ac.uns.ftn.ktsnvt.repository.AuthorityRepository;
import rs.ac.uns.ftn.ktsnvt.service.AuthorityService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.BDDMockito.given;

/**
 * Created by Katarina Cukurov on 30/11/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AuthorityServiceUnitTest {

    @Autowired
    private AuthorityService authorityService;

    @MockBean
    private AuthorityRepository authorityRepository;


    @Before
    public void setUp(){
        Authority authority = new Authority("ADMIN");

        given(
                this.authorityRepository.findByName("ADMIN")
        ).willReturn(
                authority
        );

        given(
                this.authorityRepository.findByName("NIMDA")
        ).willReturn(
                null
        );
    }

    @Test
    public void testFindByName() {
        /*
         * Test proverava ispravnost rada metode findByName servisa AuthorityService
         * (dobavljanje autoriteta na osnovu naziva)
         * u slucaju kada postoji autoritet sa zadatim nazivom
         * */

        /*
        autoritet sa nazivom = ADMIN ima podatke:
            name = ADMIN
         */
        Authority authority = this.authorityService.findByName("ADMIN");

        assertNotNull(authority);
        assertEquals("ADMIN", authority.getName());
    }

    @Test(expected = NotFoundException.class)
    public void testFindByUsernameNonexistent() {
         /*
         * Test proverava ispravnost rada metode findByName servisa AuthorityService
         * (dobavljanje autoriteta na osnovu naziva)
         * u slucaju kada ne postoji autoritet sa zadatim nazivom - ocekivano NotFoundException
         * */

        // ne postoji authority sa imenom NIMDA;
        Authority authority = this.authorityService.findByName("NIMDA");
    }
}
