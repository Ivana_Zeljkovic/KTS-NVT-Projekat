package rs.ac.uns.ftn.ktsnvt.service.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.City;
import rs.ac.uns.ftn.ktsnvt.service.CityService;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Nikola Garabandic on 01/12/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class CityServiceIntegrationTest {

    @Autowired
    private CityService cityService;


    @Test
    public void findByNameAndPostalNumber(){
        //Id city-a sa imenom Novi Sad i postanskim brojem 21000 je 100
        City city = this.cityService.findByNameAndPostalNumber("Novi Sad", 21000);

        assertNotNull(city);
        assertEquals("Novi Sad", city.getName());
        assertEquals(21000, city.getPostalNumber());
    }

    @Test(expected = NotFoundException.class)
    public void findNoneByNameAndPostalNumber(){
        //Grad sa nazivom Zrenjani ne postoji u tabeli.
        City city = this.cityService.findByNameAndPostalNumber("Zrenjanin", 13000);
    }
}
