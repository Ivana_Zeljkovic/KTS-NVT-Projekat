package rs.ac.uns.ftn.ktsnvt.web.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Katarina Cukurov on 07/11/2017.
 */
public class BuildingCreateDTO implements Serializable {

    @NotNull
    private int numberOfFloors;
    @NotNull
    private boolean hasParking;
    @NotNull
    @Valid
    private AddressDTO address;
    @NotNull
    @Size(min = 1)
    private List<ApartmentCreateDTO> apartments;
    private Long firmManagerId;

    public BuildingCreateDTO(){}

    public BuildingCreateDTO(int numberOfFloors, boolean hasParking, AddressDTO address, List<ApartmentCreateDTO> apartments, Long firmManagerId) {
        this.numberOfFloors = numberOfFloors;
        this.hasParking = hasParking;
        this.address = address;
        this.apartments = apartments;
        this.firmManagerId = firmManagerId;
    }

    public int getNumberOfFloors() {
        return numberOfFloors;
    }

    public void setNumberOfFloors(int numberOfFloors) {
        this.numberOfFloors = numberOfFloors;
    }

    public boolean isHasParking() {
        return hasParking;
    }

    public void setHasParking(boolean hasParking) {
        this.hasParking = hasParking;
    }

    public AddressDTO getAddress() {
        return address;
    }

    public void setAddress(AddressDTO address) {
        this.address = address;
    }

    public List<ApartmentCreateDTO> getApartments() {
        return apartments;
    }

    public void setApartments(List<ApartmentCreateDTO> apartments) {
        this.apartments = apartments;
    }

    public Long getFirmManagerId() {
        return firmManagerId;
    }

    public void setFirmManagerId(Long firmManagerId) {
        this.firmManagerId = firmManagerId;
    }
}
