export class FloorConverter {
  static getFloor(number: number): string {
    let str = '';
    switch (number) {
      case 1: {
        str = 'First';
        break;
      }
      case 2: {
        str = 'Second';
        break;
      }
      case 3: {
        str = 'Third';
        break;
      }
      case 4: {
        str = 'Fourth';
        break;
      }
      case 5: {
        str = 'Fifth';
        break;
      }
      case 6: {
        str = 'Sixth';
        break;
      }
      case 7: {
        str = 'Seventh';
        break;
      }
      case 8: {
        str = 'Eight';
        break;
      }
      case 9: {
        str = 'Ninth';
        break;
      }
      case 10: {
        str = 'Tenth';
        break;
      }
    }
    return str;
  }
}
