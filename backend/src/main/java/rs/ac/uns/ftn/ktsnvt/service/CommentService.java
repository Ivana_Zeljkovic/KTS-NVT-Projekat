package rs.ac.uns.ftn.ktsnvt.service;

import org.springframework.data.domain.Page;
import rs.ac.uns.ftn.ktsnvt.model.entity.Comment;
import rs.ac.uns.ftn.ktsnvt.model.entity.Damage;

/**
 * Created by Nikola Garabandic on 24/11/2017.
 */
public interface CommentService {

    Comment save(Comment comment);

    Comment findOne(Long commentId);

    void belongToDamage(Long commentId, Damage damage);

    Page<Comment> getAllForDamage(Long damageId, Integer pageNumber, Integer pageSize,
                              String sortDirection, String sortProperty);

    void delete(Long id);
}
