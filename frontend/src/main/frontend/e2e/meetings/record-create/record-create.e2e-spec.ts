import { browser } from "protractor";
// page
import { LoginPage } from "../../page-objects/login.po";
import { MeetingListPage } from "../../page-objects/meeting-list.po";
import { RecordCreatePage } from "../../page-objects/record-create.po";


describe('Create record for finished meeting', () => {
  let loginPage: LoginPage;
  let meetingListPage: MeetingListPage;
  let recordCreatePage: RecordCreatePage;
  let numberOfFinishedMeeting: any;


  beforeAll(() => {
    loginPage = new LoginPage();
    meetingListPage = new MeetingListPage();
    recordCreatePage = new RecordCreatePage();
    numberOfFinishedMeeting = {'value': 0};

    browser.get('/');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');

    loginPage.setUsername('kaca');
    loginPage.setPassword('kaca');
    loginPage.getSignInButton().click();

    browser.wait(function() {
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/home/notifications';
      });
    }).then(function() {
      browser.get('http://localhost:49152/#/meetings');

      browser.wait(function() {
        return meetingListPage.getMeetingListElement().isDisplayed();
      }).then(function() {
        meetingListPage.getFinishedMeetingsList().isPresent().then(isPresent => {
          if(isPresent) {
            meetingListPage.getFinishedMeetingsTableRows().count().then(value => {
              expect(value).toBeGreaterThan(0);
              numberOfFinishedMeeting.value = value;

              meetingListPage.getFinishedMeetingMenu(0).click();

              browser.wait(function() {
                return meetingListPage.getContentOfFinishedMeetingMenu(0).isDisplayed();
              }).then(function() {
                expect(meetingListPage.getCreateRecordOptionFromFinishedMeetingMenu(0).isDisplayed()).toBe(true);
                meetingListPage.getCreateRecordOptionFromFinishedMeetingMenu(0).click();
              });
            });
          }
        });
      });
    });
  });

  afterAll(() => {
    browser.executeScript('window.localStorage.clear();');
  });



  it('should show all elements on page for create record when user click on Create record option ' +
    'in menu of finished meeting', () => {
    if(numberOfFinishedMeeting.value !== 0) {
      browser.wait(function () {
        return recordCreatePage.getRecordCreateElement().isDisplayed();
      }).then(function() {
        expect(recordCreatePage.getContentInput().isDisplayed()).toBe(true);
        expect(recordCreatePage.getContentInput().getAttribute('value')).toEqual('');
        expect(recordCreatePage.getSaveButton().isDisplayed()).toBe(true);
        expect(recordCreatePage.getSaveButton().isEnabled()).toBe(false);
        expect(recordCreatePage.getResetButton().isDisplayed()).toBe(true);
        expect(recordCreatePage.getResetButton().isEnabled()).toBe(false);
        expect(recordCreatePage.getCancelButton().isDisplayed()).toBe(true);
        expect(recordCreatePage.getCancelButton().isEnabled()).toBe(true);
      });
    }
  });



  it('should enable Reset button when user change value in Content field, reset form and disable Reset ' +
    'button when he click on Reset button, and show error message that content is required', () => {
    if(numberOfFinishedMeeting.value !== 0) {
      recordCreatePage.setContent('Some value');
      recordCreatePage.getTitle().click();

      expect(recordCreatePage.getResetButton().isEnabled()).toBe(true);

      recordCreatePage.getResetButton().click();
      expect(recordCreatePage.getResetButton().isEnabled()).toBe(false);
      expect(recordCreatePage.getContentInput().getAttribute('value')).toEqual('');
      expect(recordCreatePage.getContentRequiredError().isDisplayed()).toBe(true);
    }
  });



  it('enable Save button when user put some value in Content field, and close modal when user click on ' +
    'Cancel button', () => {
    if(numberOfFinishedMeeting.value !== 0) {
      recordCreatePage.setContent('Some new value');
      recordCreatePage.getTitle().click();
      expect(recordCreatePage.getSaveButton().isEnabled()).toBe(true);

      recordCreatePage.getCancelButton().click();

      meetingListPage = new MeetingListPage();
      meetingListPage.getFinishedMeetingMenu(0).click();

      browser.wait(function () {
        return meetingListPage.getContentOfFinishedMeetingMenu(0).isDisplayed();
      }).then(function () {
        expect(meetingListPage.getCreateRecordOptionFromFinishedMeetingMenu(0).isDisplayed()).toBe(true);
      });
    }
  });
});
