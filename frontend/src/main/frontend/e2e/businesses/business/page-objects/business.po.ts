import {by, element, ElementArrayFinder, ElementFinder} from "protractor";

export class BusinessPage {
  // get methods
  getPanelHeading(): ElementFinder {
    return element(by.className('panel-heading-location'));
  }

  getPanelBody(): ElementFinder {
    return element(by.className('panel-body'));
  }

  getMap(): ElementFinder {
    return element(by.tagName('agm-map'));
  }

  getBusinessTable(): ElementFinder {
    return element(by.id('businessTable'));
  }

  getName(): ElementFinder {
    return element(by.xpath("//tr[1]/td[2]"));
  }

  getDescription(): ElementFinder {
    return element(by.xpath("//tr[2]/td[2]"));
  }

  getEmail(): ElementFinder {
    return element(by.xpath("//tr[3]/td[2]"));
  }

  getPhoneNumber(): ElementFinder {
    return element(by.xpath("//tr[4]/td[2]"));
  }

  getBusinessType(): ElementFinder {
    return element(by.xpath("//tr[5]/td[2]"));
  }

  getCity(): ElementFinder {
    return element(by.xpath("//tr[6]/td[2]"));
  }

  getPostalNumber(): ElementFinder {
    return element(by.xpath("//tr[7]/td[2]"));
  }

  getStreet(): ElementFinder {
    return element(by.xpath("//tr[8]/td[2]"));
  }

  getNumber(): ElementFinder {
    return element(by.xpath("//tr[9]/td[2]"));
  }

}
