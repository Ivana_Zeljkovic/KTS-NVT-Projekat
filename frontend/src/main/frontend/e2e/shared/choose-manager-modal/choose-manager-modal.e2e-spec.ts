import {browser} from "protractor";
// page
import {LoginPage} from "../../page-objects/login.po";
import {BuildingListPage} from "../../buildings/building-list/page-objects/building-list.po";
import {BuildingRegistrationPage} from "../../buildings/building-registration/page-objects/building-registration.po";
import {ChooseManagerModalPage} from "./page-objects/choose-manager-modal.po";

describe('Choose manager modal', () => {
  let loginPage: LoginPage;
  let chooseManagerModalPage: ChooseManagerModalPage;
  let buildingsListPage: BuildingListPage;
  let buildingRegistrationPage: BuildingRegistrationPage;

  beforeEach(() => {
    loginPage = new LoginPage();
    chooseManagerModalPage = new ChooseManagerModalPage();
    buildingsListPage = new BuildingListPage();
    buildingRegistrationPage = new BuildingRegistrationPage();
    browser.get("/");

    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');
    loginPage.setUsername('ivana');
    loginPage.setPassword('ivana');
    loginPage.getSignInButton().click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/home');
    browser.get('http://localhost:49152/#/buildings');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/buildings');

    buildingsListPage.getAddButton().click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/buildings/register');
    buildingRegistrationPage.getDisplayAddManager().click();
  });

  afterEach(() => {
    browser.executeScript('window.localStorage.clear();');
  });

  it('should load all elements of modal', () => {
    expect(chooseManagerModalPage.getPageTitle().isDisplayed()).toBe(true);
    expect(chooseManagerModalPage.getBusinessTable().isDisplayed()).toBe(true);
    expect(chooseManagerModalPage.getPagination().isDisplayed()).toBe(true);
    expect(chooseManagerModalPage.getCloseButton().isDisplayed()).toBe(true);
  });

});
