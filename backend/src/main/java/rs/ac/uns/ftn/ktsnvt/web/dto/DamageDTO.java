package rs.ac.uns.ftn.ktsnvt.web.dto;

import rs.ac.uns.ftn.ktsnvt.model.entity.Damage;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Ivana Zeljkovic on 23/11/2017.
 */
public class DamageDTO implements Serializable {

    private Long id;
    private String description;
    private Date date;
    private String typeName;
    private Long apartmentId;
    private Long buildingId;
    private AddressDTO address;
    private Integer floorNumber;
    private Integer apartmentNumber;
    private boolean urgent;
    private boolean fixed;
    private String image;
    private TenantForDamageDTO responsiblePerson;
    private BusinessForDamageDTO responsibleInstitution;
    private BusinessForDamageDTO selectedBusiness;
    private TenantForDamageDTO creator;
    private long numberOfElements;

    public DamageDTO() { }

    public DamageDTO(Damage damage) {
        this.id = damage.getId();
        this.description = damage.getDescription();
        this.date = damage.getDate();
        this.typeName = damage.getType().getName();
        this.apartmentId = null;
        this.floorNumber = null;
        this.apartmentNumber = null;
        if(damage.getApartment() != null) {
            this.apartmentId = damage.getApartment().getId();
            this.floorNumber = damage.getApartment().getFloor();
            this.apartmentNumber = damage.getApartment().getNumber();
        }
        this.buildingId = damage.getBuilding().getId();
        this.address = new AddressDTO(damage.getBuilding().getAddress());
        this.urgent = damage.isUrgent();
        this.fixed = damage.isFixed();
        this.image = (damage.getImage() == null) ? null : damage.getImage();
        this.responsiblePerson = damage.getResponsiblePerson() == null ?
                null : new TenantForDamageDTO(damage.getResponsiblePerson());
        this.responsibleInstitution = damage.getResponsibleInstitution() ==
                null ? null : new BusinessForDamageDTO(damage.getResponsibleInstitution());
        this.selectedBusiness = damage.getSelectedBusiness() == null ?
                null : new BusinessForDamageDTO(damage.getSelectedBusiness());
        this.creator = new TenantForDamageDTO(damage.getCreator());
    }

    public DamageDTO(Damage damage, long numberOfElements) {
        this.id = damage.getId();
        this.description = damage.getDescription();
        this.date = damage.getDate();
        this.typeName = damage.getType().getName();
        this.apartmentId = null;
        this.floorNumber = null;
        this.apartmentNumber = null;
        if(damage.getApartment() != null) {
            this.apartmentId = damage.getApartment().getId();
            this.floorNumber = damage.getApartment().getFloor();
            this.apartmentNumber = damage.getApartment().getNumber();
        }
        this.buildingId = damage.getBuilding().getId();
        this.address = new AddressDTO(damage.getBuilding().getAddress());
        this.urgent = damage.isUrgent();
        this.fixed = damage.isFixed();
        this.image = (damage.getImage() == null) ? null : damage.getImage();
        this.responsiblePerson = damage.getResponsiblePerson() == null ?
                null : new TenantForDamageDTO(damage.getResponsiblePerson());
        this.responsibleInstitution = damage.getResponsibleInstitution() ==
                null ? null : new BusinessForDamageDTO(damage.getResponsibleInstitution());
        this.selectedBusiness = damage.getSelectedBusiness() == null ?
                null : new BusinessForDamageDTO(damage.getSelectedBusiness());
        this.creator = new TenantForDamageDTO(damage.getCreator());
        this.numberOfElements = numberOfElements;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public Long getApartmentId() {
        return apartmentId;
    }

    public void setApartmentId(Long apartmentId) {
        this.apartmentId = apartmentId;
    }

    public Long getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(Long buildingId) {
        this.buildingId = buildingId;
    }

    public AddressDTO getAddress() { return address; }

    public void setAddress(AddressDTO address) { this.address = address; }

    public Integer getFloorNumber() { return floorNumber; }

    public void setFloorNumber(Integer floorNumber) { this.floorNumber = floorNumber; }

    public Integer getApartmentNumber() { return apartmentNumber; }

    public void setApartmentNumber(Integer apartmentNumber) { this.apartmentNumber = apartmentNumber; }

    public boolean isUrgent() { return urgent; }

    public void setUrgent(boolean urgent) { this.urgent = urgent; }

    public boolean isFixed() { return fixed; }

    public void setFixed(boolean fixed) { this.fixed = fixed; }

    public String getImage() { return image; }

    public void setImage(String image) { this.image = image; }

    public TenantForDamageDTO getResponsiblePerson() { return responsiblePerson; }

    public void setResponsiblePerson(TenantForDamageDTO responsiblePerson) { this.responsiblePerson = responsiblePerson; }

    public BusinessForDamageDTO getResponsibleInstitution() { return responsibleInstitution; }

    public void setResponsibleInstitution(BusinessForDamageDTO responsibleInstitution) { this.responsibleInstitution = responsibleInstitution; }

    public BusinessForDamageDTO getSelectedBusiness() { return selectedBusiness; }

    public void setSelectedBusiness(BusinessForDamageDTO selectedBusiness) { this.selectedBusiness = selectedBusiness; }

    public TenantForDamageDTO getCreator() { return creator; }

    public void setCreator(TenantForDamageDTO creator) { this.creator = creator; }

    public long getNumberOfElements() { return numberOfElements; }

    public void setNumberOfElements(long numberOfElements) { this.numberOfElements = numberOfElements; }


}
