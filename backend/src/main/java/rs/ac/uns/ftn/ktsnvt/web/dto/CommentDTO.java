package rs.ac.uns.ftn.ktsnvt.web.dto;

import rs.ac.uns.ftn.ktsnvt.model.entity.Comment;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Ivana Zeljkovic on 13/01/2018.
 */
public class CommentDTO implements Serializable {

    private Long id;
    private String content;
    private String creatorUsername;
    private Date date;
    private long numberOfElements;

    public CommentDTO() { }

    public CommentDTO(Comment comment) {
        this.id = comment.getId();
        this.content = comment.getContent();
        this.creatorUsername = comment.getCreator().getUsername();
        this.date = comment.getDate();
    }

    public CommentDTO(Comment comment, long numberOfElements) {
        this.id = comment.getId();
        this.content = comment.getContent();
        this.creatorUsername = comment.getCreator().getUsername();
        this.date = comment.getDate();
        this.numberOfElements = numberOfElements;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreatorUsername() { return creatorUsername; }

    public void setCreatorUsername(String creatorUsername) { this.creatorUsername = creatorUsername; }

    public Date getDate() { return date; }

    public void setDate(Date date) { this.date = date; }

    public long getNumberOfElements() { return numberOfElements; }

    public void setNumberOfElements(long numberOfElements) {
        this.numberOfElements = numberOfElements;
    }
}
