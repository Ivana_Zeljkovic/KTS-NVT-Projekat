import { QuestionCreate } from "./question-create";

export class QuestionnaireCreate {
  constructor(public questions: Array<QuestionCreate>) { }
}
