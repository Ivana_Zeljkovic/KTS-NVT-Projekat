export interface Administrator {
  id: number;
  firstName: string;
  lastName: string;
  username: string;
  numberOfElements?: number;
}
