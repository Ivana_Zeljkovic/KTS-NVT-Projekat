package rs.ac.uns.ftn.ktsnvt.model.entity;

import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "billboard")
@Where(clause="deleted=0")
public class Billboard {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Column(nullable = false, columnDefinition = "INTEGER DEFAULT 0")
    @Version
    private int version;

    @Column(nullable = false, columnDefinition = "BOOL DEFAULT FALSE")
    private boolean deleted;

    @OneToMany(mappedBy = "billboard", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Notification> notifications;

    @OneToMany(mappedBy = "billboard", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<MeetingItem> meetingItems;

    public Billboard() {
        this.notifications = new ArrayList<>();
        this.meetingItems = new ArrayList<>();
    }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public int getVersion() { return version; }

    public void setVersion(int version) { this.version = version; }

    public boolean isDeleted() { return deleted; }

    public void setDeleted(boolean deleted) { this.deleted = deleted; }

    public List<Notification> getNotifications() { return notifications; }

    public void setNotifications(List<Notification> notifications) { this.notifications = notifications; }

    public List<MeetingItem> getMeetingItems() { return meetingItems; }

    public void setMeetingItems(List<MeetingItem> meetingItems) { this.meetingItems = meetingItems; }


}
