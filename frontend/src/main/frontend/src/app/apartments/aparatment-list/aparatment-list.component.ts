import { Component, OnInit } from '@angular/core';
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {Router} from "@angular/router";
import {ToasterService} from "angular5-toaster/dist";
// model
import {PageAndSort} from "../../shared/models/page-and-sort";
import {Apartment} from "../../shared/models/apartment";
// error
import {AppError} from "../../shared/errors/app-error";
import {NotFoundError} from "../../shared/errors/not-found-error";
import {ForbiddenError} from "../../shared/errors/forbidden-error";
import {BadRequestError} from "../../shared/errors/bad-request-error";
//service
import {JwtService} from "../../core/services/jwt.service";
import {ApartmentService} from "../../core/services/apartment.service";

@Component({
  selector: 'app-aparatment-list',
  templateUrl: './aparatment-list.component.html',
  styleUrls: ['./aparatment-list.component.css']
})
export class AparatmentListComponent implements OnInit {

  apartmentList: Array<Apartment>;
  modalRef: BsModalRef;
  //Elements for sort
  sortList: string[];
  selectedSort: string;
  filterList: string[];
  selectedFilter: string;
  filterOn: boolean;

  // Elements for bootstrap paging
  currentPage: number;
  totalItems: number;
  maxSize: number;
  itemsPerPage: number;

  // Elements for getting data from server
  pageSize: string;
  sortDirection: string;
  sortProperties: string[];

  constructor(private router: Router, private apartmentService: ApartmentService, private jwtService: JwtService,
              private toasterService: ToasterService, private modalService: BsModalService) {
    this.sortList = ['Building', 'Surface', 'Floor'];
    this.selectedSort = 'Building';
    this.filterList = ['None', 'Empty apartments'];
    this.selectedFilter = 'None';
    this.filterOn = false;

    // Sett up for bootstrap paging
    this.currentPage = 1;
    this.maxSize = 3;
    this.itemsPerPage = 10;

    // Sett up for getting damages request
    this.pageSize = '10';
    this.sortDirection = 'ASC';
    this.sortProperties = ['id'];
  }

  ngOnInit() {
    this.loadApartmentList();
  }

  loadApartmentList(){
    let page = new PageAndSort((this.currentPage - 1).toString(), this.pageSize, this.sortDirection, this.sortProperties);
    this.apartmentService.getPage(page).subscribe(data => {
      this.filterOn = false;
      this.apartmentList = data;
      if(data.length > 0)
        this.totalItems = data[0].numberOfElements;
      else
        this.totalItems = 0;
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Apartments not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Error', 'Bad request!');
      else {
        this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
        throw error;
      }
    });
  }

  setSort(sort: string) {
    this.selectedSort = sort;
  }

  callSort(){
    if(this.selectedSort == 'Building')
      this.sortProperties = ['building.id'];
    else if(this.selectedSort == 'Floor')
      this.sortProperties = ['floor'];
    else if(this.selectedSort == 'Surface')
      this.sortProperties = ['surface'];
    if(this.filterOn)
      this.getEmptyApartments();
    else
      this.loadApartmentList();
  }

  pageChanged(event: any) {
    this.currentPage = event.page;
    this.loadApartmentList();
  }

  setFilter(filter: string){
    this.selectedFilter = filter;
  }

  callFilter(){
    this.currentPage = 1;
    if(this.selectedFilter == 'Empty apartments')
      this.getEmptyApartments();
    else
      this.loadApartmentList();
  }

  getEmptyApartments(){
    let page = new PageAndSort((this.currentPage - 1).toString(), this.pageSize, this.sortDirection, this.sortProperties);
    this.apartmentService.getPage(page, 'apartments/free_apartments').subscribe(data =>{
      this.filterOn = true;
      this.apartmentList = data;
      if(data.length > 0)
        this.totalItems = data[0].numberOfElements;
      else
        this.totalItems = 0;
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Apartments not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Error', 'Bad request!');
      else {
        this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
        throw error;
      }
    });
  }

  moreDetails(apartment: Apartment) {
    this.router.navigate([`/apartments/${apartment.id}`]);
  }

}
