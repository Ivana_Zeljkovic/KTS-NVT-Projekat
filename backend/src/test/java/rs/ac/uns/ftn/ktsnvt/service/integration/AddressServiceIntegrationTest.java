package rs.ac.uns.ftn.ktsnvt.service.integration;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Address;
import rs.ac.uns.ftn.ktsnvt.service.AddressService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Nikola Garabandic on 01/12/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class AddressServiceIntegrationTest {

    @Autowired
    private AddressService addressService;


    @Test
    @Transactional
    public void testFindOne(){
        //adresa sa Id-em 100 ima podatke : street = Novi Sad, number = 21000, City: Name = Novi Sad postalNumber = 21000
        Address address = this.addressService.findOne(100L);

        assertNotNull(address);
        assertEquals("Janka Veselinovica", address.getStreet());
        assertEquals(8, address.getNumber());
        assertEquals("Novi Sad", address.getCity().getName());
        assertEquals(21000, address.getCity().getPostalNumber());
    }

    @Test(expected = NotFoundException.class)
    public void testFindOneNonexistent() {
        // Adresa sa id-em 1 ne postoji
        Address address = this.addressService.findOne(1L);
    }
}
