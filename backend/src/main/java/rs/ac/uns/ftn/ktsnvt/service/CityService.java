package rs.ac.uns.ftn.ktsnvt.service;

import rs.ac.uns.ftn.ktsnvt.model.entity.City;

/**
 * Created by Katarina Cukurov on 07/11/2017.
 */
public interface CityService {

    City findOne(Long id);

    void remove(Long id);

    City save(City city);

    City findByName(String name);

    City findByNameAndPostalNumber(String name, int postalNumber);
}
