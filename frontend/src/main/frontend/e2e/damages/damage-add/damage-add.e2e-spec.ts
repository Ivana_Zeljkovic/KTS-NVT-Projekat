import { browser } from "protractor";
// page
import { DamageListPage } from "../../page-objects/damage-list.po";
import { DamageAddPage } from "../page-objects/damage-add.po";
import { ChooseApartmentPage } from "../page-objects/choose-apartment.po";
import { LoginPage } from "../../page-objects/login.po";
import { NavbarPage } from "../../page-objects/navbar.po";


describe('Creating new damage', () => {
  let damageListPage: DamageListPage;
  let damageAddPage: DamageAddPage;
  let chooseApartmentPage: ChooseApartmentPage;
  let loginPage: LoginPage;
  let navbarPage: NavbarPage;

  beforeAll(() => {
    loginPage = new LoginPage();
    navbarPage = new NavbarPage();
    damageListPage = new DamageListPage();
    damageAddPage = new DamageAddPage();
    chooseApartmentPage = new ChooseApartmentPage();

    browser.get('/');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');

    loginPage.setUsername('kaca');
    loginPage.setPassword('kaca');
    loginPage.getSignInButton().click();

    browser.wait(function() {
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/home/notifications';
      });
    }).then(function() {
      expect(navbarPage.getNavbarElement().isDisplayed()).toBe(true);
      expect(navbarPage.getDamageDropdownLink().isDisplayed()).toBe(true);
      navbarPage.getDamageDropdownLink().click();

      browser.wait(function() {
        return navbarPage.getContentOfDamageDropdownLink().isDisplayed();
      });
    });
  });

  afterAll(() => {
    browser.executeScript('window.localStorage.clear();');
  });



  it('should open modal dialog for create new damage when user in on page with damages that are created ' +
    'by him/her, and click on button Add', () => {
    expect(navbarPage.getDamagesIReportedLink().isDisplayed()).toBe(true);

    navbarPage.getDamagesIReportedLink().click();

    browser.wait(function() {
      return damageListPage.getDamageListElement().isDisplayed();
    }).then(function() {
      //expect to see Add button for creating new damage
      expect(damageListPage.getAddButton().isDisplayed()).toBe(true);
      expect(damageListPage.getAddButton().isEnabled()).toBe(true);

      damageListPage.getAddButton().click();

      browser.wait(function() {
        return damageAddPage.getDamageAddElement().isDisplayed();
      }).then(function() {
        // expect to see all elements on page for creating new damage
        expect(damageAddPage.getTitle().isDisplayed()).toBe(true);
        expect(damageAddPage.getTitle().getText()).toEqual('Report damage');
        expect(damageAddPage.getDescriptionInput().isDisplayed()).toBe(true);
        expect(damageAddPage.getDescriptionInput().getAttribute('value')).toEqual('');
        expect(damageAddPage.getDamageTypeSelector().isDisplayed()).toBe(true);
        damageAddPage.getDamageTypeOptions().count().then(value => {
          // only 5 values for damage type: Other, Electrical, Gas, Exterior, Plumbing
          expect(value).toEqual(5);
        });
        expect(damageAddPage.getDamageUrgencySelector().isDisplayed()).toBe(true);
        damageAddPage.getDamageUrgencyOptions().count().then(value => {
          // only two values for urgency of damage: true and false
          expect(value).toEqual(2);
        });
        expect(damageAddPage.getDamageLocationSelector().isDisplayed()).toBe(true);
        damageAddPage.getDamageLocationOptions().count().then(value => {
          // only two values for damage location: in apartment and in building
          expect(value).toEqual(2);
        });
        expect(damageAddPage.getImageUploadInput().isDisplayed()).toBe(true);
        expect(damageAddPage.getSaveButton().isDisplayed()).toBe(true);
        expect(damageAddPage.getSaveButton().isEnabled()).toBe(false);
        expect(damageAddPage.getResetButton().isDisplayed()).toBe(true);
        expect(damageAddPage.getResetButton().isEnabled()).toBe(false);
        expect(damageAddPage.getCancelButton().isDisplayed()).toBe(true);
        expect(damageAddPage.getCancelButton().isEnabled()).toBe(true);
      });
    });
  });



  it('should show error message about required description when user put focus on this filed' +
    ' and leave it', () => {
    damageAddPage.setDamageDescription('');
    damageAddPage.getDamageUrgent(0).click();

    expect(damageAddPage.getDescriptionRequiredError().isDisplayed()).toBe(true);
  });



  it('should enable Reset button when user change some part of form and reset all initial values ' +
    'when user click on Reset button, what causes that Reset button is again in disabled state', () => {
    let initialValues = {'description':'', 'type':'', 'urgency':''};

    // store initial value of damage type
    damageAddPage.getDamageType(0).getAttribute('value').then(value => {
      initialValues.type = value;
    });
    // store initial value of damage urgency
    damageAddPage.getDamageUrgent(0).getAttribute('value').then(value => {
      initialValues.urgency = value;
    });
    // initial value for description is empty string

    damageAddPage.setDamageDescription('Some description');
    damageAddPage.getDamageType(2).click();
    damageAddPage.getDamageUrgent(1).click();

    expect(damageAddPage.getResetButton().isEnabled()).toBe(true);
    damageAddPage.getResetButton().click();

    // check all current values (they should be same as initial values)
    expect(damageAddPage.getDescriptionInput().getAttribute('value')).toEqual(initialValues.description);
    damageAddPage.getDamageTypeSelector().getAttribute('ng-reflect-model').then(value => {
      let damageType = value;
      if(value.includes('_'))
        damageType = value.split('_')[0];
      damageType = damageType.toLowerCase();
      expect(damageType).toEqual(initialValues.type.toLowerCase());
    });
    damageAddPage.getDamageUrgencySelector().getAttribute('ng-reflect-model').then(value => {
      expect(value).toEqual(initialValues.urgency);
    });
    expect(damageAddPage.getResetButton().isEnabled()).toBe(false);
  });



  it('should enable Save button after putting some value in Description input and again disable when ' +
    'user click to set location of damage to In apartment', () => {
    damageAddPage.setDamageDescription('Some description');
    damageAddPage.getDamageUrgent(1).click();
    expect(damageAddPage.getSaveButton().isEnabled()).toBe(true);

    // damage location select have two values, first one is In building, second one is In apartment,
    // but if user choose second one, he must choose apartment from modal and until he does that,
    // save button is disabled
    damageAddPage.getDamageLocation(1).click();
    expect(damageAddPage.getButtonChooseApartment().isDisplayed()).toBe(true);
    expect(damageAddPage.getButtonChooseApartment().isEnabled()).toBe(true);
    expect(damageAddPage.getSaveButton().isEnabled()).toBe(false);
  });



  it('should open modal dialog foor choosing apartment when user click on Choose button', () => {
    expect(damageAddPage.getButtonChooseApartment().isEnabled()).toBe(true);
    damageAddPage.getButtonChooseApartment().click();

    browser.wait(function() {
      return chooseApartmentPage.getChooseApartmentElement().isDisplayed();
    }).then(function() {
      expect(chooseApartmentPage.getTitle().isDisplayed()).toBe(true);
      expect(chooseApartmentPage.getCloseButton().isDisplayed()).toBe(true);
      expect(chooseApartmentPage.getCloseButton().isEnabled()).toBe(true);

      chooseApartmentPage.getCloseButton().click();
    });
  });
});
