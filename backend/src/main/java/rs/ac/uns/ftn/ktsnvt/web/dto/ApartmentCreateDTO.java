package rs.ac.uns.ftn.ktsnvt.web.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by Katarina Cukurov on 14/11/2017.
 */
public class ApartmentCreateDTO implements Serializable {

    @NotNull
    private int number;
    @NotNull
    private int floor;
    @NotNull
    private int surface;

    public ApartmentCreateDTO() {}

    public ApartmentCreateDTO(int number, int floor, int surface) {
        this.number = number;
        this.floor = floor;
        this.surface = surface;
    }

    public int getNumber() {return number;}

    public void setNumber(int number) {this.number = number;}

    public int getFloor() {return floor;}

    public void setFloor(int floor) {this.floor = floor;}

    public int getSurface() {return surface;}

    public void setSurface(int surface) {this.surface = surface;}
}
