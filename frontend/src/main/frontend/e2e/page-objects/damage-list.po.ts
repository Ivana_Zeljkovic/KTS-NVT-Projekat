import {by, element, ElementArrayFinder, ElementFinder} from "protractor";

export class DamageListPage {
  // get methods
  getDamageListElement(): ElementFinder {
    return element(by.tagName('app-damage-list'));
  }

  getTitle(): ElementFinder {
    return element(by.css('div.row-title > h3'));
  }

  getSearchDateRangeInput(): ElementFinder {
    return element(by.id('fromToDate'));
  }

  getSearchDateRangeClearButton(): ElementFinder {
    return element(by.buttonText('Clear'));
  }

  getSearchDateRangeSearchButton(): ElementFinder {
    return element(by.buttonText('Search'));
  }

  getAddButton(): ElementFinder {
    return element(by.buttonText('Add'));
  }

  getDamageTypesResponsibilityOption(): ElementFinder {
    return element(by.css('app-damage-list div.div-responsibility-option > a'));
  }

  getDamageList(): ElementFinder {
    return element(by.id('damageList'));
  }

  getMessageNoDamageInList(): ElementFinder {
    return element(by.id('messageEmptyList'));
  }

  getPanelsFromDamageList(): ElementArrayFinder {
    return element.all(by.css('div.panel'));
  }

  getPaginatorInDamageList(): ElementFinder {
    return element(by.tagName('pagination'));
  }

  getCreatorNameFromDamagePanel(index: number): ElementFinder {
    return element(by.css(`#damageList > .panel:nth-child(${index+1}) .damage-metadata span:nth-child(2)`));
  }

  getDateFromDamagePanel(index: number): ElementFinder {
    return element(by.css(`#damageList > .panel:nth-child(${index+1}) .damage-metadata span:nth-child(4)`));
  }

  getMenuFromDamagePanel(index: number): ElementFinder {
    return element(by.css(`#damageList > .panel:nth-child(${index+1}) .damage-menu`));
  }

  getContentOfMenuFromDamagePanel(index: number): ElementFinder {
    return element(by.css(`#damageList > .panel:nth-child(${index+1}) .damage-menu #damagePopover`));
  }

  getMenuOptionsFromDamagePanel(): ElementArrayFinder {
    return element.all(by.css('.damage-menu div.ng-star-inserted > a'));
  }

  getDamageDetailsLinkFromDamagePanel(index: number): ElementFinder {
    return element(by.css(`#damageList > .panel:nth-child(${index+1}) .damage-menu #damagePopover > div:nth-child(1) > a`));
  }

  getDeclareDamageFixedLinkFromDamagePanel(index: number): ElementFinder {
    return element(by.css(`#damageList > .panel:nth-child(${index+1}) .damage-menu #damagePopover > div:nth-child(2) > a`));
  }

  getLinkFromDamagePanel(index: number, order: number): ElementFinder {
    return element(by.css(`#damageList > .panel:nth-child(${index+1}) .damage-menu #damagePopover > div:nth-child(${order}) > a`));
  }

  getDamageTypeFromDamagePanel(index: number): ElementFinder {
    return element((by.css(`#damageList > .panel:nth-child(${index+1}) div.panel-body > p.damage-type-paragraph`)));
  }

  getDamageDescriptionFromDamagePanel(index: number): ElementFinder {
    return element(by.css(`#damageList > .panel:nth-child(${index+1}) div.panel-body > p:nth-child(3)`));
  }

  getLabelUrgentFromDamagePanel(index: number): ElementFinder {
    return element(by.css(`#damageList > .panel:nth-child(${index+1}) div.panel-body > div.label-urgent`));
  }

  getLabelInFixingProcessFromDamagePanel(index: number): ElementFinder {
    return element(by.css(`#damageList > .panel:nth-child(${index+1}) div.panel-body > div.label-fixing`));
  }

  getSendRequestForOffer() : ElementFinder{
    return element(by.id('sendRequest'))
  }

  getChooseBusiness() : ElementFinder{
    return element(by.id('selectBusiness'))
  }
}
