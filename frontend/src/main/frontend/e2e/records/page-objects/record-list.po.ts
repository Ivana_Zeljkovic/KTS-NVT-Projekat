import {by, element, ElementArrayFinder, ElementFinder} from "protractor";

export class RecordListPage {
  // get methods
  getRecordListElement(): ElementFinder {
    return element(by.tagName('app-record-list'));
  }

  getTitle(): ElementFinder {
    return element(by.css('.row-title'));
  }

  getRecords(): ElementArrayFinder {
    return element.all(by.css('.record-list .panel'));
  }

  getMeetingDateFromRecord(index: number): ElementFinder {
    return element(by.css(`.record-list .panel:nth-child(${index+1}) .record-metadata > span`));
  }

  getMenuFromRecord(index: number): ElementFinder {
    return element(by.css(`.record-list .panel:nth-child(${index+1}) .record-menu > span`));
  }

  getContentOfMenuFromRecord(index: number): ElementFinder {
    return element(by.css(`.record-list .panel:nth-child(${index+1}) .record-menu #recordPopover`));
  }

  getDetailsOptionFromMenu(index: number): ElementFinder {
    return element(by.css(`.record-list .panel:nth-child(${index+1}) .record-menu #recordDetails`));
  }

  getContentFromRecord(index: number): ElementFinder {
    return element(by.css(`.record-list .panel:nth-child(${index+1}) .panel-body`))
  }
}
