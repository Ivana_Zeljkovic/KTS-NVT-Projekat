import { CityCreate } from "./city-create";

export class AddressCreate {
  constructor(private street: string,
              private number: number,
              private city: CityCreate){ }
}
