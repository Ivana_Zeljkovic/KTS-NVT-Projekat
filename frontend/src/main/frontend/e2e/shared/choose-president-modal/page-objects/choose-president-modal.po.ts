import {by, element, ElementFinder} from "protractor";

export class ChoosePresidentModalPage {
  // get methods
  getPageTitle(): ElementFinder {
    return element(by.tagName('h4'));
  }

  getTenantsTable(): ElementFinder {
    return element(by.id('presidentTable'));
  }

  getPagination(): ElementFinder {
    return element(by.id('pagination'));
  }

  getNoElements(): ElementFinder {
    return element(by.tagName('h3'));
  }

  getCloseButton(): ElementFinder {
    return element(by.id('closeButton'));
  }
}
