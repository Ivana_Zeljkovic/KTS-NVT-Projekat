import {browser, by, element, ElementFinder} from "protractor";
// page
import {AdminsListPage} from "./page-objects/admins-list.po";
import {LoginPage} from "../../page-objects/login.po";
import {AdminRegistrationPage} from "../admin-registration/page-objects/admin-registration.po";
import {ConfirmDeletePage} from "../../page-objects/confirm-delete.po";

describe('Admins list view', () => {
  let loginPage: LoginPage;
  let adminsListPage: AdminsListPage;
  let adminRegistrationPage: AdminRegistrationPage;
  let confirmDeletePage: ConfirmDeletePage;
  let newAdmin: any;
  let empty: any;

  beforeEach(() => {
    loginPage = new LoginPage();
    adminsListPage = new AdminsListPage();
    confirmDeletePage = new ConfirmDeletePage();
    browser.get("/");

    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');

    newAdmin = {
      'firstName': 'Aaaaaa',
      'lastName': 'Aaaaaa',
      'username': 'Aaaaaa',
      'pass': 'Aaaaaa',
      'pass2': 'Aaaaaa'
    }
    empty = {
      'empty': ''
    }
  });

  afterEach(() => {
    browser.executeScript('window.localStorage.clear();');
  });

  it("should successfully load page and all of its components", () => {
    loginPage.setUsername('ivana');
    loginPage.setPassword('ivana');
    loginPage.getSignInButton().submit();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/home');
    browser.get("http://localhost:49152/#/admins");
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/admins');

    expect(adminsListPage.getPageTitle().isDisplayed()).toBe(true);
    expect(adminsListPage.getDropdownToggle().isDisplayed()).toBe(true);
    expect(adminsListPage.getAddButton().isDisplayed()).toBe(true);
    expect(adminsListPage.getSortButton().isDisplayed()).toBe(true);
    expect(adminsListPage.getAdminsTable().isDisplayed()).toBe(true);
    expect(adminsListPage.getPagination().isDisplayed()).toBe(true);

  });

  it("should be sorted by first name when loaded", () => {
    loginPage.setUsername('ivana');
    loginPage.setPassword('ivana');
    loginPage.getSignInButton().submit();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/home');
    browser.get("http://localhost:49152/#/admins");
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/admins');

    let firstNameFirstRow = element(by.xpath("//tr[first()]/td[1]"));
    let firstNameLastRow = element(by.xpath("//tr[last()]/td[1]"));
    expect(firstNameFirstRow < firstNameLastRow);
  });

  it("should sort admins by last name", () => {
    loginPage.setUsername('ivana');
    loginPage.setPassword('ivana');
    loginPage.getSignInButton().submit();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/home');
    browser.get("http://localhost:49152/#/admins");
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/admins');

    adminsListPage.getDropdownToggle().click();
    element(by.css('.dropdown-menu li:nth-child(2)')).click();
    expect(adminsListPage.getDropdownToggle().getText()).toBe("Last name");
    adminsListPage.getSortButton().click();
    let lastNameFirstRow = element(by.xpath("//tr[first()]/td[2]"));
    let lastNameLastRow = element(by.xpath("//tr[last()]/td[2]"));
    expect(lastNameFirstRow < lastNameLastRow);

  });

  it("should sort admins by first name", () => {
    loginPage.setUsername('ivana');
    loginPage.setPassword('ivana');
    loginPage.getSignInButton().submit();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/home');
    browser.get("http://localhost:49152/#/admins");
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/admins');

    adminsListPage.getDropdownToggle().click();
    element(by.css('.dropdown-menu li:nth-child(1)')).click();
    expect(adminsListPage.getDropdownToggle().getText()).toBe("First name");
    adminsListPage.getSortButton().click();
    let firstNameFirstRow = element(by.xpath("//tr[first()]/td[1]"));
    let firstNameLastRow = element(by.xpath("//tr[last()]/td[1]"));
    expect(firstNameFirstRow < firstNameLastRow);
  });

  it("should register new admin and add it to table", () => {
    loginPage.setUsername('ivana');
    loginPage.setPassword('ivana');
    loginPage.getSignInButton().submit();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/home');
    browser.get("http://localhost:49152/#/admins");
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/admins');

    adminsListPage.getAddButton().click();
    adminRegistrationPage = new AdminRegistrationPage();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/admins/register');
    adminRegistrationPage.setFirstName(newAdmin.firstName);
    adminRegistrationPage.setLastName(newAdmin.lastName);
    adminRegistrationPage.setUsername(newAdmin.username);
    adminRegistrationPage.setPassword(newAdmin.pass);
    adminRegistrationPage.setConfirmPassword(newAdmin.pass2);
    adminRegistrationPage.getSingUpButton().click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/admins/register');
    browser.get('http://localhost:49152/#/admins');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/admins');
    let firstNameFirstRow = element(by.xpath("//tr[first()]/td[1]"));
    expect(firstNameFirstRow == newAdmin.firstName);
  });

  it("should not have delete button on currently logged in admin", () => {

    loginPage.setUsername(newAdmin.username);
    loginPage.setPassword(newAdmin.lastName);
    loginPage.getSignInButton().submit();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/home');
    browser.get("http://localhost:49152/#/admins");
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/admins');

    let deleteButtonFirstRow = element(by.xpath("//tr[first()]/td[4]"));

    expect(deleteButtonFirstRow == empty.empty);

  });

  it("should delete admin and remove it from table", () => {
    loginPage.setUsername('ivana');
    loginPage.setPassword('ivana');
    loginPage.getSignInButton().submit();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/home');
    browser.get("http://localhost:49152/#/admins");
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/admins');

    element(by.id(newAdmin.username)).click();

    browser.wait(function() {

      return confirmDeletePage.getConfirmDeleteElement().isDisplayed();
    }).then(function() {

      expect(confirmDeletePage.getConfirmButton().isDisplayed()).toBe(true);
      expect(confirmDeletePage.getConfirmButton().isEnabled()).toBe(true);
      expect(confirmDeletePage.getDeclineButton().isDisplayed()).toBe(true);
      expect(confirmDeletePage.getDeclineButton().isEnabled()).toBe(true);

      confirmDeletePage.getConfirmButton().click();
      expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/admins');
      let firstNameFirstRow = element(by.xpath("//tr[first()]/td[1]"));
      expect(firstNameFirstRow != newAdmin.firstName);
      });
    });


});
