package rs.ac.uns.ftn.ktsnvt.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;
import rs.ac.uns.ftn.ktsnvt.controller.util.TestUtil;
import rs.ac.uns.ftn.ktsnvt.web.dto.*;

import javax.annotation.PostConstruct;
import java.nio.charset.Charset;

import static org.hamcrest.Matchers.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Created by Ivana Zeljkovic on 02/12/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class AdminControllerIntegrationTest {

    private static final String URL_PREFIX = "/api/admins";

    private MediaType contentType = new MediaType(
            MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    private MockMvc mockMvc;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja administratora
    private String accessTokenAdmin;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja stanara
    private String accessTokenTenant;


    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private TestRestTemplate testRestTemplate;


    @PostConstruct
    public void setup() {
        this.mockMvc = MockMvcBuilders.
                webAppContextSetup(webApplicationContext).apply(springSecurity()).build();
    }

    // pre izvrsavanja testa, neophodno je izvrsiti logovanje u aplikaciju da bismo dobili token
    @Before
    public void login() throws Exception {
        ObjectMapper mapper = new ObjectMapper();

        // logovanje stanara
        LoginDTO loginDTOTenant = new LoginDTO("kaca", "kaca");

        ResponseEntity<TokenDTO> responseEntityTenant =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOTenant,
                        TokenDTO.class);
        // preuzmemo token za logovanog stanara iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenTenant = responseEntityTenant.getBody().getValue();


        // logovanje administratora
        LoginDTO loginDTO = new LoginDTO("ivana", "ivana");

        ResponseEntity<TokenDTO> responseEntity =
                testRestTemplate.postForEntity("/api/login",
                        loginDTO,
                        TokenDTO.class);
        // preuzmemo token za logovanog administratora iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenAdmin = responseEntity.getBody().getValue();
    }


    // TESTOVI ZA getAllAdmins
    @Test
    public void testGetAllAdmins() throws Exception {
       /*
        * Test proverava ispravnost rada metode getAllAdmins kontrolera AdminController
        * (dobavljanje liste svih administratora)
        * */

        MultiValueMap<String, String> params = TestUtil.getParams(
                "0", "10", "ASC", "id", "");

        this.mockMvc.perform(get(URL_PREFIX)
                .header("Authentication-Token", this.accessTokenAdmin)
                .params(params))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$.[*].id").exists())
                .andExpect(jsonPath("$.[*].firstName").value(hasItem("Ivana")))
                .andExpect(jsonPath("$.[*].lastName").value(hasItem("Zeljkovic")));
    }

    @Test
    public void testGetAllAdminsByNotAuthenticatedUser() throws Exception {
       /*
        * Test proverava ispravnost rada metode getAllAdmins kontrolera AdminController
        * (dobavljanje liste svih administratora)
        * ali dobavljanje resursa pokusava da izvrsi korisnik koji nije ulogovan
        * */

        MultiValueMap<String, String> params = TestUtil.getParams(
                "0", "10", "ASC", "id", "");

        this.mockMvc.perform(get(URL_PREFIX)
                .params(params))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetAllAdminsByNotAuthenticatedAdmin() throws Exception {
       /*
        * Test proverava ispravnost rada metode getAllAdmins kontrolera AdminController
        * (dobavljanje liste svih administratora)
        * ali dobavljanje resursa pokusava da izvrsi ulogovani korisnik koji nema ulogu administratora, vec stanara
        * */

        MultiValueMap<String, String> params = TestUtil.getParams(
                "0", "10", "ASC", "id", "");

        this.mockMvc.perform(get(URL_PREFIX)
                .header("Authentication-Token", accessTokenTenant)
                .params(params))
                .andExpect(status().isForbidden());
    }

    // TESTOVI ZA getAdmin
    @Test
    public void testGetAdmin() throws Exception {
       /*
        * Test proverava ispravnost rada metode getAdmin kontrolera AdminController
        * (dobavljanje konkretnog administratora na osnovu zadatog ID-a)
        * pri cemu u bazi postoji administrator sa ID-em koji je zadat u URL-u
        * */

        Long id = 99L;

        this.mockMvc.perform(get(URL_PREFIX + "/" + id)
                .header("Authentication-Token", accessTokenAdmin))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value("99"))
                .andExpect(jsonPath("$.firstName").value("Ivana"))
                .andExpect(jsonPath("$.lastName").value("Zeljkovic"));
    }

    @Test
    public void testGetAdminByNotAuthenticatedUser() throws Exception {
       /*
        * Test proverava ispravnost rada metode getAdmin kontrolera AdminController
        * (dobavljanje konkretnog administratora na osnovu zadatog ID-a)
        * ali dobavljanje resursa pokusava da izvrsi korisnik koji nije ulogovan
        * */

        Long id = 99L;

        this.mockMvc.perform(get(URL_PREFIX + "/" + id))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetAdminByNotAuthenticatedAdmin() throws Exception {
       /*
        * Test proverava ispravnost rada metode getAdmin kontrolera AdminController
        * (dobavljanje konkretnog administratora na osnovu zadatog ID-a)
        * ali dobavljanje resursa pokusava da izvrsi ulogovani korisnik koji nema ulogu administratora, vec stanara
        * */

        Long id = 99L;

        this.mockMvc.perform(get(URL_PREFIX + "/" + id)
                .header("Authentication-Token", accessTokenTenant))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetAdminNonexistentAdmin() throws Exception {
       /*
        * Test proverava ispravnost rada metode getAdmin kontrolera AdminController
        * (dobavljanje konkretnog administratora na osnovu zadatog ID-a)
        * pri cemu u bazi ne postoji administrator sa ID-em koji je zadat u URL-u
        * */

        Long id = 200L;

        this.mockMvc.perform(get(URL_PREFIX + "/" + id)
                .header("Authentication-Token", accessTokenAdmin))
                .andExpect(status().isNotFound());
    }

    // TESTOVI za registerAdmin
    @Test
    @Transactional
    @Rollback(true)
    public void testRegisterAdmin() throws Exception {
       /*
        * Test proverava ispravnost rada metode registerAdmin kontrolera AdminController
        * (registracija novog administratora od strane postojeceg)
        * */

        LoginDTO loginAccountDTO = new LoginDTO("novak", "novak");
        AdminCreateDTO adminDTO = new AdminCreateDTO();
        adminDTO.setFirstName("Novak");
        adminDTO.setLastName("Novakovic");
        adminDTO.setLoginAccount(loginAccountDTO);

        String adminDTOJson = TestUtil.json(adminDTO);

        this.mockMvc.perform(post(URL_PREFIX)
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(adminDTOJson))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.firstName").value("Novak"))
                .andExpect(jsonPath("$.lastName").value("Novakovic"))
                .andExpect(jsonPath("$.id").exists());
    }

    @Test
    public void testRegisterAdminByNotAuthenticatedUser() throws Exception {
       /*
        * Ttest proverava ispravnost rada metode registerAdmin kontrolera AdminController
        * (registracija novog administratora od strane postojeceg)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        LoginDTO loginAccountDTO = new LoginDTO("novak", "novak");
        AdminCreateDTO adminDTO = new AdminCreateDTO();
        adminDTO.setFirstName("Novak");
        adminDTO.setLastName("Novakovic");
        adminDTO.setLoginAccount(loginAccountDTO);

        String adminDTOJson = TestUtil.json(adminDTO);

        this.mockMvc.perform(post(URL_PREFIX)
                .contentType(contentType)
                .content(adminDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testRegisterAdminByNotAuthenticatedAdmin() throws Exception {
       /*
        * Test proverava ispravnost rada metode registerAdmin kontrolera AdminController
        * (registracija novog administratora od strane postojeceg)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu administratora
        * */

        LoginDTO loginAccountDTO = new LoginDTO("novak", "novak");
        AdminCreateDTO adminDTO = new AdminCreateDTO();
        adminDTO.setFirstName("Novak");
        adminDTO.setLastName("Novakovic");
        adminDTO.setLoginAccount(loginAccountDTO);

        String adminDTOJson = TestUtil.json(adminDTO);

        this.mockMvc.perform(post(URL_PREFIX)
                .header("Authentication-Token", accessTokenTenant)
                .contentType(contentType)
                .content(adminDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testRegisterAdminUsernameAlreadyExist() throws Exception {
       /*
        * Test proverava ispravnost rada metode registerAdmin kontrolera AdminController
        * (registracija novog administratora od strane postojeceg)
        * u slucaju kada je dolazni DTO objekat validan: poseduje sva polja neohodna za registraciju - ime, prezime i kredencijale
        * pri cemu je izabrano korisnicko ime vec zauzeto od strane nekog drugog korisnika u aplikaciji
        * */
        LoginDTO loginAccountDTO = new LoginDTO("ivana", "ivana");
        AdminCreateDTO adminDTO = new AdminCreateDTO();
        adminDTO.setFirstName("Ivana");
        adminDTO.setLastName("Durmic");
        adminDTO.setLoginAccount(loginAccountDTO);

        String adminDTOJson = TestUtil.json(adminDTO);

        this.mockMvc.perform(post(URL_PREFIX)
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(adminDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testRegisterAdminMissingFirstName() throws Exception {
       /*
        * Test proverava ispravnost rada metode registerAdmin kontrolera AdminController
        * (registracija novog administratora od strane postojeceg)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne poseduje sva polja neohodna za registraciju - nedostaje polje firstName
        * */

        LoginDTO loginAccountDTO = new LoginDTO("novak", "novak");
        AdminCreateDTO adminDTO = new AdminCreateDTO();
        adminDTO.setLastName("Novakovic");
        adminDTO.setLoginAccount(loginAccountDTO);

        String adminDTOJson = TestUtil.json(adminDTO);

        this.mockMvc.perform(post(URL_PREFIX)
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(adminDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testRegisterAdminMissingLastName() throws Exception {
       /*
        * Test proverava ispravnost rada metode registerAdmin kontrolera AdminController
        * (registracija novog administratora od strane postojeceg)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne poseduje sva polja neohodna za registraciju - nedostaje polje lastName
        * */

        LoginDTO loginAccountDTO = new LoginDTO("novak", "novak");
        AdminCreateDTO adminDTO = new AdminCreateDTO();
        adminDTO.setFirstName("Novak");
        adminDTO.setLoginAccount(loginAccountDTO);

        String adminDTOJson = TestUtil.json(adminDTO);

        this.mockMvc.perform(post(URL_PREFIX)
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(adminDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testRegisterAdminMissingUsername() throws Exception {
       /*
        * Test proverava ispravnost rada metode registerAdmin kontrolera AdminController
        * (registracija novog administratora od strane postojeceg)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne poseduje sva polja neohodna za registraciju - nedostaje polje username
        * */

        LoginDTO loginAccountDTO = new LoginDTO(null, "novak");
        AdminCreateDTO adminDTO = new AdminCreateDTO();
        adminDTO.setFirstName("Novak");
        adminDTO.setLastName("Novakovic");
        adminDTO.setLoginAccount(loginAccountDTO);

        String adminDTOJson = TestUtil.json(adminDTO);

        this.mockMvc.perform(post(URL_PREFIX)
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(adminDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testRegisterAdminMissingPassword() throws Exception {
        /*
        * Test proverava ispravnost rada metode registerAdmin kontrolera AdminController
        * (registracija novog administratora od strane postojeceg)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne poseduje sva polja neohodna za registraciju - nedostaje polje password
        * */

        LoginDTO loginAccountDTO = new LoginDTO("novak", null);
        AdminCreateDTO adminDTO = new AdminCreateDTO();
        adminDTO.setFirstName("Novak");
        adminDTO.setLastName("Novakovic");
        adminDTO.setLoginAccount(loginAccountDTO);

        String adminDTOJson = TestUtil.json(adminDTO);

        this.mockMvc.perform(post(URL_PREFIX)
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(adminDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testRegisterAdminInappropriateTypeOfUsername() throws Exception {
       /*
        * Test proverava ispravnost rada metode registerAdmin kontrolera AdminController
        * (registracija novog administratora od strane postojeceg)
        * u slucaju kada je dolazni DTO objekat nevalidan: poseduje sva polja neohodna za registraciju,
        * ali je tip podatka u polju username nevalidan
        * */

        String adminDTOJson = "{loginAccount:{" +
                "username:1," +
                "password:'novak'}," +
                "firstName:'Novak'," +
                "lastName:'Novakovic'}";

        this.mockMvc.perform(post(URL_PREFIX)
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(adminDTOJson))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za deleteAdmin
    @Test
    @Transactional
    @Rollback(true)
    public void testDeleteAdmin() throws Exception {
       /*
        * Test proverava ispravnost rada metode deleteAdmin kontrolera AdminController
        * (uklanjanje konkretnog administratora na osnovu zadatog ID-a)
        * pri cemu u bazi postoji administrator sa ID-em koji je zadat u URL-u
        * */

        Long id = 100L;

        this.mockMvc.perform(delete(URL_PREFIX + "/" + id)
                .header("Authentication-Token", accessTokenAdmin))
                .andExpect(status().isOk());
        // nakon brisanja, ne postoji trazeni resurs u bazi
        this.mockMvc.perform(get(URL_PREFIX + "/" + id)
                .header("Authentication-Token", accessTokenAdmin))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeleteAdminByNotAuthenticatedUser() throws Exception {
       /*
        * Test proverava ispravnost rada metode deleteAdmin kontrolera AdminController
        * (uklanjanje konkretnog administratora na osnovu zadatog ID-a)
        * ali uklanjanje resursa pokusava da izvrsi korisnik koji nije ulogovan
        * */

        Long id = 99L;

        this.mockMvc.perform(delete(URL_PREFIX + "/" + id))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDeleteAdminByNotAuthenticatedAdmin() throws Exception {
       /*
        * Test proverava ispravnost rada metode deleteAdmin kontrolera AdminController
        * (uklanjanje konkretnog administratora na osnovu zadatog ID-a)
        * ali uklanjanje resursa pokusava da izvrsi ulogovani korisnik koji nema ulogu administratora, vec stanara
        * */

        Long id = 99L;

        this.mockMvc.perform(delete(URL_PREFIX + "/" + id)
                .header("Authentication-Token", accessTokenTenant))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testDeleteAdminNonexistentAdmin() throws Exception {
       /*
        * Test proverava ispravnost rada metode deleteAdmin kontrolera AdminController
        * (uklanjanje konkretnog administratora na osnovu zadatog ID-a)
        * pri cemu u bazi ne postoji administrator sa ID-em koji je zadat u URL-u
        * */

        Long id = 200L;

        this.mockMvc.perform(delete(URL_PREFIX + "/" + id)
                .header("Authentication-Token", accessTokenAdmin))
                .andExpect(status().isNotFound());
    }

    // TESTOVI za updateAdmin
    @Test
    @Transactional
    @Rollback(true)
    public void testUpdateAdmin() throws Exception {
       /*
        * Test proverava ispravnost rada metode updateAdmin kontrolera AdminController
        * (azuriranje konkretnog administratora na osnovu zadatog ID-a)
        * pri cemu u bazi postoji administrator sa ID-em koji je zadat u URL-u
        * */

        AdminUpdateDTO adminUpdateDTO = new AdminUpdateDTO(
                "Ivana", "Durmic", "ivana", "novi");

        String adminUpdateDTOJson = TestUtil.json(adminUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/99")
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(adminUpdateDTOJson))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value("99"))
                .andExpect(jsonPath("$.firstName").value("Ivana"))
                .andExpect(jsonPath("$.lastName").value("Durmic"));
    }

    @Test
    @Transactional
    @Rollback(true)
    public void testUpdateAdminMissingNewPassAndCurrentPass() throws Exception {
       /*
        * Test proverava ispravnost rada metode updateAdmin kontrolera AdminController
        * (azuriranje konkretnog administratora na osnovu zadatog ID-a)
        * pri cemu dolazni DTO objekat validan: ne poseduje polja currentPassword i newPassword - azuriraju se samo ime i prezime
        * */

        AdminUpdateDTO adminUpdateDTO = new AdminUpdateDTO();
        adminUpdateDTO.setFirstName("Ivana");
        adminUpdateDTO.setLastName("Durmic");

        String adminUpdateDTOJson = TestUtil.json(adminUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/99")
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(adminUpdateDTOJson))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value("99"))
                .andExpect(jsonPath("$.firstName").value("Ivana"))
                .andExpect(jsonPath("$.lastName").value("Durmic"));
    }

    @Test
    public void testUpdateAdminByNotAuthenticatedUser() throws Exception {
       /*
        * Test proverava ispravnost rada metode updateAdmin kontrolera AdminController
        * (azuriranje konkretnog administratora na osnovu zadatog ID-a)
        * ali azuriranje resursa pokusava da izvrsi korisnik koji nije ulogovan
        * */

        AdminUpdateDTO adminUpdateDTO = new AdminUpdateDTO(
                "Ivana", "Durmic", "ivana", "novi");

        String adminUpdateDTOJson = TestUtil.json(adminUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/99")
                .contentType(contentType)
                .content(adminUpdateDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testUpdateAdminByNotAuthenticatedAdmin() throws Exception {
       /*
        * Test proverava ispravnost rada metode updateAdmin kontrolera AdminController
        * (azuriranje konkretnog administratora na osnovu zadatog ID-a)
        * ali azuriranje resursa pokusava da izvrsi ulogovani korisnik koji nema ulogu administratora, vec stanara
        * */

        AdminUpdateDTO adminUpdateDTO = new AdminUpdateDTO(
                "Ivana", "Durmic", "ivana", "novi");

        String adminUpdateDTOJson = TestUtil.json(adminUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/99")
                .header("Authentication-Token", accessTokenTenant)
                .contentType(contentType)
                .content(adminUpdateDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testUpdateAdminNonexistentAdmin() throws Exception {
       /*
        * Test proverava ispravnost rada metode updateAdmin kontrolera AdminController
        * (azuriranje konkretnog administratora na osnovu zadatog ID-a)
        * pri cemu u bazi ne postoji administrator sa ID-em koji je zadat u URL-u
        * */

        AdminUpdateDTO adminUpdateDTO = new AdminUpdateDTO(
                "Ivana", "Durmic", "ivana", "novi");

        String adminUpdateDTOJson = TestUtil.json(adminUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/200")
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(adminUpdateDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testUpdateAdminNonCurrentAdmin() throws Exception {
       /*
        * Test proverava ispravnost rada metode updateAdmin kontrolera AdminController
        * (azuriranje konkretnog administratora na osnovu zadatog ID-a)
        * pri cemu u bazi postoji administrator sa ID-em koji je zadat u URL-u, ali to nije ujedno i ID
        * trenutno ulogovanog administratora
        * */

        AdminUpdateDTO adminUpdateDTO = new AdminUpdateDTO(
                "Marko", "Markovic", "marko", "novi");

        String adminUpdateDTOJson = TestUtil.json(adminUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/100")
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(adminUpdateDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testUpdateAdminMissingFirstName() throws Exception {
       /*
        * Test proverava ispravnost rada metode updateAdmin kontrolera AdminController
        * (azuriranje konkretnog administratora na osnovu zadatog ID-a)
        * pri cemu dolazni DTO objekat nevalidan: ne poseduje sva polja - nedostaje polje firstName
        * */

        AdminUpdateDTO adminUpdateDTO = new AdminUpdateDTO();
        adminUpdateDTO.setLastName("Durmic");
        adminUpdateDTO.setCurrentPassword("ivana");
        adminUpdateDTO.setNewPassword("novi");

        String adminUpdateDTOJson = TestUtil.json(adminUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/99")
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(adminUpdateDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateAdminMissingLastName() throws Exception {
       /*
        * Test proverava ispravnost rada metode updateAdmin kontrolera AdminController
        * (azuriranje konkretnog administratora na osnovu zadatog ID-a)
        * pri cemu dolazni DTO objekat nevalidan: ne poseduje sva polja - nedostaje polje lastName
        * */

        AdminUpdateDTO adminUpdateDTO = new AdminUpdateDTO();
        adminUpdateDTO.setFirstName("Ivana");
        adminUpdateDTO.setCurrentPassword("ivana");
        adminUpdateDTO.setNewPassword("novi");

        String adminUpdateDTOJson = TestUtil.json(adminUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/99")
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(adminUpdateDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateAdminMissingCurrentPassButNotNewPass() throws Exception {
       /*
        * Test proverava ispravnost rada metode updateAdmin kontrolera AdminController
        * (azuriranje konkretnog administratora na osnovu zadatog ID-a)
        * pri cemu dolazni DTO objekat nevalidan: ne poseduje sva polja - nedostaje polje currentPassword
        * */

        AdminUpdateDTO adminUpdateDTO = new AdminUpdateDTO();
        adminUpdateDTO.setFirstName("Ivana");
        adminUpdateDTO.setLastName("Durmic");
        adminUpdateDTO.setNewPassword("novi");

        String adminUpdateDTOJson = TestUtil.json(adminUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/99")
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(adminUpdateDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateAdminMissingNewPassButNotCurrentPass() throws Exception {
       /*
        * Test proverava ispravnost rada metode updateAdmin kontrolera AdminController
        * (azuriranje konkretnog administratora na osnovu zadatog ID-a)
        * pri cemu dolazni DTO objekat nevalidan: ne poseduje sva polja - nedostaje polje newPassword
        * */

        AdminUpdateDTO adminUpdateDTO = new AdminUpdateDTO();
        adminUpdateDTO.setFirstName("Ivana");
        adminUpdateDTO.setLastName("Durmic");
        adminUpdateDTO.setCurrentPassword("ivana");

        String adminUpdateDTOJson = TestUtil.json(adminUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/99")
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(adminUpdateDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateAdminInappropriateTypeOfFirstName() throws Exception {
       /*
        * Test proverava ispravnost rada metode updateAdmin kontrolera AdminController
        * (azuriranje konkretnog administratora na osnovu zadatog ID-a)
        * pri cemu dolazni DTO objekat nevalidan: poseduje sva polja ali je tip podatka
        * u polju ime nevalidan (nije String nego int)
        * */

        String adminUpdateDTOJson = "{" +
                    "firstName:1," +
                    "lastName:'Durmic'," +
                    "currentPassword:'ivana'," +
                    "newPassword:'novi'" +
                "}";

        this.mockMvc.perform(put(URL_PREFIX + "/99")
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(adminUpdateDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateAdminInvalidCurrentPass() throws Exception {
       /*
        * Test proverava ispravnost rada metode updateAdmin kontrolera AdminController
        * (azuriranje konkretnog administratora na osnovu zadatog ID-a)
        * pri cemu dolazni DTO objekat nevalidan: poseduje sva polja ali je vrednost
        * currentPassword nevalidna
        * */

        AdminUpdateDTO adminUpdateDTO = new AdminUpdateDTO("Ivana", "Durmic",
                "neispravno", "novi");

        String adminUpdateDTOJson = TestUtil.json(adminUpdateDTO);

        this.mockMvc.perform(put(URL_PREFIX + "/99")
                .header("Authentication-Token", accessTokenAdmin)
                .contentType(contentType)
                .content(adminUpdateDTOJson))
                .andExpect(status().isBadRequest());
    }
}
