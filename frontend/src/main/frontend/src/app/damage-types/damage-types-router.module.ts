import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";
// component
import { DamageTypesComponent } from "./damage-types/damage-types.component";

const routes: Routes = [
  { path: '', component: DamageTypesComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class DamageTypesRouterModule { }
