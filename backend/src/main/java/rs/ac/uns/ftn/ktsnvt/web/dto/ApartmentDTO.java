package rs.ac.uns.ftn.ktsnvt.web.dto;

import rs.ac.uns.ftn.ktsnvt.model.entity.Apartment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ivana Zeljkovic on 10/12/2017.
 */
public class ApartmentDTO implements Serializable {

    private Long id;
    private int number;
    private int floor;
    private int surface;
    private TenantDTO owner;
    private List<TenantDTO> tenants;
    private Long buildingId;
    private long numberOfElements;

    public ApartmentDTO() { }

    public ApartmentDTO(Apartment apartment) {
        this.id = apartment.getId();
        this.number = apartment.getNumber();
        this.floor = apartment.getFloor();
        this.surface = apartment.getSurface();
        this.owner = apartment.getOwner() == null ? null : new TenantDTO(apartment.getOwner());
        this.tenants = new ArrayList<>();
        if(!apartment.getTenants().isEmpty())
            apartment.getTenants().forEach(tenant -> this.tenants.add(new TenantDTO(tenant)));
        this.buildingId = apartment.getBuilding().getId();
    }

    public ApartmentDTO(Apartment apartment, long numberOfElements) {
        this.id = apartment.getId();
        this.number = apartment.getNumber();
        this.floor = apartment.getFloor();
        this.surface = apartment.getSurface();
        this.owner = apartment.getOwner() == null ? null : new TenantDTO(apartment.getOwner());
        this.tenants = new ArrayList<>();
        if(!apartment.getTenants().isEmpty())
            apartment.getTenants().forEach(tenant -> this.tenants.add(new TenantDTO(tenant)));
        this.buildingId = apartment.getBuilding().getId();
        this.numberOfElements = numberOfElements;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public int getSurface() {
        return surface;
    }

    public void setSurface(int surface) {
        this.surface = surface;
    }

    public TenantDTO getOwner() {
        return owner;
    }

    public void setOwner(TenantDTO owner) {
        this.owner = owner;
    }

    public List<TenantDTO> getTenants() {
        return tenants;
    }

    public void setTenants(List<TenantDTO> tenants) {
        this.tenants = tenants;
    }

    public Long getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(Long buildingId) {
        this.buildingId = buildingId;
    }

    public long getNumberOfElements() { return numberOfElements; }

    public void setNumberOfElements(long numberOfElements) { this.numberOfElements = numberOfElements; }
}
