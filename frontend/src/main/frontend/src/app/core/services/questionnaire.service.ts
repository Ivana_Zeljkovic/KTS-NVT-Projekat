import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
// service
import { DataService } from "./data.service";
// models
import { Questionnaire } from "../../shared/models/questionnaire";
import { QuestionnaireResults } from "../../shared/models/questionnaire-results";


@Injectable()
export class QuestionnaireService extends DataService<Questionnaire> {

  constructor(http: HttpClient) {
    super('', http);
  }

  getResults(buildingID: number, questionnaireID: number): Observable<QuestionnaireResults> {
    let url = `${this.urlBase}/buildings/${buildingID}/questionnaires/${questionnaireID}/results`;
    return this.http.get<QuestionnaireResults>(`${url}`)
      .catch(this.handleErrors);
  }
}
