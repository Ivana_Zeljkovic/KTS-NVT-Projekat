import { Address } from "./address";
import { Offer } from './offer';

export interface Business {
  id: number;
  name: string;
  description: string;
  phoneNumber: string;
  email: string;
  businessType: string;
  address: Address;
  isCompany: boolean;
  confirmed: boolean;
  numberOfElements?: number;
  offer?: Offer;
}
