package rs.ac.uns.ftn.ktsnvt.web.dto;

import rs.ac.uns.ftn.ktsnvt.model.entity.Questionnaire;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Ivana Zeljkovic on 19/01/2018.
 */
public class QuestionnaireResultsDTO implements Serializable {

    private Long questionnaireId;
    private List<QuestionResultsDTO> results;
    private int numberOfVotes;

    public QuestionnaireResultsDTO() {
        this.results = new ArrayList<>();
    }

    public QuestionnaireResultsDTO(Questionnaire questionnaire) {
        this.questionnaireId = questionnaire.getId();
        this.numberOfVotes = questionnaire.getVotes().size();
        this.results = new ArrayList<>();

        HashMap<Long, HashMap<String, Integer>> resultsMap = new HashMap<>();

        questionnaire.getQuestions().forEach(question -> {
            HashMap<String, Integer> values = new HashMap<>();
            question.getAnswers().forEach(answer -> values.put(answer, 0));
            resultsMap.put(question.getId(), values);
        });

        questionnaire.getVotes().forEach(vote ->
            vote.getQuestionsVotes().forEach(questionVote -> {
                Long questionId = questionVote.getQuestion().getId();
                String questionAnswer = questionVote.getAnswer();

                int newStateOfVotes = resultsMap.get(questionId).get(questionAnswer) + 1;
                resultsMap.get(questionId).put(questionAnswer, newStateOfVotes);
            })
        );

        questionnaire.getQuestions().forEach(question -> {
            QuestionResultsDTO questionResults = new QuestionResultsDTO();
            questionResults.setContent(question.getContent());
            questionResults.setQuestionId(question.getId());

            List<AnswerResultDTO> votingResults = new ArrayList<>();
            for(String answer : question.getAnswers()) {
                int votesNumber = 0;
                if(resultsMap.containsKey(question.getId()) && resultsMap.get(question.getId()).containsKey(answer))
                    votesNumber = resultsMap.get(question.getId()).get(answer);

                votingResults.add(new AnswerResultDTO(answer, votesNumber));
            }
            questionResults.setAnswers(votingResults);
            this.results.add(questionResults);
        });
    }

    public Long getQuestionnaireId() {
        return questionnaireId;
    }

    public void setQuestionnaireId(Long questionnaireId) {
        this.questionnaireId = questionnaireId;
    }

    public List<QuestionResultsDTO> getResults() {
        return results;
    }

    public void setResults(List<QuestionResultsDTO> results) {
        this.results = results;
    }

    public int getNumberOfVotes() {
        return numberOfVotes;
    }

    public void setNumberOfVotes(int numberOfVotes) {
        this.numberOfVotes = numberOfVotes;
    }
}
