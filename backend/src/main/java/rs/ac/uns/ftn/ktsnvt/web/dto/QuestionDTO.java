package rs.ac.uns.ftn.ktsnvt.web.dto;

import rs.ac.uns.ftn.ktsnvt.model.entity.Question;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Ivana Zeljkovic on 13/11/2017.
 */
public class QuestionDTO implements Serializable {

    private Long id;
    private String content;
    private List<String> answers;

    public QuestionDTO() { }

    public QuestionDTO(Question question) {
        this.id = question.getId();
        this.content = question.getContent();
        this.answers = question.getAnswers();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<String> getAnswers() {
        return answers;
    }

    public void setAnswers(List<String> answers) {
        this.answers = answers;
    }
}
