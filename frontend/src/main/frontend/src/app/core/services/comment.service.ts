import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
// model
import { Comment } from "../../shared/models/comment";
// service
import { DataService } from "./data.service";


@Injectable()
export class CommentService extends DataService<Comment> {

  constructor(http: HttpClient) {
    super('', http);
  }
}
