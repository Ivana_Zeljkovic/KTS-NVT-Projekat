package rs.ac.uns.ftn.ktsnvt.repository.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.model.entity.Account;
import rs.ac.uns.ftn.ktsnvt.repository.AccountAuthorityRepository;
import rs.ac.uns.ftn.ktsnvt.repository.AccountRepository;

import static org.junit.Assert.*;

/**
 * Created by Ivana Zeljkovic on 27/11/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class AccountAuthorityRepositoryIntegrationTest {

    @Autowired
    private AccountAuthorityRepository accountAuthorityRepository;

    @Autowired
    private AccountRepository accountRepository;


    @Test
    @Transactional
    @Rollback(true)
    public void testRemovePresidentRole() {
        /*
         * Test proverava ispravnost rada metode removePresidentRole repozitorijuma AccountAuthorityRepository
         * (uklanjanje uloge predsednika skupstine stanara nad konkretnim stanarom)
         * u slucaju kada izabrani stanar poseduje zadatu rolu (PRESIDENT)
         * */

        Account account = this.accountRepository.findOne(new Long(101));
        int role_size_before_remove = account.getAccountAuthorities().size();

        // stanar ciji account ima ID = 101 je predsednik skupstine stanara
        int deleted_rows_num = this.accountAuthorityRepository.removePresidentRole(new Long(101));

        account = this.accountRepository.findOne(new Long(101));
        assertEquals(1, deleted_rows_num);
        assertEquals(1, (role_size_before_remove-1));
        assertEquals("TENANT", account.getAccountAuthorities().get(0).getAuthority().getName());
    }

    @Test
    @Transactional
    @Rollback(true)
    public void testRemoveNonexistentPresidentRole() {
        /*
         * Test proverava ispravnost rada metode removePresidentRole repozitorijuma AccountAuthorityRepository
         * (uklanjanje uloge predsednika skupstine stanara nad konkretnim stanarom)
         * u slucaju kada izabrani stanar ne poseduje zadatu rolu (PRESIDENT)
         * */

        Account account = this.accountRepository.findOne(new Long(102));
        int role_size_before_remove = account.getAccountAuthorities().size();

        // stanar ciji account ima ID = 102 nije predsednik skupstine stanara
        int deleted_rows_num = this.accountAuthorityRepository.removePresidentRole(new Long(102));

        account = this.accountRepository.findOne(new Long(102));
        assertEquals(0, deleted_rows_num);
        assertEquals(1, role_size_before_remove);
        assertEquals("TENANT", account.getAccountAuthorities().get(0).getAuthority().getName());
    }

    @Test
    @Transactional
    @Rollback(true)
    public void testRemovePresidentRoleOnNonexistentAccount() {
        /*
         * Test proverava ispravnost rada metode removePresidentRole repozitorijuma AccountAuthorityRepository
         * (uklanjanje uloge predsednika skupstine stanara nad konkretnim stanarom)
         * u slucaju kada ne postoji nalog sa zadatim ID-em
         * */

        // ne postoji account ciji je ID = 200
        int deleted_rows_num = this.accountAuthorityRepository.removePresidentRole(new Long(200));

        Account account = this.accountRepository.findOne(new Long(200));
        assertEquals(0, deleted_rows_num);
        assertNull(account);
    }
}
