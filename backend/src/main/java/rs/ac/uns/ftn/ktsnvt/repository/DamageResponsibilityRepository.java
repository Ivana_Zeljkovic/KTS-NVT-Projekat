package rs.ac.uns.ftn.ktsnvt.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import rs.ac.uns.ftn.ktsnvt.model.entity.DamageResponsibility;

import java.util.List;

/**
 * Created by Ivana Zeljkovic on 23/11/2017.
 */
public interface DamageResponsibilityRepository extends JpaRepository<DamageResponsibility, Long> {

    @Query("SELECT dr FROM DamageResponsibility dr WHERE dr.damageType.id = :damageTypeId AND" +
            " dr.responsibleTenant.id = :tenantId AND dr.building.id = :buildingId")
    DamageResponsibility findOneByDamageTypeAndTenantAndBuilding(@Param("damageTypeId") Long damageTypeId,
                                                                 @Param("tenantId") Long tenantId,
                                                                 @Param("buildingId") Long buildingId);

    @Query("SELECT dr FROM DamageResponsibility dr WHERE dr.damageType.id = :damageTypeId AND" +
            " dr.responsibleInstitution.id = :institutionId AND dr.building.id = :buildingId")
    DamageResponsibility findOneByDamageTypeAndInstitutionAndBuilding(@Param("damageTypeId") Long damageTypeId,
                                                                 @Param("institutionId") Long institutionId,
                                                                 @Param("buildingId") Long buildingId);

    @Query("SELECT dr FROM DamageResponsibility dr WHERE dr.damageType.id = :damageTypeId AND dr.building.id = :buildingId")
    DamageResponsibility findOneByDamageTypeAndBuilding(@Param("damageTypeId") Long damageTypeId, @Param("buildingId") Long buildingId);

    @Query(value = "SELECT dr FROM DamageResponsibility dr WHERE dr.responsibleInstitution.id = :institutionId")
    Page<DamageResponsibility> findAllByInstitutionId(@Param("institutionId") Long institutionId, Pageable pageable);

    @Query("SELECT dr FROM DamageResponsibility dr WHERE dr.building.id = :buildingId ORDER BY dr.damageType.id ASC")
    List<DamageResponsibility> findAllByBuildingId(@Param("buildingId") Long buildingId);
}
