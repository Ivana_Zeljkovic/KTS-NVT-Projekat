import { browser } from "protractor";
// page
import { LoginPage } from "../../page-objects/login.po";
import { NavbarPage } from "../../page-objects/navbar.po";
import { DamageListPage } from "../../page-objects/damage-list.po";
import { DamageAddPage } from "../../damages/page-objects/damage-add.po";
import { ConfirmDeletePage } from "../../page-objects/confirm-delete.po";
import { DelegateResponsibilityToPersonPage } from "../../page-objects/delegate-responsibility-to-person.po";


describe('Delegate responsibility for damage/damage type to another tenant', () => {
  let loginPage: LoginPage;
  let navbarPage: NavbarPage;
  let damageListPage: DamageListPage;
  let damageAddPage: DamageAddPage;
  let confirmDeletePage: ConfirmDeletePage;
  let delegateResponsibilityToPersonPage: DelegateResponsibilityToPersonPage;
  let existingDamage: any;

  beforeAll(() => {
    loginPage = new LoginPage();
    navbarPage = new NavbarPage();
    damageListPage = new DamageListPage();
    damageAddPage = new DamageAddPage();
    confirmDeletePage = new ConfirmDeletePage();
    delegateResponsibilityToPersonPage = new DelegateResponsibilityToPersonPage();

    existingDamage = {'description':'Some description', 'urgency':'true', 'date':''};

    browser.get('/');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');

    loginPage.setUsername('kaca');
    loginPage.setPassword('kaca');
    loginPage.getSignInButton().click();

    // before all of these tests we should add damage in list
    browser.wait(function () {
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/home/notifications';
      });
    }).then(function () {
      expect(navbarPage.getNavbarElement().isDisplayed()).toBe(true);
      expect(navbarPage.getDamageDropdownLink().isDisplayed()).toBe(true);
      navbarPage.getDamageDropdownLink().click();

      browser.wait(function () {
        return navbarPage.getContentOfDamageDropdownLink().isDisplayed();
      }).then(function () {
        expect(navbarPage.getDamagesIReportedLink().isDisplayed()).toBe(true);
        navbarPage.getDamagesIReportedLink().click();

        browser.wait(function () {
          return damageListPage.getDamageListElement().isDisplayed();
        }).then(function () {
          //expect to see enable button Add on this page
          expect(damageListPage.getAddButton().isDisplayed()).toBe(true);
          expect(damageListPage.getAddButton().isEnabled()).toBe(true);

          damageListPage.getAddButton().click();

          browser.wait(function () {
            return damageAddPage.getDamageAddElement().isDisplayed();
          }).then(function () {
            damageAddPage.setDamageDescription(existingDamage.description);
            damageAddPage.getDamageUrgent(1).click();
            damageAddPage.getSaveButton().click();

            damageListPage.getDateFromDamagePanel(0).getText().then(value => {
              existingDamage.date = value;
            });
          });
        });
      });
    });
  });

  afterAll(() => {
    // after all these tests, we should remove added damage
    navbarPage.getDamageDropdownLink().click();

    browser.wait(function () {
      return navbarPage.getContentOfDamageDropdownLink().isDisplayed();
    }).then(function () {
      expect(navbarPage.getDamagesIReportedLink().isDisplayed()).toBe(true);
      navbarPage.getDamagesIReportedLink().click();

      browser.wait(function () {
        return damageListPage.getDamageListElement().isDisplayed();
      }).then(function () {
        // delete option is third in option list (in menu)
        expect(damageListPage.getMenuFromDamagePanel(0).isDisplayed()).toBe(true);
        damageListPage.getMenuFromDamagePanel(0).click();

        browser.wait(function () {
          // delete is third option in damage menu
          return damageListPage.getLinkFromDamagePanel(0, 3).isDisplayed();
        }).then(function () {
          damageListPage.getLinkFromDamagePanel(0, 3).click();

          browser.wait(function () {
            // expect to show modal dialog for delete confirmation
            return confirmDeletePage.getConfirmDeleteElement().isDisplayed();
          }).then(function () {
            // expect to modal has all elements
            expect(confirmDeletePage.getConfirmButton().isDisplayed()).toBe(true);
            expect(confirmDeletePage.getConfirmButton().isEnabled()).toBe(true);
            expect(confirmDeletePage.getDeclineButton().isDisplayed()).toBe(true);
            expect(confirmDeletePage.getDeclineButton().isEnabled()).toBe(true);

            confirmDeletePage.getConfirmButton().click();
            browser.executeScript('window.localStorage.clear();');
          });
        });
      });
    });
  });



  it('should open modal for delegating responsibility to another tenant, when user click on Delegate ' +
    'responsibility option in damage menu on Damages I am responsbile person for page', () => {
    expect(navbarPage.getDamageDropdownLink().isDisplayed()).toBe(true);
    navbarPage.getDamageDropdownLink().click();

    browser.wait(function () {
      return navbarPage.getContentOfDamageDropdownLink().isDisplayed();
    }).then(function () {
      expect(navbarPage.getDamagesResponsibleForLink().isDisplayed()).toBe(true);
      navbarPage.getDamagesResponsibleForLink().click();

      browser.wait(function () {
        return damageListPage.getDamageListElement().isDisplayed();
      }).then(function () {
        // added damage should be first in list and should have option Delegate responsibility option in damage menu
        expect(damageListPage.getDamageDescriptionFromDamagePanel(0).getText()).toEqual(existingDamage.description);
        expect(damageListPage.getLabelUrgentFromDamagePanel(0).isDisplayed()).toBe(true);

        expect(damageListPage.getMenuFromDamagePanel(0).isDisplayed()).toBe(true);
        damageListPage.getMenuFromDamagePanel(0).click();

        browser.wait(function() {
          return damageListPage.getContentOfMenuFromDamagePanel(0).isDisplayed();
        }).then(function() {
          // link for delegating responsibility to another person is second in damage menu
          expect(damageListPage.getLinkFromDamagePanel(0, 2).isDisplayed()).toBe(true);
          damageListPage.getLinkFromDamagePanel(0,2).click();

          browser.wait(function() {
            return delegateResponsibilityToPersonPage.getDelegateResponsibilityToPersonElement().isDisplayed();
          }).then(function() {
            // choose first tenant from list if that tenant is not current logged president
            delegateResponsibilityToPersonPage.getTenantUsername(0).getText().then(value => {
              if(value.includes('(me)')) {
                delegateResponsibilityToPersonPage.getButtonDelegate(0).click();
              }
              else {
                delegateResponsibilityToPersonPage.getButtonDelegate(1).click();
              }

              // expect that selected damage is not in list anymore - first damage in list for sure has different date
              expect(damageListPage.getDateFromDamagePanel(0).getText()).not.toEqual(existingDamage.date);
            });
          });
        });
      });
    });
  });
});
