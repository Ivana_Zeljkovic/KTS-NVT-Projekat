import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router } from "@angular/router";
import { AgmMap } from "@agm/core";
import { ToasterService } from "angular5-toaster/dist";
// service
import { TenantService } from "../../core/services/tenant.service";
import { LocationService } from "../../core/services/location.service";
// model
import { Tenant } from "../../shared/models/tenant";
// error
import { AppError } from "../../shared/errors/app-error";
import {NotFoundError} from "../../shared/errors/not-found-error";
import {ForbiddenError} from "../../shared/errors/forbidden-error";
import {BadRequestError} from "../../shared/errors/bad-request-error";


@Component({
  selector: 'app-tenant',
  templateUrl: './tenant.component.html',
  styleUrls: ['./tenant.component.css']
})
export class TenantComponent implements OnInit {
  tenant: Tenant;
  id: number;
  longitude: number;
  latitude: number;
  zoom: number;
  @ViewChild(AgmMap) map: AgmMap;

  constructor(private tenantService: TenantService, private route: ActivatedRoute,
              private locationService: LocationService, private toasterService: ToasterService) {
    this.zoom = 14;
    // default coordinates if there are no coordinates for given address
    this.latitude = 45.2461;
    this.longitude = 19.8517;

    this.route.params.subscribe((param: Params) => {
      this.id = param['id'];
      this.getTenant();
    });
  }

  ngOnInit() {
    this.getTenant();
  }

  getTenant(){
    this.tenantService.get(this.id).subscribe(data => {
      let tenantData = data as Tenant;
      this.tenant = tenantData;

      if(tenantData.address != null){
        this.locationService.getCoordinates(`${tenantData.address.street} ${tenantData.address.city.name}`)
          .subscribe(response => {
            if (response.status == 'OK') {
              this.latitude = response.results[0].geometry.location.lat;
              this.longitude = response.results[0].geometry.location.lng;
              this.map.triggerResize();
            } else if (response.status == 'ZERO_RESULTS') {
              this.toasterService.pop('error', 'Error', 'Address couldn\'t be found!');
            } else {
              this.toasterService.pop('error','Error','Error in detecting coordinates of given address!');
            }
          }, (error: AppError) => {
            this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
            throw error;
          });
      }
    },(error: AppError) => {
      if(error instanceof NotFoundError)
        this.toasterService.pop('error', 'Error', 'Tenant not found!');
      else if(error instanceof ForbiddenError)
        this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
      else if(error instanceof BadRequestError)
        this.toasterService.pop('error', 'Error', 'Tenant does not exist!');
      else {
        this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
        throw error;
      }
    });
  }
}
