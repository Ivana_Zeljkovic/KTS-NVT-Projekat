import {LoginPage} from "../../page-objects/login.po";
import {BusinessProfilePage} from "../../profile/business-profile/page-objects/business-profile.po";
import {browser} from "protractor";
import {BusinessProfileAccountsModalPage} from "./page-objects/business-profile-accounts-modal.po";
import {BusinessAddAcountModalPage} from "../business-add-account/page-objects/businesss-add-account.po";
import {count} from "rxjs/operator/count";

describe('Listing all accounts except current on modal in business profile component', () => {


  let loginPage: LoginPage;
  let businessProfilePage: BusinessProfilePage;
  let businessProfileAccountsModal: BusinessProfileAccountsModalPage;
  let addAccountModalPage: BusinessAddAcountModalPage;

  beforeAll(() => {

    loginPage = new LoginPage();
    browser.get("http://localhost:49152/#/login?returnUrl=%2Fprofile");
    loginPage.setUsername('firm2');
    loginPage.setPassword('firm2');
    loginPage.getSignInButton().submit();

    businessProfilePage = new BusinessProfilePage();
    browser.wait(function () {
      return browser.getCurrentUrl().then(value => {
        return value == 'http://localhost:49152/#/profile'
      });
    });

  });

  beforeEach(() => {
    businessProfileAccountsModal = new BusinessProfileAccountsModalPage();
    addAccountModalPage = new BusinessAddAcountModalPage();

    businessProfilePage.getManageAccounts().click();
    browser.wait(() =>{
      return businessProfileAccountsModal.getModalFooter().isDisplayed();
    });

  });

  afterEach(() => {
    businessProfileAccountsModal.getCloseButton().click();
  });


  afterAll(() => {
    browser.executeScript('window.localStorage.clear();');
  });


  it('Should list no accounts', () => {
    expect(businessProfileAccountsModal.getAllAccountsList().count()).toBe(0);
  });

  it('Should open new modal, add valid username and password and then close it', () => {
    businessProfileAccountsModal.getOpenAddAccountModal().click().then(function () {
      addAccountModalPage.setUsername('jova12');
      addAccountModalPage.setPassword('jova12');
      addAccountModalPage.setConfirmPassword('jova12');

      expect(addAccountModalPage.getAddAccountButton().isEnabled()).toBe(true);

      addAccountModalPage.getAddAccountButton().click();
      addAccountModalPage.getCloseButton().click().then(function () {
        browser.wait(function () {
          return businessProfileAccountsModal.getAllAccountsList().isDisplayed();
        }).then(function () {
            expect(businessProfileAccountsModal.getAllAccountsList().count()).toBe(1);
        }
        );
      });
    });
  });


});
