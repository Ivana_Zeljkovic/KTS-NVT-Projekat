package rs.ac.uns.ftn.ktsnvt.web.dto;


import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Ivana Zeljkovic on 06/12/2017.
 */
public class QuestionUpdateDTO implements Serializable {

    @NotNull
    private Long id;
    @NotNull
    @NotEmpty
    private String content;
    @NotNull
    @Size(min = 1)
    private List<String> answers;

    public QuestionUpdateDTO() { }

    public QuestionUpdateDTO(Long id, String content, List<String> answers) {
        this.id = id;
        this.content = content;
        this.answers = answers;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<String> getAnswers() {
        return answers;
    }

    public void setAnswers(List<String> answers) {
        this.answers = answers;
    }
}
