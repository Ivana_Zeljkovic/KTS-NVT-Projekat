export class Token {
  sub: string;
  created: Date;
  roles: Array<string>;
  apartmentID: number;
  buildingID: number;
  id: number;
  isCouncilMember: boolean;
  isOwner: boolean;
  exp: Date;
}
