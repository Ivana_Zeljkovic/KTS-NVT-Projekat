package rs.ac.uns.ftn.ktsnvt.service.integration;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.exception.BadRequestException;
import rs.ac.uns.ftn.ktsnvt.exception.NotFoundException;
import rs.ac.uns.ftn.ktsnvt.model.entity.Record;
import rs.ac.uns.ftn.ktsnvt.service.RecordService;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class RecordServiceIntegrationTest {

    @Autowired
    RecordService recordService;


    @Test
    public void testCheckCouncilForRecord() {
        /*
         * Test proverava ispravnost rada metode checkCouncilForRecord repozitorijuma RecordRepository
         * (pronalazenje  izvestaja na osnovu ID-a i ID-a skupstine stanara kom pripada sastanak ciji se izvestaj trazi)
         * */

        // u bazi postoji skupstina stanara sa ID-em = 100 kojoj pripada sastanak kom pripada izvestaj sa ID-em = 100
        this.recordService.checkCouncilForRecord(Long.valueOf(100), Long.valueOf(100));
    }

    @Test(expected = BadRequestException.class)
    public void testCheckCouncilForRecordInvalid() {
        /*
         * Test proverava ispravnost rada metode checkCouncilForRecord repozitorijuma RecordRepository
         * (pronalazenje  izvestaja na osnovu ID-a i ID-a skupstine stanara kom pripada sastanak ciji se izvestaj trazi)
         * u slucaju kada postoji izvestaj sa zadatim ID-em ali ne pripada sastanku skupstine stanara sa zadatim ID-em
         * */

        // u bazi postoji skupstina stanara sa ID-em = 101 ali njoj ne pripada sastanak kom pripada izvestaj sa ID-em = 100
        this.recordService.checkCouncilForRecord(Long.valueOf(100), Long.valueOf(101));
    }
}
