import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
// service
import { DataService } from "./data.service";
// models
import { Offer } from "../../shared/models/offer";


@Injectable()
export class OfferService extends DataService<Offer> {

  constructor(http: HttpClient) {
    super('', http);
  }

  getOfferByBuildingAndDamageId(businessId : number, damageId: number){
    return this.http.get<Offer>(`${this.urlBase}/businesses/${businessId}/damages/${damageId}/accepted_offer`)
      .catch(this.handleErrors);
  }
}
