export class PresidentUpdate {
  constructor(private firstName: string,
              private lastName: string,
              private username: string,
              private questionnaireId?: number) { }
}
