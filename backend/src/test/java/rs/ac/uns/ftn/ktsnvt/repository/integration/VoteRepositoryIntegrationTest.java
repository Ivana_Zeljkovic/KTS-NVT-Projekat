package rs.ac.uns.ftn.ktsnvt.repository.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.ktsnvt.repository.VoteRepository;

import static org.junit.Assert.assertEquals;

/**
 * Created by Nikola Garabandic on 30/11/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class VoteRepositoryIntegrationTest {

    @Autowired
    private VoteRepository voteRepository;


    @Test
    public void testCountPersonalVotes(){
        /*
        * Test proverava ispravnost rada metode countPersonalVotes repozitorijuma VoteRepository
        * (dobavljanje broja postojecih glasova stanara sa zadatim ID-em u anketi sa zadatim ID-em)
        * */

        //Stanar sa id-em 101 je glasao na questionnaire-u sa idem 100 tacno jednom.
        int number = this.voteRepository.countPersonalVotes(101L, 100L);

        assertEquals(1, number);
    }
}
