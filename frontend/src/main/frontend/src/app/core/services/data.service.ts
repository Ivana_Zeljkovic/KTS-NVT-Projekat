import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
// error
import { BadRequestError } from "../../shared/errors/bad-request-error";
import { NotFoundError } from "../../shared/errors/not-found-error";
import { AppError } from "../../shared/errors/app-error";
import { ConflictError } from "../../shared/errors/conflict-error";
import { ForbiddenError } from "../../shared/errors/forbidden-error";
// error
import { PageAndSort } from "../../shared/models/page-and-sort";


@Injectable()
export class DataService<T> {
  protected readonly urlBase = "/api";
  constructor(private readonly urlSpecific: string, protected http: HttpClient) { }


  get(id: number): Observable<T>;
  get(id: number, urlFirstSuffix: string): Observable<T>;
  get(id: number, urlFirstSuffix: string,
      firstId: number, urlSecondSuffix: string): Observable<T>;
  get(id: number, urlFirstSuffix: string,
      firstId: number, urlSecondSuffix:string,
      secondId: number, urlThirdSuffix: string): Observable<T>;

  get(id: number, urlFirstSuffix?: string, firstId?: number, urlSecondSuffix?: string, secondId?: number, urlThirdSuffix?: string): Observable<T> {
    let url = this.getUrl(urlFirstSuffix, firstId, urlSecondSuffix, secondId, urlThirdSuffix);
    return this.http.get<T>(`${url}/${id}`).catch(this.handleErrors);
  }


  getPage(pageAndSort: PageAndSort): Observable<Array<T>>;
  getPage(pageAndSort: PageAndSort, urlFirstSuffix: string): Observable<Array<T>>;
  getPage(pageAndSort: PageAndSort, urlFirstSuffix: string,
         firstId: number, urlSecondSuffix: string): Observable<Array<T>>;
  getPage(pageAndSort: PageAndSort, urlFirstSuffix: string,
         firstId: number, urlSecondSuffix:string,
         secondId: number, urlThirdSuffix: string): Observable<Array<T>>;

  getPage(pageAndSort: PageAndSort, urlFirstSuffix?: string, firstId?: number, urlSecondSuffix?:string, secondId?: number, urlThirdSuffix?: string): Observable<Array<T>> {
    let url = this.getUrl(urlFirstSuffix, firstId, urlSecondSuffix, secondId, urlThirdSuffix);
    let params: HttpParams = new HttpParams()
      .append('pageNumber', pageAndSort.pageNumber)
      .append('pageSize', pageAndSort.pageSize)
      .append('sortDirection', pageAndSort.sortDirection)
      .append('sortProperty', pageAndSort.sortProperties[0])
      .append((pageAndSort.sortProperties.length > 1) ? 'sortPropertySecond' : '',
        (pageAndSort.sortProperties.length > 1) ? pageAndSort.sortProperties[1] : '');
    return this.http.get<Array<T>>(`${url}`, {params}).catch(this.handleErrors);
  }


  getAll(): Observable<Array<T>>;
  getAll(urlFirstSuffix: string): Observable<Array<T>>;
  getAll(urlFirstSuffix: string,
         firstId: number, urlSecondSuffix: string): Observable<Array<T>>;
  getAll(urlFirstSuffix: string,
         firstId: number, urlSecondSuffix:string,
         secondId: number, urlThirdSuffix: string): Observable<Array<T>>;

  getAll(urlFirstSuffix?: string, firstId?: number, urlSecondSuffix?:string, secondId?: number, urlThirdSuffix?: string): Observable<Array<T>> {
    let url = this.getUrl(urlFirstSuffix, firstId, urlSecondSuffix, secondId, urlThirdSuffix);
    return this.http.get<Array<T>>(`${url}`).catch(this.handleErrors);
  }


  save(resource: any): Observable<T>;
  save(resource: any, urlFirstSuffix: string): Observable<T>;
  save(resource: any, urlFirstSuffix: string,
       firstId: number, urlSecondSuffix: string): Observable<T>;
  save(resource: any, urlFirstSuffix: string,
       firstId: number, urlSecondSuffix: string,
       secondId: number, urlThirdSuffix: string): Observable<T>;

  save(resource: any, urlFirstSuffix?: string, firstId?: number, urlSecondSuffix?: string, secondId?: number, urlThirdSuffix?: string): Observable<T>{
    let url = this.getUrl(urlFirstSuffix, firstId, urlSecondSuffix, secondId, urlThirdSuffix);
    return this.http.post<T>(`${url}`, resource).catch(this.handleErrors);
  }


  update(id: number, resource: any): Observable<T>;
  update(id: number, resource: any, urlFirstSuffix: string): Observable<T>;
  update(id: number, resource: any, urlFirstSuffix: string,
         firstId: number, urlSecondSuffix: string): Observable<T>;
  update(id: number, resource: any, urlFirstSuffix: string,
         firstId: number, urlSecondSuffix: string,
         secondId: number, urlThirdSuffix: string): Observable<T>;

  update(id: number, resource: any, urlFirstSuffix?: string, firstId?: number, urlSecondSuffix?: string, secondId?: number, urlThirdSuffix?: string): Observable<T> {
    let url = this.getUrl(urlFirstSuffix, firstId, urlSecondSuffix, secondId, urlThirdSuffix);
    return this.http.put<T>(`${url}/${id}`, resource).catch(this.handleErrors);
  }


  updatePart(resource: any): Observable<T>;
  updatePart(resource: any, urlFirstSuffix: string): Observable<T>;
  updatePart(resource: any, urlFirstSuffix: string,
             firstId: number, urlSecondSuffix: string): Observable<T>;
  updatePart(resource: any, urlFirstSuffix: string,
             firstId: number, urlSecondSuffix: string,
             secondId: number, urlThirdSuffix: string): Observable<T>;
  updatePart(resource: any, urlFirstSuffix: string,
             firstId: number, urlSecondSuffix: string,
             secondId: number, urlThirdSuffix: string,
             thirdId: number, urlForthSuffix: string): Observable<T>;

  updatePart(resource: any, urlFirstSuffix?: string, firstId?: number, urlSecondSuffix?: string, secondId?: number, urlThirdSuffix?: string, thirdId?: number, urlForthSuffix?: string): Observable<T> {
    let url = this.getUrl(urlFirstSuffix, firstId, urlSecondSuffix, secondId, urlThirdSuffix);
    url = (thirdId && urlForthSuffix) ? `${url}/${thirdId}/${urlForthSuffix}` : `${url}`;
    return this.http.patch<T>(`${url}`, resource).catch(this.handleErrors);
  }


  remove(id: number): Observable<void>;
  remove(id: number, urlFirstSuffix: string): Observable<void>;
  remove(id: number, urlFirstSuffix: string,
         firstId: number, urlSecondSuffix: string): Observable<void>;
  remove(id: number, urlFirstSuffix: string,
         firstId: number, urlSecondSuffix: string,
         secondId: number, urlThirdSuffix: string): Observable<void>;

  remove(id: number, urlFirstSuffix?: string, firstId?: number, urlSecondSuffix?: string, secondId?: number, urlThirdSuffix?: string): Observable<void> {
    let url = this.getUrl(urlFirstSuffix, firstId, urlSecondSuffix, secondId, urlThirdSuffix);
    return this.http.delete<void>(`${url}/${id}`).catch(this.handleErrors);
  }


  protected getUrl(urlFirstSuffix?: string, firstId?: number, urlSecondSuffix?: string, secondId?: number, urlThirdSuffix?: string): string {
    let url: string;

    if(!urlFirstSuffix && !firstId && !urlSecondSuffix && !secondId && !urlThirdSuffix)
      url = '';
    else if(!firstId && !urlSecondSuffix && !secondId && !urlThirdSuffix)
      url = `${urlFirstSuffix}`;
    else if(!secondId && !urlThirdSuffix)
      url = `${urlFirstSuffix}/${firstId}/${urlSecondSuffix}`;
    else
      url = `${urlFirstSuffix}/${firstId}/${urlSecondSuffix}/${secondId}/${urlThirdSuffix}`;

    let finalUrl = `${this.urlBase}`;
    if(this.urlSpecific != '')
      finalUrl = `${finalUrl}/${this.urlSpecific}`;
    if(url != '')
      finalUrl = `${finalUrl}/${url}`;

    return finalUrl;
  }

  protected handleErrors(response: Response) {
    if(response.status === 400)
      return Observable.throw(new BadRequestError());
    else if(response.status === 403)
      return Observable.throw(new ForbiddenError());
    else if(response.status === 404)
      return Observable.throw(new NotFoundError());
    else if(response.status === 409)
      return Observable.throw(new ConflictError());
    return Observable.throw(new AppError(response));
  }
}
