import { Component, EventEmitter, Input, Output } from '@angular/core';
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { ToasterService } from "angular5-toaster/dist";
import * as _ from "lodash";
// model
import { MeetingItem } from "../../../shared/models/meeting-item";
import { DamageForMeetingItem } from "../shared/models/damage-for-meeting-item";
import { MeetingItemCreate } from "../../../shared/models/meeting-item-create";
import { MeetingItemUpdate } from "../../../shared/models/meeting-item-update";
// service
import { MeetingItemService } from "../../../core/services/meeting-item.service";
import { JwtService } from "../../../core/services/jwt.service";
// error
import { AppError } from "../../../shared/errors/app-error";
import { BadRequestError } from "../../../shared/errors/bad-request-error";
import { ForbiddenError } from "../../../shared/errors/forbidden-error";
import { NotFoundError } from "../../../shared/errors/not-found-error";
// component
import { QuestionnaireComponent } from "../../../shared/components/questionnaire/questionnaire.component";
import { DamageForQuestionnaireComponent } from "../damage-for-questionnaire/damage-for-questionnaire.component";


@Component({
  selector: 'app-meeting-item',
  templateUrl: './meeting-item.component.html',
  styleUrls: ['./meeting-item.component.css']
})
export class MeetingItemComponent {
  @Input() model: any;
  @Input() initialValues: any;
  @Input() update: boolean;
  @Input() meetingItemID: number;
  @Output() submitted = new EventEmitter<MeetingItem>();
  @Input() damageForMeetingItem: DamageForMeetingItem;
  @Input() damageForMeetingItemInitial: DamageForMeetingItem;
  changeInForm: boolean;
  damageChanged: boolean;
  questionnaireChanged: boolean;


  constructor(public modalRef: BsModalRef, private meetingItemService: MeetingItemService,
              private jwtService: JwtService, private toasterService: ToasterService,
              private modalService: BsModalService) {
    this.changeInForm = false;
    this.damageChanged = false;
    this.questionnaireChanged = false;
  }

  showQuestionnaire() {
    const modalRef = this.modalService.show(QuestionnaireComponent);

    modalRef.content.isReviewMode = false;
    if(!this.update)
      modalRef.content.model = this.model.questionnaireCreate;
    else
      modalRef.content.model = this.model.questionnaireUpdate;
    modalRef.content.initialValues = _.cloneDeep(modalRef.content.model);

    modalRef.content.submitted.subscribe(() => {
      if(!this.update)
        this.questionnaireChanged = JSON.stringify(this.model.questionnaireCreate) !== JSON.stringify(this.initialValues.questionnaireCreate);
      else
        this.questionnaireChanged = JSON.stringify(this.model.questionnaireUpdate) !== JSON.stringify(this.initialValues.questionnaireUpdate);
    });
  }

  connectWithDamage() {
    const modalRef = this.modalService.show(DamageForQuestionnaireComponent);

    modalRef.content.damageSelected.subscribe((damage) => {
      this.damageChanged = this.damageForMeetingItem.id !== damage.id;
      this.damageForMeetingItem.id = damage.id;
      this.damageForMeetingItem.description = damage.description;
    });
  }

  removeDamage() {
    this.damageForMeetingItem.id = -1;
    this.damageForMeetingItem.description = '';
    this.damageChanged = this.damageForMeetingItem.id !== this.damageForMeetingItemInitial.id;
  }

  onSubmit() {
    let buildingID = this.jwtService.getBuildingIdFromToken();

    if(!this.update) {
      let meetingItem: MeetingItemCreate;
      if(this.model.questionnaireCreate.questions.length === 0)
        meetingItem = new MeetingItemCreate(this.model.content, this.model.title);
      else
        meetingItem = this.model;

      if(this.damageForMeetingItem.id != -1)
        meetingItem.damageId = this.damageForMeetingItem.id;

      this.meetingItemService.save(meetingItem, 'buildings', buildingID, 'billboard/meeting_items')
        .subscribe(newMeetingItem => {
          this.submitted.emit(newMeetingItem);
          this.modalRef.hide();
        }, (error: AppError) => {
          if(error instanceof BadRequestError)
            this.toasterService.pop('error', 'Error', 'Given data have bad format!');
          else if(error instanceof ForbiddenError)
            this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
          else if(error instanceof NotFoundError)
            this.toasterService.pop('error', 'Error', 'Building not found!');
          else {
            this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
            throw error;
          }
        });
    }
    else {
      let meetingItem: MeetingItemUpdate;
      if(this.model.questionnaireUpdate && this.model.questionnaireUpdate.questions.length === 0)
        meetingItem = new MeetingItemUpdate(this.model.content, this.model.title);
      else
        meetingItem = this.model;

      if(this.damageForMeetingItem.id != -1)
        meetingItem.damageId = this.damageForMeetingItem.id;
      else
        meetingItem.damageId = null;

      this.meetingItemService.update(this.meetingItemID, meetingItem, 'buildings', buildingID, 'billboard/meeting_items')
        .subscribe(updatedMeetingItem => {
          this.submitted.emit(updatedMeetingItem);
          this.modalRef.hide();
        }, (error: AppError) => {
          if(error instanceof BadRequestError)
            this.toasterService.pop('error', 'Error', 'Given data have bad format!');
          else if(error instanceof ForbiddenError)
            this.toasterService.pop('error', 'Error', 'You don\'t have permission for this action!');
          else if(error instanceof NotFoundError)
            this.toasterService.pop('error', 'Error', 'Building not found/Selected meeting-details item/Questionnaire/Some question not found!');
          else {
            this.toasterService.pop('error', 'Error', 'Something unexpected happened! \nSee information about error in console.');
            throw error;
          }
        });
    }
  }

  reset() {
    this.model = _.cloneDeep(this.initialValues);
    this.changeInForm = false;
    this.damageForMeetingItem = _.cloneDeep(this.damageForMeetingItemInitial);
    this.damageChanged = false;
    this.questionnaireChanged = false;
  }

  changeExist(newValues) {
    this.changeInForm = (newValues.title !== this.initialValues.title || newValues.content !== this.initialValues.content);
  }
}
