package rs.ac.uns.ftn.ktsnvt.web.dto;

import rs.ac.uns.ftn.ktsnvt.model.entity.Business;
import rs.ac.uns.ftn.ktsnvt.model.entity.DamageResponsibility;
import rs.ac.uns.ftn.ktsnvt.model.entity.Tenant;

import java.io.Serializable;

/**
 * Created by Ivana Zeljkovic on 23/11/2017.
 */
public class DamageResponsibilityDTO implements Serializable {

    private Long id;
    private Long tenantId;
    private String tenantName;
    private Long institutionId;
    private String institutionName;
    private Long damageTypeId;
    private Long buildingId;
    private AddressDTO address;
    private String damageTypeName;
    private long numberOfElements;


    public DamageResponsibilityDTO() { }

    public DamageResponsibilityDTO(DamageResponsibility damageResponsibility) {
        this.id = damageResponsibility.getId();
        Tenant tenant = damageResponsibility.getResponsibleTenant();
        if(tenant != null) {
            this.tenantId = tenant.getId();
            this.tenantName = tenant.getFirstName() + " " + tenant.getLastName();
        }
        else {
            this.tenantId = null;
            this.tenantName = null;
        }
        Business institution = damageResponsibility.getResponsibleInstitution();
        if(institution != null) {
            this.institutionId = institution.getId();
            this.institutionName = institution.getName();
        }
        else {
            this.institutionId = null;
            this.institutionName = null;
        }
        this.damageTypeId = damageResponsibility.getDamageType().getId();
        this.buildingId = damageResponsibility.getBuilding().getId();
        this.address = new AddressDTO(damageResponsibility.getBuilding().getAddress());
        this.damageTypeName = damageResponsibility.getDamageType().getName();
    }

    public DamageResponsibilityDTO(DamageResponsibility damageResponsibility, long numberOfElements) {
        this.id = damageResponsibility.getId();
        Tenant tenant = damageResponsibility.getResponsibleTenant();
        if(tenant != null) {
            this.tenantId = tenant.getId();
            this.tenantName = tenant.getFirstName() + " " + tenant.getLastName();
        }
        else {
            this.tenantId = null;
            this.tenantName = null;
        }
        Business institution = damageResponsibility.getResponsibleInstitution();
        if(institution != null) {
            this.institutionId = institution.getId();
            this.institutionName = institution.getName();
        }
        else {
            this.institutionId = null;
            this.institutionName = null;
        }
        this.damageTypeId = damageResponsibility.getDamageType().getId();
        this.buildingId = damageResponsibility.getBuilding().getId();
        this.address = new AddressDTO(damageResponsibility.getBuilding().getAddress());
        this.damageTypeName = damageResponsibility.getDamageType().getName();
        this.numberOfElements = numberOfElements;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    public String getTenantName() { return tenantName; }

    public void setTenantName(String tenantName) { this.tenantName = tenantName; }

    public Long getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(Long institutionId) {
        this.institutionId = institutionId;
    }

    public String getInstitutionName() { return institutionName; }

    public void setInstitutionName(String institutionName) { this.institutionName = institutionName; }

    public Long getDamageTypeId() {
        return damageTypeId;
    }

    public void setDamageTypeId(Long damageTypeId) {
        this.damageTypeId = damageTypeId;
    }

    public Long getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(Long buildingId) {
        this.buildingId = buildingId;
    }

    public AddressDTO getAddress() {
        return address;
    }

    public void setAddress(AddressDTO address) {
        this.address = address;
    }

    public String getDamageTypeName() {
        return damageTypeName;
    }

    public void setDamageTypeName(String damageTypeName) {
        this.damageTypeName = damageTypeName;
    }

    public long getNumberOfElements() { return numberOfElements; }

    public void setNumberOfElements(long numberOfElements) { this.numberOfElements = numberOfElements; }
}
