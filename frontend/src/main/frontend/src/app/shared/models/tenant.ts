import { Address } from "./address";

export interface Tenant {
  id: number;
  firstName: string;
  lastName: string;
  birthDate: Date;
  email: string;
  phoneNumber: string;
  username: string;
  confirmed: boolean;
  address: Address;
  numberOfElements?: number;
}
