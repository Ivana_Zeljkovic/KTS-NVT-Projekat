export interface Comment {
  id: number;
  content: string;
  creatorUsername: string;
  date: Date;
  numberOfElements?: number;
}
