import {browser, by, element, ElementFinder} from "protractor";
// page
import {LoginPage} from "../../page-objects/login.po";
import {ConfirmDeletePage} from "../../page-objects/confirm-delete.po";
import {BusinessesListPage} from "./page-objects/businesses-list.po";
import {BusinessRegistrationPage} from "../../page-objects/business-registration.po";
import {BusinessAddAcountModalPage} from "../../shared/business-add-account/page-objects/businesss-add-account.po";

describe('Businesses list view', () => {
  let loginPage: LoginPage;
  let businessesListPage: BusinessesListPage;
  let confirmDeletePage: ConfirmDeletePage;
  let businessRegistrationPage: BusinessRegistrationPage;
  let businessAddAccount: BusinessAddAcountModalPage;
  let empty: any;
  let newBusiness: any;

  beforeEach(() => {
    loginPage = new LoginPage();
    businessesListPage = new BusinessesListPage();
    confirmDeletePage = new ConfirmDeletePage();
    browser.get("/");

    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');
    loginPage.setUsername('ivana');
    loginPage.setPassword('ivana');
    loginPage.getSignInButton().click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/home');
    browser.get('http://localhost:49152/#/businesses');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/businesses');

    newBusiness = {
      'name' : 'Aaaaaaaaa',
      'decription' : 'MUP Novi Sad je novosadsko odeljenje ministartstva unutrasnjih poslova',
      'businessType' : '',
      'email' : 'informatika@gmail.com',
      'phoneNumber' : '315890',
      'city' : 'Novi Sad',
      'postalCode' : '21000',
      'number' : '79',
      'street' : 'Bulevar Kralja Petra'

    };

  });

  afterEach(() => {
    browser.executeScript('window.localStorage.clear();');
  });

  it("should successfully load page and all of its components", () => {

    expect(businessesListPage.getInstitutionsHeading().isDisplayed()).toBe(true);
    expect(businessesListPage.getDropdownToggleInstitution().isDisplayed()).toBe(true);
    expect(businessesListPage.getSortButtonInstitution().isDisplayed()).toBe(true);
    expect(businessesListPage.getRegisterButtonInstitution().isDisplayed()).toBe(true);
    expect(businessesListPage.getInstitutionTable().isDisplayed()).toBe(true);
    expect(businessesListPage.getPaginationInstitution().isDisplayed()).toBe(true);

    expect(businessesListPage.getSortButtonInstitution().isEnabled()).toBe(true);
    expect(businessesListPage.getRegisterButtonInstitution().isEnabled()).toBe(true);

    expect(businessesListPage.getFirmsHeading().isDisplayed()).toBe(true);
    expect(businessesListPage.getDropdownToggleFirm().isDisplayed()).toBe(true);
    expect(businessesListPage.getSortButtonFirm().isDisplayed()).toBe(true);
    expect(businessesListPage.getRegisterButtonFirm().isDisplayed()).toBe(true);
    expect(businessesListPage.getFirmTable().isDisplayed()).toBe(true);
    expect(businessesListPage.getPaginationFirm().isDisplayed()).toBe(true);

    expect(businessesListPage.getSortButtonFirm().isEnabled()).toBe(true);
    expect(businessesListPage.getRegisterButtonFirm().isEnabled()).toBe(true);

  });

  it("should be sorted institutions table by name when loaded", () => {
    let nameFirstRow = element(by.xpath("//tr[1]/td[1]"));
    let nameLastRow = element(by.xpath("//tr[3]/td[1]"));
    expect(nameFirstRow < nameLastRow);
  });

  it("should sort institutions by email", () => {
    businessesListPage.getDropdownToggleInstitution().click();
    element(by.css('.dropdown-menu li:nth-child(2)')).click();
    expect(businessesListPage.getDropdownToggleInstitution().getText()).toBe("Email");
    businessesListPage.getSortButtonInstitution().click();
    let emailFirstRow = element(by.xpath("//tr[1]/td[2]"));
    let emailLastRow = element(by.xpath("//tr[3]/td[2]"));
    expect(emailFirstRow < emailLastRow);

  });

  it("should sort institutions by name", () => {
    businessesListPage.getDropdownToggleInstitution().click();
    element(by.css('.dropdown-menu li:nth-child(1)')).click();
    expect(businessesListPage.getDropdownToggleInstitution().getText()).toBe("Name");
    businessesListPage.getSortButtonInstitution().click();
    let nameFirstRow = element(by.xpath("//tr[1]/td[1]"));
    let nameLastRow = element(by.xpath("//tr[3]/td[1]"));
    expect(nameFirstRow < nameLastRow);
  });

  it("should sort institutions by phone number", () => {
    businessesListPage.getDropdownToggleInstitution().click();
    element(by.css('.dropdown-menu li:nth-child(3)')).click();
    expect(businessesListPage.getDropdownToggleInstitution().getText()).toBe("Phone number");
    businessesListPage.getSortButtonInstitution().click();
    let phoneNumberFirstRow = element(by.xpath("//tr[1)]/td[3]"));
    let phoneNumberLastRow = element(by.xpath("//tr[3]/td[3]"));
    expect(phoneNumberFirstRow < phoneNumberLastRow);
  });

  it("should sort institutions by business type", () => {
    businessesListPage.getDropdownToggleInstitution().click();
    element(by.css('.dropdown-menu li:nth-child(4)')).click();
    expect(businessesListPage.getDropdownToggleInstitution().getText()).toBe("Business type");
    businessesListPage.getSortButtonInstitution().click();
    let businessTypeFirstRow = element(by.xpath("//tr[1]/td[4]"));
    let businessTypeLastRow = element(by.xpath("//tr[3]/td[4]"));
    expect(businessTypeFirstRow < businessTypeLastRow);
  });


  it('should open page with more details about institution when click on button', () => {
    let moreButton = element.all(by.xpath("//tr[1]/td[6]")).get(0).element(by.css('.btn.btn-default'));
    moreButton.click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/businesses/102');
  });


  it("should register new institution and add it to table", () => {

    businessesListPage.getRegisterButtonInstitution().click();
    businessRegistrationPage = new BusinessRegistrationPage();
    businessAddAccount = new BusinessAddAcountModalPage();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/registration/institution');
    businessRegistrationPage.setName(newBusiness.name);
    businessRegistrationPage.setEmail(newBusiness.email);
    businessRegistrationPage.setPhoneNumber(newBusiness.phoneNumber);
    businessRegistrationPage.setStreet(newBusiness.street);
    businessRegistrationPage.setNumber(newBusiness.number);
    businessRegistrationPage.setCity(newBusiness.city);
    businessRegistrationPage.setPostalCode(newBusiness.postalCode);
    businessRegistrationPage.setDescription(newBusiness.decription);

    businessRegistrationPage.getAddNewAccountButton().click().then(function () {
      businessAddAccount.setUsername('jovana12');
      businessAddAccount.setPassword('jovana12');
      businessAddAccount.setConfirmPassword('jovana12');
      businessAddAccount.getAddAccountButton().click();
    });

    expect(businessRegistrationPage.getRegisterButton().isEnabled()).toBe(true);
    businessRegistrationPage.getRegisterButton().submit();

    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/registration/institution');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/home');
    browser.get('http://localhost:49152/#/businesses');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/businesses');
    let nameFirstRow = element(by.xpath("//tr[1]/td[1]"));
    expect(nameFirstRow == newBusiness.name);
  });



  it("should delete institution and remove it from table", () => {

    let deleteButton = element.all(by.xpath("//tr[1]/td[5]")).get(0).element(by.css('.btn.btn-danger'));
    deleteButton.click();

    browser.wait(function() {

      return confirmDeletePage.getConfirmDeleteElement().isDisplayed();
    }).then(function() {

      expect(confirmDeletePage.getConfirmButton().isDisplayed()).toBe(true);
      expect(confirmDeletePage.getConfirmButton().isEnabled()).toBe(true);
      expect(confirmDeletePage.getDeclineButton().isDisplayed()).toBe(true);
      expect(confirmDeletePage.getDeclineButton().isEnabled()).toBe(true);

      confirmDeletePage.getConfirmButton().click();
      expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/businesses');
      let nameFirstRow = element(by.xpath("//tr[1]/td[1]"));
      expect(nameFirstRow != newBusiness.name);
      });
    });

  it("should be sorted firms table by name when loaded", () => {
    let nameFirstRow = element.all(by.xpath("//tr[1]/td[1]")).get(1);
    let nameLastRow = element.all(by.xpath("//tr[3]/td[1]")).get(1);
    expect(nameFirstRow < nameLastRow);
  });

  it("should sort firms by email", () => {
    businessesListPage.getDropdownToggleFirm().click();
    element(by.css('#dropdownMenuFirms li:nth-child(2)')).click();
    expect(businessesListPage.getDropdownToggleFirm().getText()).toBe("Email");
    businessesListPage.getSortButtonFirm().click();
    let emailFirstRow = element.all(by.xpath("//tr[1]/td[2]")).get(1);
    let emailLastRow = element.all(by.xpath("//tr[3]/td[2]")).get(1);
    expect(emailFirstRow < emailLastRow);

  });

  it("should sort firms by name", () => {
    businessesListPage.getDropdownToggleFirm().click();
    element(by.css('#dropdownMenuFirms li:nth-child(1)')).click();
    expect(businessesListPage.getDropdownToggleFirm().getText()).toBe("Name");
    businessesListPage.getSortButtonFirm().click();
    let nameFirstRow = element.all(by.xpath("//tr[1]/td[1]")).get(1);
    let nameLastRow = element.all(by.xpath("//tr[3]/td[1]")).get(1);
    expect(nameFirstRow < nameLastRow);
  });

  it("should sort firms by phone number", () => {
    businessesListPage.getDropdownToggleFirm().click();
    element(by.css('#dropdownMenuFirms li:nth-child(3)')).click();
    expect(businessesListPage.getDropdownToggleFirm().getText()).toBe("Phone number");
    businessesListPage.getSortButtonFirm().click();
    let phoneNumberFirstRow = element.all(by.xpath("//tr[1)]/td[3]")).get(1);
    let phoneNumberLastRow = element.all(by.xpath("//tr[3]/td[3]")).get(1);
    expect(phoneNumberFirstRow < phoneNumberLastRow);
  });

  it("should sort firms by business type", () => {
    businessesListPage.getDropdownToggleFirm().click();
    element(by.css('#dropdownMenuFirms li:nth-child(4)')).click();
    expect(businessesListPage.getDropdownToggleFirm().getText()).toBe("Business type");
    businessesListPage.getSortButtonFirm().click();
    let businessTypeFirstRow = element.all(by.xpath("//tr[1]/td[4]")).get(1);
    let businessTypeLastRow = element.all(by.xpath("//tr[3]/td[4]")).get(1);
    expect(businessTypeFirstRow < businessTypeLastRow);
  });


  it('should open page with more details about firms when click on button', () => {
    let moreButton = element.all(by.xpath("//tr[1]/td[6]")).get(1).element(by.css('.btn.btn-default'));
    moreButton.click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/businesses/100');
  });


  it("should register new firm and add it to table", () => {

    businessesListPage.getRegisterButtonFirm().click();
    businessRegistrationPage = new BusinessRegistrationPage();
    businessAddAccount = new BusinessAddAcountModalPage();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/registration/firm');
    businessRegistrationPage.setName(newBusiness.name);
    businessRegistrationPage.setEmail(newBusiness.email);
    businessRegistrationPage.setPhoneNumber(newBusiness.phoneNumber);
    businessRegistrationPage.setStreet(newBusiness.street);
    businessRegistrationPage.setNumber(newBusiness.number);
    businessRegistrationPage.setCity(newBusiness.city);
    businessRegistrationPage.setPostalCode(newBusiness.postalCode);
    businessRegistrationPage.setDescription(newBusiness.decription);

    businessRegistrationPage.getAddNewAccountButton().click().then(function () {
      businessAddAccount.setUsername('jovana12');
      businessAddAccount.setPassword('jovana12');
      businessAddAccount.setConfirmPassword('jovana12');
      businessAddAccount.getAddAccountButton().click();
    });

    expect(businessRegistrationPage.getRegisterButton().isEnabled()).toBe(true);
    businessRegistrationPage.getRegisterButton().submit();

    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/registration/firm');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/home');
    browser.get('http://localhost:49152/#/businesses');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/businesses');
    let nameFirstRow = element.all(by.xpath("//tr[1]/td[1]")).get(1);
    expect(nameFirstRow == newBusiness.name);
  });



  it("should delete firm and remove it from table", () => {

    let deleteButton = element.all(by.xpath("//tr[1]/td[5]")).get(1).element(by.css('.btn.btn-danger'));
    deleteButton.click();

    browser.wait(function() {

      return confirmDeletePage.getConfirmDeleteElement().isDisplayed();
    }).then(function() {

      expect(confirmDeletePage.getConfirmButton().isDisplayed()).toBe(true);
      expect(confirmDeletePage.getConfirmButton().isEnabled()).toBe(true);
      expect(confirmDeletePage.getDeclineButton().isDisplayed()).toBe(true);
      expect(confirmDeletePage.getDeclineButton().isEnabled()).toBe(true);

      confirmDeletePage.getConfirmButton().click();
      expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/businesses');
      let nameFirstRow = element.all(by.xpath("//tr[5]/td[1]")).get(1);
      expect(nameFirstRow != newBusiness.name);
    });
  });

  it('should confirm firms registration when click on confirm button', () => {
    let confirmButton = element.all(by.xpath("//tr[2]/td[7]")).get(1).element(by.css('.btn.btn-success'));
    confirmButton.click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/businesses');

    let confirmSpace = element.all(by.xpath("//tr[2]/td[7]")).get(1);
    empty = {
      'empty': ''
    };
    expect(confirmSpace == empty.empty);
  });
});
