package rs.ac.uns.ftn.ktsnvt.repository.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import rs.ac.uns.ftn.ktsnvt.model.entity.Questionnaire;
import rs.ac.uns.ftn.ktsnvt.repository.QuestionRepository;
import rs.ac.uns.ftn.ktsnvt.repository.QuestionnaireRepository;

import static org.junit.Assert.*;

/**
 * Created by Ivana Zeljkovic on 07/12/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class QuestionRepositoryIntegrationTest {

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private QuestionnaireRepository questionnaireRepository;


    @Test
    @Transactional
    @Rollback(true)
    public void testRemoveAllAnswersByQuestionId() {
        /*
        * Test proverava ispravnost rada metode removeAllAnswersByQuestionId repozitorijuma QuestionRepository
        * (uklanjanje svih odgovora koja su vezana za pitanje sa zadatim ID-em)
        * */

        //pitanje sa ID-em = 101 ima odgovore: Yes, No

        Questionnaire questionnaire = this.questionnaireRepository.findOneByIdAndBillboardId(Long.valueOf(101), Long.valueOf(100));

        assertEquals("First question", questionnaire.getQuestions().get(0).getContent());
        assertEquals(2, questionnaire.getQuestions().get(0).getAnswers().size());

        int removed_rows = this.questionRepository.removeAllAnswersByQuestionId(Long.valueOf(101));

        assertEquals(2, removed_rows);
    }

    @Test
    @Transactional
    @Rollback(true)
    public void testRemoveAllByQuestionnaireId() {
        /*
        * Test proverava ispravnost rada metode removeAllByQuestionnaireId repozitorijuma QuestionRepository
        * (uklanjanje svih pitanja koja su vezana za anketu sa zadatim ID-em)
        * */

        /*
        anketa sa ID-em 101 ima sledece podatke:
            id = 101,
            meeting_item_id = 104,
            questions: 1 - id = 101,
                           content = First question,
                           answers: Yes, No
                       2 - id = 102
                           content = Second question
                           answers: Confirm, Decline
        */
        Questionnaire questionnaire = this.questionnaireRepository.findOneByIdAndBillboardId(Long.valueOf(101), Long.valueOf(100));

        assertNotNull(questionnaire);
        assertEquals(Long.valueOf(104), questionnaire.getMeetingItem().getId());
        assertEquals(2, questionnaire.getQuestions().size());
        assertEquals("First question", questionnaire.getQuestions().get(0).getContent());
        assertEquals("Second question", questionnaire.getQuestions().get(1).getContent());
        assertEquals(2, questionnaire.getQuestions().get(0).getAnswers().size());
        assertEquals(2, questionnaire.getQuestions().get(1).getAnswers().size());

        int removed_answers_1 = this.questionRepository.removeAllAnswersByQuestionId(Long.valueOf(101));
        int removed_answers_2 = this.questionRepository.removeAllAnswersByQuestionId(Long.valueOf(102));
        int removed_questions = this.questionRepository.removeAllByQuestionnaireId(Long.valueOf(101));

        assertEquals(2, removed_answers_1);
        assertEquals(2, removed_answers_2);
        assertEquals(2, removed_questions);
    }
}
