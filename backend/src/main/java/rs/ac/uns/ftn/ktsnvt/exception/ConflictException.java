package rs.ac.uns.ftn.ktsnvt.exception;

/**
 * Created by Ivana Zeljkovic on 25/11/2017.
 */
public class ConflictException extends RuntimeException {

    public ConflictException() { }

    public ConflictException(String message) {
        super(message);
    }
}
