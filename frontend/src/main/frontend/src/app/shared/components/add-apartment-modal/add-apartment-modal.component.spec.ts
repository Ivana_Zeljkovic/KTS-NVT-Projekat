import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddApartmentModalComponent } from './add-apartment-modal.component';

describe('AddApartmentModalComponent', () => {
  let component: AddApartmentModalComponent;
  let fixture: ComponentFixture<AddApartmentModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddApartmentModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddApartmentModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
