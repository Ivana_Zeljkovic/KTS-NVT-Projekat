import { browser } from "protractor";
// page
import { LoginPage } from "../../page-objects/login.po";
import { MeetingItemListPage } from "../../home/tenant-home/page-objects/meeting-item-list.po";
import { MeetingItemPage } from "../../page-objects/meeting-item.po";
import { MeetingListPage } from "../../page-objects/meeting-list.po";
import { MeetingAddPage } from "../../meetings/page-objects/meeting-add.po";
import { QuestionnaireListPage } from "../page-objects/questionnaire-list.po";
import { VotePage } from "../page-objects/vote.po";
import { QuestionnairePage } from "../../page-objects/questionnaire.po";
import { QuestionPage } from "../../page-objects/question.po";
import { MeetingItemListModalPage } from "../../meetings/page-objects/meeting-item-list-modal.po";
import { ResultsPage } from "../page-objects/results.po";
// helper
import { DateConverter } from "../../util/date-converter";


describe('Questionnaire list', () => {
  let loginPage: LoginPage;
  let meetingItemListPage: MeetingItemListPage;
  let meetingItemPage: MeetingItemPage;
  let meetingListPage: MeetingListPage;
  let meetingAddPage: MeetingAddPage;
  let meetingItemListModalPage: MeetingItemListModalPage;
  let questionnairePage: QuestionnairePage;
  let questionPage: QuestionPage;
  let questionnaireListPage: QuestionnaireListPage;
  let votePage: VotePage;
  let resultsPage: ResultsPage;
  let newMeetingItem: any;
  let newMeeting: any;
  let firstQuestion: any;
  let existingQuestionnaireIndex: any;
  let numberOfQuestionnaires: any;
  let numberOfFinishedQuestionnaires: any;


  beforeAll(() => {
    loginPage = new LoginPage();
    meetingListPage = new MeetingListPage();
    meetingAddPage = new MeetingAddPage();
    meetingItemListPage = new MeetingItemListPage();
    meetingItemPage = new MeetingItemPage();
    meetingItemListModalPage = new MeetingItemListModalPage();
    questionnairePage = new QuestionnairePage();
    questionPage = new QuestionPage();
    questionnaireListPage = new QuestionnaireListPage();
    votePage = new VotePage();
    resultsPage = new ResultsPage();
    newMeetingItem = {'title':'New title', 'date':'', 'content':'New content', 'creator':''};
    newMeeting = {'date':'', 'duration':''};
    firstQuestion = {'content':'First question?', 'answer1':'First answer', 'answer2':'Second answer'};
    existingQuestionnaireIndex = {'value': -1};
    numberOfQuestionnaires = {'value': 0};
    numberOfFinishedQuestionnaires = {'value': 0};


    browser.get('/');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');

    loginPage.setUsername('kaca');
    loginPage.setPassword('kaca');
    loginPage.getSignInButton().click();

    browser.wait(function() {
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/home/notifications';
      });
    }).then(function() {
      browser.get('http://localhost:49152/#/home/meeting-details-items');

      browser.wait(function () {
        return browser.getCurrentUrl().then(value => {
          return value === 'http://localhost:49152/#/home/meeting-details-items';
        });
      }).then(function () {
        expect(meetingItemListPage.getMeetingItemListElement().isDisplayed()).toBe(true);
        // before test we should add new meeting item
        expect(meetingItemListPage.getAddButton().isDisplayed()).toBe(true);
        expect(meetingItemListPage.getAddButton().isEnabled()).toBe(true);

        meetingItemListPage.getAddButton().click();

        browser.wait(function () {
          // expect to show modal - meeting item page for create new meeting item
          return meetingItemPage.getMeetingItemElement().isDisplayed();
        }).then(function () {
          meetingItemPage.setTitle(newMeetingItem.title);
          meetingItemPage.setContent(newMeetingItem.content);
          meetingItemPage.getQuestionnaireLink().click();

          browser.wait(function() {
            return questionnairePage.getQuestionnaireElement().isDisplayed();
          }).then(function() {
            expect(questionnairePage.getLinkForCreateQuestion().isDisplayed()).toBe(true);
            questionnairePage.getLinkForCreateQuestion().click();

            browser.wait(function() {
              return questionPage.getQuestionElement().isDisplayed();
            }).then(function() {
              questionPage.setContent(firstQuestion.content);
              questionPage.setNewAnswer(firstQuestion.answer1);
              questionPage.getAddAnswerButton().click();
              questionPage.setNewAnswer(firstQuestion.answer2);
              questionPage.getAddAnswerButton().click();

              questionPage.getSaveButton().click();

              browser.wait(function() {
                return questionnairePage.getSaveButton().isEnabled();
              }).then(function () {
                questionnairePage.getSaveButton().click();

                expect(meetingItemPage.getSaveButton().isDisplayed()).toBe(true);
                meetingItemPage.getSaveButton().click();

                browser.waitForAngular();
                expect(meetingItemListPage.getTitleFromMeetingItemPanel(0).getText()).toEqual(newMeetingItem.title);
                meetingItemListPage.getDateFromMeetingItemPanel(0).getText().then(date => {
                  newMeetingItem.date = date;
                });
                meetingItemListPage.getCreatorNameFromMeetingItemPanel(0).getText().then(creator => {
                  newMeetingItem.creator = creator;
                });

                browser.get('http://localhost:49152/#/meetings');

                browser.wait(function() {
                  return meetingListPage.getMeetingListElement().isDisplayed();
                });
              });
            });
          });
        });
      });
    });
  });

  afterAll(() => {
    browser.executeScript('window.localStorage.clear();');
  });



  it('should add new meeting with previous created meeting item', () => {
    expect(meetingListPage.getAddLink().isDisplayed()).toBe(true);

    meetingListPage.getAddLink().click();

    browser.wait(function() {
      return meetingAddPage.getMeetingAddElement().isDisplayed();
    }).then(function() {
      expect(meetingAddPage.getMeetingDuration().isDisplayed()).toBe(true);
      expect(meetingAddPage.getChooseMeetingItemsLink().isDisplayed()).toBe(true);

      meetingAddPage.getMeetingDuration().clear();
      meetingAddPage.setMeetingDuration('60');
      newMeeting.duration = '60';
      let date = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
      date.setHours(8,0,0);
      newMeeting.date = DateConverter.convertDateToString(date);
      meetingAddPage.getChooseMeetingItemsLink().click();

      browser.wait(function() {
        return meetingItemListModalPage.getMeetingItemListModalElement().isDisplayed();
      }).then(function() {
        meetingItemListModalPage.getMeetingItems().count().then(value => {
          // we expect that in list is for sure one meeting item (that we added in beforeAll section)
          expect(value).toBeGreaterThan(0);

          expect(meetingItemListModalPage.getMeetingItemCreator(0).getText()).toEqual(newMeetingItem.creator);
          meetingItemListModalPage.getMeetingItemDate(0).getText().then(date => {
            let valueDate = date.split(' ')[0];
            let valueTime = `${date.split(' ')[1].split(':')[0]}:${date.split(' ')[1].split(':')[1]}`;
            let existingDate = newMeetingItem.date.split(' ')[0];
            let existingTime = `${newMeetingItem.date.split(' ')[1].split(':')[0]}:${newMeetingItem.date.split(' ')[1].split(':')[1]}`;
            expect(valueDate).toEqual(existingDate);
            expect(valueTime).toEqual(existingTime);
          });
          expect(meetingItemListModalPage.getLabelSelect(0).isDisplayed()).toBe(true);

          meetingItemListModalPage.getLabelSelect(0).click();

          browser.wait(function() {
            return meetingItemListModalPage.getLabelRemove(0).isDisplayed();
          }).then(function() {
            expect(meetingItemListModalPage.getSaveButton().isEnabled()).toBe(true);
            meetingItemListModalPage.getSaveButton().click();
            meetingAddPage = new MeetingAddPage();

            browser.wait(function() {
              return meetingAddPage.getMeetingAddElement().isDisplayed();
            }).then(function() {
              meetingAddPage.getMeetingItems().count().then(numberOfMeetingItems => {
                expect(numberOfMeetingItems).toEqual(1);
                expect(meetingAddPage.getSaveButton().isEnabled()).toBe(true);
                meetingAddPage.getSaveButton().click();

                meetingListPage = new MeetingListPage();

                browser.wait(function () {
                  return meetingListPage.getMeetingListElement().isDisplayed();
                }).then(function () {
                  // expect to see new meeting as first in list
                  meetingListPage.getMeetingDate(0).getText().then(date => {
                    let valueDate = date.split(' ')[0];
                    let valueTime = `${date.split(' ')[1].split(':')[0]}:${date.split(' ')[1].split(':')[1]}`;
                    let existingDate = newMeeting.date.split(' ')[0];
                    let existingTime = `${newMeeting.date.split(' ')[1].split(':')[0]}:${newMeeting.date.split(' ')[1].split(':')[1]}`;
                    expect(valueDate).toEqual(existingDate);
                    expect(valueTime).toEqual(existingTime);
                  });
                  expect(meetingListPage.getMeetingDuration(0).getText()).toEqual(`${newMeeting.duration} minutes`);
                  expect(meetingListPage.getMeetingNumberOfMeetingItems(0).getText()).toEqual('1');
                });
              });
            });
          });
        });
      });
    });
  });



  it('should show list of active questionnaires, and there should be questionnaire from previous created ' +
    'meeting and meeting item', () => {
    browser.get('http://localhost:49152/#/questionnaires');

    browser.wait(function() {
      return questionnaireListPage.getQuestionnaireListElement().isDisplayed();
    }).then(function() {
      expect(questionnaireListPage.getTitle().isDisplayed()).toBe(true);
      questionnaireListPage.getQuestionnaires().count().then(value => {
        // there must be at least one questionnaire
        expect(value).toBeGreaterThanOrEqual(1);

        // iterate to find index of previous added meeting item
        for(let i: number = 0; i < value; i++) {
          expect(questionnaireListPage.getDateFromQuestionnairePanel(i).isDisplayed()).toBe(true);
          expect(questionnaireListPage.getCreatorFromQuestionnairePanel(i).isDisplayed()).toBe(true);
          expect(questionnaireListPage.getDateExpiredForQuestionnaire(i).isDisplayed()).toBe(true);
          expect(questionnaireListPage.getNumberOFQuestions(i).isDisplayed()).toBe(true);
          if(existingQuestionnaireIndex.value === -1) {
            // looking for index of added questionnaire
            questionnaireListPage.getDateFromQuestionnairePanel(i).getText().then(dateOfMeetingItem => {
              if(dateOfMeetingItem === newMeetingItem.date)
                existingQuestionnaireIndex.value = i;
            });
          }
          expect(questionnaireListPage.getMenuFromQuestionnairePanel(i).isDisplayed()).toBe(true);
          questionnaireListPage.getMenuFromQuestionnairePanel(i).click();

          browser.wait(function() {
            return questionnaireListPage.getContentOfMenuFromQuestionnairePanel(i).isDisplayed();
          }).then(function() {
            expect(questionnaireListPage.getReviewOptionFromMenu(i).isDisplayed()).toBe(true);
            expect(questionnaireListPage.getVoteOptionFromMenu(i).isDisplayed()).toBe(true);

            // to close popover window
            questionnaireListPage.getMenuFromQuestionnairePanel(i).click();
          });
        }
      });
    });
  });



  it('should open vote dialog when user click on Vote option in menu of previous added questionnaire', () => {
    if(existingQuestionnaireIndex.value !== -1) {
      expect(questionnaireListPage.getMenuFromQuestionnairePanel(existingQuestionnaireIndex.value).isDisplayed()).toBe(true);
      questionnaireListPage.getMenuFromQuestionnairePanel(existingQuestionnaireIndex.value).click();

      browser.wait(function() {
        return questionnaireListPage.getContentOfMenuFromQuestionnairePanel(existingQuestionnaireIndex.value).isDisplayed();
      }).then(function() {
        expect(questionnaireListPage.getVoteOptionFromMenu(existingQuestionnaireIndex.value).isDisplayed()).toBe(true);
        questionnaireListPage.getVoteOptionFromMenu(existingQuestionnaireIndex.value).click();

        browser.wait(function() {
          return votePage.getVoteElement().isDisplayed();
        }).then(function() {
          expect(votePage.getTitle().isDisplayed()).toBe(true);
          expect(votePage.getSaveButton().isDisplayed()).toBe(true);
          expect(votePage.getSaveButton().isEnabled()).toBe(true);
          expect(votePage.getResetButton().isDisplayed()).toBe(true);
          expect(votePage.getResetButton().isEnabled()).toBe(false);
          expect(votePage.getCancelButton().isDisplayed()).toBe(true);
          expect(votePage.getCancelButton().isEnabled()).toBe(true);
          votePage.getQuestions().count().then(numberOfQuestions => {
            expect(numberOfQuestions).toEqual(1);
          });
          expect(votePage.getQuestionContent(0).getText()).toEqual(firstQuestion.content);
          votePage.getQuestionAnswers(0).count().then(numberOfAnswers => {
            expect(numberOfAnswers).toEqual(2);
          });
          expect(votePage.getQuestionAnswerText(0,0).isDisplayed()).toBe(true);
          expect(votePage.getQuestionAnswerText(0,0).getText()).toEqual(firstQuestion.answer1);
          expect(votePage.getQuestionAnswerRadioButton(0,0).isDisplayed()).toBe(true);
          expect(votePage.getQuestionAnswerText(0,1).isDisplayed()).toBe(true);
          expect(votePage.getQuestionAnswerText(0,1).getText()).toEqual(firstQuestion.answer2);
          expect(votePage.getQuestionAnswerRadioButton(0,1).isDisplayed()).toBe(true);
        });
      });
    }
  });



  it('should find questionnaire and save index of that questionnaire', () => {
    browser.executeScript('window.localStorage.clear();');
    // it must log in Nikola as user, because he has only one apartment in this building, so after voting, questionnaire
    // will be removed from active questionnaire list
    loginPage = new LoginPage();
    questionnaireListPage = new QuestionnaireListPage();
    votePage = new VotePage();

    browser.get('/');
    expect(browser.getCurrentUrl()).toEqual('http://localhost:49152/#/login?returnUrl=%2Fhome');

    loginPage.setUsername('nikola');
    loginPage.setPassword('nikola');
    loginPage.getSignInButton().click();

    browser.wait(function() {
      return browser.getCurrentUrl().then(value => {
        return value === 'http://localhost:49152/#/home/notifications';
      });
    }).then(function() {
      browser.get('http://localhost:49152/#/questionnaires');

      browser.wait(function() {
        return questionnaireListPage.getQuestionnaireListElement().isDisplayed();
      }).then(function() {
        existingQuestionnaireIndex.value = -1;

        // iterate to find index of wanted questionnaire
        questionnaireListPage.getQuestionnaires().count().then(value => {
          numberOfQuestionnaires.value = value;

          for(let i:number = 0; i < value; i++) {
            if(existingQuestionnaireIndex.value === -1) {
              questionnaireListPage.getDateFromQuestionnairePanel(i).getText().then(date => {
                if(date === newMeetingItem.date) {
                  existingQuestionnaireIndex.value = i;
                }
              });
            }
          }
        });
      });
    });
  });



  it('should save vote on questionnaire when user click on Save button, and remove that questionnaire from ' +
    'active questionnaires, if user vote for every personal apartment once', () => {
    if(numberOfQuestionnaires.value !== 0 && existingQuestionnaireIndex.value !== -1) {
      expect(questionnaireListPage.getMenuFromQuestionnairePanel(existingQuestionnaireIndex.value).isDisplayed()).toBe(true);
      questionnaireListPage.getMenuFromQuestionnairePanel(existingQuestionnaireIndex.value).click();

      browser.wait(function () {
        return questionnaireListPage.getContentOfMenuFromQuestionnairePanel(existingQuestionnaireIndex.value).isDisplayed();
      }).then(function () {
        expect(questionnaireListPage.getVoteOptionFromMenu(existingQuestionnaireIndex.value).isDisplayed()).toBe(true);
        questionnaireListPage.getVoteOptionFromMenu(existingQuestionnaireIndex.value).click();

        browser.wait(function () {
          return votePage.getVoteElement().isDisplayed();
        }).then(function () {
          expect(votePage.getQuestionAnswerText(0, 0).getText()).toEqual(firstQuestion.answer1);
          expect(votePage.getQuestionAnswerRadioButton(0, 0).isSelected()).toBe(true);
          expect(votePage.getSaveButton().isEnabled()).toBe(true);

          votePage.getSaveButton().click();

          questionnaireListPage.getQuestionnaires().count().then(newValue => {
            expect(newValue).toBeLessThanOrEqual(numberOfQuestionnaires.value);
          });
        });
      });
    }
  });



  it('should show list with questionnaires that are expired with option for review results of that questionnaire', () => {
    questionnaireListPage = new QuestionnaireListPage();
    browser.get('http://localhost:49152/#/questionnaires/results');

    browser.wait(function() {
      return questionnaireListPage.getQuestionnaireListElement().isDisplayed();
    }).then(function() {
      expect(questionnaireListPage.getTitle().isDisplayed()).toBe(true);
      questionnaireListPage.getQuestionnaires().count().then(value => {
        numberOfFinishedQuestionnaires.value = value;

        if(value > 0) {
          for (let i: number = 0; i < value; i++) {
            expect(questionnaireListPage.getDateFromQuestionnairePanel(i).isDisplayed()).toBe(true);
            expect(questionnaireListPage.getCreatorFromQuestionnairePanel(i).isDisplayed()).toBe(true);
            expect(questionnaireListPage.getDateExpiredForQuestionnaire(i).isDisplayed()).toBe(true);
            // date expired must be before date in current moment
            questionnaireListPage.getDateExpiredForQuestionnaire(i).getText().then(dateExpired => {
              let date = DateConverter.convertStringToDate(dateExpired).getTime();
              let currentDate = new Date().getTime();
              expect(currentDate - date).toBeGreaterThan(0);
            });
            expect(questionnaireListPage.getNumberOFQuestions(i).isDisplayed()).toBe(true);
            expect(questionnaireListPage.getDateFromQuestionnairePanel(i).isDisplayed()).toBe(true);
            expect(questionnaireListPage.getMenuFromQuestionnairePanel(i).isDisplayed()).toBe(true);
            questionnaireListPage.getMenuFromQuestionnairePanel(i).click();

            browser.wait(function () {
              return questionnaireListPage.getContentOfMenuFromQuestionnairePanel(i).isDisplayed();
            }).then(function () {
              expect(questionnaireListPage.getReviewOptionFromMenu(i).isDisplayed()).toBe(true);
              expect(questionnaireListPage.getResultsOptionFromMenu(i).isDisplayed()).toBe(true);

              // to close popover window
              questionnaireListPage.getMenuFromQuestionnairePanel(i).click();
            });
          }
        }
      });
    });
  });



  it('should show all elements on results page when user click on Results option in menu of questionnaire', () => {
    if(numberOfFinishedQuestionnaires.value > 0) {
      let numberOfQuestions = {'value': 0};

      questionnaireListPage.getQuestions(0).count().then(value => {
        numberOfQuestions.value = value;
      });

      questionnaireListPage.getMenuFromQuestionnairePanel(0).click();

      browser.wait(function () {
        return questionnaireListPage.getContentOfMenuFromQuestionnairePanel(0).isDisplayed();
      }).then(function () {
        expect(questionnaireListPage.getReviewOptionFromMenu(0).isDisplayed()).toBe(true);
        expect(questionnaireListPage.getResultsOptionFromMenu(0).isDisplayed()).toBe(true);

        questionnaireListPage.getResultsOptionFromMenu(0).click();

        browser.wait(function() {
          return resultsPage.getResultsElement().isDisplayed();
        }).then(function() {
          expect(resultsPage.getTitle().isDisplayed()).toBe(true);
          expect(resultsPage.getCloseButton().isDisplayed()).toBe(true);
          expect(resultsPage.getNumberOfVotes().isDisplayed()).toBe(true);
          resultsPage.getQuestions().count().then(numOfQuestions => {
            expect(numOfQuestions).toEqual(numberOfQuestions.value);

            for(let i:number = 0; i < numOfQuestions; i++) {
              // every question must have content and answers
              expect(resultsPage.getContentOfQuestion(i).isDisplayed()).toBe(true);

              resultsPage.getQuestionAnswers(i).count().then(numberOFAnswers => {
                expect(numberOFAnswers).toBeGreaterThan(0);

                for(let j:number = 0; j < numberOFAnswers; j++) {
                  // every answer must have content, number of votes and percentage value
                  expect(resultsPage.getAnswerOnQuestion(i, j).isDisplayed()).toBe(true);
                  expect(resultsPage.getVoteNumberForAnswerOnQuestion(i, j).isDisplayed()).toBe(true);
                  expect(resultsPage.getVotePercentageForAnswerOnQuestion(i, j).isDisplayed()).toBe(true);
                }
              });
            }
          });
        });
      });
    }
  });
});
