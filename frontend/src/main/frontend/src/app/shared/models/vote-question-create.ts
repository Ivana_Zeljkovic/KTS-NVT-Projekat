export class VoteQuestionCreate {
  constructor(public questionId: number,
              public answer: string) { }
}
