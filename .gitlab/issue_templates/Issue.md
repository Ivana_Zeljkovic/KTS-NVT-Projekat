# Issue template

1) Description
A clear and concise description of what the issue is about.
Template for description: As a <user role> (or system) I want to <goal>

2) Acceptance criteria
List of issue’s requirements that developers need to implement and test.