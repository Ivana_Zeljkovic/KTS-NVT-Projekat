package rs.ac.uns.ftn.ktsnvt.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;
import rs.ac.uns.ftn.ktsnvt.controller.util.TestUtil;
import rs.ac.uns.ftn.ktsnvt.web.dto.LoginDTO;
import rs.ac.uns.ftn.ktsnvt.web.dto.RecordCreateDTO;
import rs.ac.uns.ftn.ktsnvt.web.dto.TokenDTO;

import javax.annotation.PostConstruct;
import java.nio.charset.Charset;

import static org.hamcrest.Matchers.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Created by Ivana Zeljkovic on 11/12/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class RecordControllerIntegrationTest {

    private static final String URL_PREFIX = "/api/buildings";

    private MediaType contentType = new MediaType(
            MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    private MockMvc mockMvc;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja predsednika skupstine stanara
    private String accessTokenPresident, accessTokenPresidentInvalid;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja stanara
    private String accessTokenTenant;
    // JWT token za pristup REST servisima, dobijen nakon uspesnog logovanja administratora
    private String accessTokenAdmin;
    private MultiValueMap<String, String> params;


    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private TestRestTemplate testRestTemplate;


    @PostConstruct
    public void setup() {
        this.mockMvc = MockMvcBuilders.
                webAppContextSetup(webApplicationContext).apply(springSecurity()).build();
    }

    // pre izvrsavanja testa, neophodno je izvrsiti logovanje u aplikaciju da bismo dobili token
    @Before
    public void login() throws Exception {
        // logovanje predsednika
        LoginDTO loginDTO = new LoginDTO("kaca", "kaca");

        ResponseEntity<TokenDTO> responseEntity =
                testRestTemplate.postForEntity("/api/login",
                        loginDTO,
                        TokenDTO.class);
        // preuzmemo token za logovanog predsednika iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenPresident = responseEntity.getBody().getValue();


        // logovanje predsednika koji je u drugoj zgradi predsednik
        LoginDTO loginDTO2 = new LoginDTO("leontina", "leontina");

        ResponseEntity<TokenDTO> responseEntity2 =
                testRestTemplate.postForEntity("/api/login",
                        loginDTO2,
                        TokenDTO.class);
        // preuzmemo token za logovanog predsednika iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenPresidentInvalid = responseEntity2.getBody().getValue();


        // logovanje stanara
        LoginDTO loginDTOTenant = new LoginDTO("nikola", "nikola");

        ResponseEntity<TokenDTO> responseEntityTenant =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOTenant,
                        TokenDTO.class);
        // preuzmemo token za logovanog stanara iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenTenant = responseEntityTenant.getBody().getValue();


        // logovanje administratora
        LoginDTO loginDTOAdmin = new LoginDTO("ivana", "ivana");

        ResponseEntity<TokenDTO> responseEntityAdmin =
                testRestTemplate.postForEntity("/api/login",
                        loginDTOAdmin,
                        TokenDTO.class);
        // preuzmemo token za logovanog administratora iz zaglavlja odgovora i setujemo ga na globalni nivo,
        // jer ce nam trebati za testiranje REST kontrolera
        this.accessTokenAdmin = responseEntityAdmin.getBody().getValue();

        // za pageable atribut
        params = TestUtil.getParams(
                "0", "10", "ASC", "id", "");
    }

    // TESTOVI za createRecord
    @Test
    @Transactional
    @Rollback(true)
    public void testCreateRecord() throws Exception {
        /*
        * Test proverava ispravnost rada metode createRecord kontrolera RecoredController
        * (kreiranje zapisnika sa odrzanog sastanka skupstine stanara sa zadatim ID-em)
        * */

        RecordCreateDTO recordCreateDTO = new RecordCreateDTO("Record from meeting 103");
        String recordDTOJson = TestUtil.json(recordCreateDTO);

        // kreiranje izvestaja sa odrzanog sastanka sa ID-em = 103
        this.mockMvc.perform(post(URL_PREFIX + "/100/council/meetings/103/record")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(recordDTOJson))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.content").value("Record from meeting 103"));
    }

    @Test
    public void testCreateRecordByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode createRecord kontrolera RecoredController
        * (kreiranje zapisnika sa odrzanog sastanka skupstine stanara sa zadatim ID-em)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        RecordCreateDTO recordCreateDTO = new RecordCreateDTO("Record from meeting 103");
        String recordDTOJson = TestUtil.json(recordCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/100/council/meetings/103/record")
                .contentType(contentType)
                .content(recordDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testCreateRecordByNotAuthenticatedPresident() throws Exception {
        /*
        * Test proverava ispravnost rada metode createRecord kontrolera RecoredController
        * (kreiranje zapisnika sa odrzanog sastanka skupstine stanara sa zadatim ID-em)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu predsednika skupstine stanara
        * */

        RecordCreateDTO recordCreateDTO = new RecordCreateDTO("Record from meeting 103");
        String recordDTOJson = TestUtil.json(recordCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/100/council/meetings/103/record")
                .header("Authentication-Token", this.accessTokenTenant)
                .contentType(contentType)
                .content(recordDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testCreateRecordNonexistentBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode createRecord kontrolera RecoredController
        * (kreiranje zapisnika sa odrzanog sastanka skupstine stanara sa zadatim ID-em)
        * u slucaju kada ne postoji zgrada sa zadatim ID-em
        * */

        RecordCreateDTO recordCreateDTO = new RecordCreateDTO("Record from meeting 103");
        String recordDTOJson = TestUtil.json(recordCreateDTO);

        // ne postoji zgrada sa ID-em = 200
        this.mockMvc.perform(post(URL_PREFIX + "/200/council/meetings/103/record")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(recordDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testCreateRecordPresidentFromInvalidBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode createRecord kontrolera RecoredController
        * (kreiranje zapisnika sa odrzanog sastanka skupstine stanara sa zadatim ID-em)
        * u slucaju kada trenutno logovani predsedni kskupstine stanara nije ujedno i predsednik
        * skupstine stanara u zgradi sa zadatim ID-em
        * */

        RecordCreateDTO recordCreateDTO = new RecordCreateDTO("Record from meeting 103");
        String recordDTOJson = TestUtil.json(recordCreateDTO);

        // ulogovan je stanar sa ID-em = 107 koji je predsednik skupstine u zgradi sa ID-em = 101 (a ne sa ID-em = 100)
        this.mockMvc.perform(post(URL_PREFIX + "/100/council/meetings/103/record")
                .header("Authentication-Token", this.accessTokenPresidentInvalid)
                .contentType(contentType)
                .content(recordDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testCreateRecordNonexistentMeeting() throws Exception {
        /*
        * Test proverava ispravnost rada metode createRecord kontrolera RecoredController
        * (kreiranje zapisnika sa odrzanog sastanka skupstine stanara sa zadatim ID-em)
        * u slucaju kada ne postoji sastanak sa zadatim ID-em
        * */

        RecordCreateDTO recordCreateDTO = new RecordCreateDTO("Record from meeting 103");
        String recordDTOJson = TestUtil.json(recordCreateDTO);

        // ne postoji sastanak sa ID-em = 200
        this.mockMvc.perform(post(URL_PREFIX + "/100/council/meetings/200/record")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(recordDTOJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testCreateRecordAlreadyExist() throws Exception {
        /*
        * Test proverava ispravnost rada metode createRecord kontrolera RecoredController
        * (kreiranje zapisnika sa odrzanog sastanka skupstine stanara sa zadatim ID-em)
        * u slucaju kada za sastanak sa zadatim ID-em vec postoji kreiran zapisnik
        * */

        RecordCreateDTO recordCreateDTO = new RecordCreateDTO("Record from meeting 103");
        String recordDTOJson = TestUtil.json(recordCreateDTO);

        //za sastanak sa ID-em = 100 vec postoji kreiran zapisnik
        this.mockMvc.perform(post(URL_PREFIX + "/100/council/meetings/100/record")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(recordDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateRecordMeetingHasNotFinishedYet() throws Exception {
        /*
        * Test proverava ispravnost rada metode createRecord kontrolera RecoredController
        * (kreiranje zapisnika sa odrzanog sastanka skupstine stanara sa zadatim ID-em)
        * u slucaju kada se sastanak jos uvek nije odrzao
        * */

        RecordCreateDTO recordCreateDTO = new RecordCreateDTO("Record from meeting 103");
        String recordDTOJson = TestUtil.json(recordCreateDTO);

        // sastanak sa ID-em = 101 se jos uvek nije odrzao
        this.mockMvc.perform(post(URL_PREFIX + "/100/council/meetings/101/record")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(recordDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateRecordMissingContent() throws Exception {
        /*
        * Test proverava ispravnost rada metode createRecord kontrolera RecoredController
        * (kreiranje zapisnika sa odrzanog sastanka skupstine stanara sa zadatim ID-em)
        * u slucaju kada je dolazni DTO objekat nevalidan: ne sadrzi sva neophodna polja - nedostaje polje content
        * */

        RecordCreateDTO recordCreateDTO = new RecordCreateDTO();
        // nedostaje vrednost polja content
        recordCreateDTO.setContent(null);
        String recordDTOJson = TestUtil.json(recordCreateDTO);

        this.mockMvc.perform(post(URL_PREFIX + "/100/council/meetings/103/record")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(recordDTOJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateRecordInappropriateTypeOfContent() throws Exception {
        /*
        * Test proverava ispravnost rada metode createRecord kontrolera RecoredController
        * (kreiranje zapisnika sa odrzanog sastanka skupstine stanara sa zadatim ID-em)
        * u slucaju kada je dolazni DTO objekat nevalidan: sadrzi sva neophodna polja, ali polje content ima nevalidan tip
        * */

        // vrednost polja content neodgovarajuceg je tipa
        String recordDTOJson = "{content:1}";

        this.mockMvc.perform(post(URL_PREFIX + "/100/council/meetings/103/record")
                .header("Authentication-Token", this.accessTokenPresident)
                .contentType(contentType)
                .content(recordDTOJson))
                .andExpect(status().isBadRequest());
    }

    // TESTOVI za getRecords
    @Test
    public void testGetRecords() throws Exception {
        /*
        * Test proverava ispravnost rada metode getRecords kontrolera RecoredController
        * (dobavljanje liste svih zapisnika sa odrzanih sastanaka skupstine stanara u zgradi sa zadatim ID-em)
        * */

        /*
        zgrada sa ID-em ima samo jedan zapisnik za odrzani sastanak koji ima sledece podatke:
            id = 100,
            content = First record
        */
        this.mockMvc.perform(get(URL_PREFIX + "/100/council/records")
                .header("Authentication-Token", this.accessTokenTenant)
                .params(params))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.[*]", hasSize(1)))
                .andExpect(jsonPath("$.[0].id").value(100))
                .andExpect(jsonPath("$.[0].content").value("First record"));
    }

    @Test
    public void testGetRecordsByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode getRecords kontrolera RecoredController
        * (dobavljanje liste svih zapisnika sa odrzanih sastanaka skupstine stanara u zgradi sa zadatim ID-em)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/100/council/records")
                .params(params))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetRecordsByNotAuthenticatedTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode getRecords kontrolera RecoredController
        * (dobavljanje liste svih zapisnika sa odrzanih sastanaka skupstine stanara u zgradi sa zadatim ID-em)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu stanara
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/100/council/records")
                .header("Authentication-Token", this.accessTokenAdmin)
                .params(params))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetRecordsNonexistentBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode getRecords kontrolera RecoredController
        * (dobavljanje liste svih zapisnika sa odrzanih sastanaka skupstine stanara u zgradi sa zadatim ID-em)
        * u slucaju kada ne postoji zgrada sa zadatim ID-em
        * */

        // ne postoji zgrada sa ID-em = 200
        this.mockMvc.perform(get(URL_PREFIX + "/200/council/records")
                .header("Authentication-Token", this.accessTokenTenant)
                .params(params))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetRecordsPresidentFromInvalidBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode getRecords kontrolera RecoredController
        * (dobavljanje liste svih zapisnika sa odrzanih sastanaka skupstine stanara u zgradi sa zadatim ID-em)
        * u slucaju kada trenutno logovani predsedni kskupstine stanara nije ujedno i predsednik
        * skupstine stanara u zgradi sa zadatim ID-em
        * */

        // ulogovan je stanar sa ID-em = 107 koji je predsednik skupstine u zgradi sa ID-em = 101 (a ne sa ID-em = 100)
        this.mockMvc.perform(get(URL_PREFIX + "/100/council/records")
                .header("Authentication-Token", this.accessTokenPresidentInvalid)
                .params(params))
                .andExpect(status().isForbidden());
    }

    // TESTOVI za getRecordById
    @Test
    public void testGetRecordById() throws Exception {
        /*
        * Test proverava ispravnost rada metode getRecordById kontrolera RecoredController
        * (dobavljanje konkretnog zapisnika sa odrzanog sastanaka skupstine stanara u zgradi sa zadatim ID-em,
        * na osnovu zadatog ID-a sastanka)
        * */

        /*
        zgrada sa ID-em ima samo jedan zapisnik za odrzani sastanak koji ima sledece podatke:
            id = 100,
            content = First record
        */
        this.mockMvc.perform(get(URL_PREFIX + "/100/council/records/100")
                .header("Authentication-Token", this.accessTokenTenant))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(100))
                .andExpect(jsonPath("$.content").value("First record"));
    }

    @Test
    public void testGetRecordByIdByNotAuthenticatedUser() throws Exception {
        /*
        * Test proverava ispravnost rada metode getRecordById kontrolera RecoredController
        * (dobavljanje konkretnog zapisnika sa odrzanog sastanaka skupstine stanara u zgradi sa zadatim ID-em,
        * na osnovu zadatog ID-a sastanka)
        * u slucaju kada akciju izvrsava nelogovani korisnik
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/100/council/records/100"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetRecordByIdByNotAuthenticatedTenant() throws Exception {
        /*
        * Test proverava ispravnost rada metode getRecordById kontrolera RecoredController
        * (dobavljanje konkretnog zapisnika sa odrzanog sastanaka skupstine stanara u zgradi sa zadatim ID-em,
        * na osnovu zadatog ID-a sastanka)
        * u slucaju kada akciju izvrsava logovani korisnik koji nema ulogu stanara
        * */

        this.mockMvc.perform(get(URL_PREFIX + "/100/council/records/100")
                .header("Authentication-Token", this.accessTokenAdmin))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetRecordByIdNonexistentBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode getRecordById kontrolera RecoredController
        * (dobavljanje konkretnog zapisnika sa odrzanog sastanaka skupstine stanara u zgradi sa zadatim ID-em,
        * na osnovu zadatog ID-a sastanka)
        * u slucaju kada ne postoji zgrada sa zadatim ID-em
        * */

        // ne postoji zgrada sa ID-em = 200
        this.mockMvc.perform(get(URL_PREFIX + "/200/council/records/100")
                .header("Authentication-Token", this.accessTokenTenant))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetRecordByIdPresidentFromInvalidBuilding() throws Exception {
        /*
        * Test proverava ispravnost rada metode getRecordById kontrolera RecoredController
        * (dobavljanje konkretnog zapisnika sa odrzanog sastanaka skupstine stanara u zgradi sa zadatim ID-em,
        * na osnovu zadatog ID-a sastanka)
        * u slucaju kada trenutno logovani predsedni kskupstine stanara nije ujedno i predsednik
        * skupstine stanara u zgradi sa zadatim ID-em
        * */

        // ulogovan je stanar sa ID-em = 107 koji je predsednik skupstine u zgradi sa ID-em = 101 (a ne sa ID-em = 100)
        this.mockMvc.perform(get(URL_PREFIX + "/100/council/records/100")
                .header("Authentication-Token", this.accessTokenPresidentInvalid))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetRecordByIdNonexistentRecord() throws Exception {
        /*
        * Test proverava ispravnost rada metode getRecordById kontrolera RecoredController
        * (dobavljanje konkretnog zapisnika sa odrzanog sastanaka skupstine stanara u zgradi sa zadatim ID-em,
        * na osnovu zadatog ID-a sastanka)
        * u slucaju kada ne postoji izvestaj sa zadatim ID-em
        * */

        // ne postoji izvestaj sa ID-em = 200
        this.mockMvc.perform(get(URL_PREFIX + "/100/council/records/200")
                .header("Authentication-Token", this.accessTokenTenant))
                .andExpect(status().isNotFound());
    }
}
