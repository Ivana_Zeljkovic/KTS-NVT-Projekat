import {by, element, ElementArrayFinder, ElementFinder} from "protractor";

export class BusinessesListPage {
  // get methods institutions
  getInstitutionsHeading(): ElementFinder {
    return element(by.id('headingInstitutions'));
  }

  getDropdownToggleInstitution(): ElementFinder {
    return element(by.id('dropdownButtonInstitution'));
  }

  getSortButtonInstitution(): ElementFinder {
    return element(by.id('sortInstitution'));
  }

  getRegisterButtonInstitution(): ElementFinder {
    return element(by.id('registerInstitution'));
  }

  getInstitutionTable(): ElementFinder {
    return element(by.id("institutionsTable"));
  }

  getPaginationInstitution(): ElementFinder {
    return element(by.id("paginationInstitution"));
  }

  // get methods firms
  getFirmsHeading(): ElementFinder {
    return element(by.id('headingFirms'));
  }

  getDropdownToggleFirm(): ElementFinder {
    return element(by.id('dropdownButtonFirms'));
  }

  getSortButtonFirm(): ElementFinder {
    return element(by.id('sortFirms'));
  }

  getRegisterButtonFirm(): ElementFinder {
    return element(by.id('registerFirm'));
  }

  getFirmTable(): ElementFinder {
    return element(by.id("firmsTable"));
  }

  getPaginationFirm(): ElementFinder {
    return element(by.id("paginationFirm"));
  }


  getToaster(): ElementFinder {
    return element(by.tagName("toaster-container"));
  }
}
