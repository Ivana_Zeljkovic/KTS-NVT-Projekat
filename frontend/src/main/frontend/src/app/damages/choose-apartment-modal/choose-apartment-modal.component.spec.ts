import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooseApartmentModalComponent } from './choose-apartment-modal.component';

describe('ChooseApartmentModalComponent', () => {
  let component: ChooseApartmentModalComponent;
  let fixture: ComponentFixture<ChooseApartmentModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChooseApartmentModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseApartmentModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
